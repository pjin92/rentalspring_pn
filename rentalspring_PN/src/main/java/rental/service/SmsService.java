package rental.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rental.common.Log;
import rental.constant.Sms;
import rental.dao.SmsDAO;
import whois.whoisSMS;

@Service("smsService")
public class SmsService {

	@Autowired
	SmsDAO smsDAO;
	
	private whoisSMS whois_sms;
	
	synchronized private void getInstance() {
		if(whois_sms==null) {
			whois_sms=new whoisSMS();
		}
	}
	
	/**문자발송. sh_type에는 Sms.TYPE 상수<br>
	 * e_id는 발송 직원id*/
	public String sendSms(String sh_type,int e_id,int m_idx,String sms_to,String sms_from,String sms_content) {
		String msg="";
		try {
			
			if(whois_sms==null) {
				getInstance();
			}
			int sms_idx=1;//현재 sms id 1개만 사용
			Map<String,Object> map_sms=smsDAO.selectSmsDetail(sms_idx);
			
			
			
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("sms_idx", sms_idx);
			map.put("m_idx", m_idx);
			map.put("sh_phone", sms_to);
			map.put("sh_from", sms_from);
			map.put("sh_content", sms_content);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.KOREA);
			Date cNow = new Date();
			String sms_date = sdf.format(cNow);
			map.put("sh_senddate", sms_date);
			map.put("e_id", e_id);
			map.put("sh_resultcode", Sms.RESULTCODE_BEFORE);
			map.put("sh_type", sh_type);
			
			String sms_type="L";// 설정 하지 않는다면 80byte 넘는 메시지는 쪼개져서 sms로 발송, L 로 설정하면 80byte 넘으면 자동으로 lms 변환
			smsDAO.insertSmsHistory(map);
			int sh_idx=Integer.parseInt(map.get("sh_idx")+"");
			
			sms_content = new String(sms_content.getBytes("utf-8"), "ISO-8859-1");
			
			whois_sms.login(map_sms.get("sms_id")+"", map_sms.get("sms_pw")+"");
	
			// UTF-8 설정
			whois_sms.setUtf8();
	
			// 파라메터 설정
			whois_sms.setParams (sms_to,sms_from,sms_content,sms_date,sms_type);
	
			// 문자발송
			whois_sms.emmaSend();
	
			// 결과값 가져오기
			int retCode = whois_sms.getRetCode();
	
			// 발송결과 메세지
			msg=whois_sms.getRetMessage();
	
			// 성공적으로 발송한경우 남은 문자갯수( 종량제 사용의 경우, 남은 발송가능한 문자수를 확인합니다.)
			//int retLastPoint = whois_sms.getLastPoint();
			
			map.put("sh_resultcode", retCode);
			smsDAO.updateSmsHistory(sh_idx, retCode);
			
			/*
			 * Send Message
			 * success : Code=>0, CodeMsg=>Success!!, LastPoint=>9999
			 * fail 1  : Code=>100, CodeMsg=>Not Registered ID
			 * fail 2  : Code=>200, CodeMsg=>Not Enough Point
			 * fail 3  : Code=>300, CodeMsg=>Login Fail
			 * fail 4  : Code=>400, CodeMsg=>No Valid Number
			 * fail 5  : Code=>500, CodeMsg=>No Valid Message
			 * fail 6  : Code=>600, CodeMsg=>Auth Fail
			 * fail 7  : Code=>700, CodeMsg=>Invalid Recall Number
			 */
			switch (retCode) {
				case 0:msg="문자발송 성공";	break;
				case 200:msg="포인트가 부족합니다";break;
				case 600:msg="허가받지 못한 발신자 번호입니다.";break;
			}
		}catch (Exception e) {
			e.printStackTrace();
			Log.warn("msg발송 중 오류"+e.toString());
			msg="발송 중 오류 발생.";
		}
		return msg;
	}
	
	/**sms발송내역 리스트(dataTable)*/
	public List<Map<String,Object>> selectSmsHistoryList(Map<String,Object> map){
		return smsDAO.selectSmsHistoryList(map);
	}
	
	/**count sms발송내역 리스트(dataTable)*/
	public int countSmsHistoryList(){
		return smsDAO.countSmsHistoryList();
	}
}
