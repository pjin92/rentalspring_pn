package rental.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.AssertTrue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import rental.common.Common;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.InstallState;
import rental.constant.MemberFile;
import rental.constant.MemberInfo;
import rental.dao.CommonDAO;
import rental.dao.DeliveryDAO;
import rental.dao.InstallDAO;
import rental.dao.MemberDAO;
import rental.dao.ProductDAO;
import rental.dao.StockDAO;

@Service("installService")
public class InstallService {

	@Autowired
	InstallDAO installDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	MemberDAO memberDAO;
	@Autowired 
	StockDAO stockDAO;
	@Autowired 
	ProductDAO productDAO;

	// public Map<String, Object> selectInstallDetail(int midx) {
	// return installDAO.selectInstallDetail(midx);
	// }

	/**설치확인서 등록 GET. mf와 if 가져옴. if는 null가능 */
	public List<Map<String, Object>> selectinstallFileList(int m_idx) {
		return installDAO.selectinstallFileList(m_idx);
	}

	/** 설치확인서 등록 (재고체크 후 보유량보다 설치량 많으면 등록안됨)*/
	@Transactional(isolation=Isolation.SERIALIZABLE)
	synchronized public String insertInstallFile(Map<String, Object> pmap, MultipartFile file) {
		try {
			int midx = Integer.parseInt(pmap.get("m_idx") + "");
			int mi_idx = Integer.parseInt(pmap.get("mi_idx") + "");
			int eid = Integer.parseInt(((Map) pmap.get("login")).get("e_id") + "");
			
			int if_count=installDAO.countInstallDetail(midx);
			Map<String,Object> member=memberDAO.selectMemberDetail(midx);
			//신 LED만 스톡 변경
			String pr_idx = member.get("m_pr_idx")+"";
			String rentalcode = member.get("pr_rentalcode")+"";
			
			Map<String,Object> iStock = new HashMap<String, Object>();
			System.out.println(rentalcode);
			if(rentalcode.contains("led-")) {
				List<HashMap<String,Object>> stock = productDAO.rProductDetail(pr_idx);
				Map<String, Object> nowStock = stockDAO.checkTechnicianStock(eid);
				int size = stock.size()+1;
				
				
				for(int i=1;i<size;i++) {
					iStock.put("tled_led"+i, stock.get(i-1).get("prp_num"));
				}
				for(int i=1;i<size;i++) {
					int nStock = Integer.parseInt(nowStock.get("tled_led"+i)+"");
					int isStock = Integer.parseInt(iStock.get("tled_led"+i)+"");
					
					if(isStock > nStock) {
						return "재고가 부족합니다.\n재고 요청 후 다시 시도하시기 바랍니다.";
					}
				}
			}else {
				//System.out.println("구LED");
			}
				iStock.put("e_id", eid);
				System.out.println(iStock.entrySet());
				int result = stockDAO.installedStock(iStock);
				if(result <0) {
				return "재고차감 실패";
				}
			
			if(if_count>0) {
				return "이미 등록되었습니다. 설치확인서 재등록을 이용해주세요.";
			}
			String if_file="";
			if(file!=null) {
				//이전 설치확인서 -> 구설치확인서
				List<Map<String,Object>>list_if=memberDAO.selectMemberFileList(midx+"", MemberFile.NAME_INSTALL);
				if(list_if!=null&&list_if.size()>0) {
					for(int i=0;i<list_if.size();i++) {
						int old_mf_idx=Integer.parseInt(list_if.get(i).get("mf_idx")+"");
						memberDAO.updateOldInstallFile(old_mf_idx);
					}
				}
				
				//업로드된 설치확인서
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("m_idx", midx);
				map.put("mf_name", MemberFile.NAME_INSTALL);
				map.put("mf_file", file.getOriginalFilename());
				String mf_memo=(int)(Math.random()*1000000000)+"";
				map.put("mf_memo", mf_memo);
				int mf_idx=memberDAO.insertMemberFile(map);
				
				String path=MemberFile.PATH+map.get("mf_name");
				File f=new File(path);
				if(!f.exists()) {
					f.mkdirs();
				}
				map.put("mf", mf_idx+"_"+mf_memo);
				file.transferTo(new File(path+"/"+mf_idx+"_"+mf_memo));
				
				if_file=mf_idx+"_"+mf_memo;
			}else {
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("m_idx", midx);
				map.put("mf_name", MemberFile.NAME_INSTALL);
				map.put("mf_file", "파일없음");
				map.put("mf_memo", "파일없음");
				int mf_idx=memberDAO.insertMemberFile(map);
				if_file=mf_idx+"_"+"파일없음";
			}
			
			pmap.put("m_idx", midx);
			pmap.put("mi_idx", mi_idx);
			pmap.put("if_name", InstallState.NAME_INSTALL_CONFIRMATION);
			pmap.put("if_file", if_file);
			pmap.put("if_memo", "");
			pmap.put("if_addmemo", pmap.get("if_addmemo"));
			pmap.put("if_extra_memo", pmap.get("if_extra_memo"));
			String if_extra=pmap.get("if_extra")+"";
			if_extra=if_extra.replaceAll("[^0-9]", "");
			if(if_extra.equals("")) {
				if_extra="0";
			}
			pmap.put("if_extra", if_extra);
			
			pmap.put("e_id", eid);
			int if_idx = installDAO.insertInstallFile(pmap);
			//pmap.put("if_idx", if_idx);
			int pr_type = installDAO.getPr_type(pmap.get("m_idx") + "");
			
			//System.out.println("pr_type===="+pr_type);
			int im_idx = installDAO.selectInstallmatch(Integer.parseInt(pmap.get("m_idx") + ""));
			pmap.put("im_idx", im_idx);
			// int result = installDAO.insertInstallState(pmap);
			pmap.put("ds_state", Delivery.STATE_INSTALLED);
			
			String ds_date=(String)pmap.get("ds_date");
			if(ds_date==null||ds_date.trim().equals("")) {
				Calendar now=Calendar.getInstance();
				ds_date=Common.getDateByString(now.getTime());
				pmap.put("ds_date", ds_date);
			}
			
	
			Map<String,Object> memberUp=new HashMap<String, Object>();
			memberUp.put("m_idx", midx);
			memberUp.put("m_install_finish_date", ds_date);
			
			String m_detail=member.get("m_detail")+"";
			String m_process=member.get("m_process")+"";
			String m_jubsoo=pmap.get("m_jubsoo")+"";//일반,선설치
			if(m_jubsoo.equals("선설치")) {
				memberUp.put("m_state", MemberInfo.STATE_INSTALL_FIRST);
				memberUp.put("m_process", MemberInfo.PROCESS_VISITED);
			}else {
				if(m_process.equals(MemberInfo.PROCESS_ILSI_FINISH)||m_process.equals(MemberInfo.PROCESS_RENT_FINISH)) {
					
				}else if(m_process.equals(MemberInfo.PROCESS_JOONGDO)||m_process.equals(MemberInfo.PROCESS_HAEBAN)||m_process.equals(MemberInfo.PROCESS_EXIT)||m_process.equals(MemberInfo.PROCESS_RETURN)){
					
				}else if(m_detail.equals(MemberInfo.DETAIL_ILSI)) {
					memberUp.put("m_process", MemberInfo.PROCESS_ILSI);
				}else {
					memberUp.put("m_process", MemberInfo.PROCESS_RENT);
				}
				
				String m_state=member.get("m_state")+"";
				if(m_state.equals(MemberInfo.STATE_WAITING)) {
					memberUp.put("m_process", MemberInfo.STATE_FINISH);
				}
			}
			
			memberDAO.updateMember(memberUp);
			
			int result1 = installDAO.updateDeliveryState(pmap);
			
			Map<String, Object> rf_date = installDAO.getRentalFinishDate((HashMap)pmap);
			List<HashMap<String, Object>> date = installDAO.selectInstallfinish((HashMap)pmap);
			
			/**CS체크리스트 생성*/
			if(date!=null&&!date.isEmpty()) {
				for (int i = 0; i < date.size(); i++) {
					HashMap<String, Object> hm = new HashMap<String, Object>();
					hm = date.get(i);
					hm.put("m_rental_finish_date", rf_date.get("finishdate")+"");
					String sdate = hm.get("if_log") + "";
					int edate = Integer.parseInt(hm.get("m_rental_finish_date").toString().replaceAll("-", ""));
					int startYear = Integer.parseInt(sdate.substring(0, 4));
					int startMonth = Integer.parseInt(sdate.substring(5, 7));
					int startDate = Integer.parseInt(sdate.substring(8, 10));
					Calendar cal = Calendar.getInstance();
					// Calendar의 Month는 0부터 시작하므로 -1 해준다.
					// Calendar의 기본 날짜를 startDt로 셋팅해준다.
					cal.set(startYear, startMonth - 1, startDate);
					while (true) {
						// 날짜 출력
						System.out.println(Common.getDateByString(cal.getTime()));
						// Calendar의 날짜를 하루씩 증가한다.
						cal.add(Calendar.DATE, Integer.parseInt(hm.get("csm_sycle") + "")); // one day increment
						hm.put("cl_date", cal.getTime());
						hm.put("m_idx", pmap.get("m_idx"));
						int result3 = installDAO.insertCsList(hm);
	
						// 현재 날짜가 종료일자보다 크면 종료
						if (Common.getDateByInteger(cal.getTime()) > edate) {
							break;
						}
					}
				}
			}
		} catch (Exception fe) {
			fe.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return "오류발생";
		}
		return "등록완료";
	}
	
	/** 설치확인서 재등록 */
	@Transactional
	synchronized public String reuploadInstallFile(Map<String, Object> pmap, MultipartFile file) {
		try {
			int midx = Integer.parseInt(pmap.get("m_idx") + "");
			//이전 설치확인서 -> 구설치확인서
			List<Map<String,Object>>list_if=memberDAO.selectMemberFileList(midx+"", MemberFile.NAME_INSTALL);
			if(list_if!=null&&list_if.size()>0) {
				for(int i=0;i<list_if.size();i++) {
					int old_mf_idx=Integer.parseInt(list_if.get(i).get("mf_idx")+"");
					memberDAO.updateOldInstallFile(old_mf_idx);
				}
			}
			
			Map<String,Object> member=memberDAO.selectMemberDetail(midx);
			Map<String,Object> memberUp=new HashMap<String, Object>();
			memberUp.put("m_idx", midx);
			
			String m_detail=member.get("m_detail")+"";
			String m_process=member.get("m_process")+"";
			if(m_process.equals(MemberInfo.PROCESS_ILSI_FINISH)||m_process.equals(MemberInfo.PROCESS_RENT_FINISH)) {
				
			}else if(m_process.equals(MemberInfo.PROCESS_JOONGDO)||m_process.equals(MemberInfo.PROCESS_HAEBAN)||m_process.equals(MemberInfo.PROCESS_EXIT)||m_process.equals(MemberInfo.PROCESS_RETURN)){
				
			}else if(m_detail.equals(MemberInfo.DETAIL_ILSI)) {
				memberUp.put("m_process", MemberInfo.PROCESS_ILSI);
			}else {
				memberUp.put("m_process", MemberInfo.PROCESS_RENT);
			}
			
			String m_state=member.get("m_state")+"";
			if(m_state.equals(MemberInfo.STATE_WAITING)) {
				memberUp.put("m_process", MemberInfo.STATE_FINISH);
			}
			
			memberDAO.updateMember(memberUp);
			
			//업로드된 설치확인서
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("m_idx", midx);
			map.put("mf_name", MemberFile.NAME_INSTALL);
			map.put("mf_file", file.getOriginalFilename());
			String mf_memo=(int)(Math.random()*1000000000)+"";
			map.put("mf_memo", mf_memo);
			int mf_idx=memberDAO.insertMemberFile(map);
			
			String path=MemberFile.PATH+map.get("mf_name");
			File f=new File(path);
			if(!f.exists()) {
				f.mkdirs();
			}
			map.put("mf", mf_idx+"_"+mf_memo);
			file.transferTo(new File(path+"/"+mf_idx+"_"+mf_memo));
			
			String if_file=mf_idx+"_"+mf_memo;
			pmap.put("if_file", if_file);
			installDAO.updateInstallFile(map);
			
		} catch (Exception fe) {
			fe.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return "오류발생";
		}
		return "등록완료";
	}

	/**
	 * CS점검관리 리스트
	 */

	public List<Map<String, Object>> selectInstallList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";

		if (order.equals("rental") || order.equals("yangdo")) {
			order = "m_idx";
		} else if (order.equals("mi_addr")) {
			order = "mi_addr1,mi_addr2,mi_addr3";
		} else if (order.equals("m_free_date")) {
			// 무료체험일 m_eta + m_free_day
		} else if (commonDAO.checkColumn(order) == 0) {
			return null;
		}
		map.put("column", order);

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}

		List<Map<String, Object>> result = installDAO.selectInstallList(map, MemberInfo.TYPE_DELIVERY);
		return result;
	}

	public Map<String, Object> selectinstallFileDown(String if_idx, String if_memo) {
		return installDAO.selectInstallFileDown(if_idx, if_memo);
	}

	/**
	 * 방문예정일 변경
	 */
	public int updateClDate(HashMap<String, Object> rmap) {
		return installDAO.updateClDate(rmap);
	}

	/**
	 * 해당설치건의 기사변경
	 */
	public int updateTechnician(Map<String, Object> map) {
		return installDAO.updateTechnician(map);
	}

	/**
	 * CS점검관리 방문예정일 목록 세부내용 Table로 불러서 AJAX로 append함
	 */
	public List<Map<String, Object>> customerCsList(Map<String, Object> map) {
		return installDAO.selectCsList(map, MemberInfo.TYPE_DELIVERY);
	}

	/**
	 * 해당설치건의 고객정보
	 */
	public Map<String, Object> selectInstallCustomer(Map<String, Object> map) {

		return installDAO.selectInstallCustomer(map, MemberInfo.TYPE_DELIVERY);
	}

	/**
	 * 해당설치건의 고객메모
	 */

	public int updateCsInstallMemo(Map<String, Object> map) {
		return installDAO.updateCsInstallMemo(map);
	}

	/**
	 * 해당 설치건의 상태변경 (설치전->설치요청)
	 */

	public int csInstall(Map<String, Object> map) {
		return installDAO.csInstall(map, Delivery.STATE_REQUEST_INSTALL);
	}

	/**
	 * 해당 설치건의 상태변경 (설치요청->설치완료)
	 */
	@Transactional
	public int csInstallComplete(Map<String, Object> map) {
		int result2 = 0;
		try {
			int result = installDAO.csInstallComplete(map, Delivery.STATE_INSTALLED);
			result2 = installDAO.csInstallCompleteDate(map);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result2;
	}

	public Map<String, Object> selectOrigDate(int cl_idx) {
		return installDAO.selectOrigDate(cl_idx);
	}

	public int updateOrgDate(HashMap<String, Object> rmap) {
		return installDAO.updateOrgDate(rmap);
	}

	public int insertAddCsProduct(Map<String, Object> map) {
		return installDAO.insertAddCsProduct(map);
	}

	public List<Map<String, Object>> selectCsProductList(Map<String, Object> map) {
		return installDAO.selectCsProductList(map);
	}

	public List<Map<String, Object>> selectInstallCalList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";

		if (order.equals("rental") || order.equals("yangdo")) {
			order = "m_idx";
		} else if (order.equals("mi_addr")) {
			order = "mi_addr1,mi_addr2,mi_addr3";
		} else if (order.equals("m_free_date")) {
			// 무료체험일 m_eta + m_free_day
		} else if (commonDAO.checkColumn(order) == 0) {
			return null;
		}
		map.put("column", order);

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}

		List<Map<String, Object>> result = installDAO.selectInstallCalList(map, MemberInfo.TYPE_DELIVERY);
		System.out.println(result.size());
		return result;
	}

	public List<Map<String, Object>> selectInstallListApp(Map<String, Object> map) {
		List<Map<String, Object>> result = installDAO.selectInstallListApp(map, MemberInfo.TYPE_DELIVERY);
		return result;
	}

	public List<HashMap<String, Object>> selectJungsan(String idx) {
		String[] m_idx = null;
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> list = new ArrayList<String>();
		if (idx.contains(",")) {
			m_idx = idx.split(",");
			for (int i = 0; i < m_idx.length; i++) {
				list.add(m_idx[i]);
				System.out.println(list.get(i));
			}
			map.put("m_idx", list);
		} else {
			list.add(idx);
			map.put("m_idx", list);
		}

		return installDAO.selectJungsan(map);
	}

	public int insertJunsan(Map<String, Object> map) {

		List<HashMap<String, Object>> jungsan = new ArrayList<HashMap<String, Object>>();
		
		String jr_name = map.get("jr_name")+"";
		String jr_code = map.get("jr_code")+"";
		String memo = map.get("memo")+"";
		String jr_log = map.get("jr_log")+"";
		String jr_writer= map.get("e_name")+"";
		
		System.out.println(memo);
		
		for (String key : map.keySet()) {
			String id = "";
			if (key.startsWith("m_idx") == true) {
				HashMap<String, Object> jmap = new HashMap<String, Object>();
				id = key.replace("m_idx", "");
				jmap.put("m_idx", Integer.parseInt(id));
				if (key.contains(id)) {
					jmap.put("jr_name",jr_name);
					jmap.put("jr_code", jr_code);
					jmap.put("memo", memo);
					jmap.put("jr_log", jr_log);
					jmap.put("jr_writer", jr_writer);
					jmap.put("jr_adjust", map.get("jr_adjust" + id));
					jmap.put("jr_total", map.get("jr_total" + id));
					jmap.put("jr_memo", map.get("jr_memo" + id));
					jmap.put("jr_reason", map.get("jr_reason" + id));
					jmap.put("jr_e_id", map.get("jr_e_id" + id));
					jungsan.add(jmap);
				}
			}
		}
		return installDAO.insertJungsan(jungsan);
	}
	
	/**if 2.메모 부분 업데이트. 없는 경우 새로 생성(설치확인서 있는 경우 그 정보까지 입력)*/
	synchronized public void updateInstallFile(Map<String,Object>map) {
		String if_extra=map.get("if_extra")+"";
		if_extra=if_extra.replaceAll("[^0-9]", "");
		map.put("if_extra", if_extra);
		
		String if_idx=map.get("if_idx")+"";
		if_idx=if_idx.replaceAll("[^0-9]", "");
		if(if_idx==null||if_idx.equals("")||if_idx.equals("0")) {
			//if가 없는 경우(기존 설치확인서 구전산에서 넘어온 데이터도 저장
			String m_idx=map.get("m_idx")+"";
			List<Map<String,Object>> mfList=memberDAO.selectMemberFileList(m_idx, MemberFile.NAME_INSTALL);
			
			String if_file;
			if(mfList!=null&&mfList.size()>0) {
				Map<String,Object> mf=mfList.get(0);
				String mf_idx=mf.get("mf_idx")+"_"+"";
				String mf_memo=mf.get("mf_memo")+"";
				
				if(mf_memo.equals("")) {
					//구전산자료
					if_file=mf.get("mf_file")+"";
				}else {
					if_file=mf_idx+"_"+mf_memo;
				}
			}else {
				//설치확인서 없는 경우
				if_file="";
			}
			map.put("if_file", if_file);
			map.put("if_name", InstallState.NAME_INSTALL_CONFIRMATION);
			if(!map.containsKey("if_memo")) {
				map.put("if_memo", "");
			}
			installDAO.insertInstallFile(map);
		}else {
			installDAO.updateInstallFile(map);
		}
		
		int midx=Integer.parseInt(map.get("m_idx")+"");
		Map<String,Object> member=memberDAO.selectMemberDetail(midx);
		Map<String,Object> memberUp=new HashMap<String, Object>();
		memberUp.put("m_idx", midx);
		
		String m_detail=member.get("m_detail")+"";
		String m_process=member.get("m_process")+"";
		if(m_process.equals(MemberInfo.PROCESS_ILSI_FINISH)||m_process.equals(MemberInfo.PROCESS_RENT_FINISH)) {
			
		}else if(m_process.equals(MemberInfo.PROCESS_JOONGDO)||m_process.equals(MemberInfo.PROCESS_HAEBAN)||m_process.equals(MemberInfo.PROCESS_EXIT)||m_process.equals(MemberInfo.PROCESS_RETURN)){
			
		}else if(m_detail.equals(MemberInfo.DETAIL_ILSI)) {
			memberUp.put("m_process", MemberInfo.PROCESS_ILSI);
		}else {
			memberUp.put("m_process", MemberInfo.PROCESS_RENT);
		}
		memberDAO.updateMember(memberUp);
	}

	
	/**
	 * 기사배정 취소 (미배정상태로 돌리기)
	 * */
	public String matchCancel(int m_idx) {
		int result =installDAO.matchCancel(m_idx); 
		if(result>0) {
			return "삭제완료";
		}	else {
			return	"삭제실패.다시 시도해주세요";
		}
		
	}

	public List<Map<String, Object>> selectInstallCompleteList(Map<String, Object> map) {
		List<Map<String, Object>> result = installDAO.selectInstallCompleteList(map, MemberInfo.TYPE_DELIVERY);
		return result;
	}

}
