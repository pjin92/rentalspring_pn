package rental.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.format.CellFormatType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.InstallState;
import rental.constant.MemberInfo;
import rental.constant.Pay;
import rental.constant.ProductRental;
import rental.dao.CommonDAO;
import rental.dao.DeliveryDAO;
import rental.dao.MemberDAO;
import rental.dao.PayDAO;
import rental.dao.ProductDAO;

@Service("deliveryService")
public class DeliveryService {

	@Autowired
	DeliveryDAO deliveryDAO;
	@Autowired
	ProductDAO productDAO;
	@Autowired
	MemberDAO memberDAO;

	@Autowired
	CommonDAO commonDAO;
	@Autowired
	PayDAO payDAO;

	/**
	 * ds_idx return. ds_state는 설치전 혹은 배송전을 입력.<br>
	 * ds_history 기본값 'Y'*/
	public int insertDeliveryState(String ds_state, String ds_history) {
		return deliveryDAO.insertDeliveryState(ds_state, ds_history);

	}

	/** insertDelivery */
	public void insertDelivery(int mi_idx, int prp_idx, int ds_idx) {
		deliveryDAO.insertDelivery(mi_idx, prp_idx, ds_idx);
	}

	/**
	 * delivery_state 조회<br>
	 * prp_idxs에는 prp_idx가 ,를 구분자로 들어감(추후 확장성 위해)<br>
	 * 배송자가 2이상인 경우를 위해 list로 받아옴
	 */
	public List<Map<String, Object>> selectDeliveryState(int m_idx) {
		return deliveryDAO.selectDeliveryState(m_idx);
	}

	/** ds_idx로 ds조회 */
	public Map<String, Object> selectDeliveryStateDetail(int ds_idx) {
		return deliveryDAO.selectDeliveryStateDetail(ds_idx);
	}

	/** 구매취소. 배송 정보 초기화. m_state 취소(가망) */
	@Transactional
	public String cancelBuy(int ds_idx,int m_idx) {
		String msg = "";
		try {
			
			Map<String, Object> del = deliveryDAO.selectDeliveryStateDetail(ds_idx);
			if (del == null || del.isEmpty()) {
				return "배송정보가 없습니다.";
			}
			String ds_state = (String) del.get("ds_state");
			if (ds_state == null || ds_state.equals("") || !(ds_state.equals(Delivery.STATE_BEFORE_DELIVERY)||ds_state.equals(Delivery.STATE_REQUEST_DELIVERY)
					|| ds_state.equals(Delivery.STATE_BEFORE_INSTALL)|| ds_state.equals(Delivery.STATE_REQUEST_INSTALL))) {
				return "현재 상태: " + ds_state + "\n배송/설치전,배송/설치요청 상태만 구매취소 가능합니다.";
			}
			Map<String, Object> member = new HashMap<String, Object>();
			member.put("m_idx", m_idx);
			member.put("m_state", MemberInfo.STATE_CANCEL);
			member.put("m_process", MemberInfo.PROCESS_CANCEL_BUY);
			memberDAO.updateMember(member);
			
			// 구매취소로 배송상태 변경
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ds_idx", ds_idx);
			map.put("ds_state", Delivery.STATE_CANCEL_DELIVERY);
			int update = deliveryDAO.updateDeliveryState(map);
			if (update > 0) {
				msg = "구매취소 처리하였습니다.";
			} else {
				msg = "다시 시도해주세요.";
			}
		}catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
		return msg;
	}

	/** 구매취소 취소 */
	@Transactional
	public String cancelBuyCancel(int ds_idx,int m_idx) {
		String msg = "";

		Map<String, Object> del = deliveryDAO.selectDeliveryStateDetail(ds_idx);
		if (del == null || del.isEmpty()) {
			return "배송정보가 없습니다.";
		}
		String ds_state = (String) del.get("ds_state");
		if (ds_state == null || ds_state.equals("") || !(ds_state.equals(Delivery.STATE_CANCEL_DELIVERY))) {
			return "현재 상태: " + ds_state + "\n구매취소 상태일 때만 가능합니다.";
		}
		
		Map<String, Object> member = new HashMap<String, Object>();
		member.put("m_idx", m_idx);
		member.put("m_state", MemberInfo.STATE_FINISH);
		member.put("m_process", MemberInfo.PROCESS_BEFORE_RENT);
		memberDAO.updateMember(member);

		String ds_state_set = "";
		try {
			List<Map<String, Object>> dlist = deliveryDAO.selectDeliveryListByDs(ds_idx);
			if (dlist != null && dlist.size() > 0) {
				//delivery를 ds_idx로 조회해서 첫 상품 기준으로 설치,배송 판별
				Map<String, Object> map_ds=dlist.get(0);
				int prp_idx = Integer.parseInt(map_ds.get("d_prp_idx")+"");
				Map<String, Object> pmap = productDAO.selectPrpDetail(prp_idx);
				int p_type = Integer.parseInt(pmap.get("p_type")+"");
				if (p_type == ProductRental.TYPE_DELIVERY) {
					ds_state_set = Delivery.STATE_BEFORE_DELIVERY;
				} else if (p_type == ProductRental.TYPE_INSTALL) {
					ds_state_set = Delivery.STATE_BEFORE_INSTALL;
				} else {
					return "상품에 설치,배송 데이터가 없습니다.";
				}

			}
			
			String ds_req=(String)del.get("ds_req");
			if(ds_req!=null&&!ds_req.equals("")) {
				if (ds_state_set.equals(Delivery.STATE_BEFORE_DELIVERY)) {
					ds_state_set=Delivery.STATE_REQUEST_DELIVERY;
				}else{
					ds_state_set=Delivery.STATE_REQUEST_INSTALL;
				}
			}
		} catch (Exception e) {
			return "오류발생" + e.toString();
		}
		
		// 배송전or요청 으로 배송상태 변경
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ds_idx", ds_idx);
		map.put("ds_state", ds_state_set);
		int update = deliveryDAO.updateDeliveryState(map);
		if (update > 0) {
			msg = ds_state_set + "으로 상태가 변경되었습니다.";
		} else {

			msg = "다시 시도해주세요.";
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}

		return msg;
	}

	/**설치관리 검색(dt)*/
	public List<Map<String, Object>> selectInstallList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";
		if (order.equals("i_e_name")) {
		} else if (order.equals("mi_addr")) {
		} else if (commonDAO.checkColumn(order) == 0) {
			return null;
		}
		map.put("column", order);

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		return deliveryDAO.selectInstallList(map);
	}
	/**설치관리 검색(dt) cnt*/
	public int countInstallList(Map<String, Object> map) {
		return deliveryDAO.countInstallList(map);
	}
	/**
	 * 배송 정보 저장 및 배송완료<br>
	 * 배송완료일이 있는 경우 배송완료(반품,해지반품,해지)로 상태 변경<br>
	 * 계약완료일=배송완료일. 무료체험인경우만  14일 이후<br>
	 * (option)렌탈 중으로 상태변경
	 */
	@Transactional
	public String delivery(int ds_idx,Map<String, Object> map) {
		String msg = "";
		try {
			String ds_state="";
			
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			int m_idx=Integer.parseInt(map.get("m_idx")+"");
			
			Map<String,Object> member=memberDAO.selectMemberDetail(m_idx);
			String m_jubsoo=member.get("m_jubsoo")+"";
			int pr_type=Integer.parseInt(member.get("pr_type")+"");
			String m_contract_date=map.get("m_contract_date")+"";
			
			Map<String,Object> memberUp=new HashMap<String, Object>();
			memberUp.put("m_idx", m_idx);
			
			String m_final_date=(String)member.get("m_final_date");
			
			String ds_req=(String)map.get("ds_req");
			String ds_date=(String)map.get("ds_date");
			String ds_req_cancel=(String)map.get("ds_req_cancel");
			String ds_date_cancel=(String)map.get("ds_date_cancel");
			
			int ds_state_flag=0;
			if(ds_req!=null&&!ds_req.equals(""))ds_state_flag=1;
			if(ds_date!=null&&!ds_date.equals(""))ds_state_flag=2;
			if(ds_req_cancel!=null&&!ds_req_cancel.equals(""))ds_state_flag=3;
			if(ds_date_cancel!=null&&!ds_date_cancel.equals(""))ds_state_flag=4;
			
			if(ds_state_flag<1) {
				//요청일 없는 경우 배송전,설치전 상태
				if(pr_type==ProductRental.TYPE_DELIVERY) {
					ds_state=Delivery.STATE_BEFORE_DELIVERY;
				}else {
					ds_state=Delivery.STATE_BEFORE_INSTALL;
				}
				
				//최종계약 확정된 경우 설치전,요청으로 돌아갈 수 없음
				if(m_final_date!=null&&!m_final_date.equals("")) {
					return "이미 최종계약 확정된 고객입니다.\n"+ds_state+"으로 상태 변경할 수 없습니다.";
				}
			}else {
				if(ds_state_flag<2) {
					//배송,설치 완료일 없는 경우 배송요청,설치요청 상태(배송요청일 까지 있음)
					if(pr_type==ProductRental.TYPE_DELIVERY) {
						ds_state=Delivery.STATE_REQUEST_DELIVERY;
					}else {
						ds_state=Delivery.STATE_REQUEST_INSTALL;
					}
					m_contract_date="";

					//최종계약 확정된 경우 설치전,요청으로 돌아갈 수 없음
					if(m_final_date!=null&&!m_final_date.equals("")) {
						return "이미 최종계약 확정된 고객입니다.\n"+ds_state+"으로 상태 변경할 수 없습니다.";
					}
				}else {
					//배송완료일 있는 경우 계약완료일 세팅
					if(m_contract_date==null||m_contract_date.equals("")) {
						//계약일 = 입력 없는 경우 무료체험은 배송완료+14일, else 배송완료일
						if(m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)) {
							Calendar cal_cd=Calendar.getInstance();
							cal_cd.setTime(sdf.parse(ds_date));
							cal_cd.add(Calendar.DATE, 14);
							m_contract_date=sdf.format(cal_cd.getTime());
						}else if(m_contract_date.equals("")){
							m_contract_date=ds_date;
						}
					}
					memberUp.put("m_contract_date", m_contract_date);
					
					if(ds_state_flag<3) {
						//반품 요청일 없는 경우 배송완료,설치완료 상태(배송완료일 까지 있음)
						if(pr_type==ProductRental.TYPE_DELIVERY) {
							ds_state=Delivery.STATE_DELIVERED;
						}else {
							ds_state=Delivery.STATE_INSTALLED;
						}
						payDAO.continueRental(m_idx);
					}else {
						//배송완료일 2주 뒤인지 아닌지 판별
						boolean afterTwoWeeks;
						Calendar dsdate=Calendar.getInstance();
						
						if(ds_date==null||ds_date.equals("")) {
							TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
							return "배송/설치 완료일 입력 부탁드립니다.";
						}
						dsdate.setTime(sdf.parse(ds_date));
						dsdate.add(Calendar.DATE, 14);
						Calendar now=Calendar.getInstance();
						if(now.before(dsdate)) {
							//배송완료 2주이내
							afterTwoWeeks=false;
						}else {
							//배송완료 2주이후
							afterTwoWeeks=true;
						}
						
						if(ds_state_flag<4) {
							//반품 완료일 없는 경우 반품요청 상태(반품 요청일 까지 있음)
							if(afterTwoWeeks) {
								//배송완료 2주 이후는 해지반품
								ds_state=Delivery.STATE_REQUEST_HAEBAN;
							}else {
								//무료체험 배송완료 2주이내는 회수
								//배송완료 2주 이내는 반품
								if(m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)) {
									ds_state=Delivery.STATE_REQUEST_EXIT;
								}else{
									ds_state=Delivery.STATE_REQUEST_RETURN;
								}
							}
						}else {
							String pay_state;
							//반품완료일까지 있는 경우
							if(afterTwoWeeks) {
								//배송완료 2주 이후는 해지반품
								ds_state=Delivery.STATE_HAEBAN;
								pay_state=Pay.STATE_HAEBAN;
							}else {
								//무료체험 배송완료 2주이내는 회수
								//배송완료 2주 이내는 반품
								if(m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)) {
									ds_state=Delivery.STATE_EXIT;
									pay_state=Pay.STATE_EXIT;
								}else{
									ds_state=Delivery.STATE_RETURN;
									pay_state=Pay.STATE_RETURN;
								}
							}
							payDAO.noMoreRental(m_idx, pay_state);
						}
					}
				}
			}
			map.put("ds_state", ds_state);
			msg=ds_state+" 상태로 저장됐습니다.";
			
			//배송/설치완료일 ds_date와 맞추기
			memberUp.put("m_install_finish_date", ds_date);//설치완료일
			//계약완료일 업데이트
			memberUp.put("m_contract_date", m_contract_date);
			//m_free_day
			String m_free_day=map.get("m_free_day")+"";
			m_free_day=m_free_day.replaceAll("[^0-9]", "");
			if(m_free_day.equals(""))m_free_day="0";
			memberUp.put("m_free_day",m_free_day);
			
			//진행상태
			String m_detail=member.get("m_detail")+"";//매출구분
			String m_process=member.get("m_process")+"";
			
			if(m_process.equals(MemberInfo.PROCESS_JOONGDO)||m_process.equals(MemberInfo.PROCESS_ILSI_FINISH)||m_process.equals(MemberInfo.PROCESS_RENT_FINISH)) {
				
			}else {
				if(ds_state.equals(Delivery.STATE_BEFORE_DELIVERY)||ds_state.equals(Delivery.STATE_BEFORE_INSTALL)
						||ds_state.equals(Delivery.STATE_REQUEST_DELIVERY)||ds_state.equals(Delivery.STATE_REQUEST_INSTALL)) {
					//배송전,설치전,배송요청,설치요청 렌탈전
					memberUp.put("m_process", MemberInfo.PROCESS_BEFORE_RENT);
				}else if(ds_state.equals(Delivery.STATE_DELIVERED)||ds_state.equals(Delivery.STATE_INSTALLED)) {
					//배송,설치완료
					if(m_detail.equals(MemberInfo.DETAIL_ILSI)) {
						memberUp.put("m_process", MemberInfo.PROCESS_ILSI);
					}else {
//						if(m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)) {
//							//무료체험일 때
//							Calendar checkFree=Calendar.getInstance();
//							checkFree.setTime(sdf.parse(ds_date));
//							int i_m_free=Integer.parseInt(m_free_day);
//							checkFree.add(Calendar.DATE, i_m_free);
//							
//							Calendar now=Calendar.getInstance();
//							
//							if(now.before(checkFree)) {
//								memberUp.put("m_process", MemberInfo.PROCESS_TEST);
//							}else {
//								memberUp.put("m_process", MemberInfo.PROCESS_RENT);
//							}
//							
//						}else {
							//무료체험 아닐 때
							memberUp.put("m_process", MemberInfo.PROCESS_RENT);
//						}
					}
				}else if(ds_state.equals(Delivery.STATE_EXIT)){
					//회수
					memberUp.put("m_state", MemberInfo.STATE_CANCEL);
					memberUp.put("m_process", MemberInfo.PROCESS_EXIT);
				}else if(ds_state.equals(Delivery.STATE_HAEBAN)) {
					//해지반품
					memberUp.put("m_state", MemberInfo.STATE_CANCEL);
					memberUp.put("m_process", MemberInfo.PROCESS_HAEBAN);
				}else if(ds_state.equals(Delivery.STATE_RETURN)) {
					//반품
					memberUp.put("m_state", MemberInfo.STATE_CANCEL);
					memberUp.put("m_process", MemberInfo.PROCESS_RETURN);
				}
				
			}
			
			//체험중인 경우 그대로 체험 중
			if(m_process.equals(MemberInfo.PROCESS_TEST)){
				memberUp.put("m_process", MemberInfo.PROCESS_TEST);
			}
			
			memberDAO.updateMember(memberUp);
			
			map.put("ds_idx", ds_idx);
			deliveryDAO.updateDeliveryState(map);
			
		} catch (Exception e) {
			e.printStackTrace();
			msg = "오류 발생";
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg;
	}

	/**기사 매칭*/
	@Transactional
	public int insertMatchTechnician(Map<String, Object> map) {
		try {
			int ds_idx = deliveryDAO.selectdsIdx(map);
			Map<String, Object> ds = deliveryDAO.selectDeliveryStateDetail(ds_idx);
			map.put("ds_idx", ds_idx);
			if(ds.get("ds_req")==null || ds.get("ds_req").equals("")) {
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				map.put("ds_req",sdf.format(calendar.getTime()));
			}
			if (ds.get("ds_date")==null || ds.get("ds_date").equals("")) {
				map.put("ds_state", Delivery.STATE_REQUEST_INSTALL);
			} else {
				map.put("ds_state", Delivery.STATE_INSTALLED);
			}
			deliveryDAO.updateDeliveryState(map);
			
			deliveryDAO.deleteInstall_match(map);
			deliveryDAO.insertMatchTechnician(map);
			return 1;
		} catch (Exception e) {
			return -1;
		}
	}

	public int updateNewStatus(String mi_idx) {
		return deliveryDAO.updateNewStatus(mi_idx);
	}
	/**배송관리 조회*/
	public List<Map<String,Object>> selectCarryList(Map<String,Object> map){
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";
		if (order.equals("mi_addr")) {
		
		}else if (commonDAO.checkColumn(order) == 0) {
			return null;
		}
		map.put("column", order);

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		return deliveryDAO.selectCarryList(map);
	}
	/**배송관리 조회 dt cnt*/
	public int countCarryList(Map<String,Object> map){
		return deliveryDAO.countCarryList(map);
	}
	
	
	/*ds_idx로 ds history조회(반품 등등.. 인 경우 배송완료 정보)**/
	public Map<String,Object> selectDsHistory(String ds_idx){
		return deliveryDAO.selectDsHistory(ds_idx);
	}
	
	/**송장관리 엑셀 업로드*/
	public String carryExcel(File f) {
		String fname=f.getName();
		fname=fname.substring(fname.indexOf(".")+1);
		Workbook wb;
		InputStream is=null;
		String errMsg="";
		try {
			is=new FileInputStream(f);
			if(fname.equals("xls")) {
				wb=new HSSFWorkbook(is);
			}else if(fname.equals("xlsx")) {
				wb=new XSSFWorkbook(is);
			}else {
				return "엑셀 파일을 업로드해주세요.";
			}
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Sheet sh=wb.getSheetAt(0);
			//엑셀 행반복
			for(int i=1;i<sh.getPhysicalNumberOfRows();i++) {
				errMsg=i+"행 ";
				Row row=sh.getRow(i);
				//엑셀 한 행의 데이터 가져오기. 날짜 타입 날짜로
				String ds_state=row.getCell(0).toString();//구분
				String m_num=row.getCell(1).toString();//고객회원번호
				String ds_company=row.getCell(2).toString();//택배사
				String ds_num=row.getCell(3).toString();//송장번호
				String ds_date="";
				String m_contract_date="";
				
				Map<String,Object> member=memberDAO.selectMemberDetail(m_num);
				if(member==null||member.isEmpty()) {
					return m_num+" 고객번호 오류";
				}
				
				try{
					ds_date=sdf.format(row.getCell(4).getDateCellValue());//배송완료일
					
					m_contract_date=ds_date;
					
					String m_jubsoo=member.get("m_jubsoo")+"";
					//무료체험일 경우 계약완료일은 배송완료+14일
					if(m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)){
						Calendar now=Calendar.getInstance();
						now.setTime(sdf.parse(ds_date));
						now.add(Calendar.DATE, 14);
						m_contract_date=sdf.format(now.getTime());
					}
				}catch (Exception ee) {}
				
				String ds_serial=row.getCell(5).toString();//시리얼번호
				
				int m_idx=Integer.parseInt(member.get("m_idx")+"");
				List<Map<String,Object>> dslist=deliveryDAO.selectDeliveryState(m_idx);
				if(dslist==null||dslist.isEmpty()){
					return m_num+" 고객 배송정보 오류";
				}
				
				Map<String,Object> map=new HashMap<String, Object>();
				Map<String,Object> memberUp=new HashMap<String, Object>();
				memberUp.put("m_idx", m_idx);
				int ds_idx=Integer.parseInt(dslist.get(0).get("ds_idx")+"");
				
				map.put("ds_idx", ds_idx);
				//구분에 배송 글자 있으면 배송, 없으면 회수,반품 등 업데이트
				if(ds_state.contains("배송")) {
					if(!(ds_company==null||ds_company.equals(""))) {
						map.put("ds_company", ds_company);
					}
					if(!(ds_num==null||ds_num.equals(""))) {
						map.put("ds_num", ds_num);
					}
					
					if(!(ds_date==null||ds_date.equals(""))) {
						map.put("ds_date", ds_date);
						map.put("ds_state", Delivery.STATE_DELIVERED);
						memberUp.put("m_contract_date", m_contract_date);
						
					}else {
						memberUp.put("m_contract_date", "");
					}
					//계약일 업로드(배송완료일 없는 경우 계약일도 공란)
					memberDAO.updateMember(memberUp);
					
					if(!(ds_serial==null||ds_serial.equals(""))) {
						map.put("ds_serial", ds_serial);
					}
				}else {
					if(!(ds_company==null||ds_company.equals(""))) {
						map.put("ds_company_cancel", ds_company);
					}
					if(!(ds_num==null||ds_num.equals(""))) {
						map.put("ds_num_cancel", ds_num);
					}
					if(!(ds_date==null||ds_date.equals(""))) {
						map.put("ds_date_cancel", ds_date);
						if(ds_state.contains("해지")) {
							map.put("ds_state", Delivery.STATE_HAEBAN);
						}else if(ds_state.contains("반품")) {
							map.put("ds_state", Delivery.STATE_RETURN);
						}else if(ds_state.contains("회수")) {
							map.put("ds_state", Delivery.STATE_EXIT);
						}
					}
				}
				deliveryDAO.updateDeliveryState(map);
				memberDAO.updateMemberProcess(m_idx);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return errMsg+"데이터 업로드 중 오류 발생.";
		}finally {
			try{
				if(is!=null)is.close();
			}catch (Exception e) {}
		}
		return "엑셀 업로드 완료.";
	}
	

	public List<HashMap<String, Object>> selectds_state() {
		return deliveryDAO.selectds_state();
	}

	/**어플에서 방문 후 설치요청취소*/
	@Transactional
	public String cancelInstallReq(int m_idx) {
		String msg="";
		try {
			List<Map<String,Object>> dsList=deliveryDAO.selectDeliveryState(m_idx);
			Map<String,Object> ds=dsList.get(0);
			if(!ds.get("ds_date").equals("")) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return "설치완료일이 없는 경우에만 설치요청 취소 가능합니다.";
			}
			
			Map<String,Object> member=new HashMap<String, Object>();
			member.put("m_idx", m_idx);
			member.put("m_state", MemberInfo.STATE_CANCEL);
			member.put("m_process", MemberInfo.PROCESS_VISITED);
			memberDAO.updateMember(member);
			
			
			int ds_idx=Integer.parseInt(ds.get("ds_idx")+"");
			member.put("ds_idx", ds_idx);
			member.put("ds_state", Delivery.STATE_BEFORE_INSTALL);
			
			deliveryDAO.updateDeliveryState(member);
			msg="취소완료";
		}catch (Exception e) {
			e.printStackTrace();
			msg="오류 발생.";
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg;
	}
}
