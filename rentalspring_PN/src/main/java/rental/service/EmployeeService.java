package rental.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.CustomerFile;
import rental.dao.CommonDAO;
import rental.dao.EmployeeDAO;

@Service("employeeService")
public class EmployeeService {

	@Autowired
	EmployeeDAO employeeDAO;
	@Autowired
	CommonDAO commonDAO;

	/**
	 * <b>e_logid로 회원정보 가져오기</b><br>
	 * 아이디 없는 경우 null return;<br>
	 * 리턴되는 맵에서 key 값 logincheck로 아래 value<br>
	 * pw null혹은 일치하지 않는 경우:no<br>
	 * pw 일치:yes<br>
	 */
	public Map<String, Object> login(Map<String, Object> map) {
		Map<String, Object> rmap = null;
		try {
			rmap = employeeDAO.login(map.get("e_logid") + "");
			if (rmap != null) {
				String e_pass = map.get("e_pass") + "";
				String e_pass_hash=employeeDAO.selectSha2(e_pass);

				if (rmap.get("e_pass").equals(e_pass_hash)) {
					rmap.put("logincheck", "yes");
				} else {
					rmap.put("logincheck", "no");
				}
			}
		} catch (Exception e) {
			rental.common.Log.warn("employeeService login method : " + e.toString());
		}
		return rmap;
	}

	/** 권한 조회 */
	public String getAuth(String e_logid) {
		Map<String, Object> rmap = employeeDAO.login(e_logid);
		String auth = "";
		if (rmap != null) {
			auth = rmap.get("e_power") + "";
		}
		return auth;
	}

	/** 부서로 employee정보 가져오기. null인경우 모든 정보 조회 */
	public List<Map<String, Object>> selectEmployeeListByDepartment(String e_department) {
		return employeeDAO.selectEmployeeListByDepartment(e_department);
	}

	/**
	 * e_id로 employee정보 가져오기<br>
	 * e_id,e_name,e_branch,e_department,e_position,e_power
	 */
	public Map<String, Object> selectEmployeeDetail(String e_id) {
		try {
			int i_e_id = Integer.parseInt(e_id);
			return employeeDAO.selectEmployeeDetail(i_e_id);
		} catch (Exception e) {
			Log.warn(e.toString() + " e_id로 정보조회 실패" + e_id);
			return null;
		}
	}

	@Transactional
	public int insertTechnician(Map<String, Object> map) {
		int result = 0;
		map.put("t_creater", map.get("e_name"));
		String t_pass = "";
		String t_logid = "";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.reset();
			t_logid = map.get("t_logid") + "";
			md.update(t_logid.getBytes("UTF-8"));
			t_pass = String.format("%040x", new BigInteger(1, md.digest()));
			map.put("t_pass", t_pass);
			
			result = employeeDAO.insertTechnician(map);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Map<String, Object>> selecttechList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";
		
		if (commonDAO.checkColumn(order) == 0) {
			return null;
		} else {
			map.put("column", order);
		}

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}

		return employeeDAO.selectTechList(map);
	}

	public Object countTechList(Map<String, Object> map, int techAuth) {
		return employeeDAO.countTechList(map, techAuth);
	}

	public Map<String, Object> selectTechnicianDetail(int eid) {
		return employeeDAO.selectTechnicianDetail(eid);
	}

	/**
	 * 기사리스트
	 */
	public List<Map<String, Object>> selecttechnicianList(Map<String, Object> map) {
		return employeeDAO.selecttechnicianList(map);
	}

	/** 부서 리스트 */
	public List<String> selectDeptList() {
		return employeeDAO.selectDeptList();
	}

	public List<HashMap<String, Object>> selectCity() {
		return employeeDAO.selectCity();
	}

	public List<HashMap<String, Object>> selectDistrict(String province) {
		return employeeDAO.selectDistrict(province);
	}

	@Transactional
	public void updateTechnicianInfo(Map<String, Object> map) {
		
		try {
			Map<String,Object> hmap = employeeDAO.selectTechnicianDetail(Integer.parseInt(map.get("t_idx")+""));
			hmap.put("t_modify_memo", map.get("t_modify_memo"));
			hmap.put("modifier", ((HashMap)map.get("login")).get("e_name"));
			employeeDAO.insertTechnicianHistory(hmap);
			employeeDAO.updateTechnicianInfo(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**고객사 로그인. 로그인 id로 정보 조회*/
	public Map<String, Object> customerLogin(String clogin_id) {
		return employeeDAO.customerLogin(clogin_id);
	}

	/**임직원관리 조회 dataTable용*/
	public List<Map<String,Object>> selectEmployeeList(Map<String,Object> map){
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		return employeeDAO.selectEmployeeList(map);
	}
	
	/**임직원관리 조회 dataTable용 count*/
	public int countEmployeeList(){
		return employeeDAO.countEmployeeList();
	}
	
	/**직원등록시 e_number중복확인*/
	public int checkId(String e_number){
		return employeeDAO.checkId(e_number);
	}
	
	/**insert emp*/
	public void insertEmployee(Map<String,Object> map) {
		employeeDAO.insertEmployee(map);
	}
	
	/**update emp*/
	@Transactional
	public void updateEmployee(Map<String,Object> map) {
		try {
			Map<String,Object> emap = employeeDAO.selectEmployeeInfoDetail(map.get("e_id")+"");
			emap.put("modifier", ((HashMap)map.get("login")).get("e_id"));
			emap.put("e_modify_memo", map.get("e_modify_memo"));
			employeeDAO.insertEmployeeHistory(emap);
			employeeDAO.updateEmployee(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**각 임직원 상세페이지 employee정보 전부 가져오기 By e_id */
	public Map<String, Object> selectEmployeeInfoDetail(String e_id) {
		return employeeDAO.selectEmployeeInfoDetail(e_id);
	}

	
	/**거래처관리 DT 리스트 */
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";
		if (commonDAO.checkColumn(order) == 0) {
			return null;
		} else {
			map.put("column", order);
		}

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		
		return employeeDAO.selectCustomerList(map);
	}

	/**거래처관리 DT 리스트 갯수*/
	public int countCustomerList() {
		return employeeDAO.countCustomerList();
	}

	/**거래처 추가*/
	@Transactional
	public String insertCustomer(Map<String, Object> map,MultipartFile c_file,MultipartFile c_bankbook) {
		try {
			String regisete_path=CustomerFile.PATH+CustomerFile.FILE_REGISTRATION;
			String bankbook_path=CustomerFile.PATH+CustomerFile.FILE_BANKBOOK;
			
			int c_id = employeeDAO.insertCustomer(map);
			File f=new File(regisete_path);
			if(!f.exists()) {
				f.mkdirs();
			}
			File f2=new File(bankbook_path);
			if(!f2.exists()) {
				f2.mkdirs();
			}
			
			String c_comname = map.get("c_comname")+"";
			String account = map.get("c_account")+"";
			account = account.replaceAll("!\"#[$]%&\\(\\)\\{\\}@`[*]:[+];-.<>,\\^~|'\\[\\]", ""); 
			
			String regex = ".*[\\[\\][:]\\\\/?[*]].*";

			if(c_comname.matches(regex)){  // text에 정규식에 있는 문자가 있다면 true 없다면 false 
				//대괄호는 소괄호로
				c_comname = c_comname.replaceAll("\\[", "\\(");
				c_comname = c_comname.replaceAll("\\]", "\\)");
				//나머지 특수문자는 제거
				c_comname = c_comname.replaceAll("[[:]\\\\/?[*]]", "");  
			}
			c_file.transferTo(new File(regisete_path+"/"+c_id+"_"+c_comname));
			c_bankbook.transferTo(new File(bankbook_path+"/"+c_id+"_"+account));
		} catch (Exception e) {
			e.printStackTrace();
			return "오류 발생";
		}
		return "업로드 완료";
	}

	/**Customer>category1 list*/
	public List<HashMap<String, Object>> selectCategory() {
		return employeeDAO.selectCategory();
	}

	/**Customer> c_type리스트 */
	public List<HashMap<String, Object>> selectType() {
		return employeeDAO.selectType();
	}

	/**Customer>c_state리스트*/
	public List<HashMap<String, Object>> selectState() {
		return employeeDAO.selectState();
	}

	public Map<String, Object> selectCustomerInfoDetail(String c_id) {
		return employeeDAO.selectCustomerInfoDetail(c_id);
	}

	@Transactional
	public String updateCustomer(Map<String, Object> map, MultipartFile c_bankbook, MultipartFile c_file) {
		try {
			String regisete_path=CustomerFile.PATH+CustomerFile.FILE_REGISTRATION;
			String bankbook_path=CustomerFile.PATH+CustomerFile.FILE_BANKBOOK;
			
			Map<String,Object> cmap = employeeDAO.selectCustomerInfoDetail(map.get("c_id")+"");
			cmap.put("c_modify_memo", map.get("c_modify_memo"));
			cmap.put("modifier", ((HashMap)map.get("login")).get("e_id"));
			employeeDAO.insertCustomerHistory(cmap);
			employeeDAO.updateCustomer(map);
			File f=new File(regisete_path);
			if(!f.exists()) {
				f.mkdirs();
			}
			File f2=new File(bankbook_path);
			if(!f2.exists()) {
				f2.mkdirs();
			}
			
			int c_id=Integer.parseInt(map.get("c_id")+"");
			String c_comname = map.get("c_comname")+"";
			String account = map.get("c_account")+"";
			String regex = ".*[\\[\\][:]\\\\/?[*]].*";
			if(c_bankbook != null) {
				account = account.replaceAll("!\"#[$]%&\\(\\)\\{\\}@`[*]:[+];-.<>,\\^~|'\\[\\]", ""); 
				c_bankbook.transferTo(new File(bankbook_path+"/"+c_id+"_"+account));
			}
			
			if(c_file != null ) {
				if(c_comname.matches(regex)){  // text에 정규식에 있는 문자가 있다면 true 없다면 false 
					//대괄호는 소괄호로
					c_comname = c_comname.replaceAll("\\[", "\\(");
					c_comname = c_comname.replaceAll("\\]", "\\)");
					//나머지 특수문자는 제거
					c_comname = c_comname.replaceAll("[[:]\\\\/?[*]]", "");  
					c_comname = c_comname.replaceAll("!\"#[$]%&\\(\\)\\{\\}@`[*]:[+];-.<>,\\^~|'\\[\\]", ""); 
				}
				
				c_file.transferTo(new File(regisete_path+"/"+c_id+"_"+c_comname));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return "오류 발생";
		}
		return "업로드 완료";
	}

	/**직급리스트*/
	public List<HashMap<String, Object>> selectPosition() {
		return employeeDAO.selectPosition();
	}
	/**직무리스트*/
	public List<HashMap<String, Object>> selectJob() {
		return employeeDAO.selectJob();
	}
	/**권한리스트*/
	public List<HashMap<String, Object>> selectPower() {
		return employeeDAO.selectPower();
	}
	
	/**소속리스트*/
	public List<HashMap<String, Object>> selectBranch() {
		return employeeDAO.selectBranch();
	}
	
	/**부서리스트*/
	public List<HashMap<String, Object>> selectDepartment() {
		return employeeDAO.selectDepartment();
	}

	/**거래처 변경 리스트*/
	public List<Map<String, Object>> selectCustomerModifyList(Map<String, Object> map) {
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		return employeeDAO.selectCustomerModifyList(map);
	}
	/**선택된 거래처 이력 상세보기*/
	public Map<String, Object> selectCustomerHistory(String ch_idx) {
		return employeeDAO.selectCustomerHistory(ch_idx);
	}
	/**선택된 거래처 이력 이전 수정버전*/
	public Map<String, Object> selectCustomerPrvHistory(String ch_idx, String c_id) {
		Map<String, Object> imap = new HashMap<String, Object>();
		imap.put("ch_idx", ch_idx);
		imap.put("c_id", c_id);
		return employeeDAO.selectCustomerPrvHistory(imap);
	}

	/**임직원 변경이력리스트*/
	public List<HashMap<String, Object>> selectEmployeeModifyList(Map<String, Object> map) {
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		
		return employeeDAO.selectEmployeeModifyList(map);
	}

	/**임직원 선택된 변경이력 상세보기 */
	public Map<String, Object> selectEmployeeHistoryInfoDetail(String eh_idx) {
		return employeeDAO.selectEmployeeHistoryInfoDetail(eh_idx);
	}
	/**임직원 선택된 변경이력 이전버전 데이터 */
	public Map<String, Object> selectEmployeeHistoryPrvDetail(String e_id, String eh_idx) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("e_id", e_id);
		map.put("eh_idx", eh_idx);
		return employeeDAO.selectEmployeeHistoryPrvDetail(map);
	}
	/**임직원 개별 이력관리 수량*/
	public Object selectEmployeeModifyListCount(String e_id) {
		return employeeDAO.selectEmployeeModifyListCount(e_id);
	}

	/**거래처 개별 이력관리 수량*/
	public Object selectCustomerModifyListCount(String c_id) {
		return employeeDAO.selectCustomerModifyListCount(c_id);
	}

	/**기사 수정내용 이력관리 리스트*/
	public List<Map<String, Object>> selectTechnicianModifyList(Map<String, Object> map) {
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		
		return employeeDAO.selectTechnicianModifyList(map);
	}
	/**기사 수정내용 이력관리 리스트 갯수*/
	public List<Map<String, Object>> selectTechnicianModifyListCount(String t_id) {
		return employeeDAO.selectTechnicianModifyListCount(t_id);
	}

}
