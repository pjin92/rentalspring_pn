package rental.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import rental.dao.MenuDAO;

@Service("menuService")
public class MenuService {
	@Autowired
	MenuDAO menuDAO;
	
	/**권한에 따른 menu 리스트 리턴<br>
	 * key: m=메인메뉴 리스트,m_idx=submenu리스트<br>
	 * menu,submenu map에 view위한 class 키밸류 active=active 넣음 
	 * */
	public Map<String, List<Map<String, Object>>> menuList(String e_power,String uri){
		if(uri==null||uri.equals("")) {
			uri="nullValue";
		}
		
		Map<String, List<Map<String, Object>>> returnMap=new HashMap<String, List<Map<String, Object>>>();
		
		List<Map<String, Object>> mList=new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> list=menuDAO.selectMenuList();
		for(int i=0;i<list.size();i++) {
			Map<String, Object>mmap=list.get(i);
			String m_auth=mmap.get("m_auth")+"";
			String m_uri=mmap.get("m_uri")+"";
			if(m_uri.equals(uri)) {
				mmap.put("active", "active");
			}
			
			if(m_auth.contains(","+e_power+",")){
				int m_idx=Integer.parseInt(mmap.get("m_idx")+"");
				List<Map<String, Object>> sm=menuDAO.selectSubmenuList(m_idx);
				if(sm!=null&&sm.size()>0) {
					List<Map<String, Object>> smList=new ArrayList<Map<String, Object>>();
					for(int j=0;j<sm.size();j++) {
						Map<String, Object>smmap=sm.get(j);
						String sm_auth=smmap.get("sm_auth")+"";
						
						String sm_uri=smmap.get("sm_uri")+"";
						if(sm_uri.equals(uri)) {
							mmap.put("active", "active");
							smmap.put("active", "active");
						}
						if(sm_auth.contains(","+e_power+",")){
							smList.add(smmap);
						}
					}
					if(smList.size()>0) {
						returnMap.put(m_idx+"", smList);
					}
				}
				mList.add(mmap);
			}
		}
		returnMap.put("m", mList);
		return returnMap;
	}
	
}
