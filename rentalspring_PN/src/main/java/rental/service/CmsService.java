package rental.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Base64.Encoder;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import comm_module.BaseAction;
import comm_module.XC_JavaSocket;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import rental.common.AesEncrypt;
import rental.common.CardHV;
import rental.common.Log;
import rental.constant.Cms;
import rental.constant.MemberFile;
import rental.constant.MemberInfo;
import rental.constant.Pay;
import rental.constant.Sms;
import rental.constant.Vaccount;
import rental.dao.CmsDAO;
import rental.dao.CommonDAO;
import rental.dao.MemberDAO;
import rental.dao.PayDAO;
import rental.dao.ProductDAO;
import rental.dao.VaccountDAO;

@Service("cmsService")
public class CmsService {
	
	@Autowired
	CmsDAO cmsDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	PayDAO payDAO;
	@Autowired
	ProductDAO productDAO;
	@Autowired
	MemberDAO memberDAO;
	@Autowired
	VaccountDAO vaccountDAO;
	@Autowired
	SmsService smsService;
	
	/**sdsi 효성cms 정보*/
	final private String PROGRAM_ID="gnrent99";
	/**sdsi 효성cms 정보*/
	final private String PROGRAM_PW="99gnrent";
	/**cms card API van key*/
	final private String VAN="zA3CAPLBtAlcW06h";
	
	/** cms list 조회<br>
	 * DataTabl에서 전달받은 파라미터를 매개변수로.null값도 가능*/
	public List<Map<String,Object>> selectCmsList(Map<String,Object> map){
		if(map!=null) {
			Object oc=map.get("order[0][column]");
			String order=map.get("columns["+oc+"][data]")+"";
			String asc=map.get("order[0][dir]")+"";
			if(commonDAO.checkColumn(order)==0) {
				return null;
			}
			map.put("column", order);
			
			if(asc.equals("asc")) {
				map.put("order", "asc");
			}else {
				map.put("order", "desc");
			}
		}
		return cmsDAO.selectCmsList(map);
	}
	
	public int countCms() {
		return cmsDAO.countCms();
	}
	
	public int insertCms(Map<String,Object>map) {
		return cmsDAO.insertCms(map);
	}
	
	/**cms_real insert 또는 update*/
	public void insertCmsReal(int m_idx,int cms_idx,int cr_type) {
		Map<String,Object> info=cmsDAO.selectCmsByMidx(m_idx);
		
		if(info!=null&&!info.get("m_cms").equals(MemberInfo.CMS_UNREGISTERD)) {
			return;
		}
		//cms_real에 insert
		Map<String,Object> cmsRealMap=new HashMap<String, Object>();
		cmsRealMap.put("m_idx", m_idx);
		cmsRealMap.put("cms_idx", cms_idx);
		cmsRealMap.put("cr_type", cr_type);
		if(cmsDAO.updateCmsReal(cmsRealMap)<1) {
			cmsDAO.insertCmsReal(cmsRealMap);
		}
	}
	
	/**회원 신청하기<br>
	 * type>0 신청, type==0 신청취소*/
	synchronized public String regiCms(String ps_date,int type) {
		boolean regi=true;
		if(type<1) {
			regi=false;
		}
		
		String msg="";
		if(!isRegTime(ps_date,Pay.SEND_REGI_TIME)) {
			return "신청가능한 일자 아닙니다.";
		}
		//cms_idx별 전문
		HashMap<String, List<String>> cms_loopDatas=new HashMap<String, List<String>>();
		//전문작성
		List<Map<String,Object>> psList=payDAO.selectPaySendByDate(null, ps_date, "회원신청");
		//신청실패한 mnum,reason담는 map
		Map<String, String>mnum_reason=new HashMap<String, String>();
		
		if(regi) {
			for(int i=0;i<psList.size();i++) {
				Map<String,Object> ps=psList.get(i);
				ps.remove("mf_name");
				ps.remove("mf_idx");
				ps.remove("mf_memo");
				
				String loopData="D";
				loopData+=writeData((String)ps.get("cms_id"),10);
				String m_num=(String)ps.get("m_num");
				if(m_num==null) {
					Log.warn(ps_date+"/"+(String)ps.get("ps_idx")+"=ps_idx/m_num null");
					continue;
				}
				Map<String,Object> member=memberDAO.selectMemberDetail(m_num);
				if(member==null){
					Log.warn(ps_date+"/"+m_num+"/member data null");
					continue;
				}
				
				String ps_pay_idx=ps.get("ps_pay_idx")+"";//sql에서 등록,수정으로 바꿔서 가져옴
				if(ps_pay_idx.equals("등록")) {
					loopData+="N";
				}else if(ps_pay_idx.equalsIgnoreCase("수정")){
					loopData+="U";
				}else {
					Log.warn(ps_date+"/"+ps_pay_idx+"/ps_pay_idx 등록,수정이 아닌 값");
					mnum_reason.put(m_num, "등록,수정요청이 아닙니다.");
					continue;
				}
				loopData+=writeData(m_num,20);
				loopData+=writeData((String)ps.get("ps_owner"),20);
				loopData=spaceData(loopData, 72);
				String sb_code=(String)ps.get("sb_code");
				if(sb_code==null) {
					Log.warn(ps_date+"/"+sb_code+"/sb_code null");
					mnum_reason.put(m_num, "은행 확인 부탁드립니다.");
					continue;
				}
				loopData+=writeData(sb_code,2);;
				loopData+=writeData((String)ps.get("ps_pay_num"),15);
				loopData+=writeData((String)ps.get("ps_owner"),20);
				
				String m_id_type=member.get("m_id_type")+"";
				String ps_id=ps.get("ps_id")+"";
				ps_id=ps_id.replaceAll("[^0-9]", "");
				if(m_id_type.equals(MemberInfo.ID_TYPE_PERSON)) {
					ps_id=ps_id.substring(0, 6);//개인
				}else {
					ps_id=ps_id.substring(0, 10);//법인
				}
				loopData+=writeData(ps_id,13);
				loopData=spaceData(loopData, 298);
				loopData+="\r\n";
					
				String cms_id=(String)ps.get("cms_id");
				List<String> loops=cms_loopDatas.get(cms_id);
				if(loops==null) {
					loops=new ArrayList<String>();
					cms_loopDatas.put(cms_id, loops);
				}
				loops.add(loopData);
			}
		}else {
			List<Map<String,Object>> cmsList=cmsDAO.selectCmsList(null);
			for(int i=0;i<cmsList.size();i++) {
				cms_loopDatas.put(cmsList.get(i).get("cms_id")+"", null);
			}
		}

		//db에 값 저장하기 위한 에러여부 체크,에러시 value -1,성공시 value에 신청건수
		Map<String, Integer>cmsSuccess=new HashMap<String, Integer>();
		for(String cms_id:cms_loopDatas.keySet()) {
			BaseAction con=null;
			try {
				List<String> loopData=cms_loopDatas.get(cms_id);
				AesEncrypt ae=new AesEncrypt();
				String cms_pw=cmsDAO.selectCmsDetail(cms_id).get("cms_pw")+"";
				cms_pw=ae.decrypt(cms_pw);
				con=new XC_JavaSocket();
				
				//연결
				if(con.connect("121.134.74.90", 21000)<0){
					cmsSuccess.put(cms_id, -1);
					Log.warn(ps_date+"/"+cms_id+"/회원신청 연결실패");
					continue;
				}
				//시작전문
				String start="S";
				start+=writeData(cms_id, 10);
				start+=writeData(cms_pw, 10);
				start+=writeData(PROGRAM_ID, 10);
				start+=writeData(PROGRAM_PW, 10);
				start+=writeData("MEM", 3);
				start+=writeData("A", 1);//신청A 결과 R 신청내역조회 S
				start+=writeData(ps_date.replaceAll("[^0-9]", ""), 8);
				start=spaceData(start,298);
				start+="\r\n";
				con.sendData(start);
				byte[] recStart=con.recvData();
				if(recStart[243]=='N') {
					cmsSuccess.put(cms_id, -1);
					Log.warn(ps_date+"/"+cms_id+"/회원신청 recStart="+new String(recStart, 244, 34,"utf-8"));
					con.close();
					continue;
				}
				
				String header="H";
				header+=writeData(cms_id, 10);
				//data 신청 건수
				if(regi) {
					header+=writeData(loopData.size()+"", 6);
				}else {
					header+=writeData("0", 6);
				}
				header=spaceData(header, 298);
				header+="\r\n";
				con.sendData(header);
				
				if(regi) {
					for(int i=0;i<loopData.size();i++) {
						con.sendData(loopData.get(i));
					}
				}
				
				String tail="T";
				tail+=writeData(cms_id, 10);
				tail=spaceData(tail, 298);
				tail+="\r\n";
				con.sendData(tail);
				
				byte[] recHeader=con.recvData();
				if(recHeader[243]=='N') {
					cmsSuccess.put(cms_id, -1);
					con.close();
					Log.warn(ps_date+"/"+cms_id+"/회원신청 recHeader="+new String(recHeader, 244, 34,"utf-8"));
					continue;
				}
				
				byte[] recTail=null;
				boolean b_error=true;
				while(b_error){
					byte[] errorLoop=con.recvData();
					if(errorLoop[0]=='T') {
						recTail=errorLoop;
						b_error=false;
					}else {
						String error_mnum=new String(errorLoop,12,20);
						error_mnum=error_mnum.trim();
						String reason=new String(errorLoop,183,34,"EUC-KR");
						reason=reason.trim();
						mnum_reason.put(error_mnum, reason);
					}
				}
				
				if(recTail.length>280) {
					if(recTail[243]=='N') {
						cmsSuccess.put(cms_id, -1);
						Log.warn(ps_date+"/"+cms_id+"/회원신청 recTail="+new String(recTail, 244, 34,"EUC_KR"));
					}
				}else {
					cmsSuccess.put(cms_id, -1);
					Log.warn(ps_date+"/"+cms_id+"/회원신청 recTail 길이 짧음"+new String(recTail,"EUC_KR").trim());
				}
				byte[] recEnd=con.recvData();
				if(con.close()==1) {
					if(regi) {
						if(!cmsSuccess.containsKey(cms_id)) {
							cmsSuccess.put(cms_id, loopData.size());
						}
					}else {
						cmsSuccess.put(cms_id, 0);
					}
				}
			}catch (Exception e) {
				cmsSuccess.put(cms_id, -1);
				e.printStackTrace();
			}finally {
				if(con!=null)con.close();
			}
		}
		boolean upDb=true;
		//db update
		for(int i=0;i<psList.size();i++) {
			Map<String,Object> ps=psList.get(i);
			int m_idx=Integer.parseInt(ps.get("ps_m_idx")+"");
			ps.put("m_idx", m_idx);
			String ps_pay_idx=ps.get("ps_pay_idx")+"";//sql에서 등록,수정으로 바꿔서 가져옴
			if(regi) {
				String cms_id=ps.get("cms_id")+"";
				if(cmsSuccess.get(cms_id)>0) {
					String m_num=ps.get("m_num")+"";
					ps.put("ps_state", Pay.SEND_STATE_SEND);
					if(mnum_reason.containsKey(m_num)) {
						ps.put("ps_reason", mnum_reason.get(m_num)+"");
					}else {
						ps.put("ps_reason", "");
						
						if(ps_pay_idx.equals("등록")) {
							String m_cms_original=memberDAO.selectMemberDetail(m_idx).get("m_cms")+"";//현재 db에 있는 값
							if(!m_cms_original.equals(MemberInfo.CMS_REG_HASFILE)) {
								//파일이미 등록한 경우는 m_stat변경 안함
								ps.put("m_cms", MemberInfo.CMS_REG_NEEDFILE);
							}
						}else if(ps_pay_idx.equalsIgnoreCase("수정")){
							ps.put("m_cms", MemberInfo.CMS_CHANGE_ING);
						}
						
					}
					
				}else {
					ps.put("ps_state", Pay.SEND_STATE_ERROR);
					ps.put("ps_reason", "신청오류");
				}
				
			}else {
				//신청취소
				ps.put("ps_state", Pay.SEND_STATE_BEFORE);
				//ps.put("ps_reason", "");
				
				if(ps_pay_idx.equals("등록")) {
					ps.put("m_cms", MemberInfo.CMS_UNREGISTERD);
				}else if(ps_pay_idx.equalsIgnoreCase("수정")){
					ps.put("m_cms", MemberInfo.CMS_CHANGE);
				}
			}
			int result=payDAO.updatePaySend(ps);
			if(ps.containsKey("m_cms")) {
				memberDAO.updateMemberCms(ps);
			}
			
			if(result<1) {
				upDb=false;
			}
		}
		
		if(upDb) {
			msg="처리하였습니다.";
		}else {
			msg="DB 업데이트 실패. 신청내역 조회 부탁드립니다.";
		}
		
		return msg;
	}
	
	/**은행회원,출금 신청 조회. sdsi_data는 CMS.SDSI_DATA 상수*/
	public String regiCmsRead(String ps_date,String sdsi_data) {
		String msg="";
		Map<String,String> cms=new HashMap<String, String>();
		List<Map<String,Object>> cmsList=cmsDAO.selectCmsList(null);
		for(int i=0;i<cmsList.size();i++) {
			Map<String,Object> cmsMap=cmsList.get(i);
			String cms_id=(String)cmsMap.get("cms_id");
			cms.put(cms_id, "0");
		}
	
		for(String cms_id:cms.keySet()) {
			BaseAction con=null;
			try {
				AesEncrypt ae=new AesEncrypt();
				String cms_pw=cmsDAO.selectCmsDetail(cms_id).get("cms_pw")+"";
				cms_pw=ae.decrypt(cms_pw);
				con=new XC_JavaSocket();
				
				//연결
				if(con.connect("121.134.74.90", 21000)<0){
					Log.warn(ps_date+"/"+cms_id+"/회원 "+sdsi_data+" 조회 연결실패");
					continue;
				}
				//시작전문
				String start="S";
				start+=writeData(cms_id, 10);
				start+=writeData(cms_pw, 10);
				start+=writeData(PROGRAM_ID, 10);
				start+=writeData(PROGRAM_PW, 10);
				start+=writeData(sdsi_data, 3);
				start+=writeData("S", 1);//신청A 결과 R 신청내역조회 S
				start+=writeData(ps_date.replaceAll("[^0-9]", ""), 8);
				start=spaceData(start,298);
				start+="\r\n";
				con.sendData(start);
				byte[] recStart=con.recvData();
				if(recStart[243]=='N') {
					String errorCode=new String(recStart, 244, 34,"EUC-KR");
					errorCode=errorCode.trim();
					if(!errorCode.equals("S019데이터없음")){
						Log.warn(ps_date+"/"+cms_id+"/회원 "+sdsi_data+" 조회 recStart="+errorCode);
					}
					con.close();
					continue;
				}
				
				byte[] recTail=null;
				boolean b_tail=true;
				while(b_tail){
					byte[] errorLoop=con.recvData();
					if(errorLoop[0]=='T') {
						recTail=errorLoop;
						b_tail=false;
					}
					//System.out.println("rec=="+new String(errorLoop,"EUC-KR"));
				}
				
				String sentData=new String(recTail, 11, 6,"EUC-KR");
				cms.put(cms_id, sentData.trim());
				if(recTail.length>280) {
					if(recTail[243]=='N') {
						Log.warn(ps_date+"/"+cms_id+"/회원"+sdsi_data+"조회 recTail="+new String(recTail, 244, 34,"EUC-KR"));
					}
				}else {
					Log.warn(ps_date+"/"+cms_id+"/회원"+sdsi_data+"조회 recTail 길이 짧음"+new String(recTail,"EUC-KR").trim());
				}
				byte[] recEnd=con.recvData();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				if(con!=null)con.close();
			}
		}
		
		int totalCount=0;
		try{
			for(String cms_id:cms.keySet()) {
				int num=Integer.parseInt(cms.get(cms_id));
				if(num>0) {
					msg+=cms_id+":"+num+"건\n";
					totalCount+=num;
				}
			}
		}
		catch (Exception e) {}
		
		if(msg.equals("")) {
			msg="신청 내역이 없습니다.";
		}else {
			msg+="총 "+totalCount+"건";
		}
		return msg;
	}

	/**은행회원,출금 결과 조회. sdsi_data는 CMS.SDSI_DATA 상수*/
	@Transactional(isolation=Isolation.SERIALIZABLE)
	public String cmsResult(String ps_date,String sdsi_data) {
		String msg="";
		Map<String,String> cms=new HashMap<String, String>();
		List<Map<String,Object>> cmsList=cmsDAO.selectCmsList(null);
		for(int i=0;i<cmsList.size();i++) {
			Map<String,Object> cmsMap=cmsList.get(i);
			String cms_id=(String)cmsMap.get("cms_id");
			cms.put(cms_id, "0");
		}
		Map<String,String> mnum_reason=new HashMap<String, String>();
		Map<String,Integer> mnum_reason_count=new HashMap<String, Integer>();
		Map<String,String> cms_fail=new HashMap<String, String>();
		for(String cms_id:cms.keySet()) {
			BaseAction con=null;
			try {
				AesEncrypt ae=new AesEncrypt();
				String cms_pw=cmsDAO.selectCmsDetail(cms_id).get("cms_pw")+"";
				cms_pw=ae.decrypt(cms_pw);
				con=new XC_JavaSocket();
				
				//연결
				if(con.connect("121.134.74.90", 21000)<0){
					Log.warn(ps_date+"/"+cms_id+"/ "+sdsi_data+" 결과요청 연결실패");
					continue;
				}
				//시작전문
				String start="S";
				start+=writeData(cms_id, 10);
				start+=writeData(cms_pw, 10);
				start+=writeData(PROGRAM_ID, 10);
				start+=writeData(PROGRAM_PW, 10);
				start+=writeData(sdsi_data, 3);
				start+=writeData("R", 1);//신청A 결과 R 신청내역조회 S
				start+=writeData(ps_date.replaceAll("[^0-9]", ""),8);
				start=spaceData(start,298);
				start+="\r\n";
				con.sendData(start);
				byte[] recStart=con.recvData();
				if(recStart[243]=='N') {
					String errorCode=new String(recStart, 244, 34,"EUC-KR");
					errorCode=errorCode.trim();
					
					if(errorCode.indexOf("S007")!=-1){
						return errorCode;
					}
					if(!errorCode.equals("S019데이터없음")){
						Log.warn(ps_date+"/"+cms_id+"/ "+sdsi_data+" 결과요청 recStart="+errorCode);
					}
					cms_fail.put(cms_id, errorCode);
					con.close();
					continue;
				}
				
				byte[] recTail=null;
				boolean b_tail=true;
				while(b_tail){
					byte[] errorLoop=con.recvData();
					if(errorLoop[0]=='H') {
						
					}else if(errorLoop[0]=='T') {
						recTail=errorLoop;
						b_tail=false;
					}else {
						//System.out.println("rec=="+new String(errorLoop,"EUC-KR"));
						String m_num=new String(errorLoop,12,20,"EUC-KR");
						String reason="";
						if(sdsi_data.equals(Cms.SDSI_DATA_MEM)) {
							reason=new String(errorLoop,183, 34,"EUC-KR");
						}else if(sdsi_data.equals(Cms.SDSI_DATA_PAY)) {
							reason=new String(errorLoop,244, 34,"EUC-KR");
						}
						m_num=m_num.trim();
						mnum_reason.put(m_num, reason.trim());
						if(mnum_reason_count.containsKey(m_num)) {
							int cnt=mnum_reason_count.get(m_num);
							mnum_reason_count.put(m_num, cnt+1);
						}else {
							mnum_reason_count.put(m_num, 1);
						}
					}
				}
				String sentData=new String(recTail, 11, 6,"EUC-KR");
				if(recTail.length>280) {
					if(recTail[243]=='N') {
						String errorCode=new String(recTail, 244, 34,"EUC-KR");
						Log.warn(ps_date+"/"+cms_id+"/"+sdsi_data+"결과요청 recTail="+errorCode);
					}else {
						cms.put(cms_id, sentData.trim());
					}
				}else {
					Log.warn(ps_date+"/"+cms_id+"/"+sdsi_data+"결과요청 recTail 길이 짧음"+new String(recTail,"EUC-KR").trim());
				}
				byte[] recEnd=con.recvData();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				if(con!=null)con.close();
			}
		}
		
		List<Map<String,Object>> psList;
		if(sdsi_data.equals(Cms.SDSI_DATA_MEM)) {
			psList=payDAO.selectPaySendByDate(null, ps_date, "회원신청");
		}else if(sdsi_data.equals(Cms.SDSI_DATA_PAY)) {
			psList=payDAO.selectPaySendByDate(null, ps_date, null);
		}else {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return "잘못된 접근입니다.";
		}
		
		try{
			for(int ps=0;ps<psList.size();ps++) {
				Map<String,Object> psMap=psList.get(ps);
				String ps_state=psMap.get("ps_state")+"";
				
				
					
				Map<String,Object> updateResult=new HashMap<String, Object>();
				updateResult.put("ps_idx", psMap.get("ps_idx"));
				updateResult.put("ps_state", Pay.SEND_STATE_SUCCESS);
				//회원신청은 신청중일 때만 결과 반영
				if(sdsi_data.equals(Cms.SDSI_DATA_MEM)&&ps_state.equals(Pay.SEND_STATE_SEND)) {
					String m_num=psMap.get("m_num")+"";
					String cms_id=psMap.get("cms_id")+"";
					if(mnum_reason.containsKey(m_num)) {
						updateResult.put("ps_reason", mnum_reason.get(m_num));
						updateResult.put("m_cms_fail", mnum_reason.get(m_num));
						updateResult.put("m_cms", MemberInfo.CMS_UNREGISTERD);
					}else if(cms.get(cms_id).equals("0")){
						updateResult.put("ps_reason", Pay.SEND_STATE_ERROR);
						updateResult.put("m_cms", MemberInfo.CMS_UNREGISTERD);
					}else{
						updateResult.put("ps_reason", "");
						updateResult.put("m_cms_fail", "");
						updateResult.put("m_cms", MemberInfo.CMS_REGISTERD);
					}
					
					payDAO.updatePaySend(updateResult);
					
					String m_cms=psMap.get("m_cms")+"";
					if(!m_cms.equals(MemberInfo.CMS_REGISTERD)) {
						updateResult.put("m_idx", psMap.get("ps_m_idx"));
						memberDAO.updateMemberCms(updateResult);
					}
				}else if(sdsi_data.equals(Cms.SDSI_DATA_PAY)) {
					String m_num=psMap.get("m_num")+"";
					
					int reasonCnt=0;
					if(mnum_reason_count.containsKey(m_num)) {
						reasonCnt=mnum_reason_count.get(m_num);
					}
					
					String cms_id=psMap.get("cms_id")+"";
					boolean pay_add=false;//출금실패로 pay새로 insert하는 경우
					if(mnum_reason.containsKey(m_num)&&reasonCnt>0) {
						updateResult.put("ps_reason", mnum_reason.get(m_num));
						updateResult.put("pay_reason", mnum_reason.get(m_num));
						updateResult.put("pay_state", Pay.STATE_FAIL);
						pay_add=true;
						
						reasonCnt--;
						mnum_reason_count.put(m_num, reasonCnt);
					}else if(cms.get(cms_id).equals("0")){
						updateResult.put("ps_reason", Pay.SEND_STATE_ERROR);
						updateResult.put("pay_reason", cms_fail.get(cms_id));
						updateResult.put("pay_state", Pay.STATE_FAIL);
						pay_add=true;
					}else{
						updateResult.put("ps_reason", "");
						updateResult.put("pay_reason", "");
						updateResult.put("pay_real", psMap.get("ps_money"));
						updateResult.put("pay_state", Pay.STATE_PAID);
					}
					
					payDAO.updatePaySend(updateResult);
					
					int pay_idx=Integer.parseInt(psMap.get("ps_pay_idx")+"");
					Map<String,Object> payOriginal=payDAO.selectPayDetail(pay_idx);
					
					updateResult.put("pay_idx", pay_idx);
					updateResult.put("pay_method", Pay.METHOD_BANK);
					payDAO.updatePay(updateResult);
					
					if(pay_add) {
						payOriginal.put("m_idx", payOriginal.get("pay_m_idx"));
						payOriginal.put("pay_cnt2", Integer.parseInt(payOriginal.get("pay_cnt2")+"")+1);
						payOriginal.put("pay_date", "");
						payOriginal.put("pay_state", Pay.STATE_UNPAID);
						payOriginal.put("pay_carryover_date", "");
						payOriginal.put("pay_reason", "");
						payOriginal.put("pay_bigo", "");
						payOriginal.put("pay_num", "");
						payDAO.insertPay(payOriginal);
					}
				}
				
			}
			msg="결과 반영 완료";
		}catch (Exception e) {
			e.printStackTrace();
			Log.warn("CMS 결과 반영 중 오류 발생"+ps_date+"/"+sdsi_data);
			msg="결과 반영 중 오류 발생.";
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg;
	}
	
	/**효성cms 회원 신청 후 동의자료 업로드(일자별로)*/
	public String sendFile(String ps_date) {
		String msg="";
		List<Map<String,Object>> psList=payDAO.selectPaySendByDate(null, ps_date, "회원신청");
		try {
			AesEncrypt ae=new AesEncrypt();
			Calendar now=Calendar.getInstance();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
			String date=sdf.format(now.getTime());
			
			for(int psList_i=0;psList_i<psList.size();psList_i++) {
				Map<String,Object>map=psList.get(psList_i);
				if(map.get("m_cms").equals(MemberInfo.CMS_REG_NEEDFILE)) {
					File f=new File(MemberFile.PATH+MemberFile.NAME_AGREEMENT_CERTFICATE+"/"+map.get("mf_idx")+"_"+map.get("mf_memo"));
					String originalFileName=map.get("mf_file")+"";//원본파일 확장명
					if(!f.exists()) {
						f=new File(MemberFile.PATH+MemberFile.NAME_RECORD+"/"+map.get("m_num")+".jpg");
						if(!f.exists()) {
							f=new File(MemberFile.PATH+MemberFile.NAME_RECORD+"/"+originalFileName);
						}
					}
					int index=originalFileName.lastIndexOf(".");
					
					String gubun;//증빙구분
					String dataType;//데이터유형
					if(index<1) {
						continue;
					}
					originalFileName=originalFileName.substring(index);
					originalFileName=originalFileName.toLowerCase();
					if(originalFileName.equals(".jpg")||originalFileName.equals(".jpeg")||originalFileName.equals(".png")
							||originalFileName.equals(".gif")||originalFileName.equals(".tif")||originalFileName.equals(".tiff")
							||originalFileName.equals(".pdf")) {
						gubun="01";
					}else if(originalFileName.equals(".mp3")||originalFileName.equals(".wav")||originalFileName.equals(".wma")) {
						gubun="02";
					}else if(originalFileName.equals(".der")) {
						gubun="03";
					}else {
						Log.warn("약정서인증파일. 허용되지 않은 확장명"+map.entrySet());
						continue;
					}
					dataType=originalFileName.substring(1);
					if(f.exists()) {
						BaseAction con=null;
						try {
							//인증자료 base64로 encoding
							Encoder enc=Base64.getEncoder();
							byte[] b_encode=enc.encode(FileUtils.readFileToByteArray(f));
							
							String cms_id=map.get("cms_id")+"";
							String cms_pw=cmsDAO.selectCmsDetail(cms_id).get("cms_pw")+"";
							cms_pw=ae.decrypt(cms_pw);
	
							con = new XC_JavaSocket();
							if(con.connect("106.250.169.67", 25000)<0){
								return "효성 접속 실패";
							}
							String start="S";
							start+=writeData(cms_id, 10);
							start+=writeData(cms_pw, 10);
							start+=writeData(PROGRAM_ID, 10);
							start+=writeData(PROGRAM_PW, 10);
							start+=writeData(date, 8);
							start+="1000"+"100";
							start+="B";
							start+=writeData(map.get("m_num")+"", 20);
							start+=gubun;
							start+=writeData(dataType, 5);
							
							int f_length=(int)f.length();
							String str_len="000000"+f_length;
							str_len=str_len.substring(str_len.length()-6);
							start+=str_len;
							start=spaceData(start, 200);
							start+="\r\n";
							con.sendData(start);
							byte[] recvStart=con.recvData();
							if(recvStart[90]=='N'){
								Log.warn(new String(recvStart,91,34,"EUC-KR"));
								con.close();
								continue;
							}
							int repeat_count=b_encode.length/60000;
							if(b_encode.length%60000!=0){
								repeat_count++;
							}
							for(int i=0;i<repeat_count;i++){
								String send_data="D";
								send_data+=writeData(cms_id,10);
								send_data+="B";
								send_data+=writeData(map.get("m_num")+"", 20);
								if(i!=repeat_count-1){
									send_data+="N";
								}else{
									send_data+="Y";
								}
								String count="0"+(i+1);
								send_data+=count.substring(count.length()-2);
								send_data=spaceData(send_data, 100);
								if(i!=repeat_count-1){
									send_data+=new String(b_encode,60000*i,60000);
								}else{
									send_data+=new String(b_encode,60000*i,(b_encode.length-60000*i));
									send_data=spaceData(send_data, 60100);
								}
								send_data+="\r\n";
								con.sendData(send_data);
							}
							
							byte[] recvEnd=con.recvData();
							System.out.println("resut"+new String(recvEnd,"EUC-kr"));
							if(recvEnd[32]=='Y'){
								try{
									Map<String,Object>memberUp=new HashMap<String, Object>();
									memberUp.put("m_idx", Integer.parseInt(map.get("ps_m_idx")+""));
									memberUp.put("m_cms", MemberInfo.CMS_REG_HASFILE);
									memberDAO.updateMemberCms(memberUp);
								}catch (Exception ee) {
									Log.warn("배치 동의자료(약정서) 등록 db 반영 실패"+ee.toString());
								}
							}else{
								Log.warn(new String(recvEnd,32,35,"EUC-KR"));
							}
							con.close();
						}catch (Exception e) {
							e.printStackTrace();
						}finally {
							try {
								if(con!=null)con.close();
							}catch (Exception e) {}
						}
					}
				}
			}
			msg="파일 전송 완료했습니다.";
		}catch (Exception e) {
			e.printStackTrace();
			msg="오류 발생";
		}
		return msg;
	}
	
	/**출금 신청하기<br>
	 * type>0 신청, type==0 신청취소*/
	@Transactional(isolation=Isolation.SERIALIZABLE)
	synchronized public String payCms(String ps_date,int type) {
		String applyError="";//신청오류
		boolean regi=true;
		if(type<1) {
			regi=false;
		}
		
		String msg="";
		if(!isRegTime(ps_date,Pay.SEND_PAY_TIME)) {
			return "신청가능한 일자가 아닙니다.";
		}
		//cms_idx별 전문
		HashMap<String, List<String>> cms_loopDatas=new HashMap<String, List<String>>();
		//cms_idx별 총신청금액
		HashMap<String, Integer> cms_money=new HashMap<String, Integer>();
		//전문작성
		List<Map<String,Object>> psList=payDAO.selectPaySendByDate(null, ps_date, null);
		//신청실패한 mnum,reason담는 map
		Map<String, List<String>>mnum_reasons=new HashMap<String, List<String>>();
		
		//신청오류 cms_id reason담는 map
		Map<String, String>cms_fail_reasons=new HashMap<String, String>();
		
		if(regi) {
			for(int i=0;i<psList.size();i++) {
				Map<String,Object> ps=psList.get(i);
				ps.remove("mf_name");
				ps.remove("mf_idx");
				ps.remove("mf_memo");
				String cms_id=(String)ps.get("cms_id");
				if(cms_id==null||cms_id.equals("")) {
					Log.warn("은행출금신청 오류 cms_id null ps_idx="+(String)ps.get("ps_idx"));
					continue;
				}
				//전문 담는 List 연번위해 지금 선언. 마지막에 cms_id(key),List(value) map에 담김
				List<String> loops=cms_loopDatas.get(cms_id);
				if(loops==null) {
					loops=new ArrayList<String>();
					cms_loopDatas.put(cms_id, loops);
				}
				
				String loopData="D";
				loopData+=writeData(cms_id,10);
				String m_num=(String)ps.get("m_num");
				if(m_num==null) {
					Log.warn(ps_date+"/"+(String)ps.get("ps_idx")+"=ps_idx/m_num null");
					continue;
				}
				
				loopData+="N";
				loopData+=writeData(m_num,20);
				loopData+=writeData((String)ps.get("ps_owner"),20);
				//연번
				String orderNum="000000"+(loops.size()+1);
				orderNum=orderNum.substring(orderNum.length()-6);
				loopData+=writeData(orderNum,6);
				loopData=spaceData(loopData, 64);
				
				String ps_money=ps.get("ps_money")+"";
				ps_money=ps_money.replaceAll("[^0-9]", "");
				if(ps_money.equals("")||ps_money.equals("0")) {
					Log.warn(ps_date+"/"+(String)ps.get("ps_idx")+"=ps_idx/ps_money==0");
					continue;
				}
				
				//총신청금액 합
				int ps_money_i=0;
				if(cms_money.containsKey(cms_id)){
					ps_money_i=cms_money.get(cms_id);
				}
				ps_money_i+=Integer.parseInt(ps_money);
				cms_money.put(cms_id, ps_money_i);
				
				loopData+=writeData(ps_money,10);
				String m_phone=ps.get("m_phone")+"";
				m_phone=m_phone.replaceAll("[^0-9]", "");
				loopData+=writeData(m_phone,20);
				loopData=spaceData(loopData, 298);
				loopData+="\r\n";
					
				loops.add(loopData);
			}
		}else {
			//신청취소의 경우 모든 cms_id에 list==null로 세팅
			List<Map<String,Object>> cmsList=cmsDAO.selectCmsList(null);
			for(int i=0;i<cmsList.size();i++) {
				cms_loopDatas.put(cmsList.get(i).get("cms_id")+"", null);
			}
		}

		//db에 값 저장하기 위한 에러여부 체크,에러시 value -1,성공시 value에 신청건수
		Map<String, Integer>cmsSuccess=new HashMap<String, Integer>();
		for(String cms_id:cms_loopDatas.keySet()) {
			
			//전문송신
			BaseAction con=null;
			try {
				List<String> loopData=cms_loopDatas.get(cms_id);
				AesEncrypt ae=new AesEncrypt();
				String cms_pw=cmsDAO.selectCmsDetail(cms_id).get("cms_pw")+"";
				cms_pw=ae.decrypt(cms_pw);
				con=new XC_JavaSocket();
				
				//연결
				if(con.connect("121.134.74.90", 21000)<0){
					cmsSuccess.put(cms_id, -1);
					Log.warn(ps_date+"/"+cms_id+"/회원신청 연결실패");
					cms_fail_reasons.put(cms_id, "효성통신실패");
					continue;
				}
				//시작전문
				String start="S";
				start+=writeData(cms_id, 10);
				start+=writeData(cms_pw, 10);
				start+=writeData(PROGRAM_ID, 10);
				start+=writeData(PROGRAM_PW, 10);
				start+=writeData(Cms.SDSI_DATA_PAY, 3);
				start+=writeData("A", 1);//신청A 결과 R 신청내역조회 S
				start+=writeData(ps_date.replaceAll("[^0-9]", ""), 8);
				start=spaceData(start,298);
				start+="\r\n";
				con.sendData(start);
				byte[] recStart=con.recvData();
				if(recStart[243]=='N') {
					cmsSuccess.put(cms_id, -1);
					String cms_error=new String(recStart, 244, 34,"EUC-KR");
					cms_error=cms_error.trim();
					applyError+=cms_id+":"+cms_error+"\n";
					Log.warn(ps_date+"/"+cms_id+"/출금 신청 recStart="+cms_error);
					cms_fail_reasons.put(cms_id, cms_error);
					con.close();
					continue;
				}
				
				String header="H";
				header+=writeData(cms_id, 10);
				//data 신청 건수
				if(regi) {
					header+=writeData(loopData.size()+"", 6);
					header+=writeData(cms_money.get(cms_id)+"", 15);
				}else {
					header+=writeData("0", 6);
					header+=writeData("0", 15);
				}
				header=spaceData(header, 298);
				header+="\r\n";
				con.sendData(header);
				
				if(regi) {
					for(int i=0;i<loopData.size();i++) {
						con.sendData(loopData.get(i));
					}
				}
				
				String tail="T";
				tail+=writeData(cms_id, 10);
				tail=spaceData(tail, 298);
				tail+="\r\n";
				con.sendData(tail);
				
				byte[] recHeader=con.recvData();
				if(recHeader[243]=='N') {
					cmsSuccess.put(cms_id, -1);
					con.close();
					String cms_error=new String(recHeader, 244, 34,"EUC-KR");
					cms_error=cms_error.trim();
					applyError+=cms_id+":"+cms_error+"\n";
					Log.warn(ps_date+"/"+cms_id+"/출금신청 recHeader="+new String(recHeader, 244, 34,"EUC_KR"));
					cms_fail_reasons.put(cms_id, cms_error);
					continue;
				}
				
				byte[] recTail=null;
				boolean b_error=true;
				while(b_error){
					byte[] errorLoop=con.recvData();
					if(errorLoop[0]=='T') {
						recTail=errorLoop;
						b_error=false;
					}else {
						String error_mnum=new String(errorLoop,12,20);
						error_mnum=error_mnum.trim();
						String reason=new String(errorLoop,244,34,"EUC-KR");
						reason=reason.trim();
						
						//한 고객이 출금2건이상(출금실패)하는 경우를 위해 list로 처리
						List<String> reasons;
						if(mnum_reasons.containsKey(error_mnum)) {
							reasons=mnum_reasons.get(error_mnum);
						}else {
							reasons=new ArrayList<String>();
						}
						reasons.add(reason);
						mnum_reasons.put(error_mnum, reasons);
					}
				}
				System.out.println(new String(recTail,"EUC_KR"));
				if(recTail.length>280) {
					if(recTail[243]=='N') {
						String cms_error=new String(recStart, 244, 34,"EUC-KR");
						cms_error=cms_error.trim();
						applyError+=cms_id+":"+cms_error+"\n";
						Log.warn(ps_date+"/"+cms_id+"/출금신청 recTail="+new String(recTail, 244, 34,"EUC_KR"));
						cms_fail_reasons.put(cms_id, cms_error);
						cmsSuccess.put(cms_id, -1);
					}
				}else {
					cmsSuccess.put(cms_id, -1);
					Log.warn(ps_date+"/"+cms_id+"/출금신청 recTail 길이 짧음"+new String(recTail,"EUC_KR").trim());
					String cms_error=new String(recStart, 244, 34,"EUC-KR");
					cms_error=cms_error.trim();
					applyError+=cms_id+":"+cms_error+"\n";
					cms_fail_reasons.put(cms_id, cms_error);
				}
				byte[] recEnd=con.recvData();
				con.close();
				
				if(regi) {
					if(!cmsSuccess.containsKey(cms_id)) {
						cmsSuccess.put(cms_id, loopData.size());
					}
				}else {
					cmsSuccess.put(cms_id, 0);
				}
			}catch (Exception e) {
				cmsSuccess.put(cms_id, -1);
				e.printStackTrace();
				
				applyError+=cms_id+":자바오류"+e.toString()+"\n";
				cms_fail_reasons.put(cms_id, e.toString());
			}finally {
				if(con!=null)con.close();
			}
		}
		
		
		//db update
		boolean upDb=true;
		for(int i=0;i<psList.size();i++) {
			Map<String,Object> ps=psList.get(i);
			Map<String,Object> updateMap=new HashMap<String, Object>();
			updateMap.put("ps_idx", ps.get("ps_idx"));
			updateMap.put("pay_idx", ps.get("ps_pay_idx"));
			
			updateMap.put("pay_reason", "");
			if(regi) {
				updateMap.put("pay_state", Pay.STATE_ON_PROCESS);
				String cms_id=ps.get("cms_id")+"";
				if(cmsSuccess.get(cms_id)>0) {
					String m_num=ps.get("m_num")+"";
					updateMap.put("ps_state", Pay.SEND_STATE_SEND);
					updateMap.put("ps_reason", "");
					if(mnum_reasons.containsKey(m_num)) {
						List<String> reasons=mnum_reasons.get(m_num);
						if(reasons.size()>0) {
							String failReason=reasons.get(0);
							updateMap.put("ps_reason", failReason);
							updateMap.put("pay_reason", failReason);
							reasons.remove(0);
							
							if(failReason.equals("D030미등록회원")) {
								Map<String,Object> updateCms=new HashMap<String, Object>();
								updateCms.put("m_idx", ps.get("ps_m_idx"));
								updateCms.put("m_cms", MemberInfo.CMS_UNREGISTERD);
								memberDAO.updateMemberCms(updateCms);
								Log.warn("출금신청 결과 미등록 회원인 경우"+updateCms.entrySet());
							}
						}else {
							mnum_reasons.remove(m_num);
						}
					}
					
				}else {
					if(cms_fail_reasons.get(cms_id).indexOf("S007")==-1){
						//신청날짜 초과해서 신청한 경우 신청오류 reason 업데이트 안함
						updateMap.put("ps_state", Pay.SEND_STATE_ERROR);
						updateMap.put("ps_reason", cms_fail_reasons.get(cms_id));
					}
					
				}
				
			}else {
				//신청취소
				updateMap.put("ps_state", Pay.SEND_STATE_BEFORE);
				//ps.put("ps_reason", "");
				updateMap.put("pay_state", Pay.SEND_STATE_SEND);
			}
			//System.out.println("umap"+updateMap.entrySet());
			int result=payDAO.updatePaySend(updateMap);
			payDAO.updatePay(updateMap);
			
			if(result<1) {
				upDb=false;
			}
		}
		
		if(upDb) {
			msg="처리하였습니다.";
		}else {
			msg="DB 업데이트 실패. 신청내역 조회 부탁드립니다.";
		}
		
		if(!applyError.equals("")) {
			applyError="\n신청오류\n"+applyError;
		}
		return msg+applyError;
	}
	
	/**효성Cms에 신청 보낼 수 있는 시간인지. time에는 Pay.send 상수.*/
	public boolean isRegTime(String ps_date,int time) {
		try{
			Calendar now=Calendar.getInstance();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Date ps=sdf.parse(ps_date);
			Calendar ps_cal=Calendar.getInstance();
			ps_cal.setTime(ps);
			ps_cal.set(Calendar.HOUR_OF_DAY, time);
			
			//출금 신청은 하루 전 17시까지임.
			if(time==Pay.SEND_PAY_TIME) {
				now.add(Calendar.DATE, -1);
			}
			
			//주말넘어가기
			int addDate=now.get(Calendar.DAY_OF_WEEK);
			if(addDate==7) {
				now.add(Calendar.DATE, -1);
			}else if(addDate==1) {
				now.add(Calendar.DATE, -2);
			}
			
			return now.before(ps_cal);
		}catch (Exception e) {
			return false;
		}
	}
	
	/**카드api 회원 등록,수정<br>
	 * 등록안된 경우 등록으로 진행된다고 함*/
	public int changeCardCms(int m_idx) {
		Map<String,Object> memberCms=cmsDAO.selectCmsByMidx(m_idx);
		if(memberCms==null) {
			Map<String,Object> memberUp=new HashMap<String, Object>();
			memberUp.put("m_idx", m_idx);
			memberUp.put("m_cms_fail", "cms id 선택 부탁드립니다.");
    		memberDAO.updateMember(memberUp);
			return -1;
		}
		int result=0;
		Response response=null;
		try {
			String cms_card=memberCms.get("cms_card")+"";
			AesEncrypt ae=new AesEncrypt();
			cms_card=ae.decrypt(cms_card);
			
			MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
			
			HostnameVerifier hv=new CardHV();
	    	OkHttpClient client = new OkHttpClient.Builder().hostnameVerifier(hv).build();
	    	
	    	String memberName=memberCms.get("m_pay_owner")+"";
	    	String phone=memberCms.get("m_phone")+"";
	    	String memeberId=memberCms.get("m_num")+"";
	    	;
	    	String paymentNumber=memberCms.get("m_pay_num")+"";
	    	String payerName=memberCms.get("m_pay_owner")+"";
	    	if(memberName.equals("")) {
	    		memberName=payerName;
	    	}
	    	
	    	String m_id_type=memberCms.get("m_id_type")+"";
			String m_pay_id=memberCms.get("m_pay_id")+"";
			m_pay_id=m_pay_id.replaceAll("[^0-9]", "");
			if(m_id_type.equals(MemberInfo.ID_TYPE_PERSON)) {
				m_pay_id=m_pay_id.substring(0, 6);//개인
			}else {
				m_pay_id=m_pay_id.substring(0, 10);//법인
			}
			
	    	String payerNumber=m_pay_id;
	    	String validYear=memberCms.get("m_card_year")+"";
	    	String validMonth=memberCms.get("m_card_month")+"";
	    	
	    	String body_send="\"memberId\":\""+memeberId+"\",";
	    	body_send+="\"memberName\":\""+memberName+"\",";
	    	body_send+="\"phone\":\""+phone+"\",";
	    	body_send+="\"paymentKind\":\"CARD\",";
	    	body_send+="\"paymentNumber\":\""+paymentNumber+"\",";
	    	body_send+="\"payerNumber\":\""+payerNumber+"\",";
	    	body_send+="\"payerName\":\""+payerName+"\",";
	    	body_send+="\"validYear\":\""+validYear+"\",";
	    	body_send+="\"validMonth\":\""+validMonth+"\"";
	    	
	    	RequestBody body = RequestBody.create(mediaType, "{"+body_send+"}");
	        Request req = new Request.Builder()
	        .url("https://api.efnc.co.kr/v1/members/"+memberCms.get("m_num"))
	        .put(body)
	        .addHeader("content-type", "application/json;charset=UTF-8")
	        .addHeader("Authorization", "VAN "+VAN+":"+cms_card)
	        .build();
	        response = client.newCall(req).execute();
	        String str=response.body().string().toString();
	        JSONParser jp=new JSONParser();
	        JSONObject jo=(JSONObject)jp.parse(str);
	        
	        
	        Log.warn(m_idx+"카드 회원 등록/수정 "+str);
	        //m_cms update
	        Map<String,Object> memberUp=new HashMap<String, Object>();
	        memberUp.put("m_idx", m_idx);
	        if(jo.containsKey("error")) {
	        	//삭제 실패
	        	JSONObject errorObj=(JSONObject)jp.parse(jo.get("error")+"");
	        	String developerMessage=errorObj.get("developerMessage")+"";

	        	if(developerMessage.equals("해당 자원을 찾을 수 없습니다.")) {
	        		//회원등록안된경우
	        		memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
	        		memberUp.put("m_cms_fail", "");
	        		memberDAO.updateMemberCms(memberUp);
	        		result=1;
	        	}else {
	        		memberUp.put("m_cms_fail", errorObj.get("developerMessage"));
	        		memberDAO.updateMember(memberUp);
	        		result=-1;
	        	}
	        }else {
	        	JSONObject memObj=(JSONObject)jp.parse(jo.get("member")+"");
	        	if(memObj.get("status").equals("신청실패")) {
	        		JSONObject resultObj=(JSONObject)jp.parse(memObj.get("result")+"");
	        		String failMsg=resultObj.get("message")+"";
	        		memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
	        		memberUp.put("m_cms_fail", failMsg.trim());
	        	}else {
	        		//등록 성공
	        		memberUp.put("m_cms", MemberInfo.CMS_REGISTERD);
	        		memberUp.put("m_cms_fail", "");
	        	}
	        	memberDAO.updateMemberCms(memberUp);
	        	result=1;
	        }
	    }catch (SSLPeerUnverifiedException spue) {
	    	Log.warn("card api 회원 등록/수정 hostName 다른 값 가져옴.");
	    	result=-1;
		} catch (Exception e) {
	    	e.printStackTrace();
	    	result=-1;
	    }finally {
	    	if(response!=null) response.close();
	    }
		return result;
	}
	/**카드api 회원 삭제*/
	public int deleteCms(int m_idx) {
		Map<String,Object> memberCms=cmsDAO.selectCmsByMidx(m_idx);
		if(memberCms==null) {
			Log.warn("회원 삭제 map==null "+m_idx);
			return -1;
		}
		
		int result=0;
		Response response=null;
		try {
			String cms_card=memberCms.get("cms_card")+"";
			AesEncrypt ae=new AesEncrypt();
			cms_card=ae.decrypt(cms_card);
			
			MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
			
			HostnameVerifier hv=new CardHV();
	    	OkHttpClient client = new OkHttpClient.Builder().hostnameVerifier(hv).build();
	    	
	    	//String body_send="";
	    	//RequestBody body = RequestBody.create(mediaType, "{"+body_send+"}");
	        Request req = new Request.Builder()
	        .url("https://api.efnc.co.kr/v1/members/"+memberCms.get("m_num"))
	        .delete()
	        .addHeader("content-type", "application/json;charset=UTF-8")
	        .addHeader("Authorization", "VAN "+VAN+":"+cms_card)
	        .build();
	        response = client.newCall(req).execute();
	        String str=response.body().string().toString();
	        
	        //m_cms update
	        Map<String,Object> memberUp=new HashMap<String, Object>();
	        memberUp.put("m_idx", m_idx);
	        
	        Log.warn(m_idx+" 회원 삭제"+str);
	        if(str==null||str.trim().equals("")) {
	        	memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
	        	memberUp.put("m_cms_fail", "");
        		memberDAO.updateMemberCms(memberUp);
        		result=1;
	        }else {
	        	JSONParser jp=new JSONParser();
	        	JSONObject jo=(JSONObject)jp.parse(str);
	        	
	        	if(jo.containsKey("error")) {
	        		//삭제 실패
	        		JSONObject errorObj=(JSONObject)jp.parse(jo.get("error")+"");
	        		String developerMessage=errorObj.get("developerMessage")+"";
	        		
	        		if(developerMessage.equals("해당 자원을 찾을 수 없습니다.")) {
	        			//회원등록안된경우
	        			memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
	        			memberUp.put("m_cms_fail", "");
	        			memberDAO.updateMemberCms(memberUp);
	        			result=1;
	        		}else {
	        			memberUp.put("m_cms_fail", errorObj.get("message"));
	        			memberDAO.updateMember(memberUp);
	        			result=-1;
	        		}
	        	}else {
	        		//삭제 성공
	        		memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
	        		memberUp.put("m_cms_fail", "");
	        		memberDAO.updateMemberCms(memberUp);
	        		result=1;
	        	}
	        }
	        
	    }catch (SSLPeerUnverifiedException spue) {
	    	Log.warn("card api 회원 삭제 hostName 다른 값 가져옴.");
	    	result=-1;
		} catch (Exception e) {
			Log.warn("삭제 실패 Exception"+e.toString()+"/midx="+m_idx);
	    	e.printStackTrace();
	    	result=-1;
	    }finally {
	    	if(response!=null) response.close();
	    }
		return result;
	}
	
	/**카드api 출금*/
	synchronized public String payCard(String pay_idxs, int smsSender_eid) {
		String msg="출금 신청 완료";
		
		StringTokenizer st=new StringTokenizer(pay_idxs, ",");
		int total=st.countTokens();
		int result=0;
		
		Calendar now=Calendar.getInstance();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String pay_date=sdf.format(now.getTime());
		String sms_date=now.get(Calendar.YEAR)+"년 "+(now.get(Calendar.MONTH)+1)+"월 "+now.get(Calendar.DATE)+"일";
		while(st.hasMoreTokens()) {
			Response response=null;
			try {
				String str_pay_idx=st.nextToken();
				int pay_idx=Integer.parseInt(str_pay_idx);
				Map<String,Object> pay=payDAO.selectPayDetail(pay_idx);
				if(!pay.get("m_cms").equals(MemberInfo.CMS_REGISTERD)) {
					continue;
				}else if(!pay.get("pay_state").equals(Pay.STATE_UNPAID)) {
					continue;
				}
				
				//pay 출금중으로 상태 바꿈
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("pay_idx", pay_idx);
				map.put("pay_state", Pay.STATE_ON_PROCESS);
				map.put("pay_date", pay_date);
				String pay_num="000"+(Math.random()*1000);
				pay_num=Pay.NUM_CARD+pay_idx+pay_num.substring(pay_num.length()-3);
				map.put("pay_num", pay_num);
				map.put("pay_bigo", pay.get("cms_id"));
				map.put("pay_method", pay.get(Pay.METHOD_CARD));
				payDAO.updatePay(map);
				
				map.remove("pay_date");
				map.remove("pay_num");
				
				//카드 출금 요청
				String cms_card=pay.get("cms_card")+"";
				AesEncrypt ae=new AesEncrypt();
				cms_card=ae.decrypt(cms_card);
				MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
				
				HostnameVerifier hv=new CardHV();
		    	OkHttpClient client = new OkHttpClient.Builder().hostnameVerifier(hv).build();
		    	
		    	String body_send="\"transactionId\":\""+pay_num+"\",";
		    	body_send+="\"memberId\":\""+pay.get("m_num")+"\",";
		    	body_send+="\"cardPointFlag\":\"N\",";
		    	body_send+="\"callAmount\":\""+pay.get("pay_money")+"\"";
		    	
		    	RequestBody body = RequestBody.create(mediaType, "{"+body_send+"}");
		        Request req = new Request.Builder()
		        .url("https://api.efnc.co.kr/v1/payments/card")
		        .post(body)
		        .addHeader("content-type", "application/json;charset=UTF-8")
		        .addHeader("Authorization", "VAN "+VAN+":"+cms_card)
		        .build();
		        System.out.println("body_send="+body_send);
		        response = client.newCall(req).execute();
		        String str=response.body().string().toString();
		        JSONParser jp=new JSONParser();
		        JSONObject jo=(JSONObject)jp.parse(str);
				System.out.println("str="+str);
		        //pay 결과 update
				boolean sendSms=false;
		        if(jo.containsKey("error")) {
		        	//출금 신청 실패
		        	map.put("pay_state", Pay.STATE_FAIL);
		        	JSONObject errorObj=(JSONObject)jp.parse(jo.get("error")+"");
		        	String developerMessage=errorObj.get("developerMessage")+"";

		        	if(developerMessage.equals("해당 자원을 찾을 수 없습니다.")) {
		        		Log.warn("카드 출금 시도.회원 미등록 상태"+pay.get("m_num")+"/m_idx"+pay.get("m_idx"));
		        		//회원등록안된경우
		        		Map<String,Object> memberUp=new HashMap<String, Object>();
		        		memberUp.put("m_idx", pay.get("m_idx"));
		        		memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
		        		memberUp.put("m_cms_fail", "");
		        		memberDAO.updateMemberCms(memberUp);
		        	}
		        	
		        	//출금실패 이유 업데이트
					map.put("pay_reason", developerMessage);
					payDAO.updatePay(map);
					
		        }else {
		        	//출금 신청 성공
		        	JSONObject payment=(JSONObject)jp.parse(jo.get("payment")+"");
		        	System.out.println(jo.get("payment"));
		        	System.out.println("status="+payment.get("status"));
		        	if(payment.get("status").equals("승인성공")) {
		        		map.put("pay_state", Pay.STATE_PAID);
		        		map.put("pay_real", pay.get("pay_money"));
		        		payDAO.updatePay(map);
		        		sendSms=true;
		        	}else {
		        		System.out.println("result.messgae=");
		        		System.out.println(((JSONObject)payment.get("result")).get("message"));
		        		//출금실패 이유 업데이트
		        		map.put("pay_state", Pay.STATE_FAIL);
						map.put("pay_reason", ((JSONObject)payment.get("result")).get("message"));
						payDAO.updatePay(map);
						
		        	}
		        }
		        
		        int pay_m_idx=Integer.parseInt(pay.get("pay_m_idx")+"");
		        //출금 신청전 데이터 ++
		        pay.put("m_idx", pay_m_idx);
		        pay.put("pay_date", "");
		        pay.put("pay_state", Pay.STATE_UNPAID);
		        pay.put("pay_carryover_date", "");
		        pay.put("pay_reason", "");
		        pay.put("pay_bigo", "");
		        pay.put("pay_num", "");
		        result=payDAO.insertPay(pay);
		        
		        //문자발송
		        if(sendSms) {
		        	String m_phone=pay.get("m_phone")+"";
		        	m_phone.replaceAll("[^0-9]", "");
		        	if(m_phone.length()<12) {
		        		String pr_idx=pay.get("m_pr_idx")+"";
		        		Map<String,Object> map_pr=productDAO.selectProductRentalDetail(pr_idx);
		        		String sms_from=map_pr.get("pr_sms")+"";
		        		//String sms_msg = sendingCompany+" \n"+name+"고객님 "+year+"년"+month+"월"+day+"일\n"+pay_cnt+"회 렌탈료 "+df.format(i_pay)+"원 출금될 예정입니다.";
		        		String sms_content=map_pr.get("pr_sms_company")+"\n"+pay.get("m_pay_owner")+" 님 "+sms_date+" "+pay.get("pay_cnt")+"회 렌탈료 "+pay.get("pay_money_f")+"원 출금될 예정입니다.";
		        		smsService.sendSms(Sms.TYPE_PAY, smsSender_eid, pay_m_idx, m_phone, sms_from, sms_content);
		        	}else {
		        		Log.warn(pay_m_idx+" 문자 수신 번호 길이 오류"+m_phone+"/카드출금문자발송");
		        	}
		        }
			}catch (SSLPeerUnverifiedException spue) {
		    	Log.warn("card api 카드 출금 hostName 다른 값 가져옴.");
			} catch (Exception e) {
				Log.warn("카드 출금 중 에러발생"+e.toString());
		    	e.printStackTrace();
		    }finally {
		    	if(response!=null) response.close();
		    }
		}
		
		if(total!=result) {
			msg+="\n\n"+total+"건 중 "+result+"건 출금요청";
			msg+="\n미등록회원 인경우 등록 후 출금 진행 부탁 드립니다.";
		}
		
		return msg;
	}
	
	/**카드api 출금조회*/
	public String payCardCheck(int pay_idx){
		String msg="test";
		Response response=null;
		try {
			Map<String,Object> pay=payDAO.selectPayDetail(pay_idx);
			String pay_state=(String)pay.get("pay_state");
			String pay_num=(String)pay.get("pay_num");
			if(pay_num==null||pay_num.equals("")) {
				return "출금 요청 정보가 없습니다.";
			}
			
			String cms_card=pay.get("cms_card")+"";
			AesEncrypt ae=new AesEncrypt();
			cms_card=ae.decrypt(cms_card);
			
			String pay_bigo=pay.get("pay_bigo")+"";
			if(!pay_bigo.equals("")) {
				Map<String,Object> cmsMap=cmsDAO.selectCmsDetail(pay_bigo);
				if(cmsMap!=null) {
					cms_card=cmsMap.get("cms_card")+"";
					cms_card=ae.decrypt(cms_card);
				}
			}
			
			HostnameVerifier hv=new CardHV();
	    	OkHttpClient client = new OkHttpClient.Builder().hostnameVerifier(hv).build();
	    	
	        Request req = new Request.Builder()
	        .url("https://api.efnc.co.kr/v1/payments/card/"+pay_num)
	        .get()
	        .addHeader("content-type", "application/json;charset=UTF-8")
	        .addHeader("Authorization", "VAN "+VAN+":"+cms_card)
	        .build();
	        response = client.newCall(req).execute();
	        String str=response.body().string().toString();
	        JSONParser jp=new JSONParser();
	        JSONObject jo=(JSONObject)jp.parse(str);
			//System.out.println("str="+str);
	        //pay 결과 update
	        if(jo.containsKey("error")) {
	        	JSONObject errorObj=(JSONObject)jp.parse(jo.get("error")+"");
	        	String developerMessage=errorObj.get("developerMessage")+"";
	        	
	        	if(developerMessage.equals("해당 승인건을 찾을 수 없습니다.")&&(pay_state.equals(Pay.STATE_ON_PROCESS)||pay_state.equals(Pay.STATE_PAID))) {
	        		Log.warn("출금조회 결과 요청건 없음(데이터는 기록있음)"+pay.get("pay_idx")+"/"+pay.get("pay_state")+"/"+pay.get("pay_date")+"/"+pay.get("pay_reason")+"/"+pay.get("pay_num")+"/");
	        		Map<String,Object> payUp=new HashMap<String, Object>();
	        		payUp.put("pay_idx", pay_idx);
	        		payUp.put("pay_state", Pay.STATE_UNPAID);
	        		payUp.put("pay_date", "");
	        		payUp.put("pay_reason", "");
	        		payUp.put("pay_num", "");
	        		payDAO.updatePay(payUp);
	        		
	        		msg="출금 요청 기록이 없습니다.\n출금 상태가 신청전으로 변경됩니다.";
	        	}else {
	        		msg=developerMessage;
	        	}
	        }else {
	        	//출금 신청 성공
	        	JSONObject payment=(JSONObject)jp.parse(jo.get("payment")+"");
	        	//System.out.println(jo.get("payment"));
	        	//System.out.println("status="+payment.get("status"));
	        	JSONObject result=(JSONObject)payment.get("result");
	        	msg=result.get("message")+"";
	        	
	        	//db에 상태 반영 안되어있는 경우의 로직
	        	if(payment.get("status").equals("승인취소성공")&&!pay_state.equals(Pay.STATE_REFUND)) {
	        		Log.warn("카드 승인 취소 결과 반영. pay 새로 insert함/pay_idx="+pay_idx);
	        		
	        		Map<String,Object> map=new HashMap<String, Object>();
	        		map.put("pay_idx", pay_idx);
	        		map.put("pay_state", Pay.STATE_REFUND);
	        		payDAO.updatePay(map);
	        	}else if(payment.get("status").equals("승인실패")&&!pay_state.equals(Pay.STATE_FAIL)){
	        		Log.warn("카드 승인 실패 결과 반영. pay 새로 insert함/pay_idx="+pay_idx);
	        		
	        		Map<String,Object> map=new HashMap<String, Object>();
	        		map.put("pay_idx", pay_idx);
	        		map.put("pay_reason", msg);
	        		map.put("pay_state", Pay.STATE_FAIL);
	        		payDAO.updatePay(map);
	        	}
	        	
	        	if(msg.equals("승인성공")) {
	        		DecimalFormat df=new DecimalFormat("#,###,###,###");
	        		msg=payment.get("memberId")+"\n"+payment.get("memberName")+"\n"+payment.get("paymentDate")+"\n";
	        		msg+=df.format(payment.get("actualAmount"))+"원 출금완료";
	        	}
	        }
	        
	        pay.put("m_idx", pay.get("pay_m_idx"));
	        pay.put("pay_date", "");
	        pay.put("pay_state", Pay.STATE_UNPAID);
	        pay.put("pay_carryover_date", "");
	        pay.put("pay_reason", "");
	        pay.put("pay_bigo", "");
	        pay.put("pay_num", "");
	        payDAO.insertPay(pay);
		}catch (SSLPeerUnverifiedException spue) {
	    	Log.warn("card api 카드 출금 hostName 다른 값 가져옴.");
		} catch (Exception e) {
			Log.warn("카드 출금 중 에러발생"+e.toString());
	    	e.printStackTrace();
	    }finally {
	    	if(response!=null) response.close();
	    }
		
		return msg;
	}
	
	/**카드api 출금취소*/
	public String payCardCancel(int pay_idx) {
		String msg="";
		Response response=null;
		try {
			Map<String,Object> pay=payDAO.selectPayDetail(pay_idx);
			if(!pay.get("pay_state").equals(Pay.STATE_PAID)) {
				return "출금완료 상태만 취소 가능합니다.";
			}
				
			//카드 출금 취소 요청
			String cms_card=pay.get("cms_card")+"";
			AesEncrypt ae=new AesEncrypt();
			cms_card=ae.decrypt(cms_card);
			
			String pay_bigo=pay.get("pay_bigo")+"";
			if(!pay_bigo.equals("")) {
				Map<String,Object> cmsMap=cmsDAO.selectCmsDetail(pay_bigo);
				if(cmsMap!=null) {
					cms_card=cmsMap.get("cms_card")+"";
					cms_card=ae.decrypt(cms_card);
				}
			}
			
			MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
			
			HostnameVerifier hv=new CardHV();
	    	OkHttpClient client = new OkHttpClient.Builder().hostnameVerifier(hv).build();
		    
	    	String pay_num=(String)pay.get("pay_num");
	    	RequestBody body = RequestBody.create(mediaType, "{}");
	        Request req = new Request.Builder()
	        .url("https://api.efnc.co.kr/v1/payments/card/"+pay_num+"/cancel")
	        .post(body)
	        .addHeader("content-type", "application/json;charset=UTF-8")
	        .addHeader("Authorization", "VAN "+VAN+":"+cms_card)
	        .build();
	        response = client.newCall(req).execute();
	        String str=response.body().string().toString();
	        JSONParser jp=new JSONParser();
	        JSONObject jo=(JSONObject)jp.parse(str);
			System.out.println("str="+str);
	        //pay 결과 update
			String pay_state=(String)pay.get("pay_state");
			
			Map<String,Object> payUp=new HashMap<String, Object>();
			payUp.put("pay_idx", pay_idx);
			payUp.put("pay_state", Pay.STATE_REFUND);
			if(jo.containsKey("error")) {
	        	JSONObject errorObj=(JSONObject)jp.parse(jo.get("error")+"");
	        	String developerMessage=errorObj.get("developerMessage")+"";
	        	msg=developerMessage;
	        	
	        	if(developerMessage.equals("해당 승인건을 찾을 수 없습니다.")||developerMessage.equals("기취소 완료건")) {
	        		Log.warn("출금취소 결과 요청건 없음(데이터는 기록있음)"+pay.get("pay_idx")+"/"+pay.get("pay_state")+"/"+pay.get("pay_date")+"/"+pay.get("pay_reason")+"/"+pay.get("pay_num")+"/");
	        		payDAO.updatePay(payUp);
	        	}
	        }else {
	        	//출금 취소 신청 성공
	        	JSONObject payment=(JSONObject)jp.parse(jo.get("payment")+"");
	        	JSONObject result=(JSONObject)payment.get("result");
	        	msg=result.get("message")+"";
	        	
	        	//db에 상태 반영 안되어있는 경우의 로직
	        	if(payment.get("status").equals("승인취소성공")) {
	        		payDAO.updatePay(payUp);
	        		msg="승인취소성공";
	        	}else{
	        		Log.warn("카드 취소 실패.pay_idx="+pay_idx+"/"+str);
	        	}
	        	
	        }
			//pay새로 insert
			pay.put("m_idx", pay.get("pay_m_idx"));
			pay.put("pay_date", "");
			pay.put("pay_state", Pay.STATE_UNPAID);
			pay.put("pay_carryover_date", "");
			pay.put("pay_reason", "");
			pay.put("pay_bigo", "");
			pay.put("pay_num", "");
			payDAO.insertPay(pay);
		}catch (SSLPeerUnverifiedException spue) {
	    	Log.warn("card api 카드 출금 hostName 다른 값 가져옴.");
		} catch (Exception e) {
			Log.warn("카드 출금 중 에러발생"+e.toString());
	    	e.printStackTrace();
	    }finally {
	    	if(response!=null) response.close();
	    }
		
		return msg;
	}
	
	/**실시간출금 조회*/
	public String realtimeCheck(int pay_idx) {
		String msg="";
		BaseAction con=null;
		try {
			Map<String,Object> pay=payDAO.selectPayDetail(pay_idx);
			AesEncrypt ae=new AesEncrypt();
			String cms_id=pay.get("cms_id")+"";
			String cms_pw=ae.decrypt(pay.get("cms_pw")+"");
			
			String pay_bigo=pay.get("pay_bigo")+"";
			if(!pay_bigo.equals("")) {
				Map<String,Object> cmsMap=cmsDAO.selectCmsDetail(pay_bigo);
				if(cmsMap!=null) {
					cms_id=cmsMap.get("cms_id")+"";
					cms_pw=ae.decrypt(cmsMap.get("cms_pw")+"");
				}
			}
			
			con=new XC_JavaSocket();
			
			//연결
			if(con.connect("121.134.74.90", 24000)<0){
				Log.warn(cms_id+"실시간출금 소켓 접속실패");
				return "통신 실패";
			}
			//시작전문
			String start=writeData(cms_id, 10);
			start+=writeData(cms_pw, 10);
			start+=writeData(PROGRAM_ID, 10);
			start+=writeData(PROGRAM_PW, 10);
			
			Calendar now=Calendar.getInstance();
			SimpleDateFormat sdf=new SimpleDateFormat("yyMMdd");
			start+=writeData(sdf.format(now.getTime()), 6);//yymmdd
			start+=writeData("1000", 4);
			start+=writeData("200", 3);
			start=spaceData(start,100);
			
			SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
			start+=writeData(sdf.format(sdf2.parse(pay.get("pay_date")+"")), 6);//yymmdd
			
			String orderNum=pay.get("pay_num")+"";//연번 ex.000001
			orderNum="000000"+orderNum.replace(Pay.NUM_REALTIME+pay_idx, "");
			orderNum=orderNum.substring(orderNum.length()-6);
			start+=writeData(orderNum,6);
			
			start=spaceData(start,320);
			//System.out.println("start="+start);
			con.sendData(start);
			byte[] rec=con.recvData();
			
			String payTime=new String(rec,100,6,"EUC-KR").trim();
			String payMnum=new String(rec,162,20,"EUC-KR").trim();
			if(payMnum.length()>1) {
				payMnum=payMnum.substring(0, payMnum.length()-1);
				msg=sdf2.format(sdf.parse(payTime))+" "+payMnum+"\n";
				String m_num=pay.get("m_num")+"";
				
				if(!m_num.equals(payMnum)) {
					Log.warn("실시간출금 조회 mnum이 다름"+pay_idx+"/"+m_num+"/결과"+payMnum+"/"+rec[71]);
					return "출금,조회 고객이 다릅니다.\n관리자에게 문의 부탁드립니다.";
				}
			}
			
			if(rec[71]=='Y') {
				String payMoney=new String(rec,220,15,"EUC-KR").trim();
				DecimalFormat df=new DecimalFormat("#,###,###,###");
				msg+=df.format(Integer.parseInt(payMoney))+"원\n출금완료";
				
				String pay_state=pay.get("pay_state")+"";
				if(!pay_state.equals(Pay.STATE_PAID)) {
					Log.warn("실시간출금 조회로 결과 반영 함. 이전 값 "+pay_idx+"/"+pay.get("pay_state")+"/"+pay.get("pay_reason"));
					Map<String,Object> payUpdate=new HashMap<String, Object>();
					payUpdate.put("pay_idx", pay_idx);
					payUpdate.put("pay_real", Integer.parseInt(payMoney));
					payUpdate.put("pay_state", Pay.STATE_PAID);
					payUpdate.put("pay_reason", Pay.REASON_REALTIME);
					payDAO.updatePay(payUpdate);
				}
			}else {
				//System.out.println("rec="+new String(rec,"EUC-KR").trim());
				String errorCode=new String(rec,72,4,"EUC-KR").trim();
				msg="출금실패 코드:"+errorCode;
				if(errorCode.equalsIgnoreCase("R088")) {
					msg+=" 원거래없음.";
				}
				
				String pay_state=pay.get("pay_state")+"";
				if(!pay_state.equals(Pay.STATE_FAIL)) {
					Log.warn("실시간출금 조회로 결과 반영 함. 이전 값 "+pay_idx+"/"+pay.get("pay_state")+"/"+pay.get("pay_reason"));
				}
				Map<String,Object> payUpdate=new HashMap<String, Object>();
				payUpdate.put("pay_idx", pay_idx);
				payUpdate.put("pay_state", Pay.STATE_FAIL);
				payUpdate.put("pay_reason", Pay.REASON_REALTIME+" "+errorCode);
				payDAO.updatePay(payUpdate);
				
				pay.put("m_idx", pay.get("pay_m_idx"));
		        pay.put("pay_date", "");
		        pay.put("pay_state", Pay.STATE_UNPAID);
		        pay.put("pay_carryover_date", "");
		        pay.put("pay_reason", "");
		        pay.put("pay_bigo", "");
		        pay.put("pay_num", "");
		        payDAO.insertPay(pay);
			}
		}catch (Exception e) {
			msg="오류발생.";
			Log.warn("실시간출금조회 오류/"+e.toString());
		}finally {
			if(con!=null)con.close();
		}
		
		return msg;
	}
	
	/**실시간출금*/
	synchronized public String realtimePay(int pay_idx) {
		String msg="";
		BaseAction con=null;
		BaseAction con2=null;
		try {
			
			Map<String,Object> pay=payDAO.selectPayDetail(pay_idx);
			String cms_id=pay.get("cms_id")+"";
			
			String pay_state=pay.get("pay_state")+"";
			if(!pay_state.equals(Pay.STATE_UNPAID)) {
				return "신청전 상태만 출금신청 가능합니다.";
			}
			
			String m_pay_info=pay.get("m_pay_info")+"";
			String bankCode=payDAO.selectSubBank(m_pay_info, "sb_silsigan");
			if(bankCode==null||bankCode.trim().equals("")) {
				return "실시간출금을 지원하지 않는 은행입니다.";
			}
			
			
			Calendar now=Calendar.getInstance();
			SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
			String pay_date=sdf2.format(now.getTime());
			String pay_num=payDAO.insertPayNum(pay_idx, pay_date,cms_id);
			int m_idx=Integer.parseInt(pay.get("pay_m_idx")+"");
			
			File f;
			String originalFileName;
			List<Map<String,Object>> mf=memberDAO.selectMemberFileList(m_idx+"", MemberFile.NAME_AGREEMENT_CERTFICATE);
			if(mf==null||mf.size()==0) {
				f=new File(MemberFile.PATH+MemberFile.NAME_RECORD+"/"+pay.get("m_num")+".jpg");
				originalFileName=pay.get("m_num")+".jpg";
			}else {
				Map<String,Object> mfMap=mf.get(0);
				f=new File(MemberFile.PATH+MemberFile.NAME_AGREEMENT_CERTFICATE+"/"+mfMap.get("mf_idx")+"_"+mfMap.get("mf_memo"));
				originalFileName=mfMap.get("mf_file")+"";//원본파일 확장명
				if(!f.exists()) {
					f=new File(MemberFile.PATH+MemberFile.NAME_RECORD+"/"+pay.get("m_num")+".jpg");
					originalFileName=pay.get("m_num")+".jpg";
					if(!f.exists()) {
						f=new File(MemberFile.PATH+MemberFile.NAME_RECORD+"/"+mfMap.get("mf_file"));
						originalFileName=mfMap.get("mf_file")+"";
					}
				}
			}
			
			if(!f.exists()) {
				
				Map<String,Object> payUpdate=new HashMap<String, Object>();
				payUpdate.put("pay_idx", pay_idx);
				payUpdate.put("pay_state", Pay.STATE_FAIL);
				payUpdate.put("pay_bigo", cms_id);
				payUpdate.put("pay_reason", Pay.REASON_REALTIME+" R088");
				payDAO.updatePay(payUpdate);
				
				pay.put("m_idx", pay.get("pay_m_idx"));
		        pay.put("pay_date", "");
		        pay.put("pay_state", Pay.STATE_UNPAID);
		        pay.put("pay_carryover_date", "");
		        pay.put("pay_reason", "");
		        pay.put("pay_bigo", "");
		        pay.put("pay_num", "");
		        payDAO.insertPay(pay);
				
				return MemberFile.NAME_AGREEMENT_CERTFICATE+"이 없습니다.";
			}
			
			int index=originalFileName.lastIndexOf(".");
			
			String gubun;//증빙구분
			String dataType;//데이터유형
			if(index<1) {
				return "지원하지 않는 인증서 확장명입니다.";
			}
			originalFileName=originalFileName.substring(index);
			if(originalFileName.equals(".jpg")||originalFileName.equals(".jpeg")||originalFileName.equals(".png")
					||originalFileName.equals(".gif")||originalFileName.equals(".tif")||originalFileName.equals(".tiff")
					||originalFileName.equals(".pdf")) {
				gubun="01";
			}else if(originalFileName.equals(".mp3")||originalFileName.equals(".wav")||originalFileName.equals(".wma")) {
				gubun="02";
			}else if(originalFileName.equals(".der")) {
				gubun="03";
			}else {
				return "약정서인증파일. 허용되지 않은 확장명";
			}
			dataType=originalFileName.substring(1);
			
			FileInputStream fis=new FileInputStream(f);
			ByteArrayOutputStream bao= new ByteArrayOutputStream();
			int ori_len=(int)f.length();
			byte[]b_temp=new byte[ori_len];
			Encoder enc=Base64.getEncoder();
			int len=0;

			while((len=fis.read(b_temp))!=-1){
				bao.write(b_temp, 0, len);
			}

			byte[] aft=bao.toByteArray();
			fis.close();
			bao.close();

			byte[] b_encode=enc.encode(aft);
			int i_len=b_encode.length;
			String str_base=new String(b_encode,0,i_len);
			
			
			AesEncrypt ae=new AesEncrypt();
			
			String cms_pw=ae.decrypt(pay.get("cms_pw")+"");
			//연결
			con=new XC_JavaSocket();
			if(con.connect("106.250.169.67", 25000)<0){
				Log.warn(cms_id+"실시간출금 소켓 접속실패");
				return "통신 실패";
			}
			//시작전문
			String start="S"+writeData(cms_id, 10);
			start+=writeData(cms_pw, 10);
			start+=writeData(PROGRAM_ID, 10);
			start+=writeData(PROGRAM_PW, 10);
			
			SimpleDateFormat sdf4y=new SimpleDateFormat("yyyyMMdd");
			start+=writeData(sdf4y.format(now.getTime()), 8);//yymmdd
			start+=writeData("1000", 4);
			start+=writeData("100", 3);
			start+="R";
			start+=writeData(pay.get("m_num")+Pay.NUM_REALTIME, 20);//실시간출금는 회원번호+"r"
			start+=gubun;
			start+=writeData(dataType, 5);
			
			int f_length=(int)f.length();
			String str_len="000000"+f_length;
			str_len=str_len.substring(str_len.length()-6);
			start+=str_len;
			
			start=spaceData(start, 200);
			start+="\r\n";
			con.sendData(start);
			
			byte[] recvStart=con.recvData();
			if(recvStart[90]=='N'){
				return "증빙 중 오류 발생. "+new String(recvStart,91,34,"EUC-KR");
			}
			int repeat_count=b_encode.length/60000;
			if(b_encode.length%60000!=0){
				repeat_count++;
			}
			for(int i=0;i<repeat_count;i++){
				String send_data="D";
				send_data+=writeData(cms_id,10);
				send_data+="R";
				send_data+=writeData(pay.get("m_num")+Pay.NUM_REALTIME, 20);
				if(i!=repeat_count-1){
					send_data+="N";
				}else{
					send_data+="Y";
				}
				String count="0"+(i+1);
				send_data+=count.substring(count.length()-2);
				send_data=spaceData(send_data, 100);
				if(i!=repeat_count-1){
					send_data+=new String(b_encode,60000*i,60000);
				}else{
					send_data+=new String(b_encode,60000*i,(b_encode.length-60000*i));
					send_data=spaceData(send_data, 60100);
				}
				send_data+="\r\n";
				con.sendData(send_data);
			}
			
			byte[] recvEnd=con.recvData();
			if(recvEnd[32]=='Y'){
				msg="증빙 등록 완료\n";
			}else{
				return "증빙 자료 전송 중 종료전문 오류"+new String(recvEnd,32,35,"EUC-KR");
			}
			con.close();
			
			
			con2 = new XC_JavaSocket();
			if(con2.connect("121.134.74.90", 24000)<0){
				return "출금 소켓접속 실패";
			}
			
			//시작전문
			String send=writeData(cms_id, 10);
			send+=writeData(cms_pw, 10);
			send+=writeData(PROGRAM_ID, 10);
			send+=writeData(PROGRAM_PW, 10);
			
			SimpleDateFormat sdf=new SimpleDateFormat("yyMMdd");
			send+=writeData(sdf.format(now.getTime()), 6);//yymmdd
			send+=writeData("1000", 4);
			send+=writeData("100", 3);
			
			String orderNum=pay_num;//연번 ex.000001
			orderNum="000000"+orderNum.replace(Pay.NUM_REALTIME+pay_idx, "");
			orderNum=orderNum.substring(orderNum.length()-6);
			send+=writeData(orderNum,6);
			
			send=spaceData(send,100);
			
			send+=writeData(bankCode, 2);
			send+=writeData(pay.get("m_pay_num")+"", 15);
			send+=writeData(pay.get("m_pay_id")+"", 13);
			send+=writeData(pay.get("m_pay_owner")+"", 20);
			send+=writeData(pay.get("m_num")+Pay.NUM_REALTIME, 20);
			
			String pay_mon="00000000000000000"+pay.get("pay_money");
			pay_mon=pay_mon.substring(pay_mon.length()-15);
			send+=pay_mon;
			
			send=spaceData(send,232);
			
			String m_phone=pay.get("m_phone")+"";
			m_phone=m_phone.replaceAll("[^0-9]", "");
			send+=writeData(m_phone, 20);
			send=spaceData(send,320);

			con2.sendData(send);
			
			byte[] b_temp2=con2.recvData();
			if(b_temp2[71]=='Y'){
				//String payMoney=new String(b_temp2,220,15,"EUC-KR").trim();
				String payMoney=pay.get("pay_money")+"";
				DecimalFormat df=new DecimalFormat("#,###,###,###");
				msg+=df.format(Integer.parseInt(payMoney))+"원\n출금완료";
				
				Map<String,Object> payUpdate=new HashMap<String, Object>();
				payUpdate.put("pay_idx", pay_idx);
				payUpdate.put("pay_state", Pay.STATE_PAID);
				payUpdate.put("pay_reason", Pay.REASON_REALTIME);
				payUpdate.put("pay_real", Integer.parseInt(payMoney));
				payDAO.updatePay(payUpdate);
				
			}else{
				String fail_reason=new String(b_temp2,72,4,"EUC-KR").trim();
				msg="출금실패 코드:"+fail_reason;
				
				Map<String,Object> payUpdate=new HashMap<String, Object>();
				payUpdate.put("pay_idx", pay_idx);
				payUpdate.put("pay_state", Pay.STATE_FAIL);
				payUpdate.put("pay_reason", Pay.REASON_REALTIME+" "+fail_reason);
				payDAO.updatePay(payUpdate);
				
				pay.put("m_idx", pay.get("pay_m_idx"));
		        pay.put("pay_date", "");
		        pay.put("pay_state", Pay.STATE_UNPAID);
		        pay.put("pay_carryover_date", "");
		        pay.put("pay_reason", "");
		        pay.put("pay_bigo", "");
		        pay.put("pay_num", "");
		        payDAO.insertPay(pay);
		        
			}
				
		}catch (Exception e) {
			msg="오류발생.";
			Log.warn("실시간출금 오류/"+e.toString());
		}finally {
			if(con!=null)con.close();
			if(con2!=null)con2.close();
		}
		
		return msg;
	}
	
	/**CMS회원 상태 조회*/
	public String checkCms(int m_idx) {
		Map<String,Object> memberCms=cmsDAO.selectCmsByMidx(m_idx);
		if(memberCms==null) {
			return "cms id 데이터가 없습니다.";
		}

		String msg;
		Response response=null;
		try {
			String cms_card=memberCms.get("cms_card")+"";
			AesEncrypt ae=new AesEncrypt();
			cms_card=ae.decrypt(cms_card);
			
			//MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
			
			HostnameVerifier hv=new CardHV();
	    	OkHttpClient client = new OkHttpClient.Builder().hostnameVerifier(hv).build();
	    	
	    	String m_num=memberCms.get("m_num")+"";
	        Request req = new Request.Builder()
	        .url("https://api.efnc.co.kr/v1/members/"+m_num)
	        .get()
	        .addHeader("content-type", "application/json;charset=UTF-8")
	        .addHeader("Authorization", "VAN "+VAN+":"+cms_card)
	        .build();
	        
	        response = client.newCall(req).execute();
	        String str=response.body().string().toString();
	        JSONParser jp=new JSONParser();
	        JSONObject jo=(JSONObject)jp.parse(str);
	        //System.out.println(str);
	        
	        Map<String,Object> memberUp=new HashMap<String, Object>();
	        memberUp.put("m_idx", m_idx);
	        if(jo.containsKey("error")) {
	        	JSONObject errorObj=(JSONObject)jp.parse(jo.get("error")+"");
	        	String developerMessage=errorObj.get("developerMessage")+"";

	        	if(developerMessage.equals("해당 자원을 찾을 수 없습니다.")) {
	        		//회원등록안된경우
	        		memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
	        		//memberUp.put("m_cms_fail", "");
	        		memberDAO.updateMemberCms(memberUp);
	        		msg="미등록 회원입니다.";
	        	}else {
	        		msg="오류 발생.\n"+developerMessage;
	        	}
	        }else {
	        	JSONObject memberObj=(JSONObject)jp.parse(jo.get("member")+"");
	        	String status=memberObj.get("status")+"";//신청중,신청완료
	        	status=status.trim();
	        	//System.out.println(memberObj.entrySet());
	        	if(status.equals("신청완료")) {
	        		//회원등록된경우
	        		String m_cms=memberCms.get("m_cms")+"";//현db에 cms상태
	        		if(m_cms.equals(MemberInfo.CMS_CHANGE)) {
	        			//결제정보수정요청인경우 그대로 수정요청
	        			memberUp.put("m_cms", MemberInfo.CMS_CHANGE);
	        		}else {
	        			//등록된 경우 등록완료로 상태 변경
	        			memberUp.put("m_cms", MemberInfo.CMS_REGISTERD);
	        		}
	        		memberUp.put("m_cms_fail", "");
	        		
	        		String paymentKind=memberObj.get("paymentKind")+"";//CMS
	        		String paymentCompany=memberObj.get("paymentCompany")+"";
	        		String paymentNumber=memberObj.get("paymentNumber")+"";
	        		String payerName=memberObj.get("payerName")+"";
	        		if(paymentKind.equalsIgnoreCase("CMS")) {
	        			String sb_code="000"+Integer.parseInt(paymentCompany.trim());
	        			sb_code=sb_code.substring(sb_code.length()-2);
	        			msg="은행결제: "+payDAO.selectSubBankByCode(sb_code);
	        			msg+="\n계좌번호: "+paymentNumber;
	        			msg+="\n예금주명: "+payerName;
	        		}else {
	        			msg=paymentKind+": "+paymentCompany;
	        			msg+="\n카드번호: "+paymentNumber;
	        			msg+="\n카드소유주: "+payerName;
	        		}
	        	}else if(status.equals("신청중")) {
	        		if(!memberDAO.selectMemberDetail(m_idx).get("m_cms").equals(MemberInfo.CMS_REG_HASFILE)) {
	        			memberUp.put("m_cms", MemberInfo.CMS_REG_NEEDFILE);
	        		}
	        		msg="등록 신청 중인 회원입니다.";
	        	}else {
	        		JSONObject result=(JSONObject)jp.parse(memberObj.get("result")+"");
	        		msg=status+" 회원 입니다.\n"+result.get("message");
	        		memberUp.put("m_cms", MemberInfo.CMS_UNREGISTERD);
	        		memberUp.put("m_cms_fail", result.get("message"));
	        	}
	        	memberDAO.updateMemberCms(memberUp);
	        }
	    }catch (SSLPeerUnverifiedException spue) {
	    	Log.warn("cms회원 조회 중 hostName 다른 값 가져옴.");
	    	msg="잘못된 접근입니다.";
		} catch (Exception e) {
	    	e.printStackTrace();
	    	msg="오류 발생.";
	    }finally {
	    	if(response!=null) response.close();
	    }
		return msg;
	}
	
	/**가상계좌입금확인(최초확인)<br>
	 * 미수신은 null,날짜별은 date입력yyyyMMdd*/
	public String checkVPay(String date) {
		String msg="";
		String msg_tail="\n\n\n";
		List<Map<String,Object>> cmsList=cmsDAO.selectCmsList(null);
		Map<String,Integer> map_payCnt=new HashMap<String, Integer>();
		DecimalFormat df=new DecimalFormat("#,###,###,###");
		for(int i=0;i<cmsList.size();i++) {
			Map<String,Object> cmsMap=cmsList.get(i);
			String cms_id=(String)cmsMap.get("cms_id");
			
			BaseAction con=null;
			try {
				AesEncrypt ae=new AesEncrypt();
				String cms_pw=cmsDAO.selectCmsDetail(cms_id).get("cms_pw")+"";
				cms_pw=ae.decrypt(cms_pw);
				con=new XC_JavaSocket();
				
				//연결
				if(con.connect("121.134.74.90", 26000)<0){
					Log.warn(cms_id+"가상계좌 결과요청 연결실패");
					continue;
				}
				//시작전문
				String start="S";
				start+=writeData(cms_id, 8);
				start+=writeData(cms_pw, 8);
				start+=writeData(PROGRAM_ID, 8);
				start+=writeData(PROGRAM_PW, 8);
				start+=writeData("2000", 4);
				start=spaceData(start,135);
				con.sendData(start);
				byte[] recStart=con.recvData();
				if(recStart[37]=='N') {
					String errorCode=new String(recStart, 38, 34,"EUC-KR");
					errorCode=errorCode.trim();
					Log.warn(cms_id+"가상계좌 시작전문 오류 "+errorCode);
					con.close();
					continue;
				}
				
				String gongtong="R";
				gongtong+=writeData(cms_id, 8);
				gongtong+=writeData("2000", 4);
				gongtong+=writeData("100", 3);
				gongtong=spaceData(gongtong,80);
				
				if(date!=null) {
					date=date.replaceAll("[^0-9]", "");
					gongtong+="D";//날짜
					gongtong+=writeData(date, 8);
				}else {
					gongtong+="A";//미수신
				}
				gongtong=spaceData(gongtong,135);
				con.sendData(gongtong);
				
				byte[] recGongtong=con.recvData();
				if(recGongtong[16]=='N'){
					String errorCode=new String(recStart, 17, 34,"EUC-KR");
					errorCode=errorCode.trim();
					Log.warn(cms_id+"가상계좌 공통부분 전문 오류 "+errorCode);
					con.close();
					continue;
				}
				
				boolean bool_vData=true;
				while(bool_vData){
					byte[] b_data=con.recvData();
					if(b_data[0]=='H'){
						String cnt=new String(b_data, 16, 6,"EUC-KR");
						map_payCnt.put(cms_id, Integer.parseInt(cnt.replaceAll("[^0-9]", "")));
					}else if(b_data[0]=='D'){
						
						String m_num=new String(b_data, 16, 20,"EUC-KR");
						m_num=m_num.trim();
						
						String pay_date=new String(b_data, 37, 8,"EUC-KR");//yyyyMMdd
						pay_date=pay_date.substring(0, 4)+"-"+pay_date.substring(4, 6)+"-"+pay_date.substring(6, 8);
						
						String v_account=new String(b_data, 47, 15,"EUC-KR");
						v_account=v_account.trim();
						
						String pay_real=new String(b_data, 62, 12,"EUC-KR");
						pay_real=pay_real.replaceAll("[^0-9]", "");
						
						String pay_num=new String(b_data, 74, 10,"EUC-KR");
						pay_num=pay_num.trim();
						
						msg+=pay_date+" "+cms_id+" "+m_num+" "+df.format(Integer.parseInt(pay_real))+"원";
						
						Map<String,Object> map_v=vaccountDAO.selectVaccountDetail(v_account);
						int v_idx=Integer.parseInt(map_v.get("v_idx")+"");
						String v_condition=map_v.get("v_condition")+"";
						
						List<Map<String,Object>> paybigos=payDAO.selectPayNumList(v_account);
						if(b_data[36]=='D'){//입금
							msg+=" 입금";
							
							if(paybigos==null||paybigos.size()==0||map_v.get("v_reuse").equals(Vaccount.REUSE_TRUE)){
								//pay 테이블에 입금내역 적용
								int v_count=Integer.parseInt(map_v.get("v_count")+"");
								int m_idx=Integer.parseInt(map_v.get("v_m_idx")+"");
								List<Map<String,Object>> unpaidList=payDAO.selectUnpaidList(m_idx);
								
								int updatePayReal=(Integer.parseInt(pay_real) )/v_count;
								for(int vc=0;vc<v_count;vc++) {
									Map<String,Object> payUp=unpaidList.get(vc);
									payUp.put("pay_real", updatePayReal);
									payUp.put("pay_date", pay_date);
									payUp.put("pay_state", Pay.STATE_PAID);
									payUp.put("pay_reason", Pay.REASON_V_ACCOUNT);
									payUp.put("pay_bigo", cms_id);
									payUp.put("pay_method", Pay.METHOD_BANK);
									payUp.put("pay_num", v_account);
									payDAO.updatePay(payUp);
								}
							}
							//1회용 가상계좌 사용완료 처리
							if(map_v.get("v_reuse").equals(Vaccount.REUSE_FALSE)) {
								vaccountDAO.deleteV(v_idx);
							}
						}else if(b_data[36]=='C'){//입금취소
							msg+=" 입금취소";
							
							for(int pb=0;pb<paybigos.size();pb++) {
								Map<String,Object> payUp=paybigos.get(pb);
								payUp.put("pay_state", Pay.STATE_REFUND);
								payUp.put("pay_real", 0);
								payUp.put("pay_reason", Pay.REASON_V_ACCOUNT+" 출금취소");
								payDAO.updatePay(payUp);
								
								payUp.put("m_idx", payUp.get("pay_m_idx"));
								payUp.put("pay_date", "");
								payUp.put("pay_state", Pay.STATE_UNPAID);
								payUp.put("pay_reason", "");
								payUp.put("pay_bigo", "");
								payUp.put("pay_method", "");
								payUp.put("pay_num", "");
								payDAO.insertPay(payUp);
							}
							
						}else {
							//error
							msg+=b_data[36];
							Log.warn("가상계좌 입금확인 중 알 수없는 구분값"+new String(b_data,"EUC-KR"));
						}
						msg+=" 가상계좌:"+v_account+"\n";

					}else if(b_data[0]=='T'){
						String payed=new String(b_data, 22, 12,"EUC-KR");
						payed=payed.replaceAll("[^0-9]", "");
						msg_tail+=cms_id+" 입금 금액:"+df.format(Integer.parseInt(payed))+"원\n";
						String canceled=new String(b_data, 40, 12,"EUC-KR");
						canceled=canceled.replaceAll("[^0-9]", "");
						msg_tail+=cms_id+" 취소 금액:"+df.format(Integer.parseInt(payed))+"원\n\n";
						
						String tail=new String(b_data,"EUC-KR");
						tail="T"+cms_id+"2000"+tail.substring(13);
						con.sendData(tail);
						bool_vData=false;
					}
				}
				
				String end="E"+cms_id+"2000";
				end=spaceData(end, 135);
				con.sendData(end);
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				if(con!=null)con.close();
			}
		}
		
		return msg+msg_tail;
	}
	
	/**전문 작성 시 사용하는 메서드. data+스페이스 를 길이 만큼.<br>
	 * 전문+=writeDate(데이터,길이) 로 사용하면 됨*/
	public String writeData(String data,int length) {
		if(data==null) {
			data="";
		}
		try {
			//데이터 길이가 더 긴 경우 잘라주기
			if(data.getBytes("EUC-KR").length>length) {
				data=new String(data.getBytes("EUC-KR"),0,length);
			}
			//데이터 길이 짧은 경우 뒤에 스페이스로 맞춰 주기
			for(int i=data.getBytes("EUC-KR").length;i<length;i++) {
				data+=" ";
			}
		}catch (Exception e) {
			
		}
		return data;
	}
	/**전문 작성 시 사용하는 메서드. 공백채우기(EUCKR 인코딩 기준 공백)<br>
	 * 전문 길이 공백채우기 (총 size를 입력)<br>
	 * data=spaceData(data,사이즈)로 사용*/
	public String spaceData(String data,int totalLength) {
		try{
			for(int i=data.getBytes("EUC-KR").length;i<totalLength;i++) {
				data+=" ";
			}
		}catch (Exception e) {
			data="";
		}
		return data;
	}
	
	
	/**CMS회원 상태 조회*/
	public void searchCms(int m_idx) {
		
		List<Map<String,Object>> cmsList=cmsDAO.groupCmsId();
		Map<String,Object> member=memberDAO.selectMemberDetail(m_idx);
		
		Map<String,Object> cmsResult=new HashMap<String, Object>();
		for(int c=0;c<cmsList.size();c++) {
			
			Response response=null;
			Map<String,Object> cmsMap=cmsList.get(c);
			try {
				String cms_id=cmsMap.get("cms_id")+"";
				String cms_card=cmsMap.get("cms_card")+"";
				AesEncrypt ae=new AesEncrypt();
				cms_card=ae.decrypt(cms_card);
				
				//MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
				
				HostnameVerifier hv=new CardHV();
				OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).hostnameVerifier(hv).build();

				String m_num=member.get("m_num")+"";
				
				Request req = new Request.Builder()
						.url("https://api.efnc.co.kr/v1/members/"+m_num)
						.get()
						.addHeader("content-type", "application/json;charset=UTF-8")
						.addHeader("Authorization", "VAN "+VAN+":"+cms_card)
						.build();
				
				response = client.newCall(req).execute();
				String str=response.body().string().toString();
				JSONParser jp=new JSONParser();
				JSONObject jo=(JSONObject)jp.parse(str);
				//System.out.println(str);
				cmsResult.put(cms_id, str);
				Map<String,Object> memberCheck=new HashMap<String, Object>();
				if(jo.containsKey("error")) {
					JSONObject errorObj=(JSONObject)jp.parse(jo.get("error")+"");
					String developerMessage=errorObj.get("developerMessage")+"";
					
					if(developerMessage.equals("해당 자원을 찾을 수 없습니다.")) {
						
					}else {
						
					}
				}else {
					JSONObject memberObj=(JSONObject)jp.parse(jo.get("member")+"");
					String status=memberObj.get("status")+"";//신청중,신청완료
					status=status.trim();
					//System.out.println(memberObj.entrySet());
					if(status.equals("신청완료")) {
						Log.warn(m_num+"/"+cms_id+"/"+str);
					}else if(status.equals("신청중")) {
						
					}else {
						
					}
				}
			}catch (SSLPeerUnverifiedException spue) {
				Log.warn("cms회원 조회 중 hostName 다른 값 가져옴.");
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				if(response!=null) response.close();
			}
			
		}
		
		System.out.println(cmsResult.entrySet());
	}
	
}
