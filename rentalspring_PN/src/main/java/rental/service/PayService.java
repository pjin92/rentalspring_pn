package rental.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import rental.common.Log;
import rental.constant.Cms;
import rental.constant.MemberInfo;
import rental.constant.Pay;
import rental.dao.CmsDAO;
import rental.dao.CommonDAO;
import rental.dao.MemberDAO;
import rental.dao.PayDAO;

@Service("payService")
public class PayService {

	@Autowired
	PayDAO payDAO;
	@Autowired
	MemberDAO memberDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	CmsDAO cmsDAO;

	/**m_idx로 pay count*/
	public int countPayByMidx(int m_idx) {
		return payDAO.countPayByMidx(m_idx);
	}
	
	/**최종계약확정시 member에 최종계약일자 update + pay insert*/
	@Transactional
	synchronized public String insertPay(int m_idx) {
		try{
			Map<String,Object>member=memberDAO.selectMemberDetail(m_idx);
			if(member==null||member.isEmpty()) {
				return "회원 정보가 없습니다.";
			}
			String m_final_date=(String)member.get("m_final_date");
			if(!m_final_date.equals("")) {
				return "이미 최종계약확정되었습니다.";
			}
			if(payDAO.selectPayListByMidx(m_idx,-1,-1).size()>0) {
				return "이미 결제 데이터가 있습니다. 관리자에게 연락하세요.";
			}
			Map<String,Object>map=new HashMap<String, Object>();
			map.put("m_idx", m_idx);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			map.put("m_final_date", sdf.format(Calendar.getInstance().getTime()));
			int result=memberDAO.updateMember(map);
			if(result<1) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
			
			
			//pay공통정보
			map.put("pay_cnt2", 1);
			map.put("pay_money", Integer.parseInt(member.get("m_rental")+""));
			map.put("pay_date", "");
			map.put("pay_state", Pay.STATE_UNPAID);
			map.put("pay_carryover_date", "");
			map.put("pay_reason", "");
			map.put("pay_bigo", "");
			map.put("pay_num", "");
			int m_period=Integer.parseInt(member.get("m_period")+"");
			
			Calendar payDate=Calendar.getInstance();
			String m_paystart=(String)member.get("m_paystart")+"-"+(String)member.get("m_paydate");;
			payDate.setTime(sdf.parse(m_paystart));
			
			int m_paydate=Integer.parseInt(member.get("m_paydate")+"");
			for(int i=1;i<=m_period;i++) {
				//pay개별 정보
				map.put("pay_cnt", i);
				map.put("pay_year", payDate.get(Calendar.YEAR));
				map.put("pay_month", payDate.get(Calendar.MONTH)+1);
				
				payDate.set(Calendar.DAY_OF_MONTH, m_paydate);
				switch (payDate.get(Calendar.DAY_OF_WEEK)) {
					case 1:payDate.add(Calendar.DATE, 1);break;
					case 7:payDate.add(Calendar.DATE, 2);break;
					default:break;
				}
				
				map.put("pay_original_date", sdf.format(payDate.getTime()));
				
				int insertPay=payDAO.insertPay(map);
				if(insertPay<1) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return "결제 정보 입력 중 에러발생";
				}
				payDate.add(Calendar.MONTH, 1);
			}
			return "최종계약확정했습니다.";
		}catch(NumberFormatException ne) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return "결제정보(렌탈료,결제일,렌탈기간) 확인 부탁드립니다.";
		}catch(ParseException pe) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return "결제시작일 지정 부탁드립니다.";
		}catch (Exception e) {
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return "오류 발생.";
		}
	}
	
	/**은행회원등록신청 리스트<br>
	 * start,length를 사용.*/
	public List<Map<String,Object>> selectPaySendStatistic(Map<String,Object>map) {
		return payDAO.selectPaySendStatistic(map);
	}
	
	/**은행출금신청 리스트<br>
	 * start,length를 사용. ps_date,total,bef,ing,suc,fail*/
	public List<Map<String,Object>> selectPayStatistic(Map<String,Object>map) {
		return payDAO.selectPayStatistic(map);
	}
	
	/**은행등록신청한 날짜count*/
	public int countPaySendStatistic() {
		return payDAO.countPaySendStatistic();
	}
	
	/**은행출금신청한 날짜count*/
	public int countPayStatistic() {
		return payDAO.countPayStatistic();
	}
	
	/**은행회원 전문송신할 바구니에 담기 (등록,수정)*/
	@Transactional(isolation=Isolation.SERIALIZABLE)
	synchronized public String regiPaySend(Map<String,Object>map) {
		int result=0;
		int cnt=0;
		String msg="";
		try{
			String type=map.get("type")+"";//미등록 or 등록완료
			StringTokenizer m_idx=new StringTokenizer(map.get("m_idx")+"", ",");
			int pay_idx=Integer.parseInt(map.get("pay_idx")+"");
			int cms_idx=-1;
			String ps_date=map.get("ps_date")+"";
			if(type.equals(MemberInfo.CMS_UNREGISTERD)) {
				cms_idx=Integer.parseInt(map.get("cms_idx")+"");
			}else {
				//수정인경우 날짜, cms_idx는 아래에서 개별로
				Calendar now=Calendar.getInstance();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				if(now.get(Calendar.HOUR_OF_DAY)>=Pay.SEND_REGI_TIME) {
					now.add(Calendar.DATE, 1);
					//주말 넘어가기
					int addDate=now.get(Calendar.DAY_OF_WEEK);
					if(addDate==7) {
						now.add(Calendar.DATE, 2);
					}else if(addDate==1) {
						now.add(Calendar.DATE, 1);
					}
				}
				ps_date=sdf.format(now.getTime());
			}
			
			while(m_idx.hasMoreElements()) {
				cnt++;
				
				int midx=Integer.parseInt(m_idx.nextToken());
				if(payDAO.countPaySendState(midx)>0) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return "이미 신청전,신청중인 상태의 회원이 있습니다.";
				}
				//미등록상태만 등록,등록완료 상태만 수정 신청 가능하도록 체크
				Map<String,Object> member=memberDAO.selectMemberDetail(midx);
				String m_cms=(String)member.get("m_cms");
				if(!m_cms.equals(type)) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return type+" 상태가 아닌 회원이 있습니다.";//member.get("m_num");
				}
				
				if(type.equals(MemberInfo.CMS_CHANGE)) {
					Map<String,Object> mc=cmsDAO.selectCmsByMidx(midx);
					try{
						cms_idx=Integer.parseInt(mc.get("cms_idx")+"");
					}catch (Exception ecms) {
						Log.warn(ecms.toString()+" selectCmsByMidx("+midx+"Integer.parseInt(mc.get(cms_idx))");
						return "cms 등록 정보가 없습니다.";
					}
				}else {
					//cms_real에 insert
					Map<String,Object> cmsRealMap=new HashMap<String, Object>();
					cmsRealMap.put("m_idx", midx);
					cmsRealMap.put("cms_idx", cms_idx);
					if(cmsDAO.updateCmsReal(cmsRealMap)<1) {
						cmsRealMap.put("cr_type", Cms.CR_TYPE_BANK);
						cmsDAO.insertCmsReal(cmsRealMap);
					}
				}
				
				member.put("pay_idx", pay_idx);
				member.put("cms_idx",cms_idx);
				member.put("ps_date",ps_date);
				member.put("ps_reason", "");
				member.put("ps_state",Pay.SEND_STATE_BEFORE);
				result+=payDAO.insertPaySend(member);
				
			}
			msg="신청목록에 등록완료";
		}catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			e.printStackTrace();
			msg="오류발생";
		}
		return msg;
	}
	
	/**은행CMS 신청한 목록 가져오기(날짜 기준)<br>
	 * type==null인 경우 출금. not null인 경우 등록,수정 데이터*/
	public List<Map<String,Object>> selectPaySendByDate(Map<String,Object>map,String ps_date,String type) {
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		
		return payDAO.selectPaySendByDate(map,ps_date,type);
	}
	/**은행CMS 신청한 count가져오기(날짜 기준)<br>
	 * type==null인 경우 출금. not null인 경우 등록,수정 데이터*/
	public int countPaySendByDate(String ps_date,String type) {
		return payDAO.countPaySendByDate(ps_date, type);
	}
	
	/**pay에서 신청전,신청중 상태 개수 조회*/
	public int countUnpaid(int m_idx) {
		return payDAO.countUnpaid(m_idx);
	}
	
	/**은행회원 등록,출금 바구니에서 빼기<br>
	 * ps_idx,ps_idx,ps_idx로 delete. ps_state==신청전 인경우에만*/
	@Transactional(isolation=Isolation.SERIALIZABLE)
	public String deletePaySend(String ps_idxs) {
		String msg="";
		try{
			StringTokenizer st=new StringTokenizer(ps_idxs, ",");
			int cnt=st.countTokens();
			int result=0;
			while(st.hasMoreTokens()) {
				int ps_idx=Integer.parseInt(st.nextToken());
				Map<String,Object> ps=payDAO.selectPaySendDetail(ps_idx);
				int ps_pay_idx=Integer.parseInt(ps.get("ps_pay_idx")+"");
				int delPs=payDAO.deletePaySend(ps_idx);
				if(ps_pay_idx>0&&delPs>0) {
					//출금신청인 경우. 신청전일 때만 delPs>0 
					Map<String,Object> map=new HashMap<String, Object>();
					map.put("pay_idx", ps_pay_idx);
					map.put("pay_reason","");
					map.put("pay_num","");
					map.put("pay_date", "");
					map.put("pay_state", Pay.STATE_UNPAID);
					payDAO.updatePay(map);
				}
				result+=delPs;
			}
			if(cnt==result) {
				msg="신청목록에서 삭제완료.";
			}else {
				msg="신청전 상태에서만 가능합니다.";
			}
		}catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			msg="수행중 오류발생";
		}
		return msg;
	}
	/**pay_year 최소,최대값. key= min,max*/
	public Map<String,Object> payYearRange(){
		return payDAO.payYearRange();
	}
	/**pay List*/
	public List<Map<String,Object>> selectPayList(Map<String,Object> map){
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(order.equals("pay_original")) {
			
		}else if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		return payDAO.selectPayList(map);
	}
	/**count pay List (dt용)*/
	public int countPayList(Map<String,Object> map) {
		return payDAO.countPayList(map);
	}
	
	/**pay_idx로 업데이트*/
	public void updatePay(Map<String,Object> map) {
		payDAO.updatePay(map);
	}
	
	/**pay_idx로 조회.*/
	public Map<String,Object> selectPayDetail(int pay_idx) {
		Map<String,Object> map=payDAO.selectPayDetail(pay_idx);
		map.remove("cms_id");
		map.remove("cms_pw");
		map.remove("cms_card");
		return map;
	}
	
	/**은행 출금 신청 전문송신할 바구니에 담기<br>
	 * pay_date, ( {pay_idx},{pay_idx} )*/
	@Transactional(isolation=Isolation.SERIALIZABLE)
	synchronized public String paySend(String pay_date,String pay_idxs) {
		String msg="";
		int result=0;
		int cnt=0;
		try{
			StringTokenizer st=new StringTokenizer(pay_idxs, ",");
			while(st.hasMoreTokens()){
				cnt++;
				int pay_idx=Integer.parseInt(st.nextToken());
				Map<String,Object> pay=payDAO.selectPayDetail(pay_idx);
				String pay_state=pay.get("pay_state")+"";
				if(pay_state.equals(Pay.STATE_UNPAID)&&payDAO.countPaySendByPayIdx(pay_idx)==0) {
					pay.put("m_idx", pay.get("pay_m_idx"));
					if(!pay.get("m_pay_method").equals(Pay.SEND_METHOD_BANK)) {
						continue;
					}
					pay.put("m_rental", pay.get("pay_money"));
					pay.put("ps_date", pay_date);
					pay.put("ps_reason", "");
					pay.put("ps_state", Pay.SEND_STATE_BEFORE);
					payDAO.insertPaySend(pay);
					
					Map<String,Object> payUp=new HashMap<String, Object>();
					payUp.put("pay_idx", pay_idx);
					payUp.put("pay_date", pay_date);
					payUp.put("pay_state", Pay.STATE_ON_PROCESS);
					payUp.put("pay_reason", "");
					payUp.put("pay_bigo", "");
					payUp.put("pay_num", "");
					payDAO.updatePay(payUp);
					result++;
				}
			}
			msg="일괄 출금 신청 목록에 업데이트 됐습니다.";
			if(cnt>result) {
				msg+="\n신청전 상태만 처리 가능합니다.";
			}
		}catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			e.printStackTrace();
			msg="오류발생";
		}
		return msg;
	}
	
	/**sub_bank list*/
	public List<Map<String,Object>> selectSubBankList() {
		return payDAO.selectSubBankList();
	}
	/**sub_card list . 통일성 위해 sb_name에도 카드 명 담음*/
	public List<Map<String,Object>> selectSubCardList() {
		return payDAO.selectSubCardList();
	}
	
	/**채권관리 데이터 조회*/
	public List<Map<String,Object>> withdrawStatistic(Map<String,Object> map){
		if(map!=null) {
			String pr_idx=(String)map.get("pr_idx");
			if(pr_idx==null||pr_idx.equals("")) {
				map.remove("pr_idx");
			}
			String e_department=(String)map.get("e_department");
			if(e_department==null||e_department.equals("전체")) {
				map.remove("e_department");
			}
			String e_name=(String)map.get("e_name");
			if(e_name==null||e_name.trim().equals("")) {
				map.remove("e_name");
			}
			String mi_name=(String)map.get("mi_name");
			if(mi_name==null||mi_name.trim().equals("")) {
				map.remove("mi_name");
			}
			String keyword=(String)map.get("keyword");
			if(keyword==null||keyword.equals("%%")) {
				map.remove("keyword");
			}
			
			
			Object oc=map.get("order[0][column]");
			String order=map.get("columns["+oc+"][data]")+"";
			String asc=map.get("order[0][dir]")+"";
			if(order.equals("delay")){
				
			}else if(order.equals("m_total_real_f")||order.equals("unpaidMoney_f")||order.equals("delayMoney_f")
					||order.equals("m_rental_f")||order.equals("paidMoney_f")) {
				order=order.substring(0, order.length()-2);
			}else if(order.equals("midx")) {
				order="member.m_idx";
			}else if(commonDAO.checkColumn(order)==0) {
				return null;
			}
			map.put("column", order);
			
			if(asc.equals("asc")) {
				map.put("order", "asc");
			}else {
				map.put("order", "desc");
			}
		}
		return payDAO.withdrawStatistic(map);
	}
	
	/**pay의 고객 수 count(연체관리 검색조건)*/
	public int countWithdrawStatistic(Map<String,Object> map){
		return payDAO.countWithdrawStatistic(map);
	}
	
	/**m_idx로 pay조회.length 마이너스1이면 전부*/
	public List<Map<String,Object>> selectPayListByMidx(int m_idx,int start,int length){
		return payDAO.selectPayListByMidx(m_idx,start,length);
	}
	
	/**m_idx로 각 pay_cnt의 max(pay_cnt2)의 기록만 가져오기 (통계용)*/
	public List<Map<String,Object>> selectPayListWithMaxCnt2(int m_idx){
		return payDAO.selectPayListWithMaxCnt2(m_idx);
	}
	
	/**출금완료건 회차 변경.<br>
	 * map에는 pay_idx,pay_cnt(변경될 cnt),login정보*/
	@Transactional
	public int changeCnt(String m_idx_str,Map<String,Object> map) {
		//pay_send 수정 안하고 pay의 pay_cnt 등등 수정
		int result;
		try {
			int pay_idx=Integer.parseInt(map.get("pay_idx")+"");
			int m_idx=Integer.parseInt(m_idx_str);
			int pay_cnt=Integer.parseInt(map.get("pay_cnt")+"");//선택된 회차(바뀌어야되는 회차)
			Map<String,Object> payMap=payDAO.selectPayDetail(pay_idx);
			if(!payMap.get("pay_state").equals(Pay.STATE_PAID)) {
				return -1;//출금완료 상태인 회차만 변경이 가능합니다.
			}
			
			Map<String,Object> selectedPay=payDAO.selectPayCntRecent(m_idx, pay_cnt);//선택된 회차의 정보
			if(!selectedPay.get("pay_state").equals(Pay.STATE_UNPAID)) {
				return -2;//신청전의 회차로만 변경이 가능합니다.
			}
			if(!payMap.get("pay_money").equals(selectedPay.get("pay_money"))) {
				return -3;//출금 렌탈금액이 같은 건만 회차 변경이 가능합니다.
			}
			
			Map<String, Object> paid=new HashMap<String, Object>();
			paid.put("pay_idx", pay_idx);
			paid.put("pay_cnt", selectedPay.get("pay_cnt"));
			paid.put("pay_cnt2", selectedPay.get("pay_cnt2"));
			paid.put("pay_year", selectedPay.get("pay_year"));
			paid.put("pay_month", selectedPay.get("pay_month"));
			paid.put("pay_money", selectedPay.get("pay_money"));
			paid.put("pay_real", selectedPay.get("pay_real"));
			payDAO.updatePay(paid);
			
			selectedPay.put("pay_cnt", payMap.get("pay_cnt"));
			selectedPay.put("pay_cnt2", payMap.get("pay_cnt2"));
			selectedPay.put("pay_year", payMap.get("pay_year"));
			selectedPay.put("pay_month", payMap.get("pay_month"));
			selectedPay.put("pay_money", payMap.get("pay_money"));
			selectedPay.put("pay_real", payMap.get("pay_real"));
			payDAO.updatePay(selectedPay);
			
			result=1;//변경완료
			Log.warn("회차 변경/m_idx="+m_idx_str+"/"+map.entrySet());
		}catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			Log.warn("회차 변경중 오류 발생"+e.toString()+"/"+map.entrySet());
			result=-100;
		}
		
		return result;
	}
	
	/**결제시작일 변경.<br>
	 * m_idx,pay_year,pay_month,m_paydate(ex 05),login정보*/
	@Transactional
	public int changePayStart(String m_idx_str,Map<String,Object> map) {
		int result;
		try {
			int m_idx=Integer.parseInt(m_idx_str);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Calendar payStart=Calendar.getInstance();
			payStart.set(Calendar.YEAR, Integer.parseInt(map.get("pay_year")+""));
			payStart.set(Calendar.MONTH, (Integer.parseInt(map.get("pay_month")+""))-1);
			payStart.set(Calendar.DATE, Integer.parseInt(map.get("m_paydate")+""));
			
			String original=memberDAO.selectMemberDetail(m_idx).get("m_paystart")+"";
			Map<String,Object> member=new HashMap<String, Object>();
			member.put("m_idx", m_idx);
			member.put("m_paystart", sdf.format(payStart.getTime()));
			memberDAO.updateMember(member);
			
			map.put("m_idx", m_idx);
			payDAO.updatePayStart(map);
			
			result=1;//변경완료
			Log.warn("결제시작일 변경/m_idx="+m_idx_str+"/원래 결제일"+original+"/"+map.entrySet());
		}catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			Log.warn("결제시작일 변경중 오류 발생"+e.toString()+"/"+map.entrySet());
			result=-100;
		}
		
		return result;
	}
	
	/**m_idx로 연체금 가져오는 메서드(연체관리의 dt쿼리에서 1건만 가져오도록 수정한 쿼리)*/
	public Map<String,Object> selectMemberDelayMoney(int m_idx){
		return payDAO.selectMemberDelayMoney(m_idx);
	}
	/**m_idx로 pay_original_date 가장 빠른날짜 조회(신청전 상태만).최초 연체일자 확인에 사용*/
	public String selectPayDelayDate(int m_idx) {
		return payDAO.selectPayDelayDate(m_idx);
	}
	
	/**은행 sms전송하기위한 데이터 조회*/
	public List<Map<String,Object>> selectPaySendSmsList(String ps_date){
		return payDAO.selectPaySendSmsList(ps_date);
	}
	
	/**반품,해지 등 pay_state 신청전만 상태값 변경하는 메서드*/
	public void noMoreRental(int m_idx) {
		try{
			Map<String,Object> member=memberDAO.selectMemberDetail(m_idx);
			//상태값 따라서 pay_state변경 (회수,해지,반품 등의 경우)
			String m_process=member.get("m_process")+"";
			String m_detail=member.get("m_detail")+"";
			String pay_state="";
			if(m_process.equals(MemberInfo.PROCESS_EXIT)) {
				pay_state=Pay.STATE_EXIT;
			}else if(m_process.equals(MemberInfo.PROCESS_HAEBAN)) {
				pay_state=Pay.STATE_HAEBAN;
			}else if(m_process.equals(MemberInfo.PROCESS_RETURN)) {
				pay_state=Pay.STATE_RETURN;
			}else if(m_detail.equals(MemberInfo.DETAIL_ILSI)) {
				pay_state=Pay.STATE_ILSI;
			}
			//pay_state 변경
			if(!pay_state.equals("")) {
				if(payDAO.countOnProcess(m_idx)==0) {
					payDAO.noMoreRental(m_idx, pay_state);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**이체 희망일 변경에 따른 pay.pay_original_date 변경*/
	public void updatePayOriginalDate(int m_idx,String m_paydate) {
		payDAO.updatePayOriginalDate(m_idx,m_paydate);
	}
	
}
