package rental.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import rental.common.Log;
import rental.constant.MemberInfo;
import rental.dao.CommonDAO;
import rental.dao.MemberDAO;

/**상수 list 등등...가져오는 서비스*/
@Service("commonService")
public class CommonService {
	@Autowired
	CommonDAO commonDAO;
	
	/**m_state 상담상태 list 가져오기*/
	public List<String> mstateList() {
		return commonDAO.mstateList();
	}
	/**m_jubsoo 접수구분 list 가져오기*/
	public List<String> mjubsooList() {
		return commonDAO.mjubsooList();
	}
	/**m_detail 매출구분 접수 */
	public List<String> mdetailList() {
		return commonDAO.mdetailList();
	}
	
	/**lsr_status 재고요청 */
	public List<String> lsr_statusList() {
		return commonDAO.lsr_statusList();
	}
	
	/**mprocess 진행상태 list 가져오기*/
	public List<String> mprocessList(){
		return commonDAO.mprocessList();
	}
}
