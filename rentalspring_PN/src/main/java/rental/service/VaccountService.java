package rental.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import comm_module.BaseAction;
import comm_module.XC_JavaSocket;
import rental.common.AesEncrypt;
import rental.common.Log;
import rental.constant.Pay;
import rental.constant.Vaccount;
import rental.dao.CmsDAO;
import rental.dao.CommonDAO;
import rental.dao.MemberDAO;
import rental.dao.VaccountDAO;

@Service("vaccountService")
public class VaccountService {

	@Autowired
	VaccountDAO vaccountDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	CmsDAO cmsDAO;
	@Autowired
	MemberDAO memberDAO;

	/**sdsi 효성cms 정보*/
	final private String PROGRAM_ID="gnrent99";
	/**sdsi 효성cms 정보*/
	final private String PROGRAM_PW="99gnrent";
	
	
	/**#{v_condition},#{v_money},#{v_count},#{v_regi_date}
	,#{v_end_date},#{v_reuse},#{v_account},#{v_name},#{v_bank}
	,#{m_idx}*/
	public int insertV(Map<String,Object>map) {
		return vaccountDAO.insertV(map);
	}
	
	/**m_idx로 사용중인 가상계좌 조회*/
	public List<Map<String,Object>> selectVlist(int m_idx){
		return vaccountDAO.selectVlist(m_idx);
	}
	
	/**m_idx,은행코드로 사용중인 가상계좌 조회*/
	public Map<String,Object> selectVdetail(int m_idx,String v_bank){
		return vaccountDAO.selectVdetail(m_idx, v_bank);
	}
	
	/**가상계좌 등록*/
	public String regiVaccount(int m_idx,Map<String,Object> map) {
		String msg="";
		BaseAction con=null;
		try {
			map.put("m_idx", m_idx);
			
			String junmoon="1000";//전문구분
			String georae="";//거래구분
			
			String v_bank=(String)map.get("v_bank");
			String v_money=(String)map.get("v_money");
			if(v_money==null) {
				v_money="0";
			}
			v_money=v_money.replaceAll("[^0-9]", "");
			map.put("v_money", Integer.parseInt(v_money));
			
			if(v_bank==null||v_bank.trim().equals("")) {
				return "가상계좌은행을 선택해주세요.";
			}
			if(v_money.equals("0")) {
				return "입금 금액 확인 부탁드립니다.";
			}

			String v_end_date=(String)map.get("v_end_date");
			v_end_date=v_end_date.replaceAll("[^0-9]", "");
			map.put("v_end_date", v_end_date );
			
			if(vaccountDAO.selectVdetail(m_idx, v_bank)!=null) {
				return "이미 등록된 가상계좌가 있습니다.";
			}
			int cms_idx=Integer.parseInt(map.get("v_cms_idx")+"");
			Map<String,Object> cms=cmsDAO.selectCmsDetail(cms_idx);
			if(cms==null) {
				return "입금은행을 선택해주세요.";
			}
			String cms_id=cms.get("cms_id")+"";
			map.put("cms_idx", cms_idx);
			
			AesEncrypt ae=new AesEncrypt();
			String cms_pw=ae.decrypt(cms.get("cms_pw")+"");
			
			Map<String,Object> member=memberDAO.selectMemberDetail(m_idx);
			
			con=new XC_JavaSocket();
			
			//연결
			if(con.connect("121.134.74.90", 26000)<0){
				Log.warn(cms_id+" 가상계좌 등록 소켓 접속실패");
				return "통신 실패";
			}
			
			//시작전문
			String start="S";
			start+=writeData(cms_id, 8);
			start+=writeData(cms_pw, 8);
			start+=writeData(PROGRAM_ID, 8);
			start+=writeData(PROGRAM_PW, 8);
			start+=junmoon;//회원,입금
			start=spaceData(start, 135);
			
			con.sendData(start);
			byte[] rec=con.recvData();
			
			if(rec[37]=='N') {
				//System.out.println(start);
				
				String v_error=new String(rec,38,34,"EUC-KR");
				Log.warn("가상계좌 등록 시작전문 오류."+m_idx+"/"+v_bank+v_error);
				return "시작전문오류. "+v_error;
			}
			
			//요청공통부
			String reqStart="R";
			reqStart+=writeData(cms_id, 8);
			reqStart+=junmoon;//회원,입금
			
			int v_reuse=Integer.parseInt(map.get("v_reuse")+"");
			if(v_reuse==Vaccount.REUSE_FALSE) {
				georae="110";
			}else {
				georae="100";
			}
			reqStart+=georae;
			reqStart=spaceData(reqStart, 80);
			//요청개별부
			reqStart+=writeData(member.get("m_num")+"",20);
			reqStart+=v_bank;
			reqStart+=writeData(member.get("m_pay_owner")+"",20);
			reqStart+="Y";//금액체크
			reqStart+=writeData(map.get("v_money")+"",12);
			
			//1회용 가상계좌 개별부 (추가부분)
			if(v_reuse==Vaccount.REUSE_FALSE) {
				reqStart+="Y";
				reqStart+=writeData(v_end_date , 8);
				
				//현금영수증
				String v_receipt=(String)map.get("v_receipt");
				v_receipt=v_receipt.replaceAll("[^0-9]", "");
				if(v_receipt.equals("")) {
					reqStart+="N";
					reqStart=spaceData(reqStart, 165);
				}else {
					reqStart+="Y";
					reqStart+=writeData(v_receipt, 20);
				}
			}
			//System.out.println("reqStart:"+reqStart);
			con.sendData(reqStart);
			byte[] recStart=con.recvData();
			
			if(recStart[16]=='N'){
				String errorCode=new String(recStart, 17,34,"EUC-KR");
				Log.warn("가상계좌 등록 요청 오류."+m_idx+"/"+v_bank+errorCode);
				return errorCode;
			}

			//6,7,8,9
			do{
				byte[] b=con.recvData();
				//System.out.println("b="+new String(b));
				if(b[0]=='D'){
					String v_account=new String(b,58,15,"EUC-KR");
					map.put("v_account", v_account.trim());
				}
				
				if(b[0]=='T'){
					String tail="T";
					tail+=writeData(cms_id, 8);
					tail+=junmoon;
					tail+=georae;
					tail+=writeData("1", 6);
					tail=spaceData(tail, 118);
					tail+="\r\n";
					con.sendData(tail);
					break;
				}
			}while(true);


			//10
			String end="E";
			end+=writeData(cms_id,8);
			end+=junmoon;
			end=spaceData(end, 135);
			//System.out.println("end="+end);
			con.sendData(end);
			con.close();
			
			map.put("v_condition", Vaccount.CONDITION_LIVE);
			vaccountDAO.insertV(map);
			msg="가상계좌 발급 완료";
		}catch (Exception e) {
			msg="오류발생.";
			Log.warn("가상계좌 발급 오류/"+e.toString());
		}finally {
			if(con!=null)con.close();
		}
		
		return msg;
	}
	
	/**가상계좌 삭제*/
	public String delVaccount(int m_idx,Map<String,Object> map) {
		String msg="";
		BaseAction con=null;
		try {
			int v_idx=Integer.parseInt(map.get("v_idx")+"");
			map.put("m_idx", m_idx);
			
			String junmoon="1000";//전문구분
			String georae="200";//거래구분
			
			String v_bank=(String)map.get("v_bank");
			if(v_bank==null||v_bank.trim().equals("")) {
				return "가상계좌은행을 선택해주세요.";
			}
			
			int cms_idx=Integer.parseInt(map.get("v_cms_idx")+"");
			Map<String,Object> cms=cmsDAO.selectCmsDetail(cms_idx);
			if(cms==null) {
				return "입금은행을 선택해주세요.";
			}
			String cms_id=cms.get("cms_id")+"";
			
			Map<String,Object> member=memberDAO.selectMemberDetail(m_idx);
			
			AesEncrypt ae=new AesEncrypt();
			String cms_pw=ae.decrypt(cms.get("cms_pw")+"");
			
			con=new XC_JavaSocket();
			
			//연결
			if(con.connect("121.134.74.90", 26000)<0){
				Log.warn(cms_id+" 가상계좌 등록 소켓 접속실패");
				return "통신 실패";
			}
			
			//시작전문
			String start="S";
			start+=writeData(cms_id, 8);
			start+=writeData(cms_pw, 8);
			start+=writeData(PROGRAM_ID, 8);
			start+=writeData(PROGRAM_PW, 8);
			start+=junmoon;//회원,입금
			start=spaceData(start, 135);
			
			con.sendData(start);
			byte[] rec=con.recvData();
			
			if(rec[37]=='N') {
				String v_error=new String(rec,38,34,"EUC-KR");
				Log.warn("가상계좌 삭제 시작전문 오류."+m_idx+"/"+v_bank+v_error);
				return "시작전문오류. "+v_error;
			}
			
			//요청공통부
			String reqStart="R";
			reqStart+=writeData(cms_id, 8);
			reqStart+=junmoon;//회원,입금
			reqStart+=georae;
			reqStart=spaceData(reqStart, 80);
			//요청개별부
			reqStart+=writeData(member.get("m_num")+"",20);
			reqStart+=v_bank;
			reqStart=spaceData(reqStart, 135);
			con.sendData(reqStart);
			
			byte[] recStart=con.recvData();
			
			if(recStart[16]=='N'){
				String errorCode=new String(recStart, 17,34,"EUC-KR");
				if(errorCode.trim().equals("MD08미등록회원번호")) {
					vaccountDAO.deleteV(v_idx);
					errorCode="이미 삭제된 가상계좌입니다.";
				}else {
					Log.warn("가상계좌 삭제 요청 오류."+m_idx+"/"+v_bank+errorCode);
				}
				return errorCode;
			}

			String end="E";
			end+=writeData(cms_id,8);
			end+=junmoon;
			end=spaceData(end, 135);
			con.sendData(end);
			con.close();
			
			vaccountDAO.deleteV(v_idx);
			msg="가상계좌 삭제 완료";
		}catch (Exception e) {
			msg="오류발생.";
			Log.warn("가상계좌 삭제 오류/"+e.toString());
		}finally {
			if(con!=null)con.close();
		}
		
		return msg;
	}
	
	/**전문 작성 시 사용하는 메서드. data+스페이스 를 길이 만큼.<br>
	 * 전문+=writeDate(데이터,길이) 로 사용하면 됨*/
	private String writeData(String data,int length) {
		if(data==null) {
			data="";
		}
		try {
			//데이터 길이가 더 긴 경우 잘라주기
			if(data.getBytes("EUC-KR").length>length) {
				data=new String(data.getBytes("EUC-KR"),0,length);
			}
			//데이터 길이 짧은 경우 뒤에 스페이스로 맞춰 주기
			for(int i=data.getBytes("EUC-KR").length;i<length;i++) {
				data+=" ";
			}
		}catch (Exception e) {
			
		}
		return data;
	}
	/**전문 작성 시 사용하는 메서드. 공백채우기(EUCKR 인코딩 기준 공백)<br>
	 * 전문 길이 공백채우기 (총 size를 입력)<br>
	 * data=spaceData(data,사이즈)로 사용*/
	private String spaceData(String data,int totalLength) {
		try{
			for(int i=data.getBytes("EUC-KR").length;i<totalLength;i++) {
				data+=" ";
			}
		}catch (Exception e) {
			data="";
		}
		return data;
	}
	
	/**채권관리>가상계좌관리 데이터 조회(dataTable)*/
	public List<Map<String,Object>> withdrawVaccount(Map<String,Object> map){
		if(map!=null) {
			Object oc=map.get("order[0][column]");
			String order=map.get("columns["+oc+"][data]")+"";
			String asc=map.get("order[0][dir]")+"";
			if(order.equals("v_money_f")) {
				order=order.substring(0, order.length()-2);
			}else if(commonDAO.checkColumn(order)==0) {
				return null;
			}
			map.put("column", order);
			
			if(asc.equals("asc")) {
				map.put("order", "asc");
			}else {
				map.put("order", "desc");
			}
		}
		
		return vaccountDAO.withdrawVaccount(map);
	}
	/**가상계좌관리 고객 수 count(연체관리 검색조건)*/
	public int countwithdrawVaccount(Map<String,Object> map){
		return vaccountDAO.countWithdrawVaccount(map);
	}
	
}
