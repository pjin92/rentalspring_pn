package rental.service;

import java.io.File;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.MemberFile;
import rental.constant.MemberInfo;
import rental.constant.Pay;
import rental.constant.ProductRental;
import rental.dao.CommonDAO;
import rental.dao.DeliveryDAO;
import rental.dao.EmployeeDAO;
import rental.dao.MemberDAO;
import rental.dao.PayDAO;
import rental.dao.ProductDAO;

@Service("memberService")
public class MemberService {
	@Autowired
	MemberDAO memberDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	EmployeeDAO employeeDAO;
	@Autowired
	ProductDAO productDAO;
	@Autowired
	PayDAO payDAO;
	@Autowired
	DeliveryDAO deliveryDAO;
	
	/**상담 고객 신규생성*/
	@Transactional
	public String insertMember(Map<String,Object> map){
		////데이터 유효 체크
		String m_total=map.get("m_total")+"";
		String m_rental=map.get("m_rental")+"";
		String m_period=map.get("m_period")+"";
		String m_ilsi=map.get("m_ilsi")+"";
		String m_free_day=map.get("m_free_day")+"";
		if(m_free_day==null||m_free_day.equals("")) {
			m_free_day="0";
		}
		
		String check=m_total+m_rental+m_period+m_ilsi;
		if(check.contains("null")||map.get("m_pr_idx").equals("")||m_total.equals("")||m_rental.equals("")||m_period.equals("")||m_ilsi.equals("")) {
			//int 타입 필수 정보 없는 경우
			return "렌탈기간,금액 확인 부탁드립니다.";
		}
		
		try {
			m_total=m_total.replaceAll(",", "");
			m_rental=m_rental.replaceAll(",", "");
			m_period=m_period.replaceAll(",", "");
			m_ilsi=m_ilsi.replaceAll(",", "");
			map.put("m_total", Integer.parseInt(m_total));
			map.put("m_rental", Integer.parseInt(m_rental));
			map.put("m_period", Integer.parseInt(m_period));
			map.put("m_ilsi", Integer.parseInt(m_ilsi));
			map.put("m_free_day", Integer.parseInt(m_free_day));
		}catch (Exception e) {
			return "렌탈기간,금액란에 숫자만 입력해주세요";
		}
		
		//빈 컬럼 빈값으로 세팅
		String cols="m_idx,pr_idx,m_num,m_total,m_rental";
		cols+=",m_period,m_paydate,m_paystart,m_ilsi,m_contract_date";
		cols+=",m_cb,m_eta,m_memo,m_install_finish_date,m_pay_method";
		cols+=",m_pay_info,m_pay_owner,m_pay_num,m_card_year,m_card_month";
		cols+=",m_state,m_voice_contract,m_process,m_jubsoo";
		cols+=",m_detail,m_free_day";
		
		cols+=",mi_idx,mi_name,mi_tel,mi_phone,mi_id_type"; 
		cols+=",mi_id,mi_addr1,mi_addr2,mi_addr3,mi_post"; 
		cols+=",mi_memo";
		
		String cols_arr[]=cols.split(",");
		for(int i=0;i<cols_arr.length;i++) {
			if(!map.containsKey(cols_arr[i])) {
				map.put(cols_arr[i], "");
			}
		}
		////
		memberDAO.insertMember(map);
		int m_idx=Integer.parseInt(map.get("m_idx")+"");
		String msg="";
		if(m_idx>0) {
			int mi_idx=memberDAO.insertMemberInfo(map);
			String mt=memberDAO.insertMemberType(MemberInfo.TYPE_APPLY, m_idx, mi_idx);
			Integer.parseInt(mt);
			if(mi_idx>0) {
				int mi_idx3=memberDAO.insertMemberInfo(map);
				memberDAO.insertMemberType(MemberInfo.TYPE_CONTRACT, m_idx, mi_idx3);
				
				int mi_idx2=memberDAO.insertMemberInfo(map);
				memberDAO.insertMemberType(MemberInfo.TYPE_DELIVERY, m_idx, mi_idx2);
				int pr_idx=Integer.parseInt(map.get("m_pr_idx")+"");
				Map<String,Object> pr=productDAO.selectProductRentalDetail(pr_idx+"");
				if(pr!=null) {
					int pr_type=Integer.parseInt(pr.get("pr_type")+"");
					String ds_state="";
					if(pr_type==ProductRental.TYPE_INSTALL) {
						ds_state=Delivery.STATE_BEFORE_INSTALL;
					}else{
						ds_state=Delivery.STATE_BEFORE_DELIVERY;
					}
					int ds_idx=deliveryDAO.insertDeliveryState(ds_state,Delivery.HISTORY_BASIC);
					List<HashMap<String,Object>> prplist=productDAO.selectRentalComDetail2(pr_idx);
					for(int i=0;i<prplist.size();i++) {
						int prp_idx=(Integer)prplist.get(i).get("prp_idx");
						deliveryDAO.insertDelivery(mi_idx2, prp_idx, ds_idx);
					}

					//배송,설치요청일 m_eta=ds_req 맞추기
					String ds_req=(String)map.get("m_eta");
					if(ds_req!=null&&!ds_req.equals("")) {
						Map<String,Object> dsmap=new HashMap<String, Object>();
						dsmap.put("ds_idx", ds_idx);
						dsmap.put("ds_req", ds_req);
						deliveryDAO.updateDeliveryState(dsmap);
					}
				}
				
				
				msg="등록 완료";
			}else {
				msg="고객 정보 입력 중 에러 발생.";
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
		}
		return msg;
	}
	/**기존 member에서 member_info 새로 생성*/
	@Transactional
	public int insertMemberInfo(int m_idx,String mt_code,Map<String, Object>map) {
		int mi_idx=memberDAO.insertMemberInfo(map);
		if(mi_idx>0) {
			String check=memberDAO.insertMemberType(mt_code, m_idx, mi_idx);
			Integer.parseInt(check);
		}else {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return -1;
		}
		return mi_idx;
	}
	/**updateMember. m_idx필수.<br>map에 담긴 것만 update*/
	public int updateMember(Map<String,Object> map) {
		return memberDAO.updateMember(map);
	}
	/**updateMember.결제정보 수정+cms수정*/
	@Transactional
	public int updateMemberPayInfo(Map<String,Object> map) {
		int result=memberDAO.updateMember(map);
		if(result>0) {
			result=memberDAO.updateMemberCms(map);
		}
		return result;
	}
	
	/**updateMemberInfo. mi_idx필수*/
	public int updateMemberInfo(Map<String,Object> map) {
		return memberDAO.updateMemberInfo(map);
	}

	/** customers/sangdam 에서 dataTable용 memberlist*/
	public List<Map<String,Object>> customerSangdamList(Map<String,Object> map){
		
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(order.equals("del")) {
			order="m_idx";
		}else if(order.equals("del_mi_addr")||order.equals("app_con_name")||order.equals("im_e_name")){
			//pass
		}else if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		return memberDAO.sangdamList(map);
	}
	/** customers/contract 에서 dataTable용 memberlist. 판매기수,금융기수까지<br>
	 * 상담사들은 본인 배정된 고객만 조회가능(쿼리에 있음)<br>
	 * m_state= 상담완료,재계약(구성변경)인 경우만
	 * */
	public List<Map<String,Object>> customerContractList(Map<String,Object> map){
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(order.equals("mi_addr")||order.equals("app_con_name")) {
			
		}else if(order.equals("m_free_date")) {
			//무료체험일 m_eta + m_free_day
		}else if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		return memberDAO.customerContractList(map);
	}
	/**상담관리 dt용 카운트*/
	public int countCustomerSangdamList(Map<String,Object> map){
		return memberDAO.countCustomerSangdamList(map);
	}
	/**
	 * 금융원부 리스트
	 * */
public List<Map<String,Object>> accountList(Map<String,Object> map){
		
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(order.equals("rental")||order.equals("yangdo")){
			order="m_idx";
		}else if(order.equals("c_mi_addr")||order.equals("c_mi_name")||order.equals("c_mi_id")||order.equals("c_mi_phone")||order.equals("c_mi_post")) {
			
		}else if(order.equals("m_free_date")) {
			//무료체험일 m_eta + m_free_day
		}else if(order.equals("d_mi_addr")||order.equals("m_pay_num_f")||order.equals("m_total_f")||order.equals("m_rental_f")){
			
		}else if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		return memberDAO.accountList(map, MemberInfo.TYPE_DELIVERY);
	}
	/**금융원부 리스트 cnt*/
	public int countaccountList(Map<String,Object> map){
		return memberDAO.countaccountList(map, MemberInfo.TYPE_DELIVERY);
	}

	/**계약고객  검색조건에 따른 결과 int return*/
	public int countCustomerContractList(Map<String,Object> map) {
		return memberDAO.countCustomerContractList(map);
	}
	/**member 총 숫자 return*/
	public int countMember() {
		return memberDAO.countMember();
	}
	/**m_idx로 member 조회*/
	public Map<String,Object> selectMemberDetail(int m_idx) {
		return memberDAO.selectMemberDetail(m_idx);
	}
	/**m_num으로 member 조회*/
	public Map<String,Object> selectMemberDetail(String m_num) {
		return memberDAO.selectMemberDetail(m_num);
	}
	/**m_idx로 member에 속한 member_info 조회.<br>
	 * mt_code==null인경우 모든 info 가져옴*/
	public List<Map<String,Object>> selectMemberInfo(int m_idx,String mt_code) {
		return memberDAO.selectMemberInfo(m_idx,mt_code);
	}
	
	/**mi_idx로  member_info조회*/
	public Map<String,Object> selectMemberInfoDetail(int mi_idx){
		return memberDAO.selectMemberInfoDetail(mi_idx);
	}
	
	/**판매기수 리스트. mg_idx,mg_name*/
	public List<Map<String,Object>> selectMemberGroupList(){
		return memberDAO.selectMemberGroupList();
	}
	
	/**판매기수 조회 메서드.mg_name,mgm_cnt*/
	public Map<String,Object> selectMemberGroupDetail(String m_idx){
		int midx;
		try{
			midx=Integer.parseInt(m_idx);
		}catch (Exception e) {
			Log.warn("selectMemberGroupDetail에러 "+e.toString());
			return null;
		}
		Map<String,Object>mg=memberDAO.selectMemberGroupDetail(midx);
		if(mg==null||mg.isEmpty()) {
			mg=new HashMap<String, Object>();
			mg.put("mg_name", "");
			mg.put("mgm_cnt", "");
		}
		return mg;
	}
	/**판매기수 update.<br>판매기수 없는 경우 새로 insert함*/
	public int updateMemberGroupMember(String midx,String mg_name,String mgm_cnt){
		int m_idx;
		int mg_idx;
		int result=0;
		try{
			m_idx=Integer.parseInt(midx);
			mg_idx=memberDAO.selectMgidx(mg_name);
		}catch (Exception e) {
			Log.warn("selectMemberGroupDetail에러 "+e.toString());
			return -1;
		}
		Map<String,Object>mg=memberDAO.selectMemberGroupDetail(m_idx);
		if(mg==null||mg.isEmpty()) {
			//판매기수 없는 경우
			result=memberDAO.insertMemberGroupMember(mg_idx, m_idx, mgm_cnt);
		}else {
			//판매기수 있는 경우
			result=memberDAO.updateMemberGroupMember(mg_idx, m_idx, mgm_cnt);
		}
		return result;
	}
	/**
	 * 견적 AJAX
	 * */
	public Map<String, Object> selectRetalEstimate(ParamMap pmap) {
		Map<String, Object> selectRetalEstimate = memberDAO.selectRetalEstimate(pmap);
		
		return selectRetalEstimate;
	}	
	/**상담cs배정*/
	public int updateDamdang(String m_idx,String m_damdang) {
		int midx=0;
		try {
			midx=Integer.parseInt(m_idx);
			//employee 테이블에 정보 있는지 확인
			if(employeeDAO.selectEmployeeDetail(Integer.parseInt(m_damdang))==null) {
				return -1;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		return memberDAO.updateDamdang(midx, m_damdang);
	}
	
	/**selectMemberFileList. 파일,메모 등*/
	public List<Map<String,Object>> selectMemberFileList(String m_idx,String mf_name) {
		return memberDAO.selectMemberFileList(m_idx, mf_name);
	}
	
	/**update 상담 메모.<br>
	 * mf_log를 null로 줄 때는 메모 전부 삭제*/
	@Transactional
	public void updateSangdamMemo(String m_idx,String[] mf_log,String[] mf_memo) {
		//삭제 후 다시 insert
		memberDAO.deleteMemberFileType(Integer.parseInt(m_idx), MemberFile.NAME_SANGDAM_MEMO);
		if(mf_log==null) {
			return;
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("mf_name", MemberFile.NAME_SANGDAM_MEMO);
		map.put("mf_file", "");
		
		for(int i=0;i<mf_log.length;i++) {
			map.put("mf_memo", mf_memo[i].replaceAll(System.getProperty("line.separator"), "<br>"));
			map.put("mf_log", mf_log[i]);
			memberDAO.insertMemberFile(map);
		}
		
	}
	
	/**insert member_file. 파일 업로드<br>
	 * m_idx필수. mf_log를 null로 넣을 경우 현재시간<br>
	 * #{m_idx},#{mf_name},#{mf_file}<br>
	 * mf_memo 에는 random숫자_midx*/
	@Transactional
	public String insertMemberFile(Map<String,Object>map,MultipartFile file) {
		try{
			String mf_name =map.get("mf_name")+"";
			//아래 3가지는 지웠다가 업로드
			if(mf_name.equals(MemberFile.NAME_AGREEMENT_CERTFICATE)||mf_name.equals(MemberFile.NAME_TRANSFER_NOTICE)||mf_name.equals(MemberFile.NAME_CREDIT_INQUIRY)){
				int m_idx =Integer.parseInt(map.get("m_idx")+"");
				memberDAO.deleteMemberFileType(m_idx, mf_name);
			}
			
			String mf_memo=(int)(Math.random()*1000000000)+"";
			map.put("mf_memo", mf_memo);
			int mf_idx=memberDAO.insertMemberFile(map);
			
			String path=MemberFile.PATH+map.get("mf_name");
			File f=new File(path);
			if(!f.exists()) {
				f.mkdirs();
			}
			map.put("mf", mf_idx+"_"+mf_memo);
			file.transferTo(new File(path+"/"+mf_idx+"_"+mf_memo));
		}catch (Exception fe) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return "오류 발생";
		}
		return "업로드 완료";
	}
	
	/**파일삭제메서드*/
	public void deleteMemberFile(String mf_idx) {
		Map<String,Object> map_mf=memberDAO.selectMemberFileDetail(mf_idx);
		if(map_mf==null)return;
		File f=new File(MemberFile.PATH+map_mf.get("mf_name")+"/"+mf_idx+"_"+map_mf.get("mf_memo"));
		if(!f.exists()) {
			f=new File(MemberFile.PATH+MemberFile.NAME_RECORD+"/"+map_mf.get("mf_file"));
		}
		memberDAO.deleteMemberFileIdx(Integer.parseInt(mf_idx));
	}
	
	/**다운로드 위해 파일명 검색*/
	public Map<String,Object> selectMemberFileDown(String mf_idx,String mf_memo) {
		return memberDAO.selectMemberFileDown(mf_idx, mf_memo);
	}
	
	/**상담전인 상태의 member delete*/
	@Transactional
	public boolean deleteMember(int m_idx) {
		boolean deleted=true;
		List<Map<String,Object>> mi=memberDAO.selectMemberInfo(m_idx, null);
		if(mi!=null&&mi.size()>0) {
			for(int i=0;i<mi.size();i++) {
				int mi_idx=(Integer)mi.get(i).get("mi_idx");
				if(memberDAO.deleteMemberInfo(mi_idx)==0) {
					deleted=false;
				}
			}
		}
		if(memberDAO.deleteMemberType(m_idx)==0) {
			deleted=false;
		}
		
		if(!deleted) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return deleted;
	}
	
	/**distinct(m_paydate)*/
	public List<String> payDates(){
		return memberDAO.payDates();
	}
	/**
	 * CS점검관리 방문예정일 목록
	 */
	public List<Map<String, Object>> customerCsList(Map<String, Object> map) {
		return memberDAO.selectCsList(map,MemberInfo.TYPE_DELIVERY);
	}
	public Map<String, Object> selecCustomerDetail(String m_idx) {
		return memberDAO.selecCustomerDetail(m_idx,MemberInfo.TYPE_DELIVERY);
	}
	
	/**채권관리 담당자 히스토리 조회. m_idx,length,start*/
	public List<Map<String,Object>> selectMemberDamdangHistoryList(Map<String,Object> map){
		return memberDAO.selectMemberDamdangHistoryList(map);
	}
	
	/**dt용. 채권관리 담당자 히스토리 개수 */
	public int countMemberDamdangHistoryList(int m_idx) {
		return memberDAO.countMemberDamdangHistoryList(m_idx);
	}
	
	/**채권관리 담당자 배정 */
	@Transactional
	public void insertMemberDamdang(int m_idx,int e_id,int updater_eid,String memo){
		Map<String,Object>map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("m_e_id", e_id);
		map.put("mdh_memo", "");
		map.put("mdh_log_eid", updater_eid);
		memberDAO.updateMember(map);
		memberDAO.insertMemberDamdang(map);
	}
	
	
	/**tera에서 받은 멤버 등록*/
	@Transactional(isolation=Isolation.SERIALIZABLE)
	public String receiveData(Map<String,Object> hm) {
		String msg="error";
		
		String m_num=hm.get("m_u_number")+"";
		m_num=m_num.trim();
		try {
			//pr_idx가져오기
			String m_pcode=(String)hm.get("m_pcode");
			Map<String,Object> pr_map=productDAO.selectPrByCode(m_pcode);
			
			if(pr_map==null) {
				Log.warn(m_num+"/tera/pr정보 null/"+m_pcode);
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return "wrong m_pcode";
			}
			int pr_idx=Integer.parseInt(pr_map.get("pr_idx")+"");
			//받은 정보 바뀐 db에 알맞게 세팅
			
			Map<String,Object> member=new HashMap<String, Object>();//결제자
			Map<String,Object> mi_apply=new HashMap<String, Object>();//신청자
			Map<String,Object> mi_contract=new HashMap<String, Object>();//계약자
			Map<String,Object> mi_delivery=new HashMap<String, Object>();//배송자
			
			
			
			//member 입력
			member.put("m_pr_idx", pr_idx);
			member.put("m_num", m_num);
			member.put("m_sangdam_finish", hm.get("m_sangdam_date_finish")+"");
			
			String str_m_period=(hm.get("m_prentterm")+"").replaceAll("[^0-9]", "");
			if(str_m_period.equals("")) {
				str_m_period="0";
			}
			int m_period=Integer.parseInt(str_m_period);
			String str_m_prentmoney=(hm.get("m_prentmoney")+"").replaceAll("[^0-9]", "");
			if(str_m_prentmoney.equals("")) {
				str_m_prentmoney="0";
			}
			int m_rental=Integer.parseInt(str_m_prentmoney);
			member.put("m_total", m_rental*m_period);
			member.put("m_rental", m_rental);
			member.put("m_period", m_period);
			member.put("m_sale", hm.get("m_u_sale_loc"));
			
			String m_damdang="";
			Map<String,Object> m_damdang_map=employeeDAO.login(hm.get("m_damdang")+"");
			if(m_damdang_map!=null) {
				String temp=m_damdang_map.get("e_id")+"";
				m_damdang=temp.replaceAll("[^0-9]", "");
			}
			member.put("m_damdang", m_damdang);
			
			member.put("m_date", hm.get("m_sangdam_date_finish")==null?"":hm.get("m_sangdam_date_finish"));
			
			String m_pay_date=hm.get("ms_transfer_date1")+"";
			m_pay_date=m_pay_date.replaceAll("[^0-9]", "");
			if(m_pay_date.length()!=2) {
				m_pay_date="00"+m_pay_date;
				m_pay_date=m_pay_date.substring(m_pay_date.length()-2);
			}
			member.put("m_paydate", m_pay_date);
			member.put("m_paystart", hm.get("ms_transfer_date3")==null?"":hm.get("ms_transfer_date3"));
			member.put("m_ilsi", pr_map.get("pr_ilsi"));
			member.put("m_contract_date", hm.get("ms_contract_date")==null?"":hm.get("ms_contract_date"));
			member.put("m_cb", hm.get("ms_cb")==null?"":hm.get("ms_cb"));
			String ds_req=(String)hm.get("ms_install_date1");
			if(ds_req==null) {
				ds_req="";
			}
			member.put("m_eta", ds_req);//사용안함
			member.put("m_memo", "");
			member.put("m_install_finish_date", "");
			
			//카드,계좌 결제 정보
			String m_pay_method=hm.get("ms_pay_method")+"";
			member.put("m_pay_method", m_pay_method);
			if(m_pay_method.equals(MemberInfo.PAY_METHOD_CARD)) {
				member.put("m_pay_info", hm.get("ms_card")==null?"":hm.get("ms_card"));
				member.put("m_pay_owner", hm.get("ms_card_name")==null?"":hm.get("ms_card_name"));
				member.put("m_pay_num", hm.get("ms_card_num")==null?"":hm.get("ms_card_num"));
				member.put("m_card_year", hm.get("ms_card_num_year")==null?"":hm.get("ms_card_num_year"));
				member.put("m_card_month", hm.get("ms_card_num_month")==null?"":hm.get("ms_card_num_month"));
			}else if(m_pay_method.equals(MemberInfo.PAY_METHOD_BANK)||m_pay_method.equals("")){
				String m_pay_info=payDAO.selectSubBankByCode(hm.get("ms_pay_bank")+"");
				if(m_pay_info==null)m_pay_info="";
				member.put("m_pay_info", m_pay_info);
				member.put("m_pay_owner", hm.get("ms_pay_bank_name")==null?"":hm.get("ms_pay_bank_name"));
				member.put("m_pay_num", hm.get("ms_pay_bank_num")==null?"":hm.get("ms_pay_bank_num"));
				member.put("m_card_year", "");
				member.put("m_card_month", "");
			}else{
				Log.warn(m_num+"/tera/출금정보 이상"+m_pay_method);
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return "error";
			}
			
			member.put("m_pay_id", (hm.get("ms_order_jumin")+"").replaceAll("[^0-9]", ""));
			member.put("m_state", hm.get("m_state")+"");
			member.put("m_contract_date", hm.get("ms_contract_date")==null?"":hm.get("ms_contract_date"));
			member.put("m_voice_contract", hm.get("ms_final_state_date")==null?"":hm.get("ms_final_state_date"));
			member.put("m_process", hm.get("m_gubun2")==null?"":hm.get("m_gubun2"));
			member.put("m_jubsoo", hm.get("m_gubun1")==null?"":hm.get("m_gubun1"));
			member.put("m_detail", hm.get("m_gubun")==null?"":hm.get("m_gubun"));
			member.put("m_free_day", (hm.get("m_day")+"").replaceAll("[^0-9]", ""));
			member.put("m_cms", "미등록");
			
			String m_jehyu=(String)hm.get("m_sale");
			if(m_jehyu==null)m_jehyu="GN";
			member.put("m_jehyu", m_jehyu);
			
			
			memberDAO.insertMember(member);
			
			//member insert완료 후 m_idx반환
			int m_idx=Integer.parseInt(member.get("m_idx")+"");
			if(m_idx<1) {
				Log.warn(m_num+"tera member insert중 에러.m_idx<1");
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return "error";
			}
			
			
			//상담메모
			int m_sangdam_cnt=Integer.parseInt((String)hm.get("m_sangdam_cnt"));
			if(m_sangdam_cnt>0) {
				for(int i=1;i<=m_sangdam_cnt;i++) {
					Map<String,Object> sangdamMemo=new HashMap<String, Object>();
					sangdamMemo.put("m_idx", m_idx);
					sangdamMemo.put("mf_name", MemberFile.NAME_SANGDAM_MEMO);
					sangdamMemo.put("mf_file", "");
					sangdamMemo.put("mf_memo", hm.get("m_sangdam"+i+"_memo")==null?"":hm.get("m_sangdam"+i+"_memo"));
					sangdamMemo.put("mf_log", hm.get("m_sangdam"+i+"_date")==null?"":hm.get("m_sangdam"+i+"_date"));
					
					memberDAO.insertMemberFile(sangdamMemo);
				}
			}
			
			
			//insert member_info
			mi_apply.put("m_idx", m_idx);
			mi_apply.put("mi_name", hm.get("m_u_name")==null?"":hm.get("m_u_name"));
			mi_apply.put("mi_tel", "");
			mi_apply.put("mi_phone", (hm.get("m_u_phone")+"").replaceAll("[^0-9]", ""));
			mi_apply.put("mi_id_type",MemberInfo.ID_TYPE_PERSON);
			mi_apply.put("mi_id","");
			mi_apply.put("mi_addr1", hm.get("m_u_addr1")==null?"":hm.get("m_u_addr1"));
			mi_apply.put("mi_addr2", hm.get("m_u_addr2")==null?"":hm.get("m_u_addr2"));
			mi_apply.put("mi_addr3", hm.get("m_u_addr3")==null?"":hm.get("m_u_addr3"));
			mi_apply.put("mi_post", (hm.get("m_u_addr4")+"").replaceAll("[^0-9]", ""));
			mi_apply.put("mi_memo", "");
			int mi_apply_idx=memberDAO.insertMemberInfo(mi_apply);
			memberDAO.insertMemberType(MemberInfo.TYPE_APPLY, m_idx, mi_apply_idx);
			
			mi_contract.put("m_idx", m_idx);
			mi_contract.put("mi_name", hm.get("ms_order_name")==null?"":hm.get("ms_order_name"));
			mi_contract.put("mi_tel", "");
			mi_contract.put("mi_phone", (hm.get("ms_order_phone1")+"").replaceAll("[^0-9]", ""));
			mi_contract.put("mi_id_type",MemberInfo.ID_TYPE_PERSON);
			mi_contract.put("mi_id", (hm.get("ms_contract_jumin")+"").replaceAll("[^0-9]", ""));
			mi_contract.put("mi_addr1", hm.get("ms_contract_loc1")==null?"":hm.get("ms_contract_loc1"));
			mi_contract.put("mi_addr2", hm.get("ms_contract_loc2")==null?"":hm.get("ms_contract_loc2"));
			mi_contract.put("mi_addr3", hm.get("ms_contract_loc3")==null?"":hm.get("ms_contract_loc3"));
			mi_contract.put("mi_post", (hm.get("ms_contract_loc4")+"").replaceAll("[^0-9]", ""));
			mi_contract.put("mi_memo", "");
			int mi_contract_idx=memberDAO.insertMemberInfo(mi_contract);
			memberDAO.insertMemberType(MemberInfo.TYPE_CONTRACT, m_idx, mi_contract_idx);
			
			mi_delivery.put("m_idx", m_idx);
			mi_delivery.put("mi_name", hm.get("ms_receive_name")+"");
			mi_delivery.put("mi_tel", (hm.get("ms_receive_phone2")+"").replaceAll("[^0-9]", ""));
			mi_delivery.put("mi_phone", (hm.get("ms_receive_phone1")+"").replaceAll("[^0-9]", ""));
			mi_delivery.put("mi_id_type",MemberInfo.ID_TYPE_PERSON);
			mi_delivery.put("mi_id","");
			mi_delivery.put("mi_addr1", hm.get("ms_install_loc1")==null?"":hm.get("ms_install_loc1"));
			mi_delivery.put("mi_addr2", hm.get("ms_install_loc2")==null?"":hm.get("ms_install_loc2"));
			mi_delivery.put("mi_addr3", hm.get("ms_install_loc3")==null?"":hm.get("ms_install_loc3"));
			mi_delivery.put("mi_post", (hm.get("ms_install_loc_num")+"").replaceAll("[^0-9]", ""));
			mi_delivery.put("mi_memo", hm.get("ms_install_loc4")==null?"":hm.get("ms_install_loc4"));
			int mi_delivery_idx=memberDAO.insertMemberInfo(mi_delivery);
			memberDAO.insertMemberType(MemberInfo.TYPE_DELIVERY, m_idx, mi_delivery_idx);
			
			////확장성 위해 추가한 코드. 개별배송 사용시 변경해야할 코드( ds insert 후 delivery prp_idx만큼 반복생성
			Map<String,Object> pr=productDAO.selectProductRentalDetail(pr_idx+"");
			int pr_type=Integer.parseInt(pr.get("pr_type")+"");
			String ds_state="";
			if(pr_type==ProductRental.TYPE_INSTALL) {
				ds_state=Delivery.STATE_BEFORE_INSTALL;
			}else{
				ds_state=Delivery.STATE_BEFORE_DELIVERY;
			}
			int ds_idx=deliveryDAO.insertDeliveryState(ds_state,Delivery.HISTORY_BASIC);
			List<HashMap<String,Object>> prplist=productDAO.selectRentalComDetail2(pr_idx);
			for(int i=0;i<prplist.size();i++) {
				int prp_idx=(Integer)prplist.get(i).get("prp_idx");
				deliveryDAO.insertDelivery(mi_delivery_idx, prp_idx, ds_idx);
			}
			////
			
			//배송,설치요청일 tera에서 숯침대만 보내고있다고함 0823
			if(ds_req!=null&&!ds_req.equals("")) {
				Map<String,Object> dsmap=new HashMap<String, Object>();
				dsmap.put("ds_idx", ds_idx);
				dsmap.put("ds_req", ds_req);
				
				if(pr_type==ProductRental.TYPE_INSTALL) {
					dsmap.put("ds_state", Delivery.STATE_REQUEST_INSTALL);
				}else{
					dsmap.put("ds_state", Delivery.STATE_REQUEST_DELIVERY);
				}
				deliveryDAO.updateDeliveryState(dsmap);
			}
			
			
			//판매기수 입력
			String mg_name=(String)hm.get("m_unit_cnt");
			if(mg_name==null)mg_name="";
			int mg_idx=memberDAO.selectMgidx(mg_name);// null값..
			String mgm_cnt=hm.get("m_unit_cnt2")+"";
			Map<String,Object>mg=memberDAO.selectMemberGroupDetail(m_idx);
			if(mg==null||mg.isEmpty()) {
				//판매기수 없는 경우
				memberDAO.insertMemberGroupMember(mg_idx, m_idx, mgm_cnt);
			}else {
				//판매기수 있는 경우
				memberDAO.updateMemberGroupMember(mg_idx, m_idx, mgm_cnt);
			}
			
			//mf_file (녹취파일)
			String file_name=hm.get("ms_voice_file")+"";
			Map<String,Object> voiceFile=new HashMap<String, Object>();
			voiceFile.put("m_idx", m_idx);
			voiceFile.put("mf_name", MemberFile.NAME_RECORD);
			voiceFile.put("mf_file", file_name);
			voiceFile.put("mf_memo", "");
			memberDAO.insertMemberFile(voiceFile);
			
			//mf_file (신용정보)
			String file_credit=hm.get("ms_file4")+"";
			Map<String,Object> creditFile=new HashMap<String, Object>();
			creditFile.put("m_idx", m_idx);
			creditFile.put("mf_name", MemberFile.NAME_CREDIT_INQUIRY);
			creditFile.put("mf_file", file_credit);
			creditFile.put("mf_memo", "");
			memberDAO.insertMemberFile(creditFile);
			
			msg=m_num;
		}catch (Exception e) {
			e.printStackTrace();
			msg="error";
			Log.warn(m_num+"tera insert중 에러"+e.toString());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg;
	}
	
	/**
	 * 기수 가져오기
	 * */
	public List<HashMap<String, Object>> getGisu() {
		return memberDAO.getGisu();
	}
	
	/**m_idx로 배송자 정보 조회*/
	public Map<String, Object> selectRentalbyDelivery(String m_idx) {
		return memberDAO.selectRentalbyDelivery(m_idx);
	}
	
	/**m_idx로 계약자 정보 조회*/
	public Map<String, Object> selectRentalbyContract(String m_idx) {
		return memberDAO.selectRentalbyContract(m_idx);
	}
	public String getMi_addr1(int mi_idx) {
		return memberDAO.getMi_addr1(mi_idx);
	}
	
	/**렌탈상품명 변경시 작동하는 로직*/
	@Transactional
	public void changeProduct(int pr_idx,int mi_idx,int m_idx) {
		List<HashMap<String,Object>> prplist=productDAO.selectRentalComDetail2(pr_idx);
		Map<String,Object> pr=productDAO.selectProductRentalDetail(pr_idx+"");
		int pr_type=Integer.parseInt(pr.get("pr_type")+"");
		String ds_state="";
		if(pr_type==ProductRental.TYPE_INSTALL) {
			ds_state=Delivery.STATE_REQUEST_INSTALL;
		}else{
			ds_state=Delivery.STATE_BEFORE_DELIVERY;
		}
		
		//ds삭제 후 입력
		List<Map<String,Object>> deliverys=deliveryDAO.selectDeliveryState(m_idx);
		if(deliverys!=null&&!deliverys.isEmpty()) {
			for(int i=0;i<deliverys.size();i++) {
				int ds_idx=Integer.parseInt(deliverys.get(i).get("ds_idx")+"");
				deliveryDAO.deleteDelivery(ds_idx);
				deliveryDAO.deleteDeliveryState(ds_idx);
			}
		}
		
		int ds_idx=deliveryDAO.insertDeliveryState(ds_state,Delivery.HISTORY_BASIC);
		for(int i=0;i<prplist.size();i++) {
			int prp_idx=(Integer)prplist.get(i).get("prp_idx");
			deliveryDAO.insertDelivery(mi_idx, prp_idx, ds_idx);
		}
		
		//member update
		Map<String,Object> memUp=new HashMap<String, Object>();
		memUp.put("m_idx", m_idx);
		memUp.put("m_pr_idx", pr_idx);
		memUp.put("m_total", pr.get("pr_total"));
		memUp.put("m_rental", pr.get("pr_rental"));
		memUp.put("m_period", pr.get("pr_period"));
		memUp.put("m_ilsi", pr.get("pr_ilsi"));
		memUp.put("m_state", MemberInfo.STATE_CHANGE_PRODUCT);
		memberDAO.changeProduct(memUp);
	}
	
	//install_date update (m_eta)
	public int updateinstallDate(HashMap<String, Object> rmap) {
		return memberDAO.updateinstallDate(rmap);
	}
	
	/**cms회원 dataTable*/
	public List<Map<String, Object>> selectMemberCms(Map<String,Object>map) {
		Object oc=map.get("order[0][column]");
		String order=map.get("columns["+oc+"][data]")+"";
		String asc=map.get("order[0][dir]")+"";
		if(order.equals("m_rental_f")) {
			order="m_rental";
		}else if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);
		
		if(asc.equals("asc")) {
			map.put("order", "asc");
		}else {
			map.put("order", "desc");
		}
		
		return memberDAO.selectMemberCms(map);
	}
	
	/**cms회원 dataTable count*/
	public int countMemberCms(Map<String,Object>map) {
		return memberDAO.countMemberCms(map);
	}
	
	public int getMi_idx(int m_idx) {
		return memberDAO.getMi_idx(m_idx);
	}
	
	/**앱에서 구성상품변경*/
	@Transactional
	public void changeProductApp(int pr_idx, int mi_idx, int m_idx) {
		List<HashMap<String,Object>> prplist=productDAO.selectRentalComDetail2(pr_idx);
		Map<String,Object> pr=productDAO.selectProductRentalDetail(pr_idx+"");
		
		
		int pr_type=Integer.parseInt(pr.get("pr_type")+"");
		String ds_state="";
		if(pr_type==ProductRental.TYPE_INSTALL) {
			ds_state=Delivery.STATE_REQUEST_INSTALL;
		}else{
			ds_state=Delivery.STATE_BEFORE_DELIVERY;
		}
		
		//delivery 삭제 후 입력. 1개의 ds만 사용하는 경우
		List<Map<String,Object>> deliverys=deliveryDAO.selectDeliveryState(m_idx);
		if(deliverys!=null&&!deliverys.isEmpty()) {
			int ds_idx=Integer.parseInt(deliverys.get(0).get("ds_idx")+"");
			deliveryDAO.deleteDelivery(ds_idx);
			
			for(int i=0;i<prplist.size();i++) {
				int prp_idx=(Integer)prplist.get(i).get("prp_idx");
				deliveryDAO.insertDelivery(mi_idx, prp_idx, ds_idx);
			}
			
			Map<String,Object> dsMap=new HashMap<String, Object>();
			dsMap.put("ds_idx", ds_idx);
			dsMap.put("ds_state", ds_state);
			deliveryDAO.updateDeliveryState(dsMap);
		}
		
		//member update
		Map<String,Object> memUp=new HashMap<String, Object>();
		memUp.put("m_idx", m_idx);
		memUp.put("m_pr_idx", pr_idx);
		memUp.put("m_total", pr.get("pr_total"));
		memUp.put("m_rental", pr.get("pr_rental"));
		memUp.put("m_period", pr.get("pr_period"));
		memUp.put("m_ilsi", pr.get("pr_ilsi"));
		memUp.put("m_state", MemberInfo.STATE_CHANGE_PRODUCT);
		memberDAO.changeProduct(memUp);
	}
	
	/**중도상환*/
	@Transactional
	public String joongdo(int m_idx) {
		String msg="";
		try {
			int cnt=payDAO.countOnProcess(m_idx);
			if(cnt>0) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return "출금중(신청중)인 출금데이터가 있습니다.\n출금 결과를 받은 후 처리 가능합니다.";
			}
			
			
			Map<String,Object> member=new HashMap<String, Object>();
			member.put("m_idx", m_idx);
			member.put("m_process", MemberInfo.PROCESS_JOONGDO);
			memberDAO.updateMember(member);
			
			payDAO.noMoreRental(m_idx, Pay.STATE_JOONGDO);
			
			msg="중도상환 처리하였습니다.";
		}catch (Exception e) {
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			msg="중도상환 중 오류 발생.";
		}
		
		return msg;
	}
	
	//무료체험등록
	@Transactional
	public String changeTestFree(Map<String, Object> map) {
		int m_idx=Integer.parseInt(map.get("m_idx")+"");
		Map<String,Object> member=memberDAO.selectMemberDetail(m_idx);
		String m_jubsoo=member.get("m_jubsoo")+"";
		if(!m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)) {
			return "접수구분 무료체험일 때만 가능합니다.";
		}
		
		try {
			Map<String,Object> ds=deliveryDAO.selectDeliveryState(m_idx).get(0);
			if(!ds.get("ds_state").equals(Delivery.STATE_REQUEST_INSTALL)) {
				return "설치요청 상태에서만 가능합니다.";
			}
			
			//체험중으로 바꿀 때 설치상태는 설치요청 그대로0831
			//ds.put("ds_state", Delivery.STATE_INSTALLED);
			//deliveryDAO.updateDeliveryState(ds);
		}catch (Exception e) {
			return "설치정보가 없습니다.";
		}
		
		member.put("m_process", MemberInfo.PROCESS_TEST);
		member.put("m_state", MemberInfo.STATE_FINISH);
		
		int result = memberDAO.updateMember(member);
		
		String msg;
		if(result >0 ) {
			msg="등록완료";
		}else {
			msg="등록실패";
		}
		return msg;
	}
	/**약정일 구하기*/
	public Map<String, Object> rentalFinishDate(String m_idx) {
		return memberDAO.rentalFinishDate(m_idx);
	}
}
