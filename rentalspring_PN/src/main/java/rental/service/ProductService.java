package rental.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;

import rental.common.Common;
import rental.common.ParamMap;
import rental.constant.MemberInfo;
import rental.constant.ProductRental;
import rental.dao.CommonDAO;
import rental.dao.ProductDAO;

@Service("productService")
public class ProductService {

	@Autowired
	ProductDAO productDAO;
	@Autowired
	CommonDAO commonDAO;

	final public static String UF_EXCEL_DIR = "C:/file/excel/";

	private DataSourceTransactionManager trans;

	/**
	 * dataTable에 맞춘 렌탈상품 조회 리스트<br>
	 * 
	 */
	public List<Map<String, Object>> productRentalList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";

		if (!(order.equals("pr_name") || order.equals("pr_rental") || order.equals("pr_total")
				|| order.equals("pr_period") || order.equals("pr_ilsi"))) {
			// 동적쿼리위한 칼럼명이 다른 값이 들어간 경우
			return null;
		} else {
			map.put("column", order);
		}

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}

		return productDAO.selectProductRentalList(map);
	}

	/** product_rental 총 개수 */
	public int countProductRental() {
		return productDAO.countProductRental();
	}

	/** 시작하는 숫자(0부터), 행갯수,검색어(p_name) */
	public List<Map<String, Object>> selectProductList(int start, int count, String keyword) {
		return productDAO.selectProductList(start, count, keyword);
	}

	/** product_rental pr_idx로 조회 */
	/**
	 * 렌탈상품 상세보기
	 */
	public HashMap<String, Object> selectProductRentalDetail(String pr_idx) {
		return productDAO.selectProductRentalDetail(pr_idx);
	}

	public List<Map<String, Object>> selectProductList(Map<String, Object> map) {

		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";

		map.put("column", order);

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		return productDAO.selectProductList(map);

	}

	public Object countProduct(Map<String, Object> map) {

		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";
		map.put("column", order);
		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		return productDAO.countProduct(map);
	}

	/**
	 * p_id를 이용하여 상품정보 조회
	 * 
	 */
	public Map<String, Object> selectProductDetail(String p_id) {
		int pid = 0;
		try {
			pid = Integer.parseInt(p_id);
		} catch (Exception e) {
			return null;
		}
		return productDAO.selectProductDetail(pid);
	}

	/**
	 * p_code를 이용하여 상품정보 조회
	 * 
	 */
	public Map<String, Object> selectProductDetailByP_code(String p_code) {
		return productDAO.selectProductDetailByP_code(p_code);
	}

	/**
	 * 
	 * 렌탈상품 구성
	 */
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public String insertRentalProduct(ParamMap map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		String pr_idx = "";
		rMap.put("pr_name", map.get("pr_name"));
		rMap.put("pr_memo", map.get("pr_memo"));
		rMap.put("pr_total", Common.replaceComma((String) map.get("pr_total")));
		String pr_rental = map.get("pr_rental") + "";
		pr_rental = pr_rental.replaceAll("[^0-9]", "");
		int i_pr_rental = Integer.parseInt(pr_rental);
		i_pr_rental = (i_pr_rental / 100) * 100;

		rMap.put("pr_rental", i_pr_rental);
		rMap.put("pr_ilsi", Common.replaceComma((String) map.get("pr_ilsi")));
		rMap.put("pr_period", Common.replaceComma((String) map.get("pr_period")));
		rMap.put("pr_aff", map.get("pr_aff"));
		rMap.put("pr_aff_cost", Common.replaceComma((String) map.get("pr_aff_cost")));
		rMap.put("pr_delivery1", Common.replaceComma((String) map.get("pr_delivery1")));
		rMap.put("pr_delivery2", Common.replaceComma((String) map.get("pr_delivery2")));
		rMap.put("pr_install1", Common.replaceComma((String) map.get("pr_install1")));
		rMap.put("pr_install2", Common.replaceComma((String) map.get("pr_install2")));
		rMap.put("pr_gubun", map.get("pr_gubun"));
		rMap.put("pr_rentalDC", Common.replaceComma((String) map.get("pr_rentalDC")));
		rMap.put("pr_rentalDCp", Common.replaceComma((String) map.get("pr_rentalDCp")));
		rMap.put("pr_rentalEXE", Common.replaceComma((String) map.get("pr_rentalEXE")));
		rMap.put("pr_creater", ((HashMap) map.get("login")).get("e_name"));

		String p_type = "1";

		for (String key : map.keySet()) {
			if (key.startsWith("prp_num")) {
				String id = key.replaceAll("[^0-9]", "");
				int p_id = Integer.parseInt(id);
				p_type = productDAO.selectProductDetail(p_id).get("p_type") + "";
				break;
			}
		}
		int i_p_type = Integer.parseInt(p_type);
		if (i_p_type == ProductRental.TYPE_INSTALL) {
			rMap.put("pr_type", ProductRental.TYPE_INSTALL);
		} else {
			rMap.put("pr_type", ProductRental.TYPE_DELIVERY);
		}

		rMap.put("pr_match", ProductRental.MATCHING);
		try {
			String code = getRentalCode();
			String rcode = "";
			if (code == null) {
				rcode = "RT-00000001";
			} else {
				int no = Integer.parseInt(code.replace("RT-", "").toString());
				String codeno = String.format("%08d", no + 1);
				rcode = "RT-" + codeno;
			}
			rMap.put("pr_rentalcode", rcode);

			pr_idx = productDAO.insertrentalProduct(rMap).get("pr_idx") + "";
			List<HashMap<String, Object>> prpList = new ArrayList<HashMap<String, Object>>();
			for (String key : map.keySet()) {
				String id = "";
				if (key.startsWith("prp_num") == true) {
					HashMap<String, Object> prpmap = new HashMap<String, Object>();
					id = key.replace("prp_num", "");
					String num = map.get(key) + "";
					prpmap.put("p_id", Integer.parseInt(id));
					prpmap.put("prp_num", Integer.parseInt(num));
					prpmap.put("pr_idx", Integer.parseInt(pr_idx));
					prpList.add(prpmap);
				}
			}
			productDAO.insertPRP(prpList);
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return productDAO.selectRenatalCode(pr_idx);

	}

	@Transactional(isolation = Isolation.SERIALIZABLE)
	public String OldinsertRentalProduct(ParamMap map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		String pr_idx = "";
		rMap.put("pr_name", map.get("pr_name") + "_LEDE4U");
		rMap.put("pr_memo", map.get("pr_memo"));
		rMap.put("pr_total", Common.replaceComma((String) map.get("pr_total")));

		String pr_rental = map.get("pr_rental") + "";
		pr_rental = pr_rental.replaceAll("[^0-9]", "");
		int i_pr_rental = Integer.parseInt(pr_rental);
		i_pr_rental = (i_pr_rental / 100) * 100;

		rMap.put("pr_rental", i_pr_rental);
		rMap.put("pr_period", map.get("pr_period"));
		rMap.put("pr_ilsi", Common.replaceComma((String) map.get("pr_ilsi")));
		rMap.put("pr_aff", map.get("pr_aff"));
		rMap.put("pr_aff_cost", Common.replaceComma((String) map.get("pr_aff_cost")));
		rMap.put("pr_delivery1", Common.replaceComma((String) map.get("pr_delivery1")));
		rMap.put("pr_delivery2", Common.replaceComma((String) map.get("pr_delivery2")));
		rMap.put("pr_install1", Common.replaceComma((String) map.get("pr_install1")));
		rMap.put("pr_install2", Common.replaceComma((String) map.get("pr_install2")));
		rMap.put("pr_gubun", map.get("pr_gubun"));
		rMap.put("pr_rentalDC", Common.replaceComma((String) map.get("pr_rentalDC")));
		rMap.put("pr_rentalDCp", Common.replaceComma((String) map.get("pr_rentalDCp")));
		rMap.put("pr_rentalEXE", Common.replaceComma((String) map.get("pr_rentalEXE")));
		if (map.get("pr_creater").equals("") || map.get("pr_creater") == null) {
			rMap.put("pr_creater", "TERA");
		} else {
			rMap.put("pr_creater", map.get("pr_creater"));
		}
		rMap.put("pr_type", ProductRental.TYPE_INSTALL);
		rMap.put("pr_match", ProductRental.MATCHING);
		try {
			String code = getOldLEDRentalCode();
			String rcode = "";
			if (code == null) {
				rcode = "LEDEFU-00000001";
			} else {
				int no = Integer.parseInt(code.replace("LEDEFU-", "").toString());
				String codeno = String.format("%08d", no + 1);
				rcode = "LEDEFU-" + codeno;
			}
			rMap.put("pr_rentalcode", rcode);
			HashMap<String, Object> result = productDAO.insertrentalProduct(rMap);
			pr_idx = result.get("pr_idx").toString();
			List<HashMap<String, Object>> prpList = new ArrayList<HashMap<String, Object>>();
			for (String key : map.keySet()) {
				String id = "";
				if (key.startsWith("prp_num") == true) {
					HashMap<String, Object> prpmap = new HashMap<String, Object>();
					id = key.replace("prp_num", "");
					String num = map.get(key) + "";
					prpmap.put("p_id", Integer.parseInt(id));
					prpmap.put("prp_num", Integer.parseInt(num));
					prpmap.put("pr_idx", Integer.parseInt(pr_idx));
					prpList.add(prpmap);
				}
			}
			int result1 = productDAO.insertPRP(prpList);
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return productDAO.selectRenatalCode(pr_idx);

	}

	public List<Map<String, Object>> selectrentalList(Map<String, Object> map) {
		if (map == null) {
			map = new HashMap<String, Object>();
			map.put("column", "pr_idx");
			map.put("order", "desc");
			map.put("length", -1);
		} else {
			Object oc = map.get("order[0][column]");
			String order = map.get("columns[" + oc + "][data]") + "";
			String asc = map.get("order[0][dir]") + "";
			if (commonDAO.checkColumn(order) == 0) {
				return null;
			} else {
				map.put("column", order);
			}

			if (asc.equals("asc")) {
				map.put("order", "asc");
			} else {
				map.put("order", "desc");
			}
		}

		return productDAO.selectProductRentalList(map);
	}

	/**
	 * 렌탈상품 총수량 검색
	 */
	public Object countrentalProductList(Map<String, Object> map) {
		return productDAO.countrentalProductList(map);
	}

	public Map<String, Object> selectRentallistDetail(String pr_rentalcode) {
		return productDAO.selectRentalDetail(pr_rentalcode);
	}

	/**
	 * 렌탈상품 정보조회
	 */
	public List<HashMap<String, Object>> selectRentalComDetail(String pr_rentalcode) {
		return productDAO.selectRentalComDetail(pr_rentalcode);
	}

	public List<HashMap<String, Object>> selectRentalComDetail2(int pr_idx) {
		return productDAO.selectRentalComDetail2(pr_idx);
	}

	/**
	 * 
	 * product_rental && product_rental_product 동시 삭제
	 */
	@Transactional
	public int deleteRentalProduct(Map<String, Object> map) {
		int result1 = 0;
		int result2 = 0;
		result1 = productDAO.deletePR(map);
		result2 = productDAO.deletePRP(map);
		if (result1 == 0 && result2 == 0) {
			return 0;
		} else {
			return 1;
		}
	}

	/**
	 * product_rental 수정 (update)
	 */
	public int updateRentalProduct(Map<String, Object> map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();

		rMap.put("pr_memo", map.get("pr_memo"));
		rMap.put("pr_idx", map.get("pr_idx"));
		rMap.put("pr_total", Common.replaceComma((String) map.get("pr_total")));
		rMap.put("pr_rental", Common.replaceComma((String) map.get("pr_rental")));
		rMap.put("pr_period", map.get("pr_period"));
		rMap.put("pr_ilsi", Common.replaceComma((String) map.get("pr_ilsi")));
		rMap.put("pr_aff", map.get("pr_aff"));
		rMap.put("pr_aff_cost", Common.replaceComma((String) map.get("pr_aff_cost")));
		rMap.put("pr_delivery1", Common.replaceComma((String) map.get("pr_delivery1")));
		rMap.put("pr_delivery2", Common.replaceComma((String) map.get("pr_delivery2")));
		rMap.put("pr_install1", Common.replaceComma((String) map.get("pr_install1")));
		rMap.put("pr_install2", Common.replaceComma((String) map.get("pr_install2")));
		rMap.put("pr_gubun", map.get("pr_gubun"));
		rMap.put("pr_rentalDC", Common.replaceComma((String) map.get("pr_rentalDC")));
		rMap.put("pr_rentalDCp", Common.replaceComma((String) map.get("pr_rentalDCp")));
		rMap.put("pr_rentalEXE", Common.replaceComma((String) map.get("pr_rentalEXE")));
		rMap.put("pr_modifier", ((HashMap)map.get("login")).get("e_name"));
		rMap.put("pr_mod_reason", map.get("pr_mod_reason"));
		return productDAO.updateProductRental(rMap);
	}

	/**
	 * LED렌탈상품 불러오기
	 */
	public List<HashMap<String, Object>> selectCalcList(int pRentGubun) {
		return productDAO.selectCalcList(pRentGubun);
	}

	/**
	 * 최신 LED렌탈 코드 조회
	 */
	public String getLEDRentalCode() {
		return productDAO.getLEDRentalCode();
	}

	/**
	 * 최신 LED렌탈 코드 조회
	 */
	public String getOldLEDRentalCode() {
		return productDAO.getOldLEDRentalCode();
	}

	/**
	 * 최신 렌탈코드조회
	 */
	public String getRentalCode() {
		return productDAO.getRentalCode();
	}

	//
	public List<Map<String, Object>> selectProductListApp(Map<String, Object> map) {
		return productDAO.selectProductListApp(map, ProductRental.P_RENT_GUBUN2);
	}

	//
	public List<HashMap<String, Object>> selectproductApp(int pr) {
		return productDAO.selectproductApp(pr);
	}

	// 렌탈코드로 member의 m_pr_idx 업데이트
	// public int updateMemberPridx(String rental_code) {
	// return productDAO.updateMemberPridx(rental_code);
	// }

	// 렌탈코드로 pr_idx 가져오기
	public Map<String, Object> selectpridx(String rental_code) {
		return productDAO.selectPrByCode(rental_code);
	}

	// 렌탈코드 가져오기
	public String getProductcode(String pr) {
		return productDAO.getProductcode(pr);
	}

	/**
	 * 렌탈상품 구성상품 보기
	 */
	public List<HashMap<String, Object>> rProductDetail(String pr_idx) {
		return productDAO.rProductDetail(pr_idx);
	}

	/** 기초상품조회 */
	public List<Map<String, Object>> selectBasicList(Map<String, Object> map) {
		if (map == null) {
			map = new HashMap<String, Object>();
			map.put("column", "pr_idx");
			map.put("order", "desc");
			map.put("length", -1);
		} else {
			Object oc = map.get("order[0][column]");
			String order = map.get("columns[" + oc + "][data]") + "";
			String asc = map.get("order[0][dir]") + "";
			if (commonDAO.checkColumn(order) == 0) {
				return null;
			} else {
				map.put("column", order);
			}

			if (asc.equals("asc")) {
				map.put("order", "asc");
			} else {
				map.put("order", "desc");
			}
		}

		return productDAO.selectBasicList(map);
	}

	/** 기초상품 전체수량 */
	public Object countbasicProductList(Map<String, Object> map) {
		return productDAO.countbasicProductList(map);
	}

	/** 상품 상품군 가져오기 */
	public List<HashMap<String, Object>> selectCategoryOfProduct() {
		return productDAO.selectCategoryOfProduct();
	}

	/** 기초상품등록 */
	public int productBasic(Map<String, Object> map) {

		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Date currentTime = new Date();
		String mTime = mSimpleDateFormat.format(currentTime);

		String p_code = productDAO.getBasicProductPcode();
		String code = "PAZZ" + mTime + "_";
		if (p_code == null || !p_code.contains(mTime)) {
			code = code + "0001";
		} else {
			int no = Integer.parseInt(p_code.replace(code, "").toString());
			String codeNo = String.format("%04d", no + 1);
			code = code + codeNo;
		}

		map.put("code", code);
		int result = productDAO.productBasic(map);

		return result;
	}

	/** 기초상품데이터 렌트타입 */
	public List<HashMap<String, Object>> selectP_rent_gubunOfProduct() {
		return productDAO.selectP_rent_gubunOfProduct();
	}

	/** 기초상품업데이트(등록되어지는 모든것) */
	public int updateProduct(Map<String, Object> map) {
		return productDAO.updateProduct(map);
	}

	/** 상품명 중복방지 */
	public int selectProudctNameDuplicate(Map<String, Object> map) {
		String pr_name = map.get("pr_name") + "";
		String gubun = map.get("gubun") + "";

		if (gubun.equals("2")) {
			pr_name = pr_name + "_GN퍼즐";
		} else if (gubun.equals("5")) {
			pr_name = pr_name + "_LEDE4U";
		}
		map.put("pr_name", pr_name);
		return productDAO.selectProudctNameDuplicate(map);
	}

	/**
	 * CS항목 생성중 CS소모품 상품불러오기
	 */
	public List<Map<String, Object>> selectProductOfCsList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";

		map.put("column", order);

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		return productDAO.selectProductOfCsList(map);
	}

	/**
	 * CS항목 생성중 CS소모품 상품불러오기
	 */
	public int countProductOfCsList(Map<String, Object> map) {
		return productDAO.countProductOfCsList(map);
	}
}
