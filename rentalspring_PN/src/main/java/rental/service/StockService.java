package rental.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rental.constant.ProductRental;
import rental.dao.CommonDAO;
import rental.dao.StockDAO;

@Service("stockService")
public class StockService {

	@Autowired
	StockDAO stockDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	ProductService productService;

	
	/**
	 * 기사재고 요청 등록
	 * */
	public int insertLedRequest(Map<String, Object> map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		for (int i = 1; i < calList.size() + 1; i++) {
			int j = 1;
			rMap.put("lsr_req" + i, map.get("prp_num" + calList.get(i - j).get("p_id")) + "");
		}
		
		rMap.put("e_id", map.get("e_id"));
		rMap.put("lsr_status", map.get("lsr_status"));
		rMap.put("file1", "");
		rMap.put("file2", "");
		rMap.put("file3", "");
		rMap.put("lsr_t_memo", map.get("lsr_t_memo"));
		rMap.put("e_name", map.get("e_name"));
		int result = stockDAO.insertLedRequest(rMap);
		return result;
	}

	/*
	 * public int insertLedProgress(Map<String, Object> map) { Map<String,Object>
	 * rMap = new HashMap<String, Object>(); //lsr_idx구해야함 int result =
	 * stockDAO.insertLedRequest(map);
	 * 
	 * List<HashMap<String,Object>> List = new ArrayList<HashMap<String,Object>>();
	 * for(String key:map.keySet()) { String id =""; if(key.startsWith("prp_num") ==
	 * true ) { HashMap<String,Object> remap = new HashMap<String , Object>(); id =
	 * key.replace("prp_num", ""); String num=map.get(key)+""; remap.put("p_id",
	 * Integer.parseInt(id)); remap.put("lp_num",Integer.parseInt(num));
	 * remap.put("e_id", map.get("e_id")); remap.put("lsr_idx", idx.get("lsr_idx"));
	 * List.add(remap); } } int result1 = stockDAO.insertLedPrs(List);
	 * System.out.println(map.entrySet()); return 0 ; }
	 */

	

	/**
	 * 재고요청 조회폼
	 * */
	public List<HashMap<String, Object>> selectRequestLedList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";

		if(order.equals("")) {
			order="lsr_idx";
		}else if(commonDAO.checkColumn(order)==0) {
			return null;
		}
		map.put("column", order);

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		return stockDAO.selectRequestLedList(map);
	}
	
	//LED재고요청카운트
	public int countRequestLedList(Map<String, Object> map) {
		return stockDAO.countRequestLedList(map);
	}
	
	

	/**
	 * 기사재고요청 처리량 등록폼
	 * */
	public Map<String, Object> selectRequestDetail(Map<String, Object> dmap) {
		return stockDAO.selectRequestDetail(dmap);
	}
	/**
	 * 재고량 파악 AJAX
	 * */
	public int checkStock(HashMap<String, Object> rMap) {
		return stockDAO.checkStock(rMap);
	}

	
	
	/**
	 * 기사재고요청 처리량 등록
	 * */
	@Transactional
	public int insertLedProgress(Map<String, Object> map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		HashMap<String, Object> stock = new HashMap<String, Object>();
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		for (int i = 1; i < calList.size() + 1; i++) {
			int j = 1;
			rMap.put("lsp_prs" + i, map.get("prp_num" + calList.get(i - j).get("p_id")));
			stock.put(calList.get(i - j).get("p_id").toString(), map.get("prp_num" + calList.get(i - j).get("p_id")));
		}
		rMap.put("e_id", map.get("lsr_eid"));
		rMap.put("lsr_status", map.get("lsr_status"));
		rMap.put("lsp_t_memo", map.get("lsp_t_memo"));
		rMap.put("e_name", map.get("lsr_e_name"));
		rMap.put("lsr_idx", map.get("lsrid")+"");
		try {
			 stockDAO.insertLedprogress(rMap);
			 stockDAO.updateAddTechnicianStock(rMap);
			 stockDAO.updateMinusStock(stock);
			 stockDAO.updateStatus(rMap);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
				
	}

	
	/**기사재고반환*/
	@Transactional
	public int insertLedReturn(Map<String, Object> map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		HashMap<String, Object> stock = new HashMap<String, Object>();
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		for (int i = 1; i < calList.size() + 1; i++) {
			int j = 1;
			rMap.put("lsp_prs" + i, map.get("prp_num" + calList.get(i - j).get("p_id")));
			stock.put(calList.get(i - j).get("p_id").toString(), map.get("prp_num" + calList.get(i - j).get("p_id")));
		}
		rMap.put("e_id", map.get("lsr_eid"));
		rMap.put("lsr_status", map.get("lsr_status"));
		rMap.put("lsp_t_memo", map.get("lsp_t_memo"));
		rMap.put("e_name", map.get("lsr_e_name"));
		rMap.put("lsr_idx", map.get("lsrid")+"");
		try {
			stockDAO.insertLedprogress(rMap);
			stockDAO.updateMinusTechnicianStock(rMap);
			stockDAO.updateAddStock(stock);
			stockDAO.updateStatus(rMap);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**기사 재고량 */
	public HashMap<String, Object> checkTechnicianStock(int eid) {
		return stockDAO.checkTechnicianStock( eid);
	}
	/**기사별 출고받은량*/
	public List<HashMap<String, Object>> reqStock(String e_id) {
		return stockDAO.reqStock(e_id);
	}

	/**재고 갱신*/
	public int stockRefresh(String e_id) {
		HashMap<String,Object> request = stockDAO.totalRequestStock(e_id);
		List<Map<String,Object>> installed = stockDAO.totalInstalledStock(e_id);
		HashMap<String,Object>rMap = new HashMap<String, Object>();
		if(!installed.isEmpty()) {
			int size = installed.size()+1;
			for(int i=1;i<size;i++) {
				int tled=Integer.parseInt(installed.get(i-1).get("prp_num")+"");
				int stock=Integer.parseInt(request.get("tled_led"+i)+"")-tled;
				rMap.put("tled_led"+i,stock);
			}
			rMap.put("e_id", e_id);
			return stockDAO.updateTledStock(rMap);
		}else {
			request.put("e_id",e_id);
			stockDAO.updateTledStock(request);
			return -2;
		}
	}
}
