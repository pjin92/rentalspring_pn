package rental.service;

import java.io.File;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import rental.common.Common;
import rental.common.ParamMap;
import rental.constant.CS;
import rental.constant.MemberInfo;
import rental.dao.CommonDAO;
import rental.dao.CsDAO;
import rental.dao.ProductDAO;

@Service("csService")
public class CsService {

	@Autowired
	CsDAO csDAO;
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ProductDAO productDAO;

	
	/**
	 * CS항목 보여주기
	 * */
	
	public List<Map<String, Object>> selectCsList(Map<String, Object> map) {
		Object oc = map.get("order[0][column]");
		String order = map.get("columns[" + oc + "][data]") + "";
		String asc = map.get("order[0][dir]") + "";
		if (commonDAO.checkColumn(order) == 0) {
			return null;
		} else {
			map.put("column", order);
		}

		if (asc.equals("asc")) {
			map.put("order", "asc");
		} else {
			map.put("order", "desc");
		}
		return csDAO.selectCsList(map);
	}

	public Object countrentalProductList(Map<String, Object> map) {
		return csDAO.countCsList(map);
	}
	
	/**
	 * CS항목 신규 등록
	 * @param csm_file2 
	 * */
	@Transactional
	public int insertCsManage(ParamMap map, MultipartFile csm_file) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		String csm_idx="";
		try {
			rMap.put("csm_installpee", Common.replaceComma(map.get("csm_installpee") + ""));
			rMap.put("csm_gubun", map.get("csm_gubun") + "");
			rMap.put("csm_status", map.get("csm_status") + "");
			rMap.put("csm_rental", map.get("csm_rental") + "");
			rMap.put("csm_name", map.get("csm_name") + "");
			rMap.put("csm_cycle", map.get("csm_cycle") + "");
			rMap.put("csm_reason", map.get("csm_reason") + "");
			rMap.put("csm_file", map.get("csm_file")+"");
			rMap.put("csm_modifier", ((HashMap) map.get("login")).get("e_name") + "");
			rMap.put("csm_creater", ((HashMap) map.get("login")).get("e_name") + "");
			
			csm_idx = csDAO.insertCsManage(rMap).get("csm_idx")+"";
			if(csm_file !=null) {
				String cs_path = CS.PATH+CS.CSM_FILE;
				File f=new File(cs_path);
				if(!f.exists()) {
					f.mkdirs();
				}
				String regex = ".*[\\[\\][:]\\\\/?[*]].*";
				String csm_name = map.get("csm_name")+"";
				if(csm_name.matches(regex)){  // text에 정규식에 있는 문자가 있다면 true 없다면 false 
					//대괄호는 소괄호로
					csm_name = csm_name.replaceAll("\\[", "\\(");
					csm_name = csm_name.replaceAll("\\]", "\\)");
					//나머지 특수문자는 제거
					csm_name = csm_name.replaceAll("[[:]\\\\/?[*]]", "");  
				}
				csm_file.transferTo(new File(cs_path+"/"+csm_idx+"_"+csm_name));
			}
			List<HashMap<String, Object>> csProductList = new ArrayList<HashMap<String, Object>>();
			for (String key : map.keySet()) {
				String id = "";
				if (key.startsWith("prp_num") == true) {
					HashMap<String, Object> prpmap = new HashMap<String, Object>();
					id = key.replace("prp_num", "");
					String num = map.get(key) + "";
					prpmap.put("p_id", Integer.parseInt(id));
					prpmap.put("csp_num", Integer.parseInt(num));
					prpmap.put("csm_idx", Integer.parseInt(csm_idx));
					csProductList.add(prpmap);
				}
			}
			int result1 = csDAO.insertCsProduct(csProductList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.parseInt(csm_idx);
		
	}
	
	/**
	 * csCompDetail의 CS정보
	 * */
	public Map<String, Object> selectCsManageDetail(int csm) {
		return csDAO.selectCsManageDetail(csm);
	}

	

	/**
	 * 해당설치건의 소모품 리스트
	 * */
	public List<HashMap<String, Object>> selectCsCompDetail(int csm) {
		return csDAO.selectCsCompDetail(csm);
	}

	/*
	 * cs_manage 수정
	 **/
	public int updateCs(Map<String, Object> map,MultipartFile csm_file) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		String csm_idx = map.get("csm_idx")+"";
		try {
			rMap.put("csm_memo", map.get("csm_memo"));
			rMap.put("csm_idx", csm_idx);
			rMap.put("csm_installpee", Common.replaceComma((String) map.get("csm_installpee")));
			rMap.put("csm_reason", map.get("csm_reason"));
			rMap.put("csm_gubun", map.get("csm_gubun"));
			rMap.put("csm_status", map.get("csm_status"));
			rMap.put("csm_rental", map.get("csm_rental"));
			rMap.put("csm_name", map.get("csm_name"));
			rMap.put("csm_cycle", map.get("csm_cycle"));
			rMap.put("csm_file", map.get("csm_file"));
			rMap.put("csm_modifier", ((HashMap) map.get("login")).get("e_name") + "");
			
			
			if(csm_file !=null) {
				String cs_path = CS.PATH+CS.CSM_FILE;
				File f=new File(cs_path);
				if(!f.exists()) {
					f.mkdirs();
				}
				String regex = ".*[\\[\\][:]\\\\/?[*]].*";
				String csm_name = map.get("csm_name")+"";
				if(csm_name.matches(regex)){  // text에 정규식에 있는 문자가 있다면 true 없다면 false 
					//대괄호는 소괄호로
					csm_name = csm_name.replaceAll("\\[", "\\(");
					csm_name = csm_name.replaceAll("\\]", "\\)");
					//나머지 특수문자는 제거
					csm_name = csm_name.replaceAll("[[:]\\\\/?[*]]", "");  
				}
				csm_file.transferTo(new File(cs_path+"/"+csm_idx+"_"+csm_name));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return csDAO.updateCs(rMap);
	}

	/**
	 * 렌탈상품 신규등록 -> CS항목 추가 (ajax로 해당CS정보 받아와서 데이터테이블로 추가)
	 * */
	public Map<String, Object> selectCsDetail(String csm_id) {
		int csm = Integer.parseInt(csm_id);
		return csDAO.selectCsManageDetail(csm);
	}

	/**
	 * 렌탈상품에서 CS항목 상세보기 
	 * */
	public Map<String, Object> selectRentalInfo(String pr_idx) {
		return csDAO.selectRentalInfo(pr_idx);
	}

	@Transactional
	public int insertCsMatch(Map<String, Object> map) {
		List<HashMap<String, Object>> csmtList = new ArrayList<HashMap<String, Object>>();
		int result = 0;
		int result1 = 0;
		try {
			for (String key : map.keySet()) {
				String id = "";
				if (key.startsWith("csmt_num") == true) {
					HashMap<String, Object> csmt = new HashMap<String, Object>();
					id = key.replace("csmt_num", "");
					String num = map.get(key) + "";
					csmt.put("pr_idx", map.get("pr_idx"));
					csmt.put("csm_idx", Integer.parseInt(id));
					csmt.put("csmt_num", Integer.parseInt(num));
					csmt.put("csmt_pr_idx", Integer.parseInt((String) map.get("pr_idx")));
					csmtList.add(csmt);
				}
			}
				result =csDAO.insertCsMatch(csmtList);
				result1 = csDAO.updateCsMatchStatus(map.get("pr_idx")+"");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result1;
	}

	public List<HashMap<String, Object>> selectCsMatch(String pr_idx) {
		// TODO Auto-generated method stub
		return csDAO.selectCsMatch(pr_idx);
	}

	public int insertCsProduct(ParamMap map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		rMap.put("csm_name", map.get("pr_name") + "");
		rMap.put("csm_gubun", "LED");
		rMap.put("csm_sycle", map.get("pr_period") + "");
		String e_name = "";
		
		
		if(map.get("login")==null || map.get("login").equals("")) {
			rMap.put("csm_modifier", "TERA");
			rMap.put("csm_creater", "TERA");
			
		}else {
			e_name=  ((HashMap) map.get("login")).get("e_name") + "";
			rMap.put("csm_creater", e_name);
			rMap.put("csm_modifier", e_name);
		}
		
		rMap.put("csm_status", "1");
		rMap.put("csm_memo", "");
		rMap.put("csm_rental","설치");

		 rMap.put("csm_file", map.get("csm_file")+"");
		HashMap<String, Object> result = csDAO.insertCsManage(rMap);
		String csm_idx = result.get("csm_idx").toString();
		List<HashMap<String, Object>> csProductList = new ArrayList<HashMap<String, Object>>();
		for (String key : map.keySet()) {
			String id = "";
			if (key.startsWith("prp_num") == true) {
				HashMap<String, Object> prpmap = new HashMap<String, Object>();
				id = key.replace("prp_num", "");
				String num = map.get(key) + "";
				prpmap.put("p_id", Integer.parseInt(id));
				prpmap.put("csp_num", Integer.parseInt(num));
				prpmap.put("csm_idx", Integer.parseInt(csm_idx));
				csProductList.add(prpmap);
			}
		}
		int result1 = csDAO.insertCsProduct(csProductList);
		return Integer.parseInt(csm_idx);
	}

	public int autoCsMatch(Map<String, Object> map) {
		String pr_idx = productDAO.selectPridx(map);
		System.out.println("csServcie prIdx ======="+pr_idx);
		map.put("pr_idx", Integer.parseInt(pr_idx));
		return csDAO.autocsMatch(map);
	}
}
