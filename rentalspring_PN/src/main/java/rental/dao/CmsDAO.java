package rental.dao;

import java.util.*;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("cmsDAO")
public class CmsDAO {
	@Autowired
	private SqlSessionTemplate pnservice;

	/**cms list 조회*/
	public List<Map<String,Object>> selectCmsList(Map<String,Object> map){
		if(map==null) {
			map=new HashMap<String, Object>();
			map.put("column", "cms_idx");
			map.put("order", "asc");
			map.put("length", -1);
		}
		return pnservice.selectList("selectCmsList", map);
	}
	
	public int countCms() {
		return pnservice.selectOne("countCms");
	}
	
	public int insertCms(Map<String,Object>map) {
		return pnservice.insert("insertCms", map);
	}
	
	/**cms_id로 정보 조회*/
	public Map<String,Object> selectCmsDetail(String cms_id){
		return pnservice.selectOne("selectCmsDetail", cms_id);
	}
	/**cms_idx로 정보 조회*/
	public Map<String,Object> selectCmsDetail(int cms_idx){
		return pnservice.selectOne("selectCmsDetailIdx", cms_idx);
	}
	
	/**update pay_send<br>
	 * ps_idx,ps_reason,ps_state*/
	public int updatePaySend(Map<String,Object>ps) {
		return pnservice.update("updatePaySend",ps);
	}
	
	/**m_idx로 기등록된 회원의 member,cms 정보 가져오기*/
	public Map<String,Object> selectCmsByMidx(int m_idx) {
		return pnservice.selectOne("selectCmsByMidx", m_idx);
	}
	
	/** #{cms_idx},#{m_idx},#{cr_type}*/
	public int insertCmsReal(Map<String,Object>map) {
		return pnservice.insert("insertCmsReal", map);
	}
	/** m_idx필수, #{cms_idx},#{cr_type}*/
	public int updateCmsReal(Map<String,Object>map) {
		return pnservice.update("updateCmsReal", map);
	}
	
	public int deleteCmsReal(int m_idx) {
		return pnservice.delete("deleteCmsReal", m_idx);
	}
	
	/**회원체크할 때 group by cms_id로 조회*/
	public List<Map<String,Object>> groupCmsId(){
		return pnservice.selectList("groupCmsId");
	}
}