package rental.dao;

import java.util.*;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.ParamMap;
import rental.constant.MemberInfo;

@Repository("deliveryDAO")
public class DeliveryDAO {
	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**ds_idx return. ds_state는 설치전 혹은 배송전을 입력.<br>
	 * ds_history 기본값 'Y'*/
	public int insertDeliveryState(String ds_state,String ds_history) {
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("ds_state", ds_state);
		map.put("ds_history", ds_history);
		pnservice.insert("insertDeliveryState",map);
		return Integer.parseInt(map.get("ds_idx")+"");
	}
	
	/**insertDelivery */
	public void insertDelivery(int mi_idx,int prp_idx,int ds_idx) {
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("mi_idx", mi_idx);
		map.put("prp_idx", prp_idx);
		map.put("ds_idx", ds_idx);
		pnservice.insert("insertDelivery",map);
	}
	
	/**delivery_state 조회<br>
	 * prp_idxs에는 prp_idx가 ,를 구분자로 들어감(추후 확장성 위해)<br>
	 * 배송자가 2이상인 경우를 위해 list로 받아옴*/
	public List<Map<String,Object>> selectDeliveryState(int m_idx) {
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("mt_code",MemberInfo.TYPE_DELIVERY);
		return pnservice.selectList("selectDeliveryState",map);
	}
	
	/**delivery_state 조회<br>
	 * ds_idx로 조회*/
	public Map<String,Object> selectDeliveryStateDetail(int ds_idx) {
		return pnservice.selectOne("selectDeliveryStateDetail",ds_idx);
	}
	/**delivery_state 조회<br>
	 * ds_history로 조회 (ds_idx)*/
	public Map<String,Object> selectDeliveryStateByHis(int ds_idx) {
		return pnservice.selectOne("selectDeliveryStateByHis",ds_idx+"");
	}
	
	/**ds_idx로 delivery에 있는 정보 조회*/
	public List<Map<String,Object>> selectDeliveryListByDs(int ds_idx) {
		return pnservice.selectList("selectDeliveryListByDs",ds_idx);
	}
	
	/**delivery_state ds_idx로 정보 업데이트하는 메서드*/
	public int updateDeliveryState(Map<String,Object>map) {
		//System.out.println("업데이트딜리버리스테이트"+map.entrySet());
		//m_eta와 ds_req맞추기
		String ds_req=(String)map.get("ds_req");
		if(ds_req!=null&&!ds_req.equals("")) {
			int ds_idx=Integer.parseInt(map.get("ds_idx")+"");
			int m_idx=pnservice.selectOne("selectMbyDs",ds_idx);
			Map<String,Object> mmap=new HashMap<String, Object>();
			mmap.put("m_idx", m_idx);
			mmap.put("m_eta", ds_req);
			pnservice.update("updateMember",mmap);
		}
		
		return pnservice.update("updateDeliveryState", map);
	}
	
	/**ds_idx로 delivery_state 삭제*/
	public void deleteDeliveryState(int ds_idx) {
		pnservice.delete("deleteDeliveryState",ds_idx);
	}
	/**ds_idx로 delivery 삭제*/
	public void deleteDelivery(int ds_idx) {
		pnservice.delete("deleteDelivery",ds_idx);
	}
	
	/**설치관리 검색(dt)*/
	public List<Map<String, Object>> selectInstallList(Map<String, Object> map) {
		System.out.println(map.entrySet());
		return pnservice.selectList("selectInstallList",map);
	}
	/**설치관리 검색(dt) cnt*/
	public int countInstallList(Map<String, Object> map) {
		return pnservice.selectOne("countInstallList",map);
	}
	/**기사배정*/
	public int insertMatchTechnician(Map<String, Object> map) {
		return pnservice.insert("insertMatchTechnician",map);
	}

	public int checkMatchTechnician(Map<String, Object> map) {
		return pnservice.selectOne("checkMatchTechnician", map);
	}

	public int deleteInstall_match(Map<String, Object> map) {
		return pnservice.delete("deleteMatchTechnician",map);
	}

	public int selectdsIdx(Map<String, Object> map) {
		return pnservice.selectOne("selectdsIdx",map);
	}

	public int updateNewStatus(String mi_idx) {
		return pnservice.update("updateNewStatus",mi_idx);
	}
	
	/**배송관리 조회*/
	public List<Map<String,Object>> selectCarryList(Map<String,Object> map){
		return pnservice.selectList("selectCarryList",map);
	}
	/**배송관리 조회 cnt (dt)*/
	public int countCarryList(Map<String,Object> map){
		return pnservice.selectOne("countCarryList",map);
	}
	
	/*ds_idx로 ds history조회(반품 등등.. 인 경우 배송완료 정보)**/
	public Map<String,Object> selectDsHistory(String ds_idx){
		return pnservice.selectOne("selectDsHistory",ds_idx);
	}
	/**DS상태값 리스트*/
	public List<HashMap<String, Object>> selectds_state() {
		return pnservice.selectList("selectds_state");
	}

}
