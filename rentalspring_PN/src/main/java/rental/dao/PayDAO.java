package rental.dao;

import java.util.*;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.Log;
import rental.constant.MemberInfo;
import rental.constant.Pay;

@Repository("payDAO")
public class PayDAO {
	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**m_idx로 pay count*/
	public int countPayByMidx(int m_idx) {
		return pnservice.selectOne("countPayByMidx",m_idx);
	}
	
	/**pay insert method<br>
	 * 같은 회차 신청전,출금완료 있는 경우 insert 안됨.<br>
	 * pay_cnt2는 map에 없어도 됨.*/
	synchronized public int insertPay(Map<String,Object>map) {
		int count=pnservice.selectOne("countBeforInsertPay", map);
		if(count>0) {
			return 1;
		}
		
		if(!map.containsKey("pay_carryover_time")) {
			map.put("pay_carryover_time", 0);
		}
		
		map.put("pay_method", "");
		int pay_cnt2=pnservice.selectOne("maxPayCnt2", map);
		map.put("pay_cnt2", pay_cnt2+1);
		return pnservice.insert("insertPay", map);
	}

	/**m_idx로 pay조회.length 마이너스1이면 전부*/
	public List<Map<String,Object>> selectPayListByMidx(int m_idx,int start,int length){
		Map<String,Integer>map=new HashMap<String, Integer>();
		map.put("m_idx", m_idx);
		map.put("start", start);
		map.put("length", length);
		return pnservice.selectList("selectPayListByMidx", map);
	}
	
	/**insertPaySend
	 * ps_pay_idx가 0이하인 것은 등록,수정..<br>
	 * #{pay_idx},#{m_idx},#{cms_idx},#{m_pay_method}
	 * ,#{m_pay_owner},#{m_rental},#{ps_date},#{m_pay_info},#{m_pay_num}
	 * ,#{m_pay_id},#{ps_reason},#{ps_state}*/
	public int insertPaySend(Map<String,Object>map) {
		return pnservice.insert("insertPaySend", map);
	}
	
	/**은행회원등록신청 리스트*/
	public List<Map<String,Object>> selectPaySendStatistic(Map<String,Object>map) {
		return pnservice.selectList("selectPaySendStatistic",map);
	}
	/**은행출금신청 (날짜별 )리스트*/
	public List<Map<String,Object>> selectPayStatistic(Map<String,Object>map) {
		return pnservice.selectList("selectPayStatistic",map);
	}
	
	/**은행등록신청한 날짜count*/
	public int countPaySendStatistic() {
		return pnservice.selectOne("countPaySendStatistic");
	}
	/**은행출금 신청한 날짜count*/
	public int countPayStatistic() {
		return pnservice.selectOne("countPayStatistic");
	}
	
	/**은행회원 등록,수정시 이미 신청전,신청중인지 확인하는 메서드*/
	public int countPaySendState(int m_idx) {
		return pnservice.selectOne("countPaySendState",m_idx);
	}
	/**pay에서 신청전,신청중 상태 개수 조회*/
	public int countUnpaid(int m_idx) {
		return pnservice.selectOne("countUnpaid",m_idx);
	}
	/**은행CMS 신청한 목록 가져오기(날짜 기준)<br>
	 * type==null인 경우 출금. not null인 경우 등록,수정 데이터*/
	public List<Map<String,Object>> selectPaySendByDate(Map<String,Object>map,String ps_date,String type) {
		if(map==null) {
			map=new HashMap<String, Object>();
			map.put("length", -1);
			map.put("column","ps_cms_idx");
			map.put("order","asc");
		}
		map.put("ps_date", ps_date);
		if(type!=null) {
			map.put("pay", "등록데이터만");
		}
		String keyword=(String)map.get("keyword");
		if(keyword!=null) {
			if(keyword.equals("%등%")||keyword.equals("%등록%")||keyword.equals("%록%")) {
				map.put("keywordReg", "등록");
			}else if(keyword.equals("%수%")||keyword.equals("%수정%")||keyword.equals("%정%")) {
				map.put("keywordMod", "수정");
			}
		}
		return pnservice.selectList("selectPaySendByDate",map);
	}
	/**은행CMS 신청한 count가져오기(날짜 기준)<br>
	 * type==null인 경우 출금. not null인 경우 등록,수정 데이터*/
	public int countPaySendByDate(String ps_date,String type) {
		Map<String,String> map=new HashMap<String, String>();
		map.put("ps_date", ps_date);
		if(type!=null) {
			map.put("type", "출금데이터만");
		}
		return pnservice.selectOne("countPaySendByDate",map);
	}
	
	/**ps_idx로 pay_send 조회*/
	public Map<String,Object> selectPaySendDetail(int ps_idx){
		return pnservice.selectOne("selectPaySendDetail",ps_idx);
	}
	
	/**ps_idx로 delete. ps_state==신청전 인경우에만*/
	public int deletePaySend(int ps_idx) {
		return pnservice.delete("deletePaySend", ps_idx);
	}
	
	/**update PaySend ps_idx,ps_state,ps_reason*/
	public int updatePaySend(Map<String,Object> map) {
		return pnservice.update("updatePaySend", map);
	}
	
	/**pay_year 최소,최대값. key= min,max*/
	public Map<String,Object> payYearRange(){
		return pnservice.selectOne("payYearRange");
	}
	
	/**pay List. m_pay_method 필수*/
	public List<Map<String,Object>> selectPayList(Map<String,Object> map){
		String m_pay_method=map.get("m_pay_method")+"";
		if(m_pay_method.equals(MemberInfo.PAY_METHOD_CARD)) {
			map.put("pay_method", Pay.METHOD_CARD);
		}else {
			map.put("pay_method", Pay.METHOD_BANK);
		}
		return pnservice.selectList("selectPayList",map);
	}
	
	/**count pay List. m_pay_method 필수*/
	public int countPayList(Map<String,Object> map){
		return pnservice.selectOne("countPayList",map);
	}
	
	public void updatePay(Map<String,Object> map) {
		pnservice.update("updatePay",map);
	}
	
	/**pay,member + cms_real 에서 cms_idx,cr_type 가져옴 */
	public Map<String,Object> selectPayDetail(int pay_idx) {
		return pnservice.selectOne("selectPayDetail",pay_idx);
	}
	
	/**pay_idx로 신청중 혹은 바구니에 담겨있는 출금신청이 있는지 확인*/
	public int countPaySendByPayIdx(int pay_idx) {
		return pnservice.selectOne("countPaySendByPayIdx",pay_idx);
	}
	
	/**sub_bank 코드로 은행명 가져오기*/
	public String selectSubBankByCode(String sb_code) {
		return pnservice.selectOne("selectSubBankByCode", sb_code);
	}
	
	/**sub_bank 은행이름으로 코드 가져오기.<br>
	 * String code에는 sb_code,sb_silsigan,sb_v 입력*/
	public String selectSubBank(String sb_name,String code) {
		Map<String,Object>map=pnservice.selectOne("selectSubBank", sb_name);
		if(map==null) {
			return null;
		}else {
			return map.get(code)+"";
		}
	}
	/**sub_bank list*/
	public List<Map<String,Object>> selectSubBankList() {
		return pnservice.selectList("selectSubBankList");
	}
	
	/**sub_card list. 통일성 위해 sb_name에도 카드 명 담음*/
	public List<Map<String,Object>> selectSubCardList() {
		return pnservice.selectList("selectSubCardList");
	}
	
	/**pay_date에 따른 실시간 출금 연번 입력.*/
	synchronized public String insertPayNum(int pay_idx,String pay_date,String cms_id) {
		int count=pnservice.selectOne("countPayNum",pay_date);
		count++;
		Map<String,Object> startPay=new HashMap<String, Object>();
		startPay.put("pay_idx", pay_idx);
		startPay.put("pay_date", pay_date);
		startPay.put("pay_state", Pay.STATE_ON_PROCESS);
		startPay.put("pay_reason", Pay.REASON_REALTIME+"중");
		startPay.put("pay_num", Pay.NUM_REALTIME+pay_idx+""+count);
		startPay.put("pay_method", Pay.METHOD_BANK);
		startPay.put("pay_bigo", cms_id);
		
		pnservice.update("updatePay", startPay);
		return  Pay.NUM_REALTIME+pay_idx+""+count;
	}
	
	/**채권관리 데이터 조회*/
	public List<Map<String,Object>> withdrawStatistic(Map<String,Object> map){
		return pnservice.selectList("withdrawStatistic",map);
	}
	
	/**pay의 고객 수 count(연체관리 검색조건)*/
	public int countWithdrawStatistic(Map<String,Object> map){
		return pnservice.selectOne("countWithdrawStatistic",map);
	}
	
	/**m_idx로 각 pay_cnt의 max(pay_cnt2)의 기록만 가져오기 (통계용)*/
	public List<Map<String,Object>> selectPayListWithMaxCnt2(int m_idx){
		return pnservice.selectList("selectPayListWithMaxCnt2",m_idx);
	}
	
	/**m_idx,pay_cnt의 가장 최근(max pay_cnt2) pay 정보 가져오기*/
	public Map<String,Object> selectPayCntRecent(int m_idx,int pay_cnt){
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("pay_cnt", pay_cnt);
		return pnservice.selectOne("selectPayCntRecent",map);
	}
	
	/**결제시작일 변경(pay 1회차부터 차례로)<br>
	 * m_idx,pay_year,pay_month,m_paystart*/
	public void updatePayStart(Map<String,Object> map) {
		pnservice.update("updatePayStart",map);
	}
	/**m_idx로 연체금 가져오는 메서드(연체관리의 dt쿼리에서 1건만 가져오도록 수정한 쿼리)*/
	public Map<String,Object> selectMemberDelayMoney(int m_idx){
		return pnservice.selectOne("selectMemberDelayMoney",m_idx);
	}
	/**m_idx로 pay_original_date 가장 빠른날짜 조회(신청전 상태만).최초 연체일자 확인에 사용*/
	public String selectPayDelayDate(int m_idx) {
		return pnservice.selectOne("selectPayDelayDate",m_idx);
	}
	
	/**pay_num으로 pay조회(가상계좌 입금시 pay_num에 가상계좌번호 저장)*/
	public List<Map<String,Object>> selectPayNumList(String pay_bigo){
		return pnservice.selectList("selectPayNumList",pay_bigo);
	}
	/**m_idx로 가장 낮은 회차순으로 신청전 상태의 데이터 조회*/
	synchronized public List<Map<String,Object>> selectUnpaidList(int m_idx){
		return pnservice.selectList("selectUnpaidList",m_idx);
	}
	
	/**은행 sms전송하기위한 데이터 조회*/
	public List<Map<String,Object>> selectPaySendSmsList(String ps_date){
		return pnservice.selectList("selectPaySendSmsList",ps_date);
	}
	
	/**신청중인 상태 count*/
	public int countOnProcess(int m_idx) {
		return pnservice.selectOne("countOnProcess",m_idx);
	}
	
	/**중도상환,해지 등의 경우 신청전의 pay 데이터 pay_state 변경<br>
	 * 신청중이 있는 경우 해지 불가*/
	public void noMoreRental(int m_idx,String pay_state) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("pay_state", pay_state);
		pnservice.update("noMoreRental",map);
	}
	
	/**중도상환,해지 등... 신청전으로 pay 데이터 pay_state 변경*/
	public void continueRental(int m_idx) {
		pnservice.update("continueRental",m_idx);
	}
	
	/**이체 희망일 변경에 따른 pay.pay_original_date 변경*/
	public void updatePayOriginalDate(int m_idx,String m_paydate) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("m_paydate", m_paydate);
		pnservice.update("updatePayOriginalDate",map);
	}
	
}