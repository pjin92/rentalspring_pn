package rental.dao;

import java.util.*;

import org.apache.poi.ss.formula.functions.Vlookup;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rental.common.ParamMap;

@Repository("csDAO")
public class CsDAO {
	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**
	 * 렌탈상품CS 항목 등록 시 CS항목 리스트 추가 화면 보여주기
	 * */
	public List<Map<String, Object>> selectCsList(Map<String, Object> map) {
		String mainKeyword=(String)map.get("csm_name");
		String keyword = (String)map.get("keyword");
		String gubun = (String)map.get("csm_gubun");
		String status = (String)map.get("csm_status");
		String expend = (String)map.get("csm_expend");
		
		
		if(mainKeyword==null) {
			mainKeyword="";
		}else {
			mainKeyword=mainKeyword.replaceAll("%", "");
		}
		
		if(keyword==null) {
			keyword="";
		}else {
			keyword=keyword.replaceAll("%", "");
		}
		
		if(expend ==null || expend.equals("")) {
			expend =null;
		}
		if(gubun == null || gubun.equals("")) {
			gubun="";
		}
		
		
		map.put("mainKeyword", "%"+mainKeyword+"%");
		map.put("keyword", "%"+keyword+"%");
		map.put("expend", expend);
		map.put("gubun", "%"+gubun+"%");
		return pnservice.selectList("selectCsList", map);
	}
	/**
	 * cs_manage에 cs정보 추가하기
	 * */
	public HashMap<String, Object> insertCsManage(HashMap<String, Object> rMap) {
		pnservice.insert("insertCsManage", rMap);
		return rMap; 
	}

	
	/**
	 * cs_product에 렌탈소모품 추가하기
	 * */
	public int insertCsProduct(List<HashMap<String, Object>> csProductList) {
		int result = 0;
		for(int i=0;i<csProductList.size();i++) {
			 result = pnservice.insert("insertCsProduct",csProductList.get(i));
		}
		return result;

	}

	
	/**
	 * csCompDetail의 CS정보
	 * */
	public Map<String, Object> selectCsManageDetail(int csm) {
		return pnservice.selectOne("selectCsManageDetail",csm);
	}

	/**
	 * 해당설치건의 소모품 리스트
	 * */
	public List<HashMap<String, Object>> selectCsCompDetail(int csm) {
		List<HashMap<String, Object>> selectCsCompDetail = new ArrayList<HashMap<String,Object>>();
		selectCsCompDetail = pnservice.selectList("selectCsCompDetail",csm);
		return selectCsCompDetail;
	}
	/**
	 * cs_manage update
	 * */
	public int updateCs(HashMap<String, Object> rMap) {
		return pnservice.update("updateCs",rMap);
	}

	/**
	 * 렌탈상품에서 CS항목 상세보기 
	 * */
	public Map<String, Object> selectRentalInfo(String pr_idx) {
		return pnservice.selectOne("selectRentalInfo",pr_idx);
	}
	
	/**
	 *  렌탈상품과 CS상품 매칭
	 * */
	public int insertCsMatch(List<HashMap<String, Object>> csmtList) {
		int result =0;
		for(int i=0;i<csmtList.size();i++) {
			result = pnservice.insert("insertCsMatch", csmtList.get(i));
		}
		return result;

	}
	/**
	 *  렌탈상품과 CS상품 매칭 후 렌탈상품 매칭상태 update
	 * */
	public int updateCsMatchStatus(String pr_idx) {
		return pnservice.update("updateCsMatchStatus", pr_idx);
	}
	

	/**
	 * 	CS매칭된 렌탈상품들의 CS목록 불러오기
	 * */

	public List<HashMap<String, Object>> selectCsMatch(String pr_idx) {
		return pnservice.selectList("selectCsMatch" ,pr_idx);
	}


	public Object countCsList(Map<String, Object> map) {
		String mainKeyword=(String)map.get("csm_name");
		String keyword = (String)map.get("keyword");
		String gubun = (String)map.get("csm_gubun");
		String status = (String)map.get("csm_status");
		String expend = (String)map.get("csm_expend");
		
		
		if(mainKeyword==null) {
			mainKeyword="";
		}else {
			mainKeyword=mainKeyword.replaceAll("%", "");
		}
		
		if(keyword==null) {
			keyword="";
		}else {
			keyword=keyword.replaceAll("%", "");
		}
		
		if(expend ==null || expend.equals("")) {
			expend =null;
		}
		if(gubun == null || gubun.equals("")) {
			gubun="";
		}
		
		
		
		map.put("mainKeyword", "%"+mainKeyword+"%");
		map.put("keyword", "%"+keyword+"%");
		map.put("expend", expend);
		map.put("gubun", "%"+gubun+"%");
		return pnservice.selectOne("countCsList",map);
	}
	public int autocsMatch(Map<String, Object> map) {
		return pnservice.insert("autocsMatch",map);
	}

}
