package rental.dao;

import java.text.SimpleDateFormat;
import java.util.*;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.Log;
import rental.constant.Sms;
import whois.whoisSMS;

@Repository("smsDAO")
public class SmsDAO {

	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**sms_idx로 sms id,pw가져오기*/
	public Map<String,Object> selectSmsDetail(int sms_idx){
		return pnservice.selectOne("selectSmsDetail", sms_idx);
	}
	
	/**db에 발송전 insert<br>
	 * int sms_idx,int m_idx,String sms_to,String sms_from,String sms_msg,String sms_date,int e_id,String sh_type*/
	public void insertSmsHistory(Map<String,Object>map) {
		map.put("sh_resultcode", Sms.RESULTCODE_BEFORE);
		pnservice.insert("insertSmsHistory",map);
	}
	
	/**sms 결과반영*/
	public void updateSmsHistory(int sh_idx,int sh_resultcode) {
		Map<String,Object>map=new HashMap<String, Object>();
		map.put("sh_idx", sh_idx);
		map.put("sh_resultcode", sh_resultcode);
		pnservice.update("updateSmsHistory",map);
	}
	
	/**sms발송내역 리스트(dataTable)*/
	public List<Map<String,Object>> selectSmsHistoryList(Map<String,Object> map){
		return pnservice.selectList("selectSmsHistoryList", map);
	}
	
	/**count sms발송내역 리스트(dataTable)*/
	public int countSmsHistoryList(){
		return pnservice.selectOne("countSmsHistoryList");
	}
}
