package rental.dao;

import java.util.*;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.ParamMap;

@Repository("stockDAO")
public class StockDAO {
	
	@Autowired
	private SqlSessionTemplate pnservice;

	
	/**
	 * 기사재고 요청 등록
	 * */
	public int insertLedRequest(Map<String, Object> rMap) {
		return pnservice.insert("insertLedRequest",rMap);
	}

	//기사재고요청리스트
	public List<HashMap<String, Object>> selectRequestLedList(Map<String, Object> map) {
		String lsr_creater=(String)map.get("lsr_creater");
		String lsr_status = (String)map.get("lsr_status");
		if(lsr_creater==null) {
			lsr_creater="";
		}else {
			lsr_creater=lsr_creater.replaceAll("%", "");
		}
		
		if(lsr_status==null) {
			lsr_status="";
		}
		
		map.put("lsr_creater", "%"+lsr_creater+"%");
		map.put("lsr_status", lsr_status);
		return pnservice.selectList("selectRequestLedList", map);
	}
	//기사재고요청카운트
	public int countRequestLedList(Map<String, Object> map) {
		return pnservice.selectOne("countRequestLedList", map);
	}

	public Map<String, Object> selectRequestDetail(Map<String, Object> dmap) {
		return pnservice.selectOne("selectRequestDetail",dmap);
	}

	public int checkStock(HashMap<String, Object> rMap) {
		return pnservice.selectOne("checkStock",rMap);
	}

	public int updateLedStock(List<HashMap<String, Object>> prpList) {
		int result= 0;
		for(int i=0;i<prpList.size();i++) {
		result = pnservice.update("updateStock",prpList.get(i));
		}
		return result;
	}

	public int updateAddTechnicianStock(HashMap<String, Object> rMap) {
		return pnservice.update("updateAddTechnicianStock",rMap);
	}

	public int insertLedReq(HashMap<String, Object> rMap) {
		return	 pnservice.insert("insertLedReq",rMap);
		 
	}

	/**
	 * 기사재고요청 처리량 등록
	 * */
	public int insertLedprogress(HashMap<String, Object> rMap) {
		return	 pnservice.insert("insertLedprogress",rMap);
	}

	public int updateMinusStock(HashMap<String, Object> List) {
		int result =0;
			for(String key : List.keySet()) {
				HashMap<String,Object> map = new HashMap<String, Object>();
				map.put("key", key);
				map.put("value",List.get(key));
				result = pnservice.update("updateMinusStock" ,map);
				map.clear();
			}
		
		return result;
	}

	public int updateMinusTechnicianStock(HashMap<String, Object> rMap) {
//		System.out.println(rMap.entrySet());
		return pnservice.update("updateMinusTechnicianStock",rMap);
	}

	public int updateAddStock(HashMap<String, Object> Stock) {
		int result =0;
		for(String key : Stock.keySet()) {
			HashMap<String,Object> map = new HashMap<String, Object>();
			map.put("key", key);
			map.put("value",Stock.get(key));
			
			result = pnservice.update("updateAddStock" ,map);
			map.clear();
		}
	
	return result;
	}

	public int updateStatus(HashMap<String, Object> rMap) {
		return pnservice.update("updateStatus",rMap);
	}

	/**기사 재고량*/
	public HashMap<String, Object> checkTechnicianStock(int eid) {
		return pnservice.selectOne("checkTechnicianStock",eid);
	}

	/**재고 요청량*/
	public List<HashMap<String, Object>> reqStock(String e_id) {
		return pnservice.selectList("reqStock",e_id);
	}

	/**신LED설치완료시 재고 차감*/
	public int installedStock(Map<String, Object> istock) {
		return pnservice.update("installedTechnicianStock",istock);
	}
	
	/**기사별 출고받은량*/
	public HashMap<String, Object> totalRequestStock (String e_id){
		return pnservice.selectOne("totalRequestStock",e_id);
	}
	
	/**기사별 총 설치량**/
	public List<Map<String,Object>> totalInstalledStock(String e_id){
		return pnservice.selectList("totalInstalledStock",e_id);
	}

	/**기사별 재고 업데이트*/
	public int updateTledStock(HashMap<String, Object> rMap) {
		return pnservice.update("updateTledStock",rMap);
	}
}
