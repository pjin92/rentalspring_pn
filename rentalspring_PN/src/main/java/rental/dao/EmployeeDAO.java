
package rental.dao;

import java.util.*;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.ParamMap;

@Repository("employeeDAO")
public class EmployeeDAO {
	
	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**로그인 체크하는 메서드.
	 * e_id,e_name,e_branch,e_department,e_position,e_power,e_logid,e_pass
	 * */
	public Map<String, Object> login(String e_logid){
		return pnservice.selectOne("loginCheck", e_logid);
	}
	/**부서로 employee정보 가져오기. null인경우 모든 정보 조회*/
	public List<Map<String,Object>> selectEmployeeListByDepartment(String e_department) {
		Map<String,Object> param=new HashMap<String, Object>();
		if(!(e_department==null||e_department.equals(""))) {
			param.put("e_department", e_department);
		}
		return pnservice.selectList("employeeListByDepartment", param);
	}
	/**e_id로 employee정보 가져오기 */
	public Map<String, Object> selectEmployeeDetail(int e_id){
		return pnservice.selectOne("selectEmployeeDetail", e_id);
	}

	public int insertTechnician(Map<String, Object> map) {
		return 	pnservice.insert("insertTechnician",map); 
	}

	public List<Map<String, Object>> selectTechList(Map<String, Object> map) {
		return	pnservice.selectList("selectTechList", map);
	}

	public Object countTechList(Map<String, Object> map,int techAuth) {
		return pnservice.selectOne("countTechList",map);
	}

	public Map<String, Object> selectTechnicianDetail(int tidx) {
		return pnservice.selectOne("selectTechnicianDetail",tidx);
	}

	public int insertTechnicianStock(String eid) {
		
		return pnservice.insert("insertTechnicianStock",eid);
	}
	/**
	 * 기사리스트
	 * */
	public List<Map<String, Object>> selecttechnicianList(Map<String, Object> map) {
		return	pnservice.selectList("selecttechnicianList", map);
	}
	
	/**부서 리스트*/
	public List<String> selectDeptList(){
		return pnservice.selectList("selectDeptList");
	}
	public List<HashMap<String, Object>> selectCity() {
		return pnservice.selectList("selectCity");
	}
	public List<HashMap<String, Object>> selectDistrict(String province) {
		return pnservice.selectList("selectDistrict",province);
	}
	public int updateTechnicianInfo(Map<String, Object> map) {
		return pnservice.update("updateTechnicianInfo",map);
	}
	
	/**고객사 로그인. 로그인 id로 정보 조회*/
	public Map<String, Object> customerLogin(String clogin_id) {
		return pnservice.selectOne("customerLogin",clogin_id);
	}
	
	/**sha2 512로 해쉬하는 메서드(sql function사용)*/
	public String selectSha2(String e_pass) {
		return pnservice.selectOne("selectSha2",e_pass);
	}
	
	/**임직원관리 조회 dataTable용*/
	public List<Map<String,Object>> selectEmployeeList(Map<String,Object> map){
		return pnservice.selectList("selectEmployeeList",map);
	}
	/**임직원관리 조회 dataTable용 count*/
	public int countEmployeeList(){
		return pnservice.selectOne("countEmployeeList");
	}
	/**직원등록시 e_number중복확인*/
	public int checkId(String e_number){
		return pnservice.selectOne("checkId",e_number);
	}
	/**insert emp*/
	public void insertEmployee(Map<String,Object> map) {
		pnservice.insert("insertEmployee", map);
		pnservice.update("updateEmployee",map);
	}
	/**update emp*/
	public int updateEmployee(Map<String,Object> map) {
		return pnservice.update("updateEmployee",map);
	}
	
	/**employee정보 전부 가져오기 By e_id*/
	public Map<String, Object> selectEmployeeInfoDetail(String e_id) {
		return pnservice.selectOne("selectEmployeeInfoDetail",e_id);
	}
	
	/**거래처관리 DT 리스트 */
	public List<Map<String, Object>> selectCustomerList(Map<String, Object> map) {
		String mainKeyword = (String) map.get("mainKeyword");
		
		
		if (mainKeyword == null) {
			mainKeyword = "";
		} else {
			mainKeyword = mainKeyword.replaceAll("%", "");
		}
		map.put("mainKeyword", "%" + mainKeyword + "%");
		return pnservice.selectList("selectCustomerList",map);
	}
	/**거래처관리 DT 리스트 */
	public int countCustomerList() {
		return pnservice.selectOne("countCustomerList");
	}
	/**거래처 등록*/
	public int insertCustomer(Map<String, Object> map) {
		pnservice.insert("insertCustomer",map);
		return Integer.parseInt(map.get("c_id")+"");
	}
	/**거래처>category1 list*/
	public List<HashMap<String, Object>> selectCategory() {
		return pnservice.selectList("selectCategory");
	}
	/**거래처> c_type리스트 */
	public List<HashMap<String, Object>> selectType() {
		return pnservice.selectList("selectType");
	}
	/**거래처>c_status리스트*/
	public List<HashMap<String, Object>> selectState() {
		return pnservice.selectList("selectState");
	}
	/**거래처 상세보기*/
	public Map<String, Object> selectCustomerInfoDetail(String c_id) {
		return pnservice.selectOne("selectCustomerInfoDetail",c_id);
	}
	/**거래처 update*/
	public int updateCustomer(Map<String, Object> map) {
		return pnservice.update("updateCustomer",map);
	}
	/**직급리스트*/
	public List<HashMap<String, Object>> selectPosition() {
		return pnservice.selectList("selectPosition");
	}
	/**직무리스트*/
	public List<HashMap<String, Object>> selectJob() {
		return pnservice.selectList("selectJob");
	}
	/**권한리스트*/
	public List<HashMap<String, Object>> selectPower() {
		return pnservice.selectList("selectPower");
	}
	/**소속리스트*/
	public List<HashMap<String, Object>> selectBranch() {
		return pnservice.selectList("selectBranch");
	}
	/**부서리스트*/
	public List<HashMap<String, Object>> selectDepartment() {
		return pnservice.selectList("selectDepartment");
	}
	/**거래처 이력관리 추가*/
	public int insertCustomerHistory(Map<String, Object> map) {
		return pnservice.insert("insertCustomerHistory",map);
	}
	/**거래처 변경 리스트*/
	public List<Map<String, Object>> selectCustomerModifyList(Map<String, Object> map) {
		return pnservice.selectList("selectCustomerModifyList",map);
	}
	/**선택된 거래처 이력 상세보기*/
	public Map<String, Object> selectCustomerHistory(String ch_idx) {
		return pnservice.selectOne("selectCustomerHistory",ch_idx);
	}
	/**선택된 거래처 이력 이전 수정버전*/
	public Map<String, Object> selectCustomerPrvHistory(Map<String, Object> imap) {
		return pnservice.selectOne("selectCustomerPrvHistory",imap);
	}
	
	/**임직원 이력관리 추가*/
	public int insertEmployeeHistory(Map<String, Object> emap) {
		return pnservice.insert("insertEmployeeHistory",emap);
	}
	/**임직원 이력관리 리스트*/
	public List<HashMap<String, Object>> selectEmployeeModifyList(Map<String, Object> map) {
		return pnservice.selectList("selectEmployeeModifyList",map);
	}
	/**임직원 선택된 변경이력 상세보기 */
	public Map<String, Object> selectEmployeeHistoryInfoDetail(String eh_idx) {
		return pnservice.selectOne("selectEmployeeHistoryInfoDetail",eh_idx);
	}
	/**임직원 선택된 변경이력 이전버전 데이터 */
	public Map<String, Object> selectEmployeeHistoryPrvDetail(Map<String, Object> map) {
		return pnservice.selectOne("selectEmployeeHistoryPrvDetail",map);
	}
	/**기사 변경이력 추가*/
	public int insertTechnicianHistory(Map<String, Object> hmap) {
		return pnservice.insert("insertTechnicianHistory",hmap);
	}
	/**임직원상세보기 > 이력내역 갯수*/
	public Object selectEmployeeModifyListCount(String eid) {
		return pnservice.selectOne("selectEmployeeModifyListCount",eid);
	}
	/**거래처상세보기 > 이력내역 갯수*/
	public Object selectCustomerModifyListCount(String c_id) {
		return pnservice.selectOne("selectCustomerModifyListCount",c_id);
	}
	public List<Map<String, Object>> selectTechnicianModifyList(Map<String, Object> map) {
		return pnservice.selectList("selectTechnicianModifyList",map);
	}
	public List<Map<String, Object>> selectTechnicianModifyListCount(String t_id) {
		return pnservice.selectOne("selectTechnicianModifyListCount",t_id);
	}
}
