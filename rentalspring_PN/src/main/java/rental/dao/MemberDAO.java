package rental.dao;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.AesEncrypt;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.MemberInfo;

@Repository("memberDAO")
public class MemberDAO {
	
	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**member에 insert하는 메서드.<br>
	 * insert후 update도 실행.(빠진 field들 위해서)*/
	synchronized public int insertMember(Map<String, Object> member) {
		String m_num_check=(String)member.get("m_num");
		if(m_num_check==null||m_num_check.equals("")) {
			Calendar now=Calendar.getInstance();
			String year=now.get(Calendar.YEAR)+"";
			year=year.substring(2);
			String month=(now.get(Calendar.MONTH)+1)+"";
			month="0"+month;
			month=month.substring(month.length()-2);
			String pre_num=year+month;
			String m_num=pnservice.selectOne("selectLastMnum", "rt"+pre_num+"%");
			if(m_num==null) {
				m_num="0";
			}else {
				m_num=m_num.replaceAll("[^0-9]", "");
			}
			String next_num="000000"+(Integer.parseInt(m_num)+1);;
			next_num=next_num.substring(next_num.length()-4);
			member.put("m_num", "rt"+pre_num+next_num);
		}
		member.put("m_cms", MemberInfo.CMS_UNREGISTERD);
		
		String m_state=(String)member.get("m_state");
		if(m_state==null||m_state.equals("")){
			member.put("m_state",MemberInfo.STATE_BEFORE);
		}
		
		pnservice.insert("insertMember", member);
		pnservice.update("updateMember",member);
		int m_idx=Integer.parseInt(member.get("m_idx")+"");
		return m_idx;
	}
	/**member_info에 insert하는 메서드*/
	public int insertMemberInfo(Map<String, Object> memberInfo){
		String mi_id_type=memberInfo.get("mi_id_type")+"";
		//유효데이터만 받기
		if( !(mi_id_type.equals(MemberInfo.ID_TYPE_COMPANY)||mi_id_type.equals(MemberInfo.ID_TYPE_PERSON)) ){
			memberInfo.put("mi_id_type", MemberInfo.ID_TYPE_PERSON);
		}
		pnservice.insert("insertMemberInfo", memberInfo);
		int mi_idx=Integer.parseInt(memberInfo.get("mi_idx")+"");
		return mi_idx;
	}
	/**member_type에 insert하는 메서드<br>
	 * service단에서 parseInt로 에러발생시 롤백
	 * */
	public String insertMemberType(String mt_code,int m_idx,int mi_idx){
		if( !(mt_code.equals(MemberInfo.TYPE_APPLY)||mt_code.equals(MemberInfo.TYPE_CONTRACT)||mt_code.equals(MemberInfo.TYPE_DELIVERY)) ){
			return "롤백";
		}
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("mt_code", mt_code);
		map.put("m_idx", m_idx);
		map.put("mi_idx", mi_idx);
		pnservice.insert("insertMemberType", map);
		return "1";
	}
	
	/**상담 관리 고객 검색 (dt)<br>
	 * start,length,[keyword]검색조건
	 * */
	public List<Map<String,Object>> sangdamList(Map<String,Object> map){
		return pnservice.selectList("customerSangdamList", map);
	}
	/**계약고객 조회. m_state= 상담완료,재계약(구성변경)인 경우만
	 * */
	public List<Map<String,Object>> customerContractList(Map<String,Object> map){
		return pnservice.selectList("customerContractList", map);
	}
	/**Member 목록 가져오기<br>
	 * dt용 count
	 * */
	public int countCustomerSangdamList(Map<String,Object> map){
		return pnservice.selectOne("countCustomerSangdamList", map);
	}
	
	
	/**
	 * 금융원부 리스트
	 * */
	public List<Map<String,Object>> accountList(Map<String,Object> map,String mt_code){
		map.put("mt_code", mt_code);
		return pnservice.selectList("accountList", map);
	}
	/**금융원부 리스트 cnt*/
	public int countaccountList(Map<String,Object> map,String mt_code){
		map.put("mt_code", mt_code);
		return pnservice.selectOne("countaccountList", map);
	}
	
	/**계약고객 검색 count*/
	public int countCustomerContractList(Map<String,Object> map) {
		return pnservice.selectOne("countCustomerContractList",map);
	}
	
	/**member count*/
	public int countMember() {
		return pnservice.selectOne("countMember");
	}
	
	/**member 정보*/
	public Map<String,Object> selectMemberDetail(int m_idx){
		return pnservice.selectOne("selectMemberDetail", m_idx);
	}
	/**member 정보*/
	public Map<String,Object> selectMemberDetail(String m_num){
		return pnservice.selectOne("selectMemberDetailMnum", m_num);
	}
	/**member info 정보, mt_code null인 경우 모든 정보가져옴*/
	public List<Map<String,Object>> selectMemberInfo(int m_idx,String mt_code){
		HashMap<String, Object> hm=new HashMap<String, Object>();
		hm.put("m_idx", m_idx);
		if(mt_code!=null&&!mt_code.equals("")) {
			hm.put("mt_code", mt_code);
		}
		return pnservice.selectList("selectMemberInfo", hm);
	}
	
	/**mi_idx로  조회*/
	public Map<String,Object> selectMemberInfoDetail(int mi_idx){
		return pnservice.selectOne("selectMemberInfoDetail", mi_idx);
	}
	
	/**m_idx로 판매기수 조회*/
	public  Map<String,Object> selectMemberGroupDetail(int m_idx){
		return pnservice.selectOne("selectMemberGroupDetail", m_idx);
	}
	
	/**판매기수 리스트. mg_idx,mg_name*/
	public List<Map<String,Object>> selectMemberGroupList(){
		return pnservice.selectList("selectMemberGroupList");
	}
	/**판매기수 update*/
	public int updateMemberGroupMember(int mg_idx,int m_idx,String mgm_cnt) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("mg_idx", mg_idx);
		map.put("m_idx", m_idx);
		map.put("mgm_cnt", mgm_cnt);
		return pnservice.update("updateMgm",map);
	}
	/**판매기수 update*/
	public int insertMemberGroupMember(int mg_idx,int m_idx,String mgm_cnt) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("mg_idx", mg_idx);
		map.put("m_idx", m_idx);
		map.put("mgm_cnt", mgm_cnt);
		return pnservice.insert("insertMgm",map);
	}
	
	/*지정값으로 견적서 내용 가져오기*/
	public Map<String, Object> selectRetalEstimate(ParamMap pmap) {
		return pnservice.selectOne("selectRetalEstimate" ,pmap.get("re_idx"));
	}
	
	/**cs담당 배정*/
	public int updateDamdang(int m_idx,String m_damdang){
		HashMap<String, Object> hm=new HashMap<String, Object>();
		hm.put("m_idx",m_idx);
		hm.put("m_damdang",m_damdang);
		return pnservice.update("updateDamdang",hm);
	}
	
	/**updateMember map에 담긴 것만 update.m_idx 필수<br>
	 * m_cms는 업데이트 되지 않음*/
	public int updateMember(Map<String, Object> map) {
		
		//m_eta와 ds_req맞추기
		String ds_req=(String)map.get("m_eta");
		if(ds_req!=null&&!ds_req.equals("")) {
			Map<String,Object> dsmap=new HashMap<String, Object>();
			dsmap.put("m_idx", map.get("m_idx"));
			dsmap.put("mt_code", MemberInfo.TYPE_DELIVERY);
			List<Map<String,Object>> dslist=pnservice.selectList("selectDeliveryState",map);
			if(dslist!=null&&dslist.size()>0) {
				for(int i=0;i<dslist.size();i++) {
					dsmap.put("ds_idx", dslist.get(i).get("ds_idx"));
					dsmap.put("ds_req", ds_req);
					pnservice.update("updateDeliveryState",dsmap);
				}
			}
		}
		
		return pnservice.update("updateMember", map);
	}
	/**cms 상태 업데이트.m_idx필수. m_cms,m_cms_fail */
	public int updateMemberCms(Map<String, Object> map) {
		return pnservice.update("updateMemberCms", map);
	}
	
	/**updateMemberInfo*/
	public int updateMemberInfo(Map<String, Object> map) {
		return pnservice.update("updateMemberInfo", map);
	}
	
	/**selectMemberFileList. 파일,메모 등*/
	public List<Map<String,Object>> selectMemberFileList(String m_idx,String mf_name) {
		HashMap<String, Object> hm=new HashMap<String, Object>();
		hm.put("m_idx", m_idx);
		hm.put("mf_name", mf_name);
		return pnservice.selectList("selectMemberFileList", hm);
	}
	/**다운로드 위해 파일명 검색*/
	public Map<String,Object> selectMemberFileDown(String mf_idx,String mf_memo) {
		HashMap<String, Object> hm=new HashMap<String, Object>();
		hm.put("mf_idx", mf_idx);
		hm.put("mf_memo", mf_memo);
		return pnservice.selectOne("selectMemberFileDown", hm);
	}
	
	/**mf_idx로 정보 조회*/
	public Map<String,Object> selectMemberFileDetail(String mf_idx) {
		return pnservice.selectOne("selectMemberFileDetail", mf_idx);
	}
	
	
	/**deleteMemberFile. 파일,메모 등*/
	public void deleteMemberFileType(int m_idx,String mf_name) {
		HashMap<String, Object> hm=new HashMap<String, Object>();
		hm.put("m_idx", m_idx);
		hm.put("mf_name", mf_name);
		pnservice.delete("deleteMemberFileType", hm);
	}
	/**mf_idx로 db delete*/
	public void deleteMemberFileIdx(int mf_idx) {
		pnservice.delete("deleteMemberFileIdx",mf_idx);
	}
	
	/**insert member_file.mf_idx return<br>
	 * m_idx필수. mf_log를 null로 넣을 경우 현재시간<br>
	 * #{m_idx},#{mf_name},#{mf_file},#{mf_memo},#{mf_log}*/
	public int insertMemberFile(Map<String,Object> map) {
		pnservice.insert("insertMemberFile", map);
		return Integer.parseInt(map.get("mf_idx")+"");
	}
	
	/**mgm,file,fm,member delete.<br>
	 * 판매기수,금융기수,메모+파일,member 삭제*/
	public int deleteMember(int m_idx) {
		pnservice.delete("deleteMgm",m_idx);
		pnservice.delete("deleteMemberFileAll", m_idx);
		pnservice.delete("deleteFm", m_idx);
		return pnservice.delete("deleteMember", m_idx);
		
	}
	public int deleteMemberInfo(int mi_idx) {
		return pnservice.delete("deleteMemberInfo", mi_idx);
	}
	public int deleteMemberType(int m_idx) {
		return pnservice.delete("deleteMemberType", m_idx);
	}
	
	/**distinct(m_paydate)*/
	public List<String> payDates(){
		return pnservice.selectList("payDates");
	}
	/**
	 * CS점검관리 방문예정일 목록
	 */
	public List<Map<String, Object>> selectCsList(Map<String, Object> map, String mt_code) {
		map.put("mt_code", mt_code);
		map.put("install_complete", Delivery.STATE_INSTALLED);
		return pnservice.selectList("selectInstallCsList", map);
	}
	public Map<String, Object> selecCustomerDetail(String m_idx,String mt_code) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("mt_code", mt_code);
		return pnservice.selectOne("selecCustomerDetail",map);
	}
	
	/**#{m_idx},#{m_e_id},#{mdh_memo},now(),#{mdh_log_eid}*/
	public void insertMemberDamdang(Map<String,Object> map) {
		pnservice.insert("insertMemberDamdang",map);
	}
	
	/**채권관리 담당자 히스토리 조회. m_idx,length,start*/
	public List<Map<String,Object>> selectMemberDamdangHistoryList(Map<String,Object> map){
		return pnservice.selectList("selectMemberDamdangHistoryList",map);
	}
	
	/**dt용. 채권관리 담당자 히스토리 개수 */
	public int countMemberDamdangHistoryList(int m_idx) {
		return pnservice.selectOne("countMemberDamdangHistoryList",m_idx);
	}
	
	/**mg_name으로 mg_idx가져오기.<br>
	 * 없으면 insert시킴*/
	public int selectMgidx(String mg_name) {
		Integer mg_idx=pnservice.selectOne("selectMgidx",mg_name);
		if(mg_idx==null) {
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("mg_name", mg_name);
			pnservice.insert("insertMg",map);
			return Integer.parseInt(map.get("mg_idx")+"");
		}else {
			return mg_idx;
		}
	}
	
	public List<HashMap<String, Object>> getGisu() {
		return pnservice.selectList("getGisu");
	}

	/**m_idx로 배송자 정보 조회*/
	public Map<String, Object> selectRentalbyDelivery(String m_idx) {
		return pnservice.selectOne("selectRentalbyDelivery",m_idx);
	}
	/**m_idx로 계약자 정보 조회*/
	public Map<String, Object> selectRentalbyContract(String m_idx) {
		return pnservice.selectOne("selectRentalbyContract",m_idx);
	}
	
	/**현재 상태에 따라 상태 변경하는 메서드*/
	public void updateMemberProcess(int m_idx) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		//상태값 변경 로직 구현 필요
	}
	
	/**추천 기사 매칭을 위한 지역 가져오기*/
	public String getMi_addr1(int mi_idx) {
		return pnservice.selectOne("getmi_addr1", mi_idx);
	}
	
	/**설치,배송완료 전 상품 변경 멤버 정보 변경 메서드*/
	public void changeProduct(Map<String,Object>map) {
		pnservice.update("changeProduct",map);
	}
	//update member ->eta install date update
	public int updateinstallDate(HashMap<String, Object> rmap) {
		return pnservice.update("updateinstallDate",rmap);
	}
	
	/**cms회원 dataTable*/
	public List<Map<String, Object>> selectMemberCms(Map<String,Object>map) {
		return pnservice.selectList("selectMemberCms",map);
	}
	/**cms회원 dataTable count*/
	public int countMemberCms(Map<String,Object>map) {
		return pnservice.selectOne("countMemberCms",map);
	}
	
	public int getMi_idx(int m_idx) {
		return pnservice.selectOne("getMi_idx",m_idx);
	}

	/**설치확인서 새로 업로드 됐을 때, 이전 설치확인서의 mf_name을 구설치확인서로 변경*/
	public void updateOldInstallFile(int mf_idx) {
		pnservice.update("updateOldInstallFile",mf_idx);
	}
	
	/**약정일 구하기*/
	public Map<String, Object> rentalFinishDate(String m_idx) {
		return pnservice.selectOne("rentalFinishDate",m_idx);
	}
}
