package rental.dao;

import java.util.*;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("menuDAO")
public class MenuDAO {
	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**메뉴 가져오기*/
	public List<Map<String, Object>>selectMenuList(){
		return pnservice.selectList("menu");
	}
	/**메뉴idx에 따라 서브메뉴 가져오기*/
	public List<Map<String, Object>>selectSubmenuList(int m_idx){
		return pnservice.selectList("submenu",m_idx);
	}
	
}
