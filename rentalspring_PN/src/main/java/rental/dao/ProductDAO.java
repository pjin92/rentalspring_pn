package rental.dao;

import java.util.*;

import org.apache.poi.ss.formula.functions.Vlookup;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.ParamMap;

@Repository("productDAO")
public class ProductDAO {
	@Autowired
	private SqlSessionTemplate pnservice;

	/**
	 * product_rental 목록 가져오기<br>
	 * start,length ,[keyword]
	 */
	public List<Map<String, Object>> selectProductRentalList(Map<String, Object> map) {
		String mainKeyword = (String) map.get("pr_name");
		String keyword = (String) map.get("keyword");
		String code = (String) map.get("pr_rentalcode");

		if (mainKeyword == null) {
			mainKeyword = "";
		} else {
			mainKeyword = mainKeyword.replaceAll("%", "");
		}

		if (keyword == null) {
			keyword = "";
		} else {
			keyword = keyword.replaceAll("%", "");
		}

		if (code == null) {
			code = "";
		} else {
			code = code.replaceAll("%", "");
		}
		map.put("mainKeyword", "%" + mainKeyword + "%");
		map.put("keyword", "%" + keyword + "%");
		map.put("code", "%" + code + "%");

		return pnservice.selectList("selectProductRentalList", map);
	}

	/** product_rental 총 개수 가져오기 */
	public int countProductRental() {
		return pnservice.selectOne("countProductRental");
	}

	/**
	 * product 목록 가져오기<br>
	 * start:시작 number(0부터 시작)<br>
	 * count:가져올 목록의 개수<br>
	 * keyword:p_name검색조건
	 */
	public List<Map<String, Object>> selectProductList(int start, int count, String keyword) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", start);
		map.put("count", count);
		if (keyword != null && !keyword.trim().equals("")) {
			map.put("keyword", "%" + keyword + "%");
		}
		return pnservice.selectList("selectProductList", map);
	}

	/**
	 * product_rental 추가부분 newProductRental에서 입력받은 데이터들을 넣어준다
	 **/
	public HashMap<String, Object> insertrentalProduct(HashMap<String, Object> rMap) {
		pnservice.insert("insertProductRental", rMap);
		return rMap;
	}

	/**
	 * product_rental 상품 추가 newProductRental에서 입력받은 데이터들을 넣어준다
	 * 
	 * @param pRentGubun1
	 **/
	public List<Map<String, Object>> selectProductList(Map<String, Object> map) {
		// map.put("rent_gubun", pRentGubun1);

		return pnservice.selectList("selectProductList", map);

	}

	public List<Map<String, Object>> selectProductListApp(Map<String, Object> map, int pRentGubun2) {
		map.put("rent_gubun", pRentGubun2);
		return pnservice.selectList("selectProductListApp", map);
	}

	/** product_rental pr_idx로 조회 */
	/**
	 * 렌탈상품 상세보기
	 */
	public HashMap<String, Object> selectProductRentalDetail(String pr_idx) {
		return pnservice.selectOne("selectProductRentalDetail", pr_idx);
	}

	public Object countProduct(Map<String, Object> map) {
		// map.put("rent_gubun", pRentGubun1);
		return pnservice.selectOne("countProduct", map);
	}

	/**
	 * p_id로 상품정보 조회
	 */
	public Map<String, Object> selectProductDetail(int pid) {
		return pnservice.selectOne("selectProductDetail", pid);
	}

	/**
	 * ProductRentalProduct에 추가하기 (pr_idx받아온 후)
	 */
	public int insertPRP(List<HashMap<String, Object>> prpList) {
		int result = 0;
		for (int i = 0; i < prpList.size(); i++) {
			result = pnservice.insert("insertPRP", prpList.get(i));
		}
		return result;
	}

	/**
	 * 렌탈상품 조회 및 찾기 (p_name 받아온 후)
	 */

	public List<HashMap<String, Object>> rentalProductSearch(String pr_name) {
		List<HashMap<String, Object>> rentalList = new ArrayList<HashMap<String, Object>>();
		rentalList = pnservice.selectList("rentalProductSearch", pr_name);
		return rentalList;
	}

	public Object countrentalProductList(Map<String, Object> map) {
		String mainKeyword = (String) map.get("mainKeyword");
		if (mainKeyword == null) {
			mainKeyword = "";
		} else {
			mainKeyword = mainKeyword.replaceAll("%", "");
		}
		map.put("mainKeyword", "%" + mainKeyword + "%");
		return pnservice.selectOne("countrentalProductList", map);
	}

	public Map<String, Object> selectRentalDetail(String pr_rentalcode) {
		return pnservice.selectOne("selectRentalDetail", pr_rentalcode);
	}

	public List<HashMap<String, Object>> selectRentalComDetail(String pr_rentalcode) {
		List<HashMap<String, Object>> selectRentalComDetail = new ArrayList<HashMap<String, Object>>();
		selectRentalComDetail = pnservice.selectList("selectRentalComDetail", pr_rentalcode);
		return selectRentalComDetail;
	}

	/**
	 * product_rental 삭제
	 */
	public int deletePR(Map<String, Object> map) {
		int result = 0;
		result = pnservice.delete("deletePR", map);
		return result;
	}

	/**
	 * 
	 * product_rental_product 삭제
	 */
	public int deletePRP(Map<String, Object> map) {
		int result = 0;
		result = pnservice.delete("deletePRP", map);
		return result;
	}

	/**
	 * product_rental 업데이트
	 */
	public int updateProductRental(Map<String, Object> map) {
		return pnservice.update("updateProductRental", map);
	}

	/** prp_idx로 product_rental_product, product 정보 조회 */
	public Map<String, Object> selectPrpDetail(int p_id) {
		return pnservice.selectOne("selectPrpDetail", p_id);
	}

	public List<HashMap<String, Object>> selectCalcList(int pRentGubun) {
		List<HashMap<String, Object>> list = pnservice.selectList("selectCalcList", pRentGubun);
		return list;
	}

	/**
	 * 최신 LED렌탈 코드 조회
	 */
	public String getLEDRentalCode() {
		return pnservice.selectOne("getLEDRentalCode");
	}

	/**
	 * 최신 구LED렌탈 코드 조회
	 */
	public String getOldLEDRentalCode() {
		return pnservice.selectOne("getOldLEDRentalCode");
	}

	/**
	 * 최신 렌탈코드조회
	 */
	public String getRentalCode() {
		return pnservice.selectOne("getRentalCode");
	}

	/** pr_rentalcode로 조회 */
	public Map<String, Object> selectPrByCode(String pr_rentalcode) {
		return pnservice.selectOne("selectPrByCode", pr_rentalcode);
	}

	public String selectRenatalCode(String pr_idx) {
		return pnservice.selectOne("selectRenatalCode", pr_idx);
	}

	public List<HashMap<String, Object>> selectRentalComDetail2(int pr_idx) {
		List<HashMap<String, Object>> selectRentalComDetail = new ArrayList<HashMap<String, Object>>();
		selectRentalComDetail = pnservice.selectList("selectRentalComDetail2", pr_idx);
		return selectRentalComDetail;
	}

	public int insertRP(HashMap<String, Object> hm) {
		return pnservice.insert("insertUploadedCustomer", hm);
	}

	public List<HashMap<String, Object>> selectproductApp(int pr) {
		return pnservice.selectList("productApp", pr);
	}

	public String selectPridx(Map<String, Object> map) {
		return pnservice.selectOne("selectPridx", map);
	}

	// public int updateMemberPridx(String rental_code) {
	// return pnservice.update("updateMemberPridx",rental_code);
	// }

	public String getProductcode(String pr) {
		return pnservice.selectOne("getProductcode", pr);
	}

	/** prp left join product 조회 */
	public List<HashMap<String, Object>> rProductDetail(String pr_idx) {
		return pnservice.selectList("rProductDetail", pr_idx);
	}

	// m_idx 로 pr_idx 구하기
	public String getPridxByMidx(Map<String, Object> pmap) {
		HashMap<String, Object> map = pnservice.selectOne("getPrIdxbyMidx", pmap);
		String pr_idx = map.get("pr_idx") + "";
		return pr_idx;
	}

	/** 기초상품조회 */
	public List<Map<String, Object>> selectBasicList(Map<String, Object> map) {
		String code = (String) map.get("p_code");
		String category1 = (String) map.get("p_category1");
		String p_name = (String) map.get("p_name");
		String date = (String) map.get("date");

		if (date == null || date.equals("")) {
			date = null;
		} else {
			map.put("date", date);
		}

		if (p_name == null) {
			p_name = "";
		} else {
			p_name = p_name.replaceAll("%", "");
		}

		if (code == null) {
			code = "";
		} else {
			code = code.replaceAll("%", "");
		}

		if (category1 == null) {
			category1 = "";
		} else {
			category1 = category1.replaceAll("%", "");
		}

		map.put("p_name", "%" + p_name + "%");
		map.put("code", "%" + code + "%");
		map.put("category1", "%" + category1 + "%");

		return pnservice.selectList("selectBasicList", map);
	}

	public Object countbasicProductList(Map<String, Object> map) {
		String code = (String) map.get("p_code");
		String category1 = (String) map.get("p_category1");
		String p_name = (String) map.get("p_name");
		String date = (String) map.get("date");

		if (date == null || date.equals("")) {
			date = null;
		} else {
			map.put("date", date);
		}

		if (p_name == null) {
			p_name = "";
		} else {
			p_name = p_name.replaceAll("%", "");
		}

		if (code == null) {
			code = "";
		} else {
			code = code.replaceAll("%", "");
		}

		if (category1 == null) {
			category1 = "";
		} else {
			category1 = category1.replaceAll("%", "");
		}

		map.put("p_name", "%" + p_name + "%");
		map.put("code", "%" + code + "%");
		map.put("category1", "%" + category1 + "%");
		return pnservice.selectOne("countbasicProductList", map);
	}

	/** 기초 상품 상품군 가져오기 */
	public List<HashMap<String, Object>> selectCategoryOfProduct() {
		return pnservice.selectList("selectCategoryOfProduct");
	}

	/** 기초상품등록 */
	public int productBasic(Map<String, Object> map) {
		return pnservice.insert("productBasic", map);
	}

	/** 기초상품데이터 렌트타입 */
	public List<HashMap<String, Object>> selectP_rent_gubunOfProduct() {
		return pnservice.selectList("selectP_rent_gubunOfProduct");
	}

	/** p_code로 상품조회 */
	public Map<String, Object> selectProductDetailByP_code(String p_code) {
		return pnservice.selectOne("selectProductDetailByP_code", p_code);
	}

	/** 기초상품업데이트(등록되어지는 모든것) */
	public int updateProduct(Map<String, Object> map) {
		return pnservice.update("updateProduct", map);
	}

	// 최신 기초상품의 렌탈코드 가져오기
	public String getBasicProductPcode() {
		return pnservice.selectOne("getBasicProductPcode");
	}

	/** 상품명중복방지 */
	public int selectProudctNameDuplicate(Map<String, Object> map) {
		return pnservice.selectOne("selectProudctNameDuplicate", map);
	}

	/**
	 * CS항목 생성중 CS소모품 상품불러오기
	 */
	public List<Map<String, Object>> selectProductOfCsList(Map<String, Object> map) {
		return pnservice.selectList("selectProductOfCsList", map);
	}

	public int countProductOfCsList(Map<String, Object> map) {
		return pnservice.selectOne("countProductOfCsList", map);
	}
}
