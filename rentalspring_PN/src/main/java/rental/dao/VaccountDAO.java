package rental.dao;

import java.util.*;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("vaccountDAO")
public class VaccountDAO {
	
	@Autowired
	private SqlSessionTemplate pnservice;
	
	/**#{v_condition},#{v_money},#{v_count}
	,#{v_end_date},#{v_reuse},#{v_account},#{v_name}
	,#{v_bank}#{v_receipt},#{m_idx},#{cms_idx}*/
	public int insertV(Map<String,Object>map) {
		return pnservice.insert("insertV",map);
	}
	
	/**m_idx로 사용중인 가상계좌 조회*/
	public List<Map<String,Object>> selectVlist(int m_idx){
		return pnservice.selectList("selectVlist",m_idx);
	}
	
	/**m_idx,은행코드로 사용중인 가상계좌 조회<br>
	 * v_bank==null일 경우 m_idx의 가상계좌 중 1개를 조회*/
	public Map<String,Object> selectVdetail(int m_idx,String v_bank){
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("v_bank", v_bank);
		return pnservice.selectOne("selectVdetail",map);
	}
	
	/**가상계좌번호로 v_account조회*/
	public Map<String,Object> selectVaccountDetail(String v_account){
		return pnservice.selectOne("selectVaccountDetail",v_account);
	}
	
	/**가상계좌삭제. db에서는 reuse를 마이너스1로 업데이트*/
	public void deleteV(int v_idx) {
		pnservice.update("deleteV",v_idx);
	}
	
	/**가상계좌관리 dataTable 조회*/
	public List<Map<String,Object>> withdrawVaccount(Map<String,Object> map){
		return pnservice.selectList("withdrawVaccount",map);
	}
	
	/**가상계좌관리 수 count(연체관리 검색조건)*/
	public int countWithdrawVaccount(Map<String,Object> map){
		return pnservice.selectOne("countWithdrawVaccount",map);
	}
}
