package rental.dao;

import java.util.*;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.InstallState;

@Repository("installDAO")
public class InstallDAO {
	@Autowired
	private SqlSessionTemplate pnservice;
	
	public int countInstallDetail(int midx) {
		return pnservice.selectOne("countInstallDetail", midx);
	}
	
	/**로그인 체크하는 메서드.
	 * e_id,e_name,e_branch,e_department,e_position,e_power,e_logid,e_pass
	 */
	public Map<String, Object> login(String e_logid) {
		return pnservice.selectOne("loginCheck", e_logid);
	}

	/** 부서로 employee정보 가져오기 */
	public List<Map<String, Object>> selectEmployeeListByDepartment(String e_department) {
		return pnservice.selectList("employeeListByDepartment", e_department);
	}

	/** e_id로 employee정보 가져오기 */
	public Map<String, Object> selectEmployeeDetail(int e_id) {
		return pnservice.selectOne("selectEmployeeDetail", e_id);
	}

	public Map<String, Object> insertTechnician(Map<String, Object> map) {
		pnservice.insert("insertTechnician", map);

		return map;
	}

	public List<Map<String, Object>> selectTechList(Map<String, Object> map) {
		return pnservice.selectList("selectTechList", map);
	}

	public Object countTechList(Map<String, Object> map, int techAuth) {
		map.put("tech_auth", techAuth);
		return pnservice.selectOne("countTechList", map);
	}

	public int insertTechnicianStock(String eid) {

		return pnservice.insert("insertTechnicianStock", eid);
	}

	public List<Map<String, Object>> selecttechnicianList(Map<String, Object> map) {
		return pnservice.selectList("selecttechnicianList", map);
	}

	// public int insertInstallState(HashMap<String, Object> map) {
	// return pnservice.insert("insertInstallState", map);
	// }

	/**설치확인서 등록 GET. mf와 if 가져옴. if는 null가능 */
	public List<Map<String, Object>> selectinstallFileList(int m_idx) {
		return pnservice.selectList("selectInstallFileList", m_idx);
	}

	/** 설치확인서 등록완료
	 *  파일 추가
	 *  */
	public int insertInstallFile(Map<String, Object> map) {
		pnservice.insert("insertInstallFile", map);
		return Integer.parseInt(map.get("if_idx") + "");
	}

	/** 설치확인서 등록완료 
	 * 기사 매칭되어있는지 확인<br>
	 * 매칭안되어있으면 0반환
	 * */
	public int selectInstallmatch(int midx) {
		Integer im_idx = pnservice.selectOne("selectInstallState", midx);
		if(im_idx==null) {
			im_idx=0;
		}
		return im_idx;
	}
	/**
	 * CS점검관리 리스트
	 */
	public List<Map<String, Object>> selectInstallList(Map<String, Object> map, String mt_code) {
		map.put("mt_code", mt_code);
		map.put("install_complete", Delivery.STATE_INSTALLED);
		String mainKeyword = (String) map.get("mainKeyword");
		if (mainKeyword == null) {
			mainKeyword = "";
		} else {
			mainKeyword = mainKeyword.replaceAll("%", "");
		}
		map.put("mainKeyword", "%" + mainKeyword + "%");
		 List<Map<String, Object>> result = pnservice.selectList("selectInstallCompleteList", map);
		return result;
	}
	
	public List<Map<String, Object>> selectInstallListApp(Map<String, Object> map, String mt_code) {
		map.put("mt_code", mt_code);
		map.put("install_complete", Delivery.STATE_INSTALLED);
		
		 List<Map<String, Object>> result = pnservice.selectList("selectInstallListApp", map);
		return result;
	}


	/** 설치확인서 등록완료 
	 * 설치상태 변경 
	 * */
	public int updateDeliveryState(Map<String, Object> pmap) {
		return pnservice.update("updateDeliveryState_install", pmap);
	}


	public Map<String, Object> selectInstallFileDown(String if_idx, String if_memo) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("if_idx", if_idx);
		hm.put("if_memo", if_memo);
		return pnservice.selectOne("selectInstallFileDown", hm);
	}

	/** 설치확인서 등록완료 
	 * 렌탈만료일 설정
	 * */
	public Map<String, Object> getRentalFinishDate(HashMap<String, Object> pmap) {
		return pnservice.selectOne("getRentalFinishDate", pmap);
	}

	/** 설치확인서 등록완료 
	 * CS점검 리스트 추가
	 * */
	
	public int insertCsList(HashMap<String, Object> pmap) {
		pmap.put("cl_gubun", InstallState.REGURAL_CS);
		return pnservice.insert("insertCsList", pmap);
	}

	/** 설치확인서 등록완료 
	 * CS점검 주기 구하기
	 * */
	
	public List<HashMap<String, Object>> selectInstallfinish(HashMap<String, Object> pmap) {
		return pnservice.selectList("selectInstallfinish", pmap);
	}
	/**
	 * 방문예정일 변경
	 * */
	public int updateClDate(HashMap<String, Object> rmap) {
		return pnservice.update("updateClDate",rmap);
	}

	/**
	 * 해당설치건의 기사변경
	 * */
	public int updateTechnician(Map<String, Object> map) {
		return pnservice.update("updateTechnician",map);
	
	}
	/**
	 * CS점검관리 방문예정일 목록 세부내용 Table로 불러서 AJAX로 append함
	 */
	public List<Map<String, Object>> selectCsList(Map<String, Object> map, String mt_code) {
		map.put("mt_code", mt_code);
		map.put("install_complete", Delivery.STATE_INSTALLED);
		System.out.println("cslist / "+map.entrySet());
		return pnservice.selectList("selectInstallCsListDetail", map);
	}
	/**
	 * 해당설치건의 고객정보
	 * */
	public Map<String, Object> selectInstallCustomer(Map<String, Object> map,String mt_code) {
		map.put("mt_code", mt_code);
		return pnservice.selectOne("selectInstallCustomer",map);
	}
	/**
	 * 해당설치건의 고객메모
	 * */
	public int updateCsInstallMemo(Map<String, Object> map) {
		return pnservice.update("updateCsInstallMemo",map);
	}
	
	/**
	 * 해당 설치건의 상태변경 (설치전->설치요청)
	 * */
	public int csInstall(Map<String, Object> map,String mt_code) {
		map.put("mt_code", mt_code);
		return pnservice.insert("csInstall",map);
	}
	/**
	 * 해당 설치건의 상태변경 (설치요청->설치완료)
	 * */
	public int csInstallComplete(Map<String, Object> map, String mt_code) {
		map.put("mt_code", mt_code);
		return pnservice.update("csInstallComplete",map);
	}

	public int csInstallCompleteDate(Map<String, Object> map) {
		return pnservice.update("csInstallCompleteDate",map);
	}

	public Map<String, Object> selectOrigDate(int cl_idx) {
		return pnservice.selectOne("selectOrigDate",cl_idx);
	}

	public int updateOrgDate(HashMap<String, Object> rmap) {
		return pnservice.update("updateOrgDate",rmap);
	}

	public int insertAddCsProduct(Map<String, Object> map) {
		return pnservice.insert("insertAddCsProduct",map);
	}

	public List<Map<String, Object>> selectCsProductList(Map<String, Object> map) {
		List<Map<String, Object>> list = pnservice.selectList("selectCsProductList",map);
		return list;
	}

	public List<Map<String, Object>> selectInstallCalList(Map<String, Object> map, String mt_code) {
		map.put("mt_code", mt_code);
		map.put("install_complete", Delivery.STATE_INSTALLED);
		String mainKeyword = (String) map.get("mainKeyword");
		if (mainKeyword == null) {
			mainKeyword = "";
		} else {
			mainKeyword = mainKeyword.replaceAll("%", "");
		}
		map.put("mainKeyword", "%" + mainKeyword + "%");
		 List<Map<String, Object>> result = pnservice.selectList("selectInstallCompleteList", map);
//		 List<Map<String, Object>> result2 = pnservice.selectList("selectInstallCalList", map);
		 List<Map<String, Object>> result3 = new ArrayList<Map<String,Object>>(); 
		 for(int i=0;i<result.size();i++) {
			 Map<String,Object> rmap = new HashMap<String, Object>();
			 rmap=result.get(i);
			 rmap.put("gubun", "설치");
			 result3.add(rmap);
		 }
	
//		 for(int i=0;i<result2.size();i++) {
//			 Map<String,Object> rmap = new HashMap<String, Object>();
//			 rmap=result2.get(i);
//			 rmap.put("gubun", "점검");
//			 rmap.put("if_log", rmap.get("cl_complete_date"));
//			 result3.add(rmap);
//		 }
		 
		 return result3;
	}

	public List<HashMap<String, Object>> selectJungsan(Map<String, Object> map) {
		return pnservice.selectList("selectJungsan",map);
	}

	public int insertJungsan(List<HashMap<String, Object>> jungsan) {
		int result =0;
		for(int i=0;i<jungsan.size();i++) {
			result = pnservice.insert("insertJungsan",jungsan.get(i));
		}
		return result;
	}

	public int getPr_type(String m_idx) {
		return pnservice.selectOne("getPr_type",m_idx);
	}
	
	/**install_file update*/
	public int updateInstallFile(Map<String,Object> map) {
		return pnservice.update("updateInstallFile", map);
	}

	public int matchCancel(int m_idx) {
		return pnservice.delete("matchCancel",m_idx);
	}

	
	/**cs항목*/
	public List<Map<String, Object>> selectInstallCompleteList(Map<String, Object> map, String mt_code) {
		map.put("mt_code", mt_code);
		map.put("install_complete", Delivery.STATE_INSTALLED);
		 List<Map<String, Object>> result = pnservice.selectList("selectInstallCompleteList", map);
		return result;
	}
	
}
