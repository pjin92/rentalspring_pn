package rental.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("commonDAO")
public class CommonDAO {
	@Autowired
	private SqlSessionTemplate information;
	@Autowired
	private SqlSessionTemplate pnservice;

	/**db에 해당 field명이 있는지 확인. 있으면 0보다 크고 없으면 0을 return*/
	public int checkColumn(String column_name) {
		return information.selectOne("checkColumn", column_name);
	}
	
	/**m_state 상담상태 list 가져오기*/
	public List<String> mstateList(){
		return pnservice.selectList("mstateList");
	}
	/**mjubsooL 접수구분 list 가져오기*/
	public List<String> mjubsooList(){
		return pnservice.selectList("mjubsooList");
	}
	/**mdetail 매출구분 list 가져오기*/
	public List<String> mdetailList(){
		return pnservice.selectList("mdetailList");
	}
	
	/**int 데이터 필드명 가져오기*/
	public List<String> intColumn(){
		return information.selectList("intColumn");
	}

	public List<String> lsr_statusList() {
		return pnservice.selectList("lsr_statusList");
	}
	
	/**mprocess 진행상태 list 가져오기*/
	public List<String> mprocessList(){
		return pnservice.selectList("mprocessList");
	}
	
}
