package rental.constant;

public class InstallState {
	
	/**member_file file path<br>
	 * path뒤의 경로는 path/{if_name}/{if_idx}_{if_memo}<br>
	 * 파일이름은 {if_file}*/
	final public static String PATH="C:/rentalfile/Install_confirmation/";
	
	/** 설치확인서등록*/
	final public static String NAME_INSTALL_CONFIRMATION="설치확인서등록";
	final public static String NAME_INSTALL_NOTMATCH="미배정";
	final public static String NAME_INSTALL_MATCH ="배정";
	final public static String NAME_INSTALL_INSTALL_REQ="설치요청";
	final public static String NAME_INSTALL_INSTALL_COMPLETE="설치완료";
	
	final public static int REGURAL_CS=0;
	final public static int EMERGENCY_CS=1;
}
