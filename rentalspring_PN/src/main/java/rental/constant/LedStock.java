package rental.constant;

/**product_rental 상수 클래스*/
public class LedStock {
	
	/**lsr_status 출고요청*/
	final public static String STATUS_REQUEST_STOCK = "출고요청";
	/**lsr_status 출고완료**/
	final public static String STATUS_COMPLETE_STOCK = "출고완료";
	/**lsr_status 반품요청*/
	final public static String STATUS_REQUEST_RETURN = "반품요청";
	/**lsr_status반품완료*/
	final public static String STATUS_COMPLETE_RETURN = "반품완료";
	/**lsr_status 취소*/
	final public static String STATUS_STOCK_CANCEL = "취소";
	
	
}
