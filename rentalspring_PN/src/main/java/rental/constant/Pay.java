package rental.constant;

public class Pay {
	
	//pay_send 상수 
	
	/**ps_pay_idx 등록*/
	final static public int PS_PAY_IDX_REGISTER=0;
	/**ps_pay_idx 수정*/
	final static public int PS_PAY_IDX_CHANGE=-1;
	/**ps_pay_idx 삭제(사용안함)*/
	final static public int PS_PAY_IDX_DELETE=-2;
	
	/**은행회원 등록,수정 당일 신청 마감시간. 당일 12시*/
	final static public int SEND_REGI_TIME=12;
	/**은행회원 등록,수정,출금 결과 요청 가능시간. 하루 후 11시*/
	final static public int SEND_RESULT_TIME=11;
	/**은행회원 출금신청 마감시간. 하루전 17시*/
	final static public int SEND_PAY_TIME=17;
	
	/**ps_state 신청전(바구니에 담긴 상태)*/
	final static public String SEND_STATE_BEFORE="신청전";
	/**ps_state 신청중(성공적으로 신청)*/
	final static public String SEND_STATE_SEND="신청중";
	/**ps_state 신청오류(신청 중에 오류발생.)*/
	final static public String SEND_STATE_ERROR="신청오류";
	/**ps_state 신청완료(결과 받아오기 완료)+결과가 성공 or 실패*/
	final static public String SEND_STATE_SUCCESS="신청완료";
	
	/**ps_method 계좌이체*/
	final static public String SEND_METHOD_BANK="계좌이체";
	
	//pay 상수
	
	/**pay_state 신청전*/
	final static public String STATE_UNPAID="신청전";
	/**pay_state 출금중*/
	final static public String STATE_ON_PROCESS="신청중";
	/**pay_state 출금완료*/
	final static public String STATE_PAID="출금완료";
	/**pay_state 출금실패*/
	final static public String STATE_FAIL="출금실패";
	/**pay_state 출금취소*/
	final static public String STATE_REFUND="출금취소";
	/**pay_state 중도상환*/
	final static public String STATE_JOONGDO="중도상환";
	/**pay_state 일시불*/
	final static public String STATE_ILSI="일시불";
	/**pay_state 반품*/
	final static public String STATE_RETURN="반품";
	/**pay_state 회수*/
	final static public String STATE_EXIT="회수";
	/**pay_state 해지*/
	final static public String STATE_HAEBAN="해지반품";
	
	/**pay_num 직접입금*/
	final static public String NUM_DIRECT="직접입금";
	/**pay_num 카드결제인 경우 prefix로 붙임. 'c'+pay_idx+random(3자리)*/
	final static public String NUM_CARD="c";
	/**pay_num 실시간출금인 경우 prefix로 붙임. 'r'+pay_idx+연번<br>
	 * 회원번호에는 뒤에 붙임 m_num+'r'*/
	final static public String NUM_REALTIME="r";
	
	/**pay_reason 실시간출금인 경우.(출금실패인경우 prefix로 붙임.) */
	final static public String REASON_REALTIME="실시간출금";
	/**pay_reason 가상계좌입금인 경우 */
	final static public String REASON_V_ACCOUNT="가상계좌입금";
	/**pay_reason 직접입금인 경우 prefix로 */
	final static public String REASON_DIRECT="(직접입금)";
	
	/**pay_method 계좌이체. cms출금데이터 조회시 결제방식이 바뀌더라도 출금실패,완료 건들은 제대로 조회되도록 history로 남김*/
	final static public String METHOD_BANK="bank";
	/**pay_method 카드. cms출금데이터 조회시 결제방식이 바뀌더라도 출금실패,완료 건들은 제대로 조회되도록 history로 남김*/
	final static public String METHOD_CARD="card";
}
