package rental.constant;

/**product_rental 상수 클래스*/
public class Product {
	
	/**p_type 설치*/
	final static public String P_TYPE_INSTALL ="설치";
	/**p_type 배송*/
	final static public String P_TYPE_DELIVERY ="배송";
	
	/**p_rent_gubun 신LED구성품*/
	final static public String P_RENT_GUNUN_NEWLED ="신LED 구성품";
	/**p_rent_gubun 구LED구성품*/
	final static public String P_RENT_GUNUN_OLDLED ="구LED 구성품";
	/**p_rent_gubun CS용 상품*/
	final static public String P_RENT_GUNUN_CSPRODUCT ="CS용 상품";
	/**p_rent_gubun 일반렌트상품*/
	final static public String P_RENT_GUNUN_DEFAULTRENT ="일반렌트상품";
	/**p_rent_gubun 일반상품*/
	final static public String P_RENT_GUNUN_DEFAULTPRODUCT ="일반상품";

	

	
	
	
}
