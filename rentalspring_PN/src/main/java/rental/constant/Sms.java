package rental.constant;

/**
 * Send Message
 * success : Code=>0, CodeMsg=>Success!!, LastPoint=>9999
 * fail 1  : Code=>100, CodeMsg=>Not Registered ID
 * fail 2  : Code=>200, CodeMsg=>Not Enough Point
 * fail 3  : Code=>300, CodeMsg=>Login Fail
 * fail 4  : Code=>400, CodeMsg=>No Valid Number
 * fail 5  : Code=>500, CodeMsg=>No Valid Message
 * fail 6  : Code=>600, CodeMsg=>Auth Fail
 * fail 7  : Code=>700, CodeMsg=>Invalid Recall Number
 */
public class Sms {
	
	/**sh_resultcode 발송전, default값*/
	public static final int RESULTCODE_BEFORE=-1;
	/**sh_resultcode발송성공<br>
	 * Send Message<br>
	 * success : Code=>0, CodeMsg=>Success!!, LastPoint=>9999<br>
	 * fail 1  : Code=>100, CodeMsg=>Not Registered ID<br>
	 * fail 2  : Code=>200, CodeMsg=>Not Enough Point<br>
	 * fail 3  : Code=>300, CodeMsg=>Login Fail<br>
	 * fail 4  : Code=>400, CodeMsg=>No Valid Number<br>
	 * fail 5  : Code=>500, CodeMsg=>No Valid Message<br>
	 * fail 6  : Code=>600, CodeMsg=>Auth Fail<br>
	 * fail 7  : Code=>700, CodeMsg=>Invalid Recall Number
	 */
	public static final String RESULTCODE_SUCCESS="0";
	
	
	/**sh_type 출금*/
	public static final String TYPE_PAY="P";
	/**sh_type 가상계좌*/
	public static final String TYPE_VACCOUNT="V";
	/**sh_type 기타*/
	public static final String TYPE_ETC="E";
	
}
