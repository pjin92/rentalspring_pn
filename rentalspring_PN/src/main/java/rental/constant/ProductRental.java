package rental.constant;

/**product_rental 상수 클래스*/
public class ProductRental {
	

	final public static int P_RENT_GUBUN1 = 1;
	final public static int P_RENT_GUBUN2 = 2;
	final public static int P_RENT_GUBUN5 = 5;
	
	/**pr_type,product.p_type 배송상품*/
	final public static int TYPE_DELIVERY=1;
	/**pr_type,product.p_type 설치상품*/
	final public static int TYPE_INSTALL=2;
	
	/**매칭상태*/
	final public static int NOT_MATCH=0;
	final public static int MATCHING=1;
	
	
	
}
