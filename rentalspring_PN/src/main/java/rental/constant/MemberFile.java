package rental.constant;

public class MemberFile {
	
	/**member_file file path<br>
	 * path뒤의 경로는 path/{mf_name}/{mf_idx}_{mf_memo}<br>
	 * 파일이름은 {mf_file}*/
	final public static String PATH="D:/rentalfile/customer/";
	
	/**mf_type 파일*/
	final public static String TYPE_FILE="파일";
	/**mf_type 메모*/
	final public static String TYPE_MEMO="메모";
	
	/**mf_name 상담메모*/
	final public static String NAME_SANGDAM_MEMO="상담메모";
	/**mf_name 녹취파일*/
	final public static String NAME_RECORD="녹취파일";
	/**mf_name 약정서인증파일*/
	final public static String NAME_AGREEMENT_CERTFICATE="약정서인증파일";
	/**mf_name 양도통지서*/
	final public static String NAME_TRANSFER_NOTICE="양도통지서";
	
	/**mf_name 설치전후사진*/
	final public static String NAME_INSTALL_PIC="설치전후사진";
	
	/**mf_name 신용정보조회동의서*/
	final public static String NAME_CREDIT_INQUIRY="신용정보조회동의서";
	/**mf_name 설치확인서*/
	final public static String NAME_INSTALL="설치확인서";
	/**mf_name 구설치확인서*/
	final public static String NAME_INSTALL_EX="구설치확인서";
	/**mf_name 설치 철거 확인서*/
	final public static String NAME_INSTALL_REMOVE="설치 철거 확인서";
	/**mf_name 폐기확인서*/
	final public static String NAME_DISPOSAL="폐기확인서";
}
