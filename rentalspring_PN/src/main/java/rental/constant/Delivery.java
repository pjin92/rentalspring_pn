package rental.constant;

public class Delivery {
	
	/**송장관리 엑셀 업로드 저장 경로*/
	final public static String PATH="D:/rentalfile/delivery/Excel/";
	/**송장관리 엑셀 양식 파일*/
	final public static String PATH_FORM="D:/rentalfile/form/송장관리엑셀양식.xls";
	
	/**ds_history 사용하는 배송상태 Y*/
	final public static String HISTORY_BASIC="Y";
	
	/**ds_state db에 값 입력전(null)*/
	final public static String STATE_NONE="정보없음";
	
	/**ds_state 구매취소*/
	final public static String STATE_CANCEL_DELIVERY="구매취소";
	
	/**ds_state 배송전*/
	final public static String STATE_BEFORE_DELIVERY="배송전";
	/**ds_state 설치전*/
	final public static String STATE_BEFORE_INSTALL="설치전";
	/**ds_state 배송요청*/
	final public static String STATE_REQUEST_DELIVERY="배송요청";
	/**ds_state 설치요청*/
	final public static String STATE_REQUEST_INSTALL="설치요청";
	/**ds_state 반품요청*/
	final public static String STATE_REQUEST_RETURN="반품요청";
	/**ds_state 회수요청*/
	final public static String STATE_REQUEST_EXIT="회수요청";
	/**ds_state 해지반품요청*/
	final public static String STATE_REQUEST_HAEBAN="해지반품요청";
	
	/**ds_state 배송완료*/
	final public static String STATE_DELIVERED="배송완료";
	/**ds_state 설치완료*/
	final public static String STATE_INSTALLED="설치완료";
	/**ds_state 반품완료*/
	final public static String STATE_RETURN="반품완료";
	/**ds_state 회수완료*/
	final public static String STATE_EXIT="회수완료";
	/**ds_state 해지반품완료*/
	final public static String STATE_HAEBAN="해지반품완료";
	
	
	
	/**PRODUCT테이블의 p_rent_gubun에서 나뉨. 설치상품*/
	final public static int INSTALL_PRODUCT = 3;
	
	/**PRODUCT테이블의 p_rent_gubun에서 나뉨. 배송상품*/
	final public static int DELIVERY_PRODUCT = 4;
	
}
