package rental.constant;

/**member_info*/
public class MemberInfo {
	/**m_state 상담전*/
	final static public String STATE_BEFORE="상담전";
	/**m_state 상담완료*/
	final static public String STATE_FINISH="상담완료";
	/**m_state 렌탈상품 변경시*/
	final static public String STATE_CHANGE_PRODUCT="재계약(구성변경)";
	/**m_state 취소(가망)*/
	final static public String STATE_CANCEL="취소(가망)";
	/**m_state 선설치(렌탈전환)*/
	final static public String STATE_INSTALL_FIRST="선설치(렌탈전환)";
	/**m_state 대기(어플에서만 가능) 어플에서 보임 */
	final static public String STATE_WAITING="대기";
	/**m_state 보류 (웹에서만 가능) 어플에서 안보이도록*/
	final static public String STATE_HOLD="보류";
	
	/**mi_type 결제자 (member table)*/
	final static public String TYPE_PAYER="결제자";
	/**mi_type 신청자(상담자)*/
	final static public String TYPE_APPLY="신청자";
	/**mi_type 계약자*/
	final static public String TYPE_CONTRACT="계약자";
	/**mi_type 배송 받는 사람*/
	final static public String TYPE_DELIVERY="배송자";
	
	/**mi_id_type 개인*/
	final static public String ID_TYPE_PERSON="개인";
	/**mi_id_type 법인*/
	final static public String ID_TYPE_COMPANY="법인";
	
	/**md_history 사용여부. Y<br>md_m_idx당 하나의 md_history만 Y값을 가짐*/
	final static public String MD_HISTORY_CURRENT="Y";
	/**md_history 사용여부. N<br> md_m_idx당 하나의 md_history만 Y값을 가짐 */
	final static public String MD_HISTORY_PREV="N";
	
	/**m_pay_method 계좌이체*/
	final static public String PAY_METHOD_BANK="계좌이체";
	/**m_pay_method 카드*/
	final static public String PAY_METHOD_CARD="카드";
	
	
	/**m_cms 미등록*/
	final static public String CMS_UNREGISTERD="미등록";
	/**m_cms 등록완료*/
	final static public String CMS_REGISTERD="등록완료";
	/**m_cms 등록중(파일미등록)*/
	final static public String CMS_REG_NEEDFILE="등록중(파일미등록)";
	/**m_cms 등록중(파일등록완료)*/
	final static public String CMS_REG_HASFILE="등록중";
	/**m_cms 수정요청*/
	final static public String CMS_CHANGE="수정요청";
	/**m_cms 정보수정 중 (수정신청완료)*/
	final static public String CMS_CHANGE_ING="수정중";
	
	//m_jubsoo
	/**m_jubsoo 개인영업*/
	final static public String JUBSOO_PERSONAL="개인영업";
	/**m_jubsoo 렌탈*/
	final static public String JUBSOO_RENTAL="렌탈";
	/**m_jubsoo 무료체험*/
	final static public String JUBSOO_FREE_EXPERIENCE="무료체험";
	/**m_jubsoo 방문견적*/
	final static public String JUBSOO_VISIT="방문견적";
	
	//m_detail 매출구분 상수
	/**m_detail 매출구분 일시불*/
	final static public String DETAIL_ILSI="일시불";
	/**m_detail 매출구분 렌탈*/
	final static public String DETAIL_RENTAL="렌탈";
	/**m_detail 매출구분 렌탈(자체)*/
	final static public String DETAIL_RENTAL_SELF="렌탈(자체)";
	/**m_detail 매출구분 부분일시불*/
	final static public String DETAIL_ILSI_PART="부분일시불";
	/**m_detail 매출구분 체험접수*/
	final static public String DETAIL_TEST="체험접수";
	/**m_detail 매출구분 체험접수취소*/
	final static public String DETAIL_TEST_CANCEL="체험접수취소";
	/**m_detail 매출구분 무료이벤트*/
	final static public String DETAIL_EVENT="무료이벤트";
	
	//m_process상수
	/**m_process 렌탈전*/
	final static public String PROCESS_BEFORE_RENT="렌탈전";
	/**m_process 방문완료*/
	final static public String PROCESS_VISITED="방문완료";
	/**m_process 렌탈중*/
	final static public String PROCESS_RENT="렌탈중";
	/**m_process 체험전*/
	final static public String PROCESS_BEFORE_TEST="체험전";
	/**m_process 체험중*/
	final static public String PROCESS_TEST="체험중";
	/**m_process 일시불*/
	final static public String PROCESS_ILSI="일시불";
	/**m_process 반품*/
	final static public String PROCESS_RETURN="반품";
	/**m_process 회수*/
	final static public String PROCESS_EXIT="회수";
	/**m_process 해지반품*/
	final static public String PROCESS_HAEBAN="해지반품";
	/**m_process 중도상환*/
	final static public String PROCESS_JOONGDO="중도상환";
	/**m_process 구매취소*/
	final static public String PROCESS_CANCEL_BUY="구매취소";
	/**m_process 렌탈완료*/
	final static public String PROCESS_RENT_FINISH="렌탈완료";
	/**m_process 일시불완료*/
	final static public String PROCESS_ILSI_FINISH="일시불완료";
	
	
	/**정산미완료*/
	final static public int JUNGSAN_NOT_FINTISH=0;
	/**정산완료*/
	final static public int JUNGSAN_FINISH=1;
	
}
