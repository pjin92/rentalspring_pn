package rental.constant;

public class Vaccount {
	
	/**가상계좌 상태(사용만료계좌)<br>
	 * 곱해줘서 update한다.*/
	static public final int REUSE_DEL=-1;
	/**가상계좌 상태(1회용 계좌)*/
	static public final int REUSE_FALSE=1;
	/**가상계좌 상태(재사용 계좌)*/
	static public final int REUSE_TRUE=2;
	
	
	/**가상계좌 등록상태 등록완료*/
	static public final String CONDITION_LIVE="등록완료";
	/**가상계좌 등록상태 삭제*/
	static public final String CONDITION_DEAD="삭제완료";
}
