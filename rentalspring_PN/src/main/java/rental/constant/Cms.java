package rental.constant;

public class Cms {

	/**은행회원*/
	static public final int CR_TYPE_BANK=1;
	/**카드회원*/
	static public final int CR_TYPE_CARD=6;
	
	/**신청전문조회 data구분 회원*/
	static public final String SDSI_DATA_MEM="MEM";
	
	/**신청전문조회 data구분 출금*/
	static public final String SDSI_DATA_PAY="PAY";
	
}
