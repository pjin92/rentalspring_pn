package rental.constant;

/**상수*/
public class Employee {
	
	/**ps_pay_idx 등록*/
	final static public int ADMIN_AUTH = 3;
	
	/**e_power 상담원*/
	final static public int POWER_SANGDAM= 1;
	/**e_power cs관리*/
	final static public int POWER_CS_MANAGER= 2;
	/**e_power DB관리*/
	final static public int POWER_DB_MANAGER= 3;
	/**e_power 설치기사*/
	final static public int POWER_KISA= 4;
	/**e_power 일반기사*/
	final static public int POWER_KISA_NORMAL= 5;
	/**e_power 설치배송관리*/
	final static public int POWER_KISA_MANAGER= 6;
	/**e_power admin*/
	final static public int POWER_ADMIN= 7;
	/**e_power CMS관리*/
	final static public int POWER_CMS_MANAGER= 8;
	/**e_power 샵쇼핑관리*/
	final static public int POWER_SHOP= 9;
	
	/**직원,기사 상태  정상*/
	final static public String EMPLOYEE_STATUS_NORMALCY="정상";
	/**직원,기사 상태  퇴사*/
	final static public String EMPLOYEE_STATUS_LEAVE="퇴사";
	/**직원,기사 상태  사용중지*/
	final static public String EMPLOYEE_STATUS_STOPUSING="사용중지";

}
