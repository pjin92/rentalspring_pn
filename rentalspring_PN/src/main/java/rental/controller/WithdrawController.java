package rental.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.Cms;
import rental.constant.MemberInfo;
import rental.constant.Pay;
import rental.constant.Sms;
import rental.service.CmsService;
import rental.service.DeliveryService;
import rental.service.EmployeeService;
import rental.service.MemberService;
import rental.service.PayService;
import rental.service.ProductService;
import rental.service.SmsService;
import rental.service.VaccountService;

@Controller
@RequestMapping("withdraw/")
public class WithdrawController {

	@Autowired
	CmsService cmsService;
	@Autowired
	MemberService memberService;
	@Autowired
	DeliveryService deliveryService;
	@Autowired
	ProductService productService;
	@Autowired
	PayService payService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	VaccountService vaccountService;
	@Autowired
	SmsService smsService;
	
	//은행 출금관리
	@RequestMapping(value="bank",method=RequestMethod.GET)
	public ModelAndView bank() {
		ModelAndView mav=new ModelAndView("withdraw/bank");
		Calendar now=Calendar.getInstance();
		mav.addObject("yearNow", now.get(Calendar.YEAR));
		mav.addObject("monthNow", now.get(Calendar.MONTH)+1);
		mav.addObject("memberGroups", memberService.selectMemberGroupList());
		mav.addObject("payYear",payService.payYearRange());
		mav.addObject("payDates",memberService.payDates());
		return mav;
	}
	
	//cms 은행출금 일자별 현황 dt
	@RequestMapping(value="bank/sendList",method=RequestMethod.POST)
	public @ResponseBody Object bankSend(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Calendar now=Calendar.getInstance();
		List<Map<String,Object>> clist=payService.selectPayStatistic(pmap.getMap());
		int filtered=0;
		if(clist!=null&&clist.size()!=0) {
			DecimalFormat df=new DecimalFormat("#,###,###,###,###,###,###");
			for(int i=0;i<clist.size();i++) {
				Map<String,Object> mapTemp=clist.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				String ps_date=(String)mapTemp.get("ps_date");
				String td="<a onclick='psDate()'>"+ps_date+"</a>";
				mapTemp.put("ps_date", td);
				
				//숫자 컴마 붙이기
				mapTemp.put("total", df.format(mapTemp.get("total")));
				mapTemp.put("ing", df.format(mapTemp.get("ing")));
				mapTemp.put("suc", df.format(mapTemp.get("suc")));
				mapTemp.put("fail", df.format(mapTemp.get("fail")));
				//결과요청버튼
				try {
					String resultBttn="<a ps_date='"+ps_date+"' ";
					Date ps=sdf.parse(ps_date);
					Calendar ps_cal=Calendar.getInstance();
					ps_cal.setTime(ps);
					ps_cal.set(Calendar.HOUR_OF_DAY, Pay.SEND_PAY_TIME);
					
					now.add(Calendar.DATE, 1);
					//주말넘어가기
					int addDate=now.get(Calendar.DAY_OF_WEEK);
					if(addDate==7) {
						now.add(Calendar.DATE, 2);
					}else if(addDate==1) {
						now.add(Calendar.DATE, 1);
					}
					
					if (now.before(ps_cal)) {
					//신청일이 오늘 보다 미래(신청가능)
						resultBttn+="class='btn bg-primary legitRipple mr-10' onclick='sendCms(\"put\")'>";
						resultBttn+=" 신 청 ";
						resultBttn+="</a>";
						resultBttn+="<a class='btn bg-success legitRipple mr-10' onclick='sendCms(\"get\")'>신청내역 조회</a>";
						resultBttn+="<a class='btn bg-warning legitRipple mr-10' onclick='sendCms(\"delete\")'>신청취소";
					}else {
						now.add(Calendar.DATE, -1);
					//신청일이 오늘보다 이전(과거)
						ps_cal.add(Calendar.DATE, 1);//다음날 11시 이후 결과요청 가능
						ps_cal.set(Calendar.HOUR_OF_DAY, Pay.SEND_RESULT_TIME);
						
						if(now.after(ps_cal)) {
						//어제보다 이전
							resultBttn+="class='btn bg-slate-600 legitRipple mr-10' onclick='getResult()'>";
							resultBttn+="결과 요청";
						}else {
						//어제~오늘
							resultBttn+=" class='btn bg-success legitRipple mr-10' onclick='sendCms(\"get\")'>신청내역 조회</a>";
							resultBttn+="<a class='btn bg-slate-300 legitRipple'>";
							resultBttn+="결과 대기";
						}
					}
					resultBttn+="</a>";
					mapTemp.put("cms_result", resultBttn);
				}catch (Exception e) {
					mapTemp.put("cms_result", "오류 발생");
				}
			}
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", payService.countPayStatistic());
		return returnMap;
	}
	
	//출금신청 목록에 추가(바구니에 담기)
	@RequestMapping(value="bank/send",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankSendPost(String pay_date,String pay_idx) {
		String msg="";
		if(cmsService.isRegTime(pay_date,Pay.SEND_PAY_TIME)) {
			msg=payService.paySend(pay_date,pay_idx);
		}else {
			msg="false";
		}
		return msg;
	}
	
	//출금 dataTable조회
	@RequestMapping(value="pay",method=RequestMethod.POST)
	public @ResponseBody Object pay(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		List<Map<String,Object>> list=payService.selectPayList(pmap.getMap());
		int filtered=0;
		DecimalFormat df=new DecimalFormat("#,###,###,###,###,###,###");
		for(int i=0;i<list.size();i++) {
			Map<String,Object> mapTemp=list.get(i);
			int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
			mapTemp.put("pay_idx", "<input type='hidden' name='pay_idx' value='"+mapTemp.get("pay_idx")+"'>");
			mapTemp.put("pay_money", df.format(mapTemp.get("pay_money")));
			
			String reason=mapTemp.get("pay_reason")+"";
			String pay_method=mapTemp.get("pay_method")+"";
			String pay_state=mapTemp.get("pay_state")+"";
			String bttn="";
			if((pmap.get("m_pay_method").equals(MemberInfo.PAY_METHOD_CARD)&&pay_state.equals(Pay.STATE_UNPAID))||pay_method.equals(Pay.METHOD_CARD)) {
				if(!reason.equals(Pay.REASON_V_ACCOUNT)&&!reason.startsWith(Pay.REASON_DIRECT)) {
					if(!mapTemp.get("pay_state").equals(Pay.STATE_UNPAID)||!mapTemp.get("pay_num").equals("")) {
						bttn+="<a class='btn bg-slate-600 legitRipple' onclick='checkPay()'>조회</a>";
					}
					if(mapTemp.get("pay_state").equals(Pay.STATE_PAID)) {
						bttn+="<a class='btn bg-warning legitRipple ml-5' onclick='cancelPay()'>취소</a>";
					}
				}
			}else{
				if(mapTemp.get("pay_state").equals(Pay.STATE_UNPAID)&&mapTemp.get("pay_num").equals("")) {
					bttn+="<a class='btn btn-primary legitRipple' onclick='realTime()'>출금</a>";
				}else if(reason.startsWith("실시간")&&!mapTemp.get("pay_num").equals("")){
					bttn+="<a class='btn bg-slate-600 legitRipple' onclick='checkPay()'>조회</a>";
				}
				
			}
			
			String m_pay_owner=(String)mapTemp.get("m_pay_owner");
			if(m_pay_owner==null||m_pay_owner.equals("")) {
				m_pay_owner="(결제자 이름 없음)";
			}
			mapTemp.put("m_pay_owner", "<a onclick='member()' m_idx='"+mapTemp.get("m_idx")+"'>"+m_pay_owner+"</a>");
			mapTemp.put("bttn", bttn);
			if(filtered<rownum) {
				filtered=rownum;
			}
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", list);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", payService.countPayList(pmap.getMap()));
		return returnMap;
	}
	
	@RequestMapping(value="direct",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object directPost(String pay_idx,String pay_date,String pay_reason) {
		String msg="";
		try{
			StringTokenizer st=new StringTokenizer(pay_idx,",");
			while(st.hasMoreTokens()) {
				int payidx=Integer.parseInt(st.nextToken());
				Map<String,Object>payMap=payService.selectPayDetail(payidx);
				String pay_state=payMap.get("pay_state")+"";
				if(pay_state.equals(Pay.STATE_UNPAID)){
					Map<String,Object> map=new HashMap<String, Object>();
					map.put("pay_idx", payidx);
					map.put("pay_real", payMap.get("pay_money"));
					map.put("pay_reason",Pay.REASON_DIRECT+pay_reason);
					map.put("pay_num",Pay.NUM_DIRECT);
					map.put("pay_date", pay_date);
					map.put("pay_state", Pay.STATE_PAID);
					
					if(payMap.get("m_pay_method").equals(MemberInfo.PAY_METHOD_CARD)){
						map.put("pay_method", Pay.METHOD_CARD);
					}else {
						map.put("pay_method", Pay.METHOD_BANK);
					}
					payService.updatePay(map);
				}else {
					msg="\n신청전 상태만 직접입금처리 가능합니다.";
				}
			}
		}catch (Exception e) {
			Log.warn("직접입금 처리 중 오류:"+e.toString()+"/"+pay_idx+"/"+pay_date+"/"+pay_reason);
			return "오류 발생";
		}
		return "직접입금 처리하였습니다."+msg;
	}
	
	@RequestMapping(value="direct",method=RequestMethod.DELETE,produces="text/plain;charset=utf8")
	public @ResponseBody Object directDelete(String pay_idx) {
		String msg="";
		try{
			StringTokenizer st=new StringTokenizer(pay_idx,",");
			while(st.hasMoreTokens()) {
				int payidx=Integer.parseInt(st.nextToken());
				Map<String,Object> pay=payService.selectPayDetail(payidx);
				String pay_num=pay.get("pay_num")+"";
				String pay_state=pay.get("pay_state")+"";
				if(pay_state.equals(Pay.STATE_PAID)&&pay_num.equals(Pay.NUM_DIRECT)){
					Map<String,Object> map=new HashMap<String, Object>();
					map.put("pay_idx", payidx);
					map.put("pay_reason","");
					map.put("pay_num","");
					map.put("pay_date", "");
					map.put("pay_state", Pay.STATE_UNPAID);
					map.put("pay_method", "");
					payService.updatePay(map);
				}else {
					msg="\n직접입금 처리건만 취소 처리 가능합니다.";
				}
			}
		}catch (Exception e) {
			Log.warn("직접입금 취소 처리 중 오류:"+e.toString()+"/"+pay_idx);
			return "오류 발생";
		}
		return "직접입금 취소 처리하였습니다."+msg;
	}
	
	//신청날짜 은행출금 신청 목록 보기(dataTable)
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.POST)
	public @ResponseBody Object bankSendDetail(@PathVariable String ps_date,ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		List<Map<String,Object>> list=payService.selectPaySendByDate(pmap.getMap(),ps_date, null);
		int filtered=0;
		if(list!=null&&list.size()!=0) {
			DecimalFormat df=new DecimalFormat("#,###,###,###,###,###,###");
			for(int i=0;i<list.size();i++) {
				Map<String,Object> mapTemp=list.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				mapTemp.put("ps_idx", "<input name='ps_idx' type='hidden' value='"+mapTemp.get("ps_idx")+"'/>");
				mapTemp.put("ps_money", df.format(Long.parseLong(mapTemp.get("ps_money")+"")));
			}
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", list);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", payService.countPaySendByDate(ps_date, ""));
		return returnMap;
	}
	
	//신청날짜로 은행출금 신청 조회
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object sendListCmsPut(@PathVariable String ps_date) {
		return cmsService.regiCmsRead(ps_date,Cms.SDSI_DATA_PAY);
	}
	
	//은행출금 신청(효성CMS)
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.PUT,produces="text/plain;charset=utf8")
	public @ResponseBody Object sendListCmsGet(@PathVariable String ps_date) {
		String msg;
		try{
			msg=cmsService.payCms(ps_date, 10);
		}catch (Exception e) {
			msg="오류발생";
		}
		return msg;
	}
	//은행출금 신청 취소(효성CMS)
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.DELETE,produces="text/plain;charset=utf8")
	public @ResponseBody Object sendListCmsDelete(@PathVariable String ps_date) {
		String msg;
		try{
			msg=cmsService.payCms(ps_date, 0);
		}catch (Exception e) {
			msg="오류발생";
		}
		return msg;
	}
	
	//은행 출금 신청 결과 받기
	@RequestMapping(value="bank/result/{ps_date}",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankResult(@PathVariable String ps_date) {
		String msg="";
		msg=cmsService.cmsResult(ps_date, Cms.SDSI_DATA_PAY);
		
		return msg;
	}
	
	//은행 출금 신청 대기에서 sms보내기
	@RequestMapping(value="bank/sendSms",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankSendSms(ParamMap pmap) {
		int e_id=pmap.getLoginEid();
		
		Calendar payDate=Calendar.getInstance();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String ps_date=pmap.get("ps_date")+"";
		try{
			payDate.setTime(sdf.parse(ps_date));
			payDate.add(Calendar.DATE, 1);//공휴일,주말 경우 출금일 밀리는 것 적용 안됨
		}catch (Exception e) {
			return "날짜형식 오류입니다.";
		}
		
		String sms_date=payDate.get(Calendar.YEAR)+"년 "+(payDate.get(Calendar.MONTH)+1)+"월 "+payDate.get(Calendar.DATE)+"일 ";
		
		List<Map<String,Object>> psList=payService.selectPaySendSmsList(ps_date);
		int success=0;
		for(int i=0;i<psList.size();i++) {
			Map<String,Object> map_pay=psList.get(i);
			int m_idx=Integer.parseInt(map_pay.get("m_idx")+"");
			String m_phone=map_pay.get("m_phone")+"";
			String pr_sms=map_pay.get("pr_sms")+"";
			String sms_content=map_pay.get("pr_sms_company")+"\n"+map_pay.get("m_pay_owner")+" 님 "+sms_date+" "+map_pay.get("pay_cnt")+"회 렌탈료 "+map_pay.get("pay_money_f")+"원 출금될 예정입니다.";
			String result=smsService.sendSms(Sms.TYPE_PAY, e_id, m_idx, m_phone, pr_sms, sms_content);
			
			if(result.equals("문자발송 성공")) {
				success++;
			}
		}
		
		if(success==psList.size()) {
			return success+"건 문자 발송하였습니다.";
		}else {
			return success+"/"+psList.size()+"건 문자 발송하였습니다.";
		}
	}
		
		
	//금융관리(카드회원)
	@RequestMapping(value="card",method=RequestMethod.GET)
	public ModelAndView card() {
		ModelAndView mav=new ModelAndView("withdraw/card");
		Calendar now=Calendar.getInstance();
		mav.addObject("yearNow", now.get(Calendar.YEAR));
		mav.addObject("monthNow", now.get(Calendar.MONTH)+1);
		mav.addObject("memberGroups", memberService.selectMemberGroupList());
		mav.addObject("payYear",payService.payYearRange());
		mav.addObject("payDates",memberService.payDates());
		return mav;
	}
	
	//카드출금신청
	@RequestMapping(value="card",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object cardPost(String pay_idx,ParamMap pmap) {
		int e_id=0;
		try{
			Map<String,Object> login=(Map)pmap.get("login");
			e_id=Integer.parseInt(login.get("e_id")+"");
		}catch (Exception e) {}
		return cmsService.payCard(pay_idx,e_id);
	}
	
	//카드출금조회
	@RequestMapping(value="card/{pay_idx}",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object cardGet(@PathVariable String pay_idx) {
		int payidx=Integer.parseInt(pay_idx);
		return cmsService.payCardCheck(payidx);
	}
	//카드출금취소
	@RequestMapping(value="card/{pay_idx}",method=RequestMethod.DELETE,produces="text/plain;charset=utf8")
	public @ResponseBody Object cardDelete(@PathVariable String pay_idx) {
		int payidx=Integer.parseInt(pay_idx);
		return cmsService.payCardCancel(payidx);
	}
	
	//은행 실시간출금 조회
	@RequestMapping(value="bank/{pi}",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankPi(@PathVariable String pi) {
		int pay_idx=Integer.parseInt(pi);
		return cmsService.realtimeCheck(pay_idx);
	}
	//은행 실시간출금
	@RequestMapping(value="bank/{pi}",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankPiPost(@PathVariable String pi) {
		int pay_idx=Integer.parseInt(pi);
		return cmsService.realtimePay(pay_idx);
	}
	
	//채권관리
	@RequestMapping(value="statistic",method=RequestMethod.GET)
	public ModelAndView statistic() {
		ModelAndView mav=new ModelAndView("withdraw/statistic");
		mav.addObject("prList",productService.selectrentalList(null));
		mav.addObject("deptList",employeeService.selectDeptList());
		mav.addObject("empList",employeeService.selectEmployeeListByDepartment(null));
		return mav;
	}
	//채권관리 dt
	@RequestMapping(value="statistic",method=RequestMethod.POST)
	public @ResponseBody Object statisticPost(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();

		List<Map<String,Object>> list=payService.withdrawStatistic(pmap.getMap());
		int filtered=0;
		int total=payService.countWithdrawStatistic(pmap.getMap());
		
		if((pmap.get("length")+"").equals("-1")) {
			filtered=total;
		}else {
			if(list!=null&&list.size()!=0) {
				DecimalFormat df=new DecimalFormat("#,###,###,###,###,###,###");
				for(int i=0;i<list.size();i++) {
					Map<String,Object> mapTemp=list.get(i);
					int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
					if(filtered<rownum) {
						filtered=rownum;
					}
				}
			}
			
			try {
				filtered+=Integer.parseInt(pmap.get("start")+"");
			}catch (Exception e) {}
		}
		
		returnMap.put("data", list);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", total);
		return returnMap;
	}
	
	//채권관리 팝업
	@RequestMapping(value="statistic/{midx}",method=RequestMethod.GET)
	public ModelAndView statisticPopup(@PathVariable String midx) {
		ModelAndView mav=new ModelAndView("withdraw/statisticDetail");
		int m_idx=Integer.parseInt(midx);
		mav.addObject("mi_contract",memberService.selectMemberInfo(m_idx, MemberInfo.TYPE_CONTRACT));
		Map<String,Object> member=memberService.selectMemberDetail(m_idx);
		mav.addObject("member",member);
		mav.addObject("ds",deliveryService.selectDeliveryState(m_idx));
		try {
			String m_pay_num=(String)member.get("m_pay_num");
			int length=m_pay_num.length();
			if(length>4) {
				m_pay_num=m_pay_num.substring(0,4);
				for(int i=0;i<length-4;i++) {
					m_pay_num+="*";
				}
			}
			member.put("m_pay_num", m_pay_num);
			
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Calendar now=Calendar.getInstance();
			mav.addObject("today", sdf.format(now.getTime()));
//			String m_paystart=(String)member.get("m_paystart");
//			Date paystart=sdf.parse(m_paystart);
//			String m_end_date=(String)member.get("m_end_date");
//			try {
//				Date end=sdf.parse(m_end_date);
//				
//				long diff = end.getTime() - paystart.getTime();
//				long diffDays = diff / (24 * 60 * 60 * 1000);
//				System.out.println("위약금 일수 "+diffDays);
//			}catch (Exception ee) {
//				//System.out.println("해지일 없음.");
//			}
			mav.addObject("pay",payService.selectPayListWithMaxCnt2(m_idx));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return mav;
	}
	
	//채권관리 팝업>3.메모 저장
	@RequestMapping(value="statistic/{midx}/memo",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object statisticMemo(@PathVariable String midx,String m_memo) {
		String msg="";
		int m_idx=Integer.parseInt(midx);
		Map<String,Object>map=new HashMap<String, Object>();
		map.put("m_idx", m_idx);
		map.put("m_memo", m_memo);
		int result=memberService.updateMember(map);
		if(result>0) {
			msg="저장 완료.";
		}else {
			msg="저장 실패.\n다시 시도해주세요.";
		}
		return msg;
	}
	
	//채권관리 팝업>출력페이지
	@RequestMapping(value="statistic/{midx}/print",method=RequestMethod.GET)
	public ModelAndView statisticPopupPrint(@PathVariable String midx) {
		ModelAndView mav=new ModelAndView("withdraw/statisticDetailPrint");
		int m_idx=Integer.parseInt(midx);
		mav.addObject("mi_contract",memberService.selectMemberInfo(m_idx, MemberInfo.TYPE_CONTRACT));
		Map<String,Object> member=memberService.selectMemberDetail(m_idx);
		mav.addObject("member",member);
		mav.addObject("memberDelay",payService.selectMemberDelayMoney(m_idx));
		mav.addObject("firstDelay",payService.selectPayDelayDate(m_idx));
		
		try {
			String m_pay_num=(String)member.get("m_pay_num");
			int length=m_pay_num.length();
			if(length>4) {
				m_pay_num=m_pay_num.substring(0,4);
				for(int i=0;i<length-4;i++) {
					m_pay_num+="*";
				}
			}
			member.put("m_pay_num", m_pay_num);
			
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			String m_paystart=(String)member.get("m_paystart");
			Date paystart=sdf.parse(m_paystart);
			String m_end_date=(String)member.get("m_end_date");
			Date end=sdf.parse(m_end_date);
			
			long diff = end.getTime() - paystart.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);
			
		}catch (Exception e) {
			//e.printStackTrace();
		}
		
		return mav;
	}
	
	//가상계좌
	@RequestMapping(value="vaccount",method=RequestMethod.GET)
	public ModelAndView vaccount() {
		ModelAndView mav=new ModelAndView("withdraw/vaccount");
		
		return mav;
	}
	//가상계좌 dt
	@RequestMapping(value="vaccount",method=RequestMethod.POST)
	public @ResponseBody Object vaccountPost(ParamMap pmap) {
		pmap.put("mainKeyword", "%"+pmap.get("mainKeyword")+"%");
		Map<String, Object> returnMap=new HashMap<String, Object>();
		List<Map<String,Object>> list=vaccountService.withdrawVaccount(pmap.getMap());
		int filtered=0;
		int total=vaccountService.countwithdrawVaccount(pmap.getMap());
		
		if((pmap.get("length")+"").equals("-1")) {
			filtered=total;
		}else {
			if(list!=null&&list.size()!=0) {
				DecimalFormat df=new DecimalFormat("#,###,###,###,###,###,###");
				for(int i=0;i<list.size();i++) {
					Map<String,Object> mapTemp=list.get(i);
					int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
					if(filtered<rownum) {
						filtered=rownum;
					}
				}
			}
			
			try {
				filtered+=Integer.parseInt(pmap.get("start")+"");
			}catch (Exception e) {}
		}
		
		returnMap.put("data", list);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", total);
		return returnMap;
	}
	@RequestMapping(value="checkVPay",method=RequestMethod.GET,produces="text/plain;charset=utf-8")
	public @ResponseBody Object checkVPay() {
		String msg=cmsService.checkVPay(null);
		System.out.println(msg);
		return msg;
	}
	
}
