package rental.controller;

import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Common;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.InstallState;
import rental.constant.MemberInfo;
import rental.constant.ProductRental;
import rental.service.CsService;
import rental.service.DeliveryService;
import rental.service.MemberService;
import rental.service.ProductService;
import rental.service.EmployeeService;
import rental.service.InstallService;

@Controller
@RequestMapping("install/")
public class InstallController {

	@Autowired
	DeliveryService deliveryService;
	@Autowired
	InstallService installService;

	@Autowired
	EmployeeService employeeService;

	@Autowired
	MemberService memberService;

	@Autowired
	CsService csservice;
	
	@Autowired
	ProductService productService;

	@RequestMapping(value = "installComplete", method = RequestMethod.GET)
	public ModelAndView installList() {
		ModelAndView mav = new ModelAndView("install/installComplete");
		return mav;
	}
	/**
	 * CS점검관리 리스트
	 */
	@RequestMapping(value = "installComplete", method = RequestMethod.POST)
	public @ResponseBody Object install(ParamMap map) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		int e_power = Integer.parseInt(((HashMap)map.get("login")).get("e_power").toString()); 
		int e_id = Integer.parseInt(((HashMap)map.get("login")).get("e_id").toString());
		
		map.put("e_power", e_power);
		map.put("e_id", e_id);
		List<Map<String, Object>> clist = installService.selectInstallList(map.getMap());
		int filtered = 0;
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String m_idx = mapTemp.get("m_idx") + "";
				String mi_idx = mapTemp.get("mi_idx") + "";
				String td = "<a m_idx='" + m_idx + "' ' onclick='member()'>" + mapTemp.get("mi_name") + "</a>";
				mapTemp.put("mi_name", td);
				mapTemp.put("m_idx", "");
				String pr_code = mapTemp.get("pr_rentalcode") + "";
				String td3 = "<a pr_rentalcode='" + pr_code + "' ' onclick='rentalProduct()'>" + mapTemp.get("pr_name")
						+ "</a>";
				mapTemp.put("pr_name", td3);
				if (mapTemp.get("m_install_finish_date").equals("") || mapTemp.get("m_install_finish_date") == null) {
					mapTemp.put("m_install_finish_date", "미설치");
				} else {
					mapTemp.put("m_install_finish_date", mapTemp.get("m_install_finish_date"));
				}
				if (mapTemp.get("ds_state").equals(InstallState.NAME_INSTALL_INSTALL_COMPLETE)) {

					String td2 = "<a href='/file/install/" + mapTemp.get("if_idx") + "_" + mapTemp.get("if_memo") + "'>"
							+ mapTemp.get("if_file") + " </a>";
					mapTemp.put("m_tech", td2);
				} else {
					String td2 = "<a m_idx='" + m_idx + "' mi_idx='" + mi_idx
							+ "' onclick='installComplete()'> 설치확인서등록 </a>";
					mapTemp.put("m_tech", td2);
				}

				String td4 = "<a m_idx='" + m_idx + "' e_id ='"+mapTemp.get("m_eid")+"' onclick='inspect()'> 점검목록 </a>";
				mapTemp.put("inspect", td4);
				// 주소
				String addr = mapTemp.get("mi_addr1") + "";
				addr += " " + mapTemp.get("mi_addr2");
				addr += " " + mapTemp.get("mi_addr3");
				mapTemp.put("mi_addr", addr);

				mapTemp.put("rental", "렌탈");
				mapTemp.put("yangdo", "양도");
			}
		}
		try {
			filtered += Integer.parseInt(map.get("start") + "");
		} catch (Exception e) {
		}
		if (clist == null) {
			clist = new ArrayList<Map<String, Object>>();
		}
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);

		// returnMap.put("recordsTotal", deliveryService.selectInstallList(map.getMap()));
		return returnMap;
	}

	/**
	 * 설치확인서 등록 GET
	 */
	@RequestMapping(value = "addComplete", method = RequestMethod.GET)
	public ModelAndView addComplete(ParamMap map) {
		ModelAndView mav = new ModelAndView("install/addComplete");
		int m_idx = Integer.parseInt(map.getMap().get("m_idx").toString());
		int mi_idx = Integer.parseInt(map.getMap().get("mi_idx").toString());
		mav.addObject("m_idx", m_idx);
		mav.addObject("mi_idx", mi_idx);
		
		List<Map<String,Object>> flist=installService.selectinstallFileList(m_idx);
		if(flist!=null&&flist.size()>0) {
			mav.addObject("install", flist.get(0));
			mav.setViewName("install/addCompleted");
			
			mav.addObject("ds",deliveryService.selectDeliveryState(m_idx).get(0));
		}
		return mav;
	}
	
	/**설치확인서 2.확인 내용 수정*/
	@RequestMapping(value="addComplete",method=RequestMethod.PUT,produces="text/plain;charset=utf8")
	public @ResponseBody Object addCompletePut(ParamMap map) {
		try{
			installService.updateInstallFile(map.getMap());
			return "수정완료";
		}catch (Exception e) {
			e.printStackTrace();
			return "오류발생";
		}
	}
 
//	
//	@RequestMapping(value = "installfileList", method = RequestMethod.GET)
//	public @ResponseBody Object installfileList(@PathVariable String if_name, @PathVariable String m_idx) {
//
//		if (if_name == null || if_name.equals("")) {
//			return null;
//		}
//		return installService.selectinstallFileList(Integer.parseInt(m_idx), InstallState.NAME_INSTALL_INSTALL_COMPLETE);
//
//	}

	/** 설치확인서 등록완료 */
	@RequestMapping(value = "installconfirmation", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object installconfirmation(ParamMap map, MultipartFile file) {
		
		int midx = Integer.parseInt(map.get("m_idx") + "");
		Map<String,Object> member=memberService.selectMemberDetail(midx);
		String m_jubsoo=member.get("m_jubsoo")+"";
		
		String m_jubsoo_selected=map.get("m_jubsoo")+"";//일반,선설치
		if(m_jubsoo_selected.equals("일반설치")) {
			if(!(m_jubsoo.equals(MemberInfo.JUBSOO_PERSONAL)||m_jubsoo.equals(MemberInfo.JUBSOO_RENTAL))) {
				return "접수구분: 렌탈,개인영업 일 때만 일반 설치 완료 가능합니다.";
			}
		}else {
			//방문견적/무료체험설치
			if(m_jubsoo.equals(MemberInfo.JUBSOO_VISIT)||m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)) {
				
			}else{
				return "접수구분: 방문견적,무료체험 일 때만 방문견적/무료체험설치 완료 가능합니다.";
			}
		}
		
		String msg =installService.insertInstallFile(map.getMap(), file);
		return msg;
		
		
	}
	/** 설치확인서 재등록. */
	@RequestMapping(value = "reuploadInstallFile", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object reuploadInstallFile(ParamMap map, MultipartFile file) {
		if(file==null){
			return "파일이 없습니다.";
		}
		String msg =installService.reuploadInstallFile(map.getMap(), file);
		return msg;
	}

	/**
	 * CS점검관리 방문예정일 목록
	 */
	@RequestMapping(value = "inspect/{m_idx}")
	public @ResponseBody Object inspect(ParamMap pmap, @PathVariable String m_idx) {
		ModelAndView mav = new ModelAndView("install/table/inspectTR");

		pmap.put("m_idx", m_idx);
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> clist = memberService.customerCsList(pmap.getMap());
		int filtered = 0;
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				String midx = mapTemp.get("m_idx") + "";
				String td3 = "<a data-toggle='modal' cl_idx='" + mapTemp.get("cl_idx") + "' m_idx='" + midx
						+ "' onclick='select_Date()'>" + mapTemp.get("cl_date") + "</a>";
				mapTemp.put("cl_date", td3);
			}
		}

		mav.addObject("clist", clist);
		return mav;
	}

	/**
	 * CS점검관리 방문예정일 목록 세부내용 Table로 불러서 AJAX로 append함
	 */
	@RequestMapping(value = "inspectDetail", method = RequestMethod.GET, produces = "text/plain;charset=utf8")
	public @ResponseBody Object inspectDetail(ParamMap pmap) {
		ModelAndView mav = new ModelAndView("install/table/inspectDetailTR");
		pmap.put("m_idx", pmap.get("m_idx"));
		pmap.put("cl_idx", pmap.get("cl_idx"));

		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> clist = installService.customerCsList(pmap.getMap());
		int filtered = 0;
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				String midx = mapTemp.get("m_idx") + "";
				String miidx = mapTemp.get("mi_idx") + "";
				String td = "<a data-toggle='modal' href='#myModal4'  m_idx='" + midx
						+ "' onclick='install_member()' cl_idx='" + mapTemp.get("cl_idx") + "'>"
						+ mapTemp.get("mi_name") + "</a>";
				mapTemp.put("mi_name", td);
				mapTemp.put("m_idx", "");
				// 주소
				String addr = mapTemp.get("mi_addr1") + "";
				addr += " " + mapTemp.get("mi_addr2");
				addr += " " + mapTemp.get("mi_addr3");
				mapTemp.put("mi_addr", addr);
				

				String td6 = "<a data-toggle='modal' href='#myModal5' csm_idx='" + mapTemp.get("csm_idx")
						+ "' onclick='cslist()' >" + mapTemp.get("csm_name") + " </a>";
				mapTemp.put("csm_name", td6);
			

				if(mapTemp.get("state").equals("설치완료")) {
					
					String td3 = mapTemp.get("cl_date")+"" ;
					mapTemp.put("cl_date", td3);
					String td4 =mapTemp.get("e_name")+"";
					mapTemp.put("m_tech", td4);

					String td5 = mapTemp.get("csds_state")+"" ;
					mapTemp.put("state", td5);
					
					
				}else {
					
				String td3 = "<a data-toggle='modal' href='#myModal2' cl_idx='" + mapTemp.get("cl_idx")
						+ "' onclick='changeDate()'>" + mapTemp.get("cl_date") + "</a>";
				mapTemp.put("cl_date", td3);
				String td4 = "<a data-toggle='modal' m_idx='" + midx + "' mi_idx='" + miidx + "'cl_idx='"
						+ mapTemp.get("cl_idx") + "' onclick='technician()' href='#myModal3'> " + mapTemp.get("e_name")
						+ " </a>";
				mapTemp.put("m_tech", td4);

				String td5 = "<a state='" + mapTemp.get("csds_state") + "' m_idx='" + midx + "' cl_idx='"
						+ mapTemp.get("cl_idx") + "'csds_idx='" + mapTemp.get("csds_idx")
						+ "' onclick='addCsInstall()'> " + mapTemp.get("csds_state") + "</a>";
				mapTemp.put("state", td5);

				}
			}
		}

		mav.addObject("clist", clist);
		return mav;
	}

	
	/**
	 * CS 방문예정일 변경
	 * */
	@RequestMapping(value = "inspectUpdate", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object inspectUpdate(ParamMap map) {
		String msg = "";
		String date = map.get("date") + "";
		String time = map.get("time") + "";
		int result =0;
		int result2 =0;
		HashMap<String, Object> rmap = new HashMap<String, Object>();
		String datetime = date + " " + time;
		rmap.put("cl_idx", map.get("cl_idx") + "");
		rmap.put("dateTime", datetime);

		Map<String,Object> orgDate = installService.selectOrigDate(Integer.parseInt(rmap.get("cl_idx")+""));
		
		try {
			if(orgDate.get("cl_original_date").equals("") || orgDate.get("cl_original_date")==null) {
			    rmap.put("cl_original_date", orgDate.get("cl_date"));
				result = installService.updateOrgDate(rmap);
				 result2 = installService.updateClDate(rmap);
			}else {
				
				 result = installService.updateClDate(rmap);
			}
			
			msg = "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "등록 실패";
		}

		System.out.println(msg + " / msg");
		return msg;
	}

	
	/**
	 * 기사리스트
	 * */
	@RequestMapping(value = "updateTechnician", method = RequestMethod.GET)
	public @ResponseBody Object updateTechnicianView(ParamMap map) {
		ModelAndView mav = new ModelAndView("install/table/technicianTR");
		int m_idx = Integer.parseInt(map.getMap().get("m_idx").toString());
		int mi_idx = Integer.parseInt(map.getMap().get("mi_idx").toString());
		int cl_idx = Integer.parseInt(map.getMap().get("cl_idx").toString());
		List<Map<String, Object>> tlist = employeeService.selecttechnicianList(map.getMap());
		for (int i = 0; i < tlist.size(); i++) {
			Map<String, Object> mapTemp = tlist.get(i);
			mapTemp.put("t_name", mapTemp.get("e_name"));
			String td = "<a cl_idx='" + cl_idx + "'  m_idx='" + m_idx + "' e_name='" + mapTemp.get("e_name")
					+ "' e_id='" + mapTemp.get("e_id") + "' onclick='updateMatchTechnician()'>" + mapTemp.get("e_name")
					+ "</a>";
			String td2 = "<a e_id='" + mapTemp.get("e_id") + "' onclick='showTechStock()'>재고보기</a>";
			mapTemp.put("e_name", td);
			mapTemp.put("stock", td2);
			mapTemp.put("m_idx", m_idx);
			mapTemp.put("mi_idx", mi_idx);
		}
		mav.addObject("tlist", tlist);
		return mav;
	}
	
	/**
	 * 해당설치건의 기사변경
	 * */

	@RequestMapping(value = "updateTechnician", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object updateTechnician(ParamMap map) {
		String msg = "";
		try {
			int result = installService.updateTechnician(map.getMap());
			msg = "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "등록 실패";
		}

		System.out.println(msg + " / msg");
		return msg;
	}

	/**
	 * 해당설치건의 고객정보
	 * */
	@RequestMapping(value = "install_customer/{m_idx}", method = RequestMethod.GET, produces = "text/plain;charset=utf8")
	public @ResponseBody Object install_customer(ParamMap map, @PathVariable String m_idx) {
		ModelAndView mav = new ModelAndView("install/table/install_customerTable");
		map.put("m_idx", m_idx);
		Map<String, Object> i_customer = installService.selectInstallCustomer(map.getMap());

		mav.addObject("icm", i_customer);
		return mav;

	}

	/**
	 * 해당설치건의 소모품 리스트
	 * */
	@RequestMapping(value = "csList/{csm_idx}", method = RequestMethod.GET, produces = "text/plain;charset=utf8")
	public @ResponseBody Object cslist(ParamMap map, @PathVariable String csm_idx) {
		ModelAndView mav = new ModelAndView("install/table/csTable");
		List<HashMap<String, Object>> selectCsCompDetail = csservice.selectCsCompDetail(Integer.parseInt(csm_idx));
		for (int i = 0; i < selectCsCompDetail.size(); i++) {
			selectCsCompDetail.get(i).put("p_cost", Common.comma(selectCsCompDetail.get(i).get("p_cost") + ""));
			selectCsCompDetail.get(i).put("p_price", Common.comma(selectCsCompDetail.get(i).get("p_price") + ""));
		}
		mav.addObject("data", selectCsCompDetail);
		return mav;
	}

	/**
	 * 해당설치건의 고객메모
	 * */
	@RequestMapping(value = "updateCsInstallMemo/{cl_idx}", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object updateCsInstallMemo(ParamMap map, @PathVariable String cl_idx) {
		String msg = "";
		map.getMap().put("cl_idx", cl_idx);
		try {
			int result = installService.updateCsInstallMemo(map.getMap());
			msg = "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "등록 실패";
		}

		System.out.println(msg + " / msg");
		return msg;
	}

	
	/**
	 * 해당 설치건의 상태변경 (설치전->설치요청)
	 * */
	@RequestMapping(value = "csInstall/{cl_idx}", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object csInstall(ParamMap map, @PathVariable String cl_idx) {
		String msg = "";
		map.getMap().put("cl_idx", cl_idx);
		try {
			int result = installService.csInstall(map.getMap());
			msg = "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "등록 실패";
		}

		System.out.println(msg + " / msg");
		return msg;
	}
	/**
	 * 해당 설치건의 상태변경 (설치요청->설치완료)
	 * */
	@RequestMapping(value = "csInstallComplete/{csds_idx}", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object csInstallComplete(ParamMap map, @PathVariable String csds_idx) {
		String msg = "";
		map.getMap().put("csds_idx", csds_idx);
		map.getMap().put("cl_idx", map.getMap().get("cl_idx"));
		try {
			int result = installService.csInstallComplete(map.getMap());
			msg = "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "등록 실패";
		}

		System.out.println(msg + " / msg");
		return msg;
	}
	
	@RequestMapping(value = "csproductList" ,method= RequestMethod.GET)
	public @ResponseBody Object productList(ParamMap pmap) {
		ModelAndView mav = new ModelAndView("install/table/csproductTR");
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> plist = installService.selectCsProductList(pmap.getMap());
		int filtered = 0;
		if (plist != null && plist.size() != 0) {
			for (int i = 0; i < plist.size(); i++) {

				Map<String, Object> pMap = plist.get(i);
				String p_name = "<a idx='" + pMap.get("p_id") + "' onclick='choose(this)'>" + pMap.get("p_name")
						+ "</a>";
				pMap.put("p_name", p_name);
			}
		}

		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		mav.addObject("data",plist);
		mav.addObject("recoresFiltered",filtered);
		mav.addObject("recordsTotal",productService.countProduct(pmap.getMap()));
		return mav;
	}

	@RequestMapping(value="addcsProduct", method=RequestMethod.POST, produces ="application/text; charset=utf8")
	public @ResponseBody Object csProduct(ParamMap map) {
		int result = 0;
		int result2 = 0;
		String date = map.get("date") + "";
		String time = map.get("time") + "";
		String datetime = date + " " + time;
		map.getMap().put("cl_date", datetime);
		
		try {
			map.getMap().put("csm_cycle", 0);
			map.getMap().put("csm_status",1);
//			result = csservice.insertCsManage(map);
			map.getMap().put("csm_idx", result);
			result2 = installService.insertAddCsProduct(map.getMap());
			return "등록 완료";
		} catch (Exception e) {
			return "등록 실패";
		}
	
	}
	@RequestMapping(value="addcsProductApp", method=RequestMethod.POST, produces ="application/text; charset=utf8")
	public @ResponseBody Object csProductApp(ParamMap map) {
		int result = 0;
		int result2 = 0;
		String date = map.get("date") + "";
		String time = map.get("time") + "";
		String datetime = date + " " + time;
		map.getMap().put("cl_date", datetime);
		map.getMap().put("cl_eid",((HashMap) map.get("login")).get("e_id"));
		try {
			map.getMap().put("csm_sycle", 0);
			map.getMap().put("csm_status",1);
//			result = csservice.insertCsManage(map);
			map.getMap().put("csm_idx", result);
			result2 = installService.insertAddCsProduct(map.getMap());
			return "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록 실패";
		}
		
	}

	@RequestMapping(value = "productTR/{p_id}", method = RequestMethod.GET)
	public @ResponseBody Object productTR2(@PathVariable String p_id) {
		ModelAndView mav = new ModelAndView("/product/table/productTR");
		Map<String, Object> plist = productService.selectProductDetail(p_id);
		mav.addObject("plist", plist);
		return mav;
	}
	
	
	@RequestMapping(value = "installCalculate", method = RequestMethod.GET)
	public ModelAndView installCalulate() {
		ModelAndView mav = new ModelAndView("install/installCalculate");
		return mav;
	}
	/**
	 * CS점검관리 리스트
	 */
	@RequestMapping(value = "installCalculate", method = RequestMethod.POST)
	public @ResponseBody Object installCalulate(ParamMap map) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> clist = installService.selectInstallCalList(map.getMap());
		int filtered = 0;
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String m_idx = mapTemp.get("m_idx") + "";
				String mi_idx = mapTemp.get("mi_idx") + "";
				String td = "<a m_idx='" + m_idx + "' ' onclick='member()'>" + mapTemp.get("mi_name") + "</a>";
				mapTemp.put("mi_name", td);
//				mapTemp.put("m_idx", "");

				String pr_idx = mapTemp.get("pr_idx") + "";
				String td3 = "<a pr_idx='" + pr_idx + "' ' onclick='rentalProduct()'>" + mapTemp.get("pr_name")
						+ "</a>";
				mapTemp.put("pr_name", td3);
				if (mapTemp.get("m_install_finish_date").equals("") || mapTemp.get("m_install_finish_date") == null) {
					mapTemp.put("m_install_finish_date", "미설치");
				} else {
					mapTemp.put("m_install_finish_date", mapTemp.get("m_install_finish_date"));
				}
				if(mapTemp.get("jr_idx").equals("미정산")) {
					mapTemp.put("jr_state", "미정산");
				}else {
					mapTemp.put("jr_state", "정산");
				}
				
				if(mapTemp.get("cl_idx") != null) {
				String key = "<input type='hidden' name='cl_idx' value='" + mapTemp.get("cl_idx")+ "' > ";
				mapTemp.put("m_idx", key);
				}else {
					String key = "<input type='hidden' name='m_idx' value='" + m_idx+ "' > ";
					mapTemp.put("m_idx", key);
				}
				String td4 = "<a m_idx='" + m_idx + "' e_id ='"+mapTemp.get("m_eid")+"' onclick='inspect()'> 미정산 </a>";
				mapTemp.put("inspect", td4);
				// 주소
				String addr = mapTemp.get("mi_addr1") + "";
				addr += " " + mapTemp.get("mi_addr2");
				addr += " " + mapTemp.get("mi_addr3");
				mapTemp.put("mi_addr", addr);
				mapTemp.put("rental", "렌탈");
				mapTemp.put("yangdo", "양도");
			}
		}
		try {
			filtered += Integer.parseInt(map.get("start") + "");
		} catch (Exception e) {
		}
		if (clist == null) {
			clist = new ArrayList<Map<String, Object>>();
		}
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);

		// returnMap.put("recordsTotal",
		// deliveryService.selectInstallList(map.getMap()));
		return returnMap;
	}
	

	@RequestMapping(value = "adjust", method = RequestMethod.GET)
	public ModelAndView adjustment(ParamMap map, String idx) {
		ModelAndView mav = new ModelAndView("install/adjust");
		
			List<HashMap<String,Object>> list = installService.selectJungsan(idx);
			String e_name = ((HashMap)map.get("login")).get("e_name")+"";
			
			for(int i=0;i<list.size();i++) {
				Map<String,Object> mapTemp = list.get(i);
				int pr_install = Integer.parseInt(mapTemp.get("pr_install1")+"");
				int if_extra = Integer.parseInt(Common.replaceComma(mapTemp.get("if_extra")+""));
				int total= (pr_install + if_extra);
				String jr_total = Common.comma(total+"");
				
				
				mapTemp.put("install", Common.comma(pr_install+""));
				mapTemp.put("extra", Common.comma(if_extra+""));
				mapTemp.put("jr_total", jr_total);
				mapTemp.put("pr_install1", Common.comma(mapTemp.get("pr_install1")+""));
			}
			mav.addObject("list",list);
			mav.addObject("e_name",e_name);
		return mav;
	}
	
	@RequestMapping(value = "adjust", method = RequestMethod.POST)
	public @ResponseBody Object adjust(ParamMap map) {
		int result = 0;
		
		try {
			result = installService.insertJunsan(map.getMap());
			return "등록완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록실패";

		}
	}
 
	/**CS점검괸리 App*/
	@RequestMapping(value = "installCompleteApp", method = RequestMethod.GET)
	public ModelAndView installCompleteApp(ParamMap map) {
		ModelAndView mav = new ModelAndView("install/installCompleteApp");
		String e_id = ((HashMap)map.get("login")).get("e_id")+"";
		map.getMap().put("e_id", e_id);
		map.getMap().put("ds_state2",Delivery.STATE_INSTALLED);
		map.getMap().put("pr_type",ProductRental.TYPE_INSTALL);
		List<Map<String, Object>> clist = installService.selectInstallCompleteList(map.getMap());
//		List<Map<String, Object>> clist = installService.selectInstallListApp(map.getMap());
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				String m_idx = mapTemp.get("m_idx") + "";
				String mi_idx = mapTemp.get("mi_idx") + "";
				String mtd ="<a href='#customerInfo' m_idx='"+m_idx+"' onclick='customerInfo()' data-rel='popup' data-position-to='window' data-transition='pop'>"+mapTemp.get("mi_name")+"</a>";
				mapTemp.put("mi_name", mtd);
				mapTemp.put("m_idx", m_idx);
				String pr_idx = mapTemp.get("pr_idx") + "";
				String td3 = "<a pr_idx='" + pr_idx + "' ' onclick='rentalProduct()'>" + mapTemp.get("pr_name")
						+ "</a>";
				mapTemp.put("pr_name", td3);
				// 주소
				String addr = mapTemp.get("mi_addr1") + "";
				addr += " " + mapTemp.get("mi_addr2");
				addr += " " + mapTemp.get("mi_addr3");
				mapTemp.put("mi_addr", addr);
			}
		}
		mav.addObject("data", clist);
		return mav;
	
	}
	
	
	@RequestMapping(value="installApp",method=RequestMethod.GET)
	public ModelAndView installApp(ParamMap map) {
		ModelAndView mav=new ModelAndView("install/installApp");
		return mav;
	}	
	
	@RequestMapping(value="installApp",method=RequestMethod.POST)
	public ModelAndView installAppPost(ParamMap map) {
		ModelAndView mav=new ModelAndView("install/installAppContent");
		String e_id = ((HashMap)map.get("login")).get("e_id")+"";
		map.getMap().put("e_id", e_id);
	
		List<Map<String, Object>> clist =null;
		
		map.printEntrySet();
		clist = installService.selectInstallListApp(map.getMap());
		
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				String m_idx = mapTemp.get("m_idx") + "";
				String mi_idx = mapTemp.get("mi_idx") + "";
				String td = "<a m_idx='" + m_idx + "'  onclick='member()'>" + mapTemp.get("mi_name") + "</a>";
				String mtd ="<a href='#customerInfo' m_idx='"+m_idx+"'mi_idx='"+mi_idx+"' onclick='customerInfo()'>"+mapTemp.get("mi_name")+"</a>";
				mapTemp.put("mi_name", mtd);
				mapTemp.put("m_idx_t", m_idx);
				String pr_idx = mapTemp.get("pr_idx") + "";
				
				String td3 = "<a href='#productInfo' pr_idx='" + pr_idx + "' m_idx='"+m_idx+"'mi_idx='"+mi_idx+"'  onclick='productInfo()'>" + mapTemp.get("pr_name")
						+ "</a>";
				mapTemp.put("pr_name", td3);
				if (mapTemp.get("m_install_finish_date").equals("") || mapTemp.get("m_install_finish_date") == null) {
					mapTemp.put("m_install_finish_date", "미설치");
				} else {
					mapTemp.put("m_install_finish_date", mapTemp.get("m_install_finish_date"));
				}
				if (mapTemp.get("ds_state").equals(InstallState.NAME_INSTALL_INSTALL_COMPLETE)) {

					String td2 = "<a href='/file/install/" + mapTemp.get("if_idx") + "_" + mapTemp.get("if_memo") + "'>"
							+ mapTemp.get("if_file") + " </a>";
					mapTemp.put("m_tech", td2);
				} else {
					String td2 = "<a m_idx='" + m_idx + "' mi_idx='" + mi_idx
							+ "' onclick='installComplete()'> 설치확인서등록 </a>";
					mapTemp.put("m_tech", td2);
				}
				
				String td4 = "<a m_idx='" + m_idx + "' e_id ='"+mapTemp.get("m_eid")+"' onclick='inspect()'> 점검목록 </a>";
				mapTemp.put("inspect", td4);
				// 주소
				String addr = mapTemp.get("mi_addr1") + "";
				addr += " " + mapTemp.get("mi_addr2");
				addr += " " + mapTemp.get("mi_addr3");
				mapTemp.put("mi_addr", addr);
				
			
			}
		}
		mav.addObject("data", clist);
		return mav;
	}	
	
	@RequestMapping(value = "inspectApp/{m_idx}")
	public @ResponseBody Object inspectApp(ParamMap pmap, @PathVariable String m_idx) {
		ModelAndView mav = new ModelAndView("install/table/inspectTRApp");
		pmap.put("m_idx", m_idx);
		List<Map<String, Object>> clist = installService.customerCsList(pmap.getMap());
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				 
				Map<String, Object> mapTemp = clist.get(i);
				String midx = mapTemp.get("m_idx") + "";
				String td3 = "<a data-toggle='modal' cl_idx='" + mapTemp.get("cl_idx") + "' m_idx='" + midx
						+ "' onclick='select_Date()'>" + mapTemp.get("cl_date") + "</a>";
				mapTemp.put("cl_date", td3);
				mapTemp.put("m_idx", midx);
				String cstd ="<a href='#csproductinfo'  csm_idx='"+mapTemp.get("csm_idx")+"' onclick='csproductinfo()' data-rel='popup' data-position-to='window' data-transition='pop'>"+mapTemp.get("csm_name")+"</a>";
				String mtd ="<a href='#customerInfo1' m_idx='"+m_idx+"' onclick='customerInfo()' data-rel='popup' data-position-to='window' data-transition='pop'>"+mapTemp.get("mi_name")+"</a>";
				mapTemp.put("csm_name", cstd);
			}
		}
		mav.addObject("clist", clist);
		return mav;
	}
	
//	
//	// 기사정산 App
//	@RequestMapping(value = "installCalculateApp", method = RequestMethod.GET)
//	public ModelAndView installCalculateApp() {
//		ModelAndView mav = new ModelAndView("install/installCalculateApp");
	
	@RequestMapping(value = "productUl/{p_id}", method = RequestMethod.GET)
	public @ResponseBody Object productTRApp(@PathVariable String p_id) {
		ModelAndView mav = new ModelAndView("/install/table/productUlApp");
		Map<String, Object> plist = productService.selectProductDetail(p_id);
		mav.addObject("plist", plist);
		return mav;
	}
	


	
	@RequestMapping(value = "csListApp/{csm_idx}", method = RequestMethod.GET, produces = "text/plain;charset=utf8")
	public @ResponseBody Object cslistApp(ParamMap map, @PathVariable String csm_idx) {
		ModelAndView mav = new ModelAndView("install/table/csTable");
		List<HashMap<String, Object>> selectCsCompDetail = csservice.selectCsCompDetail(Integer.parseInt(csm_idx));
		for (int i = 0; i < selectCsCompDetail.size(); i++) {
			selectCsCompDetail.get(i).put("p_cost", Common.comma(selectCsCompDetail.get(i).get("p_cost") + ""));
			selectCsCompDetail.get(i).put("p_price", Common.comma(selectCsCompDetail.get(i).get("p_price") + ""));
		}
		mav.addObject("data", selectCsCompDetail);
		return mav;
	}
	
	/**
	 * 렌탈상품등록 중 상품 불러오기
	 * */
	@RequestMapping(value = "productList")
	public @ResponseBody Object productListApp(ParamMap pmap) {
		ModelAndView mav = new ModelAndView("install/table/csproductUlApp");
		List<Map<String, Object>> plist = productService.selectProductListApp(pmap.getMap());
		if (plist != null && plist.size() != 0) {
			for (int i = 0; i < plist.size(); i++) {
				Map<String, Object> pMap = plist.get(i);
				String p_name = "<a idx='" + pMap.get("p_id") + "' onclick='choose(this)'>" + pMap.get("p_name")
						+ "</a>";
				pMap.put("p_name", p_name);
			}
		}
//		returnMap.put("data", plist);
//		returnMap.put("recoresFiltered", filtered);
//		returnMap.put("recordsTotal", productService.countProduct());
		mav.addObject("data",plist);
		return mav;
	}
	
	
	@RequestMapping(value = "inspectTRApp", method = RequestMethod.GET, produces = "text/plain;charset=utf8")
	public @ResponseBody Object inspectTRApp(ParamMap pmap) {
		ModelAndView mav = new ModelAndView("install/table/inspectTRApp");
		pmap.put("m_idx", pmap.get("m_idx"));
		pmap.put("cl_idx", pmap.get("cl_idx"));

		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> clist = installService.customerCsList(pmap.getMap());
		int filtered = 0;
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				String midx = mapTemp.get("m_idx") + "";
				String miidx = mapTemp.get("mi_idx") + "";
				String td = "<a data-toggle='modal' href='#myModal4'  m_idx='" + midx
						+ "' onclick='install_member()' cl_idx='" + mapTemp.get("cl_idx") + "'>"
						+ mapTemp.get("mi_name") + "</a>";
				mapTemp.put("mi_name", td);
				mapTemp.put("m_idx", "");
				// 주소
				String addr = mapTemp.get("mi_addr1") + "";
				addr += " " + mapTemp.get("mi_addr2");
				addr += " " + mapTemp.get("mi_addr3");
				mapTemp.put("mi_addr", addr);
				

				String td6 = "<a data-toggle='modal' href='#myModal5' csm_idx='" + mapTemp.get("csm_idx")
						+ "' onclick='cslist()' >" + mapTemp.get("csm_name") + " </a>";
				mapTemp.put("csm_name", td6);
			
			}
		}

		mav.addObject("clist", clist);
		return mav;
	}
	
	// 기사정산 App
	@RequestMapping(value = "installCalculateApp", method = RequestMethod.GET)
	public ModelAndView installCalulateApp() {
		ModelAndView mav = new ModelAndView("install/installCalculateApp");
		return mav;
	}

	/**
	 * 방문예정일 변경
	 * */
	@RequestMapping(value = "installdateChange", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object installdateChange(ParamMap map) {
		String msg = "";
		String date = map.get("date") + "";
		String time = map.get("time") + "";
		int result =0;
		HashMap<String, Object> rmap = new HashMap<String, Object>();
		String datetime = date + " " + time;
		rmap.put("dateTime", datetime);
		rmap.put("m_idx", map.getMap().get("m_idx")+"");
		result = memberService.updateinstallDate(rmap);
		if(result>0) {
			 msg = "등록완료";
		 }else {
			msg = "등록실패";
		}
		return msg;
	}
	
	
	
	/**방문 후 설치 취소 요청 */
	@RequestMapping(value = "cancel", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object cancel(ParamMap map) {
		int m_idx=Integer.parseInt(map.get("m_idx")+"");
		String msg=deliveryService.cancelInstallReq(m_idx);
		return msg;
	}
	
	@RequestMapping("contractAppInstallFileInfo/{m_idx}")
	public ModelAndView contractAppInstallFileInfo(@PathVariable String m_idx) {
		ModelAndView mav=new ModelAndView("install/table/contractAppInstallFileInfo");
		List<Map<String,Object>> ifList=installService.selectinstallFileList(Integer.parseInt(m_idx));
		if(ifList!=null&&ifList.size()>0) {
			
			mav.addObject("ifMap",ifList.get(0));
			if(ifList.get(0).get("mf_file").toString().contains(".")==true) {
				String [] ext = ifList.get(0).get("mf_file").toString().split(".");
				for(int i=0;i<ext.length;i++) {
					System.out.println(ext[i]);
				}
				
			}
			List<Map<String,Object>> dsList=deliveryService.selectDeliveryState(Integer.parseInt(m_idx));
			if(dsList!=null&&dsList.size()>0) {
				mav.addObject("ds_date",dsList.get(0).get("ds_date"));
			}
		}
		return mav;
	}
	//기사 미배정 버튼
	@RequestMapping(value="matchTechnician/{m_idx}",method=RequestMethod.DELETE, produces ="text/plain;charset=utf8")
	public @ResponseBody Object matchCancel(@PathVariable String m_idx) {
		String msg="";
		int midx=Integer.parseInt(m_idx);
		msg = installService.matchCancel(midx);
		return msg;
	}
	
	//무료체험
	@RequestMapping(value="changeTestFree",method=RequestMethod.POST, produces ="text/plain;charset=utf8")
	public @ResponseBody Object matchCancel(ParamMap pmap) {
		String msg=memberService.changeTestFree(pmap.getMap());
		return msg;
	}
	
}
