package rental.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



import rental.common.Log;
import rental.common.ParamMap;
import rental.service.EmployeeService;

@Controller
public class MainController {
	@Autowired
	EmployeeService employeeService;
	
	@RequestMapping("index")
	public String index() {
		return "main/index";
	}
	@RequestMapping("index2")
	public String index2() {
		return "main/index2";
	}
	@RequestMapping("main")
	public String main(ParamMap pmap) {
		return "main/main";
	}
	
	/**url에 .이 포함된 경우 이동하는 페이지*/
	@RequestMapping("wrongUrl")
	public String wrongUrl() {
		return "error/wrongUrl";
	}
	
	@RequestMapping(value="login",produces="text/plain;charset=utf8")
	public ModelAndView login(ParamMap pmap,HttpSession ses) {
		ModelAndView mav=new ModelAndView("main/msgLoc");
		Map<String, Object> emap=employeeService.login(pmap.getMap());
		if(emap==null||!emap.containsKey("logincheck")) {
			mav.addObject("msg","존재하지 않는 아이디입니다.");
			mav.addObject("loc","index");
		}else if(emap.get("logincheck").equals("no")) {
			mav.addObject("msg","잘못된 비밀번호 입니다.");
			mav.addObject("loc","index");
		}else if(emap.get("logincheck").equals("yes")) {
			emap.remove("e_pass");
			ses.setAttribute("login", emap);
			mav.setViewName("redirect:/customer/sangdam");
		}
		//세션 튕김 후 로그인 창으로 로그인 시도 시
		String loc=(String)pmap.get("loc");
		if(loc!=null&&!loc.equals("")&&!loc.equals("/")) {
			mav.setViewName("redirect:"+loc);
		}
		return mav;
	}
	//고객사 로그인
	@RequestMapping(value="loginCustomer",produces="text/plain;charset=utf8")
	public ModelAndView loginCustomer(String e_logid, String e_pass,HttpSession ses) {
		ModelAndView mav=new ModelAndView("main/msgLoc");
		Map<String, Object> emap=employeeService.customerLogin(e_logid);
		if(emap==null) {
			mav.addObject("msg","등록된 아이디가 아닙니다.");
			mav.addObject("loc","index2");
		}else{
			if(emap.get("c_logpw").equals(e_pass)) {
				emap.put("auth", "");
				emap.put("e_name", emap.get("c_comname"));
				emap.remove("c_logpw");
				ses.setAttribute("login", emap);
				mav.setViewName("redirect:/delivery/carry");
			}else {
				mav.addObject("msg","잘못된 비밀번호 입니다.");
				mav.addObject("loc","index2");
			}
		}
		return mav;
	}
	
	
	// 로그인 App
	@RequestMapping(value = "indexApp")
	public String indexApp() {
		return "main/indexApp";
	}
	
	
	@RequestMapping(value="Applogin", produces="text/plain;charset=utf8")
	public ModelAndView Applogin(ParamMap pmap,HttpSession ses) {
		ModelAndView mav=new ModelAndView("main/msgLoc");
		Map<String, Object> emap=employeeService.login(pmap.getMap());
		if(emap==null) {
			mav.addObject("msg","등록된 아이디가 아닙니다.");
			mav.addObject("loc","indexApp");
		}else if(emap.get("logincheck").equals("yes")) {
			emap.remove("e_pass");
			ses.setAttribute("login", emap);
			mav.setViewName("redirect:/install/installApp");
		}else {
				mav.addObject("msg","잘못된 비밀번호 입니다.");
				mav.addObject("loc","indexApp");
			}
		
		return mav;
	}
	
	
	@RequestMapping(value="getToken")
    public String getToken(Model model, HttpServletRequest request, HttpSession session,ParamMap map)throws Exception{
            
                System.out.println("앱에서 들어옴");
                final String apiKey = "AAAAAbE7mpo:APA91bEYJ32bPICopywqTOdxqf61rm8ihnWaSwfH0ixqkuhKV-9na-b0ppg6DSa9h8V9qO8PrvE5PozR8RQXjO4on9tcnqkDEl76EwFXdIKJ-NErBInLfbjC-sqqGlSZNGCHq9NbjAe9";
                URL url = new URL("https://fcm.googleapis.com/fcm/send");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", "key=" + apiKey);
 
                conn.setDoOutput(true);
                
                String userId =(String) request.getSession().getAttribute("ssUserId");
 
                // 이렇게 보내면 주제를 ALL로 지정해놓은 모든 사람들한테 알림을 날려준다.
//                String input = "{\"notification\" : {\"title\" : \"여기다 제목 넣기 \", \"body\" : \"여기다 내용 넣기\"}, \"to\":\"/topics/ALL\"}";
                
                // 이걸로 보내면 특정 토큰을 가지고있는 어플에만 알림을 날려준다  위에 둘중에 한개 골라서 날려주자
                String input = "{\"notification\" : {\"title\" : \" 여기다 제목넣기 \", \"body\" : \"여기다 내용 넣기\"}, \"to\":\" 여기가 받을 사람 토큰  \"}";
 
                OutputStream os = conn.getOutputStream();
                
                // 서버에서 날려서 한글 깨지는 사람은 아래처럼  UTF-8로 인코딩해서 날려주자
                os.write(input.getBytes("UTF-8"));
                os.flush();
                os.close();
 
                int responseCode = conn.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + input);
                System.out.println("Response Code : " + responseCode);
                
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
 
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                // print result
                System.out.println(response.toString());
                
 
        return "jsonView";
    }
	
	
	
	
	@RequestMapping(value="fcm")
    public String message(Model model, HttpServletRequest request, HttpSession session,ParamMap map)throws Exception{
            
                System.out.println("앱에서 들어옴");
                final String apiKey = "AAAAAbE7mpo:APA91bEYJ32bPICopywqTOdxqf61rm8ihnWaSwfH0ixqkuhKV-9na-b0ppg6DSa9h8V9qO8PrvE5PozR8RQXjO4on9tcnqkDEl76EwFXdIKJ-NErBInLfbjC-sqqGlSZNGCHq9NbjAe9";
                URL url = new URL("https://fcm.googleapis.com/fcm/send");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", "key=" + apiKey);
 
                conn.setDoOutput(true);
                
                String userId =(String) request.getSession().getAttribute("ssUserId");
 
                // 이렇게 보내면 주제를 ALL로 지정해놓은 모든 사람들한테 알림을 날려준다.
//                String input = "{\"notification\" : {\"title\" : \"여기다 제목 넣기 \", \"body\" : \"여기다 내용 넣기\"}, \"to\":\"/topics/ALL\"}";
                
                // 이걸로 보내면 특정 토큰을 가지고있는 어플에만 알림을 날려준다  위에 둘중에 한개 골라서 날려주자
                String input = "{\"notification\" : {\"title\" : \" 여기다 제목넣기 \", \"body\" : \"여기다 내용 넣기\"}, \"to\":\" 여기가 받을 사람 토큰  \"}";
 
                OutputStream os = conn.getOutputStream();
                
                // 서버에서 날려서 한글 깨지는 사람은 아래처럼  UTF-8로 인코딩해서 날려주자
                os.write(input.getBytes("UTF-8"));
                os.flush();
                os.close();
 
                int responseCode = conn.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + input);
                System.out.println("Response Code : " + responseCode);
                
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
 
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                // print result
                System.out.println(response.toString());
                
 
        return "jsonView";
    }
}
