package rental.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Common;
import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.MemberInfo;
import rental.constant.ProductRental;
import rental.service.ProductService;
import rental.service.CsService;
import rental.service.EmployeeService;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.*;

@Controller
@RequestMapping("cs/")


public class CsController {
	@Autowired
	CsService csservice;
	@Autowired
	ProductService productService;
	
	/**
	 * CS관리
	 * */
	@RequestMapping(value="csManage", method= RequestMethod.GET)
	public ModelAndView csManage() {
		ModelAndView mav = new ModelAndView("/cs/csManage");
		return mav;
	}
	
	
	@RequestMapping(value="csManage", method= RequestMethod.POST)
	public @ResponseBody Object csManage(ParamMap map) {
		ModelAndView mav = new ModelAndView("/cs/csManage");
		return mav;
	}
	
	
	/**
	 * CS항목관리
	 * */
	@RequestMapping(value="csCompManage", method= RequestMethod.GET)
	public @ResponseBody Object csCompManage() {
		ModelAndView mav = new ModelAndView("/cs/csCompManage");
		return mav;
	}
	/**
	 * CS항목조회
	 * */
	@RequestMapping(value = "csCompManage", method = RequestMethod.POST)
	public @ResponseBody Object  csCompManage(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		List<Map<String,Object>> cslist=csservice.selectCsList(pmap.getMap());
		int filtered=0;
		if(cslist!=null&&cslist.size()!=0) {
			for(int i=0;i<cslist.size();i++) {
				Map<String,Object> mapTemp=cslist.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				String csm_idx=mapTemp.get("csm_idx")+"";
				String td="<a csm_idx='"+csm_idx+"' onclick='csProduct()'>"+mapTemp.get("csm_name")+"</a>";
				mapTemp.put("csm_name", td);
				mapTemp.put("csm_idx", "");
				if(mapTemp.get("csm_status").equals(1)) {
					mapTemp.put("csm_status", "사용");
				}else {
					mapTemp.put("csm_status", "미사용");
				}
			
			}
		}
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", cslist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", csservice.countrentalProductList(pmap.getMap()));
		return returnMap;
	}
	/***************************************************************************/
	/**
	 * CS항목 신규 등록화면
	 * */
	@RequestMapping(value="csProduct", method=RequestMethod.GET)
	public ModelAndView csProduct(ParamMap map) {
		ModelAndView mav=new ModelAndView("/cs/csProduct");
		List<Map<String, Object>> mMap = productService.selectrentalList(map.getMap());
		mav.addObject("rp",mMap);
		return mav;
	}
	/**
	 * CS항목 신규 등록
	 * */
	@RequestMapping(value="csProduct", method=RequestMethod.POST, produces ="application/text; charset=utf8")
	public @ResponseBody Object csProduct(ParamMap map,MultipartFile csm_file) {
		System.out.println("dd");
		int result = 0;
		try {
			if(csm_file == null || csm_file.equals("")) {
				map.put("csm_file",	"");
			}else {
				map.put("csm_file", csm_file.getOriginalFilename());
			}
			result = csservice.insertCsManage(map,csm_file);
			return "등록 완료";
		} catch (Exception e) {
			System.out.println(e);
			return "등록 실패";
		}
	
	}
	/**
	 * CS항목관리 상세보기 화면
	 * */
	@RequestMapping(value="csCompDetail",method=RequestMethod.GET)
	public ModelAndView csCompDetail() {
		ModelAndView mav=new ModelAndView("cs/csCompDetail");
		return mav;
	}
	
	/**
	 * CS항목관리 상세보기 csm_idx값으로 해당항목 보여주기
	 * */
	@RequestMapping(value="csCompDetail/{csm_idx}",method=RequestMethod.GET)
	public ModelAndView csCompDetail(@PathVariable String csm_idx,ParamMap map) {
		ModelAndView mav=new ModelAndView("cs/csCompDetail");
		int csm=Integer.parseInt(csm_idx);
		Map<String,Object> csmap = csservice.selectCsManageDetail(csm);
		List<Map<String, Object>> mMap = productService.selectrentalList(map.getMap());
		mav.addObject("cslist" ,csmap );
		List<HashMap<String,Object>> selectCsCompDetail = csservice.selectCsCompDetail(csm);
		for(int i=0;i<selectCsCompDetail.size();i++) {
			selectCsCompDetail.get(i).put("p_cost", Common.comma(selectCsCompDetail.get(i).get("p_cost")+""));
			selectCsCompDetail.get(i).put("p_price", Common.comma(selectCsCompDetail.get(i).get("p_price")+""));
		}
		
		mav.addObject("rp",mMap);
		mav.addObject("data",selectCsCompDetail);
		mav.addObject("writer",map.getMap().get("login"));
		return mav;
	}

	/**
	 * CS 항목 수정
	 * */
	@RequestMapping(value="csCompDetail/CsModify", method= RequestMethod.POST, produces = "application/text; charset=utf8")

	public @ResponseBody Object  productRentalModify(ParamMap map,MultipartFile csm_file) {
		try {
			if(csm_file == null || csm_file.equals("")) {
				map.put("csm_file",	"");
			}else {
				map.put("csm_file", csm_file.getOriginalFilename());
			}
			csservice.updateCs(map.getMap(),csm_file);
			return "등록 완료";
		} catch (Exception e) {
			System.out.println(e);
			return "등록 실패";
					
		}
	}
	
	
	/**************************************************************************/
	/**
	 * 렌탈상품 신규등록 -> CS항목 추가 (ajax로 해당CS정보 받아와서 데이터테이블로 추가)
	 * */
	@RequestMapping(value="csRentalManage/productTR/{csm_id}", method= RequestMethod.GET)
	public @ResponseBody Object productTR2(@PathVariable String csm_id) {
		ModelAndView mav = new ModelAndView("/cs/table/productTR");
		
		Map<String, Object> cslist = csservice.selectCsDetail(csm_id);
		String del = "<a csm_id='"+csm_id+"' onclick='del()'>삭제</a>";
		cslist.put("del", del);
		mav.addObject("cslist",cslist);
		
		return mav;
	}
	
	
	/**
	 * 렌탈상품에서 CS항목 상세보기 
	 * */
	@RequestMapping(value="csRentalManage", method= RequestMethod.GET)
	public @ResponseBody Object csmanage(ParamMap map) {
		ModelAndView mav = new ModelAndView("/cs/csRentalManage");
		mav.addObject("writer",map.getMap().get("login"));
		return mav;
	}

	/**
	 * 렌탈상품에서 CS항목 상세보기 
	 * */
	@RequestMapping(value="csRentalManage/{pr_idx}", method= RequestMethod.GET)
	public @ResponseBody Object csmanage(ParamMap map, @PathVariable String pr_idx) {
		ModelAndView mav = new ModelAndView("/cs/csRentalManage");
		Map<String,Object> rentalInfo = csservice.selectRentalInfo(pr_idx);
		mav.addObject("rentalInfo",rentalInfo);
		return mav;
	}

	/**
	 * 렌탈상품CS 항목 등록 시 CS항목 리스트 추가 화면 보여주기
	 * */
	@RequestMapping(value = "csList")
	public @ResponseBody Object productList(ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> cslist = csservice.selectCsList(pmap.getMap());
		int filtered = 0;
		if (cslist != null && cslist.size() != 0) {
			for (int i = 0; i < cslist.size(); i++) {

				Map<String, Object> pMap = cslist.get(i);
				int rownum = Integer.parseInt(pMap.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String csm_name = "<a cs idx='" + pMap.get("csm_idx") + "' onclick='choose(this)'>" + pMap.get("csm_name") + "</a>";
				pMap.put("csm_name", csm_name);
			}
		}

		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		
		returnMap.put("data", cslist);
		returnMap.put("recoresFiltered", filtered);
		returnMap.put("recordsTotal", productService.countProduct(pmap.getMap()));
		return returnMap;
	}
	/**
	 * 렌탈상품 중 CS매치가 안된 상품들 등록화면
	 * */
	@RequestMapping(value="csRentalManage/csMatch", method= RequestMethod.POST, produces = "application/text; charset=utf8")

	public @ResponseBody Object  csMatch(ParamMap map) throws IOException {
		int result = 0; 
		try {
			result = csservice.insertCsMatch(map.getMap());
			return "등록 완료";
		} catch (Exception e) {
			System.out.println(e);
			return "등록 실패";
					
		}
	}
	
	
	/**
	 * 렌탈상품중 CS매칭이 된 상품의 상세보기
	 * */
	@RequestMapping(value="csMatchDetail/{pr_idx}", method= RequestMethod.GET)

	public ModelAndView  csMatchDetail(@PathVariable String pr_idx) {
		ModelAndView mav = new ModelAndView("cs/csMatchDetail");
		Map<String,Object> rentalInfo = csservice.selectRentalInfo(pr_idx);
		List<HashMap<String,Object>> csList = csservice.selectCsMatch(pr_idx);
		for(int i=0; i<csList.size();i++) {
			Map<String, Object> temp = csList.get(i);
			if(temp.get("csm_status").equals(1)) {
				temp.put("csm_status", "사용");
			}
			else{
				temp.put("csm_status", "미사용");
			}
		}
		mav.addObject("cslist",csList);
		mav.addObject("rentalInfo",rentalInfo);
		
		return mav;
	}
}
