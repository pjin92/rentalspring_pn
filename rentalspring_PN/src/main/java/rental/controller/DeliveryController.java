package rental.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Common;
import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.InstallState;
import rental.constant.MemberFile;
import rental.constant.MemberInfo;
import rental.constant.ProductRental;
import rental.service.CmsService;
import rental.service.DeliveryService;
import rental.service.MemberService;
import rental.service.EmployeeService;

@Controller
@RequestMapping("delivery/")
public class DeliveryController {

	@Autowired
	DeliveryService deliveryService;
	@Autowired
	MemberService memberService;
	@Autowired
	CmsService cmsService;
	@Autowired
	EmployeeService employeeService;
	
	/**배송 상태 변경, key:value,url:m_idx*/
	@RequestMapping(value="{ds_idx}",method=RequestMethod.PUT,produces="text/plain;charset=utf8")
	public @ResponseBody Object saveDeliveryState(@PathVariable String ds_idx,ParamMap pmap) {
		String msg="";
		try{
			//member.m_install_finish_date 설치완료일 에도 입력
			String ds_state=pmap.get("ds_state")+"";//delivery 저장,cancel 구매취소
			
			int dsidx=Integer.parseInt(ds_idx);
			int m_idx=0;
			try{
				String midx=pmap.get("url")+"";
				midx=midx.replaceAll("[^0-9]", "");
				m_idx=Integer.parseInt(midx);
				pmap.put("m_idx",m_idx);
			}catch (Exception e) {
				return "잘못된 경로로 접속하였습니다.";
			}

			if(ds_state.equals("cancel")) {
				//구매취소
				msg=deliveryService.cancelBuy(dsidx,m_idx);
			}else if(ds_state.equals("cancelcancel")){
				//구매취소 취소
				msg=deliveryService.cancelBuyCancel(dsidx,m_idx);
			}else{
				//배송 정보 저장
				msg=deliveryService.delivery(dsidx,pmap.getMap());
				
				String m_process=memberService.selectMemberDetail(m_idx).get("m_process")+"";
				if(m_process.equals(MemberInfo.PROCESS_CANCEL_BUY)||m_process.equals(MemberInfo.PROCESS_EXIT)
						||m_process.equals(MemberInfo.PROCESS_HAEBAN)||m_process.equals(MemberInfo.PROCESS_RETURN)) {
					
					if(cmsService.deleteCms(m_idx)>0) {
						msg+="\nCMS 삭제하였습니다.";
					}
				}
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			msg="오류 발생";
		}
		return msg;
	}
	
	@RequestMapping(value="install", method=RequestMethod.GET)
	public ModelAndView installList () {
		ModelAndView mav = new ModelAndView("delivery/install");
		return mav;
	} 
	
	//설치관리 조회dt
	@RequestMapping(value="install", method=RequestMethod.POST)
	public @ResponseBody Object install(ParamMap map){
		//검색조건
		String ds_state=(String)map.get("ds_state");
		if(ds_state==null||ds_state.equals("")) {
			map.remove("ds_state");
		}
		
		//날짜 range로 검색
		String m_sangdam_finish=(String)map.get("m_sangdam_finish");
		if(m_sangdam_finish==null||!m_sangdam_finish.contains("~")) {
			map.remove("m_sangdam_finish");
		}else {
			String m_sangdam_finishs[]=m_sangdam_finish.split("~");
			map.put("m_sangdam_finish1", m_sangdam_finishs[0].trim());
			map.put("m_sangdam_finish2", m_sangdam_finishs[1].trim());
		}
		
		String ds_req=(String)map.get("ds_req");
		if(ds_req==null||!ds_req.contains("~")) {
			map.remove("ds_req");
		}else {
			String ds_reqs[]=ds_req.split("~");
			map.put("ds_req1", ds_reqs[0].trim());
			map.put("ds_req2", ds_reqs[1].trim());
		}
		
		String ds_date=(String)map.get("ds_date");
		if(ds_date==null||!ds_date.contains("~")) {
			map.remove("ds_date");
		}else {
			String ds_dates[]=ds_date.split("~");
			map.put("ds_date1", ds_dates[0].trim());
			map.put("ds_date2", ds_dates[1].trim());
		}
		
		Map<String, Object> returnMap=new HashMap<String, Object>();
		List<Map<String,Object>> clist=deliveryService.selectInstallList(map.getMap());
		int filtered=0;
		if(clist!=null&&clist.size()!=0) {
			for(int i=0;i<clist.size();i++) {
				Map<String,Object> mapTemp=clist.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				String m_idx=mapTemp.get("m_idx")+"";
				String mi_idx=mapTemp.get("mi_idx")+"";
				String td="<a m_idx='"+m_idx+"' ' onclick='member()'>"+mapTemp.get("mi_name")+"</a>";
				mapTemp.put("mi_name", td);
				
				String i_e_name=(String)mapTemp.get("i_e_name");
				if(i_e_name==null)i_e_name="배정";
				String td2="<a m_idx='"+m_idx+"' mi_idx='"+mi_idx+"' onclick='technician()'>"+i_e_name+"</a>";
				mapTemp.put("i_e_name" ,td2);
					
				String pr_code = mapTemp.get("pr_rentalcode")+"";
				String td3 = "<a pr_rentalcode='"+pr_code+"' ' onclick='rentalProduct()'>"+mapTemp.get("pr_name")+"</a>"; 
				mapTemp.put("pr_name", td3);
				
				String mf_file;
				if (mapTemp.get("mf_idx")!=null) {
					mf_file=mapTemp.get("mf_file")+"";
				} else {
					mf_file="설치확인서등록";
				}
				String td4 = "<a m_idx='" + m_idx + "' mi_idx='"+ mi_idx+"' onclick='installComplete()'>"+mf_file+"</a>";
				mapTemp.put("mf_file", td4);
			}
		}
		
		try {
			filtered+=Integer.parseInt(map.get("start")+"");
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(clist==null) {
			clist=new ArrayList<Map<String,Object>>();
		}
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", deliveryService.countInstallList(map.getMap()));
		return returnMap;
	}
	
	

	@RequestMapping(value="matchTechnician", method=RequestMethod.GET)
	public ModelAndView matchTechnicianshow (ParamMap map) {
		ModelAndView mav = new ModelAndView("delivery/matchTechnician");
		//System.out.println(map.entrySet());
		int m_idx = Integer.parseInt(map.getMap().get("m_idx").toString());
		int mi_idx = Integer.parseInt(map.getMap().get("mi_idx").toString());
		
		
		String mi_addr1 = memberService.getMi_addr1(mi_idx);
		
		List<Map<String, Object>> clist = new ArrayList<Map<String,Object>>();
		List<Map<String, Object>> cslist = new ArrayList<Map<String,Object>>();
		List<Map<String, Object>> tlist = employeeService.selecttechnicianList(map.getMap());
		for(int i=0;i<tlist.size();i++) {
			Map<String, Object> mapTemp =tlist.get(i);
			
			mapTemp.put("t_name", mapTemp.get("e_name"));
			String td = "<a e_id='" + mapTemp.get("e_id")+ "' e_name='"+mapTemp.get("e_name")+ "' onclick='addMatchTechnician()'>"+mapTemp.get("e_name")+"</a>";
			String td2 = "<a e_id='" + mapTemp.get("e_id")+ "' onclick='showTechStock()'>재고보기</a>";
			mapTemp.put("e_name", td);
			mapTemp.put("stock", td2);
			mapTemp.put("m_idx", m_idx);
			mapTemp.put("mi_idx", mi_idx);
			if(mapTemp.get("e_activity_loc").toString().contains(mi_addr1)) {
				clist.add(mapTemp);
			}else {
				cslist.add(mapTemp);
			}
		}
		mav.addObject("clist",clist);
		mav.addObject("tlist",cslist);
		return mav;
	}
	@RequestMapping(value="matchTechnician", method=RequestMethod.POST,produces = "application/text; charset=utf8")
	public @ResponseBody Object matchTechnician (ParamMap map) {
		int result = deliveryService.insertMatchTechnician(map.getMap());
		
		 if(result >0) {
		 	return "등록 완료";
		}else {
			return "등록 실패";
		}
	}
	
	//배송관리
	@RequestMapping(value="carry", method=RequestMethod.GET)
	public ModelAndView carry() {
		ModelAndView mav = new ModelAndView("delivery/carry");
		return mav;
	}
	//배송관리 dt
	@RequestMapping(value="carry", method=RequestMethod.POST)
	public @ResponseBody Object carryPost(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		
		String ds_state=(String)pmap.get("ds_state");
		if(ds_state==null||ds_state.equals("")) {
			pmap.remove("ds_state");
		}
		
		//날짜 range로 검색
		String ds_req=(String)pmap.get("ds_req");
		if(ds_req==null||!ds_req.contains("~")) {
			System.out.println(ds_req);
			pmap.remove("ds_req");
		}else {
			String ds_reqs[]=ds_req.split("~");
			pmap.put("ds_req1", ds_reqs[0].trim());
			pmap.put("ds_req2", ds_reqs[1].trim());
		}
		String ds_date=(String)pmap.get("ds_date");
		if(ds_date==null||!ds_date.contains("~")) {
			System.out.println(ds_date);
			pmap.remove("ds_date");
		}else {
			String ds_dates[]=ds_date.split("~");
			pmap.put("dsdate1", ds_dates[0].trim());
			pmap.put("dsdate2", ds_dates[1].trim());
		}
		
		List<Map<String,Object>> datas=deliveryService.selectCarryList(pmap.getMap());
		int filtered=0;
		if(datas!=null&&datas.size()!=0&&!(pmap.get("length")+"").equals("-1")) {
			for(int i=0;i<datas.size();i++) {
				Map<String,Object> mapTemp=datas.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
			}
		}
		
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		if(datas==null) {
			datas=new ArrayList<Map<String,Object>>();
		}
		returnMap.put("data", datas);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", deliveryService.countCarryList(pmap.getMap()));
		return returnMap;
	}
	//배송관리 엑셀 업로드
	@RequestMapping(value="carry/excel", method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object carryExcel(MultipartFile deliveryFile) {
		String msg="";
		try {
			File dir=new File(Delivery.PATH);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			Calendar now=Calendar.getInstance();
			Long l_now=now.getTime().getTime();
			File saved=new File(Delivery.PATH+l_now+"_"+deliveryFile.getOriginalFilename());
			deliveryFile.transferTo(saved);
			msg="파일업로드";
			msg=deliveryService.carryExcel(saved);
		}catch (Exception e) {
			if(msg.equals("")) {
				msg="파일 업로드 중 오류 발생.";
			}else {
				msg="데이터 업로드 중 오류 발생.";
			}
			Log.warn("송장관리 엑셀업로드."+msg+e.toString());
		}
		return msg;
	}
	
}
