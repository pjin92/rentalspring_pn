package rental.controller;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import rental.common.AesEncrypt;
import rental.common.CardHV;
import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.MemberInfo;
import rental.service.CmsService;
import rental.service.CommonService;
import rental.service.DeliveryService;
import rental.service.EmployeeService;
import rental.service.MemberService;
import rental.service.PayService;
import rental.service.ProductService;
import rental.service.VaccountService;

@Controller
@RequestMapping("tera/")
public class TeraController {
	
	@Autowired
	CommonService commonService;
	@Autowired
	DeliveryService deliveryService;
	@Autowired
	MemberService memberService;
	@Autowired
	PayService payService;
	@Autowired
	ProductService productService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	VaccountService vaccountService;
	@Autowired
	CmsService cmsService;
	
	
	//테라에서 정보 받아옴
	@RequestMapping(value="regi",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object regi(ParamMap hm,HttpServletResponse response) {
		//gn이 아닌 cj등은 설치까지만. 결제 x
		String msg="";
		
		hm.remove("login");
		Log.warn("tera에서 전송받은 데이터."+hm.entrySet());
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		String tera=hm.get("tera")+"";
		if(tera==null||!tera.trim().equals("tera")){
			return "잘못된 접근입니다.";
		}
		//상태값. 테라에서 괄호를 못보낸다고함
		String m_state=hm.get("m_state")+"";
		if(m_state.equals("취소_가망")){
			hm.put("m_state", "취소(가망)");
		}else if(m_state.equals("선설치_렌탈전환")) {
			hm.put("m_state", "선설치(렌탈전환)");
		}
		
		String m_gubun=hm.get("m_gubun")+"";
		if(m_gubun.equals("자체_렌탈")||m_gubun.equals("렌탈_자체")){
			hm.put("m_gubun", "렌탈(자체)");
		}
		
		//오타..ms_intall_date1 ->ms_install_date1
		String ms_intall_date1=hm.get("ms_intall_date1")+"";
		if(ms_intall_date1!=null){
			hm.put("ms_install_date1", dateString(ms_intall_date1));
		}
		
		String ms_final_state_date=hm.get("ms_final_state_date")+"";
		hm.put("ms_final_state_date", dateString(ms_final_state_date));

		//결제시작일 ,종료일
		String ms_transfer_date3=hm.get("ms_transfer_date3")+"";
		hm.put("ms_transfer_date3", dateString(ms_transfer_date3));
		String ms_transfer_date2=hm.get("ms_transfer_date2")+"";
		hm.put("ms_transfer_date2", dateString(ms_transfer_date2));
		
		hm.put("m_sangdam_date_finish", dateString(hm.get("m_sangdam_date_finish")+""));
//		hm.put("ms_final_state_date", hm.get("ms_final_state_date")+"");
//		hm.put("ms_contract_date", hm.get("ms_contract_date")+"");
//		hm.put("ms_install_date1", (hm.get("ms_install_date1")+""));
//		hm.put("ms_transfer_date3", dateString(hm.get("ms_transfer_date3")+""));
		
		try{
			String card=hm.get("ms_card")+"";
			if(card!=null&&!card.trim().equals("")){
				String cardName="";
				int card_i=Integer.parseInt(card);
				switch(card_i){
				case 1:cardName="";break;
				case 2:cardName="현대카드";break;
				case 3:cardName="BC카드";break;
				case 4:cardName="국민카드";break;
				case 5:cardName="삼성카드";break;
				case 6:cardName="신한카드";break;
				case 7:cardName="롯데카드";break;
				case 8:cardName="하나카드";break;
				case 9:cardName="씨티카드";break;
				case 10:cardName="우리카드";break;
				case 11:cardName="광주카드";break;
				case 12:cardName="전북카드";break;
				case 13:cardName="수협카드";break;
				case 14:cardName="산은카드";break;
				case 15:cardName="NH농협카드";break;
				case 16:cardName="기업카드";break;
				case 17:cardName="농협BC카드";break;
				case 18:cardName="농협카드";break;
				case 19:cardName="외환카드";break;
				case 20:cardName="외환카드(하나)";break;
				case 21:cardName="제일은행비자카드";break;
				case 22:cardName="중소기업카드";break;
				}
				hm.put("ms_card", cardName);
			}
			
			//전화번호
			String m_u_phone=(String)hm.get("m_u_phone");
			String ms_order_phone1=(String)hm.get("ms_order_phone1");
			String ms_receive_phone1=(String)hm.get("ms_receive_phone1");
			String ms_receive_phone2=(String)hm.get("ms_receive_phone2");
			
			hm.put("m_u_phone", m_u_phone.replaceAll("[^0-9]", ""));
			hm.put("ms_order_phone1", ms_order_phone1.replaceAll("[^0-9]", ""));
			hm.put("ms_receive_phone1", ms_receive_phone1.replaceAll("[^0-9]", ""));
			hm.put("ms_receive_phone2", ms_receive_phone2.replaceAll("[^0-9]", ""));

			String m_prentmoneyTemp=(String)hm.get("m_prentmoney");
			if(m_prentmoneyTemp!=null){
				m_prentmoneyTemp=m_prentmoneyTemp.replaceAll("[^0-9]", "");
				hm.put("m_prentmoney", m_prentmoneyTemp);
			}
			
			if(memberService.selectMemberDetail(hm.get("m_u_number")+"")!=null) {
				return "exists";
			}
			
			//transaction 데이터 insert
			msg=memberService.receiveData(hm.getMap());
			
		}catch(Exception e){
			e.printStackTrace();
			Log.warn("tera 에러 발생"+e.toString());
			msg="error";
		}
		return msg;
	}
	
	private String dateString(String date) {
		try {
			if(date==null) {
				Log.warn("date null 값");
				return "";
			}
			date=date.replaceAll("[^0-9]", "");
			if(date.startsWith("000")) {
				Log.warn("date 값 이상. 빈값 세팅"+date);
				return "";
			}
			date=date.substring(0, 8);
			SimpleDateFormat sdfNum=new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			date=sdf.format(sdfNum.parse(date));
		}catch (Exception parseE) {
			Log.warn("date 형식 오류"+date);
			date="";
		}
		return date;
	}
	
}
