package rental.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Common;
import rental.common.ParamMap;
import rental.constant.LedStock;
import rental.constant.ProductRental;
import rental.service.CommonService;
import rental.service.ProductService;
import rental.service.StockService;

@Controller
@RequestMapping("stock/")
public class StockController {
	@Autowired
	StockService stockService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	CommonService commonService;
	
	
	

	/*
	 * 재고요청 폼
	 * **/
	@RequestMapping(value="ledRequest",method=RequestMethod.GET)
	public ModelAndView ledRequest(ParamMap map) {
		ModelAndView mav = new ModelAndView("stock/ledRequest");
		mav.addObject("tlsr_status",commonService.lsr_statusList());
		return mav;
	}
	
	/**
	 * 재고요청 조회폼
	 * */
	@RequestMapping(value="ledRequest",method=RequestMethod.POST)
	public @ResponseBody Object ledRequestList(ParamMap map) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<HashMap<String,Object>> list = stockService.selectRequestLedList(map.getMap());
		
		int filtered = 0;
		if (list != null && list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				Map<String, Object> prMap = list.get(i);
				int rownum = Integer.parseInt(prMap.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String e_name = "<a lsr_idx='" + prMap.get("lsr_idx") + "' onclick='choose()'>" + prMap.get("lsr_creater")
						+ "</a>";
				prMap.put("lsr_creater", e_name);
				
			}
		}
		

		try {
			filtered += Integer.parseInt(map.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", list);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", stockService.countRequestLedList(map.getMap()));
		return returnMap;
	}
	

	
	/**
	 * 기사재고 요청 폼
	 * */
	@RequestMapping(value="AddRequest" ,method=RequestMethod.GET)
	public ModelAndView AddRequest(ParamMap map) {
		ModelAndView mav = new ModelAndView("stock/AddRequest");
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		for (int i = 0; i < calList.size(); i++) {
			Map<String, Object> mapTemp = calList.get(i);
			mapTemp.put("p_deliverycost", Common.comma(mapTemp.get("p_deliverycost") + ""));
			mapTemp.put("p_price", Common.comma(mapTemp.get("p_price") + ""));
		};
		mav.addObject("login",map.get("login"));
		mav.addObject("tech", map.getMap().get("login"));
		mav.addObject("cal", calList);
		return mav;
	}
	
	/**
	 * 기사재고 요청 등록
	 * */
	@RequestMapping(value="AddRequest",method=RequestMethod.POST ,produces="text/plain;charset=utf8")
	public @ResponseBody Object insertRequest(ParamMap map) {
		int result = 0;
		try {
			result = stockService.insertLedRequest(map.getMap());
			return "등록완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록실패";

		}
	}
	/**
	 * 재고요청 상세폼
	 * */
	@RequestMapping(value="AddProgress" ,method=RequestMethod.GET)
	public ModelAndView Progress(ParamMap map) {
		ModelAndView mav = new ModelAndView("stock/AddProgress");
		return mav;
	}

	/**
	 * 기사재고요청 처리량 등록폼
	 * */
	@RequestMapping(value="AddProgress/{lsr_idx}" ,method=RequestMethod.GET)
	public ModelAndView AddProgress(ParamMap map, @PathVariable String lsr_idx) {
		ModelAndView mav = new ModelAndView("stock/AddProgress");
		Map<String, Object> dmap = new HashMap<String,Object>();
		int lsrid = Integer.parseInt(lsr_idx);
		dmap.put("lsrid", lsrid);

		dmap.put("e_id", ((HashMap)map.get("login")).get("e_id"));
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		
		Map<String , Object> reList = stockService.selectRequestDetail(dmap);
		
		for (int i = 0; i < calList.size(); i++) {
			Map<String, Object> mapTemp = calList.get(i);
			mapTemp.put("p_deliverycost", Common.comma(mapTemp.get("p_deliverycost") + ""));
			mapTemp.put("p_price", Common.comma(mapTemp.get("p_price") + ""));
		};
		
		Map<String,Object> req = new HashMap<String, Object>();
		Map<String,Object> prs = new HashMap<String, Object>();
		for(int i=1; i<10;i++) {
			req.put("lsr_req"+i, reList.get("lsr_req"+i));
			prs.put("lsp_prs"+i, reList.get("lsp_prs"+i));
		}
		mav.addObject("login",map.get("login"));
		mav.addObject("req",req);
		mav.addObject("prs",prs);
		mav.addObject("cal", calList);
		mav.addObject("list",reList);
		return mav;
	}
	
	
	/**
	 * 기사재고요청 처리량 등록
	 * */
	@RequestMapping(value="AddProgress/{lsr_idx}/confirmProgress" ,method=RequestMethod.POST,produces = "application/text; charset=utf8")
	public @ResponseBody Object AddProgress2(ParamMap map, @PathVariable String lsr_idx) {
		int lsrid = Integer.parseInt(lsr_idx);
		map.getMap().put("lsrid", lsrid);
		
		if(map.getMap().get("lsr_status").equals(LedStock.STATUS_COMPLETE_STOCK)) {
			int result = stockService.insertLedProgress(map.getMap());
			if(result ==1) {
				return "출고완료";
			}else
			{
				return "출고 실패";
			}
		}
		else if(map.getMap().get("lsr_status").equals(LedStock.STATUS_COMPLETE_RETURN)) {
			int result = stockService.insertLedReturn(map.getMap());
			if(result ==1) {
				return "출고완료";
			}else{
				return "반품 실패";
			}
		}else {
			return "에러 발생";
		}
	}
	
	/**
	 * 재고량 파악 AJAX
	 * */
	@RequestMapping(value="AddProgress/{lsr_idx}/checkStock" ,method=RequestMethod.GET,produces = "application/text; charset=utf8")
	public @ResponseBody Object checkStock(ParamMap map) {
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		int num = Integer.parseInt((String)map.getMap().get("num"));
		int pid = Integer.parseInt((String)map.getMap().get("pid"));
		rMap.put("num",num);
		rMap.put("pid", pid);
		try {
			int result = stockService.checkStock(rMap);
			if(result == 1) {
				return true;
			}else if(result == 0){
				return "재고가 충분하지 않습니다.";
			}else {
				return "실패";
			}
		} catch (Exception e) {
			System.out.println(e);
			return "실패";
		}
	}
	
	@RequestMapping(value="techStock/{e_id}" , method=RequestMethod.GET)
	public @ResponseBody Object checkTechnicianStock(ParamMap map,@PathVariable String e_id) {
		ModelAndView mav = new ModelAndView("delivery/table/techTR");
		HashMap<String, Object> rMap = new HashMap<String, Object>();
		int eid = Integer.parseInt(e_id);
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		rMap = stockService.checkTechnicianStock(eid);
		mav.addObject("tled_led",rMap);
		mav.addObject("calList",calList);
		return mav;
	}
	
	// LED 재고 요청 App
	@RequestMapping(value="ledRequestApp",method=RequestMethod.GET)
	public ModelAndView ledRequestApp(ParamMap map) {
		ModelAndView mav = new ModelAndView("stock/ledRequestApp");
		String e_id = ((HashMap)map.get("login")).get("e_id")+"";
		
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		Map<String, Object> smap = stockService.checkTechnicianStock(Integer.parseInt(e_id));
		List<HashMap<String,Object>> reqList = stockService.reqStock(e_id);
		mav.addObject("data",smap);
		mav.addObject("stock",reqList);
		mav.addObject("tech", map.getMap().get("login"));
		mav.addObject("cal", calList);
		return mav;
	}
	
	/** LED 재고 요청 App*/
	@RequestMapping(value="AddledRequestApp",method=RequestMethod.GET)
	public ModelAndView AddledRequestApp(ParamMap map) {
		ModelAndView mav = new ModelAndView("stock/ledRequestApp");
		String e_id = ((HashMap)map.get("login")).get("e_id")+"";
		Map<String, Object> smap = stockService.checkTechnicianStock(Integer.parseInt(e_id));
		List<HashMap<String,Object>> reqList = stockService.reqStock(e_id);
		mav.addObject("data",smap);
		mav.addObject("stock",reqList);
		return mav;
	}
	/**기사별 재고 초기화**/
	@RequestMapping(value="stockRefresh/{e_id}" ,method=RequestMethod.GET,produces="application/text;charset=utf8")
	public @ResponseBody Object stockRefresh(@PathVariable String e_id) {
		try {
			stockService.stockRefresh(e_id);
			return "갱신되었습니다.";
		} catch (Exception e) {
			e.printStackTrace();
			return "갱신 실패하였습니다.";
		}
	}
}
