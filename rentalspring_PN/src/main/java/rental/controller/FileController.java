package rental.controller;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import rental.common.Log;
import rental.constant.CS;
import rental.constant.CustomerFile;
import rental.constant.Delivery;
import rental.constant.InstallState;
import rental.constant.MemberFile;
import rental.service.CsService;
import rental.service.EmployeeService;
import rental.service.InstallService;
import rental.service.MemberService;

@Controller
@RequestMapping("file/")
public class FileController {
	
	@Autowired
	MemberService memberService;
	@Autowired
	InstallService installService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	CsService csService;
	
	//member_file download
	@RequestMapping("member/{mf_idx}_{mf_memo}")
	public void memberFile(@PathVariable String mf_idx,@PathVariable String mf_memo,HttpServletResponse response) {
		FileInputStream fis=null;
		ServletOutputStream sos=null;
		try{
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Transfer-Encoding", "binary");
			
			Map<String,Object>map=memberService.selectMemberFileDown(mf_idx, mf_memo);
			
			if(map==null||map.isEmpty()) {
				//System.out.println("wr");
				response.setHeader("Content-Disposition","attachment; fileName=\"error\";");
				return;
			}else {
				response.setHeader("Content-Disposition","attachment; fileName=\"" + URLEncoder.encode(map.get("mf_file")+"", "UTF-8") + "\";");
			}
			
			sos = response.getOutputStream();
			File f=new File(MemberFile.PATH+map.get("mf_name")+"/"+mf_idx+"_"+mf_memo);
			if(!f.exists()) {
				f=new File(MemberFile.PATH+map.get("mf_name")+"/"+map.get("mf_file"));
				if(!f.exists()) {
					f=new File(MemberFile.PATH+"녹취파일/"+map.get("mf_file"));
					if(!f.exists()) {
						//System.out.println("noFile");
						return;
					}
				}
			}
			fis=new FileInputStream(f);
			FileCopyUtils.copy(fis, sos);
			sos.flush();
			
		}catch (Exception e) {
			Log.warn(e.toString());
		}finally {
			try{
				if(fis!=null)fis.close();
				if(sos!=null)sos.close();
			}catch (Exception e) {}
		}
	}
	
	//배송관리.송장관리 엑셀양식다운로드
	@RequestMapping("delivery/carry/excel")
	public void deliveryCarryExcel(HttpServletResponse response) {
		FileInputStream fis=null;
		ServletOutputStream sos=null;
		try{
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setHeader("Content-Disposition","attachment; fileName=\"" + URLEncoder.encode("송장관리엑셀양식.xls", "UTF-8") + "\";");
			
			sos = response.getOutputStream();
			File f=new File(Delivery.PATH_FORM);
			fis=new FileInputStream(f);
			FileCopyUtils.copy(fis, sos);
			sos.flush();
			
		}catch (Exception e) {
			Log.warn(e.toString());
		}finally {
			try{
				if(fis!=null)fis.close();
				if(sos!=null)sos.close();
			}catch (Exception e) {}
		}
	}
		
		
	@RequestMapping(value="{mf_idx}",method=RequestMethod.DELETE,produces="text/plain;charset=utf8")
	public @ResponseBody Object fileDelete(@PathVariable String mf_idx) {
		String msg="삭제 완료";
		try{
			memberService.deleteMemberFile(mf_idx);
		}catch (Exception e) {
			e.printStackTrace();
			msg="오류 발생";
		}
		return msg;
	}
	

	//customer file download
		@RequestMapping("customer_cr/{c_id}_{c_comname}")
		public void customerC_File(@PathVariable String c_id,@PathVariable String c_comname,HttpServletResponse response) {
			FileInputStream fis=null;
			ServletOutputStream sos=null;
			try{
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Transfer-Encoding", "binary");
				
				Map<String,Object>map=employeeService.selectCustomerInfoDetail(c_id);
				
				System.out.println(map.entrySet());
				if(map==null||map.isEmpty()) {
					//System.out.println("wr");
					response.setHeader("Content-Disposition","attachment; fileName=\"error\";");
					return;
				}else {
					response.setHeader("Content-Disposition","attachment; fileName=\"" + URLEncoder.encode(map.get("c_file")+"", "UTF-8") + "\";");
				}
				
				sos = response.getOutputStream();
				File f=new File(CustomerFile.PATH+CustomerFile.FILE_REGISTRATION+"/"+c_id+"_"+c_comname);
				if(!f.exists()) {
					f=new File(CustomerFile.PATH+CustomerFile.FILE_REGISTRATION+"/"+map.get("c_file"));
					if(!f.exists()) {
						f=new File(CustomerFile.PATH+"사업자등록증/"+map.get("c_file"));
						if(!f.exists()) {
							return;
						}
					}
				}
				
			
				fis=new FileInputStream(f);
				FileCopyUtils.copy(fis, sos);
				sos.flush();
				
			}catch (Exception e) {
				Log.warn(e.toString());
			}finally {
				try{
					if(fis!=null)fis.close();
					if(sos!=null)sos.close();
				}catch (Exception e) {}
			}
		}
		
		//customer file download
				@RequestMapping("customer_bb/{c_id}_{c_account}")
				public void customerBankbook(@PathVariable String c_id,@PathVariable String c_account,HttpServletResponse response) {
					FileInputStream fis=null;
					ServletOutputStream sos=null;
					try{
						response.setContentType("application/octet-stream");
						response.setHeader("Content-Transfer-Encoding", "binary");
						
						Map<String,Object>map=employeeService.selectCustomerInfoDetail(c_id);
						
						if(map==null||map.isEmpty()) {
							//System.out.println("wr");
							response.setHeader("Content-Disposition","attachment; fileName=\"error\";");
							return;
						}else {
							response.setHeader("Content-Disposition","attachment; fileName=\"" + URLEncoder.encode(map.get("c_bankbook")+"", "UTF-8") + "\";");
						}
						
						sos = response.getOutputStream();
						File f=new File(CustomerFile.PATH+CustomerFile.FILE_BANKBOOK+"/"+c_id+"_"+c_account);
						if(!f.exists()) {
							f=new File(CustomerFile.PATH+CustomerFile.FILE_BANKBOOK+"/"+map.get("c_bankbook"));
							if(!f.exists()) {
								f=new File(CustomerFile.PATH+"통장사본/"+map.get("c_bankbook"));
								if(!f.exists()) {
									return;
								}
							}
						}
						fis=new FileInputStream(f);
						FileCopyUtils.copy(fis, sos);
						sos.flush();
						
					}catch (Exception e) {
						Log.warn(e.toString());
					}finally {
						try{
							if(fis!=null)fis.close();
							if(sos!=null)sos.close();
						}catch (Exception e) {}
					}
				}
				
				
				
				//CS Product File Download
				@RequestMapping("csproduct/{csm_idx}_{csm_name}")
				public void csproduct(@PathVariable String csm_idx,@PathVariable String csm_name,HttpServletResponse response) {
					FileInputStream fis=null;
					ServletOutputStream sos=null;
					try{
						response.setContentType("application/octet-stream");
						response.setHeader("Content-Transfer-Encoding", "binary");
						
						Map<String,Object>map=csService.selectCsDetail(csm_idx);
						
						if(map==null||map.isEmpty()) {
							//System.out.println("wr");
							response.setHeader("Content-Disposition","attachment; fileName=\"error\";");
							return;
						}else {
							response.setHeader("Content-Disposition","attachment; fileName=\"" + URLEncoder.encode(map.get("csm_file")+"", "UTF-8") + "\";");
						}
						
						sos = response.getOutputStream();
						File f=new File(CS.PATH+CS.CSM_FILE+"/"+csm_idx+"_"+csm_name);
						if(!f.exists()) {
							f=new File(CS.PATH+CS.CSM_FILE+"/"+map.get("csm_file"));
							if(!f.exists()) {
								f=new File(CustomerFile.PATH+"CS설명/"+map.get("csm_file"));
								if(!f.exists()) {
									return;
								}
							}
						}
						fis=new FileInputStream(f);
						FileCopyUtils.copy(fis, sos);
						sos.flush();
						
					}catch (Exception e) {
						Log.warn(e.toString());
					}finally {
						try{
							if(fis!=null)fis.close();
							if(sos!=null)sos.close();
						}catch (Exception e) {}
					}
				}
}
