package rental.controller;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import rental.common.AesEncrypt;
import rental.common.Common;
import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.Cms;
import rental.constant.MemberInfo;
import rental.constant.Pay;
import rental.service.CmsService;
import rental.service.CommonService;
import rental.service.MemberService;
import rental.service.PayService;
import rental.service.ProductService;
import sun.awt.image.OffScreenImageSource;

@Controller
@RequestMapping("finance/")
public class FinanceController {

	@Autowired
	CmsService cmsService;
	@Autowired
	MemberService memberService;
	@Autowired
	ProductService productService;
	@Autowired
	PayService payService;
	@Autowired
	CommonService commonService;
	
	@RequestMapping(value="account",method=RequestMethod.GET)
	public ModelAndView pay() {
		ModelAndView mav=new ModelAndView("finance/account");
		
		List<HashMap<String, Object>> gisu = memberService.getGisu();
		mav.addObject("gisu",gisu);
		return mav;
		
	}
	//금융원부 조회
	@RequestMapping(value="account",method=RequestMethod.POST)
	public @ResponseBody Object payPost(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		
		//전체 기수 검색
		String m_finance=(String)pmap.get("m_finance");
		if(m_finance==null||m_finance.equals("")) {
			pmap.remove("m_finance");
		}
		
		//숫자형식 정렬위해
		Object oc=pmap.get("order[0][column]");
		String order=pmap.get("columns["+oc+"][data]")+"";
		if(order.equals("rental")){
			pmap.put("columns["+oc+"][data]","m_rental");
		}
		
		List<Map<String,Object>> clist=memberService.accountList(pmap.getMap());
		int filtered=0;
		
		if(clist!=null&&clist.size()!=0) {
			for(int i=0;i<clist.size();i++) {
				Map<String,Object> mapTemp=clist.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				
				/**고객정보*/
//				String td="<a m_idx='"+mapTemp.get("m_idx")+"' onclick='member()'>"+mapTemp.get("mi_name")+"</a>";
//				mapTemp.put("mi_name", td);
//				mapTemp.put("m_idx", "");
				
				
				String c_id_type=mapTemp.get("c_mi_id_type")+"";
				if(c_id_type.equals(MemberInfo.ID_TYPE_PERSON)) {
					String c_id=mapTemp.get("c_mi_id")+"";
					c_id=c_id.replaceAll("[^0-9]", "");
					if(c_id.length()>6) {
						c_id=c_id.substring(0, 6)+"-"+c_id.substring(6, 7);
					}
					mapTemp.put("c_mi_id", c_id);
				}
				
				String m_id_type=mapTemp.get("m_id_type")+"";
				if(m_id_type.equals(MemberInfo.ID_TYPE_PERSON)) {
					String m_pay_id=mapTemp.get("m_pay_id")+"";
					m_pay_id=m_pay_id.replaceAll("[^0-9]", "");
					if(m_pay_id.length()>6) {
						m_pay_id=m_pay_id.substring(0, 6)+"-"+m_pay_id.substring(6, 7);
					}
					mapTemp.put("m_pay_id", m_pay_id);
				}
//				mapTemp.put("mi_name", td);
//				mapTemp.put("m_idx", "");
			}
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", memberService.countaccountList(pmap.getMap()));
		return returnMap;
	}
	
	//금융기수 입력
	@RequestMapping(value="checkName",method=RequestMethod.GET)
	public @ResponseBody Object checkName(String fg,String midxs) {
		String[] m_idx=midxs.split(",");
		HashMap<String, Object>hm=new HashMap<String, Object>();
		hm.put("m_finance", fg);
		int result=0;
		for(int i=0;i<m_idx.length;i++) {
			hm.put("m_idx",m_idx[i]);
			result+=memberService.updateMember(hm);
		}
		return result;
	}
	
	//금융관리(은행회원)
	@RequestMapping(value="bank",method=RequestMethod.GET)
	public ModelAndView bank() {
		ModelAndView mav=new ModelAndView("finance/bank");
		mav.addObject("financeList", memberService.getGisu());
		mav.addObject("mprocessList", commonService.mprocessList());
		return mav;
	}
	
	//금융관리(카드회원)
	@RequestMapping(value="card",method=RequestMethod.GET)
	public ModelAndView card() {
		ModelAndView mav=new ModelAndView("finance/card");
		mav.addObject("financeList", memberService.getGisu());
		mav.addObject("mprocessList", commonService.mprocessList());
		return mav;
	}
	//CMS회원관리 회원목록 dataTable. 금융기수!=''
	@RequestMapping(value="member/{m_pay_method}",method=RequestMethod.POST)
	public @ResponseBody Object memberCms(ParamMap pmap,@PathVariable String m_pay_method) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		if(m_pay_method.equals("bank")) {
			pmap.put("m_pay_method", MemberInfo.PAY_METHOD_BANK);
		}else if(m_pay_method.equals("card")) {
			pmap.put("m_pay_method", MemberInfo.PAY_METHOD_CARD);
		}else {
			return "잘못된 접근입니다.";
		}
		
		List<Map<String,Object>> clist=memberService.selectMemberCms(pmap.getMap());
		int filtered=0;
		for(int i=0;i<clist.size();i++) {
			Map<String,Object> mapTemp=clist.get(i);
			int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
			if(filtered<rownum) {
				filtered=rownum;
			}
			
			/**고객정보*/
			String td="<a m_idx='"+mapTemp.get("m_idx")+"' onclick='member()'>"+mapTemp.get("mi_name")+"/"+mapTemp.get("m_pay_owner")+"</a>";
			mapTemp.put("m_pay_owner", td);
			mapTemp.put("m_idx", "");
			
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", memberService.countMemberCms(pmap.getMap()));
		return returnMap;
	}
			
	//cms계정관리
	@RequestMapping(value="cms",method=RequestMethod.GET)
	public ModelAndView cms() {
		ModelAndView mav=new ModelAndView("finance/cms");
		return mav;
	}
	//cms계정관리 DataTable
	@RequestMapping(value="cms",method=RequestMethod.POST)
	public @ResponseBody Object cmsPost(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		
		List<Map<String,Object>> clist=cmsService.selectCmsList(pmap.getMap());
		int filtered=0;
		if(clist!=null&&clist.size()!=0) {
			DecimalFormat df=new DecimalFormat("#,###,###,###,###,###,###");
			for(int i=0;i<clist.size();i++) {
				Map<String,Object> mapTemp=clist.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				String cms_id=(String)mapTemp.get("cms_id");
				if(cms_id==null||cms_id.equals("")){
					cms_id="(없음)";
				}
				String td="<a cms_idx='"+mapTemp.get("cms_idx")+"' onclick='cms()'>"+cms_id+"</a>";
				mapTemp.put("cms_id", td);
				
				//숫자 컴마 붙이기
				mapTemp.put("cms_amount", df.format(mapTemp.get("cms_amount")));
				mapTemp.put("m_total", df.format(mapTemp.get("m_total")));
				mapTemp.put("m_rental", df.format(mapTemp.get("m_rental")));
				mapTemp.put("m_ilsi", df.format(mapTemp.get("m_ilsi")));
			}
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", cmsService.countCms());
		return returnMap;
	}
	
	//cms 추가 팝업
	@RequestMapping(value="cms/add",method=RequestMethod.GET)
	public ModelAndView cmsAdd() {
		ModelAndView mav=new ModelAndView("finance/cmsAdd");
		return mav;
	}
	//cms 추가팝업 >cms 추가
	@RequestMapping(value="cms/add",method=RequestMethod.POST,produces="text/plain;charset=utf-8")
	public @ResponseBody Object cmsAddPost(ParamMap pmap) {
		String msg="테스트중";
		try{
			AesEncrypt ae=new AesEncrypt();
			pmap.put("cms_pw",ae.encrypt(pmap.get("cms_pw")+""));
			pmap.put("cms_card",ae.encrypt(pmap.get("cms_card")+""));
			
			String cms_amount=(String)pmap.get("cms_amount");
			cms_amount=cms_amount.replaceAll("[^0-9]", "");
			if(cms_amount==null||cms_amount.equals("")) {
				cms_amount="0";
			}
			pmap.put("cms_amount",Integer.parseInt(cms_amount));
		}catch (NumberFormatException ne) {
			return "오류발생.\n총 대출 가능금액을 확인해주세요.";
		}catch (Exception e) {
			return "암호화 중 오류발생";
		}
		int result=cmsService.insertCms(pmap.getMap());
		if(result>0) {
			msg="등록완료";
		}else {
			msg="오류발생.\n다시 시도해주세요.";
		}
		return msg;
	}
	
	//cms 은행회원 일자별 등록 현황 dt
	@RequestMapping(value="bank/sendList",method=RequestMethod.POST)
	public @ResponseBody Object bankSend(ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Calendar now=Calendar.getInstance();
		List<Map<String,Object>> clist=payService.selectPaySendStatistic(pmap.getMap());
		int filtered=0;
		if(clist!=null&&clist.size()!=0) {
			DecimalFormat df=new DecimalFormat("#,###,###,###,###,###,###");
			for(int i=0;i<clist.size();i++) {
				Map<String,Object> mapTemp=clist.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				String ps_date=(String)mapTemp.get("ps_date");
				String td="<a onclick='psDate()'>"+ps_date+"</a>";
				mapTemp.put("ps_date", td);
				
				//숫자 컴마 붙이기
				mapTemp.put("total", df.format(mapTemp.get("total")));
				mapTemp.put("regi", df.format(mapTemp.get("regi")));
				mapTemp.put("modi", df.format(mapTemp.get("modi")));
				
				//결과요청버튼
				try {
					String resultBttn="<a ps_date='"+ps_date+"' ";
					Date ps=sdf.parse(ps_date);
					Calendar ps_cal=Calendar.getInstance();
					ps_cal.setTime(ps);
					ps_cal.set(Calendar.HOUR_OF_DAY, Pay.SEND_REGI_TIME);
					
					if (now.before(ps_cal)) {
					//신청일이 오늘 보다 미래(신청가능)
						resultBttn+="class='btn bg-primary legitRipple' onclick='sendCms(\"put\")'>";
						resultBttn+=" 신 청 ";
						resultBttn+="</a>";
						resultBttn+="<a class='btn bg-success legitRipple ml-10' onclick='sendCms(\"get\")'>신청내역 조회</a>";
						resultBttn+="<a class='btn bg-warning legitRipple ml-10' onclick='sendCms(\"delete\")'>신청취소";
					}else {
					//신청일이 오늘보다 이전(과거)
						ps_cal.add(Calendar.DATE, 1);//다음날 11시 이후 결과요청 가능
						ps_cal.set(Calendar.HOUR_OF_DAY, Pay.SEND_RESULT_TIME);
						
						if(now.after(ps_cal)) {
						//어제보다 이전
							resultBttn+="class='btn bg-slate-600 legitRipple' onclick='getResult()'>";
							resultBttn+="결과 요청";
						}else {
						//어제~오늘
							resultBttn+=" class='btn bg-success legitRipple ml-10' onclick='sendCms(\"get\")'>신청내역 조회</a>";
							resultBttn+="<a class='btn bg-slate-300 legitRipple ml-10'>";
							resultBttn+="결과 대기";
						}
					}
					resultBttn+="</a>";
					mapTemp.put("cms_result", resultBttn);
				}catch (Exception e) {
					mapTemp.put("cms_result", "오류 발생");
				}
			}
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", payService.countPaySendStatistic());
		return returnMap;
	}
	
	//신청날짜 은행등록 신청 목록 보기(dataTable)
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.POST)
	public @ResponseBody Object bankSendDetail(@PathVariable String ps_date,ParamMap pmap) {
		Map<String, Object> returnMap=new HashMap<String, Object>();
		List<Map<String,Object>> list=payService.selectPaySendByDate(pmap.getMap(),ps_date, "등록수정만");
		int filtered=0;
		if(list!=null&&list.size()!=0) {
			for(int i=0;i<list.size();i++) {
				Map<String,Object> mapTemp=list.get(i);
				int rownum=Integer.parseInt(mapTemp.get("rownum")+"");
				if(filtered<rownum) {
					filtered=rownum;
				}
				mapTemp.put("ps_idx", "<input name='ps_idx' type='hidden' value='"+mapTemp.get("ps_idx")+"'/>");
			}
		}
		
		try {
			filtered+=Integer.parseInt(pmap.get("start")+"");
		}catch (Exception e) {}
		
		returnMap.put("data", list);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", payService.countPaySendByDate(ps_date, ""));
		return returnMap;
	}
	
	//신청날짜로 은행회원 신청
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.PUT,produces="text/plain;charset=utf8")
	public @ResponseBody Object sendListCms(@PathVariable String ps_date) {
		return cmsService.regiCms(ps_date,1);
	}
	//신청날짜로 은행회원 신청취소
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.DELETE,produces="text/plain;charset=utf8")
	public @ResponseBody Object sendListCmsDelete(@PathVariable String ps_date) {
		return cmsService.regiCms(ps_date,0);
	}
	//신청날짜로 은행회원 신청 조회
	@RequestMapping(value="bank/sendList/{ps_date}",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object sendListCmsPut(@PathVariable String ps_date) {
		return cmsService.regiCmsRead(ps_date,Cms.SDSI_DATA_MEM);
	}
	
	//cms 은행회원 등록 수정(바구니에 담기)
	@RequestMapping(value="bank/regi",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankRegiPost(ParamMap pmap) {
		//reg or change
		String type=pmap.get("type")+"";
		if(type.equals("reg")) {
			pmap.put("pay_idx", Pay.PS_PAY_IDX_REGISTER);
			pmap.put("type", MemberInfo.CMS_UNREGISTERD);
		}else if(type.equals("change")) {
			pmap.put("pay_idx", Pay.PS_PAY_IDX_CHANGE);
			pmap.put("type", MemberInfo.CMS_CHANGE);
		}else {
			return "잘못된 접근입니다.";
		}
		
		String m_idxs=(String)pmap.get("m_idx");
		if(m_idxs==null||m_idxs.equals("")) {
			return "선택된 고객이 없습니다.";
		}
		
		return payService.regiPaySend(pmap.getMap());
	}
	
	//cms 은행회원신청,출금신청 목록에서 삭제(바구니에서 빼기)
	@RequestMapping(value="bank/deletePaySend",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankRegiDelete(String ps_idx) {
		String msg="";
		if(ps_idx==null||ps_idx.equals("")) {
			return "선택된 고객이 없습니다.";
		}
		msg=payService.deletePaySend(ps_idx);
		return msg;
	}
	
	//신청 후 일자별로 파일 일괄 등록
	@RequestMapping(value="bank/sendFile",produces="text/plain;charset=utf8")
	public @ResponseBody Object bankSendFile(String ps_date) {
		return cmsService.sendFile(ps_date);
	}
	
	//카드 회원 등록 cms_real insert + 등록(수정과 같음)
	@RequestMapping(value="card/api",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object cardApiPost(String m_idx,String cms_idx) {
		String msg="";
		StringTokenizer st=new StringTokenizer(m_idx, ",");
		int total=st.countTokens();
		int fail=0;//요청한 작업 처리할 수 없는 상태의 고객
		int cmsidx=Integer.parseInt(cms_idx);
		while(st.hasMoreTokens()) {
			String str_m_idx=st.nextToken();
			int midx=Integer.parseInt(str_m_idx);
			//cms_real에 insert
			cmsService.insertCmsReal(midx,cmsidx, Cms.CR_TYPE_CARD);
			if(cmsService.changeCardCms(midx)<1) {
				fail++;
			}
		}
		if(fail>0) {
			msg="회원 등록하였습니다.\n"+total+"건 중 "+(total-fail)+"건 등록완료";
		}else {
			msg="회원 등록하였습니다.";
		}
		return msg;
	}
	//카드 회원 수정
	@RequestMapping(value="card/api",method=RequestMethod.PUT,produces="text/plain;charset=utf8")
	public @ResponseBody Object cardApiPut(String m_idx) {
		String msg="";
		StringTokenizer st=new StringTokenizer(m_idx, ",");
		int total=st.countTokens();
		int fail=0;//요청한 작업 처리할 수 없는 상태의 고객
		while(st.hasMoreTokens()) {
			String str_midx=st.nextToken();
			int midx=Integer.parseInt(str_midx);
			if(cmsService.changeCardCms(midx)<1) {
				fail++;
			}
		}
		if(fail>0) {
			msg="회원 정보 수정하였습니다.\n"+total+"건 중 "+(total-fail)+"건 수정완료";
		}else {
			msg="회원 정보 수정하였습니다.";
		}
		return msg;
	}
	//카드 회원 삭제
	@RequestMapping(value="card/api",method=RequestMethod.DELETE,produces="text/plain;charset=utf8")
	public @ResponseBody Object cardApiDelete(String m_idx) {
		String msg="";
		String[] st=m_idx.split(",");
		int total=st.length;
		int fail=0;//요청한 작업 처리할 수 없는 상태의 고객
		for(int i=0;i<total;i++){
			String str_midx=st[i];
			int midx=Integer.parseInt(str_midx);
			int result=cmsService.deleteCms(midx);
			if(result<0) {
				fail++;
			}
		}
		if(fail>0) {
			msg="회원 삭제하였습니다.\n"+total+"건 중 "+(total-fail)+"건 삭제완료";
		}else {
			msg="회원 삭제하였습니다.";
		}
		return msg;
	}
	
	//은행 회원 신청 결과 받기
	@RequestMapping(value="bank/result/{ps_date}",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object bankResult(@PathVariable String ps_date) {
		String msg="";
		msg=cmsService.cmsResult(ps_date, Cms.SDSI_DATA_MEM);
		
		return msg;
	}
	
	//cms회원상태 조회
	@RequestMapping(value="{m_idx}/checkCms",method=RequestMethod.PUT,produces="text/plain;charset=utf8")
	public @ResponseBody Object checkCms(@PathVariable String m_idx) {
		String msg=cmsService.checkCms(Integer.parseInt(m_idx));
		return msg;
	}
	
	//cms회원상태 cms전부 조회
	@RequestMapping(value="searchCms",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object searchCms() {
		//String test="28820,28915,28922,28937,29235,29259,29327,29389,29390,29407,29421,29647,29803,29839,30031,30069,30089,30251,30409,30533,30644,32770,32794,32879,32915,32962,33031,33060,33072,33096,33119,33132,33208,33244,33281,33382,33387,33406,33414,33428,33439,33444,33460,33471,33472,33478,33487,33492,33496,33532,33562,33589,33676,33728,33749,33756,33803,33829,33886,33894,33896,33902,33917,33932,33943,33946,33947,34039,34046,34077,34090,34105,34108,34134,34136,34138,34165,34181,34195,34230,34265,34338,34357,34401,34435,34442,34448,34469,34470,34473,34491,34541,34547,34602,34607,34617,34619,34639,34652,34680,34681,34682,34683,34684,34685,34686,34718,34719,34720,34860,34914,34995,34997,35032,35059,35070,35076,35083,35092,35107,35123,35142,35173,35174,35200,35230,35235,35248,35270,35285,35293,35319,35321,35359,35425,65985";
		String test="";
		if(test.equals("")) {
			return "test";
		}
		StringTokenizer st=new StringTokenizer(test,",");
		int total=st.countTokens();
		int cnt=0;
		while(st.hasMoreTokens()) {
			String str_midx=st.nextToken();
			int m_idx=Integer.parseInt(str_midx);
			cmsService.searchCms(m_idx);
		}
		System.out.println(total+":total");
		System.out.println(cnt+":cnt");
		
		return "test";
	}
}
