package rental.controller;

import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.*;
import java.util.Base64.Decoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Common;
import rental.common.ParamMap;
import rental.constant.CustomerFile;
import rental.constant.Employee;
import rental.constant.ProductRental;
import rental.service.EmployeeService;
import rental.service.PayService;

@Controller
@RequestMapping("employee/")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@Autowired
	PayService payService;
	
	/**임직원관리페이지*/
	@RequestMapping(value="",method=RequestMethod.GET)
	public ModelAndView employee() {
		ModelAndView mav = new ModelAndView("employee/employee");
		List<HashMap<String,Object>> position = employeeService.selectPosition();
		List<HashMap<String,Object>> job = employeeService.selectJob();
		List<HashMap<String,Object>> power = employeeService.selectPower();
		List<HashMap<String,Object>> branch = employeeService.selectBranch();
		List<HashMap<String,Object>> department = employeeService.selectDepartment();
		
		mav.addObject("position",position);
		mav.addObject("job",job);
		mav.addObject("power",power);
		mav.addObject("branch",branch);
		mav.addObject("department",department);
		return mav;
	}
	
	/**임직원관리페이지 dataTable*/
	@RequestMapping(value="",method=RequestMethod.POST)
	public @ResponseBody Object employeePost(ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		List<Map<String, Object>> elist = employeeService.selectEmployeeList(pmap.getMap());
		int filtered = 0;
		if (elist != null && elist.size() != 0) {
			for (int i = 0; i < elist.size(); i++) {
				Map<String, Object> mapTemp = elist.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String e_name = mapTemp.get("e_name")+"";
				String e_id = mapTemp.get("e_id")+"";
				String td1 = "<a e_id='"+e_id+"' onclick='employeeDetail()'>"+e_name+"</a>";
				mapTemp.put("e_name", td1);
				mapTemp.put("e_id", "");
				
				int e_status = Integer.parseInt(mapTemp.get("e_status")+"");
				if(e_status==1) {
					mapTemp.put("t_status",Employee.EMPLOYEE_STATUS_NORMALCY);
				}else if(e_status ==2) {
					mapTemp.put("t_status",Employee.EMPLOYEE_STATUS_LEAVE);
				}else if(e_status==3) {
					mapTemp.put("t_status",Employee.EMPLOYEE_STATUS_STOPUSING);
				}
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", elist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", employeeService.countEmployeeList());
		return returnMap;
	}
	
	
	/**각 임직원 상세페이지*/
	@RequestMapping(value="employeeDetail/{e_id}",method=RequestMethod.GET)
	public ModelAndView employeeDetail(@PathVariable String e_id) {
		ModelAndView mav = new ModelAndView("employee/employeeDetail");
		Map<String,Object> map = employeeService.selectEmployeeInfoDetail(e_id);
		map.remove("e_pass");
		List<HashMap<String,Object>> position = employeeService.selectPosition();
		List<HashMap<String,Object>> job = employeeService.selectJob();
		List<HashMap<String,Object>> power = employeeService.selectPower();
		List<HashMap<String,Object>> branch = employeeService.selectBranch();
		List<HashMap<String,Object>> department = employeeService.selectDepartment();
		
		mav.addObject("position",position);
		mav.addObject("job",job);
		mav.addObject("power",power);
		mav.addObject("branch",branch);
		mav.addObject("department",department);
		mav.addObject("employee",map);
		return mav;
	}
	
	/**각 임직원 상세페이지*/
	@RequestMapping(value="employeeHistoryList/{e_id}",method=RequestMethod.POST)
	public @ResponseBody Object employeeHistoryList(ParamMap pmap ,@PathVariable String e_id) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		pmap.printEntrySet();
		pmap.getMap().put("eid", e_id);
		List<HashMap<String, Object>> mList = employeeService.selectEmployeeModifyList(pmap.getMap());
		int filtered = 0;
		if (mList != null && mList.size() != 0) {
			for (int i = 0; i < mList.size(); i++) {
				Map<String, Object> mapTemp = mList.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String td = "<a id = '"+mapTemp.get("eh_idx")+"' onclick='modifyList()'>"+mapTemp.get("e_modify_memo")+"</a>";
				mapTemp.put("e_modify_memo", td);
				mapTemp.put("eh_idx", "");
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", mList);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", employeeService.selectEmployeeModifyListCount(e_id));
		return returnMap;
	}
	
	
	
	/**각 임직원 상세페이지*/
	@RequestMapping(value="employeeHistory/{eh_idx}_{e_id}",method=RequestMethod.GET)
	public ModelAndView employeeHistory(@PathVariable String e_id,@PathVariable String eh_idx) {
		ModelAndView mav = new ModelAndView("employee/employeeHistory");
		Map<String,Object> nmap = employeeService.selectEmployeeInfoDetail(e_id);
		Map<String,Object> hmap = employeeService.selectEmployeeHistoryInfoDetail(eh_idx);
		Map<String,Object> pvmap = employeeService.selectEmployeeHistoryPrvDetail(e_id,eh_idx);
		
		nmap.remove("e_pass");
		List<HashMap<String,Object>> position = employeeService.selectPosition();
		List<HashMap<String,Object>> job = employeeService.selectJob();
		List<HashMap<String,Object>> power = employeeService.selectPower();
		List<HashMap<String,Object>> branch = employeeService.selectBranch();
		List<HashMap<String,Object>> department = employeeService.selectDepartment();
		
		mav.addObject("position",position);
		mav.addObject("job",job);
		mav.addObject("power",power);
		mav.addObject("branch",branch);
		mav.addObject("department",department);
		mav.addObject("hData",hmap);
		mav.addObject("nData",nmap);
		mav.addObject("oData",pvmap);
		return mav;
	}
	
	
	/**임직원상세페이지 -> 수정 후 업데이트*/
	@RequestMapping(value="employeeDetail/{e_id}",method=RequestMethod.POST,produces= "application/text; charset=utf8")
	public @ResponseBody Object employeeDetailPost(ParamMap pmap) {
		try {
			employeeService.updateEmployee(pmap.getMap());
			return "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록 실패";
		}
	}
	
	/**임직원 추가 팝업*/
	@RequestMapping(value="create",method=RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView mav = new ModelAndView("employee/create");
		List<HashMap<String,Object>> position = employeeService.selectPosition();
		List<HashMap<String,Object>> job = employeeService.selectJob();
		List<HashMap<String,Object>> power = employeeService.selectPower();
		List<HashMap<String,Object>> branch = employeeService.selectBranch();
		List<HashMap<String,Object>> department = employeeService.selectDepartment();
		
		mav.addObject("position",position);
		mav.addObject("job",job);
		mav.addObject("power",power);
		mav.addObject("branch",branch);
		mav.addObject("department",department);
		return mav;
	}
	
	/**임직원 추가*/
	@RequestMapping(value="create",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	public @ResponseBody Object createPost(ParamMap pmap) {
		String msg="";
		try{
			String e_number=pmap.get("e_number")+"";
			int check=employeeService.checkId(e_number);
			if(check>0) {
				msg="이미 등록된 직원번호입니다.";
			}else {
				String e_pass=pmap.get("e_pass")+"";
				String e_pass_check=pmap.get("e_pass_check")+"";
				if(e_pass.equals(e_pass_check)) {
					employeeService.insertEmployee(pmap.getMap());
					if(pmap.get("e_id")!=null) {
						msg="success";
					}
				}else {
					msg="비밀번호가 서로 다릅니다.";
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			msg="오류발생. 다시 시도해주세요.";
		}
		
		return msg;
	}
	
	@RequestMapping("department/{e_department}")
	public @ResponseBody Object department(@PathVariable String e_department) {
		return employeeService.selectEmployeeListByDepartment(e_department);
	}
	

	@RequestMapping("tech")
	public ModelAndView tech() {
		ModelAndView mav = new ModelAndView("employee/tech");
		return mav;
	}
	
	/**
	 * 기사관리 리스트 불러오기
	 * */
	@RequestMapping(value="tech",method=RequestMethod.POST)
	public 	@ResponseBody Object techList(ParamMap map) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> tlist = employeeService.selecttechList(map.getMap());
		int filtered = 0;
		if (tlist != null && tlist.size() != 0) {
			for (int i = 0; i < tlist.size(); i++) {
				Map<String, Object> mapTemp = tlist.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String t_idx = mapTemp.get("t_idx") + "";
				String td = "<a t_idx='" + t_idx + "' onclick='technician()'>" + mapTemp.get("t_name") + "</a>";
				mapTemp.put("t_name", td);
				mapTemp.put("t_idx", "");

				int t_status = Integer.parseInt(mapTemp.get("t_status")+"");
				if(t_status==1) {
					mapTemp.put("t_status",Employee.EMPLOYEE_STATUS_NORMALCY);
				}else if(t_status ==2) {
					mapTemp.put("t_status",Employee.EMPLOYEE_STATUS_LEAVE);
				}else if(t_status==3) {
					mapTemp.put("t_status",Employee.EMPLOYEE_STATUS_STOPUSING);
				}
				
			}
		}
		try {
			filtered += Integer.parseInt(map.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", tlist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", employeeService.countTechList(map.getMap(),Employee.POWER_KISA));
		return returnMap;
	}	
	
	/**
	 * 기사등록폼
	 * */
	@RequestMapping(value="AddTechnician",  method= RequestMethod.GET)
	public ModelAndView AddTechnicanView(ParamMap map) {
		ModelAndView mav = new ModelAndView("employee/AddTechnician");
		List<HashMap<String,Object>> city = employeeService.selectCity();
		List<HashMap<String,Object>> branch = employeeService.selectBranch();
		mav.addObject("branch",branch);
		mav.addObject("t_creater",map.get("e_name"));
		mav.addObject("city",city);
		return mav;
	}	
	
	
	@RequestMapping(value="district",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	public void selectAjax(HttpServletRequest req, HttpServletResponse res, String param) { 
		try {
		   String province = param;
		   List<HashMap<String,Object>> district = employeeService.selectDistrict(province);
		   
		   // jsonArray에 추가
		   JSONArray jsonArray = new JSONArray();
		   for (int i = 0; i < district.size(); i++) {
			   jsonArray.add((URLDecoder.decode(district.get(i)+"")));
		   }

		   // jsonArray 넘김
		   res.setCharacterEncoding("UTF-8");   
		   PrintWriter pw = res.getWriter();
		   pw.print(jsonArray);
		   pw.flush();
		   pw.close();
		 
		   } catch (Exception e) {
		       System.out.println("Controller error");
		   }
		}
	
	
	
	
	/**
	 * 기사 등록
	 * */
	@RequestMapping(value="AddTechnician",  method= RequestMethod.POST,produces = "application/text; charset=utf8")
	public @ResponseBody Object AddTechnican(ParamMap map) {
		int result =0;
		String [] arr = null;
		String a_loc = "";
		if(map.getMap().get("ta_gu").toString().getClass().isArray()) {
		 arr = ((String[])map.getMap().get("ta_gu"));
			if(map.getMap().get("ta_gu") !=null){
				if(map.getMap().get("ta_gu").getClass().isArray()) {
					 arr = (String[]) map.getMap().get("ta_gu");
					 for(int i=0;i<arr.length;i++) {
						 a_loc += arr[i]+",";
					 }
				}else {
					 a_loc = map.getMap().get("ta_gu")+"";
				}
				}else {
					a_loc="";
				}
		}else {
			a_loc = map.get("ta_gu")+"";
		}
		map.getMap().put("a_loc", a_loc);
		try {
			employeeService.insertTechnician(map.getMap());
			return "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록 실패";
		}
	}	
	
	@RequestMapping(value="technician/{t_idx}",method=RequestMethod.GET)
	public ModelAndView technician(@PathVariable String t_idx) {
		ModelAndView mav = new ModelAndView("employee/technician");
		int tidx  =Integer.parseInt(t_idx);
		Map<String,Object> technician = employeeService.selectTechnicianDetail(tidx);
		List<HashMap<String,Object>> city = employeeService.selectCity();
		List<HashMap<String,Object>> district = employeeService.selectDistrict(technician.get("t_activity_loc")+"");
		List<HashMap<String,Object>> branch = employeeService.selectBranch();
		String[] dis=((String)technician.get("t_activity_dis")).split(",");
		  
		mav.addObject("branch",branch);
		mav.addObject("tlist",technician);
		mav.addObject("city",city);
		mav.addObject("district",district);
		mav.addObject("s_dis",dis);
		
		return mav;
	}
	
	/**각 거래처 상세페이지*/
	@RequestMapping(value="technicianHistoryList/{t_idx}",method=RequestMethod.POST)
	public @ResponseBody Object technicianHistoryList(ParamMap pmap , @PathVariable String t_idx) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		int filtered = 0;
		pmap.getMap().put("tid", t_idx);
		List<Map<String,Object>> mList = employeeService.selectTechnicianModifyList(pmap.getMap());
		if (mList != null && mList.size() != 0) {
			for (int i = 0; i < mList.size(); i++) {
				Map<String, Object> mapTemp = mList.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String td = "<a id = '"+mapTemp.get("th_idx")+"' onclick='modifyList()'>"+mapTemp.get("t_modify_memo")+"</a>";
				mapTemp.put("t_modify_memo", td);
				mapTemp.put("th_idx", "");
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", mList);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", employeeService.selectTechnicianModifyListCount(t_idx));
		return returnMap;
	}
	
	
	/**
	 * 기사 등록
	 * */
	@RequestMapping(value="updateTechnician",  method= RequestMethod.POST,produces = "application/text; charset=utf8")
	public @ResponseBody Object updateTechnican(ParamMap map) {
		int result =0;
		String [] arr = null;
		String a_loc = "";
		if(map.getMap().get("ta_gu").toString().getClass().isArray()) {
		 arr = ((String[])map.getMap().get("ta_gu"));
			if(map.getMap().get("ta_gu") !=null){
				if(map.getMap().get("ta_gu").getClass().isArray()) {
					 arr = (String[]) map.getMap().get("ta_gu");
					 for(int i=0;i<arr.length;i++) {
						 a_loc += arr[i]+",";
					 }
				}else {
					 a_loc = map.getMap().get("ta_gu")+"";
				}
				}else {
					a_loc="";
				}
		}else {
			a_loc = map.get("ta_gu")+"";
		}
		map.getMap().put("a_loc", a_loc);
		try {
			employeeService.updateTechnicianInfo(map.getMap());
			return "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록 실패";
		}
	}
	
	/**회원가입시 아이디 중복 체크*/
	@RequestMapping(value="checkId",method= RequestMethod.GET,produces = "application/text; charset=utf8")
	public @ResponseBody Object checkId(String e_number) {
		return employeeService.checkId(e_number)+"";
	}
	
	
	/**거래처관리페이지*/
	@RequestMapping(value="customer",method=RequestMethod.GET)
	public ModelAndView customer() {
		ModelAndView mav = new ModelAndView("employee/customer");
		List<HashMap<String,Object>> category = employeeService.selectCategory();
		mav.addObject("category",category);
		return mav;
	}
	
	/**임직원관리페이지 dataTable*/
	@RequestMapping(value="customer",method=RequestMethod.POST)
	public @ResponseBody Object customerPost(ParamMap pmap) {
		String c_date = (String) pmap.get("c_date");
		if (c_date == null || !c_date.contains("~")) {
			pmap.remove("c_date");
		} else {
			String c_dates[] = c_date.split("~");
			pmap.put("c_date1", c_dates[0].trim());
			pmap.put("c_date2", c_dates[1].trim());
		}
		
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> clist = employeeService.selectCustomerList(pmap.getMap());
		int filtered = 0;
		
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String e_name = mapTemp.get("e_name")+"";
				String e_id = mapTemp.get("e_id")+"";
				String c_comname = mapTemp.get("c_comname")+"";
				String c_id = mapTemp.get("c_id")+"";
				String td1 = "<a c_id='"+c_id+"' onclick='customerDetail()'>"+c_comname+"</a>";
				mapTemp.put("c_id","");
				mapTemp.put("c_comname", td1);
				String td2 = "<a e_id='"+e_id+"' onclick='employeeDetail()'>"+e_name+"</a>";
				mapTemp.put("e_name", td2);
			
			
			
			String c_addr ="";
			c_addr += mapTemp.get("c_addr1")+" "+mapTemp.get("c_addr2")+" "+mapTemp.get("c_addr3");
			mapTemp.put("c_addr", c_addr);
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", employeeService.countCustomerList());
		return returnMap;
	}
	
	
	/**거래처 추가 팝업*/
	@RequestMapping(value="customer/create",method=RequestMethod.GET)
	public ModelAndView customerCreate() {
		ModelAndView mav = new ModelAndView("employee/customer/create");
		List<HashMap<String,Object>> category = employeeService.selectCategory();
		List<HashMap<String,Object>> type = employeeService.selectType();
		List<HashMap<String,Object>> state = employeeService.selectState();
		List<Map<String,Object>> bank = payService.selectSubBankList();
		
		mav.addObject("category",category);
		mav.addObject("type",type);
		mav.addObject("bank",bank);
		mav.addObject("state",state);
		return mav;
	}
	
	/**거래처 추가*/
	@RequestMapping(value="customer/create",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	public @ResponseBody Object customerCreatePost(ParamMap pmap,MultipartFile c_file,MultipartFile c_bankbook) {
		String msg="";
		
			try{
				String c_pass=pmap.get("c_pass")+"";
				String c_pass_check=pmap.get("c_pass_check")+"";
					
				
				if(c_file != null) {
					pmap.put("c_file", c_file.getOriginalFilename());
				}else {
					pmap.put("c_file", c_file.getOriginalFilename());
				}
				if(c_bankbook !=null ) {
					
					pmap.put("c_bankbook", c_bankbook.getOriginalFilename());
				}
					employeeService.insertCustomer(pmap.getMap(),c_file,c_bankbook);
					msg="등록완료";
			
			}catch (Exception e) {
			e.printStackTrace();
			msg="오류발생. 다시 시도해주세요.";
			}
		
		return msg;
	}
	
	
	/**각 거래처 상세페이지*/
	@RequestMapping(value="customer/customerDetail/{c_id}",method=RequestMethod.GET)
	public ModelAndView customerDetail(@PathVariable String c_id) {
		ModelAndView mav = new ModelAndView("employee/customer/customerDetail");
		Map<String,Object> map = employeeService.selectCustomerInfoDetail(c_id);
		
		List<HashMap<String,Object>> category = employeeService.selectCategory();
		List<HashMap<String,Object>> type = employeeService.selectType();
		List<HashMap<String,Object>> state = employeeService.selectState();
		List<Map<String,Object>> bank = payService.selectSubBankList();
		
		String regex = ".*[\\[\\][:]\\\\/?[*]].*";
		String c_comname = map.get("c_comname")+"";
		if(c_comname.matches(regex)){  // text에 정규식에 있는 문자가 있다면 true 없다면 false 
			//대괄호는 소괄호로
			c_comname = c_comname.replaceAll("\\[", "\\(");
			c_comname = c_comname.replaceAll("\\]", "\\)");
			//나머지 특수문자는 제거
			c_comname = c_comname.replaceAll("[[:]\\\\/?[*]]", "");  
		}
		map.put("c_comname", c_comname);
		mav.addObject("category",category);
		mav.addObject("type",type);
		mav.addObject("bank",bank);
		mav.addObject("state",state);
		map.remove("c_logpw");
		mav.addObject("data",map);
		return mav;
	}
	
	
	/**각 거래처 상세페이지*/
	@RequestMapping(value="customer/employeeCustomerHistoryList/{c_id}",method=RequestMethod.POST)
	public @ResponseBody Object customerHistoryList(ParamMap pmap , @PathVariable String c_id) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		int filtered = 0;
		pmap.getMap().put("cid", c_id);
		List<Map<String,Object>> mList = employeeService.selectCustomerModifyList(pmap.getMap());
		if (mList != null && mList.size() != 0) {
			for (int i = 0; i < mList.size(); i++) {
				Map<String, Object> mapTemp = mList.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String td = "<a id = '"+mapTemp.get("ch_idx")+"' onclick='modifyList()'>"+mapTemp.get("c_modify_memo")+"</a>";
				mapTemp.put("c_modify_memo", td);
				mapTemp.put("ch_idx", "");
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", mList);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", employeeService.selectCustomerModifyListCount(c_id));
		return returnMap;
	}
	
	/**거래처상세페이지 -> 수정 후 업데이트*/
	@RequestMapping(value="customer/customerDetail/{c_id}",method=RequestMethod.POST,produces= "application/text; charset=utf8")
	public @ResponseBody Object customerDetail(ParamMap pmap,MultipartFile c_bankbook,MultipartFile c_file) {
		try {
			if(c_bankbook == null || c_bankbook.equals("")) {
				pmap.remove("c_bankbook");
			}else {
				pmap.put("c_bankbook", c_bankbook.getOriginalFilename());
			}
			
			if(c_file == null || c_file.equals("")) {
				pmap.remove("c_file");
			}else {
				pmap.put("c_file", c_file.getOriginalFilename());
			}
			employeeService.updateCustomer(pmap.getMap(), c_bankbook,c_file);
			return "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록 실패";
		}
	}
	
	/**선택된 거래처 이력 상세페이지*/
	@RequestMapping(value="customer/customerDetail/customerHistory/{ch_idx}_{c_id}",method=RequestMethod.GET)
	public ModelAndView customerHistory(@PathVariable String ch_idx,@PathVariable String c_id) {
		ModelAndView mav = new ModelAndView("employee/customer/customerHistory");
		
		Map<String,Object> cmap = employeeService.selectCustomerInfoDetail(c_id);
		Map<String,Object> map = employeeService.selectCustomerHistory(ch_idx);
		Map<String, Object> pvmap= employeeService.selectCustomerPrvHistory(ch_idx,c_id);
		List<HashMap<String,Object>> category = employeeService.selectCategory();
		
		List<HashMap<String,Object>> type = employeeService.selectType();
		List<HashMap<String,Object>> state = employeeService.selectState();
		List<Map<String,Object>> bank = payService.selectSubBankList();
		
		String regex = ".*[\\[\\][:]\\\\/?[*]].*";
		String c_comname = map.get("c_comname")+"";
		if(c_comname.matches(regex)){  // text에 정규식에 있는 문자가 있다면 true 없다면 false 
			//대괄호는 소괄호로
			c_comname = c_comname.replaceAll("\\[", "\\(");
			c_comname = c_comname.replaceAll("\\]", "\\)");
			//나머지 특수문자는 제거
			c_comname = c_comname.replaceAll("[[:]\\\\/?[*]]", "");  
		}
		map.put("c_comname", c_comname);
		mav.addObject("category",category);
		mav.addObject("type",type);
		mav.addObject("bank",bank);
		mav.addObject("state",state);
		map.remove("c_logpw");
		cmap.remove("c_logpw");
		mav.addObject("hData",map);
		mav.addObject("nData",cmap);
		mav.addObject("oData",pvmap);
		return mav;
	}
}
