package rental.controller;

import java.net.URLEncoder;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rental.common.AesEncrypt;
import rental.common.ParamMap;
import rental.constant.MemberInfo;
import rental.service.SmsService;

@Controller
@RequestMapping("statistic/")
public class StatisticController {
	
	@Autowired
	SmsService smsService;

	@RequestMapping(value="sms",method=RequestMethod.GET)
	public ModelAndView sms() {
		ModelAndView mav=new ModelAndView("statistic/sms");
		return mav;
	}
	
	@RequestMapping(value="sms",method=RequestMethod.POST)
	public @ResponseBody Object smsPost(ParamMap pmap) {
		Map<String,Object> returnMap=new HashMap<String, Object>();
		pmap.printEntrySet();
		List<Map<String,Object>> smsList=smsService.selectSmsHistoryList(pmap.getMap());
		
		int filtered = 0;
		if (smsList != null && smsList.size() != 0) {
			for (int i = 0; i < smsList.size(); i++) {
				Map<String, Object> mapTemp = smsList.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
			}
		}

		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}
		
		if (smsList == null) {
			smsList = new ArrayList<Map<String, Object>>();
		}
		returnMap.put("data", smsList);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", smsService.countSmsHistoryList());
		
		
		return returnMap;
	}
}
