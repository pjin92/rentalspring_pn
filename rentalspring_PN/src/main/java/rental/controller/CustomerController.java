package rental.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sun.mail.smtp.SMTPMessage;

import rental.common.AesEncrypt;
import rental.common.Common;
import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.MemberFile;
import rental.constant.MemberInfo;
import rental.constant.ProductRental;
import rental.constant.Sms;
import rental.service.CmsService;
import rental.service.CommonService;
import rental.service.DeliveryService;
import rental.service.EmployeeService;
import rental.service.InstallService;
import rental.service.MemberService;
import rental.service.PayService;
import rental.service.ProductService;
import rental.service.SmsService;
import rental.service.VaccountService;

@Controller
@RequestMapping("customer/")
public class CustomerController {

	@Autowired
	CommonService commonService;
	@Autowired
	DeliveryService deliveryService;
	@Autowired
	MemberService memberService;
	@Autowired
	PayService payService;
	@Autowired
	ProductService productService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	VaccountService vaccountService;
	@Autowired
	CmsService cmsService;
	@Autowired
	SmsService smsService;
	@Autowired
	InstallService installService;

	@RequestMapping(value = "sangdam", method = RequestMethod.GET)
	public ModelAndView sangdam() {
		ModelAndView mav = new ModelAndView("customer/sangdam");
		//List<HashMap<String, Object>> city = employeeService.selectCity();
		List<HashMap<String, Object>> ds_state = deliveryService.selectds_state();
		// List<HashMap<String,Object>> sc = employeeService.selectSC();

		//mav.addObject("city", city);
		mav.addObject("ds_state", ds_state);
		// 상담상태 list
		mav.addObject("mstates", commonService.mstateList());
		// 접수구분 list
		mav.addObject("mjubsoos", commonService.mjubsooList());
		// 접수상세(매출구분)
		mav.addObject("mdetails", commonService.mdetailList());

		return mav;
	}

	@RequestMapping(value = "sangdam", method = RequestMethod.POST)
	public @ResponseBody Object sangdamPost(ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		try {
			String m_date = (String) pmap.get("m_date");
			if (m_date == null || !m_date.contains("~")) {
				pmap.remove("m_date");
			} else {
				String m_dates[] = m_date.split("~");
				pmap.put("m_date1", m_dates[0].trim());
				pmap.put("m_date2", m_dates[1].trim());
			}

			int e_power = Integer.parseInt(((HashMap) pmap.get("login")).get("e_power").toString());
			int e_id = Integer.parseInt(((HashMap) pmap.get("login")).get("e_id").toString());

			pmap.put("e_power", e_power);
			pmap.put("e_id", e_id);

			List<Map<String, Object>> clist = memberService.customerSangdamList(pmap.getMap());
			int filtered = 0;
			if (clist != null && clist.size() != 0) {
				for (int i = 0; i < clist.size(); i++) {
					Map<String, Object> mapTemp = clist.get(i);
					int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
					if (filtered < rownum) {
						filtered = rownum;
					}

					String m_idx = mapTemp.get("m_idx") + "";
					String app_con_name = mapTemp.get("app_con_name") + "";

					// 선택 체크박스
					mapTemp.put("m_idx", "");
					// 이름 클릭 상세페이지 팝업
					if (app_con_name.equals("")) {
						app_con_name = "(이름없음)";
					}
					String td = "<a class='btn bg-primary' m_idx='" + m_idx + "' onclick='member()'>" + app_con_name
							+ "</a>";
					mapTemp.put("app_con_name", td);
					
					//출금정보팝업
					mapTemp.put("m_num", "<a m_idx='"+m_idx+"' onclick='openPay()'>"+mapTemp.get("m_num")+"</a>");
					
					// 상담배정
					String e_name = (String) mapTemp.get("e_name");
					if (e_name == null || e_name.equals("")) {
						e_name = "배정";
					}
					e_name = "<a m_idx='" + m_idx + "' mn='" + app_con_name + "' onclick='cs()'>" + e_name + "</a>";
					mapTemp.put("e_name", e_name);
					// 삭제 버튼(상담전)
					String del = "";
					if (mapTemp.get("m_state").equals(MemberInfo.STATE_BEFORE)) {
							del = "<a class='btn bg-danger' m_idx='" +m_idx+ "' onclick='del()'>삭제</a>";
					}
					mapTemp.put("del", del);

				}
			}

			try {
				filtered += Integer.parseInt(pmap.get("start") + "");
			} catch (Exception e) {
			}
			if (clist == null) {
				clist = new ArrayList<Map<String, Object>>();
			}
			returnMap.put("data", clist);
			returnMap.put("recordsFiltered", filtered);
			returnMap.put("recordsTotal", memberService.countCustomerSangdamList(pmap.getMap()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnMap;
	}

	@RequestMapping(value = "contract", method = RequestMethod.GET)
	public ModelAndView contract() {
		ModelAndView mav = new ModelAndView("customer/contract");
		
		// 상담상태 list
		mav.addObject("mstates", commonService.mstateList());
		// 접수구분 list
		mav.addObject("mjubsoos", commonService.mjubsooList());
		// 접수상세(매출구분)
		mav.addObject("mdetails", commonService.mdetailList());
		
		//배송상태
		mav.addObject("ds_state", deliveryService.selectds_state());
		
		return mav;
	}

	@RequestMapping(value = "contract", method = RequestMethod.POST)
	public @ResponseBody Object contractPost(ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();

		String m_contract_date = (String) pmap.get("m_contract_date");
		if (m_contract_date == null || !m_contract_date.contains("~")) {
			pmap.remove("m_contract_date");
		} else {
			String m_contract_dates[] = m_contract_date.split("~");
			pmap.put("m_contract_date1", m_contract_dates[0].trim());
			pmap.put("m_contract_date2", m_contract_dates[1].trim());
		}
		
		List<Map<String, Object>> clist = memberService.customerContractList(pmap.getMap());
		int filtered = 0;
		if (clist != null && clist.size() != 0) {
			for (int i = 0; i < clist.size(); i++) {
				Map<String, Object> mapTemp = clist.get(i);

				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String m_idx = mapTemp.get("m_idx") + "";
				String td = "<a m_idx='" + m_idx + "' class='btn bg-primary' onclick='member()'>"
						+ mapTemp.get("mi_name") + "</a>";
				mapTemp.put("mi_name", td);
				mapTemp.put("m_idx", "");

				String app_con_name = mapTemp.get("app_con_name") + "";

				if (app_con_name.equals("")) {
					app_con_name = "(이름없음)";
				}
				String td1 = "<a class='btn bg-primary' m_idx='" + m_idx + "' onclick='member()'>" + app_con_name
						+ "</a>";
				mapTemp.put("app_con_name", td1);
				// 상담배정
				String e_name = (String) mapTemp.get("e_name");
				if (e_name == null || e_name.equals("")) {
					e_name = "배정";
				}
				e_name = "<a m_idx='" + m_idx + "' mn='" + app_con_name + "' onclick='cs()'>" + e_name + "</a>";
				mapTemp.put("e_name", e_name);
				// 주소
				String addr = mapTemp.get("mi_addr1") + "";
				addr += " " + mapTemp.get("mi_addr2");
				addr += " " + mapTemp.get("mi_addr3");
				mapTemp.put("mi_addr", addr);

				String rental = "<a class='btn bg-green' m_idx='" + m_idx + "' onclick='rental()'>렌탈약정서</a>";
				String yangdo = "<a class='btn bg-green-700' m_idx='" + m_idx + "' onclick='yangdo()'>양도통지서</a>";

				mapTemp.put("rental", rental);
				mapTemp.put("yangdo", yangdo);
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}
		if (clist == null) {
			clist = new ArrayList<Map<String, Object>>();
		}
		returnMap.put("data", clist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", memberService.countCustomerContractList(pmap.getMap()));
		return returnMap;
	}

	@RequestMapping(value = "contract/{m_idx}/", method = RequestMethod.GET)
	public ModelAndView contractDetail(@PathVariable String m_idx) {
		ModelAndView mav = new ModelAndView("customer/contractDetail");

		int m = Integer.parseInt(m_idx);
		Map<String, Object> member = memberService.selectMemberDetail(m);
		DecimalFormat df = new DecimalFormat("#,###,###,###,###");
		member.put("m_rental", df.format(Integer.parseInt(member.get("m_rental") + "")));
		member.put("m_total", df.format(Integer.parseInt(member.get("m_total") + "")));
		member.put("m_ilsi", df.format(Integer.parseInt(member.get("m_ilsi") + "")));
		
		String m_paydate = member.get("m_paydate")+"";
		String m_paystart=member.get("m_paystart")+"-"+m_paydate;
		String m_period=member.get("m_period")+"";
		m_paystart=m_paystart.replaceAll("[^0-9]", "");
		m_period=m_period.replaceAll("[^0-9]", "");
		if(!m_paystart.equals("")&&!m_period.equals("")) {
			try{
				SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
				Calendar now=Calendar.getInstance();
				now.setTime(sdf.parse(m_paystart));
				now.add(Calendar.MONTH, Integer.parseInt(m_period)-1);
				member.put("m_pay_finish",sdf2.format(now.getTime()).substring(0, 7));
			}catch (Exception eee) {}
		}
		
		mav.addObject("m", member);
		mav.addObject("mi_apply", memberService.selectMemberInfo(m, MemberInfo.TYPE_APPLY).get(0));
		String pr_idx = member.get("m_pr_idx") + "";

		HashMap<String, Object> product = productService.selectProductRentalDetail(pr_idx);

		String td = "<a pr_idx='" + product.get("pr_idx") + "' onclick='rproductDatail()'>" + product.get("pr_name")
				+ "</a>";
		product.put("pr_name", td);
		mav.addObject("product", product);

		mav.addObject("mg", memberService.selectMemberGroupDetail(m_idx));

		// 상담CS 정보
		String m_damdang = (String) member.get("m_damdang");
		if (m_damdang != null && !m_damdang.equals("")) {
			mav.addObject("m_damdang", employeeService.selectEmployeeDetail(m_damdang));
		}

		// 2 상담메모
		mav.addObject("art2_2", memberService.selectMemberFileList(m_idx, MemberFile.NAME_SANGDAM_MEMO));
		// 3_1배송고객
		List<Map<String, Object>> mi_delivery = memberService.selectMemberInfo(m, MemberInfo.TYPE_DELIVERY);
		if (mi_delivery != null && mi_delivery.size() > 0) {
			mav.addObject("mi_delivery", mi_delivery.get(0));
		}
		// 3_2배송정보
		List<Map<String, Object>> ds_list = deliveryService.selectDeliveryState(m);
		if (mi_delivery != null && mi_delivery.size() > 0) {
			Map<String, Object> ds = ds_list.get(0);
			mav.addObject("ds", ds);

			String ds_date = (String) ds.get("ds_date");
			if (ds_date != null && !ds_date.equals("")) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Calendar dsdate = Calendar.getInstance();
					mav.addObject("today", sdf.format(dsdate.getTime()));//위약금계산에서 사용
					dsdate.setTime(sdf.parse(ds_date));
					dsdate.add(Calendar.DATE, 14);

					Calendar now = Calendar.getInstance();
					String ds_req_cancel = (String) ds.get("ds_req_cancel");
					if (ds_req_cancel != null && !ds_req_cancel.equals("")) {
						now.setTime(sdf.parse(ds_req_cancel));
					}
					if (now.before(dsdate)) {
						// 배송완료일 2주 이내
						mav.addObject("twoWeeks", "before");
					} else {
						// 배송완료일 2주 이후
						mav.addObject("twoWeeks", "after");
					}

				} catch (Exception ed) {
				}
			} else {
				mav.addObject("twoWeeks", "none");
			}
		}

		// 4계약고객
		List<Map<String, Object>> mi_contract = memberService.selectMemberInfo(m, MemberInfo.TYPE_CONTRACT);
		if (mi_contract != null && mi_contract.size() > 0) {
			mav.addObject("mi_contract", mi_contract.get(0));
		}
		// 첨부파일
		mav.addObject("records", memberService.selectMemberFileList(m_idx, MemberFile.NAME_RECORD));
		mav.addObject("credits", memberService.selectMemberFileList(m_idx, MemberFile.NAME_CREDIT_INQUIRY));
		mav.addObject("agree", memberService.selectMemberFileList(m_idx, MemberFile.NAME_AGREEMENT_CERTFICATE));
		mav.addObject("transfer", memberService.selectMemberFileList(m_idx, MemberFile.NAME_TRANSFER_NOTICE));

		// 6.가상계좌 정보
		Map<String, Object> vMap = vaccountService.selectVdetail(m, null);
		if (vMap == null) {
			vMap = new HashMap<String, Object>();
			mav.addObject("v_bank", "");
			mav.addObject("v_condition", "미등록");
		}

		mav.addObject("unpaid", payService.countUnpaid(m));
		mav.addObject("vaccount", vMap);
		mav.addObject("vaccountBank", payService.selectSubBankList());
		mav.addObject("cmsList", cmsService.selectCmsList(null));

		return mav;
	}

	/** 가상계좌 정보 조회 */
	@RequestMapping(value = "contract/{m_idx}/vAccount", method = RequestMethod.GET)
	public ModelAndView vAccountGet(@PathVariable String m_idx, String v_bank) {
		ModelAndView mav = new ModelAndView("customer/comp/artV");
		int m = Integer.parseInt(m_idx);
		mav.addObject("m", memberService.selectMemberDetail(m));

		Map<String, Object> vMap = vaccountService.selectVdetail(m, v_bank);
		if (vMap == null) {
			vMap = new HashMap<String, Object>();
			vMap.put("v_condition", "미등록");

			Calendar now = Calendar.getInstance();
			now.add(Calendar.DATE, 7);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			vMap.put("v_end_date", sdf.format(now.getTime()));

			Map<String, Object> member = memberService.selectMemberDetail(m);
			DecimalFormat df = new DecimalFormat("#,###,###,###,###");
			vMap.put("v_money", df.format(Integer.parseInt(member.get("m_rental") + "")));
			vMap.put("v_name", member.get("m_pay_owner"));
			vMap.put("v_bank", v_bank);
			vMap.remove("v_cms_idx");
		} else {

		}

		mav.addObject("vaccount", vMap);
		int cntUnpaid=payService.countUnpaid(m);
		if(cntUnpaid==0)cntUnpaid=10;
		mav.addObject("unpaid", cntUnpaid);
		
		mav.addObject("vaccountBank", payService.selectSubBankList());
		if (v_bank == null) {
			v_bank = "";
		}
		mav.addObject("v_bank", v_bank);
		mav.addObject("cmsList", cmsService.selectCmsList(null));
		return mav;
	}

	/** 가상계좌 등록 */
	@RequestMapping(value = "contract/{m_idx}/vAccount", method = RequestMethod.POST)
	public @ResponseBody Object vAccountPost(@PathVariable String m_idx, ParamMap pmap) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String v_bank = (String) pmap.get("v_bank");
		String msg = vaccountService.regiVaccount(Integer.parseInt(m_idx), pmap.getMap());
		resultMap.put("msg", msg);
		resultMap.put("v_bank", v_bank);

		return resultMap;
	}

	/** 가상계좌 삭제 */
	@RequestMapping(value = "contract/{m_idx}/vAccount", method = RequestMethod.DELETE)
	public @ResponseBody Object vAccountDelete(@PathVariable String m_idx, ParamMap pmap) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String v_bank = (String) pmap.get("v_bank");
		String msg = vaccountService.delVaccount(Integer.parseInt(m_idx), pmap.getMap());
		resultMap.put("msg", msg);
		resultMap.put("v_bank", v_bank);

		return resultMap;
	}

	/** 가상계좌 문자 세팅 */
	@RequestMapping(value = "contract/{m_idx}/smsV", method = RequestMethod.GET)
	public @ResponseBody Object smsV(@PathVariable String m_idx, String v_bank) {
		String msg;
		int m = Integer.parseInt(m_idx);
		Map<String, Object> vMap = vaccountService.selectVdetail(m, v_bank);
		if (vMap == null) {
			vMap = new HashMap<String, Object>();
			msg = "null";
		} else {
			DecimalFormat df = new DecimalFormat("#,###,###,###");

			Map<String, Object> member = memberService.selectMemberDetail(m);
			vMap.put("phone", member.get("m_phone"));

			String pr_sms = (String) member.get("pr_sms");
			if (pr_sms == null || pr_sms.equals("")) {
				pr_sms = "1522-7557";// led
			}
			vMap.put("send", pr_sms);
			msg = member.get("m_pay_owner") + "고객님\n" + df.format(vMap.get("v_money")) + "원\n" + vMap.get("sb_name")
					+ " " + vMap.get("v_account") + " 입금 부탁드립니다.";
		}
		vMap.put("msg", msg);
		return vMap;
	}

	/** 가상계좌 문자 전송 */
	@RequestMapping(value = "contract/{m_idx}/smsV", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object smsV(@PathVariable String m_idx, ParamMap pmap) {
		Map<String, Object> login = (Map<String, Object>) pmap.get("login");
		String sms_to = pmap.get("sms_to") + "";
		sms_to = sms_to.replaceAll("[^0-9]", "");
		String sms_from = pmap.get("sms_from") + "";
		sms_from = sms_from.replaceAll("[^0-9]", "");
		String sms_msg = pmap.get("sms_content") + "";
		sms_msg = sms_msg.replaceAll("<br>", System.getProperty("line.separator"));

		String msg = smsService.sendSms(Sms.TYPE_VACCOUNT, Integer.parseInt(login.get("e_id") + ""),
				Integer.parseInt(m_idx), sms_to, sms_from, sms_msg);
		return msg;
	}

	/** 상담관리>계약고객>8 파일업로드 */
	@RequestMapping(value = "contract/{m_idx}/8", method = RequestMethod.POST)
	public @ResponseBody Object customerContract8(@PathVariable String m_idx, String type, MultipartFile file) {
		String msg = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			int midx = Integer.parseInt(m_idx);
			String mf_name = "";
			if (type.equals("rental")) {
				mf_name = MemberFile.NAME_AGREEMENT_CERTFICATE;
			} else if (type.equals("notice")) {
				mf_name = MemberFile.NAME_TRANSFER_NOTICE;
			}else if (type.equals("credit")) {
				mf_name = MemberFile.NAME_CREDIT_INQUIRY;
			} else {
				map.put("msg", "잘못된 접근입니다.");
				return map;
			}

			map.put("m_idx", midx);
			map.put("mf_name", mf_name);
			map.put("mf_file", file.getOriginalFilename());
			msg = memberService.insertMemberFile(map, file);
			if (msg.equals("업로드 완료")) {
				map.put("result", "success");
			} else {
				map.put("result", "fail");
			}
		} catch (Exception e) {
			map.put("result", "fail");
			msg = "오류 발생.\n다시 시도해주세요";
		}
		map.put("msg", msg);
		return map;
	}

	// 계약고객 3_2 배송정보 refresh
	@RequestMapping(value = "{m_idx}/delivery_state", method = RequestMethod.GET)
	public ModelAndView customerDeliveryState(@PathVariable String m_idx) {
		ModelAndView mav = new ModelAndView("customer/contractDetailDeliveryState");
		int m = Integer.parseInt(m_idx);

		List<Map<String, Object>> mi_delivery = memberService.selectMemberInfo(m, MemberInfo.TYPE_DELIVERY);
		// 3_2배송정보
		List<Map<String, Object>> ds_list = deliveryService.selectDeliveryState(m);
		if (mi_delivery != null && mi_delivery.size() > 0) {
			Map<String, Object> ds = ds_list.get(0);
			mav.addObject("ds", ds);

			String ds_date = (String) ds.get("ds_date");
			if (ds_date != null && !ds_date.equals("")) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Calendar dsdate = Calendar.getInstance();
					dsdate.setTime(sdf.parse(ds_date));
					dsdate.add(Calendar.DATE, 14);

					Calendar now = Calendar.getInstance();
					String ds_req_cancel = (String) ds.get("ds_req_cancel");
					if (ds_req_cancel != null && !ds_req_cancel.equals("")) {
						now.setTime(sdf.parse(ds_req_cancel));
					}

					if (now.before(dsdate)) {
						// 배송완료일 2주 이내
						mav.addObject("twoWeeks", "before");
					} else {
						// 배송완료일 2주 이후
						mav.addObject("twoWeeks", "after");
					}
				} catch (Exception ed) {
				}
			} else {
				mav.addObject("twoWeeks", "none");
			}
		}
		// member정보
		mav.addObject("m", memberService.selectMemberDetail(m));

		return mav;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView customerGet() {
		ModelAndView mav = new ModelAndView("customer/customer");
		return mav;
	}

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customerPost(ParamMap pmap) {
		String result = "";
		try {
			String mi_name = (String) pmap.get("mi_name");
			if (mi_name == null || mi_name.equals("")) {
				return "상담 고객명을 입력해주세요";
			}
			result = memberService.insertMember(pmap.getMap());
		} catch (DuplicateKeyException dke) {
			result = "고객고유번호 생성 중 에러 발생.\n관리자에게 연락하세요.";
		} catch (Exception e) {
			e.printStackTrace();
			result = "등록 중 에러 발생.";
		}
		return result + "";
	}

	@RequestMapping(value = "mdetailList", method = RequestMethod.GET)
	public @ResponseBody Object mdetailList(String m_jubsoo) {
		return commonService.mdetailList();
	}

	// 상담전인 고객만 삭제
	@RequestMapping(value = "{m_idx}/", method = RequestMethod.DELETE, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customerDelete(@PathVariable String m_idx) {
		String msg = "";
		try {
			int midx = Integer.parseInt(m_idx);
			Map<String, Object> member = memberService.selectMemberDetail(midx);
			if (!member.get("m_state").equals(MemberInfo.STATE_BEFORE)) {
				return "상담전 상태의 고객만 삭제 가능합니다.";
			}
			if (memberService.deleteMember(midx)) {
				msg = "삭제하였습니다.";
			} else {
				msg = "잠시 후 다시 시도해주세요.";
			}

		} catch (Exception e) {
			e.printStackTrace();
			msg = "오류 발생.\n다시 시도해주세요.";
		}

		return msg;
	}

	@RequestMapping(value = "{m_idx}/", method = RequestMethod.GET)
	public ModelAndView customerGet(@PathVariable String m_idx) {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String now_str = sdf.format(now.getTime());

		ModelAndView mav = new ModelAndView("customer/customerDetail");
		int m = Integer.parseInt(m_idx);
		Map<String, Object> member = memberService.selectMemberDetail(m);
		DecimalFormat df = new DecimalFormat("#,###,###,###,###");
		member.put("m_rental", df.format(Integer.parseInt(member.get("m_rental") + "")));
		member.put("m_total", df.format(Integer.parseInt(member.get("m_total") + "")));
		member.put("m_ilsi", df.format(Integer.parseInt(member.get("m_ilsi") + "")));
		mav.addObject("m", member);
		mav.addObject("mi_apply", memberService.selectMemberInfo(m, MemberInfo.TYPE_APPLY).get(0));
		String pr_idx = member.get("m_pr_idx") + "";
		mav.addObject("product", productService.selectProductRentalDetail(pr_idx));
		mav.addObject("mg", memberService.selectMemberGroupDetail(m_idx));

		// 상담CS 정보
		String m_damdang = (String) member.get("m_damdang");
		if (m_damdang != null && !m_damdang.equals("")) {
			mav.addObject("m_damdang", employeeService.selectEmployeeDetail(m_damdang));
		}
		// 상담상태 list
		mav.addObject("mstates", commonService.mstateList());
		// 접수구분 list
		mav.addObject("mjubsoos", commonService.mjubsooList());
		String m_jubsoo = member.get("m_jubsoo") + "";
		// 접수상세(매출구분)
		mav.addObject("mdetails", commonService.mdetailList());

		// 2_2 상담메모
		mav.addObject("art2_2", memberService.selectMemberFileList(m_idx, MemberFile.NAME_SANGDAM_MEMO));
		// 3계약고객
		List<Map<String, Object>> mi_contract = memberService.selectMemberInfo(m, MemberInfo.TYPE_CONTRACT);
		if (mi_contract != null && mi_contract.size() > 0) {
			mav.addObject("mi_contract", mi_contract.get(0));
		}
		// 4배송고객
		List<Map<String, Object>> mi_delivery = memberService.selectMemberInfo(m, MemberInfo.TYPE_DELIVERY);
		if (mi_delivery != null && mi_delivery.size() > 0) {
			mav.addObject("mi_delivery", mi_delivery.get(0));
		}

		// 결제 정보
		String m_pay_method = member.get("m_pay_method") + "";
		mav.addObject("bank_methods", payService.selectSubBankList());
		mav.addObject("card_methods", payService.selectSubCardList());

		// 첨부파일
		mav.addObject("records", memberService.selectMemberFileList(m_idx, MemberFile.NAME_RECORD));
		mav.addObject("credits", memberService.selectMemberFileList(m_idx, MemberFile.NAME_CREDIT_INQUIRY));
		mav.addObject("agree", memberService.selectMemberFileList(m_idx, MemberFile.NAME_AGREEMENT_CERTFICATE));
		mav.addObject("transfer", memberService.selectMemberFileList(m_idx, MemberFile.NAME_TRANSFER_NOTICE));
		mav.addObject("installFile", memberService.selectMemberFileList(m_idx, MemberFile.NAME_INSTALL));
		mav.addObject("installRemove", memberService.selectMemberFileList(m_idx, MemberFile.NAME_INSTALL_REMOVE));
		return mav;
	}

	/** 상담관리>신청 고객정보>렌탈 상품 변경 */
	@RequestMapping(value = "{midx}/product", method = RequestMethod.PUT)
	public @ResponseBody Object changeProduct(@PathVariable String midx, ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		String msg;
		boolean reload = false;
		int m_idx = Integer.parseInt(midx);
		Map<String, Object> member = memberService.selectMemberDetail(m_idx);
		String m_contract_date = member.get("m_contract_date") + "";
		String m_install_finish_date = member.get("m_install_finish_date") + "";

		// 배송전,계약완료전에만 변경가능
		if (m_contract_date.equals("") && m_install_finish_date.equals("")) {
			int mi_idx;
			// 배송자 정보 조회,없으면 insert
			List<Map<String, Object>> mi_del = memberService.selectMemberInfo(m_idx, MemberInfo.TYPE_DELIVERY);
			if (mi_del == null || mi_del.isEmpty()) {
				// mi insert
				Map<String, Object> map_mi = new HashMap<String, Object>();
				map_mi.put("mi_id_type", MemberInfo.ID_TYPE_PERSON);
				map_mi.put("mi_id", "");
				map_mi.put("mi_name", "");
				map_mi.put("mi_tel", "");
				map_mi.put("mi_phone", "");
				map_mi.put("mi_addr1", "");
				map_mi.put("mi_addr2", "");
				map_mi.put("mi_addr3", "");
				map_mi.put("mi_post", "");
				map_mi.put("mi_memo", "");

				mi_idx = memberService.insertMemberInfo(m_idx, MemberInfo.TYPE_DELIVERY, pmap.getMap());
			} else {
				mi_idx = Integer.parseInt(mi_del.get(0).get("mi_idx") + "");
			}

			int pr_idx = Integer.parseInt(pmap.get("pr_idx") + "");
			memberService.changeProduct(pr_idx, mi_idx, m_idx);

			msg = "렌탈 상품이 변경되었습니다\n월렌탈료 확인 부탁드립니다.";
			reload = true;
		} else {
			msg = "계약완료된 고객입니다.\n상품변경이 불가능합니다.\n\n계약완료일,배송/설치완료일 확인 부탁드립니다.";
		}

		returnMap.put("msg", msg);
		returnMap.put("reload", reload);

		return returnMap;
	}

	/** 상담관리>고객정보>1.신청고객정보 수정 */
	@RequestMapping(value = "{m_idx}/1", method = RequestMethod.PUT, produces = "text/plain;charset=utf8")
	public @ResponseBody Object updateApply(@PathVariable String m_idx, ParamMap pmap) {
		try {
			int i_midx = Integer.parseInt(m_idx);
			String type = MemberInfo.TYPE_APPLY;
			// 판매처
			Map<String, Object> hm = new HashMap<String, Object>();
			hm.put("m_idx", i_midx);
			hm.put("m_sale", pmap.get("m_sale") + "");
			memberService.updateMember(hm);

			List<Map<String, Object>> list = memberService.selectMemberInfo(i_midx, type);
			int misize = list.size();
			if (misize < 1) {
				// mi insert
				memberService.insertMemberInfo(i_midx, type, pmap.getMap());
			} else {
				// mi update
				String str_mi = list.get(0).get("mi_idx") + "";
				pmap.put("mi_idx", Integer.parseInt(str_mi));
				int result = memberService.updateMemberInfo(pmap.getMap());
				if (result < 0) {
					return "오류발생";
				}
			}
			// mg_name,mgm_cnt
			String mg_name = pmap.get("mg_name") + "";
			String mgm_cnt = pmap.get("mgm_cnt") + "";
			int result = memberService.updateMemberGroupMember(m_idx, mg_name, mgm_cnt);
			if (result < 0) {
				return "판매 기수 저장 중 오류발생";
			}

			return "저장 완료했습니다.";
		} catch (Exception e) {
			Log.warn(" " + pmap.entrySet());
			return "오류 발생.\n다시 시도해주세요";
		}
	}

	/** 상담관리>고객정보>2_1 해피콜 구분,상태 정보 저장 */
	@RequestMapping(value = "{m_idx}/2_1", method = RequestMethod.PUT, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customer2_1(@PathVariable String m_idx, ParamMap pmap) {

		// System.out.println(pmap.entrySet());
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("m_idx", m_idx);
			String m_state=pmap.get("m_state")+"";
			map.put("m_state", m_state);
			String m_jubsoo=pmap.get("m_jubsoo")+"";
			map.put("m_jubsoo", m_jubsoo);
			String m_detail = (String) pmap.get("m_detail");
			map.put("m_detail", m_detail);

			// 매출구분에 따른 진행상태 변경
			int midx = Integer.parseInt(m_idx);
			Map<String, Object> map_member = memberService.selectMemberDetail(midx);
			String m_process = map_member.get("m_process") + "";
			String m_process_set = "";
			// 확장성위해 list로 받아옴. 현재는 배송자정보가 1명이기 때문에 첫번째 Map가져옴
			List<Map<String, Object>> dlist = deliveryService.selectDeliveryState(midx);
			
			if(m_state.equals(MemberInfo.STATE_FINISH)){
				String m_jubsoo_db=map_member.get("m_jubsoo")+"";
				String m_state_db=map_member.get("m_state")+"";
				
				if(m_process.equals(MemberInfo.PROCESS_VISITED)) {
					//선설치 후 상담완료 (방문완료,설치요청) + 무료체험
					if (dlist == null || dlist.isEmpty() || dlist.size() == 0) {
						
					} else {
						Map<String,Object>dsMap=dlist.get(0);
						String ds_state = dsMap.get("ds_state") + "";
						if(ds_state.equals(Delivery.STATE_INSTALLED)||ds_state.equals(Delivery.STATE_DELIVERED)){
							if (m_detail.equals(MemberInfo.DETAIL_ILSI)) {
								//일시불일 때
								m_process_set = MemberInfo.PROCESS_ILSI;
							}else {
								m_process_set = MemberInfo.PROCESS_RENT;
							}
							
						}
					}
					
				}else if(m_process.equals(MemberInfo.PROCESS_JOONGDO)||m_process.equals(MemberInfo.PROCESS_ILSI_FINISH)||m_process.equals(MemberInfo.PROCESS_RENT_FINISH)){
					//중도상환, 렌탈완료,일시불완료인경우 pass
				}else if(m_process.equals(MemberInfo.PROCESS_TEST)){
					//체험 중일 때
				}else if (m_detail.equals(MemberInfo.DETAIL_ILSI)) {
					//일시불일 때
					m_process_set = MemberInfo.PROCESS_ILSI;
				}else {
					if (dlist == null || dlist.isEmpty() || dlist.size() == 0) {
						// 배송자 정보 입력이 안된경우(배송전)
						m_process_set = MemberInfo.PROCESS_BEFORE_RENT;
					} else {
						Map<String,Object>dsMap=dlist.get(0);
						String ds_state = dsMap.get("ds_state") + "";
						String ds_date=dsMap.get("ds_date")+"";
						if (ds_state.equals(Delivery.STATE_BEFORE_DELIVERY)
								|| ds_state.equals(Delivery.STATE_REQUEST_DELIVERY)
								|| ds_state.equals(Delivery.STATE_BEFORE_INSTALL)
								|| ds_state.equals(Delivery.STATE_REQUEST_INSTALL)) {
							
							m_process_set = MemberInfo.PROCESS_BEFORE_RENT;
							
						} else if(ds_state.equals(Delivery.STATE_INSTALLED)||ds_state.equals(Delivery.STATE_DELIVERED)){
							
//							if(m_jubsoo.equals(MemberInfo.JUBSOO_FREE_EXPERIENCE)&&!ds_date.equals("")){
//								//무료체험일 때
//								SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//								Calendar checkFree=Calendar.getInstance();
//								
//								checkFree.setTime(sdf.parse(ds_date));
//								String m_free_day=map_member.get("m_free_day")+"";
//								m_free_day=m_free_day.replaceAll("[^0-9]", "");
//								if(m_free_day.equals(""))m_free_day="0";
//								int i_m_free=Integer.parseInt(m_free_day);
//								checkFree.add(Calendar.DATE, i_m_free);
//								
//								Calendar now=Calendar.getInstance();
//								
//								if(now.before(checkFree)) {
//									m_process_set=MemberInfo.PROCESS_TEST;
//								}else {
//									m_process_set=MemberInfo.PROCESS_RENT;
//								}
//							}else {
								//무료체험 아닐 때. 무료체험은 기사어플에서 처리
								m_process_set=MemberInfo.PROCESS_RENT;
//							}
						}else if(ds_state.equals(Delivery.STATE_EXIT)){
							m_process_set=MemberInfo.PROCESS_EXIT;
						}else if(ds_state.equals(Delivery.STATE_HAEBAN)){
							m_process_set=MemberInfo.PROCESS_HAEBAN;
						}else if(ds_state.equals(Delivery.STATE_RETURN)){
							m_process_set=MemberInfo.PROCESS_RETURN;
						}
					}
					
				}
			}
			
			if(!m_process_set.equals("")) {
				map.put("m_process", m_process_set);
			}

			// 상담완료일
			if (pmap.get("m_state").equals(MemberInfo.STATE_FINISH)
					&& !map_member.get("m_state").equals(MemberInfo.STATE_FINISH)) {
				Calendar now = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				map.put("m_sangdam_finish", sdf.format(now.getTime()));
			}

			int result = memberService.updateMember(map);
			if (result > 0) {
				return "저장 완료했습니다.";
			} else {
				return "업데이트 실패.\n다시 시도해주세요.";
			}
		} catch (Exception e) {
			Log.warn(" " + e.toString() + pmap.entrySet());
			return "오류 발생.\n다시 시도해주세요";
		}
	}

	/** 상담관리>고객정보>2_2 상담 메모 저장 */
	@RequestMapping(value = "{m_idx}/2_2", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customer2_2(@PathVariable String m_idx, ParamMap pmap) {
		try {
			String[] mf_log;
			String[] mf_memo;
			if ((pmap.get("mf_log") instanceof String[])) {
				mf_log = (String[]) pmap.get("mf_log");
				mf_memo = (String[]) pmap.get("mf_memo");
			} else {
				String mflog = (String) pmap.get("mf_log");
				if (mflog == null) {
					memberService.updateSangdamMemo(m_idx, null, null);
					return "저장 완료했습니다.";
				}
				mf_log = new String[1];
				mf_memo = new String[1];
				mf_log[0] = mflog;
				mf_memo[0] = pmap.get("mf_memo") + "";
			}
			memberService.updateSangdamMemo(m_idx, mf_log, mf_memo);
			return "저장 완료했습니다.";
		} catch (Exception e) {
			Log.warn(" " + e.toString() + pmap.entrySet());
			return "오류 발생.\n다시 시도해주세요";
		}
	}

	/** m_idx에 따른 mi 리스트 */
	@RequestMapping(value = "{m_idx}/milist", method = RequestMethod.GET)
	public ModelAndView miList(@PathVariable String m_idx) {
		ModelAndView mav = new ModelAndView("customer/modal/memberInfoList");
		int midx = Integer.parseInt(m_idx);
		mav.addObject("milist", memberService.selectMemberInfo(midx, null));
		return mav;
	}

	/** 상담관리>고객정보>3 계약고객정보 */
	@RequestMapping(value = "{m_idx}/3", method = RequestMethod.PUT, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customer3(@PathVariable String m_idx, ParamMap pmap) {
		String msg = "";
		try {
			int midx = Integer.parseInt(m_idx);
			// member 저장
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("m_idx", midx);
			map.put("m_contract_date", pmap.get("m_contract_date"));
			map.put("m_cb", pmap.get("m_cb"));
			int memup = memberService.updateMember(map);
			if (memup > 0) {
				// member_info 저장
				List<Map<String, Object>> milist = memberService.selectMemberInfo(midx, MemberInfo.TYPE_CONTRACT);
				if (milist == null || milist.size() == 0) {
					// mi insert
					memberService.insertMemberInfo(midx, MemberInfo.TYPE_CONTRACT, pmap.getMap());
				} else {
					// mi update
					String str_mi = milist.get(0).get("mi_idx") + "";
					pmap.put("mi_idx", Integer.parseInt(str_mi));
					int result = memberService.updateMemberInfo(pmap.getMap());
					if (result < 0) {
						return "오류발생";
					}
				}
				msg = "저장 완료.";
			} else {
				msg = "저장 중 오류 발생.";
			}
		} catch (Exception e) {
			Log.warn(" " + e.toString() + pmap.entrySet());
			return "오류 발생.\n다시 시도해주세요";
		}
		return msg;
	}

	/** 상담관리>고객정보>4 배송고객정보. transaction처리 안됨(개별배송 확장성) */
	@RequestMapping(value = "{m_idx}/4", method = RequestMethod.PUT, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customer4(@PathVariable String m_idx, ParamMap pmap) {
		String msg = "";
		try {
			int midx = Integer.parseInt(m_idx);

			// member_info 저장
			List<Map<String, Object>> milist = memberService.selectMemberInfo(midx, MemberInfo.TYPE_DELIVERY);
			if (milist == null || milist.size() == 0) {
				// mi insert
				pmap.put("mi_id_type", MemberInfo.ID_TYPE_PERSON);
				pmap.put("mi_id", "");
				int mi_idx = memberService.insertMemberInfo(midx, MemberInfo.TYPE_DELIVERY, pmap.getMap());

				//// 확장성 위해 추가한 코드. 개별배송 사용시 변경해야할 코드( ds insert 후 delivery prp_idx만큼 반복생성
				Map<String, Object> member = memberService.selectMemberDetail(midx);
				int pr_idx = Integer.parseInt(member.get("m_pr_idx") + "");
				Map<String, Object> pr = productService.selectProductRentalDetail(pr_idx + "");
				int pr_type = Integer.parseInt(pr.get("pr_type") + "");
				String ds_state = "";
				if (pr_type == ProductRental.TYPE_INSTALL) {
					ds_state = Delivery.STATE_BEFORE_INSTALL;
				} else {
					ds_state = Delivery.STATE_BEFORE_DELIVERY;
				}
				int ds_idx = deliveryService.insertDeliveryState(ds_state, Delivery.HISTORY_BASIC);
				List<HashMap<String, Object>> prplist = productService.selectRentalComDetail2(pr_idx);
				for (int i = 0; i < prplist.size(); i++) {
					int prp_idx = (Integer) prplist.get(i).get("prp_idx");
					deliveryService.insertDelivery(mi_idx, prp_idx, ds_idx);
				}
			} else {
				// mi update

				// 개별 배송인 경우 수정필요
				String str_mi = milist.get(0).get("mi_idx") + "";
				//

				pmap.put("mi_idx", Integer.parseInt(str_mi));
				int result = memberService.updateMemberInfo(pmap.getMap());
				if (result < 0) {
					return "오류발생";
				}
			}

			msg = "저장 완료.";
		} catch (Exception e) {
			Log.warn(" " + e.toString() + pmap.entrySet());
			return "오류 발생.\n다시 시도해주세요";
		}
		return msg;
	}

	/** 상담관리>고객정보>5 결제정보. */
	@RequestMapping(value = "{m_idx}/5", method = RequestMethod.PUT, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customer5(@PathVariable String m_idx, ParamMap pmap) {
		int midx = Integer.parseInt(m_idx);
		
		// 이체 희망일
		try {
			String str_m_period = pmap.get("m_period") + "";
			str_m_period = str_m_period.replaceAll("[^0-9]", "");
			if (str_m_period.equals(""))str_m_period = "0";
			pmap.put("m_period", Integer.parseInt(str_m_period));
			
			String str_m_rental = pmap.get("m_rental") + "";
			str_m_rental = str_m_rental.replaceAll("[^0-9]", "");
			if (str_m_rental.equals(""))str_m_rental = "0";
			pmap.put("m_rental", Integer.parseInt(str_m_rental));
			
			String str_m_total = pmap.get("m_total") + "";
			str_m_total = str_m_total.replaceAll("[^0-9]", "");
			if (str_m_total.equals(""))str_m_total = "0";
			pmap.put("m_total", Integer.parseInt(str_m_total));
			
			String str_m_ilsi = pmap.get("m_ilsi") + "";
			str_m_ilsi = str_m_ilsi.replaceAll("[^0-9]", "");
			if (str_m_ilsi.equals(""))str_m_ilsi = "0";
			pmap.put("m_ilsi", Integer.parseInt(str_m_ilsi));

			String mpd = pmap.get("m_paydate") + "";
			if (mpd == null || mpd.equals("")) {
				mpd = "01";
			}
			int m_paydate = Integer.parseInt(mpd);
			if (m_paydate < 1 || m_paydate > 31) {
				return "1~31사이의 숫자를 입력해주세요.";
			}
			mpd = Common.dateStr(m_paydate + "");
			pmap.put("m_paydate", mpd);

			pmap.put("m_pay_num", (pmap.get("m_pay_num") + "").replaceAll("[^0-9]", ""));
			
			
			payService.updatePayOriginalDate(midx,mpd);
			
		} catch (Exception e) {
			return "이체 희망일 확인 부탁드립니다.";
		}
		String m_phone = pmap.get("m_phone") + "";
		m_phone = m_phone.replaceAll("[^0-9]", "");
		pmap.put("m_phone", m_phone);

		String msg = "";
		try {
			
			pmap.put("m_idx", midx);
			// cms상태 변경
			Map<String, Object> member = memberService.selectMemberDetail(midx);
			if (!member.get("m_final_date").equals("")) {
				//System.out.println("최종계약 확정된 건은 렌탈기간 등 변경 불가 하도록...");
				pmap.remove("m_period");
				pmap.remove("m_rental");
				pmap.remove("m_total");
			}
			String m_cms = member.get("m_cms") + "";
			String pay_method_prev = member.get("m_pay_method") + "";
			String pay_method_now = pmap.get("m_pay_method") + "";

			// 카드명,은행명 select로 따로 있어서 결제방식에 따라 m_pay_info 데이터 입력

			if (pay_method_now.equals(MemberInfo.PAY_METHOD_BANK)) {
				member.put("m_card_year", "");
				member.put("m_card_month", "");
				pmap.put("m_card_month", "");
				pmap.put("m_card_month", "");

				pmap.put("m_pay_info", pmap.get("m_pay_info_bank"));
			} else if(pay_method_now.equals(MemberInfo.PAY_METHOD_CARD)){
				pmap.put("m_pay_info", pmap.get("m_pay_info_card"));
			}

			if (m_cms.equals(MemberInfo.CMS_UNREGISTERD)) {
				// 결제정보 업데이트(cms는 미등록)
				memberService.updateMember(pmap.getMap());
			} else {
				// cms가 미등록이 아닌경우.m_cms상태
				if (pay_method_now.equals(pay_method_prev)) {
					// 결제방식 그대로
					// 결제 정보가 변경되었는지 확인
					String m_pay_info = pmap.get("m_pay_info") + "";// 카드명,은행명
					String m_pay_owner = pmap.get("m_pay_owner") + "";
					String pm_pay_id = pmap.get("m_pay_id") + "";
					String m_pay_num = pmap.get("m_pay_num") + "";
					String m_card_year = pmap.get("m_card_year") + "";
					String m_card_month = pmap.get("m_card_month") + "";
					if (m_pay_info.equals(member.get("m_pay_info")) && m_pay_owner.equals(member.get("m_pay_owner"))
							&& pm_pay_id.equals(member.get("pm_pay_id")) && m_pay_num.equals(member.get("m_pay_num"))
							&& m_card_year.equals(member.get("m_card_year"))
							&& m_card_month.equals(member.get("m_card_month"))) {
						// 결제정보 그대로. m_cms도 그대로 put
						pmap.put("m_cms", m_cms);
					} else {
						// 결제정보 변동됨
						if (m_cms.equals(MemberInfo.CMS_REGISTERD) || m_cms.equalsIgnoreCase(MemberInfo.CMS_CHANGE)) {
							// 정보수정(이미등록완료에서 수정한 경우(수정요청))
							pmap.put("m_cms", MemberInfo.CMS_CHANGE);
						} else {
							// 등록중인 경우. 결제정보를 제외한 정보만 update
							pmap.remove("m_pay_info");
							pmap.remove("m_pay_owner");
							pmap.remove("pm_pay_id");
							pmap.remove("m_pay_num");
							pmap.remove("m_card_year");
							pmap.remove("m_card_month");
							// System.out.println(pmap.entrySet());
							memberService.updateMember(pmap.getMap());
							return "결제정보가 등록중입니다.\n등록결과를 받은 후 결제정보를 수정해주세요.";
						}

					}
				} else {
					// 결제방식이 바뀐 경우
					// cms 삭제 후 미등록으로.
					cmsService.deleteCms(Integer.parseInt(m_idx));
					pmap.put("m_cms", MemberInfo.CMS_UNREGISTERD);
					Log.warn("결제정보 변경 회원삭제" + member.get("m_num"));
				}

				Log.warn("결제정보 변경 (before)" + member.entrySet());
				Log.warn("결제정보 변경 (after)" + pmap.entrySet());
				// 결제정보+cms상태 업데이트

				//System.out.println(member.entrySet());
				//System.out.println(pmap.entrySet());
				memberService.updateMemberPayInfo(pmap.getMap());
			}

			msg = "저장 완료.";
		} catch (Exception e) {
			Log.warn(" " + e.toString() + pmap.entrySet());
			return "오류 발생.\n다시 시도해주세요";
		}
		return msg;
	}

	/** 상담관리>고객정보>6 녹취계약 파일업로드 */
	@RequestMapping(value = "{m_idx}/6", method = RequestMethod.POST, produces = "text/plain;charset=utf8")
	public @ResponseBody Object customer6(@PathVariable String m_idx, String type, MultipartFile file) {
		String msg = "";
		try {
			int midx = Integer.parseInt(m_idx);
			String mf_name = "";
			if (type.equals("record")) {
				mf_name = MemberFile.NAME_RECORD;
			} else if (type.equals("credit")) {
				mf_name = MemberFile.NAME_CREDIT_INQUIRY;
			} else {
				return "잘못된 접근입니다.";
			}

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("m_idx", midx);
			map.put("mf_name", mf_name);
			map.put("mf_file", file.getOriginalFilename());
			msg = memberService.insertMemberFile(map, file);
		} catch (Exception e) {
			msg = "오류 발생.\n다시 시도해주세요";
		}
		return msg;
	}

	/** 상담관리>고객정보>녹취계약완료 */
	@RequestMapping(value = "{m_idx}/recordContract", method = RequestMethod.PUT)
	public @ResponseBody Object recordContract(@PathVariable String m_idx) {
		Map<String, Object> hm = new HashMap<String, Object>();
		hm.put("result", 0);
		try {
			int midx = Integer.parseInt(m_idx);
			Map<String, Object> member = memberService.selectMemberDetail(midx);
			String m_voice_contract = (String) member.get("m_voice_contract");
			if (!(m_voice_contract == null || m_voice_contract.equals(""))) {
				hm.put("msg", "이미 녹취계약이 확정됐습니다.");
				hm.put("result", 1);
				hm.put("m_voice_contract", m_voice_contract);
				return hm;
			}
			String m_state = member.get("m_state") + "";
			// constant_m_state
			if (!m_state.equals(MemberInfo.STATE_FINISH)) {
				hm.put("msg", "상담완료 상태가 아닙니다.");
				return hm;
			}
			List<Map<String, Object>> rlist = memberService.selectMemberFileList(m_idx, MemberFile.NAME_RECORD);
			if (rlist == null || rlist.size() == 0) {
				hm.put("msg", "녹취파일이 없습니다.");
				return hm;
			}

			hm.put("m_idx", midx);
			Calendar now = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			hm.put("m_voice_contract", sdf.format(now.getTime()));
			int result = memberService.updateMember(hm);
			if (result > 0) {
				hm.put("result", result);
				hm.put("msg", "녹취계약이 완료됐습니다.");
			} else {
				hm.put("msg", "오류발생. 다시 시도해주세요.");
			}

		} catch (Exception e) {
			hm.put("msg", "오류 발생.\n다시 시도해주세요");
		}
		return hm;
	}

	@RequestMapping(value = "customerCalc", method = RequestMethod.GET)
	public @ResponseBody Object customerCalc(ParamMap pmap) {

		Map<String, Object> map = memberService.selectRetalEstimate(pmap);
		return 0;
	}

	// 최종계약확정
	@RequestMapping(value = "{m_idx}/final", method = RequestMethod.GET)
	public @ResponseBody Object finalContract(@PathVariable String m_idx) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		String msg = "";
		try {
			int midx = Integer.parseInt(m_idx);
			Map<String, Object> member = memberService.selectMemberDetail(midx);
			String m_state = (String) member.get("m_state");
			String m_install_finish_date = (String) member.get("m_install_finish_date");
			
			if (m_install_finish_date == null || m_install_finish_date.equals("")
					||!m_state.equals(MemberInfo.STATE_FINISH)) {
				returnMap.put("msg", "상담완료, 배송(설치)완료인 경우에만 최종계약확정이 가능합니다.");
				return returnMap;
			}
			
			if(member.get("m_detail").equals(MemberInfo.DETAIL_ILSI)) {
				returnMap.put("msg", "매출구분 일시불인 고객은 처리가 불가능합니다.");
				return returnMap;
			}
			
			
			msg = payService.insertPay(midx);
		} catch (Exception e) {
			msg = "오류발생";
		}
		returnMap.put("msg", msg);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		returnMap.put("date", sdf.format(Calendar.getInstance().getTime()));
		return returnMap;
	}

	@RequestMapping(value = "contractApp/{m_idx}", method = RequestMethod.GET)
	public ModelAndView contractApp(@PathVariable String m_idx, String mi_idx) {
		ModelAndView mav = new ModelAndView("/install/table/contractApp");
		Map<String, Object> cmap = memberService.selecCustomerDetail(m_idx);
		int result = deliveryService.updateNewStatus(mi_idx);
	
		String addr = cmap.get("mi_addr1") +"";
		addr += " " + cmap.get("mi_addr2");
		addr += " " + cmap.get("mi_addr3");
		cmap.put("mi_addr", addr);

		//System.out.println(cmap.get("if_idx") + "/" + cmap.get("if_file") + "/" + cmap.get("if_memo"));
		String if_idx = cmap.get("if_idx") + "";
		String if_file = cmap.get("if_file") + "";
		String if_memo = cmap.get("if_memo") + "";

		if (if_idx == null || if_file == null || if_memo == null) {
			cmap.put("if_file", "");
		} else {
			String td4 = "<a href='/file/install/" + cmap.get("if_idx") + "_" + cmap.get("if_memo")
					+ "' class='width-100 btn bg-info'>" + cmap.get("if_file") + " </a>";
			cmap.put("if_file", td4);
		}

		//System.out.println(cmap.entrySet());

		mav.addObject("mi_detail", cmap);
		return mav;
	}

	@RequestMapping(value = "contractApp2/{m_idx}", method = RequestMethod.GET)
	public ModelAndView contractApp2(@PathVariable String m_idx) {
		ModelAndView mav = new ModelAndView("/install/table/contractApp");
		Map<String, Object> cmap = memberService.selecCustomerDetail(m_idx);
		System.out.println(cmap.entrySet());
		String addr = cmap.get("mi_addr1") + "";
		addr += " " + cmap.get("mi_addr2");
		addr += " " + cmap.get("mi_addr3");
		cmap.put("mi_addr", addr);
		mav.addObject("mi_detail", cmap);
		return mav;
	}

	//설치확인서 있는지 확인
	@RequestMapping(value = "filecheck/{m_idx}", method = RequestMethod.GET)
	public @ResponseBody Object filecheck(@PathVariable String m_idx) {
		Map<String,Object>map_if=new HashMap<String, Object>();
		int midx=Integer.parseInt(m_idx);
		List<Map<String,Object>> list=installService.selectinstallFileList(midx);

		if(list!=null&&list.size()>0) {
			try{
				map_if=list.get(0);
				map_if.put("ds_date",deliveryService.selectDeliveryState(midx).get(0).get("ds_date"));
			}catch (Exception e) {
				e.printStackTrace();
			}
			map_if.put("msg","등록");
		}else{
			map_if.put("msg","미등록");
		}
		return map_if;
	}

	/** 렌탈약정서 */
	@RequestMapping(value = "rental/{m_idx}", method = RequestMethod.GET, produces = "text/plain;charset=utf8")
	public ModelAndView rental(@PathVariable String m_idx) {

		ModelAndView mav = new ModelAndView("customer/LED");
		//배송자정보
		Map<String, Object> dmap = memberService.selectRentalbyDelivery(m_idx);
		//계약자정보
		Map<String, Object> cmap = memberService.selectRentalbyContract(m_idx);
		//약정일 가져오기
		Map<String,Object> deliver = memberService.rentalFinishDate(m_idx);
		
		dmap.put("ds_state", deliver.get("ds_state"));
		dmap.put("ds_date", deliver.get("ds_date"));
		cmap.put("finishdate", deliver.get("finishdate").toString().substring(0,7));
		// System.out.println(cmap.entrySet());
		if (cmap.get("pr_name").toString().contains("숯")) {
			mav.setViewName("customer/bed");
		} else if (cmap.get("pr_name").toString().contains("방진망")) {
			mav.setViewName("customer/filter");
		} else if (cmap.get("pr_name").toString().contains("S-트레이너")) {
			mav.setViewName("customer/s_trainer");

		} else if (cmap.get("pr_name").toString().contains("닥터레이디")) {

			mav.setViewName("customer/drlady");
		} else if (cmap.get("pr_rentalcode").toString().contains("led")) {
			mav.setViewName("customer/LED");
		}
		/**계약정보*/
		String c_addr = cmap.get("mi_addr1") + "";
		c_addr += " " + cmap.get("mi_addr2");
		c_addr += " " + cmap.get("mi_addr3");
		cmap.put("addr", c_addr);
		
		/**배송정보*/
		String d_addr = dmap.get("mi_addr1") + "";
		d_addr += " " + dmap.get("mi_addr2");
		d_addr += " " + dmap.get("mi_addr3");
		dmap.put("addr", d_addr);
		
		
		
		String m_pay_id = cmap.get("m_pay_id")+"";
		String m_id_type = cmap.get("m_id_type")+"";
		
		if(m_id_type.equals(MemberInfo.ID_TYPE_PERSON)) {
			m_pay_id= m_pay_id.replaceAll("[^0-9]", "");
			if(m_pay_id.length()>6) {
//				System.out.println(m_pay_id);
				m_pay_id = m_pay_id.substring(0, 6)+"-"+m_pay_id.substring(6,7)+"******";
			}else {
				m_pay_id = m_pay_id +"-*******";
			}
			cmap.put("m_pay_id",m_pay_id);
			dmap.put("m_pay_id",m_pay_id);
		}
		
		
		/**결제시작일 - DB에서 일자가 빠지면 대체*/
		/*String m_daystart = cmap.get("m_paystart")+"-"+cmap.get("m_paydate");
		cmap.put("m_paystart", m_daystart);*/
		
		/**결제정보*/
		dmap.put("m_pay_num", dmap.get("m_pay_num").toString().replaceAll("(?<=.{8}).","*"));
		
		
		/**계약자 폰번호*/
		String m_phone = cmap.get("mi_phone") + "";
		m_phone = m_phone.replaceAll("[^0-9]", "");
		if (m_phone.length() == 11) {
			m_phone = m_phone.substring(0, 3) + "-" + m_phone.substring(3, 7) + "-" + m_phone.substring(7);
			cmap.put("mi_phone", m_phone);
		}
		/**배송자 폰번호*/
		String m_phone2 = dmap.get("mi_phone") + "";
		m_phone2 = m_phone.replaceAll("[^0-9]", "");
		if (m_phone2.length() == 11) {
			m_phone2 = m_phone2.substring(0, 3) + "-" + m_phone2.substring(3, 7) + "-" + m_phone2.substring(7);
			dmap.put("mi_phone", m_phone2);
		}
		
		if(cmap.get("m_process").toString().contains("일시불")) {
			cmap.put("m_paystart", MemberInfo.PROCESS_ILSI_FINISH);
		}
		System.out.println(cmap.entrySet());
		cmap.put("m_rental", Common.comma(cmap.get("m_rental")+""));
		mav.addObject("cinfo", cmap);
		mav.addObject("dinfo", dmap);
		return mav;

	}

	/** 양도확인서 */
	@RequestMapping(value = "yangdo/{m_idx}", method = RequestMethod.GET, produces = "text/plain;charset=utf8")
	public ModelAndView yangdo(@PathVariable String m_idx) {

		ModelAndView mav = new ModelAndView("customer/LED2");
		Map<String, Object> cmap = memberService.selectRentalbyDelivery(m_idx);
		//System.out.println(cmap.entrySet());
		if (cmap.get("pr_name").toString().contains("숯")) {
			mav.setViewName("customer/bed2");
		} else if (cmap.get("pr_name").toString().contains("방진망")) {
			mav.setViewName("customer/filter2");
		} else if (cmap.get("pr_name").toString().contains("S-트레이너")) {
			mav.setViewName("customer/s_trainer2");

		} else if (cmap.get("pr_name").toString().contains("닥터레이디")) {

			mav.setViewName("customer/drlady2");
		} else if (cmap.get("pr_rentalcode").toString().contains("led")) {
			mav.setViewName("customer/LED2");
		}

		String addr = cmap.get("mi_addr1") + "";
		addr += " " + cmap.get("mi_addr2");
		addr += " " + cmap.get("mi_addr3");
		cmap.put("addr", addr);
		//System.out.println(cmap.get("addr"));
		mav.addObject("info", cmap);
		return mav;

	}

	//중도상환
	@RequestMapping(value = "contract/{m_idx}/joongdo", method = RequestMethod.GET,produces = "text/plain;charset=utf8")
	public @ResponseBody Object joongdo(@PathVariable String m_idx){
		int midx=Integer.parseInt(m_idx);
		List<Map<String,Object>>list=deliveryService.selectDeliveryState(midx);
		if(list==null||list.isEmpty()) {
			return "배송/설치 정보가 없습니다.\n배송/설치 완료일이 있어야 중도상환 가능합니다.";
		}
		
		Map<String,Object> ds=list.get(0);
		String ds_date=(String)ds.get("ds_date");
		if(ds_date==null||ds_date.equals("")) {
			return "배송/설치 완료일이 있어야 중도상환 가능합니다.";
		}
		
		String msg=memberService.joongdo(midx);
		return msg;
	}
	
	//일시불,출금완료
	@RequestMapping(value = "contract/{m_idx}/paid", method = RequestMethod.PUT,produces = "text/plain;charset=utf8")
	public @ResponseBody Object paid(@PathVariable String m_idx){
		int midx=Integer.parseInt(m_idx);
		Map<String,Object> member=memberService.selectMemberDetail(midx);
		
		Map<String,Object> memberUp=new HashMap<String, Object>();
		memberUp.put("m_idx", midx);
		
		String m_detail=member.get("m_detail")+"";
		
		String msg="";

		if(m_detail.equals(MemberInfo.DETAIL_ILSI)) {
			payService.noMoreRental(midx);
			memberUp.put("m_process", MemberInfo.PROCESS_ILSI_FINISH);
			memberService.updateMember(memberUp);
			cmsService.deleteCms(midx);
			msg="일시불완료 되었습니다.";
		}else {
			int cnt=payService.countUnpaid(midx);
			if(cnt>0) {
				msg="모든 출금이 완료된 후에 처리 가능합니다.";
			}else {
				String m_final_date = (String) member.get("m_final_date");
				if (m_final_date==null || m_final_date.equals("")) {
					msg="최종계약확정 후 완료처리 가능합니다.";
				}else {
					memberUp.put("m_process", MemberInfo.PROCESS_RENT_FINISH);
					memberService.updateMember(memberUp);
					cmsService.deleteCms(midx);
					msg="렌탈완료 되었습니다.";
				}
			}
		}
			
		return msg;
	}
}
