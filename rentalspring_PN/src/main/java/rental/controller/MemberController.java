package rental.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Log;
import rental.common.ParamMap;
import rental.constant.Delivery;
import rental.constant.MemberInfo;
import rental.constant.Pay;
import rental.service.CommonService;
import rental.service.DeliveryService;
import rental.service.EmployeeService;
import rental.service.MemberService;
import rental.service.PayService;
import rental.service.ProductService;

@Controller
@RequestMapping("member/")
public class MemberController {

	@Autowired
	CommonService commonService;
	@Autowired
	DeliveryService deliveryService;
	@Autowired
	MemberService memberService;
	@Autowired
	ProductService productService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	PayService payService;
	
	//m_idx와 mt_code로 member_info 조회
	@RequestMapping(value="{m_idx}/info",method=RequestMethod.GET)
	public @ResponseBody Object customerGet(@PathVariable String m_idx,String mt_code) {
		int m=Integer.parseInt(m_idx);
		if(mt_code.equals("결제")) {
			return memberService.selectMemberDetail(m);
		}else {
			return memberService.selectMemberInfo(m, mt_code);
		}
	}
	//mi_idx로 member_info 조회
	@RequestMapping(value="info/{mi_idx}",method=RequestMethod.GET)
	public @ResponseBody Object selectMemberInfoDetail(@PathVariable String mi_idx) {
		int miidx=Integer.parseInt(mi_idx);
		return memberService.selectMemberInfoDetail(miidx);
	}
	
	//상담cs배정
	@RequestMapping(value="damdang/{m_idx}",method=RequestMethod.PUT,produces="text/plain;charset=utf-8")
	public @ResponseBody Object damdang(@PathVariable String m_idx,String m_damdang) {
		int result=memberService.updateDamdang(m_idx, m_damdang);
		return result+"";
	}
	//상담cs일괄배정
	@RequestMapping(value="damdangBatch/{e_id}",method=RequestMethod.POST,produces="text/plain;charset=utf-8")
	public @ResponseBody Object damdangBatch(@PathVariable String e_id,String m_idxs) {
		StringTokenizer st=new StringTokenizer(m_idxs, ",");
		int result=1;
		while(st.hasMoreTokens()) {
			if(memberService.updateDamdang(st.nextToken(), e_id)<0) {
				result=-1;
			}
		}
		return result+"";
	}
	//접수상세list(매출구분)
	@RequestMapping(value="mdetailList")
	public @ResponseBody Object mdetailList() {
		return commonService.mdetailList();
	}
	
	//m_idx에 따른 파일list
	@RequestMapping(value="{m_idx}/fileList",method=RequestMethod.GET)
	public @ResponseBody Object fileList(@PathVariable String m_idx,String mf_name){
		if(mf_name==null||mf_name.equals("")) {
			return null;
		}
		return memberService.selectMemberFileList(m_idx, mf_name);
	}

	
	//m_idx 렌탈약정서 업로드 페이지
	@RequestMapping(value="{m_idx}/rentalFile",method=RequestMethod.GET)
	public ModelAndView rentalFile(@PathVariable String m_idx){
		ModelAndView mav=new ModelAndView("member/rentalFile");
		mav.addObject("m_Idx", m_idx.trim());
		return mav;
	}
	
	//채권관리 담당 list조회
	@RequestMapping(value="{m_idx}/damdang/history",method=RequestMethod.GET)
	public @ResponseBody Object damdangHistoryGet(@PathVariable String m_idx,String start,String length){
		Map<String, Object> returnMap=new HashMap<String, Object>();
		Map<String,Object>map=new HashMap<String, Object>();
		int midx=Integer.parseInt(m_idx);
		map.put("m_idx", midx);
		map.put("start", Integer.parseInt(start));
		map.put("length", Integer.parseInt(length));
		
		List<Map<String,Object>> list=memberService.selectMemberDamdangHistoryList(map);
		int count=memberService.countMemberDamdangHistoryList(midx);
		returnMap.put("data", list);
		returnMap.put("recordsFiltered", count);
		returnMap.put("recordsTotal", count);
		return returnMap;
	}
	
	//채권관리 담당배정
	@RequestMapping(value="{m_idx}/damdang",method=RequestMethod.POST,produces="text/plain;charset=utf8")
	public @ResponseBody Object damdangPost(@PathVariable String m_idx,ParamMap pmap){
		String msg="";
		try{
			String e_id=(String)pmap.get("e_id");
			if(e_id==null||e_id.equals("")) {
				msg="담당 배정을 선택해주세요.";
			}else {
				int midx=Integer.parseInt(m_idx);
				int eid=Integer.parseInt(e_id);
				Map<String, Object> login=(Map<String, Object>)pmap.get("login");
				int my=Integer.parseInt(login.get("e_id")+"");
				
				String memo="";//멤버배정메모 미구현
				memberService.insertMemberDamdang(midx,eid,my,memo);
			}
			pmap.printEntrySet();
		}catch (Exception e) {
			Log.warn("담당 배정 중 오류 발생."+pmap.entrySet());
			msg="담당 배정 중 오류 발생. 다시 시도해주세요.";
		}
		
		return msg;
	}
	//m_idx 출금통계 팝업
	@RequestMapping(value="{m_idx}/pay",method=RequestMethod.GET)
	public ModelAndView pay(@PathVariable String m_idx){
		ModelAndView mav=new ModelAndView("member/pay");
		int midx=Integer.parseInt(m_idx);
		Map<String,Object> member=memberService.selectMemberDetail(midx);
		member.put("m_payend", member.get("m_payend").toString().subSequence(0, 7));
		mav.addObject("member", member);
		mav.addObject("mi_contract",memberService.selectMemberInfo(midx, MemberInfo.TYPE_CONTRACT));
		mav.addObject("payList", payService.selectPayListByMidx(midx, 0, -1));
		return mav;
	}
	//이월 모달
	@RequestMapping(value="{m_idx}/pay/{pay_idx}",method=RequestMethod.GET)
	public ModelAndView payGet(@PathVariable String m_idx,@PathVariable String pay_idx){
		ModelAndView mav=new ModelAndView("member/modal/payCarryOver");
		if(pay_idx.indexOf(",")==-1) {
			//1개 이월
			int payidx=Integer.parseInt(pay_idx);
			Map<String,Object>payMap=payService.selectPayDetail(payidx);
			payMap.remove("cms_pw");
			payMap.remove("cms_card");
			payMap.remove("cms_id");
			mav.addObject("payMap", payMap);
		}else {
			//일괄 이월
			Map<String,Object>payMap=new HashMap<String, Object>();
			payMap.put("pay_idx", pay_idx);
			Calendar now=Calendar.getInstance();
			payMap.put("pay_year", now.get(Calendar.YEAR));
			payMap.put("pay_month", now.get(Calendar.MONTH)+1);
			payMap.put("pay_original_date", "일괄 이월");
			payMap.put("m_paystart", "일괄 이월");
			mav.addObject("payMap", payMap);
		}
		return mav;
	}
	//이월
	@RequestMapping(value="{m_idx}/pay/{pay_idx}",method=RequestMethod.PUT,produces="text/plain;charset=utf-8")
	public @ResponseBody Object payPut(@PathVariable String m_idx,@PathVariable String pay_idx,ParamMap pmap){
		String msg="이월 완료";
		try{
			Map<String,Object>map=new HashMap<String, Object>();
			int pay_year=Integer.parseInt(pmap.get("pay_year")+"");
			int pay_month=Integer.parseInt(pmap.get("pay_month")+"");
			map.put("pay_idx", pay_idx);
			map.put("pay_year", pay_year);
			map.put("pay_month", pay_month);
			
			Map<String,Object> member=memberService.selectMemberDetail(Integer.parseInt(m_idx));
			String m_paystart=member.get("m_paystart")+"";
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Calendar paystart=Calendar.getInstance();
			paystart.setTime(sdf.parse(m_paystart));
			int year=paystart.get(Calendar.YEAR);
			int month=paystart.get(Calendar.MONTH)+1;
			
			if(pay_year<year) {
				return "결제 시작일 보다 이전으로 지정할 수 없습니다.";
			}else if(pay_year==year) {
				if(pay_month<month) {
					return "결제 시작일 보다 이전으로 지정할 수 없습니다.";
				}
			}
			
			map.put("pay_carryover_now", "now");//이월날짜=now,횟수+1
			if(pay_idx.indexOf(",")==-1) {
				payService.updatePay(map);
			}else {
				//일괄 이월
				StringTokenizer pay_idxs=new StringTokenizer(pay_idx,",");
				while(pay_idxs.hasMoreTokens()) {
					String str_pay_idx=pay_idxs.nextToken();
					if(str_pay_idx!=null&&!str_pay_idx.equals("")) {
						map.put("pay_idx", str_pay_idx);
						payService.updatePay(map);
					}
				}
			}
			Log.warn("이월 m_idx="+m_idx+",pay_idx="+pay_idx+','+pmap.entrySet());
			
		}catch (Exception e) {
			Log.warn("이월 중 오류 발생 "+e.toString()+" m_idx="+m_idx+",pay_idx="+pay_idx+','+pmap.entrySet());
			msg="오류발생";
		}
		
		return msg;
	}
	//회차 변경
	@RequestMapping(value="{m_idx}/changeCnt",method=RequestMethod.PUT)
	public @ResponseBody Object changeCntGet(@PathVariable String m_idx,ParamMap pmap){
		Map<String,Object> map=new HashMap<String, Object>();
		int result=payService.changeCnt(m_idx,pmap.getMap());
		map.put("result", result);
		switch (result) {
			case 1:map.put("msg", "회차 변경 완료.");break;
			case -1:map.put("msg", "출금완료 상태인 회차만 변경이 가능합니다.");break;
			case -2:map.put("msg", "신청전의 회차로만 변경이 가능합니다.");break;
			case -3:map.put("msg", "출금 렌탈금액이 같은 건만 회차 변경이 가능합니다.");break;
			case -100:map.put("msg", "오류 발생.");break;
			default:map.put("msg", "잘못된 접근입니다.");
		}
		return map;
	}
		
	//m_idx로 m_memo 업데이트(출금내역 팝업)
	@RequestMapping(value="{m_idx}/memo",method=RequestMethod.PUT)
	public @ResponseBody Object m_memo(@PathVariable String m_idx,String m_memo){
		m_memo=m_memo.replaceAll("script", "");
		m_memo=m_memo.replaceAll("[<]", "");
		m_memo=m_memo.replaceAll("[\\\\]", "");
		m_memo=m_memo.replaceAll("[\"]", "");
		Map<String,Object> member=new HashMap<String, Object>();
		member.put("m_idx", m_idx);
		member.put("m_memo", m_memo);
		if(memberService.updateMember(member)>0) {
			member.put("msg","메모 저장 완료.");
		}else {
			member.put("msg","오류발생.\n잠시후 시도해주세요.");
		}
		return member;
	}
	
	//결제시작일 변경 모달(최종계약 확정 된 상태에서)
	@RequestMapping(value="{m_idx}/payStart",method=RequestMethod.GET)
	public ModelAndView payStartGet(@PathVariable String m_idx){
		ModelAndView mav=new ModelAndView("member/modal/payStartDay");
		Map<String,Object> member=memberService.selectMemberDetail(Integer.parseInt(m_idx));
		mav.addObject("member", member);
		String m_paystart=member.get("m_paystart")+"";
		if(!m_paystart.equals("")) {
			mav.addObject("startYear", m_paystart.substring(0,4));
			mav.addObject("startMonth", m_paystart.substring(5,7));
		}
		
		return mav;
	}
	
	//결제시작일 변경
	@RequestMapping(value="{m_idx}/payStart",method=RequestMethod.PUT)
	public @ResponseBody Object payStartPut(@PathVariable String m_idx,ParamMap pmap){
		Map<String,Object> map=new HashMap<String, Object>();
		int result=payService.changePayStart(m_idx,pmap.getMap());
		map.put("result", result);
		switch (result) {
			case 1:map.put("msg", "결제 시작일이 변경되었습니다.");break;
			case -100:map.put("msg", "오류 발생.");break;
			default:map.put("msg", "잘못된 접근입니다.");
		}
		return map;
	}
	
	//어플에서 대기 처리
	@RequestMapping(value="waiting",method=RequestMethod.PUT,produces="text/plain;charset=utf-8")
	public @ResponseBody Object pending(String m_idx){
		int midx=Integer.parseInt(m_idx);
		List<Map<String,Object>> dsList=deliveryService.selectDeliveryState(midx);
		if(dsList!=null&&dsList.size()>0) {
			Map<String,Object> ds=dsList.get(0);
			String ds_state=ds.get("ds_state")+"";
			if(ds_state.equals(Delivery.STATE_BEFORE_INSTALL)||ds_state.equals(Delivery.STATE_REQUEST_INSTALL)) {
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("m_idx", midx);
				map.put("m_state", MemberInfo.STATE_WAITING);
				if(memberService.updateMember(map)>0) {
					return "대기 처리하였습니다.";
				}else {
					return "다시 시도해주세요.";
				}
			}else {
				return "설치전/설치요청 상태에서만 대기처리 가능합니다.";
			}
		}else {
			return "설치 정보가 없습니다.";
		}
	}
	
	//m_idx로 무료체험일 가져오기
	@RequestMapping(value="{m_idx}/freeday",method=RequestMethod.GET,produces="text/plain;charset=utf8")
	public @ResponseBody Object freeday(@PathVariable String m_idx) {
		int m=Integer.parseInt(m_idx);
		return memberService.selectMemberDetail(m).get("m_free_day");
	}
}
