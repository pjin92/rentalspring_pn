package rental.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rental.common.Common;
import rental.common.ParamMap;
import rental.constant.Product;
import rental.constant.ProductRental;
import rental.service.ProductService;
import rental.service.CsService;
import rental.service.EmployeeService;
import rental.service.MemberService;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("product/")
public class ProductController {

	@Autowired
	ProductService productService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	CsService csService;

	@Autowired
	MemberService memberService;

	@RequestMapping(value = "rentalProducts", method = RequestMethod.POST)
	public @ResponseBody Object rentalProducts(ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> prlist = productService.productRentalList(pmap.getMap());
		int filtered = 0;
		if (prlist != null && prlist.size() != 0) {
			for (int i = 0; i < prlist.size(); i++) {
				Map<String, Object> prMap = prlist.get(i);
				int rownum = Integer.parseInt(prMap.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String pr_name = "<a idx='" + prMap.get("pr_idx") + "' onclick='choose()'>" + prMap.get("pr_name")
						+ "</a>";
				prMap.put("pr_name", pr_name);

				// pr_total,pr_rental,pr_ilsi int에서 ,찍은 string으로
				prMap.put("pr_total", Common.comma(prMap.get("pr_total") + ""));
				prMap.put("pr_rental", Common.comma(prMap.get("pr_rental") + ""));
				prMap.put("pr_ilsi", Common.comma(prMap.get("pr_ilsi") + ""));

				String td = "<a pr_idx='" + prMap.get("pr_idx") + "' onclick='rproductDatail()'> 상세보기 </a>";
				prMap.put("detail", td);
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		returnMap.put("data", prlist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", productService.countProductRental());
		return returnMap;
	}

	/**
	 * productRentalList 조회
	 */

	@RequestMapping(value = "productRentalList", method = RequestMethod.GET)
	public ModelAndView newProductRental() {
		ModelAndView mav = new ModelAndView("/product/productRentalList");
		return mav;
	}

	/**
	 * productRentalList 조회
	 */
	@RequestMapping(value = "productRentalList", method = RequestMethod.POST)
	public @ResponseBody Object productRentalList(ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();

		/*
		 * String[] date = pmap.get("date").toString().trim().split("~"); for(int
		 * i=0;i<date.length;i++) { System.out.println(date[i]); }
		 */

		String date = (String) pmap.get("date");
		String dateTo = "";
		String dateFrom = "";
		if (date == null || date.equals("")) {

		} else {
			String[] date1 = date.split("~");
			dateFrom = date1[0].trim();
			dateTo = date1[1].trim();
			pmap.put("dateTo", dateTo);
			pmap.put("dateFrom", dateFrom);
		}

		List<Map<String, Object>> plist = productService.selectrentalList(pmap.getMap());
		int filtered = 0;
		if (plist != null && plist.size() != 0) {
			for (int i = 0; i < plist.size(); i++) {
				Map<String, Object> mapTemp = plist.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String pr_idx = mapTemp.get("pr_idx") + "";
				String pr_code = mapTemp.get("pr_rentalcode") + "";
				String td = "<a pr_rentalcode='" + pr_code + "' onclick='rentalProduct()'>" + mapTemp.get("pr_name")
						+ "</a>";

				if (mapTemp.get("pr_match").equals(1)) {
					String csAdd = "<a pr_idx='" + pr_idx + "' onclick='csMatchProduct()'> 상세보기 </a>";
					mapTemp.put("csAdd", csAdd);
				} else {
					String csAdd = "<a pr_idx='" + pr_idx + "' onclick='csProduct()'> 등록 </a>";
					mapTemp.put("csAdd", csAdd);
				}
				mapTemp.put("pr_name", td);
				mapTemp.put("pr_idx", "");

				// 월렌탈료
				mapTemp.put("pr_rental", Common.comma(mapTemp.get("pr_rental") + ""));
				mapTemp.put("pr_ilsi", Common.comma(mapTemp.get("pr_ilsi") + ""));
				mapTemp.put("pr_total", Common.comma(mapTemp.get("pr_total") + ""));
				mapTemp.put("pr_delivery1", Common.comma(mapTemp.get("pr_delivery1") + ""));
				mapTemp.put("pr_install1", Common.comma(mapTemp.get("pr_install1") + ""));
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", plist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", productService.countrentalProductList(pmap.getMap()));
		return returnMap;
	}

	/**
	 * 렌탈상품등록 폼
	 */
	@RequestMapping(value = "productRental", method = RequestMethod.GET)
	public @ResponseBody Object productRental(ParamMap map) {
		ModelAndView mav = new ModelAndView("/product/productRental");
		mav.addObject("writer", map.getMap().get("login"));
		return mav;
	}

	@RequestMapping("product")
	public ModelAndView rentalProduct() {
		ModelAndView mav = new ModelAndView("rentalProduct");
		// List<Map<String, Object>> rentalProduct = product.getRentalProduct();
		// mav.addObject("rental",rentalProduct);
		return mav;
	}

	@RequestMapping(value = "productRentalinfo/{idx}", method = RequestMethod.GET)
	public @ResponseBody Object productRental(@PathVariable String idx) {
		HashMap<String, Object> map = productService.selectProductRentalDetail(idx);
		return map;
	}

	/**
	 * 렌탈상품등록 중 상품 불러오기
	 */
	@RequestMapping(value = "productList")
	public @ResponseBody Object productList(ParamMap pmap) {

		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> plist = productService.selectProductList(pmap.getMap());
		int filtered = 0;
		if (plist != null && plist.size() != 0) {
			for (int i = 0; i < plist.size(); i++) {
				Map<String, Object> pMap = plist.get(i);
				int rownum = Integer.parseInt(pMap.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String p_name = "<a idx='" + pMap.get("p_id") + "' onclick='choose(this)'>" + pMap.get("p_name")
						+ "</a>";
				pMap.put("p_name", p_name);
				pMap.put("p_id", "");
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
			// TODO: handle exception
		}

		returnMap.put("data", plist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", productService.countProduct(pmap.getMap()));
		return returnMap;
	}

	/**
	 * cs항목 상품등록 중 상품 불러오기
	 */
	@RequestMapping(value = "csProductList")
	public @ResponseBody Object csProductList(ParamMap pmap) {

		Map<String, Object> returnMap = new HashMap<String, Object>();
		List<Map<String, Object>> plist = productService.selectProductOfCsList(pmap.getMap());
		int filtered = 0;
		if (plist != null && plist.size() != 0) {
			for (int i = 0; i < plist.size(); i++) {
				Map<String, Object> pMap = plist.get(i);
				int rownum = Integer.parseInt(pMap.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String p_name = "<a idx='" + pMap.get("p_id") + "' onclick='choose(this)'>" + pMap.get("p_name")
						+ "</a>";
				pMap.put("p_name", p_name);
				pMap.put("p_id", "");
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
			// TODO: handle exception
		}

		returnMap.put("data", plist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", productService.countProductOfCsList(pmap.getMap()));
		return returnMap;
	}

	/**
	 * 상품등록
	 */
	@RequestMapping(value = "productRental", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public @ResponseBody Object rentalProductAdd(ParamMap map, HttpServletResponse response) throws IOException {
		try {
			productService.insertRentalProduct(map);
			return "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록 실패";
		}
	}

	/**
	 * LED견적계산기 등록
	 */
	/*
	 * @RequestMapping(value = "calculate", method = RequestMethod.POST, produces =
	 * "application/text; charset=utf8") public @ResponseBody Object
	 * ledProduct(ParamMap map, HttpServletResponse response) throws IOException {
	 * String rental_code = "";
	 * 
	 * //System.out.println("인서트 들어옴"); //System.out.println(map.entrySet()); String
	 * gubun = "2"; try { //System.out.println(map.entrySet()); rental_code =
	 * productService.insertRentalProduct(map, ProductRental.USE_YES,gubun); int
	 * result = csService.insertCsProduct(map); map.put("csm_idx", result);
	 * map.put("rental_code", rental_code); int result2 =
	 * csService.autoCsMatch(map.getMap()); return rental_code; } catch (Exception
	 * e) { e.printStackTrace(); return "등록실패"; } }
	 */
	/*
	 * 렌탈상품 상세보기폼
	 **/
	@RequestMapping(value = "rentalProductComp", method = RequestMethod.GET)
	public ModelAndView rentalproducts() {
		ModelAndView mav = new ModelAndView("product/rentalProductComp");
		return mav;
	}

	/**
	 * 렌탈상품 상세보기 pr_idx로 하나씩 조회
	 */
	@RequestMapping(value = "rentalProductComp/{pr_rentalcode}", method = RequestMethod.GET)
	public ModelAndView Getrentalproducts(@PathVariable String pr_rentalcode, ParamMap map) {
		ModelAndView mav = new ModelAndView("product/rentalProductComp");
		String pr_code = pr_rentalcode;
		Map<String, Object> product = productService.selectRentallistDetail(pr_rentalcode);
		product.put("pr_rental", Common.comma(product.get("pr_rental") + ""));
		product.put("pr_ilsi", Common.comma(product.get("pr_ilsi") + ""));
		product.put("pr_total", Common.comma(product.get("pr_total") + ""));
		product.put("pr_delivery1", Common.comma(product.get("pr_delivery1") + ""));
		product.put("pr_delivery2", Common.comma(product.get("pr_delivery2") + ""));
		product.put("pr_install1", Common.comma(product.get("pr_install1") + ""));
		product.put("pr_install2", Common.comma(product.get("pr_install2") + ""));
		product.put("pr_aff_cost", Common.comma(product.get("pr_aff_cost") + ""));
		product.put("pr_rentalDC", Common.comma(product.get("pr_rentalDC") + ""));

		mav.addObject("plist", product);
		List<HashMap<String, Object>> selectRentalComDetail = productService.selectRentalComDetail(pr_rentalcode);
		for (int i = 0; i < selectRentalComDetail.size(); i++) {
			selectRentalComDetail.get(i).put("p_cost", Common.comma(selectRentalComDetail.get(i).get("p_cost") + ""));
			selectRentalComDetail.get(i).put("p_price", Common.comma(selectRentalComDetail.get(i).get("p_price") + ""));
		}

		mav.addObject("data", selectRentalComDetail);
		mav.addObject("writer", map.getMap().get("login"));
		return mav;
	}

	/**
	 * 렌탈상품 수정 -> 구성 상품 수정-> 상품 목록 불러오기
	 */
	@RequestMapping(value = "rentalProductComp/productTR/{p_id}", method = RequestMethod.GET)
	public @ResponseBody Object productTR2(@PathVariable String p_id) {
		ModelAndView mav = new ModelAndView("/product/table/productTR");
		Map<String, Object> plist = productService.selectProductDetail(p_id);
		mav.addObject("plist", plist);
		return mav;
	}

	/**
	 * 렌탈상품 신규등록 -> 상품추가
	 */
	@RequestMapping(value = "productTR/{p_id}", method = RequestMethod.GET)
	public @ResponseBody Object productTR(@PathVariable String p_id) {
		ModelAndView mav = new ModelAndView("/product/table/productTR");

		Map<String, Object> plist = productService.selectProductDetail(p_id);
		String p_cost = Common.comma(plist.get("p_cost") + "");
		String p_price = Common.comma(plist.get("p_price") + "");
		String del = "<a p_id='" + p_id + "' onclick='del()'>삭제</a>";
		plist.put("p_price", p_price);
		plist.put("p_cost", p_cost);
		plist.put("del", del);
		mav.addObject("plist", plist);

		return mav;
	}

	/**
	 * 렌탈 상품 수정
	 */
	@RequestMapping(value = "rentalProductComp/productRentalModify", method = RequestMethod.POST, produces = "application/text; charset=utf8")

	public @ResponseBody Object productRentalModify(ParamMap map, HttpServletResponse response) throws IOException {
		int result = 0;
		try {
			// result1 = productService.deleteRentalProduct(map.getMap()); prp삭제
			result = productService.updateRentalProduct(map.getMap());
			return "등록 완료";
		} catch (Exception e) {
			e.printStackTrace();
			return "등록 실패";

		}
	}

	/**
	 * 견적계산기 폼
	 */
	@RequestMapping(value = "calculate", method = RequestMethod.GET)
	public @ResponseBody Object calculate(ParamMap map) {
		ModelAndView mav = new ModelAndView("/calc/calculate");
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN2);
		String ledcode = "";

		for (int i = 0; i < calList.size(); i++) {
			Map<String, Object> mapTemp = calList.get(i);

			mapTemp.put("p_deliverycost", Common.comma(mapTemp.get("p_deliverycost") + ""));
			mapTemp.put("p_price", Common.comma(mapTemp.get("p_price") + ""));

		}
		;
		String code = productService.getLEDRentalCode();
		if (code == null) {
			ledcode = "led-00001";
		} else {
			int no = Integer.parseInt(code.replace("led-", "").toString());
			String codeno = String.format("%05d", no + 1);
			ledcode = "led-" + codeno;
		}
		mav.addObject("cal", calList);
		mav.addObject("code", ledcode);
		mav.addObject("writer", map.getMap().get("login"));
		return mav;
	}

	/**
	 * 구 견적계산기 폼
	 */
	@RequestMapping(value = "oldcalculate", method = RequestMethod.GET)
	public @ResponseBody Object oldcalculate(ParamMap map) {
		ModelAndView mav = new ModelAndView("/calc/oldcalculate");
		List<HashMap<String, Object>> calList = productService.selectCalcList(ProductRental.P_RENT_GUBUN5);
		String ledcode = "";

		for (int i = 0; i < calList.size(); i++) {
			Map<String, Object> mapTemp = calList.get(i);

			mapTemp.put("p_deliverycost", Common.comma(mapTemp.get("p_deliverycost") + ""));
			mapTemp.put("p_price", Common.comma(mapTemp.get("p_price") + ""));

		}
		;
		mav.addObject("cal", calList);
		mav.addObject("writer", map.getMap().get("login"));
		return mav;
	}

	/**
	 * LED견적계산기 등록
	 */
	@RequestMapping(value = "oldcalculate", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public @ResponseBody Object oldledProduct(ParamMap map, HttpServletResponse response) throws IOException {
		String rental_code = "";

		String gubun = "5";
		try {
			rental_code = productService.OldinsertRentalProduct(map);
			int result = csService.insertCsProduct(map);
			map.put("csm_idx", result);
			map.put("rental_code", rental_code);
			int result2 = csService.autoCsMatch(map.getMap());

			return rental_code;
		} catch (Exception e) {
			e.printStackTrace();
			return "등록실패";
		}
	}

	// App -> 상품상세내역 확인
	@RequestMapping(value = "productApp/{pr_idx}", method = RequestMethod.GET)
	public @ResponseBody Object productApp(ParamMap map, @PathVariable String pr_idx) {
		ModelAndView mav = new ModelAndView("/product/table/productAppTR");
		int pr = Integer.parseInt(pr_idx);
		List<HashMap<String, Object>> plist = productService.selectproductApp(pr);
		HashMap<String, Object> rental = productService.selectProductRentalDetail(pr_idx);
		for (int i = 0; i < plist.size(); i++) {
			plist.get(i).put("p_cost", Common.comma(plist.get(i).get("p_cost") + ""));
			plist.get(i).put("p_price", Common.comma(plist.get(i).get("p_price") + ""));
		}
		String pr_rental = rental.get("pr_rental") + "";
		rental.put("pr_rental", Common.comma(pr_rental));
		mav.addObject("rental", rental);
		mav.addObject("data", plist);
		return mav;
	}

	// App ->렌탈코드확인하여 구성변경하기
	@RequestMapping(value = "productApp", method = RequestMethod.GET)
	public @ResponseBody Object productAppMain(ParamMap map) {
		ModelAndView mav = new ModelAndView("/product/producApp");
		return mav;
	}

	/** 어플에서 구성변경시 뜨는 견적계산기 폼 */
	@RequestMapping(value = "calculateApp/{pr}", method = RequestMethod.GET)
	public @ResponseBody Object oldcalculateApp(ParamMap map, @PathVariable String pr) {
		ModelAndView mav = new ModelAndView("/product/modal/plist_APP");

		// System.out.println("pr======="+pr);
		String code = productService.getProductcode(pr);
		// System.out.println(code);
		int gubun = 0;
		if (code.contains("LEDEFU")) {
			gubun = ProductRental.P_RENT_GUBUN5;
		} else if (code.contains("led-")) {
			gubun = ProductRental.P_RENT_GUBUN2;
		}

		List<HashMap<String, Object>> calList = productService.selectCalcList(gubun);

		for (int i = 0; i < calList.size(); i++) {
			Map<String, Object> mapTemp = calList.get(i);
			mapTemp.put("p_deliverycost", Common.comma(mapTemp.get("p_deliverycost") + ""));
			mapTemp.put("p_price", Common.comma(mapTemp.get("p_price") + ""));
		}
		mav.addObject("gubun", gubun);
		mav.addObject("cal", calList);
		mav.addObject("writer", map.getMap().get("login"));
		mav.addObject("pr_period", calList.get(0).get("p_rent_term"));
		return mav;
	}

	/** 어플에서 구성변경시 뜨는 견적계산기 폼 */
	@RequestMapping(value = "normalCalculateApp/{pr}", method = RequestMethod.GET)
	public @ResponseBody Object normalCalculateApp(ParamMap map, @PathVariable String pr) {
		ModelAndView mav = new ModelAndView("/product/modal/plist_APP");

		String code = productService.getProductcode(pr);

		List<HashMap<String, Object>> calList = productService.selectRentalComDetail2(Integer.parseInt(pr));
		for (int i = 0; i < calList.size(); i++) {
			Map<String, Object> mapTemp = calList.get(i);
			mapTemp.put("p_deliverycost", Common.comma(mapTemp.get("p_deliverycost") + ""));
			mapTemp.put("p_price", Common.comma(mapTemp.get("p_price") + ""));
		}
		mav.addObject("gubun", "0");
		mav.addObject("cal", calList);
		mav.addObject("writer", ((HashMap) map.get("login")).get("e_name"));
		mav.addObject("pr_period", calList.get(0).get("pr_period"));
		return mav;
	}

	/*	*//** 앱LED견적계산기 등록(신+구) *//*
									 * @RequestMapping(value = "oldcalculateApp", method = RequestMethod.POST,
									 * produces = "application/text; charset=utf8") public @ResponseBody Object
									 * oldledProductApp(ParamMap map, HttpServletResponse response) throws
									 * IOException { String rental_code = ""; String gubun =
									 * map.getMap().get("gubun")+""; // System.out.println("gubun ====="+gubun); try
									 * { if(gubun.equals("2")) { //신LED rental_code =
									 * productService.insertRentalProduct(map, ProductRental.USE_YES,gubun); }else
									 * if(gubun.equals("5")) { //구LED rental_code=
									 * productService.OldinsertRentalProduct(map, ProductRental.USE_YES, gubun);
									 * }else { String nGubun= "rental"; rental_code =
									 * productService.insertRentalProduct(map, ProductRental.USE_YES, nGubun); }
									 * 
									 * int result = csService.insertCsProduct(map); map.put("csm_idx", result);
									 * map.put("rental_code", rental_code); int result2 =
									 * csService.autoCsMatch(map.getMap());
									 * 
									 * Map<String,Object> pridx = productService.selectpridx(rental_code);
									 * 
									 * int m_idx =Integer.parseInt(map.get("cal_m_idx")+""); int mi_idx
									 * =Integer.parseInt(map.get("cal_mi_idx")+""); int pr_idx =
									 * Integer.parseInt(pridx.get("pr_idx")+"");
									 * System.out.println(m_idx+"/"+mi_idx+"/"+pr_idx);
									 * memberService.changeProductApp(pr_idx, mi_idx,m_idx); return "등록성공"; } catch
									 * (Exception e) { e.printStackTrace(); return "등록실패"; } }
									 */
	/*
	 * 상담관리, 계약고객관리 렌탈상품 구성 확인 + 월 렌탈료
	 **/
	@RequestMapping(value = "productDetail/{pr_idx}", method = RequestMethod.GET)
	public @ResponseBody Object productDetail(ParamMap map, @PathVariable String pr_idx) {
		ModelAndView mav = new ModelAndView("/customer/table/rproductDetailTR");
		List<HashMap<String, Object>> rplist = productService.rProductDetail(pr_idx);
		HashMap<String, Object> rental = productService.selectProductRentalDetail(pr_idx);
		mav.addObject("rental", rental);
		mav.addObject("rplist", rplist);
		return mav;
	}

	/** 기초데이터 조회 */
	@RequestMapping(value = "basicProduct", method = RequestMethod.GET)
	public ModelAndView basicProduct() {
		ModelAndView mav = new ModelAndView("/product/basicProduct");
		return mav;
	}

	/** 기초데이터 조회 */
	@RequestMapping(value = "basicProduct", method = RequestMethod.POST)
	public @ResponseBody Object basicProductPost(ParamMap pmap) {
		Map<String, Object> returnMap = new HashMap<String, Object>();

		String date = (String) pmap.get("date");
		String dateTo = "";
		String dateFrom = "";
		if (date == null || date.equals("")) {

		} else {
			String[] date1 = date.split("~");
			dateFrom = date1[0].trim();
			dateTo = date1[1].trim();

			pmap.put("dateTo", dateTo);
			pmap.put("dateFrom", dateFrom);
		}

		List<Map<String, Object>> plist = productService.selectBasicList(pmap.getMap());
		int filtered = 0;
		if (plist != null && plist.size() != 0) {
			for (int i = 0; i < plist.size(); i++) {
				Map<String, Object> mapTemp = plist.get(i);
				int rownum = Integer.parseInt(mapTemp.get("rownum") + "");
				if (filtered < rownum) {
					filtered = rownum;
				}
				String p_id = mapTemp.get("p_id") + "";
				String p_code = mapTemp.get("p_code") + "";
				String td = "<a p_code='" + p_code + "' onclick='basicProduct()'>" + mapTemp.get("p_name") + "</a>";

				mapTemp.put("p_name", td);
				mapTemp.put("p_id", "");

				if (mapTemp.get("p_type").equals("1")) {
					mapTemp.put("p_type", Product.P_TYPE_DELIVERY);
				} else {
					mapTemp.put("p_type", Product.P_TYPE_INSTALL);
				}
				if (mapTemp.get("p_gubun").equals("1")) {
					mapTemp.put("p_gubun", Product.P_RENT_GUNUN_DEFAULTPRODUCT);
				} else {
					mapTemp.put("p_gubun", Product.P_RENT_GUNUN_CSPRODUCT);
				}

				// 월렌탈료
				mapTemp.put("p_cost", Common.comma(mapTemp.get("p_cost") + ""));
				mapTemp.put("p_price", Common.comma(mapTemp.get("p_price") + ""));
				mapTemp.put("p_pcost", Common.comma(mapTemp.get("p_pcost") + ""));
			}
		}
		try {
			filtered += Integer.parseInt(pmap.get("start") + "");
		} catch (Exception e) {
		}

		returnMap.put("data", plist);
		returnMap.put("recordsFiltered", filtered);
		returnMap.put("recordsTotal", productService.countbasicProductList(pmap.getMap()));
		return returnMap;
	}

	/** 기초데이터 상세페이지 */
	@RequestMapping(value = "{p_code}", method = RequestMethod.GET)
	public ModelAndView basicProductComp(@PathVariable String p_code) {
		ModelAndView mav = new ModelAndView("/product/basicProductComp");
		Map<String, Object> map = productService.selectProductDetailByP_code(p_code);
		List<HashMap<String, Object>> branch = employeeService.selectBranch();
		Map<String, Object> employee = employeeService.selectEmployeeDetail(map.get("e_id") + "");

		mav.addObject("data", map);
		mav.addObject("employee", employee);
		mav.addObject("branch", branch);
		return mav;
	}

	/** 기초데이터 등록페이지 */
	@RequestMapping(value = "productBasic", method = RequestMethod.GET)
	public ModelAndView productBasic(ParamMap pmap) {
		ModelAndView mav = new ModelAndView("/product/productBasic");
		List<HashMap<String, Object>> category1 = productService.selectCategoryOfProduct();
		List<HashMap<String, Object>> p_rent_gubun = productService.selectP_rent_gubunOfProduct();
		List<HashMap<String, Object>> branch = employeeService.selectBranch();

		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
		Date currentTime = new Date();
		String mTime = mSimpleDateFormat.format(currentTime);

		mav.addObject("login", pmap.get("login"));
		mav.addObject("category1", category1);
		mav.addObject("p_rent_gubun", p_rent_gubun);
		mav.addObject("branch", branch);
		return mav;
	}

	/** 기초데이터 등록 */
	@RequestMapping(value = "productBasic", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public @ResponseBody Object productBasicPost(ParamMap pmap) {
		String p_cost = Common.replaceComma(pmap.getMap().get("p_cost") + "");
		String p_price = Common.replaceComma(pmap.getMap().get("p_price") + "");
		pmap.getMap().put("p_cost", p_cost);
		pmap.getMap().put("p_price", p_price);
		int result = productService.productBasic(pmap.getMap());

		if (result > 0) {
			return "등록 완료";
		} else {
			return "등록 실패";
		}

	}

	/** 기초데이터 수정 */
	@RequestMapping(value = "{p_code}", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public @ResponseBody Object basicProductCompPost(@PathVariable String p_code, ParamMap pmap) {

		pmap.getMap().put("p_cost", Common.replaceComma(pmap.getMap().get("p_cost") + ""));
		pmap.getMap().put("p_price", Common.replaceComma(pmap.getMap().get("p_price") + ""));
		pmap.printEntrySet();
		int result = productService.updateProduct(pmap.getMap());

		if (result > 0) {
			return "등록 완료";
		} else {
			return "등록 실패";
		}

	}

	/** 렌탈상품명 중복방지 */
	@RequestMapping(value = "duplicate", method = RequestMethod.GET, produces = "Application/text;charset=utf8")
	public @ResponseBody Object duplicate(ParamMap pmap) {
		int result = productService.selectProudctNameDuplicate(pmap.getMap());
		if (result > 0) {
			return "중복";
		}
		return "신규";
	}

}
