package rental.common;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import rental.dao.CommonDAO;

public class CustomMapArgumentResolver implements HandlerMethodArgumentResolver {
	
	@Autowired
	CommonDAO commonDAO;
	public boolean supportsParameter(MethodParameter parameter) {
		 return ParamMap.class.isAssignableFrom(parameter.getParameterType());
	}

   public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
	  ParamMap commandMap = new ParamMap();
        
      HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
      Enumeration<?> enumeration = request.getParameterNames();
       
      String key = null;
      String[] values = null;
      commandMap.put("login", request.getSession().getAttribute("login"));
       
//      List<String> columns=commonDAO.intColumn();
       
      while(enumeration.hasMoreElements()){
    	  key = (String) enumeration.nextElement();
          values = request.getParameterValues(key);
          if(values != null){
        	  if(key.equals("start")||key.equals("length")) {
        		  commandMap.put(key, Integer.parseInt(values[0]));
        	  }else if(key.equals("search[value]")){
        		  values[0]=values[0].replaceAll("%", "&#37;");
        		  commandMap.put("keyword", "%"+values[0]+"%");
        	  }else{
        		  if(values.length > 1){
        			  commandMap.put(key,  values);
        		  }else {
        			   String value=values[0]+"";
            		   value=value.replaceAll("<", "&lt;");
            		   value=value.replaceAll("%", "&#37;");
            		   value=value.replaceAll(System.getProperty("line.separator"), "<br>");
            		   //value=value.replaceAll("[.]", "");
            		  
//            		   if(columns.contains(key)) {
//            			   value=value.replaceAll("[^0-9]", "");
//            		   }
            		   commandMap.put(key,  value.trim());
        		   }
        	   }
           }
       }
       return commandMap;
   }

}
