package rental.common;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import rental.service.EmployeeService;
import rental.service.MenuService;


public class MenuInterceptor implements HandlerInterceptor {

	@Autowired
	MenuService menuService;
	@Autowired
	EmployeeService employeeService;
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//response.sendRedirect("index.do");
		//return false;
		return true;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//menu 권한 적용
		Map<String,Object> loginMap=(Map<String,Object>)request.getSession().getAttribute("login");
		if(loginMap!=null) {
			//권한 정보 가져오기
			String auth=employeeService.getAuth(loginMap.get("e_logid")+"");
			
			String uri=request.getRequestURI();
			uri=uri.replace("/rentalspring/", "");
			request.setAttribute("menuMap", menuService.menuList(auth,uri));
			//System.out.println("requrl:"+request.getRequestURI());
		}
		
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

}
