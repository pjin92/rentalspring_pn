package rental.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginCheckFilter implements Filter{

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req=(HttpServletRequest)request;
		String uri=req.getRequestURI();
		HttpSession ses=req.getSession();
		
		if(uri.contains(".")) {
			//((HttpServletResponse)response).sendRedirect("wrongUrl");
			//return;
			
		}else if(uri.startsWith("index")||uri.startsWith("/index")){
			//index.do 로그아웃 처리까지
			String loc=(String)ses.getAttribute("loginLocation");
			ses.invalidate();
			if(loc!=null&&!loc.equals("")) {
				request.setAttribute("loc", loc);
			}
		}else if(uri.startsWith("login")||uri.startsWith("/login")||uri.startsWith("/tera")||uri.startsWith("/Applogin")) {
			//로그인 세션 체크 패스
			
		}else if(uri.equals("/product/calculate")) {
			
		}else if(uri.equals("/product/oldcalculate")) {
			
		}else if (uri.contains("/product/rentalProductComp/")) {
			
		}else if (uri.contains("/indexApp")) {
			
		}else if(uri.contains("/fcm")) {
			
		}else{
			if(ses.getAttribute("login")==null) {
				ses.setAttribute("loginLocation", uri);
				((HttpServletResponse)response).sendRedirect("/index");
				return;
			}
			ses.setMaxInactiveInterval(60*60*10);
		} 
		chain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {
	}
	public void destroy() {
	}

}
