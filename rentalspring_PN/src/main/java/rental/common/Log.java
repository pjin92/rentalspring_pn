package rental.common;

import org.apache.log4j.Logger;

public class Log {
	
	private static Logger log=Logger.getLogger(Logger.class);
	
	public Log() {
		
	}
	
	static public void warn(Object msg){
		log.warn(msg);
	}
}
