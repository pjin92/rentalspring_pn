package rental.common;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Common {

	public static String comma(String str) {
		if(str==null||str.equals("")) {
			str="0";
		}
		try {
			str=str.replaceAll("[^0-9]", "");
			DecimalFormat df=new DecimalFormat("#,###,###,###,###,###");
			str=df.format(Integer.parseInt(str));
		}catch (Exception e) {
			str="0";
		}
		return str;
	}
	
	public static String replaceComma(String str) {
		if(str == null || str.equals("")) {
			str="0";
		}
		
		try {
			str = str.replaceAll("[,]", "");
		} catch (Exception e) {
			e.printStackTrace();
			return str;
		}
		return str;
	}
	
	/**2자리 숫자의 string으로 만들기 ex) 2는 02로 ,12는 12그대로*/
	public static String dateStr(String num) {
		num="0"+Integer.parseInt(num);
		return num.substring(num.length()-2);
	}
	
	
	
	 public static int getDateByInteger(Date date) {
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	        return Integer.parseInt(sdf.format(date));
	    }
	     
	    public static String getDateByString(Date date) {
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        return sdf.format(date);
	    }

}
