package rental.common;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**카드api시 hostname api.efnc.co.kr가 아니면 허용안함.<br>
 * 또는 setHostname으로 호스트네임세팅 */
public class CardHV implements HostnameVerifier {
	
	private String hostname;
	
	public void setHostname(String hostname) {
		this.hostname=hostname;
	}
	
	public boolean verify(String hostname, SSLSession session) {
		if(this.hostname==null&&hostname.equals("api.efnc.co.kr")) {
    		return true;
    	}else if(this.hostname!=null&&hostname.equals(this.hostname)){
    		return true;
    	}else{
    		return false;
    	}
	}

}
