<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<h4 class="page-title"> 
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">기초상품 상세&amp;수정</span><small class="display-block"> 상세&amp;수정</small>
						</h4>
					</div>
				</div>
				<!-- /page header -->
				<c:set var="product" value="${data }"/>
				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 기초상품 등록정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>기초 상품명</label> 
														<input type="text" name="p_name" class="form-control" value="${product.p_name }" readonly> 
														<input type="hidden" name="p_id" value="${product.p_id }">
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default" style="background-color: #E5E5E5">
														<label>기초상품코드</label> 
														<input type="text" name="p_code" class="form-control" value="${product.p_code }" readonly>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>매입처</label> 
														<select name="c_id" class="select select2-hidden-accessible" disabled>
														<c:forEach var="branch" items="${branch}">
															<c:if test="${branch.c_id eq product.c_id }">
															<option value="${branch.c_id}"selected>${branch.comname}</option>														
															</c:if>
															<c:if test="${branch.c_id ne product.c_id }">
															<option value="${branch.c_id}">${branch.comname}</option>														
															</c:if>
														</c:forEach>
														</select>
													</div>
												</div> 
													
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>기초상품구분</label> 	
														 <select name="p_gubun"	class="select select2-hidden-accessible" disabled>
																<c:if test="${product.p_gubun eq '1' }">
																<option value="1"selected>일반상품</option>
																<option value="2">CS소모품</option>
																</c:if>
																<c:if test="${product.p_gubun eq '2' }">
																<option value="1">일반상품</option>
																<option value="2"selected>CS소모품</option>
																</c:if>
														 </select>
													</div>
												</div> 
												 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>기초상품타입</label> 	
														 <select name="p_type"	class="select select2-hidden-accessible" disabled>
																<c:if test="${product.p_type eq '2' }">
																<option value="2" selected>설치</option>
																<option value="1">배송</option>
																</c:if>
																<c:if test="${product.p_type eq '1' }">
																<option value="2" >설치</option>
																<option value="1"selected>배송</option>
																</c:if>
														 </select>
													</div>
												</div> 
												
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>기초상품 상태</label> 	
														 <select name=p_condition	class="select select2-hidden-accessible" disabled>
																<c:if test="${product.p_condition eq '1' }">
																 <option value="1" selected> 판매 </option>
																 <option value="2"> 판매중지</option>
																</c:if>
																<c:if test="${product.p_condition eq '2' }">
																 <option value="1" > 판매 </option>
																 <option value="2"selected> 판매중지</option>
																</c:if>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 기초상품 가격정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>판매가</label> 
														<input type="text" name="p_price" value="${product.p_price_f}" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>매입가</label> 
														<input type="text" name="p_cost" value="${product.p_cost_f}" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" readonly>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 기초상품 추가정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>내용량</label> 
														<input type="text" name="p_volume" value="${product.p_volume }" class="form-control"readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>규격</label> 
														<input type="text" name="p_composition" value="${product.p_composition }" class="form-control"readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>수량단위</label> 
														<input type="text" name="p_unit" value="${product.p_unit }" class="form-control"readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>유통기한(일)</label> 
														<input type="text" name="p_edate" value="${product.p_edate }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>메모</label> 
														<input type="text" name="p_memo" class="form-control">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<a class="heading-elements-toggle"><i class="icon-more"></i></a>
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>	
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//등록
	 function startRental(){
   	var btn=$(event.target).html();
   	var bttn = btn.substring(0,2);

   	var p_code = $('input[name=p_code]');
   	if(bttn=='확인'){
   		if(window.confirm('수정하시겠습니까?')){
   			var param=$('form[name=frm]').serialize();
   			var setting={
   				data:param,
   				url:"/product/"+p_code,
   				type:"post",
   				enctype:"multipart/form-data"
   			};
   			$.ajax(setting).done(function(data){
   				if(data.trim()=='등록 완료'){
   					alert('수정하였습니다.');
					opener.parent.refreshTable();
   					window.close();
   				}else{
   					alert('수정 실패하였습니다.');
   					opener.parent.refreshTable();
   					window.close();
   				}
   				
   			});
   		}
   	}else{
   		$(event.target).html('확인');
   		$(event.target).removeClass('blue-hoki');
   		$(event.target).addClass('btn-danger');
   		$("input[name=p_name]").removeAttr('readonly');
   		$("input[name=p_volume]").removeAttr('readonly');
   		$("input[name=p_unit]").removeAttr('readonly');
   		$("input[name=p_cost]").removeAttr('readonly');
   		$("input[name=p_price]").removeAttr('readonly');
   		$("input[name=p_composition]").removeAttr('readonly');
   		$("input[name=p_edate]").removeAttr('readonly');
   		$("select[name=p_type]").removeAttr('disabled');
   		$("select[name=p_condition]").removeAttr('disabled');
   		$("select[name=p_gubun]").removeAttr('disabled');
   		$("select[name=c_id]").removeAttr('disabled');
   	}
   	
   }
	</script>
</body>
</html>