<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<h4 class="page-title"> 
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">기초 상품 등록</span><small class="display-block">등록</small>
						</h4>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 기초상품 등록정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>기초상품명</label> 
														<input type="text" name="p_name" class="form-control"> 
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>기초상품코드</label> 
														<input type="text" name="p_code" class="form-control" placeholder="등록 후 자동생성" readonly >
													</div>
												</div> 
													
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>매입처</label> 
														<select name="c_id" class="select select2-hidden-accessible">
														<option value="" selected>선택</option>														
														<c:forEach var="branch" items="${branch}">
															<option value="${branch.c_id}">${branch.comname}</option>														
														</c:forEach>
														</select>
													</div>
												</div> 
												 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>기초상품구분</label> 	
														 <select name="p_gubun"	class="select select2-hidden-accessible">
																
																<option value="1"selected>일반상품</option>
																<option value="2">CS소모품</option>
														 </select>
													</div>
												</div> 
												
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>기초상품타입</label> 	
														 <select name="p_type"	class="select select2-hidden-accessible">
																<option value="2" selected>설치</option>
																<option value="1">배송</option>
														 </select>
													</div>
												</div> 
												
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>기초상품 상태</label> 	
														 <select name=p_condition	class="select select2-hidden-accessible">
															 <option value="1" selected> 판매 </option>
															 <option value="2"> 판매중지</option>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 기초상품 가격정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>판매가</label> 
														<input type="text" name="p_price" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>매입가</label> 
														<input type="text" name="p_cost" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 기초상품 추가정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>내용량</label> 
														<input type="text" name="p_volume"  class="form-control">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>규격</label> 
														<input type="text" name="p_composition"  class="form-control">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>수량단위</label> 
														<input type="text" name="p_unit"  class="form-control">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>유통기한(일)</label> 
														<input type="text" name="p_edate" class="form-control">
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>메모</label> 
														<input type="text" name="p_memo" class="form-control">
													</div>
												</div>
												
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<a class="heading-elements-toggle"><i class="icon-more"></i></a>
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger" onclick="startRental()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
						</div>	
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	function startRental(){
		var p_type = $('select[name=p_type]').val();
		var p_rent_gubun = $('select[name=p_rent_gubun]').val();
		var p_name = $('input[name=p_name]').val();
		var p_code = $('input[name=p_code]').val();
		var p_category1 = $('input[name=p_category1]').val();
		var p_cost = $('input[name=p_cost]').val();
		var p_price = $('input[name=p_price]').val();
		var p_rent_term = $('input[name=p_rent_term]').val();
		var p_rent_money = $('input[name=p_rent_money]').val();
		
		
		if(p_type == 0){
			alert("상품타입을 선택하여 주시기 바랍니다.");
			return false;
		}
		
		if(p_rent_gubun == 0){
			alert("렌트타입을 선택하여 주시기 바랍니다.");
			return false;
		}

		if(p_name == 0){
			alert("상품명을 입력하여주시기 바랍니다.");
			return false;
		}
		
		if(p_category1 == 0){
			alert("상품군을 선택하여 주시기 바랍니다.");
			return false;
		}
		
		if(p_rent_term == 0){
			alert("원가를 입력하여 주시기 바랍니다.");
			return false;
		}
		
		if(p_price == 0){
			alert("판매가를 입력하여주시기 바랍니다.");
			return false;
		}
		
		if(p_rent_money == 0){
			alert("월렌탈료를 입력하여주시기 바랍니다.");
			return false;
		}
		
		var param=$('form[name=frm]').serialize();
		$.ajax({
			url:'/product/productBasic',
			type:'post',
			data:param
		})
		.done(function(data){
			if(data.trim()=='등록 완료'){
				alert("기초상품이 등록되었습니다.")
				window.opener.refreshTable();
				window.close();
				
			}else{
				window.opener.refreshTable();
				window.close();
			}
		});
	}
	
	</script>
</body>
</html>