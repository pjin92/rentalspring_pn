<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">렌탈 상품 상세 <!-- &amp; 수정 --></span><small class="display-block">상세<!--  &amp; 수정 --></small>
						</h4>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<c:set var="rp" value="${plist}"></c:set>
						<div class="col-xs-12">
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left"></i>1. 렌탈 상품 정보
									</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
										
										
											<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>상품대분류코드</label> 	
														 <select name="pr_category"	class="select select2-hidden-accessible">
															 <option value ="1" selected>생활용품 </option>
															 <option value ="2">가전용품</option>
														 </select>
													</div>
											</div> 
											<div class="col-xs-6 col-sm-3 col-md-3">
												<div class="form-group form-group-default ">
													<label>렌탈상품코드</label> 
													<input type="text"name="pr_rentalcode" value="${rp.pr_rentalcode }"class="form-control" readonly />
												</div>
											</div>
											<div class="col-xs-6 col-sm-3 col-md-3">
												<div class="form-group form-group-default ">
													<label>렌탈 상품명</label> 
													<input type="text" name="pr_name" value="${rp.pr_name }" class="form-control" readonly />
												</div>
											</div>
											
											<div class="col-xs-6 col-sm-3 col-md-3">
												<div class="form-group form-group-default">
													<label>상품상태구분</label>
													<select class="select select2-hidden-accessible" name="pr_type" class="form-control">
														<c:choose>
															<c:when test="${rp.pr_type eq '1' }">
															<option value="1" selected>설치</option>
															<option value="2">배송</option>
															</c:when>
															<c:when test="${rp.pr_type eq '2' }">
															<option value="1">설치</option>
															<option value="2" selected>배송</option>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xs-12">
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left"></i>렌탈 상품 구성
									</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12">
												<table class="table table-hover">
													<thead class="alpha-slate">
														<tr>
															<th>상품명</th>
															<th>상품코드</th>
															<th>매입처</th>
															<th>매입가</th>
															<th>판매가</th>
															<th>수량</th>
														</tr>
													</thead>
													<tbody id="rproduct">
														<c:forEach var="rpd" items="${data}">
														<tr>
															<td>${rpd.p_name}<input type="hidden" name="p_id${rpd.p_id}" value="${rpd.p_id}"></td>
															<td>${rpd.p_code}</td>
															<td>${rpd.c_comname}</td>
															<td>${rpd.p_cost_f}</td>
															<td>${rpd.p_price_f }</td>
															<td>${rpd.prp_num}</td>
														</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<form role="form" name="frm" method="post" enctype="multipart/form-data"> 
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 재휴카드정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>제휴카드</label> 
														<input type="text" name="pr_aff" value="${rp.pr_aff}" class="form-control" readonly />
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>제휴카드 할인가</label> 
														<input type="text" name="pr_aff_cost" value="${rp.pr_aff_cost}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 배송 / 설치 비용정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>배송비(기본)</label> 
														<input type="text" name="pr_delivery1" value="${rp.pr_delivery1}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>배송비 (도서,산간)</label> 
														<input type="text" name="pr_delivery2" value="${rp.pr_delivery2}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>설치비(기본)</label> 
														<input type="text" name="pr_install1" value="${rp.pr_install1}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>설치비(도서,산간)</label> 
														<input type="text" name="pr_install2" value="${rp.pr_install2}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 할인정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>구분</label> 
														<c:choose>
															<c:when test="${rp.pr_gubun eq 1 }">
															<select class="select select2-hidden-accessible" name="pr_gubun" class="form-control">
																<option value="0">선택</option>
																<option value="1" selected>가격할인</option>
																<option value="2">할인률</option>
																<option value="3">렌탈료 면제</option>
															</select>
															</c:when>
															
															<c:when test="${rp.pr_gubun eq 2}">
															<select class="select select2-hidden-accessible" name="pr_gubun" class="form-control">
																<option value="0">선택</option>
																<option value="1">가격할인</option>
																<option value="2" selected>할인률</option>
																<option value="3">렌탈료 면제</option>
															</select>
															</c:when>
															
															<c:when test="${rp.pr_gubun eq 3 }">
															<select class="select select2-hidden-accessible" name="pr_gubun" class="form-control">
																<option value="0">선택</option>
																<option value="1">가격할인</option>
																<option value="2">할인률</option>
																<option value="3" selected>렌탈료 면제</option>
															</select>
															</c:when>

															<c:otherwise>
															<select class="select select2-hidden-accessible" name="pr_gubun" class="form-control">
																<option value="0" selected>선택</option>
																<option value="1">가격할인</option>
																<option value="2">할인률</option>
																<option value="3">렌탈료 면제</option>
															</select>
															</c:otherwise>
														</c:choose> 
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈가격할인</label> 
														<input type="text" name="pr_rentalDC" value="${rp.pr_rentalDC}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>할인률</label> 
														<input type="text" name="pr_rentalDCp" value="${rp.pr_rentalDCp}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈료 면제</label> 
														<input type="text" name="pr_rentalEXE" value="${rp.pr_rentalEXE}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>6. 렌탈 가격정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈기간</label> 
														<input type="text" name="pr_period" value="${rp.pr_period}" onkeydown="numKeyDown()" class="form-control" readonly /> 
														<input type="hidden" name="pr_idx" value="${rp.pr_idx }">
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈총액</label> 
														<input type="text" name="pr_total" value="${rp.pr_total}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>월렌탈료</label> 
														<input type="text" name="pr_rental" value="${rp.pr_rental}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>일시불판매가</label> 
														<input type="text" name="pr_ilsi" value="${rp.pr_ilsi}" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" readonly />
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<a class="heading-elements-toggle"><i class="icon-more"></i></a>
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</form>
					</div>
				</div> 
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
		function plist() {
			$('#modal_large').modal('show');
		}

		function refreshTable() {
			var table = $('#productTable').DataTable();
			table.ajax.reload(null, false);
		}

		function startRental() {
			var btn = $(event.target).html();
			var bttn = btn.substring(0, 2);
			if (bttn == '확인') {
				if (window.confirm('수정하시겠습니까?')) {
					var param = $('form[name=frm]').serialize();
					var setting = {
						data : param,
						url : "productRentalModify",
						type : "post",
						enctype : "multipart/form-data"
					};
					$.ajax(setting).done(function(data) {
						if (data.trim() == '등록 완료') {
							alert('수정하였습니다.');
							opener.parent.location.reload();
							window.close();
						} else {
							alert('수정 실패');
							window.close();
						}

					});
				}
			} else {
				$(event.target).html('확인');
				$(event.target).removeClass('blue-hoki');
				$(event.target).addClass('btn-danger');

				$('form[name=frm] input').removeAttr('readonly');
			}

		}
	</script>
</body>
</html>