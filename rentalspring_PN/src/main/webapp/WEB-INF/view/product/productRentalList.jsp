<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>렌탈상품</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">렌탈상품</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
								<div class="panel-body">
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">렌탈상품명</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="pr_name" >
							                        <!-- <span class="help-block">상품명, 모델명 등</span> -->
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">렌탈상품코드</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="pr_rentalcode" >
							                        <!-- <span class="help-block">상품명, 모델명 등</span> -->
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">작성일</label>
													<input type="text" class="form-control input-sm daterange-blank" name="date" id="date"> 
							                        <!-- <span class="help-block">상품명, 모델명 등</span> -->
												</div>
											</div> 
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
				                                    <label class="control-label col-md-5 col-lg-4 form-tit-label">대분류 선택</label>       
													<select class="form-control input-sm" id="main" name="sp_mm_idx" onchange="change()">
														<option value="0" selected>선택</option>
														<c:forEach items="${bsort}" var="i">
														<option value="${i.sp_mm_idx}" ${i.sp_mm_idx eq sd.sp_mm_idx ? 'selected':''}>${i.sp_mm_name}</option>
														</c:forEach>
													</select>         
					                           	</div>
											</div>
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
												<!-- <a onclick="javascript:popAddExcel()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>엑셀</a> -->
											</div>
										</div>					
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>렌탈상품 조회결과</h5>
									<div class="heading-elements">
										
										<!-- <div class="btn-group">
											<button type="button" class="btn bg-teal btn-labeled legitRipple" onclick="addOLedProduct()"><b><i class="icon-plus3"></i></b>구 LED신규등록</button>
											<button type="button" class="btn bg-teal-400  legitRipple" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="javascript:;" onclick="addProduct()"><i class="icon-mail5"></i> 여러 개인 경우</a></li>
												<li><a href="javascript:;" onclick="excelUpload()"><i class="icon-file-empty"></i>엑셀업로드</a></li>
												<li><a href="javascript:;" onclick="popup1()"><i class="icon-screen-full"></i> 기타 신규등록</a></li>
												<li class="divider"></li>
												<li><a href="javascript:;"><i class="icon-gear"></i> Separated line</a></li> 
											</ul>
										</div> -->
										<div class="btn-group">
											<button type="button" class="btn bg-teal-400 btn-labeled legitRipple" onclick="addProduct()"><b><i class="icon-plus3"></i></b>신규등록</button>
											<button type="button" class="btn bg-teal-400 dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu dropdown-menu-right">
												<!-- <li><a href="javascript:;" onclick="addProduct()"><i class="icon-mail5"></i> 여러 개인 경우</a></li> -->
												<li><a href="javascript:;" onclick="excelUpload()"><i class="icon-file-empty"></i>엑셀업로드</a></li>
												<!-- <li><a href="javascript:;" onclick="popup1()"><i class="icon-screen-full"></i> 기타 신규등록</a></li> -->
												<!-- <li class="divider"></li>
												<li><a href="javascript:;"><i class="icon-gear"></i> Separated line</a></li> -->
											</ul>
										</div>
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		
	
								<!-- <div class="panel-body pull-right">
									<kbd>i</kbd> 열 선택에서 항목이 선택되어 테이블 상에서 현재 보이는 값만 엑셀파일로 다운됩니다. <br> <kbd>i</kbd> 테이블 각 열의 제목을 크릭하고 드래그하면 열의 순서가 바뀌며, 바뀐 상태로 엑셀다운이 됩니다. 
								</div> -->
	
								<table class="table table-hover" id="productTable" data-page-length="25">
									<thead>
										<tr>
											<th>선택</th>
											<th>작성일</th>
											<th>렌탈상품명</th>
											<th>렌탈상품코드</th>
											<th>렌탈기간</th> 
											<th>렌탈총액</th>
											<th>월렌탈료</th>
											<th>배송비(기본)</th>
											<th>설치비(기본)</th>
											<th>렌탈구성상품</th> 
											<th>CS항목</th>
											<th>최종변경자</th>
											<th>최종변경일</th> 
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="productDel('display', '1')"><i class="icon-eye"></i>진열</a></li>
												<li><a onclick="productDel('display', '0')"><i class="icon-eye-blocked"></i>미진열</a></li>
												<hr>
												<li><a onclick="productDel('sel', '1')">판매중</a></li>
												<li><a onclick="productDel('sel', '0')">판매안함</a></li>
												<hr> 
												<li><a onclick="productDel('copy', 'copy')"><i class="icon-copy3"></i>복제</a></li>
												<li><a onclick="productDel('del', 'del')"><i class="icon-trash-alt"></i>삭제</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		   { "data": "pr_idx" }, //선택 
		    { "data": "pr_createDate"},//작성일
		    { "data": "pr_name"},//상품명
		    { "data": "pr_rentalcode"}, //렌탈상품코드
		    { "data": "pr_period"}, //렌탈기간
		    
		    { "data": "pr_total"}, //렌탈총액
		    { "data": "pr_rental"}, //월렌탈료
		    { "data": "pr_delivery1"}, //배송비
		    { "data": "pr_install1"},//설치비
		    { "data": "pr_count"}, //렌탈구성상품
		    
		    { "data": "csAdd"}, //CS항목
		    { "data": "pr_modifier"}, //최종변경자
		    { "data": "pr_log"} //최종변경일
	   		
		  ]
	});
	tableLoad('productTable');
	//첫 dataTable initialize
	//이부분이 데이터테이블에 들어가는 데이터들 가져옴
	if(! $.fn.DataTable.isDataTable('#productTable')){
		checkDt('productTable','/product/productRentalList');
	}
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#productTable').DataTable();
		table.ajax.url('/product/productRentalList?'+param).load(null,false);
	} 
	function addProduct(){
		window.open('productRental','popup','width=1200,height=800');
	}
	
	function addLedProduct(){
		window.open('calculate','calculate','width=1200,height=800');
	}
	function addOLedProduct(){
		window.open('oldcalculate','calculate','width=1200,height=800');
	}
	//현재 보이는 dataTable refresh
	function refreshTable(){
		var table=$('#productTable').DataTable();
		table.ajax.reload(null,false);
	}
	
	function rentalProduct(){
		var pr_rentalcode=$(event.target).attr('pr_rentalcode');
		window.open('/product/rentalProductComp/'+pr_rentalcode,'popup','width=1200,height=800');
	}
	
	function csProduct(){
		var pr_idx=$(event.target).attr('pr_idx');
		window.open('/cs/csRentalManage/'+pr_idx,'popup','width=1200,height=800');
	}
	function csMatchProduct(){
		var pr_idx=$(event.target).attr('pr_idx');
		window.open('/cs/csMatchDetail/'+pr_idx,'popup','width=1200,height=800');
	}
	
	
	function popAddExcel(){
		//var excelUrl 을 상위 jsp에서 정의
		
			cw=screen.availWidth; 
			ch=screen.availHeight; 
	
			sw=1024;    //띄울 창의 넓이
			sh=screen.height - 440;    //띄울 창의 높이
	
			ml=(cw-sw)/2; 
			mt=(ch-sh)/2;
			
			window.open('/product/excel','add','width='+sw+',height='+sh+',top='+mt+',left='+ml+',resizable=yes, scrollbars=yes');
	}
	</script>

</body>
</html>