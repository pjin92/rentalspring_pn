<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">렌탈 상품 등록 1</span> <small class="display-block">등록</small>
						</h4>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 렌탈 상품 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈 상품명</label> 
														<input type="text" name="pr_name" class="form-control"> 
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈상품코드</label> 
														<input type="text" name="pr_rentalcode" class="form-control">
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>생성자</label> 
														<c:set var="pr" value="${writer}"></c:set>
														<input type="text" name="pr_creater" value="${pr.e_name}" readonly class="form-control">
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 렌탈 상품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable" >
														<thead>
															<tr>
																<th>상품명</th>
																<th>렌탈료</th>
																<th>렌탈기간</th>
																<th>설치비</th>
																<th style="width:10%">수량</th>
																<!-- <th>삭제</th> -->
															</tr>
														</thead>
														<tbody id="rproduct">
															<c:forEach var="cal" items="${cal}" varStatus="status" begin="1" step="1">
															<tr>
																<td>${cal.p_name}</td>
																<td>${cal.p_price }</td>
																<td>${cal.p_rent_term}</td>
																<td>${cal.p_deliverycost }</td>
																<td>
																	<input type="text" name="${cal.p_id}num" onkeydown="numKeyUp()" onkeyup="javascript:cal('${cal.p_price}', '${cal.p_id}','${cal.p_deliverycost}');numCommaKeyUp();" class="form-control input-sm" value="0">
																	<input type ="hidden" class="form-control input-sm"  id="${cal.p_id}no" name="${cal.p_id}no" value="0" onkeydown="numKeyUp()" onkeyup="numCommaKeyUp();">
																</td> 																
																<!-- <td>
																	<input type="button" onclick="del()" value="삭제">
																</td>   -->	
															</tr>
															</c:forEach> 
														</tbody>
														<!-- <tfoot>
															<tr>
																<td colspan="5" class="no-padding no-border"><a onclick="plist()" class="btn btn-sm bg-orange-600">상품 추가</a></td>
															</tr>
														</tfoot> -->
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 렌탈 상품 가격 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈기간</label> 
														<input type="text" name="pr_period" value="39" onkeyup="numCommaKeyUp()"
															class="form-control" onkeydown="numKeyDown()" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈총액</label> 
														<input type="text" id ="pr_total" name="pr_total" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" style = "text-align:right;">
													</div>
													<input type="hidden" id="temp" name="temp" value="0" onkeydown="numKeyDown()" readonly>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label>월렌탈료</label> 
														<input type="text" id="pr_rental" name="pr_rental" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" style = "text-align:right;" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label>일시불판매가</label> 
														<input type="text" name="pr_ilsi" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label>제휴카드</label> 
														<input type="text" name="pr_aff" class="form-control">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label>제휴카드 할인가</label> 
														<input type="text" name="pr_aff_cost" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" >
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 배송 / 설치비
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>배송비(기본)</label> 
														<input type="text" name="pr_delivery1" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>배송비 (도서,산간)</label> 
														<input type="text" name="pr_delivery2" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>설치비</label> 
														<input type="text" name="pr_install1" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>설치비(도서,산간)</label> 
														<input type="text" name="pr_install2" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 이벤트 할인
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>구분</label> 	
														 <select name="pr_gubun" onchanged="select()"class="select select2-hidden-accessible" >
															 <option value="0" selected> 선택 </option>
															 <option value ="1">가격할인 </option>
															 <option value ="2">할인률</option>
															 <option value ="3">렌탈료 면제 </option>
														 </select>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>렌탈가격할인</label>
														<input type="text" id="pr_rentalDC" name="pr_rentalDC" value="0" onkeyup="discount()" class="form-control" onkeydown="numKeyDown()"> 				
														<input type="hidden" id ="dc" name="dc" value="0">	
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>할인률</label> 
														<input type="text" name="pr_rentalDCp" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>렌탈료 면제 개월수</label> 
														<input type="text" name="pr_rentalEXE" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<button type="button" class="btn btn-sm btn-default ml-20" onclick="window.close()">닫기</button>
											<div class="heading-btn pull-right">
												<a class="btn btn-sm btn-danger" onclick="startRental()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	function startRental(){
		var param=$('form[name=frm]').serialize();
		$.ajax({
			url:'productRental',
			type:'post',
			data:param
		})
		.done(function(data){
			alert(data);
			if(data.trim()=='등록 완료'){
				window.close();
				refreshTable();
			}else{
				window.close();
				refreshTable();
			}
					
		});
	};
	
	function cal(pri, pid, ins){
	
		var a = onlyNum(pri);
		var b = onlyNum(ins);
		
		var price = a.replace(/,/g, "");
		var install = b.replace(/,/g, "");
	
		
		var num = $('input[name='+pid+'num]').val(); // 갯수 받아오기
		var no = $('input[name='+pid+'no]').val(); // 갯수 저장하기
		var total = price*num; // 가격 * 갯수
		var total2 = install*num; // 설치비 * 갯수
		
		var temp2 = $('#temp').val(); // 토탈가격 임시 저장
		
		if(isNaN(temp2)){
			alert("숫자만 입력하세요");
			$("#temp").val("0");
			$('input[name='+pid+'num]').val("0"); // 갯수 받아오기
			$('input[name='+pid+'no]').val("0"); // 갯수 저장하기
		}
		var temp = Number(total)+Number(temp2)+Number(total2); //저장된 가격과 새로 받아온 가격 더해주기
		
		
		if(num == "0"){
			var tempTotal = Number(temp)-Number(price*no)-Number(install*no);
			var result = comma(Number(tempTotal).toFixed(0));
			$('input[name='+pid+'no]').val(num);
			$('#pr_total').val(result);
			$("#temp").val(tempTotal);
			$('#pr_rental').val(comma(Number(tempTotal/39).toFixed(0)));
		}
		else if(num != no){
			var tempTotal = Number(temp)-Number(price*no)-Number(install*no);
			if(isNaN(tempTotal)){
				alert("숫자만 입력하세요");
				$('input[name='+pid+'num]').val("0"); // 갯수 받아오기
				$('input[name='+pid+'no]').val("0"); // 갯수 저장하기
				$("#temp").val("0");
			}
			else{
			//tempTotal = Number(tempTotal)+ Number(temp);
			var result = comma(Number(tempTotal).toFixed(0));
			$('input[name='+pid+'no]').val(num);
			$('#pr_total').val(result);
			$("#temp").val(tempTotal);
			$('#pr_rental').val(comma(Number(tempTotal/39).toFixed(0)));
			}
		}
	};
	
	function discount(){
	 	
		
		
		
		var pr_rental = $('#pr_rental').val(); //렌탈 총액
		var pr_rentalDC = onlyNum($('#pr_rentalDC').val());
	
		var rental = pr_rental.replace(/,/g, "");
		var dis_pee = $('#pr_rentalDC').val();
		var dis = "";
		var pre = $("#dc").val();
		var pre_dis = Number(pre);
		
		
		if(pre_dis == '0'){
			 dis = Number(rental)-Number(dis_pee);
		}
		else{
			 dis = Number(rental)-Number(dis_pee)+Number(pre_dis);
		}
		var result = comma(Number(dis).toFixed(0));
		$('#pr_rental').val(result); 
		$('#dc').val(dis_pee);
	}


	</script>
</body>
</html>