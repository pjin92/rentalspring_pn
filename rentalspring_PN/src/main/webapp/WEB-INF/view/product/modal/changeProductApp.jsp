<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form role="form" name="frm" method="post" enctype="multipart/form-data">
	<!-- 렌탈 상품 정보 -->
	<div class="ui-grid-solo">
		<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. 렌탈 상품 정보</h4>
		<div class="form-group">
			<label for="pr_name">렌탈 상품명</label>
			<input type="text" name="pr_name" id="pr_name" class="">
		</div>
		<!-- <div class="form-group">
			<label for="pr_rentalcode">렌탈 상품코드</label>
			<input type="text" name="pr_rentalcode" id="pr_rentalcode" placeholder="상품등록 후 생성됩니다." readonly >
		</div> -->
		<%-- <div class="form-group">
			<label for="pr_creater">생성자</label>
			<c:set var="pr" value="${writer}"></c:set>
			</div> 
	</div>
		--%>
			<input type="hidden" name="pr_creater" id="pr_creater" value="${pr.e_name}" readonly >
			<input type="hidden" id="cal_m_idx" name="cal_m_idx">
			<input type="hidden" id="cal_mi_idx" name="cal_mi_idx">

	<!--// 렌탈 상품 정보 -->
	<!-- 렌탈 상품 구성 -->
	<div class="ui-grid-solo margin-25px-top">
		<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;2. 렌탈 상품 구성</h4>
		<table id="productTable" class="table">
			<thead class="bg-light-gray">
				<tr>
					<th>상품명</th>
<!-- 					<th>렌탈료</th> -->
					<th>렌탈기간</th>
					<th style="width:60px">수량</th>
				</tr>
			</thead>
			<tbody id="rproduct" class="bg-white">
			</tbody>
			<!-- /product/modal/plist_APP -->
		</table>
	</div>
	<!--// 렌탈 상품 구성 -->
	<!-- 렌탈 상품 가격 정보 -->
	<div class="ui-grid-solo margin-25px-top">
		<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;3. 렌탈 상품 가격 정보</h4>
		<div class="form-group">
			<label for="pr_period">렌탈 기간</label>
			<input type="text" name="pr_period" id="pr_period" value="" onkeyup="numCommaKeyUp()" onkeydown="numKeyDown()" readonly>
		</div>
		<div class="form-group">
			<label for="pr_install">설치비</label>
			<input type="text" id="pr_install" name="pr_install" value="0" onkeyup="rental();numCommaKeyUp();" onkeydown="numKeyDown()" >
			<input type="hidden" id="num_temp" name="num_temp" value="0" onkeydown="numKeyDown()" readonly>
			<input type="hidden" id="install_temp" name="install_temp" value="0" onkeydown="numKeyDown()" readonly>
		</div>
		<div class="form-group">
			<label for="pr_total">렌탈 총액</label>
			<input type="text" name="pr_total" id="pr_total" value="0" onkeyup="numCommaKeyUp()" onkeydown="numKeyDown()">
			<input type="hidden" id="temp" name="temp" value="0" onkeydown="numKeyDown()" readonly>
		</div>
		<div class="form-group">
			<label for="pr_rental">월 렌탈료</label>
			<input type="text" name="pr_rental" id="pr_rental" value="0" onkeyup="numCommaKeyUp()" onkeydown="numKeyDown()" readonly >
		</div>
		<div class="form-group">
			<label for="pr_ilsi">일시불 판매가</label>
			<input type="text" name="pr_ilsi" id="pr_ilsi" value="0" onkeyup="numCommaKeyUp()" onkeydown="numKeyDown()" >
		</div>
		<!-- <div class="form-group">
			<label for="pr_aff">제휴카드</label>
			<input type="text" name="pr_aff" id="pr_aff" >
		</div>
		<div class="form-group">
			<label for="pr_aff_cost">제휴카드 할인가</label>
			<input type="text" name="pr_aff_cost" id="pr_aff_cost" value="0" onkeyup="numCommaKeyUp()" onkeydown="numKeyDown()" >
		</div> -->
	</div>
	<!--// 렌탈 상품 가격 정보 -->	
	<a class="width-100 btn btn-submit" onclick="startRental()">등록</a>
</form>
