 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="modal_large" class="modal modal-center" data-dismiss="modal" style="display: none; z-index: 1070;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="modal_large_title">렌탈 상품 구성 추가 </span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel" id="modal_large_content">
						<table id="table" class="table table-hover" >
							<thead>
								<tr>
									<th>상품명</th>
									<th>상품코드</th>
									<th>매입처</th>
									<th>매입가</th>
									<th>판매가</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
var doubleChoose = true;

function choose(e){
var p_id = $(e).attr('idx');
var count = $('input[name=p_id'+p_id+']').length;
if(doubleChoose){
	if(count >0){
		alert("중복된 항목이 있습니다.");
	}
	else{
		$.ajax({
			url:'productTR/'+p_id ,
			success : function(data){
				$('#rproduct').append(data);
				$('#modal_large').modal('hide');
			}
		});
	}
	doubleChoose = false;
}else{
	if(count >0){
		alert("중복된 항목이 있습니다.");
	}else{
	alert("잠시만 기다려 주세요");
	}
	return false;
}


}

	
</script>