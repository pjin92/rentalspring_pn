<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<div class="ui-grid-solo">
		<div class="ui-block-a list-box-wrap"> 
			<div class="width-100">
				<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. 렌탈상품 구성내역</h4>
				<div id="productApp"></div>
				<!-- /product/table/productAppTR -->
			</div>
			
			
				<a href="#changeProduct" onclick="calculate()" class="btn btn-sm width-100 bg-gray">구성변경</a>
			<div class="width-100">
				<input type="hidden" id="pr_rentalcode" name="pr_rentalcode"> 
				<input type="hidden" id="p_pr_idx" name="p_pr_idx"> 
				<input type="hidden" id="p_m_idx" name="p_m_idx"> 
				<input type="hidden" id="p_mi_idx" name="p_mi_idx"> 
			</div> 
		</div> 	
	</div>
	
	<script> 
	
	//갯수에 따른 월렌트료 변화	
	function cal(pri, pid, ins) {

		var a = onlyNum(pri);
		var b = onlyNum(ins);
		var pee= Number(10000);
		
		var price = a.replace(/,/g, "");
		var install = b.replace(/,/g, "");

		var num = $('input[name=prp_num' + pid + ']').val(); // 입력된 갯수
		var no = $('input[name=' + pid + 'no]').val(); // 임시저장갯수
		var total = price * num; // 가격 * 갯수
		var temp2 = $('#temp').val(); // 토탈가격 임시 저장

		var num_temp = $('input[name=num_temp]').val(); // 전체갯수 임시저장
		var install_temp = $('input[name=install_temp]').val(); //설치비 임시저장
		var period = $('input[name=pr_period]').val();
		
		if (isNaN(temp2)) {
			alert("숫자만 입력하세요");
			$("#temp").val("0");
			$('input[name=prp_num' + pid + ']').val("0"); // 갯수 받아오기
			$('input[name=' + pid + 'no]').val("0"); // 갯수 저장하기
		}
		var temp = Number(total) + Number(temp2); //현재 입력된 상품가격*갯수 + 토탈가격 임시저장

	 	if (num == "0") { //입려된 갯수가 0일때
	 		num_temp = Number(num_temp) - Number(no); //전체갯수임시저장 = 전체갯수 - 임시저장갯수
			if(num_temp==0){	//전체갯수임시저장이 0이면
				var install =  Number(0);	//설치비는 0	
			}else{
				var install = Number(Number(pee*2)+(pee*Number(num_temp))); //설치비= 20000+(10000*임시저장갯수)
			}
	 		var tempTotal = Number(temp) - Number(price * no) - Number(install_temp)+ Number(install); //토탈가격 = (현가격+임시가격)-전갯수가격 - 전설치비 + 현설치비
			var result = comma(Number(tempTotal).toFixed(0));
			$('input[name=' + pid + 'no]').val(num);
			$('#pr_install').val(install);
			$('#pr_total').val(result);
			$("#temp").val(tempTotal);
			$('#num_temp').val(num_temp);
			$('#install_temp').val(install);
			$('#pr_rental').val(comma(Number(tempTotal / Number(period)).toFixed(0)));
		
	 	
	 	} else if (num != no) {
	 		
			num_temp = Number(num_temp) - Number(no); //전체갯수 - 구갯수
			num_temp = Number(num_temp)+ Number(num); //전체갯수 + 입력받은 갯수
			
	 		var install = Number(Number(pee*2)+Number(pee*num_temp)); // 설치비 = 20000*(10000* 전제+입력갯수)
			
	 		var tempTotal = Number(temp) - Number(price * no) - Number(install_temp); //(현상품가격*현갯수) - (현상품가격*구갯수)- 설치비
			if (isNaN(tempTotal)) {
				alert("숫자만 입력하세요");
				$('input[name=' + pid + 'num]').val("0"); // 갯수 받아오기
				$('input[name=' + pid + 'no]').val("0"); // 갯수 저장하기
				$("#temp").val("0");
			} else {
				
				var install = Number(Number(pee*2)+(pee*Number(num_temp))); //설치비 = 20000*(10000* 전제+입력갯수)
				tempTotal = Number(tempTotal) + Number(install); //현재가격 + 설치비
				var result = comma(Number(tempTotal).toFixed(0));
				
				$('input[name=' + pid + 'no]').val(num);
				$('#pr_total').val(result);
				$('#pr_install').val(install);
				$("#temptotal").val(tempTotal);
				$('#num_temp').val(num_temp);
				$("#temp").val(tempTotal);
				$('#install_temp').val(install);
				$('#pr_rental').val(comma(Number(tempTotal / Number(period)).toFixed(0)));
				if(num_temp =='0'){
					$('#pr_rental').val(0);
					$("#temp").val(0);
					$('#pr_total').val(0);
					$('#pr_install').val(0);
					$('#install_temp').val(0);
				}
			}
		} 
	}
		
	function rental(){
		var total = $('input[name=temp]').val();
		var ins_temp = $('input[name=pr_install]').val();
		var install = ins_temp.replace(/,/g, "");
		var i_temp = $('input[name=temp_install]').val();
		if(install=='0'){
			
		}else {
			var tempTotal = Number(total) - Number(i_temp);
			var temp = Number(total) + Number(install);
			
			$('#pr_total').val(comma(Number(temp).toFixed(0)));
			$('#pr_rental').val(comma(Number(temp / Number(period)).toFixed(0)));
			$('#temp_install').val(install);
			
			
		}
		
	}
	function startRental() {
		var param = $('form[name=frm]').serialize();
		if($('input[name=pr_name]').val() == 0){
			alert("상품명을 입력해주세요.");
			return false;
		}
		
		
		$.ajax({
			url : '/product/oldcalculateApp',
			type : 'post',
			data : param
		}).done(
				function(data) {
					if (data.trim() == '등록실패') {
						alert("등록 실패하였습니다.");
						
					} else {
						alert("상품이 등록되었습니다.");
							location.href='/install/installApp/';
					}
				});
	};

	</script>
			

			
