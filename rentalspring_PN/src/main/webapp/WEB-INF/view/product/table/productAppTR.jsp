<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:forEach var="plist" items="${data}">
<ul class="float-right-list width-100">
	<li>
		<div class="clearfix">
			<p class="tit">상품명</p>
			<p class="cont">${plist.p_name}</p>
		</div>
		<div class="clearfix">
			<p class="tit">상품코드</p>
			<p class="cont">${plist.p_code}</p>
		</div>
		<div class="clearfix">
			<p class="tit">수량</p>
			<p class="cont">${plist.prp_num}</p>
		</div>
	</li>
</ul>
</c:forEach>
<hr>
<ul class="float-right-list width-100">
	<li>
		<div class="clearfix">
			<p class="tit">렌탈기간(기본설정)</p>
			<p class="cont">${rental.pr_period}</p>
		</div>
	</li>
</ul>
<ul class="float-right-list width-100">
	<li>
		<div class="clearfix">
			<p class="tit">월 렌탈료(기본설정)</p>
			<p class="cont">${rental.pr_rental_f}</p>
		</div>
	</li>
</ul>
<ul class="float-right-list width-100">
	<li>
		<div class="clearfix">
			<p class="tit">총렌탈료(기본설정)</p>
			<p class="cont">${rental.pr_total_f}</p>
		</div>
	</li>
</ul>
	<input type="hidden" id="pr_rentalcode" name="pr_rentalcode" value="${rental.pr_rentalcode}">