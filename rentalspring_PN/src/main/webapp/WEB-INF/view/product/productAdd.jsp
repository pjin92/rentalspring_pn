<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
<%@include file="/WEB-INF/view/main/menu.jsp" %>


<div class="row" style="border:3px solid black" >
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">담당자</label>
				<input type="text" class="form-control input-sm" name="e_name" id="e_name">
				<input type="hidden" id="e_id" name="e_id" value="">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1"> 등록일</label>
					<jsp:useBean id="toDay" class="java.util.Date" />
					<fmt:formatDate value='${toDay}' pattern='yyyy-MM-dd' var="nowDate"/>

					<div class="input-group date-picker input-daterange" data-date="2012-10-11" data-date-format="yyyy-mm-dd" style="width:100%;">
						<input type="text" readonly style="cursor:pointer;" class="form-control input-sm" name="p_date" id="from" value="${nowDate}">
					</div>
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1"> 재고위치</label>
				<input type="text" class="form-control input-sm" name="p_location" id="p_location">
			</div>
		</div>
	</div>

	

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1"> 상품군 </label>
				<input type="text" class="form-control input-sm" name="p_category1" id="p_category1" onkeyup="onlyNum(this);" placeholder="숫자만 기입">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-1">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1"> 구분코드2</label>
				<input type="text" class="form-control input-sm" name="p_category2" id="p_category2" onkeyup="onlyNum(this);" placeholder="숫자만 기입">
			</div>
		</div>
	</div>

		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-1">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1"> 구분코드3</label>
				<input type="text" class="form-control input-sm" name="p_category3" id="p_category3" onkeyup="onlyNum(this);" placeholder="숫자만 기입">
			</div>
		</div>
	</div>
	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1"> 재고유형</label>
				<input type="text" class="form-control input-sm" name="p_own_stock" id="p_own_stock" onkeyup="onlyNum(this);" placeholder="숫자만 기입">
			</div>
		</div>
	</div>
</div>


<!-- ---------------------------------------------------------------------- -->
<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">상호법인명</label>
				<input type="text" class="form-control input-sm" name="c_info" id="c_info">
			</div>
		</div>
	</div>


	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">분류</label>
				<input type="text" class="form-control input-sm" name="c_type" id="c_type">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">구분코드1</label>
				<input type="text" class="form-control input-sm" name="c_category1" id="c_category1">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">구분코드2</label>
				<input type="text" class="form-control input-sm" name="c_category2" id="c_category2">
			</div>
		</div>
	</div>
	
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">구분코드3</label>
				<input type="text" class="form-control input-sm" name="c_category3" id="c_category3">
			</div>
		</div>
	</div>
	
	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">대표자명</label>
				<input type="text" class="form-control input-sm" name="c_ceoname" id="c_ceoname">
			</div>
		</div>
	</div>


	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">상호법인명</label>
				<input type="text" class="form-control input-sm" name="c_offtel1" id="c_offtel1">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">주소</label>
				<input type="text" class="form-control input-sm" name="c_name1" id="c_name1">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">우편번호</label>
				<input type="text" class="form-control input-sm" name="c_state" id="c_state">
			</div>
		</div>
	</div>
	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">전화번호</label>
					<input type="text" class="form-control input-sm" name="c_memo" id="c_memo">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">영업담당자</label>
				<input type="text" class="form-control input-sm" name="c_name1" id="c_name1">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">상태</label>
				<input type="text" class="form-control input-sm" name="c_state" id="c_state">
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
		<div class="form-group">
			<div class="input-icon right">
				<label for="form_control_1">메모</label>
				<input type="text" class="form-control input-sm" name="c_memo" id="c_memo">
			</div>
		</div>
	</div>
</div>



	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 margin-top-34">
		<div class="form-group"> 
			<div class="btn-group btn-group-justified" >
				<a href="javascript:;" class="btn btn-sm blue-hoki" onclick="insert_1()"> 등록 </a>
			</div>
		</div>
	</div>
</div>

<%@include file="/WEB-INF/view/main/js.jsp"%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
function openAddressIfEmpty(){
	if($('#u_addr1').val()==''){
		openAddress();
	}
}
function openAddress(){
	new daum.Postcode({
	    oncomplete: function(data) {
	    	
	    	
	    }
	}).open();
}
</script>
</body>
</html>