<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<h4 class="page-title"> 
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">렌탈 상품 등록</span><small class="display-block">등록</small>
						</h4>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 렌탈상품 등록정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
											
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>상품대분류코드</label> 	
														 <select name="pr_category"	class="select select2-hidden-accessible">
															 <option value ="1" selected>생활용품 </option>
															 <option value ="2">가전용품</option>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈상품코드</label> 
														<input type="text" name="pr_rentalcode" class="form-control" value="${code}" readonly>
													</div>
												</div> 
											
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈상품명</label> 
														<input type="text" name="pr_name" class="form-control"> <!-- onkeyup="productDuplicate()" -->
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>상품상태구분</label> 	
														 <select name="pr_type"	class="select select2-hidden-accessible">
															 <option value ="1" selected>설치 </option>
															 <option value ="2">배송</option>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 렌탈 상품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable" >
														<thead>
															<tr>
																<th class="active" data-orderable="false">상품명</th>
																<th class="active" data-orderable="false">상품코드</th>
																<th class="active" data-orderable="false">매입처</th>
																<th class="active" data-orderable="false">매입가</th>
																<th class="active" data-orderable="false">판매가</th>
																<th class="active" data-orderable="false">수량</th>
																<th class="active" data-orderable="false">삭제</th>
															</tr>
														</thead>
														<tbody id="rproduct">
															<tr style="display:none">
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr> 
														</tbody>
														<tfoot>
															<tr>
																<td colspan="7" class="no-padding no-border"><a onclick="plist()" class="btn btn-sm bg-orange-600 mt-10">상품 추가</a></td>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 제휴카드정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>제휴카드</label> 
														<input type="text" name="pr_aff" class="form-control">
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>제휴카드 할인가</label> 
														<input type="text" name="pr_aff_cost" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 배송 / 설치 비용정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>배송비(기본)</label> 
														<input type="text" name="pr_delivery1" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>배송비 (도서,산간)</label> 
														<input type="text" name="pr_delivery2" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>설치비(기본)</label> 
														<input type="text" name="pr_install1" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>설치비(도서,산간)</label> 
														<input type="text" name="pr_install2" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 할인정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>구분</label> 	
														 <select name="pr_gubun" class="select select2-hidden-accessible" onchange="discount(this.value)">
															 <option value="0" selected> 없음 </option>
															 <option value ="1">가격할인 </option>
															 <option value ="2">할인률</option>
															 <option value ="3">렌탈료 면제 </option>
														 </select>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>렌탈가격할인</label>
														 <input type="text" id="pr_rentalDC" name="pr_rentalDC" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" readonly> 
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>할인률</label> 
														<input type="text" id="pr_rentalDCp" name="pr_rentalDCp" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>렌탈료 면제 개월수</label> 
														<input type="text" id="pr_rentalEXE" name="pr_rentalEXE" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" readonly>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>6. 렌탈 가격정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>렌탈총액</label>
														 <input type="text" name=pr_total value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()"> 
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>렌탈료</label> 
														<input type="text" name="pr_rental" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>렌탈 개월수</label> 
														<input type="text" name="pr_period" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>일시불금액</label> 
														<input type="text" name="pr_ilsi" value="0" onkeyup="numKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<a class="heading-elements-toggle"><i class="icon-more"></i></a>
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger" onclick="startRental()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	/**상품명 중복방지*/
	function productDuplicate(){
		var pr_name = $('input[name=pr_name]').val();
		var gubun = 'rental';	
		$.ajax({
			url:'duplicate/',
			data:{'pr_name':pr_name,'gubun':gubun},
			type:'get',
		}).done(function(data){
			if(data.trim()=='중복'){
				alert('상품명이 중복됩니다.')
			}
		});
		
	}
	var table = $('#productTable').DataTable();
	
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    
			{ "data": "p_name" },
		    { "data": "p_code" },
		    { "data": "c_comname" },
		    { "data": "p_cost_f" },
		    { "data": "p_price_f" }
		  ]
		});

		ajaxDt('table','/product/productList');
	function plist() {
			$('#modal_large').modal('show');
		}
		
	function refreshTable(){
		var table=$('#productTable').DataTable();
		table.ajax.reload(null,false);
	}

	function startRental(){
		var param=$('form[name=frm]').serialize();
		$.ajax({
			url:'/product/productRental',
			type:'post',
			data:param
		})
		.done(function(data){
			if(data.trim()=='등록완료'){
				window.close();
				opener.parent.location.reload();
			}else{
				window.close();
				opener.parent.location.reload();
			}
		});
	}
	
	function discount(){
		var type = $('select[name=pr_gubun]').val();
		if(type==1){
			$('#pr_rentalEXE').attr('readonly',true);
			$('#pr_rentalEXE').val('0')
			$('#pr_rentalDCp').attr('readonly',true);
			$('#pr_rentalDCp').val('0')
			$('input[name=pr_rentalDC]').removeAttr('readonly');
		}else if(type==2){
			$('#pr_rentalDC').attr('readonly',true);
			$('#pr_rentalDC').val('0')
			$('#pr_rentalEXE').attr('readonly',true);
			$('#pr_rentalEXE').val('0')
			$('input[name=pr_rentalDCp]').removeAttr('readonly');
		}else if(type==3){
			$('#pr_rentalDC').attr('readonly',true);
			$('#pr_rentalDC').val('0')
			$('#pr_rentalDCp').attr('readonly',true);
			$('#pr_rentalDCp').val('0')
			$('input[name=pr_rentalEXE]').removeAttr('readonly');
		}
	}
	
	
	
	function del(){
	table
		.row( $(this).parents('tr') )
		.remove()
		.draw();
	}
	</script>
</body>
</html>