<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<form id="rentalProductAdd.do" name="rentalProductAdd.do" method="post">
		<div class="portlet box blue-sharp">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject" style="width: 50%;"><i
						class="fa fa-plus-square"></i> 1.렌탈상품구성 </span> <span
						class="caption-helper"> </span>
				</div>
				<div class="tools">
					<a href="" class="collapse" data-original-title="" tietle=""></a> <a
						href="" class="fullscreen" data-original-title="" tietle=""></a>
				</div>
			</div>

			<div class="portlet-body">
				<table class="table table-striped table-hover dt-responsive"
					cellspacing="0" width="100%" id="example">
					<thead>
						<tr>
							<th class="active">No.</th>
							<th class="active">렌탈상품명</th>
							<th class="active">렌탈료</th>
							<th class="active">렌탈 개월수</th>
							<th class="active">렌탈총액</th>
							<th class="active">일시불 금액</th>
							<th class="active">렌탈 총액</th>
							<th class="active">삭제</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td>1 <input type="hidden" name="p_id_1" id="p_id_1"
								value="">
							</td>
							<td><input type="text" name="p_category1_1"
								id="p_category1_1" value="" class="form-control input-sm"></td>
							<td><input type="text" name="p_category2_1"
								id="p_category2_1" value="" class="form-control input-sm"></td>
							<td><input type="text" name="p_category3_1"
								id="p_category3_1" value="" class="form-control input-sm"></td>
							<td><input type="text" name="p_name_1" id="p_name_1"
								value="" class="form-control input-sm"></td>
							<td><input type="text" name="p_code_1" id="p_code_1"
								value="" class="form-control input-sm"></td>
							<td><input type="text" name="c_comname_1" id="c_comname_1"
								value="" class="form-control input-sm"></td>

							<input type="hidden" name="p_memo_1" id="p_memo_1" value="" />
							<td><a class="btn red-soft btn-block" onclick="deleted()"
								value="삭제">삭제</a></td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
<!-- 		<a class="btn btn-sm btn-primary" onclick="changeC()" id="changeBtn">수정</a> -->

<button id="dd">ddddd</button>
	</form>





	<%@include file="../modal/modalLarge.jsp"%>
	<script>
	 $(document).ready(function () {
	        $('#example')
	                .dataTable({
	                    "responsive": true,
	                    "ajax": 'data.json'
	                });
	    });
	
		function changeC() {
					// { formData: JSON.stringify($("form[name='client_Change']").serializeObject()) }
					$.ajax({
						type : "POST",
						url : "rentalProductAdd.ajax",
						cache : false,
						enctype : "multipart/form-data",
						success : function() {
							$('#modal_large').modal('hide');
						},//success
						error : function(request, status, error) {
							alert("code:" + request.status + "\n" + "message:"
									+ request.responseText + "\n" + "error:"
									+ error);
						}
					})//ajax
			}
	 
	</script>

	<%@include file="/WEB-INF/view/main/js.jsp"%>
</body>
</html>