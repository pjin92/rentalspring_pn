<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>기초상품</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">기초상품</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
								<div class="panel-body">
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">기초상품명</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="p_name" >
							                        <!-- <span class="help-block">상품명, 모델명 등</span> -->
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">상품군</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="p_category1" >
							                        <!-- <span class="help-block">상품명, 모델명 등</span> -->
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">상품코드</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="p_code" id="p_code"> 
							                        <!-- <span class="help-block">상품명, 모델명 등</span> -->
												</div>
											</div> 
											<%-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
				                                    <label class="control-label col-md-5 col-lg-4 form-tit-label">대분류 선택</label>       
													<select class="form-control input-sm" id="main" name="sp_mm_idx" onchange="change()">
														<option value="0" selected>선택</option>
														<c:forEach items="${bsort}" var="i">
														<option value="${i.sp_mm_idx}" ${i.sp_mm_idx eq sd.sp_mm_idx ? 'selected':''}>${i.sp_mm_name}</option>
														</c:forEach>
													</select>         
					                           	</div>
											</div> --%>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group" >
													<label class="form-tit-label">등록일</label>
													<div class="form_control_1">
														<input type="text" class="form-control input-sm daterange-blank" name ="date" value=""> 
													</div>
												</div>
											</div>
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
												<!-- <a onclick="javascript:popAddExcel()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>엑셀</a> -->
											</div>
										</div>					
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>기초상품 조회결과</h5>
									<div class="heading-elements">
									<!-- 	<div class="btn-group">
											<button type="button" class="btn bg-teal-300 btn-labeled legitRipple" onclick="addLedProduct()"><b><i class="icon-plus3"></i></b>LED신규등록</button>
										</div> -->
										<!-- <div class="btn-group">
											<button type="button" class="btn bg-teal btn-labeled legitRipple" onclick="addOLedProduct()"><b><i class="icon-plus3"></i></b>구 LED신규등록</button>
											<button type="button" class="btn bg-teal-400  legitRipple" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="javascript:;" onclick="addProduct()"><i class="icon-mail5"></i> 여러 개인 경우</a></li>
												<li><a href="javascript:;" onclick="excelUpload()"><i class="icon-file-empty"></i>엑셀업로드</a></li>
												<li><a href="javascript:;" onclick="popup1()"><i class="icon-screen-full"></i> 기타 신규등록</a></li>
												<li class="divider"></li>
												<li><a href="javascript:;"><i class="icon-gear"></i> Separated line</a></li> 
											</ul>
										</div> -->
										<div class="btn-group">
											<button type="button" class="btn bg-teal-400 btn-labeled legitRipple" onclick="addProduct()" ><b><i class="icon-plus3"></i></b>신규등록</button>
<!-- 											<button type="button" class="btn bg-teal-400 dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false" ><span class="caret"></span></button> -->
											<ul class="dropdown-menu dropdown-menu-right">
												<!-- <li><a href="javascript:;" onclick="addProduct()"><i class="icon-mail5"></i> 여러 개인 경우</a></li> -->
												<li><a href="javascript:;" onclick="excelUpload()"><i class="icon-file-empty"></i>엑셀업로드</a></li>
												<!-- <li><a href="javascript:;" onclick="popup1()"><i class="icon-screen-full"></i> 기타 신규등록</a></li> -->
												<!-- <li class="divider"></li>
												<li><a href="javascript:;"><i class="icon-gear"></i> Separated line</a></li> -->
											</ul>
										</div>
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		
	
								<!-- <div class="panel-body pull-right">
									<kbd>i</kbd> 열 선택에서 항목이 선택되어 테이블 상에서 현재 보이는 값만 엑셀파일로 다운됩니다. <br> <kbd>i</kbd> 테이블 각 열의 제목을 크릭하고 드래그하면 열의 순서가 바뀌며, 바뀐 상태로 엑셀다운이 됩니다. 
								</div> -->
	
								<table class="table table-hover" id="productTable" data-page-length="25">
									<thead>
										<tr>
											<th>선택</th>
											<th>등록일</th>
											<th>상품명</th>
											<th>상품코드</th>
<!-- 											<th>재고위치</th>  -->
<!-- 											<th>상품 카테고리</th> -->
											
											
											<th>매입처</th>
											<th>원가</th>
											<th>판매가</th>
											<th>설치/배송</th> 
<!-- 											<th>담당자</th> -->
											<th>상품구분</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
								<!-- <div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="productDel('display', '1')"><i class="icon-eye"></i>진열</a></li>
												<li><a onclick="productDel('display', '0')"><i class="icon-eye-blocked"></i>미진열</a></li>
												<hr>
												<li><a onclick="productDel('sel', '1')">판매중</a></li>
												<li><a onclick="productDel('sel', '0')">판매안함</a></li>
												<hr> 
												<li><a onclick="productDel('copy', 'copy')"><i class="icon-copy3"></i>복제</a></li>
												<li><a onclick="productDel('del', 'del')"><i class="icon-trash-alt"></i>삭제</a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
			{ "data": "p_id" }, //선택 
		    { "data": "p_date"},//등록일
		    { "data": "p_name"},//상품면
		    { "data": "p_code"}, //상품코드
// 		    { "data": "p_location"}, //재고위치
		    
		    { "data": "c_comname"}, //상품군
		    { "data": "p_cost"}, //원가
		    { "data": "p_price"}, //판매가
		    { "data": "p_type"},//매출처별 판매가
		    { "data": "p_gubun"} //상태
		  ]
	});
	tableLoad('productTable');
	//첫 dataTable initialize
	//이부분이 데이터테이블에 들어가는 데이터들 가져옴
	if(! $.fn.DataTable.isDataTable('#productTable')){
		checkDt('productTable','/product/basicProduct');
	}
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#productTable').DataTable();
		table.ajax.url('/product/basicProduct?'+param).load(null,false);
	} 
	function addProduct(){
		window.open('/product/productBasic','popup','width=1200,height=800');
	}

	//현재 보이는 dataTable refresh
	function refreshTable(){
		var table=$('#productTable').DataTable();
		table.ajax.reload(null,false);
	}
	
	function basicProduct(){
		var p_code=$(event.target).attr('p_code');
		window.open('/product/'+p_code,'popup','width=1200,height=800');
	}
	
	function popAddExcel(){
		//var excelUrl 을 상위 jsp에서 정의
		
			cw=screen.availWidth; 
			ch=screen.availHeight; 
	
			sw=1024;    //띄울 창의 넓이
			sh=screen.height - 440;    //띄울 창의 높이
	
			ml=(cw-sw)/2; 
			mt=(ch-sh)/2;
			
			window.open('/product/excel','add','width='+sw+',height='+sh+',top='+mt+',left='+ml+',resizable=yes, scrollbars=yes');
	}
	</script>

</body>
</html>