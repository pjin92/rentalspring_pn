 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- large2 modal -->
<div id="modal_large2" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
                       <div class="modal-content-wrap">
			<div class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="modal_large2_title">title </span><span class="text-light"></span>
						<span style="cursor:pointer" class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2"></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<!-- Column selectors -->
				<div class="panel" id="modal_large2_panel">
					<div class="row">
					<div class="col-xs-12">
						<!-- select box 관리 -->					
						<form id="addColSel" name="addColSel" action="" class="mb-20">
							<div class="panel panel-flat border-top-xlg border-top-primary-300">	
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i><span>Select 항목 관리</span></h5>
								</div>
								<div class="pl-20 pr-20 pb-20">
									<div class="form-group">
										<div class="input-group">
											<select id="col_select" onchange="csShowInput()" class="select select2-hidden-accessible">
											</select>		
											<span class="input-group-btn ml-10"><!-- href="javascript:delColSel() -->
												<button onclick="delColSel()" class="btn bg-teal del-col" type="button">삭제</button>
											</span>																	
										</div>
										<div class="col-xs-12 mt-10 input-group">
											<input type="text" id="csInput" class="form-control">
											<span class="input-group-btn"><!-- href="javascript:csUpdate() -->
												<button onclick="csUpdate()" class="btn bg-teal update-col" type="button">수정</button>
											</span>
										</div>
									</div>												
									<div class="form-group mt-20">
										<label>select 항목 추가</label>
										<div class="col-xs-12 input-group">
											<input type="text" name="cs_name" class="form-control">
											<span class="input-group-btn">
												<a href="javascript:addColSel()" onclick="addColSel()" class="btn bg-teal add-col">추가</a>
											</span>
										</div>									
									</div>									
								</div>
							</div>
						</form>
						<!--// select box 관리 -->		
						<form role="form" name="boChange" method="post" id="boChangeForm" style="display: none;">
							<div class="panel panel-flat border-top-xlg border-top-primary-300" id="optionContent">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i><span id="p_title">일괄처리 관리</span></h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">									
										<div class="row">										
									 		<table class="table table-bordered table-xs">
												<thead>
													<tr class="alpha-brown text-bold">
														<th id="col_name"></th>
														<th class="">등록 화면</th>
														<th class="">검색 화면</th>
														<th id="boc"></th>
													</tr>
												</thead>
												<tbody id="boTbody"><!-- resource/js/design/bo.js --></tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- <div class="panel-footer">
									<button type="button" class="btn btn-sm btn-danger pull-right mr-15" onclick="boSave()">저장</button>
								</div> -->
							</div>
						</form>
					</div>
					</div>
				</div>
				<!-- /column selectors -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /large2 modal -->
