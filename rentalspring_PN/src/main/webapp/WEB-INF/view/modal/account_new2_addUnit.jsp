 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 					<!-- Large modal -->
					<div id="modal_large" class="modal modal-center">
						<div class="modal-dialog modal-lg">
                            <div class="modal-content-wrap">
								<div class="modal-content alpha-slate">
									<div class="modal-header">
									
										<h4 class="modal-title text-black">
											<i class="icon-menu2 position-left"></i>거래처 관리 <span class="text-light">데이터 신규 추가</span>
											<div class="btn-group ml-10">
						                    	<button type="button" class="btn btn-xxs border-grey text-grey-800 btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="true">미사용<span class="caret"></span></button>
						                    	<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="javascript:;"><i class="icon-checkmark3"></i>사용중</a></li>
													<li class="active"><a href="javascript:;"><i class="icon-cross2"></i>미사용</a></li>
													<li><a href="javascript:;"><i class="icon-menu7"></i>전체</a></li>
												</ul>
											</div> 
											<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer"></i></span>
										</h4>
									</div>

									<div class="modal-body">
										<!-- Column selectors -->
										<div class="panel">
											<table class="table table-bordered datatable-group">
												<thead>
													<tr class="alpha-brown text-bold">
														<th class="">DB명</th>
														<th class="">선택</th>
														<th class="">No.</th>
														<th class="">기능명</th>
														<th class="">입력폼명</th>
														<th class="">종류</th>
														<th class="">상태</th>
														<th class="">수정</th>
													</tr>
												</thead>
												<tbody id="colhTbody">
												</tbody>
											</table>
					
											<div class="panel-footer panel-footer-transparent">		
												<a class="heading-elements-toggle"><i class="icon-more"></i></a>
												<div class="heading-elements">			
													<div class="btn-group dropup ml-20">
														<button type="button" class="btn btn-xs bg-purple-400 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
														<button type="button" class="btn btn-xs bg-purple-400 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
														<ul class="dropdown-menu"><!-- dropdown-menu-right -->
															<li><a href="javascript:;"><i class="icon-checkmark3"></i>사용중</a></li>
															<li><a href="javascript:;"><i class="icon-cross2"></i>미사용</a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<!-- /column selectors -->
									</div>

									<!-- <div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">적용</button>
									</div> -->
								</div>
							</div>
						</div>
					</div>
					<!-- /Large modal -->
 					<!-- new modal -->
					<div id="modal_new" class="modal modal-center">
						<div class="modal-dialog modal-lg">
                            <div class="modal-content-wrap">
								<div class="modal-content alpha-slate">
									<div class="modal-header">
										<h4 class="modal-title text-black">
											<i class="icon-menu2 position-left"></i>거래처 관리 <span class="text-light">데이터 신규 추가</span>
											<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer"></i></span>
										</h4>
									</div>

									<div class="modal-body">										
										<!-- Column selectors -->
												<div class="row">
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(1)">
																	<img src="assets/images/form/input_default.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																기본 입력폼
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(2)">
																	<img src="assets/images/form/select_default.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																펼침 목록
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(3)">
																	<img src="assets/images/form/input_fileupload.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																파일 첨부
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(20)">
																	<img src="assets/images/form/picker_date.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																현재날짜
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(14)">
																	<img src="assets/images/form/picker_date.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																날짜
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(5)">
																	<img src="assets/images/form/input_map.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																지도 입력
															</div>
														</div>
													</div>
													
													<!-- <div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(4)">
																	<img src="assets/images/form/input_password.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																비밀번호 입력폼
															</div>
														</div>
													</div> -->
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(6)">
																	<img src="assets/images/form/input_currency.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																금액 입력 (원)
															</div>
														</div>
													</div>
													
													<!-- <div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(7)">
																	<img src="assets/images/form/input_percent.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																퍼센트 입력
															</div>
														</div>
													</div> -->
													
													<!-- <div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(8)">
																	<img src="assets/images/form/search_popup.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																검색창 선택 입력폼
															</div>
														</div>
													</div> -->
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(9)">
																	<img src="assets/images/form/radio.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																라디오 선택
															</div>
														</div>
													</div>
													
													<!-- <div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit()">
																	<img src="assets/images/form/select_search.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																 재검색 펼침 목록
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(10)">
																	<img src="assets/images/form/select_multi.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																 다중선택 펼침 목록
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(11)">
																	<img src="assets/images/form/select_radio.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																 라디오 펼침 목록
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(12)">
																	<img src="assets/images/form/textarea.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																장문 텍스트 입력폼
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(13)">
																	<img src="assets/images/form/editor.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<h6 class="text-semibold no-margin-top">Thumbnail title</h6>
																텍스트 에디터
															</div>
														</div>
													</div> -->
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a onclick="addSection()">
																	<img src="assets/images/form/input_default.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																입력 영역 구분
															</div>
														</div>
													</div>
												</div>
										<!-- /column selectors -->
									</div>

									<!-- <div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">적용</button>
									</div> -->
								</div>
							</div>
						</div>
					</div>
					<!-- /Large modal -->