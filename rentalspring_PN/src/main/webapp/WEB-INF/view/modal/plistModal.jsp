<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<table class="table table-bordered table-xxs datatable-product datatable-responsive" id="ptable">
	<thead>
		<tr class="alpha-brown text-bold">
			<th class="">상품명</th>
			<th class="">옵션1</th>
			<th class="">옵션2</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${plist}" var="i">
			<tr>	
				<td><a pp="${i.p_price}" p="${i.p_idx}" og1='${i.og1}' og2='${i.og2}' onclick="selectP()">${i.p_name}</a></td>
				<td>${i.o_idx2}</td>
				<td>${i.o_idx3}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<table style="display: none;" class="table table-bordered table-xxs datatable-product datatable-responsive" id="ptable2">
	<thead>
		<tr class="alpha-brown text-bold">
			<th>상품명</th>
			<th>옵션1</th>
			<th>옵션2</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><span id="span_p"></span></td>
			<td>
				<select id="select_og1" class="select select2-hidden-accessible">
					<option value="">선택</option>
				</select>
			</td>
			<td>
				<select id="select_og2" class="select select2-hidden-accessible">
					<option value="">선택</option>
				</select>
			</td>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3">
				<a class="btn btn-sm btn-primary legitRipple pull-right" onclick="selectOpt()">선택</a>
			</td>
		</tr>	
	</tfoot>
</table>