 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Large modal -->
<div id="modal_large" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
                       <div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="modal_large_title">title </span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<!-- Column selectors -->
				<div class="panel" id="modal_large_panel">
				</div>
				<!-- /column selectors -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Large modal -->