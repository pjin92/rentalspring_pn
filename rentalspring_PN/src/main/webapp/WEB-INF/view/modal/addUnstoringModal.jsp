<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="kr">
<c:choose>
<c:when test="${empty msg }">
<table class="table table-bordered table-xxs" id="ptable">
	<thead>
		<tr class="alpha-brown text-bold">
			<th class="">매입처</th>
			<th class="">상품명</th>
			<th class="" style="text-align: right;">기준 매출가<br>배송비</th>
			<th class="" style="text-align: right;">${c_corp_name} 매출가<br>배송비</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${plist}" var="i">
			<tr>	
				<td>${i.c_corp_name}</td>
				<td><a idx='${i.p_idx}' onclick='clicked()'>${i.p_name}</a></td>
				<td style="text-align: right;">
					<fm:formatNumber pattern="#,###,###,###,###,###,###,###">${i.p_price}</fm:formatNumber><br>
					<fm:formatNumber pattern="#,###,###,###,###,###,###,###">${i.p_delprice}</fm:formatNumber>
				</td>
				<td style="text-align: right;">
					<fm:formatNumber pattern="#,###,###,###,###,###,###,###">${i.cm_price}</fm:formatNumber><br>
					<fm:formatNumber pattern="#,###,###,###,###,###,###,###">${i.cm_delprice}</fm:formatNumber>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</c:when>
<c:otherwise>
${msg}
</c:otherwise>
</c:choose>
<script>
$('#ptable').dataTable({
    //button overide. 전체선택 선택해제 없애기.
    buttons: {            
        buttons: [
			{
				extend: 'pageLength',
				className: 'btn btn-xs bg-slate-600'
			},
            {
                extend: 'excelHtml5',
                className: 'btn btn-xs bg-slate-800 legitRipple',
                text: '<i class="icon-file-excel"></i>&nbsp;&nbsp;엑셀다운',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ]
    }
});

function clicked(){
	var p_idx=$(event.target).attr('idx');
	var c_idx=$('#c_idx_fk').val();
	if(c_idx==''){
		alert('매출처를 선택해주세요.');
		return;
	}
	var p_name=$(event.target).html();
	if(!window.confirm(p_name+'을(를) 추가하시겠습니까?')){
		return;
	}

	$.ajax({
		url:'addUnStoringPC.ajax',
		data:'p_idx='+p_idx+'&c_idx='+c_idx
	})
	.done(function(data) {
		$('.default').remove();//데이터가 없습니다. 라는 기본 tr td
		$("#mtable").append(data);
		
		$("#tableFoot").show();
		addSelect();
		cal();
		stockCode();
	});
	
}
</script>
