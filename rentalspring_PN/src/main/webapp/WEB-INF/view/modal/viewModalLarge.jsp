<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<c:choose>
<c:when test="${not empty msg }">
${msg}
</c:when>
<c:otherwise>
<div class="form-group-attached">
	<div class="row">
	<form name="modalLargeForm">
	<input type="hidden" name="pk" value="${pk }">
	<table class="table">
		<thead>
			<tr>
			<c:forEach items="${col}" var="col">
			<c:choose>
				<c:when test="${col.col_place>100 }"></c:when>
				<c:otherwise>
				<th>${col.col_name}</th>
				</c:otherwise>
			</c:choose>
			</c:forEach>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach items="${v}" var="v" varStatus="stat">
				<c:set var="col_add" value="col_add${col[stat.index].col_idx}"></c:set>
				<c:set var="col_input_name" value="${col[stat.index].col_input_name}"></c:set>
				<td>
					<c:if test="${not( col_input_name eq 'c_idx') }">
					<input type="hidden" name="${col_input_name eq 'col_add'?col_add:col_input_name}" class="info form-control" value="${v}">
					</c:if>
					<span class="${col_input_name eq 'c_idx'?'':'modalSpan'}">${v}</span>
				</td>
				</c:forEach>
			</tr>
		</tbody>
	</table>
	</form>
	</div>
</div>
<div style="height: 50px;">
<div class="btn bg-teal-400 btn-labeled legitRipple pull-right" onclick="changeCe()">수정</div>
</div>
</c:otherwise>
</c:choose>