<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<script>
$('#modal_large_title').html('${hm_p.p_name}');
</script>
<div class="form-group-attached">
	<div class="row">
	<table class="table">
		<thead>
			<tr>
			<c:forEach items="${col}" var="col">
			<c:choose>
				<c:when test="${col.col_place>100 }"></c:when>
				<c:otherwise>
				<th>${col.col_name}</th>
				</c:otherwise>
			</c:choose>
			</c:forEach>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach items="${v}" var="v" varStatus="stat">
				<c:set var="col_add" value="col_add${col[stat.index].col_idx}"></c:set>
				<c:set var="col_input_name" value="${col[stat.index].col_input_name}"></c:set>
				<td>
					<c:if test="${not( col_input_name eq 'c_idx') }">
					<input type="hidden" name="${col_input_name eq 'col_add'?col_add:col_input_name}" class="info form-control" value="${v}">
					</c:if>
					<span class="${col_input_name eq 'c_idx'?'':'modalSpan'}">${v}</span>
				</td>
				</c:forEach>
			</tr>
		</tbody>
	</table>
	</div>
</div>
