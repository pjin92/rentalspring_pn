 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 					<!-- Large modal -->
					<div id="modal_template" class="modal modal-center">
						<div class="modal-dialog modal-lg">
                            <div class="modal-content-wrap">
								<div class="modal-content alpha-slate">
									<div class="modal-header">
										<h4 class="modal-title text-black">
											<i class="icon-images3 position-left"></i>Mr. Rental 관리자 사이트<span class="text-light"> 템플릿 설정</span>
											<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right cursor-pointer" data-dismiss="modal"><i class="icon-cross2"></i></span>
										</h4>
									</div>

									<div class="modal-body">
										<div class="panel no-margin-bottom">
											<div class="panel-body">
										    	<div class="row text-center">
										    		<div class="col-xs-12">
											    		<h6 class="no-margin text-left">
										                    <i class="icon-checkmark position-left"></i><span class="text-semibold">사용중인 템플릿</span>
										                    <small class="ml-10"><a class="badge bg-danger-700">+ UPDATE</a> </small>
									                    </h6>
								                    </div>
										    		<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="template.do" class="display-inline-block mt-10">
															<img src="https://i.pinimg.com/736x/16/2b/a0/162ba004d81ee8a869900bc8af4f25f4--design-branding-freebies.jpg" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">레인보우 Rainbow
																<span class="display-block text-grey-300 text-size-mini">v3.4.2</span>
																<span class="badge badge-flat border-grey text-grey-600 mt-5">Admin</span>
															</h6>
														</a>
													</div>
												</div>
												
												<hr class="no-margin mb-20">
												
										    	<div class="row text-center">
										    		<div class="col-xs-12">
											    		<h6 class="no-margin text-left">
										                    <i class="icon-stack3 position-left"></i><span class="text-semibold">보유한 템플릿</span>
															<div class="btn-group ml-10">
										                    	<button type="button" class="btn btn-xxs border-grey text-grey-800 btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="true">미사용<span class="caret"></span></button>
										                    	<ul class="dropdown-menu dropdown-menu-right">
																	<li><a href="javascript:;"><i class="icon-checkmark3"></i>사용중</a></li>
																	<li class="active"><a href="javascript:;"><i class="icon-cross2"></i>미사용</a></li>
																	<li><a href="javascript:;"><i class="icon-menu7"></i>전체</a></li>
																</ul>
															</div> 
									                    </h6>
								                    </div>
													<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10"> <!-- data-toggle="modal" data-target="#modal_large" -->
															<img
															src="http://www.freedesignresource.com/wp-content/uploads/2017/06/anduous-freebie-a8-320x240.jpg"
															class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">푸른 하늘 
																<span class="display-block text-grey-300 text-size-mini">Anduous</span>
															</h6>
														</a>
													</div>
													<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10">
															<img src="http://www.freedesignresource.com/wp-content/uploads/2017/06/freebie-online-shop-psd-320x240.jpg" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">레인보우 
																<span class="display-block text-grey-300 text-size-mini">Rainbow</span>
															</h6>
														</a>
													</div>
										    		<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10">
															<img src="https://pixelbuddha.net/sites/default/files/freebie/freebie-1488542051.jpg" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">인사관리
																<span class="display-block text-grey-300 text-size-mini">HRD</span>
															</h6>
														</a>
													</div>
										    		<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10">
															<img src="http://multifour.com/images/ola.jpg" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">영업관리
																<span class="display-block text-grey-300 text-size-mini">Operating</span>
															</h6>
														</a>
													</div>
										    		<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10">
															<img src="https://7-t.biz/a/s/9083.jpg" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">문서관리
																<span class="display-block text-grey-300 text-size-mini">Document</span>
															</h6>
														</a>
													</div>
										    		<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10">
															<img src="http://tedxzurich.com/wp-content/uploads/2014/05/TEDxZurich_Talks-available_Screen-Mockup-320x240.png" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">쇼핑몰
																<span class="display-block text-grey-300 text-size-mini">Shop</span>
															</h6>
														</a>
													</div>
										    		<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10">
															<img src="https://pixelbuddha.net/sites/default/files/freebie/freebie-1497000633.jpg" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">교육시스템
																<span class="display-block text-grey-300 text-size-mini">LMS</span>
															</h6>
														</a>
													</div>
										    		<div class="col-xs-6 col-sm-4 col-lg-2 p-10">
														<a href="javascript:;" class="display-inline-block mt-10">
															<img src="https://pixelbuddha.net/sites/default/files/freebie/freebie-1497867610.jpg" class="img-rounded img-responsive" alt="">
															<h6 class="text-default text-bold text-size-small mt-5 mb-10">메일
																<span class="display-block text-grey-300 text-size-mini">E-mail</span>
																<span class="badge badge-flat border-grey text-grey-600 mt-5">쇼핑몰</span>
															</h6>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn border-slate text-slate-800 btn-flat pull-left" data-dismiss="modal">닫기</button>
										<button type="button" class="btn btn-danger">저장</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Large modal -->
 					<!-- new modal -->
					<div id="modal_new" class="modal modal-center">
						<div class="modal-dialog modal-lg">
                            <div class="modal-content-wrap">
								<div class="modal-content alpha-slate">
									<div class="modal-header">
										<h4 class="modal-title text-black">
											<i class="icon-menu2 position-left"></i>거래처 관리 <span class="text-light">입력폼 추가</span>
											<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2"></i></span>
										</h4>
									</div>

									<div class="modal-body">										
										<!-- Column selectors -->
												<div class="row">
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(1)">
																	<img src="assets/images/form/input_default.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																기본 입력폼
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(2)">
																	<img src="assets/images/form/select_default.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																펼침 목록
															</div>
														</div>
													</div>
													
													<div class="col-xs-6 col-sm-4 col-md-3">
														<div class="thumbnail">
															<div class="thumb">
																<a href="javascript:addUnit(3)">
																	<img src="assets/images/form/input_fileupload.png" alt="">
																	<span class="zoom-image"><i class="icon-plus22"></i></span>
																</a>
															</div>
															<div class="caption">
																<!-- <h6 class="text-semibold no-margin-top">Thumbnail title</h6> -->
																파일 첨부
															</div>
														</div>
													</div>
												</div>
										<!-- /column selectors -->
									</div>

									<!-- <div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">적용</button>
									</div> -->
								</div>
							</div>
						</div>
					</div>
					<!-- /Large modal -->