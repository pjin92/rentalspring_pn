<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<div class="page-title">
							<h4>
								<i class="icon-address-book2 position-left"></i> <span
									class="text-bold">설치 확인서 등록</span> <small class="display-block">등록</small>
							</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
					<div class="content">
						<div class="row">
							<form role="form" id="frm" name="frm" method="post" action="installCormirmation" enctype="multipart/form-data">
							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<article class="panel panel-flat margintop">
											<div class="panel-heading">
												<h5 class="panel-title">
													<i class="icon-folder-search position-left"></i>1.설치 확인서 등록
												</h5>
											</div>
											<div class="panel-body">
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default ">
														<input type="hidden" name="m_idx" value="${m_idx}">
														<input type="hidden" name="mi_idx" value="${mi_idx}">
														<input name="file" type="file" class="file-input-ajax"
																data-show-upload="false" data-show-caption="false"
																data-show-preview="true" data-main-class="input-group-xxs">
													</div>
												</div> 
											</div>
									</article>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<article class="panel panel-flat margintop">
										<div class="panel-heading">
											<h5 class="panel-title">
												<i class="icon-folder-search position-left"></i>2.설치 확인 내용
											</h5>
										</div>
										<div class="panel-body">
											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="form-group form-group-default ">
													<label>설치 완료일</label>
													<input type="text" class="form-control input-sm daterange-single text-center" name="ds_date" readonly="readonly">
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="form-group form-group-default ">
													<label>기사 메모</label> <input type="text"  class="form-control"  name="if_addmemo" value="">
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="form-group form-group-default ">
													<label>추가공임사유</label> <input type="text" name="if_extra_memo" value="" class="form-control" >
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="form-group form-group-default ">
													<label>추가공임비용</label>
													<input type="text" name="if_extra" value="" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" >
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="form-group form-group-default ">
													<label>설치유형</label>
													<select name="m_jubsoo" class="select select2-hidden-accessible">
														<option value="일반설치">일반설치</option>
														<option>방문견적/무료체험설치</option>
													</select>
												</div>
											</div>
										</div>
										
										<div class="panel-footer">
											<div class="col-xs-12 col-sm-12 col-md-12">
												<a onclick="installCormirmation()" class="btn bg-teal-400 pull-right">설치완료</a>
											</div>
										</div>
											
									</article>
								</div>
							</div>
							</form>
						</div>
					</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
		function installCormirmation(){
			var m_jubsoo=$('select[name=m_jubsoo]').val();
			blockById('frm');
			$('#frm').ajaxForm({
				url:'./installconfirmation?m_jubsoo='+m_jubsoo,
				enctype:"multipart/form-data",
				success:function(res){
					if(res.trim()=='등록성공'){
						alert('등록에 성공했습니다.');
						window.close();
					}else{
						alert(res);
					}
				},
				fail:function(){
					alert("오류 발생.\n잠시 후 시도해주세요.");
				},
				complete:function(){
					$("#frm").unblock();
				}
			});
			 $("#frm").submit() ;
		}
		

	</script>
	
	<!-- file input -->
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<script type="text/javascript" src="/assets/js/pages/uploader_bootstrap.js"></script>	
	<!-- file input end-->
</body>
</html>