<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>기사정산</title>
	<%@include file="/WEB-INF/view/main/m_css.jsp"%>
	<style type="text/css">
		#customerInfo-popup {width:90%;}
	</style>
</head>
<body>
	<!-- 기사정산  page -->
	<div data-role="page" id="calculate-page" data-url="calculate-page" data-title="기사정산">
		<div data-role="header" data-positon="fixed">
			<h1>기사정산</h1> 
			<a data-ajax="false" href="/indexApp" class="ui-back-btn ui-btn-right" data-role="button" role="button" title="로그아웃"><i class="icon-exit"></i></a>
		</div>
		<!-- search input -->	
		<div class="ui-grid-solo search-section">
			<div class="ui-block-a search-wrap">
				<form name="searchForm">
					<input type="text" name="mainKeyword" id="mainKeyword" placeholder="상품명, 신청고객명" />
					<button type="submit" class="search-btn" onsubmit="searchBtn()"><i class="icon-search4"></i></button>
				</form>
			</div>
		</div>		
		<!--// search input -->		
		<!--  main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<!-- list section -->
			<div class="ui-grid-solo">
				<!-- list 반복 -->
				<div class="ui-block-a">
					<h3 align="center">페이지 준비중 입니다.</h3>
				</div>
				<%-- <c:forEach var="in" items="${data}"> --%>
				<%-- <div class="ui-block-a list-box-wrap">
					<div class="clearfix">
						<div class="list-left-box">	
							<p class="bg-gray"><b>2018-06-08</b></p>
							<p class="bg-info"><a href="#inspect-page" m_idx="${in.m_idx}" onclick="csInfo();" class="width-100">미정산</a></p>
						</div>
						<div class="list-right-box">
							<ul class="list-wrap">
								<li class="">
									<p class="list-tit">상품명 : <strong class="list-cont">LED-28평형</strong></p>
								</li>
								<li class="">
									<p class="list-tit">설치고객명 : <a href="#customerInfo" data-rel="popup" data-position-to="window" data-transition="pop" class="">이선생</a></p>
								</li>
								<li class="">
									<p class="list-tit">주소 : <strong class="list-cont">경기도 성남시 분당구 판교역로 235</strong></p>
								</li>
								<li class="">
									<p class="list-tit">연락처 : <strong class="list-cont"><a href="tel:010-1234-5678">010-1234-5678</a></strong></p>
								</li>
							</ul>
						</div>
					</div>
				</div> --%>
				<%-- </c:forEach> --%>
				<!--// list 반복 -->														
			</div>	
			<!--// list section -->
			<!-- 고객정보 상세 팝업창  (고객명 a href == 팝업 id값 동일) -->
			<%-- <div data-role="popup" id="customerInfo" data-overlay-theme="a">
				<a href="javascript:;" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
				<%@ include file="../customer/contractApp2.jsp" %>
			</div> --%>		
			<!--// 고객정보 상세 팝업창  -->			
		</div>
		<!--//  main content -->
		<%@ include file="../main/m_menu.jsp" %>
	</div>
	<!--// 기사정산  page -->
	
	<%@include file="/WEB-INF/view/main/m_js.jsp"%>

</body>
</html>