<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<div class="page-title">
							<h4>
								<i class="icon-address-book2 position-left"></i> <span
									class="text-bold">설치확인서 상세</span>
							</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<article class="panel panel-flat margintop">
										<form id="reupInstallFile">
											<input type="hidden" name="m_idx" value="${m_idx}">
											<input type="hidden" name="mi_idx" value="${mi_idx}">
											<input type="hidden" name="if_idx" value="${install.if_idx}">
											<div class="panel-heading">
												<h5 class="panel-title">
													<i class="icon-folder-search position-left"></i>1.설치 확인서
												</h5>
											</div>
											<div class="panel-body">
												<div class="col-xs-6">
													<div class="form-group form-group-default ">
														<label>기존 설치확인서</label>
														<a href="/file/member/${install.mf_idx}_${install.mf_memo}">${install.mf_file}</a>
													</div>
												</div>
												<div class="col-xs-6">
													<div class="form-group form-group-default ">
														<input name="file" type="file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs">
													</div>
												</div> 
											</div>
											<div class="panel-footer">
												<div class="col-xs-12 col-sm-12 col-md-12">
													<a onclick="reuploadInstallFile()" class="btn bg-teal-400 pull-right">재등록</a>
												</div>
											</div>
										</form>
									</article>
								</div>
							</div>
							
							<article class="col-xs-12">
								<form name="ifInfo" id="ifInfo" method="post" enctype="multipart/form-data">
									<input type="hidden" name="m_idx" value="${param.m_idx}">
									<input type="hidden" name="mi_idx" value="${param.mi_idx}">
									<input type="hidden" name="ds_idx" value="${ds.ds_idx}">
									<input type="hidden" name="if_idx" value="${install.if_idx}">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2.설치 확인 내용
										</h5>
									</div>
									<div class="panel-body">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="form-group form-group-default ">
												<label>설치 완료일</label>
												<input type="text" class="form-control input-sm daterange-single text-center" name="ds_date" readonly="readonly" value="${ds.ds_date}">
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="form-group form-group-default ">
												<label>기사 메모</label> <input type="text"  class="form-control"  name="if_addmemo" value="${install.if_addmemo}">
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="form-group form-group-default ">
												<label>추가공임사유</label>
												<input type="text" name="if_extra_memo" class="form-control" value="${install.if_extra_memo}">
											</div>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="form-group form-group-default ">
												<label>추가공임비용</label>
												<input type="text" name="if_extra" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()" class="form-control" value="${install.if_extra_f}">
											</div>
										</div>
									</div>
									
									<div class="panel-footer">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<a onclick="updateIf()" class="btn bg-teal-400 pull-right">수정</a>
										</div>
									</div>
								</div>
								</form>
							</article>
						</div>
					</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
		function reuploadInstallFile(){
			if($('input[name=file]').val()==''){
				alert('파일이 없습니다.');
				return;
			}
			
			if(window.confirm('설치확인서 재등록 하시겠습니까?\n2.설치확인내용은 업로드 되지 않습니다.')){
				blockById('reupInstallFile');
				$('#reupInstallFile').ajaxForm({
					url:'/install/reuploadInstallFile',
					enctype:"multipart/form-data",
					type:'post',
					success:function(res){
						if(res.trim()=='등록완료'){
							alert('등록에 성공했습니다.');
							location.reload();
						}else{
							alert(res);
						}
					},
					fail:function(){
						alert("오류 발생.\n잠시 후 시도해주세요.");
					},
					complete:function(){
						$("#reupInstallFile").unblock();
					}
				});
				 $("#reupInstallFile").submit() ;
			}
		}
		function updateIf(){
			if(window.confirm('설치 확인 내용 수정하시겠습니까?\n1.설치확인서는 업로드 되지 않습니다.')){
				blockById('ifInfo');
				$.ajax({
					url:'/install/addComplete?'+$('#ifInfo').serialize(),
					type:'put'
				}).fail(function(){
					alert('잠시 후 다시 시도해주세요.');
				}).done(function(res){
					alert(res);
				}).always(function(){
					$('#ifInfo').unblock();
				});
			}
		}

	</script>
	
	<!-- file input -->
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<script type="text/javascript" src="/assets/js/pages/uploader_bootstrap.js"></script>	
	<!-- file input end-->
</body>
</html>