<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>CS 점검 관리</title>
	<%@include file="/WEB-INF/view/main/m_css.jsp"%>
	<style type="text/css">
		#customerInfo-popup {width:90%;}
	</style>
</head>
<body>
	<!-- CS 점검 관리  page -->
	<div data-role="page" id="install-page" data-url="install-page" data-title="CS 점검 관리">
		<div data-role="header" data-positon="fixed">
			<h1>CS점검관리</h1> 
			<a data-ajax="false" href="/indexApp" class="ui-back-btn ui-btn-right" data-role="button" role="button" title="로그아웃"><i class="icon-exit"></i></a>
		</div>
		<!-- search input -->	
		<div class="ui-grid-solo search-section">
			<div class="ui-block-a search-wrap">
				<form name="searchForm">
					<input type="text" name="mainKeyword" id="mainKeyword" placeholder="상품명, 신청고객명" />
					<button type="submit" class="search-btn" onsubmit="searchBtn()"><i class="icon-search4"></i></button>
				</form>
			</div>
		</div>		
		<!--// search input -->		
		<!--  main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<!-- list section -->
			<div class="ui-grid-solo">
				<!-- list 반복 -->
				<c:forEach var="in" items="${data}">
				<div class="ui-block-a list-box-wrap">
					<div class="clearfix">
						<div class="list-left-box">	
							<!-- <p class="bg-complete"><b>설치완료</b></p> -->
							<p class="bg-orange mb-sm"><a href="#none">설치확인서</a><!-- 설치확인서 경로 --></p>
							<!-- <p class="bg-gray"><b>2018-06-08</b></p> -->
							<p class="bg-info"><a href="#inspect-page" m_idx="${in.m_idx}" onclick="csInfo();" class="width-100">점검 목록</a></p>
						</div>
						<div class="list-right-box">
							<ul class="list-wrap">
								<li class="">
									<p class="list-tit">상품명 : <strong class="list-cont">${in.pr_name}</strong></p>
								</li>
								<li class="">
									<p class="list-tit">설치고객명 :${in.mi_name}</p>
								</li>
								<li class="">
									<p class="list-tit">주소 : <strong class="list-cont">${in.mi_addr }</strong></p>
								</li>
								<li class="">
									<p class="list-tit">연락처 : <strong class="list-cont"><a href="tel:010-1234-5678">${in.mi_tel }</a></strong></p>
								</li>
							</ul>
						</div>
					</div>
					<!-- <div class="list-center-box padding-10px-top">
					</div> -->
				</div>
				</c:forEach>
				<!--// list 반복 -->														
			</div>	
			<!--// list section -->
			<!-- 고객정보 상세 팝업창  (고객명 a href == 팝업 id값 동일) -->
			<div data-role="popup" id="customerInfo" data-overlay-theme="a">
				<a href="javascript:;" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
				<%@ include file="../customer/contractApp2.jsp" %>
			</div>
			<!--// 고객정보 상세 팝업창  -->			
		</div>
		<!--//  main content -->
		<%@ include file="../main/m_menu.jsp" %>
	</div>
	<!--// CS 점검 관리  page -->
	
	<!-- 점검 목록 리스트  page -->
	<div data-role="page" id="inspect-page" data-url="inspect-page" class="jqm-demos" data-title="점검 목록 리스트">
		<div data-role="header" data-positon="fixed">
			<h1>점검 목록 리스트</h1> 
			<a href="#install-page" data-direction="reverse" class="ui-back-btn" data-role="button" role="button"><i class="icon-circle-left2"></i></a>
			<a data-ajax="false" href="/indexApp" class="ui-back-btn ui-btn-right" data-role="button" role="button" title="로그아웃"><i class="icon-exit"></i></a>
		</div>	
		<!-- main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<div class="ui-grid-solo">
				<div class="ui-block-a text-right margin-10px-bottom">
					<a href="#addcsProduct-page"  class="btn bg-primary">긴급 점검 추가</a>
				</div>
			</div>
			<!-- list 반복 -->
			<div id="inspectApp" class="ui-grid-solo">
			</div>
			<!--// list 반복 -->
			
			<!-- 점검 목록 상세 팝업창  (버튼 a href == 팝업 id값 동일) -->
			<div data-role="popup" id="csproductinfo" data-overlay-theme="a">
				<a href="#" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
				<%@ include file="../install/csinfoApp.jsp" %>
			</div>		
			<!--// 점검 목록 상세 등록 팝업창 -->		
			
			<!-- 에정밀 변경 팝업창 (버튼 a href == 팝업 id값 동일) -->
			<div data-role="popup" id="dateChange" data-overlay-theme="a">
				<a href="#" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
				<div class="popup-content">	
					<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;방문 예정일 변경</h4>
					<div class="form-group">
						<input type="date" name="date" />
						<input type="time" name="time" />
						<input type="hidden" id="cl_idx" name="cl_idx">
					</div>					
					<div class="text-center margin-15px-top">
						<a href="javascript:update_date()" class="width-100 btn btn-submit">변경</a>					
					</div>
				</div>
			</div>	
			<!--// 에정밀 변경 팝업창 -->	
		</div>
		<!--// main content -->
		<%@ include file="../main/m_menu.jsp" %>
	</div>
	<!--// 점검 목록 리스트  page -->	
	
	<div data-role="page" id="addcsProduct-page" data-url="addcsProduct-page" class="jqm-demos" data-title="긴급 점검 추가">
		<div data-role="header" data-positon="fixed">
			<h1>긴급 CS 점검 추가</h1> 
			<a href="#inspect-page" data-direction="reverse" class="ui-back-btn" data-role="button" role="button"><i class="icon-circle-left2"></i></a>
		</div>	
		<!-- main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<div class="ui-grid-solo">
				<div class="ui-block-a">
					<form role="form" name="frm" method="post" enctype="multipart/form-data">
						<input type="hidden" id="m_idx" name="m_idx">
						<!-- 1. CS 항목 정보 -->
						<div class="width-100">
							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. CS 항목 정보</h4>
							<div class="form-group">
								<label for="date">요청일</label>
								<input type="date" name="date" />
								<input type="time" name="time" />
							</div>
							<div class="form-group">
								<p class="text-small">사용구분</p>
								<div data-role="controlgroup" data-type="horizontal" data-mini="true">
									<input type="radio" name="csm_satus" id="radio-choice-c" value="1" >
									<label for="radio-choice-c">사용</label>
									<input type="radio" name="csm_status" id="radio-choice-d" value="0">
									<label for="radio-choice-d">미사용</label>
								</div>
							</div>
							<div class="form-group">
								<label for="csm_memo">렌탈 상품군</label>
								<input type="text" name="csm_rental" value="${pr.e_name}" />
							</div>
						</div>
						<!--// 1. CS 항목 정보 -->
						<!-- 2. CS 소모품 구정 -->
						<div class="margin-25px-top clearfix">
							<h4 class="display-inline-block"><i class="icon-folder-search"></i>&nbsp;&nbsp;2. CS 소모품 구성</h4>	
							<div class="float-right">
								<a href="#addProduct" onclick="plist()" data-rel="popup" data-position-to="window" data-transition="pop" class="btn btn-sm bg-warning">상품추가</a>		
							</div>
								<!-- 상품 리스트 반복 -->
							<div id="select_prd">
							
								<!--// 상품 리스트 반복 -->
							</div>
							
							<!-- CS 소모품 추가 팝업창  (고객명 a href == 팝업 id값 동일) -->
							<div data-role="popup" id="addProduct" data-overlay-theme="a">
								<a href="javascript:;" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
								<div class="popup-content">
									<h4><i class="icon-menu2"></i>&nbsp;&nbsp;긴급 CS 소모품 추가</h4>
									<div id="cslist"></div>
								</div>
							</div>		
							<!--// CS 소모품 추가 팝업창  -->									
						</div>
						<!--// 2. CS 소모품 구정 -->
						<!-- 3. CS 상세정보 -->
						<div class="margin-25px-top">
							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;3. CS 상세정보</h4>
							<div class="form-group">
								<label for="csm_name">CS 제목</label>
								<input type="text" data-clear-btn="true" name="csm_name" id="csm_name" value="" />
							</div>
							<div class="form-group">
								<label for="csm_memo">CS 상세내용</label>
								<input type="text" data-clear-btn="true" name="csm_memo" id="csm_memo" value="" />
							</div>		
						</div>
						<!--// 3. CS 상세정보 -->
						<!-- 4. 공임 -->
						<div class="margin-25px-top">
							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;4. 공임</h4>
							<div class="form-group">
								<label for="csm_installpee">CS 공임</label>
								<input type="text" data-clear-btn="true" name="csm_installpee" id="csm_installpee" value="" />
							</div>
						</div>
						<input type="hidden" id="cl_idx" name="cl_idx" value="">
						<!--// 4. 공임 -->
					</form>					
				</div>
			</div>
			<div class="margin-15px-top">
			
				<a href="javascript:startRental()" class="width-100 btn btn-submit">추가</a>				
			</div>
		</div>	
		<!--// main content -->	
	</div>
	<!-- 긴급 점검 추가  page -->
	
	<!--// 긴급 점검 추가  page -->	
	
	
	<%@include file="/WEB-INF/view/main/m_js.jsp"%>
	<script type="text/javascript">
	/**설치상태변경*/
	function addCsInstall(){
		var cl_idx =$(event.target).attr('cl_idx');
		var ds = $(event.target).attr('state');
		var csds_idx = $(event.target).attr('csds_idx');
		var m_idx = $('input[name=m_idx]').val();
		
		if(ds.trim()=='설치요청'){
			if(confirm('점검완료 하시겠습니까?')==true){
				$.ajax({
					url:'/install/csInstallComplete/'+csds_idx,
					type:"POST",
					data:{'cl_idx':cl_idx},
					success :function(data){
						if (data.trim() == '등록 완료') {
							alert("점검완료처리 되었습니다.");
							$('#inspectApp').empty();
							 $.ajax({
									url:'/install/inspectApp/'+m_idx,
									success : function(data){
										$('#inspectApp').append(data);
										$('input[name=m_idx]').val(m_idx);
									}
							});
							
						} else {
							alert("점검완료처리 실패하였습니다.");
						}
					}
				});
				
				}else{
					return false;
				}
		}else{
			
		
		if(confirm('설치요청 하시겠습니까?')==true){
			
			$.ajax({
				url:'/install/csInstall/'+cl_idx,
				type:"POST",
				success :function(data){
					if (data.trim() == '등록 완료') {
						alert("점검요청처리 되었습니다.");
						$('#inspectApp').empty();
						 $.ajax({
								url:'/install/inspectApp/'+m_idx,
								success : function(data){
									$('#inspectApp').append(data);
									$('input[name=m_idx]').val(m_idx);
								}
						});
					} else {
						alert("설치요청처리 실패하였습니다.");
					}
				}
			});
			
			}else{
				return false;
			}
		
		}
	}
		 
		 
		 
		 
		 
		 function csInfo(){
			var m_idx = $(event.target).attr('m_idx');
			$('#inspectApp').empty();
			 $.ajax({
					url:'/install/inspectApp/'+m_idx,
					success : function(data){
						$('#inspectApp').append(data);
						$('input[name=m_idx]').val(m_idx);
					}
			});
		 }
		 
		 function customerInfo(){
				var m_idx = $(event.target).attr('m_idx');
				$('#contractApp').empty();
				$.ajax({
					url:'/customer/contractApp2/'+m_idx,
					type:'GET',
					success : function(data){
						$('#contractApp').append(data);
					}
				});		
			}
		 
		 function csproductinfo(){
			 var csm_idx = $(event.target).attr('csm_idx');
			 $('#csinfoApp').empty();
			 $.ajax({
					url:'/install/csList/'+csm_idx,
					success : function(data) {
						$('#csinfoApp').append(data);
					}
				});
			}
		 
		 function dateChange(){
			 var cl_idx = $(event.target).attr('cl_idx');
			 $('#cl_idx').val(cl_idx);
		 	$('input[name=date]').val('');	
		 	$('input[name=time]').val('');	
		 };
		 
		 function update_date() {
				var date = $('input[name=date]').val();
				var time = $('input[name=time]').val();
				var cl_idx = $('input[name=cl_idx]').val();
				var m_idx = $('input[name=m_idx]').val();
				$.ajax({
					url : '/install/inspectUpdate',
					type : 'post',
					data : {
						'date' : date,
						'time' : time,
						'cl_idx' : cl_idx
					},
					success : function(data) {
						if (data.trim() == '등록 완료') {
							alert("예정일을 변경하였습니다.");
							$('#dateChange').popup('close');
							$('#inspectApp').empty();
							 $.ajax({
									url:'/install/inspectApp/'+m_idx,
									success : function(data){
										$('#inspectApp').append(data);
									}
							});
						} else {
							alert("예정일 변경 실패하였습니다.");
						}
					}
				});
			}
		 
			 function choose(e) {
					var csm_id = $(e).attr('idx');
						$.ajax({
							url : '/install/productUl/' + csm_id,
							success : function(data) {
								$('#select_prd').append(data);
								$('#addProduct').popup('close');
							}
						});
				}
		 
		 function plist(){
			 $('#cslist').empty();
			 $.ajax({
				 url:'/install/productList',
				 success : function(data){
						$('#cslist').append(data);
				 }
			 });
		 }
		 
		 function startRental(){
				var param=$('form[name=frm]').serialize();
				$.ajax({
					url:'addcsProductApp',
					type:'post',
					data:param
				})
				.done(function(data){
					alert(data);
					if(data.trim()=='등록 완료'){
						$('#addcsProduct-page').popup('close');
						alert("긴급점검이 추가되었습니다.");
						$('#inspectApp').empty();
						 $.ajax({
								url:'/install/inspectApp/'+m_idx,
								success : function(data){
									$('#inspectApp').append(data);
								}
						});
					}else{
					}
				});
			}
		 
			function cslist(){
				var csm_idx = $(event.target).attr('csm_idx');
				$('#csproduct').empty();
				$.ajax({
					url:'/install/csList/'+csm_idx,
					type:'GET',
					success:function(data){
						$('#csproduct').append(data);
						$('#cl_idx').val(cl_idx);
					}
				});
			}
		 
		 
	</script>
</body>
</html>