<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>설치관리 (관리자)</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">설치관리 (관리자)</span>
							<small class="display-block"></small>
						</h4>
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body"> 
									<form name="searchForm"  onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">검색</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="mainKeyword">
							                        <span class="help-block">상품명, 신청고객명</span>
												</div>
											</div> 
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>					
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>고객 조회결과</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		 
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<th >선택</th>
											<th>설치완료일</th>
											<th>판매처</th>
											<th>판매기수</th>
											<th>설치상품</th>
	
											<th>설치고객</th>
											<th>연락처</th>
											<th>주소</th>
											<th>접수구분</th>
											<th>상태구분</th>
	
											<th>설치 전달사항</th>
											<th>설치담당자</th>
											<th>설치비용</th>
											<th>추가비용</th>
											<th>정산상태</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled" onclick="adjust()"><b><i class="icon-air"></i></b>정산</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	
	<%@include file="/WEB-INF/view/install/modal/inspect.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "m_idx" },
		    { "data": "if_log"},
		    { "data": "m_sale"},
		    { "data": "gubun"},
		    { "data": "pr_name"},
		   
		    { "data": "mi_name"},
		    { "data": "m_finance"},
		    { "data": "mi_addr"},
		    { "data": "m_jubsoo"},
		
		    { "data": "m_jubsoo"},
		    { "data": "mi_memo"},
		    { "data": "i_ename"},
		    { "data": "pee"},
		    { "data": "if_extra"},
		    { "data": "jr_state"}
		  ]
	});
	tableLoad('checkTable');
	
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		checkDt('checkTable','/install/installCalculate');
	}
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#checkTable').DataTable();
		table.ajax.url('/install/installComplete?'+param).load(null,false);
	} 
	
	//현재 보이는 dataTable refresh
	function refreshTable(){
		var table=$('#checkTable').DataTable();
		table.ajax.reload(null,false);
	}
	function member(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/contract/'+m_idx+'/','popup','width=1200,height=600');
	}
	
	function install_member(){
		var m_idx =$(event.target).attr('m_idx');
		var cl_idx =$(event.target).attr('cl_idx');
		$('#art3_1').empty();
		$.ajax({
			url:'/install/install_customer/'+m_idx,
			data:{'cl_idx':cl_idx},
			type:'GET',
			success : function(data){
				$('#art3_1').append(data);
				$('#cl_idx').val(cl_idx);
			}
		});
	}
	
	function installComplete(){
		var m_idx =$(event.target).attr('m_idx');
		var mi_idx = $(event.target).attr('mi_idx');
		window.open('/install/addComplete?m_idx='+m_idx+'&mi_idx='+mi_idx ,'popup','width=700,height=400');
	}
		
	function rentalProduct(){
		var pr_idx=$(event.target).attr('pr_idx');
		window.open('/product/rentalProductComp/'+pr_idx,'popup','width=1400,height=800');
	}
	
	function checkIdxs(){
		var checked='';
		$('.selected>td>input[name=m_idx]').each(function(){
	        checked+=this.value+',';
	     });
		if(checked!=null&&checked!=''){
			checked=checked.substring(0,checked.length-1);	
		}
		return checked;
	}
	
	
	function adjust(){
		var param=checkIdxs();
		window.open('/install/adjust?idx='+param,'popup','width=1400,height=800');
	}
	
	
	
	</script>
</body>
</html>