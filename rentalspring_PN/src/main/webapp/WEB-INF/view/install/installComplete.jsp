<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>CS점검관리 (관리자)</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">CS점검관리 (관리자)</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body"> 
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">검색</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="mainKeyword">
							                        <span class="help-block">상품명, 신청고객명</span>
												</div>
											</div>
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>					
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>고객 조회결과</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<th >선택</th>
											<th>설치(방문)예정일</th>
											<th>설치완료일</th>
											<th>판매처</th>
											<th>판매기수</th>
										
											<th>설치상품</th>
											<th>신청고객/계약고객</th>
											<th>연락처(설치)</th>
											<th>주소(설치)</th>
											<th>접수구분</th>
										
											<th>매출구분</th>
											<th>상태구분</th>
											<th>설치(방문)전달사항</th>
											<th>설치(방문)담당자</th>
											<th>설치확인서</th>
											<th>점검</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="productDel('display', '1')"><i class="icon-eye"></i>진열</a></li>
												<li><a onclick="productDel('display', '0')"><i class="icon-eye-blocked"></i>미진열</a></li>
												<hr>
												<li><a onclick="productDel('sel', '1')">판매중</a></li>
												<li><a onclick="productDel('sel', '0')">판매안함</a></li>
												<hr> 
												<li><a onclick="productDel('copy', 'copy')"><i class="icon-copy3"></i>복제</a></li>
												<li><a onclick="productDel('del', 'del')"><i class="icon-trash-alt"></i>삭제</a></li>
												<li><a onclick="refreshTable()"><i class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	
	<%@include file="/WEB-INF/view/install/modal/inspect.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "m_idx" },
		    { "data": "m_eta"},
		    { "data": "if_log"},
		    { "data": "m_sale"},
		    { "data": "mg_name"},
		  
		    { "data": "pr_name"},
		    { "data": "mi_name"},
		    { "data": "m_finance"},
		    { "data": "mi_addr"},
		    { "data": "m_jubsoo"},
		
		    { "data": "m_jubsoo"},//1회차등록
		    { "data": "ds_state"},
		    { "data": "mi_memo"},
		    { "data": "i_ename"},
		    
		    { "data": "m_tech"},
		    { "data": "inspect"}
		    
		  ]
	});
			tableLoad('checkTable');
	
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		checkDt('checkTable','/install/installComplete');
	}
	
function rentalProduct(){
	var pr_code=$(event.target).attr('pr_rentalcode');
	window.open('/product/rentalProductComp/'+pr_code,'popup','width=1400,height=800');
}

function inspect(){
	var m_idx =$(event.target).attr('m_idx');
	$('.modal-body #i_mid').val(m_idx);
	var e_id = $(event.target).attr("e_id");
	$('#datebox').empty();
	$.ajax({
		url:'/install/inspect/'+m_idx,
		success : function(data){
			$('#datebox').append(data);
			$('input[name=m_idx]').val(m_idx);
			$('#modal_large2').modal('show');
			$('input[name=cl_eid]').val(e_id);
		}
	});
}
	
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#checkTable').DataTable();
		table.ajax.url('/install/installComplete?'+param).load(null,false);
	} 
	
	//현재 보이는 dataTable refresh
	function refreshTable(){
		var table=$('#checkTable').DataTable();
		table.ajax.reload(null,false);
	}
	function member(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/contract/'+m_idx+'/','popup','width=1200,height=600');
	}
	
	function install_member(){
		var m_idx =$(event.target).attr('m_idx');
		var cl_idx =$(event.target).attr('cl_idx');
		$('#art3_1').empty();
		$.ajax({
			url:'/install/install_customer/'+m_idx,
			data:{'cl_idx':cl_idx},
			type:'GET',
			success : function(data){
				$('#art3_1').append(data);
				$('#cl_idx').val(cl_idx);
			}
		});
	}
	
	function installComplete(){
		var m_idx =$(event.target).attr('m_idx');
		var mi_idx = $(event.target).attr('mi_idx');
		window.open('/install/addComplete?m_idx='+m_idx+'&mi_idx='+mi_idx ,'popup','width=700,height=400');
	}
		
	function rentalProduct(){
		var pr_idx=$(event.target).attr('pr_idx');
		window.open('/product/rentalProductComp/'+pr_idx,'popup','width=1400,height=800');
	}
	
	function inspect(){
		var m_idx =$(event.target).attr('m_idx');
		$('.modal-body #i_mid').val(m_idx);
		var e_id = $(event.target).attr("e_id");
		$('#datebox').empty();
		$.ajax({
			url:'/install/inspect/'+m_idx,
			success : function(data){
				$('#datebox').append(data);
				$('input[name=m_idx]').val(m_idx);
				$('#modal_large2').modal('show');
				$('input[name=cl_eid]').val(e_id);
			}
		});
	}     
	
	</script>
</body>
</html>