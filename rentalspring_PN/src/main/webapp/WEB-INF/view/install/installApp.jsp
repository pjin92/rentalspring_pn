<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>설치관리</title>
<%@include file="/WEB-INF/view/main/m_css.jsp"%>
<style type="text/css">
#changeDate-popup {
	width: 90%;
}
</style>
</head>
<body onload="$('#divLoading').remove();">
	<div id="divLoading"
		style="width: 2000px; height: 2000px; background: white; position: fixed; top: -50px; left: -50px; z-index: 9999;"></div>
	<!-- 설치관리 리스트 page -->
	<div data-role="page" id="install-page" data-url="install-page"
		data-title="설치관리">
		<div data-role="header" data-positon="fixed">
			<h1>설치관리</h1>
			<a data-ajax="false" href="/indexApp"
				class="ui-back-btn ui-btn-right" data-role="button" role="button"
				title="로그아웃"><i class="icon-exit"></i></a>
		</div>
		<!-- search input -->
		<!-- <div class="ui-grid-solo search-section">
			<div class="ui-block-a search-wrap">
				<form name="searchForm">
					<input type="text" name="mainKeyword" id="mainKeyword" placeholder="상품명, 설치고객명" />
					<button type="submit" class="search-btn" onsubmit="searchBtn()"><i class="icon-search4"></i></button>
				</form>
			</div>
		</div> -->
		<!--// search input -->

		<table style="text-align: center; margin: auto; width: 100%">
			<tr>
				<td><input type="checkbox" name="checkbox-1" id="checkbox-1"
					class="custom" onclick="check()" value="설치요청" /> <label
					for="checkbox-1">설치요청</label> <input type="checkbox"
					name="checkbox-2" id="checkbox-2" class="custom" onclick="check()"
					value="설치완료" /> <label for="checkbox-2">설치완료</label></td>
				<td><input type="checkbox" name="checkbox-3" id="checkbox-3"
					class="custom" onclick="check()" value="대기" /> <label
					for="checkbox-3">대기</label> <input type="checkbox"
					name="checkbox-4" id="checkbox-4" class="custom" onclick="check()"
					value="체험중" /> <label for="checkbox-4">체험중</label></td>
			</tr>
		</table>

		<!-- main content -->
		<div id="list">
			<%-- <!-- 리스트 section -->
			<div class="ui-grid-solo">
				<!-- 리스트 반복 -->
				<c:forEach var="in" items="${data}">
				<div class="ui-block-a list-box-wrap">
					<div class="clearfix">
						<div class="list-left-box">	
							<p class="bg-danger"><b>${in.ds_state }</b></p>
							<c:choose>
							<c:when test="${in.newStatus eq '0' }">
							<p class="bg-orange"><b>NEW</b></p>
							</c:when>
							<c:otherwise>
							</c:otherwise>
							</c:choose>
							<c:choose>
							<c:when test="${in.if_log eq ''}">
							<p class="bg-gray"><b>${in.m_eta}</b></p>
							</c:when>
							<c:otherwise>
							<p class="bg-gray" ><b>${in.if_log}</b></p>
							</c:otherwise>
							</c:choose>
							<a href="#dateChange" data-rel="popup" data-position-to="window" onclick="update_date()" data-transition="pop" class="btn btn-sm bg-pink float-right" style="margin-top:-5px;">예정일 변경</a>
						</div>
						<div class="list-right-box">
							<ul class="list-wrap">
								<li class="">
									<p class="list-tit">상품명 1: <strong class="list-cont">${in.pr_name }</strong></p>
								</li>
								<li class="">
									<p class="list-tit">설치고객명1 :${in.mi_name} </p>
								</li>
								<li class="">
									<p class="list-tit">주소 : <strong class="list-cont">${in.mi_addr}</strong></p>
								</li>
								<li class="">
									<p class="list-tit">연락처 : <strong class="list-cont"><a href="tel:010-1234-5678">${in.mi_tel}</a></strong></p>
								</li>
								<li class="">
									<p class="list-tit">연락처 : <strong class="list-cont"><a href="tel:010-1234-5678">${in.mi_tel}</a></strong></p>
								</li>
								<li class="clearfix">
									<p class="list-tit display-inline-block">예정일 : <a href="#schedule"  data-rel="popup" data-position-to="window" data-transition="pop">${cl.cl_date}</a></p>
									<a href="#dateChange" data-rel="popup" data-position-to="window" cl_idx= "${cl.cl_idx}" onclick="dateChange()" data-transition="pop" class="btn btn-sm bg-pink float-right" style="margin-top:-5px;">예정일 변경</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				</c:forEach>
				<!--// 리스트 반복 -->												
			</div>	
			<!--// 리스트 section --> --%>
		</div>
		<!--// main content -->
		<!-- 예정일 변경 -->
		<div data-role="popup" id="changeDate" data-overlay-theme="a">
			<a href="#" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
			<div class="popup-content">
				<h4>
					<i class="icon-folder-search"></i>&nbsp;&nbsp;방문 예정일 변경
				</h4>
				<div class="form-group">
					<input type="date" name="date" /> <input type="time" name="time" />
					<input type="hidden" id="m_idx" name="m_idx">
				</div>
				<div class="text-center margin-15px-top">
					<a href="javascript:update_date()" class="width-100 btn btn-submit">변경</a>
				</div>
			</div>
		</div>
		<!-- 예정일 변경 -->
		<%@ include file="../main/m_menu.jsp"%>
		<%@include file="/WEB-INF/view/main/js.jsp"%>
	</div>
	<!--// 설치관리 리스트 page -->

	<!-- 고객정보 page -->
	<div data-role="page" id="customerInfo" data-url="customerInfo"
		data-title="고객정보">
		<div data-role="header" data-positon="fixed">
			<h1>고객정보</h1>
			<a href="#install-page" data-direction="reverse" class="ui-back-btn"
				data-role="button" role="button"><i class="icon-circle-left2"></i></a>
			<!-- onclick="refresh()" -->
			<a data-ajax="false" href="/indexApp"
				class="ui-back-btn ui-btn-right" data-role="button" role="button"
				title="로그아웃"><i class="icon-exit"></i></a>
		</div>
		<!-- main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<%@ include file="../customer/contractApp.jsp"%>
		</div>
	</div>

	<!-- 상품상세 페이지 -->
	<div data-role="page" id="productInfo" data-url="productInfo"
		data-title="고객정보">
		<div data-role="header" data-positon="fixed">
			<h1>상품 상세내역</h1>
			<a href="#install-page" data-direction="reverse" class="ui-back-btn"
				data-role="button" role="button"><i class="icon-circle-left2"></i></a>
			<!-- onclick="refresh()" -->
			<a data-ajax="false" href="/indexApp"
				class="ui-back-btn ui-btn-right" data-role="button" role="button"
				title="로그아웃"><i class="icon-exit"></i></a>
		</div>
		<!-- main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<%@ include file="../product/productApp.jsp"%>
		</div>
		<%@ include file="../main/m_menu.jsp"%>
	</div>
	<!--// 고객정보 page  -->

	<!-- 상품구성변경 page  -->
	<div data-role="page" id="changeProduct" data-url="changeProduct"
		data-title="상품구성변경">
		<div data-role="header" data-positon="fixed">
			<h1>상품구성변경</h1>
			<a href="#install-page" data-direction="reverse" class="ui-back-btn"
				data-role="button" role="button"><i class="icon-circle-left2"></i></a>
			<!-- onclick="refresh()"  -->
			<a data-ajax="false" href="/indexApp"
				class="ui-back-btn ui-btn-right" data-role="button" role="button"
				title="로그아웃"><i class="icon-exit"></i></a>
		</div>
		<!-- main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<%@ include file="../product/modal/changeProductApp.jsp"%>
		</div>
		<!--// main content -->
	</div>
	<!--// 상품구성변경 page  -->

	<%@include file="/WEB-INF/view/main/m_js.jsp"%>

	<script>
		function customerInfo() {
			var m_idx = $(event.target).attr('m_idx');
			var mi_idx = $(event.target).attr('mi_idx');
			$('#contractApp').empty();
			$('#ifInfo').html('');
			$.ajax({
				url : '/customer/contractApp/' + m_idx,
				data : {
					'mi_idx' : mi_idx
				},
				type : 'GET',
				success : function(data) {
					$('#contractApp').append(data);
					$('input[name=m_idx]').val(m_idx);
					$('input[name=mi_idx]').val(mi_idx);
				}
			});
			$('#ifInfo').load('/install/contractAppInstallFileInfo/' + m_idx);
		}

		function productInfo() {
			var pr_idx = $(event.target).attr('pr_idx');
			var m_idx = $(event.target).attr('m_idx');
			var mi_idx = $(event.target).attr('mi_idx');
			$('#productApp').empty();
			$.ajax({
				url : '/product/productApp/' + pr_idx,
				type : 'GET',
				success : function(data) {
					$('#p_pr_idx').val(pr_idx);
					$('#p_m_idx').val(m_idx);
					$('#p_mi_idx').val(mi_idx);
					$('#productApp').append(data);
				}
			});
		}

		function refresh() {
			history.go(0);
		}

		function check() {
			var param = '';

			if ($('#checkbox-1').is(":checked")) {
				param += 'stat1=1&';
			}

			if ($('#checkbox-2').is(":checked")) {
				param += 'stat2=2&';
			}

			if ($('#checkbox-3').is(":checked")) {
				param += 'stat3=3&';
			}

			if ($('#checkbox-4').is(":checked")) {
				param += 'stat4=4';
			}
			// installAppContent.jsp
			$.ajax({
				url : '/install/installApp',
				data : param,
				type : 'post',
				success : function(data) {
					$('#list').html(data);
				}
			});
		}

		function changeDate(m_idx_t) {
			$('#m_idx').val(m_idx_t);
			$('input[name=date]').val('');
			$('input[name=time]').val('');
		}

		function calculate() {
			var pr = $('input[name="p_pr_idx"]').val();
			var midx = $('input[name="p_m_idx"]').val();
			var miidx = $('input[name="p_mi_idx"]').val();
			var rentalcode = $('input[name=pr_rentalcode]').val();
			$('#rproduct').empty();
			if(rentalcode.match("led-")){
			$.ajax({
				url : '/product/calculateApp/' + pr,
				type : 'GET',
				success : function(data) {
					$('#cal_m_idx').val(midx);
					$('#cal_mi_idx').val(miidx);
					$('#pr_name').val('');
					$('#rproduct').append(data);

					//3.렌탈 상품 가격 정보 초기화
					$('#pr_install').val(0);
					$('#num_temp').val(0);
					$('#install_temp').val(0);
					$('#pr_total').val(0);
					$('#temp').val(0);
					$('#pr_period').val($('input[name=period]').val());
					$('#pr_period').val();
					$('#pr_rental').val(0);
					$('#pr_ilsi').val(0);
					$('#pr_aff').val('');
					$('#pr_aff_cost').val(0);
				}
			});
		}else if(rentalcode.match("LEDEFU")){
			$.ajax({
				url : '/product/calculateApp/' + pr,
				type : 'GET',
				success : function(data) {
					$('#cal_m_idx').val(midx);
					$('#cal_mi_idx').val(miidx);
					$('#pr_name').val('');
					$('#rproduct').append(data);

					//3.렌탈 상품 가격 정보 초기화
					$('#pr_install').val(0);
					$('#num_temp').val(0);
					$('#install_temp').val(0);
					$('#pr_period').val($('input[name=period]').val());
					$('#pr_total').val(0);
					$('#temp').val(0);
					$('#pr_rental').val(0);
					$('#pr_ilsi').val(0);
					$('#pr_aff').val('');
					$('#pr_aff_cost').val(0);
				}
			});
			
			
			
		}else{
			$.ajax({
				url : '/product/normalCalculateApp/' + pr,
				type : 'GET',
				success : function(data) {
					$('#cal_m_idx').val(midx);
					$('#cal_mi_idx').val(miidx);
					$('#pr_name').val('');
					$('#rproduct').append(data);

					//3.렌탈 상품 가격 정보 초기화
					$('#pr_install').val(0);
					$('#num_temp').val(0);
					$('#install_temp').val(0);
					$('#pr_period').val($('input[name=period]').val());
					$('#pr_total').val(0);
					$('#temp').val(0);
					$('#pr_rental').val(0);
					$('#pr_ilsi').val(0);
					$('#pr_aff').val('');
					$('#pr_aff_cost').val(0);
				}
			});
			
		}

		
		
		};

		function update_date() {
			var date = $('input[name=date]').val();
			var time = $('input[name=time]').val();
			var m_idx = $('input[name=m_idx]').val();
			$.ajax({
				url : '/install/installdateChange',
				type : 'post',
				data : {
					'date' : date,
					'time' : time,
					'm_idx' : m_idx
				},
				success : function(data) {
					if (data.trim() == '등록완료') {
						alert("예정일을 변경하였습니다.");
						location.href = '/install/installApp';

					} else {
						alert("예정일 변경 실패하였습니다.");
						location.href = '/install/installApp';
					}
				}
			});
		}

		function cancel(jubsoo, m_idx) {
			if (confirm('방문 후  취소하시겠습니까?')) {
				$.ajax({
					url : '/install/cancel',
					data : 'm_idx=' + m_idx,
					type : 'POST'
				}).done(function(data) {
					alert(data.trim());
					if (data.trim() == '취소완료') {
						alert("취소처리 되었습니다.");
						location.href = '/install/installApp';
					}

				});
			}
		}

		function cancelBuy(ds_idx, m_idx) {
			if (confirm('방문 전 취소(구매취소)하시겠습니까?')) {
				$.ajax(
						{
							url : '/delivery/' + ds_idx
									+ '?ds_state=cancel&url=' + m_idx,
							type : 'put'
						}).done(function(data) {
					alert(data);
					check();
				});
			}
		}

		//대기
		function waiting(m_idx) {
			if (confirm('대기 처리하시겠습니까?')) {
				$.ajax({
					url : '/member/waiting?m_idx=' + m_idx,
					type : 'put'
				}).done(function(data) {
					alert(data);
					location.href = '/install/installApp';
				});
			}
		}

		//무료체험 팝업 띄우기
		function testFree(m_idx) {
			$('input[name=m_free_begin]').val('');
			$('input[name=m_free_day]').val('');
			$('input[name=m_free_memo]').val('');
			$('input[name=m_free_finish]').val('');

			$.ajax({
				url : '/member/' + m_idx + '/freeday',
				type : 'GET',
			}).done(function(data) {
				$('input[name=m_free_day]').val(data.trim());
				$('#freeTest').popup('open');
			});
		}

		function endDate() {
			var date = $('input[name=m_free_begin]').val();
			var beginDate = new Date(date);

			var freeday = Number($('input[name=m_free_day]').val());
			var endDate = new Date(date);
			endDate.setDate(endDate.getDate() + freeday);
			$('input[name=m_free_finish]').val(
					endDate.toISOString().substring(0, 10));
		}

		function freeTest() {
			var param = $('form[name=frm]').serialize();
			$.ajax({
				url : '/install/changeTestFree',
				type : 'POST',
				data : param
			}).done(function(data) {
				if (data.trim() == '등록완료') {
					alert('무료체험이 등록되었습니다.');
					location.href = '/install/installApp';
				} else {
					alert(data);
				}
			});
		}
	</script>
</body>
</html>