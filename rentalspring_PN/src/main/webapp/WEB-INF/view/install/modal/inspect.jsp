<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="modal_large2" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i>
						<span id="modal_large_title">점검목록리스트 </span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal">
							<i class="icon-cross2" style="cursor: pointer" onclick="modal_close()"></i>
						</span>
					</h4>
					<div class="text-right mt-10">
						<a data-toggle='modal' href="#myModal6" class="btn btn-primary">긴급점검추가</a>
					</div>
				</div>
			
				<div class="modal-body" style="overflow:auto;height:150px; white-space:nowrap;">
					<input type="hidden" id="m_idx" name="m_idx">
					<div class="form-group-attached" >
						<div id ="datebox" class="row">
						</div>
					</div>
				</div>
				
				<div class="modal-body">
					<div class="panel" id="modal_large_content">
						<div class="panel-body"> 
							<table class="table table-hover nowrap" id="checkTable">
								<thead>
									<tr>
										<th>설치일자</th>
										<th>렌탈만료일자</th>
										<th>방문예정일</th>
										<th>상품명</th>
										<th>CS명</th>
	
										<th>고객명</th>
										<th>연락처</th>
										<th>주소</th>
										<th>우편번호</th>
										<th>기사</th>
										<th>점검상태</th>
								</thead>
								<tbody id="inspect">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a class="btn bg-grey-400" onclick="modal_close()"> 닫기 </a>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="/WEB-INF/view/install/modal/change_technician.jsp"%>
<%@include file="/WEB-INF/view/install/modal/install_customer.jsp"%>
<%@include file="/WEB-INF/view/install/modal/changeDate.jsp"%>
<%@include file="/WEB-INF/view/install/modal/icsList.jsp"%>
<!-- 긴급점검추가 -->
<%@include file="/WEB-INF/view/install/modal/addcsProduct.jsp"%>

<script>
	function modal_close() {
		$('#inspect').empty();
		$('#modal_large2').modal('hide');
	}
	function select_Date(){
		$('#inspect').empty();
		var m_idx =$(event.target).attr('m_idx');
		var cl_idx =$(event.target).attr('cl_idx');
		$.ajax({
			url:'/install/inspectDetail',
			data :{'m_idx':m_idx, 'cl_idx':cl_idx},
			success :function(data){
				$('#inspect').append(data);
			}
		});
	}
	/**예정일 변경*/
	function update_date() {
		var date = $('input[name=date]').val();
		var time = $('input[name=time]').val();
		var cl_idx = $('input[name=clidx]').val();
		var m_idx = $('input[name=m_idx]').val();
		$.ajax({
			url : '/install/inspectUpdate',
			type : 'post',
			data : {
				'date' : date,
				'time' : time,
				'cl_idx' : cl_idx
			},
			success : function(data) {
				if (data.trim() == '등록 완료') {
					alert("예정일을 변경하였습니다.");
					
					$('#datebox').empty();
					$.ajax({
						url:'/install/inspect/'+m_idx,
						success : function(data){
							$('#datebox').append(data);
							$('#myModal2').modal('hide');
						}
					});
					$('#inspect').empty();
					$.ajax({
						url:'/install/inspectDetail',
						data :{'m_idx':m_idx, 'cl_idx':cl_idx},
						success :function(data){
							$('#inspect').append(data);
						}
					});
						
				} else {
					alert("예정일 변경 실패하였습니다.");
				}

			}
		});
	}
	/**날짜 변경 오픈*/
	function changeDate() {
		var cl_idx =$(event.target).attr('cl_idx');
		$('#clidx').val(cl_idx);
	}
/**기사 리스트 오픈*/
	function technician() {
		var m_idx = $(event.target).attr('m_idx');
		var mi_idx = $(event.target).attr('mi_idx');
		var cl_idx = $(event.target).attr('cl_idx');
		
		$('#technician').empty();
		$.ajax({
			url : '/install/updateTechnician',
			data:{'cl_idx':cl_idx,'m_idx':m_idx,'mi_idx':mi_idx},
			
			success : function(data) {
				$('#technician').append(data);
			}
		});
	}
	/**기사 변경*/
	function updateMatchTechnician(){
		var cl_idx = $(event.target).attr('cl_idx');
		var m_idx = $(event.target).attr('m_idx');
		var e_id = $(event.target).attr('e_id');
		$.ajax({
			url:'/install/updateTechnician',
			type:'POST',
			data:{'cl_idx':cl_idx,'m_idx':m_idx, 'e_id':e_id},
			success : function(data){
				if (data.trim() == '등록 완료') {
					alert("설치기사를 변경하였습니다.");
					$('#myModal3').modal('hide');
					$('#inspect').empty();
					$.ajax({
						url:'/install/inspectDetail',
						data :{'m_idx':m_idx, 'cl_idx':cl_idx},
						success :function(data){
							$('#inspect').append(data);
						}
					});
					
				} else {
					alert("설치기사를 변경 실패하였습니다.");
				}
			}
		});
	}
	/**CS 상세내역*/
	function cslist(){
		var csm_idx = $(event.target).attr('csm_idx');
		var cl_idx = $(event.target).attr('cl_idx');
		$('#csproduct').empty();
		$.ajax({
			url:'/install/csList/'+csm_idx,
			type:'GET',
			success:function(data){
				$('#csproduct').append(data);
				$('#cl_idx').val(cl_idx);
			}
		});
	}
	/**메모 업데이트*/
	function update_memo(){
		var cl_idx = $('input[name=cl_idx]').val();
		var cl_memo = $('input[name=cl_memo]').val();
		$.ajax({
			url:'/install/updateCsInstallMemo/'+cl_idx,
			type:'POST',
		
			data:{'cl_memo': cl_memo},
			success :function(data){
				if (data.trim() == '등록 완료') {
					alert("설치 메모가 등록되었습니다.");
					$('#myModal4').modal('hide');
				} else {
					alert("설치 메모 등록 실패하였습니다.");
				}
			}
		});
		
	}
	/**설치상태변경*/
	function addCsInstall(){
		var cl_idx = $(event.target).attr('cl_idx');
		var ds = $(event.target).attr('state');
		var csds_idx = $(event.target).attr('csds_idx');
		var m_idx = $('input[name=m_idx]').val();
		if(ds.trim()=='설치요청'){
			if(confirm('설치완료 하시겠습니까?')==true){
				$.ajax({
					url:'/install/csInstallComplete/'+csds_idx,
					type:"POST",
					data:{'cl_idx':cl_idx},
					success :function(data){
						if (data.trim() == '등록 완료') {
							alert("설치완료처리 되었습니다.");
							$('#inspect').empty();
							$.ajax({
								url:'/install/inspectDetail',
								data :{'m_idx':m_idx, 'cl_idx':cl_idx},
								success :function(data){
									$('#inspect').append(data);
								}
							});
							
						} else {
							alert("설치완료처리 실패하였습니다.");
						}
					}
				});
				
				}else{
					return false;
				}
		}else{
			
		
		if(confirm('설치요청 하시겠습니까?')==true){
			
			$.ajax({
				url:'/install/csInstall/'+cl_idx,
				type:"POST",
				success :function(data){
					if (data.trim() == '등록 완료') {
						alert("설치요청처리 되었습니다.");
						$('#inspect').empty();
						$.ajax({
							url:'/install/inspectDetail',
							data :{'m_idx':m_idx, 'cl_idx':cl_idx},
							success :function(data){
								$('#inspect').append(data);
							}
						});
					} else {
						alert("설치요청처리 실패하였습니다.");
					}
				}
			});
			
			}else{
				return false;
			}
		
		}
	}
	
	</script>
