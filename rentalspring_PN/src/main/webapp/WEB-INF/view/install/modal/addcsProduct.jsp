<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="modal modal-center" id="myModal6" aria-hidden="true" style="display: none; z-index: 1070;">
	<div class="modal-dialog">
		<div class="modal-content alpha-slate">
			<div class="modal-header">
				<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"aria-hidden="true">
					<i class="icon-cross2 cursor-pointer"></i>
				</span>
				<h4 class="modal-title">긴급CS점검</h4>
			</div>
			<form role="form" name="frm" method="post" enctype="multipart/form-data">
				<div class="modal-body" style="white-space:nowrap;">
					<input type="hidden" id="m_idx" name="m_idx">
					<div class="panel panel-flat border-top-xlg border-top-primary-300">
						<div class="panel-heading">
							<h5 class="panel-title">
								<i class="icon-folder-search position-left"></i>1. CS항목 정보
							</h5>
						</div>
						<div class="panel-body">
							<div class="form-group-attached">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-6">
										<div class="form-group form-group-default ">
											<label for="form_control_1">요청일</label> 
											<input type="date" class="datepicker" name="date" value=""> 
											<input type="time" class="" name="time" value=""> 
											<input type="hidden" id="clidx" name="clidx"> 
										</div>
									</div> 
									<div class="col-xs-4 col-sm-4 col-md-3">
										<div class="form-group form-group-default ">
											<label>사용구분</label> 
											<select class="select select2-hidden-accessible" name="csm_status">
												<option value="0">선택</option>
												<option value="1">사용</option>
												<option value="2">미사용</option>
											</select>
										</div>
									</div> 
									<div class="col-xs-4 col-sm-4 col-md-3">
										<div class="form-group form-group-default ">
											<label>렌탈상품군</label> 
											<input type="text" name="csm_rental" value="${pr.e_name}" class="form-control">
										</div>
									</div> 
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-flat border-top-xlg border-top-primary-300">
						<div class="panel-heading">
							<h5 class="panel-title">
								<i class="icon-folder-search position-left"></i>2. CS 소모품 구성
							</h5>
						</div>
						<div class="panel-body">
							<div class="form-group-attached">
								<div class="row">
									<div class="col-xs-12">
										<table class="table table-hover" id="productTable2">
											<thead>
												<tr>
													<th>상품명</th>
													<th>상품코드</th>
													<th>매입처</th>
													<th>원가</th>
													<th>판매가</th>
													<th>수량</th>
													<th>삭제</th>
												</tr>
											</thead>
											<tbody id="rproduct">
											</tbody>
											<tfoot>
												<tr>
													<td class="no-border" colspan="7"><a class="btn btn-sm bg-orange-600" onclick="plist()">상품 추가</a></td>
												</tr>											
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-flat border-top-xlg border-top-primary-300">
						<div class="panel-heading">
							<h5 class="panel-title">
								<i class="icon-folder-search position-left"></i>3. CS상세정보
							</h5>
						</div>
						<div class="panel-body">
							<div class="form-group-attached">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group form-group-default ">
											<label>CS제목</label> 
											<input type="text" name="csm_name" class="form-control">
											<input type="hidden" name="cl_eid" value="">
										</div>
									</div>

									<div class="col-xs-6 col-sm-12 col-md-6">
										<div class="form-group form-group-default">
											<label>CS상세내용</label> 
											<input type="text" name="csm_memo" class="form-control">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-flat border-top-xlg border-top-primary-300">
						<div class="panel-heading">
							<h5 class="panel-title">
								<i class="icon-folder-search position-left"></i>4. 공임
							</h5>
						</div>
						<div class="panel-body">
							<div class="form-group-attached">
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-6">
										<div class="form-group form-group-default">
											<label>CS공임</label> 
											<input type="text" name="csm_installpee" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="modal-footer">
				<input type="hidden" id="cl_idx" name="cl_idx" value=""> 
				<a href="#" class="btn bg-orange-600 position-left" data-dismiss="modal">추가</a> 
				<a onclick="startRental()" class="btn btn-primary">변경</a>
			</div>
		</div>
	</div>
</div>

<%@include file="/WEB-INF/view/install/modal/productList.jsp"%>
<script>
	
	
	
	
	
	function plist() {
		$('#csprod').empty();
		$.ajax({
			url:'/install/csproductList',
			success :function(data){
				$('#csprod').append(data);
				$('#modal_large').modal('show');
			}
		});
	}

	function refreshTable() {
		var table = $('#productTable').DataTable();
		table.ajax.reload(null, false);
	}
	
	function startRental(){
		var param=$('form[name=frm]').serialize();
		$.ajax({
			url:'addcsProduct',
			type:'post',
			data:param
		})
		.done(function(data){
			alert(data);
			if(data.trim()=='등록 완료'){
				$('#myModal6').modal('hide');
				$('#inspect').empty();
				alert("긴급점검이 추가되었습니다.");
				$('#inspect').empty();
				$.ajax({
					url:'/install/inspectDetail',
					success :function(data){
						$('#inspect').append(data);
					}
				});			
			}else{
			}
		});
	}
</script>