
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="myModal5" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span
							id="modal_large_title">CS 소모품 </span><span class="text-light"></span>
						<span
							class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right"
							data-dismiss="modal"><i class="icon-cross2"
							style="cursor: pointer"></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel" >
						<table class="table table-hover">
							<thead>
								<tr>
								<tr>
									<th>상품명</th>
									<th>상품코드</th>
									<th>분류</th>
									<th>판매가</th>
									<th>수량</th>
								</tr>
							</thead>
							<tbody id="csproduct">
							
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	function choose(e) {
		var csm_id = $(e).attr('idx');
		var count = $('input[name=p_id' + p_id + ']').length;
		alert(count);
		if (count > 0) {
			alert("중복된 항목이 있습니다.");
		} else {
			$.ajax({
				url : 'install/productTR/' + csm_id,
				success : function(data) {
					$('#csProduct').append(data);
					$('#modal_large').modal('hide');
				}
			});
		}
	}
</script>