<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="modal_large3" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span
							id="modal_large_title">점검목록리스트 </span><span class="text-light"></span>
						<span
							class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right"
							data-dismiss="modal"><i class="icon-cross2"
							style="cursor: pointer" onclick="modal_close()"></i></span>
					</h4>
				</div>
				<div class="modal-body">
					<div class="panel" id="modal_large_content">

						<div class="col-xs-12">
							<div
								class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left"></i>1. 정산내용
									</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-6 col-sm-4 col-md-4">
													<div class="form-group form-group-default">
														<label>정산조정액</label> <input type="text" id="pr_rental" name="pr_rental" value="0" onkeyup="numCommaKeyUp()"
															class="form-control" onkeydown="numKeyDown()" style = "text-align:right;" >
													</div>
												</div>

											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="form-group form-group-default ">
													<label>정산조정사유</label> <input type="text"
														class="form-control input-sm" name="p_location"
														id="p_location">
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a class="btn btn-sm blue-hoki" onclick="modal_close()"> 닫기 </a>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	
</script>
