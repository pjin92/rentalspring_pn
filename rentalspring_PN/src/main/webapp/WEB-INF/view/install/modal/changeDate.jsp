<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<div class="modal" id="myModal2" aria-hidden="true"
	style="display: none; z-index: 1070;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h4 class="modal-title">방문예정일 변경</h4>
			</div>

			<div class="form-group form-group-default">
				<div class="input-icon right">
					<label for="form_control_1">요청일</label> <input type="date"
						class="datepicker" name="date" value=""> <input
						type="time" name="time" value=""> <input type="hidden"
						id="clidx" name="clidx">
				</div>
			</div>

			<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="btn">닫기</a> <a
					onclick="update_date()" class="btn btn-primary">변경</a>
			</div>
		</div>
	</div>
</div>

