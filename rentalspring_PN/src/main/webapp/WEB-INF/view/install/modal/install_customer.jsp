
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<div class="modal" id="myModal4" aria-hidden="true"
	style="display: none; z-index: 1070;" >
	<div class="modal-dialog" >
		<div class="modal-content alpha-slate">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h4 class="modal-title">고객정보</h4>
			</div>

			<div class="panel-body">
				<div class="form-group-attached">
					<div class="row">
						
						<article class="panel panel-flat margintop">
							<form id="art3_1">
							</form>
						</article>
					</div>
				</div>
			</div>

			<div class="modal-footer">
			<input type="hidden" id="cl_idx" name="cl_idx" value="">
				<a href="#" data-dismiss="modal" class="btn">닫기</a> <a
					onclick="update_memo()" class="btn btn-primary">변경</a>
			</div>
		</div>
	</div>
</div>
