
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="modal" id="myModal3" aria-hidden="true"
	style="display: none; z-index: 1070;">
	<div class="modal-dialog">
		<div class="modal-content alpha-slate">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h4 class="modal-title">설치기사 변경</h4>
			</div>

			<div class="panel-body">
				<div class="form-group-attached">
					<div class="row">
						<div class="col-xs-12">
							<table class="table table-hover" id="productTable">
								<thead>
									<tr>
										<th>번호</th>
										<th>기사명</th>
										<th>활동지역</th>
										<th>재고보기</th>
									</tr>
								</thead>
								<tbody id="technician">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="btn">닫기</a> <a
					onclick="update_date()" class="btn btn-primary">변경</a>
			</div>
		</div>
	</div>
</div>
