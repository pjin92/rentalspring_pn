<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<jsp:useBean id="currTime" class="java.util.Date" />
<fmt:formatDate value="${currTime}" pattern="yyyy-MM-dd" />
<fmt:formatDate value="${currTime}" var="currTime" pattern="yyyy-MM-dd" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
			<div class="page-header page-header-inverse has-cover">
	<!--밝은 배경 page-header-default page-header-inverse-->
		<div class="page-header-content page-header-sm">
	<!--bg-slate-400 page-header-lg / -xs -->
				<div class="page-title">
					<h4>
						<i class="icon-address-book2 position-left"></i> <span
							class="text-bold">기사 정산</span> <small class="display-block">정산</small>
					</h4>
				</div>
			</div>
</div>
<!-- /page header -->

<!-- Content area -->
<form role="form" name="frm" method="post" >
	<div class="content">
		<div class="row">
			<c:set var="cs" value="${cslist}"></c:set>
	<input type="hidden" name="csm_idx" value="${cs.csm_idx}">
	<div class="col-xs-12">
		<div
			class="panel panel-flat border-top-xlg border-top-primary-300">
			<div class="panel-heading">
				<h5 class="panel-title">
					<i class="icon-folder-search position-left"></i>1. 정산내용
				</h5>
			</div>
			<div class="panel-body">
				<div class="form-group-attached">
					<div class="row">
						<div class="col-xs-6 col-sm-4 col-md-2">
							<div class="form-group form-group-default ">
								<label>정산명</label> <input type="text"
									class="form-control input-sm" name="jr_name" id="jr_name">
							</div>
						</div>

						<div class="col-xs-6 col-sm-4 col-md-2">
							<div class="form-group form-group-default ">
								<label>정산코드</label> <input type="text"
									class="form-control input-sm" name="jr_code" id="jr_code">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<div class="panel panel-flat border-top-xlg border-top-primary-300">
			<div class="panel-heading">
				<h5 class="panel-title">
					<i class="icon-folder-search position-left"></i>기사 내용
				</h5>
			</div>
			<div class="panel-body">
				<div class="form-group-attached">
					<div class="row">
						<div class="col-xs-12">
							<table class="table table-hover nowrap" id="productTable">
								<thead>
									<tr>
										<th>기사명</th>
										<th>설치구분</th>
										<th>설치상품</th>
										<th>설치비용</th>
										<th>추가공임비용</th>
										<th>정산조정액</th>
										<th>정산합계</th>
										<th>정산조정사유</th>
									</tr>
								</thead>
					<tbody>
						<c:forEach var="list" items="${list}">
							<tr>
								<td>${list.e_name}
								<input type="hidden" name="m_idx${list.m_idx}" value="${list.m_idx }">
								<input type="hidden" name="jr_e_id${list.m_idx}" value="${list.e_id }">
								</td>
								<td>설치</td>
								<td>${list.pr_name}</td>
								<td>
								<input type="text" class="form-control input-sm" name="install${list.m_idx }" value="${list.install}" readonly />
								</td>
								<td><input type="text" class="form-control input-sm" name="extra${list.m_idx}" value="${list.extra }" readonly /></td>
								<td><input type="text"
									class="form-control input-sm" name="jr_adjust${list.m_idx}" 
									id="jr_adjust${list.m_idx}" placeholder="+ or -를 붙힌 후 금액입력" onkeyup="adjustment('${list.m_idx}')">
									
									</td>
								<td><input type="text"
									class="form-control input-sm" id="jr_total${list.m_idx}" name="jr_total${list.m_idx}"
									id="jr_total" value="${list.jr_total}">
									<input type="hidden" id="temp${list.m_idx}" name="temp${list.m_idx}" value="0">
									</td>
								<td><input type="text"
									class="form-control input-sm" name="jr_reason${list.m_idx}"
									id="jr_reason"></td>
							</tr>
						</c:forEach>
					</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<div
			class="panel panel-flat border-top-xlg border-top-primary-300">
			<div class="panel-heading">
				<h5 class="panel-title">
					<i class="icon-folder-search position-left"></i>6. 정산등록
				</h5>
			</div>
			<div class="panel-body">
				<div class="form-group-attached">
					<div class="row">
						<c:set var="pr" value="${writer}"></c:set>
						<div class="col-xs-6 col-sm-3 col-md-2">
							<div class="form-group form-group-default ">
								<label>최종정산자</label> <input type="text"
									name="e_name" value="${e_name}" readonly
									class="form-control">
							</div>
						</div>

						<div class="col-xs-6 col-sm-3 col-md-2">
							<div class="form-group form-group-default ">
								<label>최종정산일</label> <input type="text" name="jr_log"
									value="${currTime}" class="form-control" readonly />
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group form-group-default ">
								<label>정산메모</label> 
								<input type="text" class="form-control input-sm" name="memo">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<a class="heading-elements-toggle"><i class="icon-more"></i></a>
				<div class="heading-elements">
					<button type="button" class="btn btn-sm btn-default ml-20"
						onclick="window.Close()">닫기</button>
					<div class="heading-btn pull-right">
						<a class="btn btn-sm btn-danger" onclick="startAdjust()">정산</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /content area -->
		</div>
	</div>
</div>
<%@include file="/WEB-INF/view/install/modal/adjustment.jsp"%>
<%@include file="/WEB-INF/view/main/js.jsp"%>
<script>
var table = $('#productTable').DataTable();

$.extend($.fn.dataTable.defaults, {
	"columns" : [ {
		"data" : "p_name"
	}, {
		"data" : "p_code"
	}, {
		"data" : "p_category1"
	}, {
		"data" : "p_cost"
	}, {
		"data" : "p_price"
	} ]
});

function startAdjust(){
	var param=$('form[name=frm]').serialize();
	$.ajax({
		url:'/install/adjust',
		type:'post',
		data:param
	})
	.done(function(data){
		if(data.trim()=='등록완료'){
			window.close();
			opener.parent.location.reload();
		}else{
			window.close();
			opener.parent.location.reload();
		}
	});
}


function adjustment(m_idx){
	var idx = m_idx;
	var extra = $('input[name=extra'+idx+']').val();
	var install = $('input[name=install'+idx+']').val();
	extra = extra.replace(',','');
	install = install.replace(',','');
	var sum = Number(extra) + Number(install);
	var total = $('input[name=jr_total'+idx+']').val();
	total = total.replace(',','');
	
	var adjust = $('input[name=jr_adjust'+idx+']').val();
	var temp =$('input[name=temp'+idx+']').val();
	if(adjust.charAt(0)=='+'){
	sum = Number(sum) - Number(temp);
	sum = Number(sum) + Number(adjust.replace('+',''));
	$('#jr_total'+idx).val(comma(Number(sum)));
	$('#temp'+idx).val(temp);
		
	}else if(adjust.charAt(0)=='-'){
		sum = Number(sum) + Number(temp);
		sum = Number(sum) - Number(adjust.replace('+',''));
		$('#jr_total'+idx).val(comma(Number(sum)));
		$('#temp'+idx).val(temp);
	}else{
		alert('+ 또는 -를 입력하여 주시길 바랍니다.');
		$('input[name=jr_adjust'+idx+']').val('');
		return false;
	}
}

</script>
</body>
</html>