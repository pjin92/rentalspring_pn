<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="cslist" value="${clist}"></c:set>
<c:forEach var="cl" items="${cslist}" varStatus="status">
<c:choose>
<c:when test="${cl.cl_gubun eq '0' }">
<div class="ui-block-a list-box-wrap">
<div class="list-box">
	<h4 class="list-tit"><i class="icon-arrow-right5"></i>&nbsp;${status.count}회차 정기 방문 예정일</h4>
	<ul class="list-wrap">
		<li class="clearfix">
			<p class="list-tit display-inline-block">예정일 : <a href="#schedule"  data-rel="popup" data-position-to="window" data-transition="pop">${cl.cl_date}</a></p>
			<a href="#dateChange" data-rel="popup" data-position-to="window" cl_idx= "${cl.cl_idx}" onclick="dateChange()" data-transition="pop" class="btn btn-sm bg-pink float-right" style="margin-top:-5px;">예정일 변경</a>
		</li>
		<li class="">
		<p class="list-tit">CS명 : <strong class="list-cont"><a onclick="cslist()"> ${cl.csm_name}</a></strong></p>
			<input type="hidden" id="m_idx" name="m_idx" value="${cl.m_idx}">
		</li>
	</ul>
	<div class="padding-5px-top">
		
		<c:choose>	
			<c:when test="${cl.state eq '설치전'}">
			<a cl_idx ="${cl.cl_idx}" m_idx="${m_idx}" state="${cl.state}" onclick="addCsInstall()" class="btn width-100 bg-danger">점검 대기</a>
			</c:when>
			<c:when test="${cl.state eq '설치요청'}">
			<a cl_idx = "${cl.cl_idx}" m_idx="${m_idx}" csds_idx="${cl.csds_idx}" state="${cl.state}" onclick="addCsInstall()" class="btn width-100 bg-info">점검 요청</a>
			</c:when>
			<c:when test="${cl.state eq '설치완료'}">
			<a javascript:void(0) class="btn width-100 bg-complete">점검 완료</a>
			</c:when>

		</c:choose>
		<!-- 점검대기, 점검완료 클래스 구분 -->
		</div>
	</div>
</div>

</c:when>


<c:when test="${cl.cl_gubun eq '1' }">
<div class="ui-block-a list-box-wrap">
<div class="list-box">
	<h4 class="list-tit"><i class="icon-arrow-right5"></i>&nbsp;${status.count}회차 긴급점검 방문 예정일</h4>
	<ul class="list-wrap">
		<li class="clearfix">
			<p class="list-tit display-inline-block">예정일 : <a href="#schedule"  data-rel="popup" data-position-to="window" data-transition="pop">${cl.cl_date}</a></p>
			<a href="#dateChange" data-rel="popup" data-position-to="window" cl_idx= "${cl.cl_idx}" onclick="dateChange()" data-transition="pop" class="btn btn-sm bg-pink float-right" style="margin-top:-5px;">예정일 변경</a>
		</li>
		<li class="">
			<p class="list-tit">CS명 : <strong class="list-cont"> ${cl.csm_name}</strong></p>
			<input type="hidden" id="m_idx" name="m_idx" value="${cl.m_idx}">
		</li>
	</ul>
	<div class="padding-5px-top">
		
		<c:choose>	
			<c:when test="${cl.state eq '설치전'}">
			<a cl_idx ="${cl.cl_idx}" m_idx="${m_idx}" state="${cl.state}" onclick="addCsInstall()" class="btn width-100 bg-danger">점검 대기</a>
			</c:when>
			<c:when test="${cl.state eq '설치요청'}">
			<a cl_idx = "${cl.cl_idx}" m_idx="${m_idx}" csds_idx="${cl.csds_idx}" state="${cl.state}" onclick="addCsInstall()" class="btn width-100 bg-info">점검 요청</a>
			</c:when>
			<c:when test="${cl.state eq '설치완료'}">
			<a javascript:void(0) class="btn width-100 bg-complete">점검 완료</a>
			</c:when>

		</c:choose>
		<!-- 점검대기, 점검완료 클래스 구분 -->
		</div>
	</div>
</div>

</c:when>
</c:choose>
</c:forEach>

		