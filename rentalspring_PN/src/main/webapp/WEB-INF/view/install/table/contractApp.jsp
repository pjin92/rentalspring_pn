<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<tr>
		<c:set var="mi" value="${mi_detail}"/>
		<th class="bg-light-gray">렌탈 상품명</th>
		<td> ${mi.pr_name}</td>
	</tr>
	<tr>
		<th class="bg-light-gray">고객번호</th>
		<td>${mi.m_num}</td>
	</tr>
	<tr>
		<th class="bg-light-gray">설치고객명</th>
		<td>${mi.mi_name}</td>
	</tr>
	<tr>
		<th class="bg-light-gray">연락처</th>
		<td>${mi.mi_tel}</td>
	</tr>
	<tr>
		<th class="bg-light-gray">주소</th>
		<td>${mi.mi_addr}</td>
	</tr>
	<tr>
		<th class="bg-light-gray">메모</th>
		<td>${mi.mi_memo}</td>
	</tr>
	
		<c:choose>
		<c:when test="${mi.m_jubsoo eq '무료체험' }">
			<tr>
				<td colspan="2" style="text-align:right;border-bottom: none;padding-bottom: 0px;">
					<button class="width-30 btn bg-info" onclick="testFree(${mi.m_idx})"><b>무료체험</b></button>
					<button class="width-30 btn bg-orange" onclick="waiting(${mi.m_idx})"><b>대기</b></button>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:right;padding-top: 5px;">
					
					<button class="width-30 btn bg-gray" onclick="cancelBuy('${mi.ds_idx}','${mi.m_idx}')"><b>방문 전 취소</b></button>
					<button class="width-30 btn bg-primary" onclick="cancel('${mi.m_jubsoo}','${mi.m_idx}')"><b>방문 후 취소</b></button>
				</td>
			</tr>
		</c:when>
		<c:when test="${mi.m_jubsoo ne '무료체험' }">
			<tr>
				<td colspan="3" style="text-align:right;border-bottom: none;padding-bottom: 0px;">
					<button class="width-30 btn bg-orange" onclick="waiting(${mi.m_idx})"><b>대기</b></button>
					<button class="width-30 btn bg-gray" onclick="cancelBuy('${mi.ds_idx}','${mi.m_idx}')"><b>방문 전 취소</b></button>
					<button class="width-30 btn bg-primary" onclick="cancel('${mi.m_jubsoo}','${mi.m_idx}')"><b>방문 후 취소</b></button>
				</td>
			</tr>
		</c:when>
		
		
		
		
		</c:choose>
	
	
	<input type="hidden" name="m_idx" value="${mi.m_idx}">
	<input type="hidden" name="mi_idx" value="${mi.mi_idx}">		
		