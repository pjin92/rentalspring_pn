<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:forEach var="csp" items="${data}">
	<tr>
		<td>${csp.p_name}</td>
		<td>${csp.p_code}</td>
		<td>${csp.p_category1}</td>
		<td>${csp.p_price }</td>
		<td>${csp.csp_num}</td>
	</tr>
</c:forEach>
