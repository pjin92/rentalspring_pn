<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:forEach var="plist" items="${data}">
<ul class="float-right-list width-100">
<!-- 상품리스트 반복 -->
		<li>
		
		<div class="clearfix">
				<p class="tit">CS명</p>
				<p class="cont">${plist.p_name}</p>
		</div>
			<div class="clearfix">
				<p class="tit">상품코드</p>
				<p class="cont">${plist.p_code}</p>
			</div>
			<div class="clearfix">
				<p class="tit">매입처</p>
				<p class="cont">${plist.p_category1}</p>
			</div>
			<div class="clearfix">
				<p class="tit">렌탈기간</p>
				<p class="cont">${plist.p_rent_term }</p>
			</div>
			<div class="clearfix">
				<p class="tit">월렌탈료</p>
				<p id="rentMoney" class="cont">${plist.p_rent_money}</p>
			</div>
		</li>
		<!--// 상품리스트 반복 -->
	</ul>
</c:forEach>
