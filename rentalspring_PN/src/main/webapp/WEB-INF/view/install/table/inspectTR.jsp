<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="cslist" value="${clist}"></c:set>
<c:forEach var="cl" items="${cslist}" varStatus="status">
<c:choose>
<c:when test="${cl.cl_gubun eq '0' }">
<div class="col-xs-2 col-sm-2 col-md-2 ">
	<div class="form-group form-group-default">
		<div class="input-icon right">
			<label for="form_control_1">${status.index}회차 정기방문예정일</label>  ${cl.cl_date}
		</div>
		<div class="input-icon right">
		<font style="font-weight:bold">CS명 :  </font>${cl.csm_name}
		</div>
	</div>
</div>
</c:when>
<c:when test="${cl.cl_gubun eq '1' }">
<div class="col-xs-2 col-sm-2 col-md-2 ">
	<div class="form-group form-group-default">
		<div class="input-icon right">
			<label for="form_control_1">긴급방문예정일</label>  ${cl.cl_date}
		</div>
		<div class="input-icon right">
		<font style="font-weight:bold">CS명 :  </font>${cl.csm_name}
		</div>
	</div>
</div>
</c:when>

</c:choose>

</c:forEach>
