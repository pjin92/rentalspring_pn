
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

				<c:set var="ic" value="${icm}"></c:set>
				<div class="panel-heading">
					<h5 class="panel-title"><i class="icon-folder-search position-left"></i>1.설치 고객 정보</h5>
				</div>
				<div class="panel-body">
					<div class="col-xs-4 col-sm-4 col-md-4 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">배송 고객명</label>
								<input type="text" class="form-control input-sm text-right" name="mi_name" value="${ic.mi_name}" readonly="readonly"/>
							</div>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">연락처1</label>
								<input type="text" class="form-control input-sm text-right" name="mi_phone" value="${ic.mi_phone}"readonly="readonly"/>
							</div>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4  ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">연락처2</label>
								<input type="text" class="form-control input-sm text-right" name="mi_tel" value="${ic.mi_tel}"readonly="readonly"/>
							</div>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4  ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">배송 주소1</label>
								<input type="text" class="form-control input-sm text-right" name="mi_addr1" value="${ic.mi_addr1}" readonly="readonly">
							</div>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4  ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">배송 주소2</label>
								<input type="text" class="form-control input-sm text-right" name="mi_addr2" value="${ic.mi_addr2}" readonly="readonly">
							</div>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">우편번호</label>
								<input type="text" class="form-control input-sm text-right" name="mi_post" value="${ic.mi_post}" readonly="readonly">
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">상세주소</label>
								<input type="text" class="form-control input-sm text-right" name="mi_addr3" value="${ic.mi_addr3}" readonly="readonly"/>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">설치 메모</label>
								<input type="text" class="form-control input-sm" style="resize:vertical" name="cl_memo" value="${ic.cl_memo}">
							</div>
						</div>
					</div>
				</div>
