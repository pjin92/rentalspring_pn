<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="plist" value="${plist}"></c:set>
<ul class="float-right-list width-100">
	<li>
		<p>${plist.p_name}<input type="hidden" name="p_id${plist.p_id}"
		value="${plist.p_id}"></p>
		<div class="clearfix">
			<p class="tit">상품코드</p>
			<p class="cont">${plist.p_code}</p>
		</div>
		<div class="clearfix">
			<p class="tit">매입처</p>
			<p class="cont">${plist.p_category1}</p>
		</div>
		<div class="clearfix">
			<p class="tit">렌탈료</p>
			<p class="cont" id="rentMoney">${plist.p_rent_money}</p>
		</div>
		<div class="clearfix">
			<p class="tit">수량</p>
			<p class="cont"><input type="text" name="prp_num${plist.p_id}" value="" class="form-control" style="width:40px"></p>
		</div>
	</li>
</ul>