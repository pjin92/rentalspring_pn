<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="cslist" value="${clist}"></c:set>
<c:forEach var="cl" items="${cslist}">

	<tr>
		<td>${cl.if_log }</td>
		<td>${cl.m_install_finish_date }</td>
		<td>${cl.cl_date }</td>
		<td>${cl.pr_name}</td>
		<td>${cl.csm_name }</td>

		<td>${cl.mi_name }</td>
		<td>${cl.m_phone }</td>
		<td>${cl.mi_addr }</td>
		<td>${cl.mi_post }</td>
		<td>${cl.m_tech }</td>
		
		<td>${cl.state }</td>
		
	</tr>
</c:forEach>
