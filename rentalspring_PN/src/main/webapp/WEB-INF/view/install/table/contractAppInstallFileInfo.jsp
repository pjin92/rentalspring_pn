<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;2. 설치 확인서 정보</h4>
<c:choose>
	<c:when test="${empty ifMap}">
	정보 없음
	</c:when>
	<c:otherwise>
		<table class="table text-left width-100">
			<tr>
				<th class="bg-light-gray">설치확인서</th>
				<td><a href='/file/member/${ifMap.mf_idx}_${ifMap.mf_memo}.jpg'  data-ajax="false" >${ifMap.mf_file}</a></td>
			</tr>
			<tr>
				<th class="bg-light-gray">설치 완료일</th>
				<td>${ds_date}</td>
			</tr>
			<tr>
				<th class="bg-light-gray">기사 메모</th>
				<td>${ifMap.if_add_memo}</td>
			</tr>
			<tr>
				<th class="bg-light-gray">추가 공임 비용</th>
				<td>${ifMap.if_extra_f}</td>
			</tr>
			<tr>
				<th class="bg-light-gray">추가 공임 사유</th>
				<td>${ifMap.if_extra_memo}</td>
			</tr>
		</table>
	</c:otherwise>
</c:choose>
