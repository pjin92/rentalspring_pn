<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>계약고객관리>고객 정보</title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
<style type="text/css">
.margintop{
	margin-top: 20px;
}
</style>
</head>
<body>
<div class="page-container" id="topDiv">
	<div class="page-content">
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
					<div class="page-title">
						<h4>
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">계약고객</span>
							<small class="display-block">정보</small>
						</h4>
					</div>
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			
			<article class="panel panel-flat margintop">
			<form id="art2">
				<div class="panel-heading" style="cursor: pointer;" onclick="$('#art2Panel').toggle('show')">
					<h5 class="panel-title"><i class="icon-folder-search position-left"></i>2.해피콜 정보</h5>
				</div>

				<div class="panel-body" id="art2Panel">
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>상담 상태</label>
							${m.m_state}
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>접수구분</label>
							${m.m_jubsoo}
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>매출구분</label>
							${m.m_detail}
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>진행상태</label>
							${m.m_process}
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>배송/설치 상태</label>
							
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>무료체험 일수</label>
							${m.m_free_day}
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>1회차 등록</label>
							
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>1회차 출금 여부</label>
							
						</div>
					</div>
					<div class="col-xs-4  margin-top-34">
						<div class="form-group form-group-default">
							<label>무료체험 종료일</label>
							
						</div>
					</div>
				</div>
			</form>
			</article>
			
			<article class="panel panel-flat margintop">
			<form id="art3_1">
				<div class="panel-heading">
					<h5 class="panel-title"><i class="icon-folder-search position-left"></i>3_1.배송 고객 정보</h5>
				</div>

				<div class="panel-body">
					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-offset-0 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">배송 고객명</label>
								<input type="text" class="form-control input-sm text-right" name="mi_name" value="${mi_delivery.mi_name}">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-offset-0 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">연락처1</label>
								<input type="text" class="form-control input-sm text-right" name="mi_phone" value="${mi_delivery.mi_phone}">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-offset-0 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">연락처2</label>
								<input type="text" class="form-control input-sm text-right" name="mi_tel" value="${mi_delivery.mi_tel}">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-offset-0 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">배송 주소1</label>
								<input type="text" class="form-control input-sm text-right" name="mi_addr1" value="${mi_delivery.mi_addr1}" readonly="readonly">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-offset-0 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">배송 주소2</label>
								<input type="text" class="form-control input-sm text-right" name="mi_addr2" value="${mi_delivery.mi_addr2}" readonly="readonly">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-offset-0 ">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">우편번호</label>
								<input type="text" class="form-control input-sm text-right" name="mi_post" value="${mi_delivery.mi_post}" readonly="readonly">
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">상세주소<a onclick="openAddress('art3_1')">검색</a></label>
								<input type="text" class="form-control input-sm text-right" name="mi_addr3" value="${mi_delivery.mi_addr3}">
							</div>
						</div>
					</div>
					
					<div class="col-xs-12">
						<div class="form-group form-group-default">
							<div class="input-icon right">
								<label for="form_control_1">배송 메모</label>
								<input type="text" class="form-control input-sm" style="resize:vertical" name="mi_memo" value="${mi_delivery.mi_memo}">
							</div>
						</div>
					</div>
					<a onclick="saveDeliveryInfo()" class="btn bg-teal-400 pull-right" style="width:100px;">저장</a>
				</div>
			</form>
			</article>
		</div>
	</div>
</div>
<%@include file="/WEB-INF/view/main/modal.jsp"%>

<%@include file="/WEB-INF/view/main/js.jsp"%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
//1. 신청고객 정보. 다음 주소
function openAddress(formId){
	new daum.Postcode({
	    oncomplete: function(data) {
	      	var sido=data.sido;
	    	var sigungu=data.sigungu;
	    	var roadname=data.address.replace(sido,'');
	    	roadname=roadname.trim();
	    	roadname=roadname.replace(sigungu,'');
	    	roadname=roadname.trim();
	    	var zonecode=data.zonecode;
	    	$('#'+formId+' input[name=mi_addr1]').val(sido);
	    	$('#'+formId+' input[name=mi_addr2]').val(sigungu);
	    	$('#'+formId+' input[name=mi_addr3]').val(roadname);
	    	$('#'+formId+' input[name=mi_post]').val(zonecode);
	    }
	}).open({popupName:'daumAddr'});
}

//3_1 배송 고객 정보 저장
function saveDeliveryInfo(){
	blockById('art3_1');
	var param=$('#art3_1').serialize();
	var url=window.location.pathname;
	url=url.replace('/contract','');
	$.ajax({
		url:url+'4?'+param,//상담고객에서 쓰는 배송정보 저장 url
		type:'put'
	}).done(function(res){
		alert(res);
	}).always(function(){
		$('#art3_1').unblock();
	});
}
//고객정보 리스트 불러오기
function getMemberInfo(){
	$('#modal_large_title').html('고객정보 불러오기');
	var url=window.location.pathname;
	url=url.replace('/contract','');
	$('#modal_large_content').load(url+'milist');
	$('#modal_large').modal('show');
}
//3_1 배송고객 주소에 불러온 고객 주소 넣기
function memberInfoDetail(){
	var miidx=$(event.target).attr('mi');
	$.ajax('/member/info/'+miidx)
	.done(function(res){
		$('#art3_1 input[name=mi_name]').val(res.mi_name);
		$('#art3_1 input[name=mi_phone]').val(res.mi_phone);
		$('#art3_1 input[name=mi_tel]').val(res.mi_tel);
		$('#art3_1 input[name=mi_addr1]').val(res.mi_addr1);
		$('#art3_1 input[name=mi_addr2]').val(res.mi_addr2);
		$('#art3_1 input[name=mi_addr3]').val(res.mi_addr3);
		$('#art3_1 input[name=mi_post]').val(res.mi_post);
		
		$('#modal_large').modal('hide');
	});
}
////
    
//결제종료일 자동계산//
setPayEnd();
function setPayEnd(){
	var m_paystart=$('#art5 input[name=m_paystart]').val();
	var m_period=$('#art5 input[name=m_period]').val();
	try{
		var payend=new Date(m_paystart);
		payend.setMonth(payend.getMonth()+Number(m_period)-1);
		$('#art5 input[name=m_payend]').val( (payend.toISOString()).substring(0,10) );
	}catch(err){
		
	}
}

//결제방식에 따른 입력창 view
if('${m.m_pay_method}'=='계좌이체'){
	$('.cmsbank').show();
	$('.cmscard').hide();
}else{
	$('.cmsbank').hide();
	$('.cmscard').show();
}

//구매취소
function deliveryState(req){
	blockById('art3_2');
	var param=$('#art3_2').serialize();
	var ds_idx=$(event.target).attr('ds');
	
	var url=window.location.pathname;
	
	$.ajax({
		url:'/delivery/'+ds_idx+'?ds_state='+req+'&'+param+'&url='+url,
		type:'put'
	}).always(function(result){
		alert(result);
		refreshDs();
	});
}
//배송정보 새로고침
function refreshDs(){
	var url=window.location.pathname;
	url=url.replace('/contract','');
	
	$('#art3_2').unblock();
	$.ajax(url+'delivery_state')
	.done(function(res){
		$('#artDs').html(res);
		
		$('.dsdate').daterangepicker({
	    	autoUpdateInput: false,
	    	startDate: moment(),
	        singleDatePicker: true
	    });
		
		$('.dsdate').on('apply.daterangepicker', function(ev, picker) {
		    $(this).val(picker.startDate.format('YYYY-MM-DD'));
		});
	});
} 

//8.파일 업로드 type= rental,notice
function fileUpload(type){
	
	var mf=$('#mf_'+type).attr('mf');
	if(mf!=undefined){
		location.href=mf;
		return;
	}
	
	if(type=='rental'){
		$('#modal_large_title').html('렌탈 약정서 업로드');
	}else{
		$('#modal_large_title').html('양도 통지서 업로드');
	}
	var input='<form id="form8" method="post" enctype="multipart/form-data" ><input name="file" type="file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>';
	input+='<a onclick="uploadArt(\''+type+'\')" class="btn bg-teal-400 pull-right">파일 등록</a></form><br><br><br>';
	$('#modal_large_content').html(input);
	$('#modal_large').modal('show');
}
//8. 파일업로드
function uploadArt(type){
	blockById('art8');
	$('#form8').ajaxForm({
		url:'./8?type='+type,
		enctype:"multipart/form-data",
		success:function(res){
			$('#modal_large').modal('hide');
			alert(res.msg);
			if(res.result=='success'){
				$('#mf_'+type).attr('mf','/file/member/'+res.mf);
				$('#mf_'+type).html(res.mf_file);
			}
		},
		complete:function(){
			$('#art8').unblock();
		}
	});
	 $("#form8").submit() ;
}

//최종계약확정
function finalContract(){
	var url=window.location.pathname;
	url=url.replace('/contract','');
	url+='final';
	blockById('topDiv');
	
	$.ajax(url)
	.done(function(res){
		alert(res.msg);
		if(res.msg.trim()=='최종계약확정했습니다.'){
			$('#finalState').html('확정');
			$('#finalDate').html(res.date);
		}
	}).always(function(){
		$('#topDiv').unblock();
	});
	
}
//중도상환
function joongdo(){
	
}
</script>
</body>
</html>