<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
	<!-- 리스트 section -->
	<div class="ui-grid-solo">
		<!-- 리스트 반복 -->
		<c:forEach var="in" items="${data}">
		<div class="ui-block-a list-box-wrap">
			<div class="clearfix">
				<div class="list-center-box">	
					<p class="bg-danger"><b>${in.ds_state}</b></p>
					<p class="bg-complete"><b>${in.m_jubsoo}</b></p>
					
					<c:if test="${in.m_state eq '대기' }">
					<p class="bg-warning"><b>${in.m_state}</b></p>
					</c:if>
<%-- 					<p class="bg-primary" onclick="cancel('${in.m_jubsoo}','${in.m_idx}')"><b>취소</b></p> --%>
<%-- 					<p class="bg-gray" onclick="cancelBuy('${in.ds_idx}','${in.m_idx}')"><b>구매취소</b></p> --%>
					<c:choose>
						<c:when test="${in.newStatus eq '0'}">
						<p class="bg-orange"><b>NEW</b></p>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
					<%-- <c:choose>
						<c:when test="${in.if_log eq ''}">
						<p class="bg-gray"><a href="#changeDate" onclick="changeDate('${in.m_idx_t}')"  data-rel="popup" data-position-to="window" data-transition="pop">${in.m_eta}</a></p>
						</c:when>
						<c:otherwise>
						<p class="bg-gray"><b>${in.if_log}</b></p>
						</c:otherwise>
					</c:choose> --%>
				</div>
				<div class="list-center-box">
					<ul class="list-wrap">
						<li class="">
							<p class="list-tit">상품명 : <strong class="list-cont">${in.pr_name}</strong></p>
						</li>
						<li class="">
							<p class="list-tit">설치고객명 : ${in.mi_name} </p>
						</li>
						<li class="">
							<p class="list-tit">주소 : <strong class="list-cont">${in.mi_addr}</strong></p>
						</li>
						<li class="">
							<p class="list-tit">연락처 : <strong class="list-cont"><a href="tel:${in.mi_phone}">${in.mi_phone}</a></strong></p>
						</li>
						<c:choose>
							<c:when test="${in.if_log eq ''}">
							<li class="clearfix">
								<p class="list-tit display-inline-block">예정일 : <strong class="list-cont">${in.ds_req}</strong>
									<a href="#changeDate" onclick="changeDate('${in.m_idx_t}')" data-rel="popup" data-position-to="window" data-transition="pop" class="btn btn-sm bg-gray margin-5px-left">변경</a>
								</p>
							</li>
							</c:when>
							<c:otherwise>
							<li class="clearfix">
								<p class="list-tit">완료일 : <strong class="list-cont">${in.if_log}</strong></p>
							</li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
			</div>
		</div>
		</c:forEach>
		<!--// 리스트 반복 -->												
	</div>	
	<!--// 리스트 section -->
</div>
