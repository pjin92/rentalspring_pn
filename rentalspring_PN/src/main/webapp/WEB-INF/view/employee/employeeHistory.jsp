<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>임직원 변경이력</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
		<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<c:set var="hData" value="${hData}"/>
						<c:set var="nData" value="${nData}"/>
						<c:if test="${empty oData}">
						<c:set var="oData" value="${hData}"/>
						</c:if>
						<c:if test="${not empty oData}">
						<c:set var="oData" value="${oData}"/>
						</c:if>
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">임직원 ${hData.e_name}  변경이력</span> <small class="display-block"> 변경이력</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" id="frm" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 임직원 개인정보
										</h5>
									</div>
									
									<input type="hidden" name="e_id" value="${hData.e_id }">
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_name eq nData.e_name and nData.e_name eq oData.e_name}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_name eq hData.e_name and oData.e_name ne hData.e_name}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_name ne nData.e_name and oData.e_name eq hData.e_name}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>직원성명</label> 
														<input type="text" name="e_name" value="${hData.e_name }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_tell eq nData.e_tell and nData.e_tell eq oData.e_tell}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_tell eq hData.e_tell and oData.e_tell ne hData.e_tell}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_tell ne nData.e_tell and oData.e_tell eq hData.e_tell}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>자택전화</label> 
														<input type="text" name="e_tell" value="${hData.e_tell }" class="form-control"  onkeyup="autoHypenPhone('e_tell')" readonly>
													</div>
												</div>
												
														
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_phone eq nData.e_phone and nData.e_phone eq oData.e_phone}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_phone eq hData.e_phone and oData.e_phone ne hData.e_phone}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_phone ne nData.e_phone and oData.e_phone eq hData.e_phone}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>핸드폰번호</label> 
														<input type="text" name="e_phone" value="${hData.e_phone }" class="form-control"  onkeyup="autoHypenPhone('e_phone')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>주민등록번호</label> 
														<input type="text" name="e_jumin" value="${hData.e_jumin }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_addr1 eq nData.e_addr1 and nData.e_addr1 eq oData.e_addr1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_addr1 eq hData.e_addr1 and oData.e_addr1 ne hData.e_addr1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_addr1 ne nData.e_addr1 and oData.e_addr1 eq hData.e_addr1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">주소1</label>
															<input type="text" class="form-control input-sm" name="e_addr1" value="${hData.e_addr1}"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_addr2 eq nData.e_addr2 and nData.e_addr2 eq oData.e_addr2}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_addr2 eq hData.e_addr2 and oData.e_addr2 ne hData.e_addr2}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_addr2 ne nData.e_addr2 and oData.e_addr2 eq hData.e_addr2}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">주소2</label>
															<input type="text" class="form-control input-sm" name="e_addr2" value="${hData.e_addr2}" onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_addr3 eq nData.e_addr3 and nData.e_addr3 eq oData.e_addr3}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_addr3 eq hData.e_addr3 and oData.e_addr3 ne hData.e_addr3}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_addr3 ne nData.e_addr3 and oData.e_addr3 eq hData.e_addr3}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">주소3</label>
															<input type="text" class="form-control input-sm" name="e_addr3" value="${hData.e_addr3}" onclick="openAddressIfEmpty()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_zip eq nData.e_zip and nData.e_zip eq oData.e_zip}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_zip eq hData.e_zip and oData.e_zip ne hData.e_zip}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_zip ne nData.e_zip and oData.e_zip eq hData.e_zip}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="e_zip" value="${hData.e_zip }" readonly>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 임직원 사원정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직원번호</label> 
														<input type="text" name="e_number" value="${hData.e_number }" class="form-control" onkeyup="checkId()" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_branch eq nData.e_branch and nData.e_branch eq oData.e_branch}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_branch eq hData.e_branch and oData.e_branch ne hData.e_branch}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_branch ne nData.e_branch and oData.e_branch eq hData.e_branch}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<label>소속</label> 
														<select	class="select select2-hidden-accessible" name="e_branch" disabled>
																<c:forEach var="branch" items="${branch}">
																	<c:if test="${branch.c_id eq hData.e_branch }">
																	<option value="${branch.c_id}" selected>${branch.comname}</option>
																	</c:if>
																	<c:if test="${branch.c_id ne hData.e_branch }">
																	<option value="${branch.c_id}">${branch.comname}</option>
																	</c:if>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_department eq nData.e_department and nData.e_department eq oData.e_department}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_department eq hData.e_department and oData.e_department ne hData.e_department}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_department ne nData.e_department and oData.e_department eq hData.e_department}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<label>부서</label> 
														<select	class="select select2-hidden-accessible" name="e_department" disabled >
																<c:forEach var="department" items="${department}">
																	<c:if test="${department.ed_idx eq hData.e_department }">
																		<option value="${department.ed_idx}" selected>${department.ed_name}</option>
																	</c:if>	
																	<c:if test="${department.ed_idx ne hData.e_department }">
																		<option value="${department.ed_idx}">${department.ed_name}</option>
																	</c:if>	
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_position eq nData.e_position and nData.e_position eq oData.e_position}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_position eq hData.e_position and oData.e_position ne hData.e_position}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_position ne nData.e_position and oData.e_position eq hData.e_position}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<label>직급</label> 
														<select	class="select select2-hidden-accessible" name="e_position" disabled>
																<c:forEach var="position" items="${position}">
																	<c:if test="${position.ep_idx eq hData.e_position }">
																		<option value="${position.ep_idx}" selected>${position.ep_name}</option>
																	</c:if>
																	<c:if test="${position.ep_idx ne hData.e_position }">
																		<option value="${position.ep_idx}">${position.ep_name}</option>
																	</c:if>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_job eq nData.e_job and nData.e_job eq oData.e_job}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_job eq hData.e_job and oData.e_job ne hData.e_job}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_job ne nData.e_job and oData.e_job eq hData.e_job}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<label>직무</label> 
														<select	class="select select2-hidden-accessible" name="e_job" disabled>
																<c:forEach var="job" items="${job}">
																	<c:if test="${job.ej_idx eq hData.e_job }">
																		<option value="${job.ej_idx}"selected>${job.ej_name}</option>
																	</c:if>	
																	<c:if test="${job.ej_idx ne hData.e_job }">
																		<option value="${job.ej_idx}">${job.ej_name}</option>
																	</c:if>	
																		
																</c:forEach>
														 </select>
													</div>
												</div>  
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_email eq nData.e_email and nData.e_email eq oData.e_email}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_email eq hData.e_email and oData.e_email ne hData.e_email}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_email ne nData.e_email and oData.e_email eq hData.e_email}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<label>이메일</label> 
														<input type="text" name="e_email" value="${hData.e_email }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_offtel eq nData.e_offtel and nData.e_offtel eq oData.e_offtel}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_offtel eq hData.e_offtel and oData.e_offtel ne hData.e_offtel}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_offtel ne nData.e_offtel and oData.e_offtel eq hData.e_offtel}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<label>전화번호<small>(사무실번호)</small></label>  
														<input type="tel" name="e_offtel" value="${hData.e_offtel }" class="form-control" onkeyup="autoHypenPhone('e_offtel')" readonly>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 임직원 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
											
											<div class="col-xs-6 col-sm-4 col-md-3">
													<c:choose>
													<c:when test="${hData.e_status eq nData.e_status and nData.e_status eq oData.e_status}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.e_status eq hData.e_status and oData.e_status ne hData.e_status}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.e_status ne nData.e_status and oData.e_status eq hData.e_status}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
													</c:choose>
														<label>상태</label> 
														<select	class="select select2-hidden-accessible" name="e_status" disabled>
																<option value="1" selected>정상</option>
																<option value="2">퇴사</option>
																<option value="3">사용중지</option>
														 </select>
													</div>
												</div> 
												
										
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>로그인ID</label> 
														<input type="text" name="e_logid" value="${hData.e_logid }" class="form-control" readonly >
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>권한</label> 
														<select	class="select select2-hidden-accessible" name="e_power" >
																<c:forEach var="power" items="${power}">
																	<c:if test="${power.epo_idx eq hData.e_power }">
																		<option value="${power.epo_idx}"selected>${power.epo_name}</option>
																	</c:if>
																	<c:if test="${power.epo_idx ne hData.e_power }">
																		<option value="${power.epo_idx}">${power.epo_name}</option>
																	</c:if>
																</c:forEach>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
</body>
</html>