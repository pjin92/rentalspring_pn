<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">기사 상세 &amp; 수정</span> <small class="display-block">상세 &amp; 수정</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 설치기사 등록정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
											<c:set var="t" value="${tlist}"></c:set>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>기사 코드</label> 
														<input type="text" name="t_number" value="${t.t_number}"class="form-control" readonly> 
														<input type="hidden" name="t_idx" value="${t.t_idx}">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>상태</label> 
														<select	class="select select2-hidden-accessible" name="t_status" disabled>
															 <c:choose>
															 <c:when test="${t.t_status eq 1 }">
															 <option value ="1" selected>정상 </option>
															 <option value ="2">퇴사</option>
															 <option value ="3">사용중지 </option>
															 </c:when>
															 
															 <c:when test="${t.t_status eq 2 }">
															 <option value ="1">정상 </option>
															 <option value ="2" selected>퇴사</option>
															 <option value ="3">사용중지 </option>
															 </c:when>
															 
															 <c:when test="${t.t_status eq 3 }">
															 <option value ="1">정상 </option>
															 <option value ="2">퇴사</option>
															 <option value ="3" selected>사용중지 </option>
															 </c:when>
															 </c:choose> 
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 설치기사 개인정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>기사성명</label> 
														<input type="text" name="t_name" value="${t.t_name }" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>주민번호</label> 
														<input type="text" name="t_jumin" value="${t.t_jumin }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>휴대폰 번호</label> 
														<input type="tel" name="t_mobile" value="${t.t_mobile }" class="form-control" onkeyup="autoHypenPhone('t_mobile')" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>자택전화번호</label> 
														<input type="tel" name="t_tel" value="${t.t_tel }" class="form-control" onkeyup="autoHypenPhone('t_tel')" readonly>
													</div>
												</div>
										
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>E-Mail</label> 
														<input type="text" name="t_email"value="${t.t_email }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>차량번호</label> 
														<input type="text" name="t_car"value="${t.t_car}" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,도)</label>
															<input type="text" class="form-control input-sm" name="t_addr1" value="${t.t_addr1 }" onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,군,구)</label>
															<input type="text" class="form-control input-sm" name="t_addr2"  value="${t.t_addr2 }" onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">상세주소</label>
															<input type="text" class="form-control input-sm" name="t_addr3" value="${t.t_addr3 }" onclick="openAddressIfEmpty()" >
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="t_zip"  value="${t.t_zip }"readonly>
														</div>
													</div>
												</div>
												
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 기사회사정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>소속</label> 
														<select	class="select select2-hidden-accessible" name="t_branch" disabled>
																<c:forEach var="branch" items="${branch}">
																<c:if test="${branch.c_id eq t.t_branch }">
																<option value="${branch.c_id}"selected>${branch.comname}</option>
																</c:if>
																<c:if test="${branch.c_id ne t.t_branch }">
																<option value="${branch.c_id}">${branch.comname}</option>
																</c:if>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>부서</label> 
														<input type="text" name="t_department" value="${t.t_department }" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>직급</label> 
														<input type="text" name="t_position"  value="${t.t_position }" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>직무</label> 
														<input type="text" name="t_job" value="${t.t_job }" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>사무실전화</label> 
														<input type="tel" name="t_offtel" value="${t.t_offtel }" class="form-control" onkeyup="autoHypenPhone('t_offtel')" readonly>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 기사회사정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
											<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>설치지역</label> 
														<select class="select select2-hidden-accessible" name="ta_city" onchange="citySelect(this.value);" disabled>
															<c:forEach var="city" items="${city}">
																<c:choose>
																	<c:when test="${city.sub_loc1 eq tlist.t_activity_loc}">
																		<option value="${city.sub_loc1}" selected>${city.sub_loc1}</option>										
																	</c:when>
																	<c:otherwise>
																		<option value="${city.sub_loc1}" >${city.sub_loc1}</option>										
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group form-group-default">
														<label>상세설치지역</label> 
														<select class="select select2-hidden-accessible" id ="ta_gu" name="ta_gu"  multiple="multiple" disabled >
															<c:forEach var="district" items="${district}">
															<c:forEach var = "dis" items="${s_dis}">
																<c:choose>
																	<c:when test="${district.sub_loc2 eq dis }">
																	<option value="${district.sub_loc2}" selected>${district.sub_loc2}</option>
																	</c:when>
																</c:choose>
															</c:forEach>
															<option value="${district.sub_loc2}" >${district.sub_loc2}</option>
															</c:forEach>																
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>ID</label>
														 <input type="text" name="t_logid" value="${t.t_logid }" class="form-control" readonly/> 
													</div>
												</div>
<!-- 												<div class="col-xs-6 col-sm-3 col-md-2"> -->
<!-- 													<div class="form-group form-group-default"> -->
<!-- 														<label>PW</label>  -->
<%-- 														<input type="text" name="t_pass" value="${t.t_pass }" class="form-control" > --%>
<!-- 													</div> -->
<!-- 												</div> -->
												<!-- <div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>PW 확인</label> 
														<input type="text" name="t_pass2" class="form-control" >
													</div>
												</div> -->
											</div>
										</div>
									</div>
									<div id="mod_button" class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
											<!-- 	<span id="checkIdSpan" style="color: red;"></span>
												<span id="checkPwSpan" style="color: red;"></span> -->
													<a id="mod_reason" class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div id="mod_button" class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
											<!-- 	<span id="checkIdSpan" style="color: red;"></span>
												<span id="checkPwSpan" style="color: red;"></span> -->
													<a id="mod_reason" class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div id="modify_reason" class="col-xs-12" style="display:none">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 수정사유
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>변경사유</label>
														<select	class="select select2-hidden-accessible" name="e_modify_memo" onchange="modify(this.value)">
															<option value="" selected>선택</option>
															<option value="개인정보수정">개인정보수정</option>
															<option value="사원정보수정">사원정보수정</option>
															<option value="기타">기타</option>
														 </select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
													<a id="mod_reason" class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left"></i>변경이력
									</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12">
												<table class="table table-hover nowrap" id="checkTable" data-page-length="10">
													<thead class="alpha-slate">
														<tr>
															<th class="active">선택</th>
															<th class="active">수정날짜</th>
															<th class="active">수정사유</th>
															<th class="active">수정자</th>
														</tr>
													</thead>
														<tbody>
														</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	var table = $('#productTable').DataTable();
	var t_idx = $('input[name=t_idx]').val();
	
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
			{ "data": "th_idx" },
		    { "data": "t_mod_date" },
		    { "data": "t_modify_memo" },
		    { "data": "e_name" }
		  ],
		});
		//첫 dataTable initialize
		if(! $.fn.DataTable.isDataTable( '#checkTable' )){
			var param=$('form[name=searchForm]').serialize();
			checkDt('checkTable','/employee/technicianHistoryList/'+t_idx);
		}
		
	
	
	
	//등록
	 function startRental(){
		var btn=$('#mod_reason').html();
	    var bttn = btn.substring(0,2);
  	 	var t_id = $('input[name=t_id]').val();
   	
   	if(bttn=='확인'){
   		if(window.confirm('수정하시겠습니까?')){
   			var param=$('form[name=frm]').serialize();
   			var setting={
   				data:param,
   				url:'/employee/updateTechnician',
   				type:"post",
   				enctype:"multipart/form-data"
   			};
   			$.ajax(setting).done(function(data){
   				if(data.trim()=='등록 완료'){
   					alert('수정하였습니다.');
						opener.parent.refreshTable();
   					window.close();
   				}else{
   					alert('수정 실패하였습니다.');
   					opener.parent.refreshTable();
   					window.close();
   				}
   				
   			});
   		}
   	}else{
   		$('#mod_reason').html('확인');
		$('#mod_reason').removeClass('blue-hoki');
		$('#mod_reason').addClass('btn-danger');
		$('#mod_button').css('display','none');
		$('#modify_reason').css('display','');
   		$("input[name=t_department]").removeAttr('readonly');
   		$("input[name=t_position]").removeAttr('readonly');
   		$("input[name=t_job]").removeAttr('readonly');
   		$("input[name=t_mobile]").removeAttr('readonly');
   		$("input[name=t_name]").removeAttr('readonly');
   		$("input[name=t_addr3]").removeAttr('readonly');
   		$("input[name=t_tel]").removeAttr('readonly');
   		$("input[name=t_email]").removeAttr('readonly');
   		$("input[name=t_offtel]").removeAttr('readonly');
   		$("select[name=t_status]").removeAttr('disabled');
   		$("select[name=ta_city]").removeAttr('disabled');
   		$("select[name=ta_gu]").removeAttr('disabled');
   		$("select[name=t_branch]").removeAttr('disabled');
//    		$('form[name=frm] input').removeAttr('readonly');
   	}
   	
   }
	
	function citySelect(province){
		$.ajax({
		 type: "POST",
		 url: "/employee/district",
		 dataType:"json",
		 contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		 data: {param:province},
		 success: function(result){

		  //SELECT BOX 초기화           
		  $("#ta_gu").find("option").remove();
		  //배열 개수 만큼 option 추가
		   $.each(result, function(i){
			   var district  = result[i].replace(/\{/g,'');
			   district = district.replace(/\}/g,'');
			   district = district.replace(/\=/g,'');
			   district = district.replace('sub_loc2','');
			   $("#ta_gu").append("<option value='"+  district+"'>"+district+"</option>")
		   });    
		  },
		   error: function (jqXHR, textStatus, errorThrown) {
		   alert("오류가 발생하였습니다.");
		  }                     
		 });
		}
	
	

	//다음 주소
		function openAddressIfEmpty(){
			if($('input[name=t_addr1]').val()==''){
				openAddress();
			}
		}
		function openAddress(){
			new daum.Postcode({
			    oncomplete: function(data) {
			      	var sido=data.sido;
			    	var sigungu=data.sigungu;
			    	var roadname=data.address.replace(sido,'');
			    	roadname=roadname.trim();
			    	roadname=roadname.replace(sigungu,'');
			    	roadname=roadname.trim();
			    	var zonecode=data.zonecode;
			    	$('input[name=t_addr1]').val(sido);
			    	$('input[name=t_addr2]').val(sigungu);
			    	$('input[name=t_addr3]').val(roadname);
			    	$('input[name=t_zip]').val(zonecode);
			    }
			}).open({popupName:'daumAddr'});
		}
		
		function autoHypenPhone(col){
			  var str = $('input[name='+col+']').val();
				str = str.replace(/[^0-9]/g, '');
			  var tmp = '';
			  if( str.length < 4){
				  tmp=str;
			  }else if(str.length == 7){
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3);
			  }else if(str.length == 8){
				tmp += str.substr(0, 2);
			    tmp += '-';
			    tmp += str.substr(2, 2);
			    tmp += '-';
			    tmp += str.substr(4);
			  }
			  else if(str.length == 9){
				    tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 3);
				    tmp += '-';
				    tmp += str.substr(5);
				  }
			  else if(str.length == 10){
				  tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 4);
				    tmp += '-';
				    tmp += str.substr(6);
				
			  }
			  else if(str.length >11){
				  tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3, 4);
				    tmp += '-';
				    tmp += str.substr(7,4);
			  }
			  
			  else{        
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3, 4);
			    tmp += '-';
			    tmp += str.substr(7);
			  }
			     return $('input[name='+col+']').val(tmp);
			}
	</script>
</body>
</html>