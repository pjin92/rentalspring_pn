<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">거래처 상세&amp;수정</span> <small class="display-block">상세&amp;수정</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" id="frm" name="frm" method="post" enctype="multipart/form-data">
				
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 등록정보
										</h5>
									</div>
									<c:set var="data" value="${data }"/>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default disabled">
														<label>등록일</label> 
														<input type="text" name="c_date" value="${data.c_date }"class="form-control" readonly> 
														<input type="hidden" id="c_id" name="c_id" value="${data.c_id }" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>구분코드</label> 	
														<select class="select select2-hidden-accessible" name="c_category1" onchange="createAccount()"  disabled>
																<c:forEach var="category" items="${category}">
																<c:if test="${data.c_category1 eq category.cc_idx }">
																<option value="${category.cc_idx }"selected>${category.cc_name}</option>
																</c:if>
																<c:if test="${data.c_category1 ne category.cc_idx }">
																<option value="${category.cc_idx }">${category.cc_name}</option>
																</c:if>
																</c:forEach>
														</select>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>분류</label> 
														<select	class="select select2-hidden-accessible" name="c_type" disabled>
																<c:forEach var="type" items="${type}">
																<c:if test="${data.c_type eq type.ct_idx }">
																<option value="${type.ct_idx}"selected>${type.ct_name}</option>
																</c:if>	
																<c:if test="${data.c_type ne type.ct_idx }">
																<option value="${type.ct_idx}">${type.ct_name}</option>
																</c:if>																
																</c:forEach>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 거래처 기초정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>상호법인명</label> 
														<input type="text" name="c_comname" value="${data.c_comname}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>대표자성명</label> 
														<input type="text" name="c_ceoname" value="${data.c_ceoname}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>사업자번호</label> 
														<input type="text" name="c_comnum" value="${data.c_comnum}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>거래처 약칭</label> 
														<input type="text" name="c_cname" value="${data.c_cname}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>전화번호<small>(회사대표번호)</small></label>  
														<input type="tel" name="c_offtel1" value="${data.c_offtel1}" class="form-control" onkeyup="autoHypenPhone('c_offtel1')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>팩스번호</label> 
														<input type="text" name="c_fax" value="${data.c_fax}" class="form-control" onkeyup="autoHypenPhone('c_fax')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>은행</label> 
														<select	class="select select2-hidden-accessible" name="c_bank" disabled>
																<c:forEach var="bank" items="${bank}">
																<c:if test="${bank.sb_code eq data.c_bank }">
																	<option value="${bank.sb_code}"selected>${bank.sb_name}</option>
																</c:if>																
																<c:if test="${bank.sb_code ne data.c_bank }">
																	<option value="${bank.sb_code}">${bank.sb_name}</option>
																</c:if>																
																</c:forEach>
														 </select>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>계좌번호</label> 
														<input type="text" name="c_account" value="${data.c_account}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,도)</label>
															<input type="text" class="form-control input-sm" name="c_addr1" value="${data.c_addr1 }"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,군,구)</label>
															<input type="text" class="form-control input-sm" name="c_addr2" value="${data.c_addr2 }" onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">상세주소</label>
															<input type="text" class="form-control input-sm" name="c_addr3" value="${data.c_addr3 }"onclick="openAddressIfEmpty()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="c_zip" value="${data.c_zip}" readonly>
														</div>
													</div>
												</div>
												
												
													
											<div class="col-xs-6 col-sm-6 col-md-3">
												<div class="form-group form-group-default ">
												<label>사업자등록증</label> 
<!-- 													<input type="file" name="c_file" id="c_file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required> -->
												<a href="/file/customer_cr/${c_id}_${data.c_comname}">${data.c_file}</a>
												</div>
											</div> 
												
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="form-group form-group-default">
													<label>사업자등록증 재등록</label>
													<input type="file" id="c_file" name="c_file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required disabled>
												</div>
											</div>	
												
											<div class="col-xs-6 col-sm-3 col-md-3">
												<div class="form-group form-group-default">
													<label>기존 통장사본</label>
													<a href="/file/customer_bb/${c_id}_${data.c_account}">${data.c_bankbook}</a>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="form-group form-group-default">
													<label>통장사본 재등록하기</label>
													<input type="file" id="c_bankbook" name="c_bankbook" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required disabled>
												</div>
											</div>
														
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default ">
														<label>메모</label> 
														<input type="text" class="form-control input-sm" name="c_memo" value="${data.c_memo }" style="resize:vertical" readonly>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 거래처 담당자 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>성명(담당자)</label> 
														<input type="text" name="c_name1" value="${data.c_name1}" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>부서</label> 
														<input type="text" name="c_part1" value="${data.c_part1}" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>직급(직책)</label> 
														<input type="text" name="c_position1"  value="${data.c_position1}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>이메일</label> 
														<input type="text" name="c_email1"  value="${data.c_email1}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>핸드폰번호</label> 
														<input type="text" name="c_phone1"  value="${data.c_phone1}" class="form-control" onkeyup="autoHypenPhone('c_phone1')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>전화번호 (담당자)</label> 
														<input type="text" name="c_tel1"  value="${data.c_tel1}" class="form-control" onkeyup="autoHypenPhone('c_tel1')" readonly>
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>담당자메모</label> 
														<input type="text" class="form-control input-sm" name="c_memo1" value="${data.c_memo1 }" style="resize:vertical" readonly>
													</div>
												</div>
											
											
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 거래처 상태
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>상태</label> 	
														<select class="select select2-hidden-accessible" name="c_state" disabled>
																<c:forEach var="state" items="${state}">
																<c:if test="${data.c_state eq state.cs_idx }">
																<option value="${state.cs_idx}"selected>${state.cs_name}</option>
																</c:if>
																<c:if test="${data.c_state ne state.cs_idx }">
																<option value="${state.cs_idx}">${state.cs_name}</option>
																</c:if>
																</c:forEach>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>상태 메모</label> 
														<input type="text" class="form-control input-sm" name="c_memo2" value="${data.c_memo2 }" style="resize:vertical" readonly>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 거래처 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default disabled">
														<label>ID</label>
														 <input type="text" name="c_logid" value="${data.c_logid}" class="form-control" readonly> 
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="mod_button" class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
													<a id="mod_reason" class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div id="modify_reason" class="col-xs-12" style="display:none">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>6. 변경사유
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-1 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>수정사유</label>
														 <select	class="select select2-hidden-accessible" name="ㅊ_modify_memo" onchange="modify(this.value)">
															<option value="" selected>선택</option>
															<option value="개인정보수정">등록정보수정</option>
															<option value="기초정보수정">기초정보수정</option>
															<option value="담당자정보수정">담당자정보수정</option>
															<option value="거래처정보수정">거래처정보수정</option>
															<option value="기타">기타</option>
														 </select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
													<a id="mod_reason" class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left"></i>변경이력
									</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12">
												<table class="table table-hover nowrap" id="checkTable" data-page-length="10">
													<thead class="alpha-slate">
														<tr>
															<th>선택</th>
															<th>수정날짜</th>
															<th>수정사유</th>
															<th>수정자</th>
														</tr>
													</thead>
														<tbody>
														</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
		<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	var table = $('#productTable').DataTable();
	var c_id = $('input[name=c_id]').val();
	
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
			{ "data": "ch_idx" },
		    { "data": "c_modify_date" },
		    { "data": "c_modify_memo" },
		    { "data": "e_name" }
		  ],
		});
		//첫 dataTable initialize
		if(! $.fn.DataTable.isDataTable( '#checkTable' )){
			var param=$('form[name=searchForm]').serialize();
			checkDt('checkTable','/employee/customer/employeeCustomerHistoryList/'+c_id);
		}
		
	
	
	
	 function startRental(){
	    	var btn=$('#mod_reason').html();
	    	var bttn = btn.substring(0,2);
	    	var c_id = $('input[name=c_id]').val();
	    	
	    	if(bttn=='확인'){
	    		if(window.confirm('수정하시겠습니까?')){
	    			var param=$('form[name=frm]').serialize();
	    			$('#frm').ajaxForm({
	    				url:'/employee/customer/customerDetail/'+c_id,
	    				type:'post',
	    				enctype:"multipart/form-data",
	    				 contentType: false,
	    				success:function(res){
	    					if(res.trim()=='등록완료'){
	    						alert('수정에 성공했습니다.');
	    						window.opener.refreshTable();
	    						window.close();
	    					}else{
	    						alert(res);
	    						window.opener.refreshTable();
	    						window.close();
	    					}
	    				}
	    			});
	    			$("#frm").submit() ;
	    		}
	    	}else{
	    		$('#mod_reason').html('확인');
	    		$('#mod_reason').removeClass('blue-hoki');
	    		$('#mod_reason').addClass('btn-danger');
	    		$('#mod_button').css('display','none');
	    		$('#modify_reason').css('display','');
// 	    		$("input[name=c_ceoname]").removeAttr('readonly');
	    		$("input[name=c_bank]").removeAttr('readonly');
	    		$("input[name=c_comname]").removeAttr('readonly');
	    		$("input[name=c_ceoname]").removeAttr('readonly');
	    		$("input[name=c_comnum]").removeAttr('readonly');
	    		$("input[name=c_account]").removeAttr('readonly');
	    		$("input[name=c_cname]").removeAttr('readonly');
	    		$("input[name=c_fax]").removeAttr('readonly');
	    		$("input[name=c_offtel1]").removeAttr('readonly');
	    		$("input[name=c_name1]").removeAttr('readonly');
	    		$("input[name=c_part1]").removeAttr('readonly');
	    		$("input[name=c_position1]").removeAttr('readonly');
	    		$("input[name=c_email1]").removeAttr('readonly');
	    		$("input[name=c_phone1]").removeAttr('readonly');
	    		$("input[name=c_tel1]").removeAttr('readonly');
	    		$("input[name=c_addr3]").removeAttr('readonly');
	    		$("input[name=c_memo]").removeAttr('readonly');
	    		$("input[name=c_memo1]").removeAttr('readonly');
	    		$("input[name=c_memo2]").removeAttr('readonly');
	    		$("select[name=c_type]").removeAttr('disabled');
	    		$("select[name=c_category1]").removeAttr('disabled');
	    		$("select[name=c_state]").removeAttr('disabled');
	    		$("select[name=c_bank]").removeAttr('disabled');
	    		$("#c_bankbook").removeAttr('disabled');
	    		$("#c_file").removeAttr('disabled');
	    	}
	    	
	    }
	//다음 주소
		function openAddressIfEmpty(){
			if($('input[name=c_addr1]').val()==''){
				openAddress();
			}
		}
		function openAddress(){
			new daum.Postcode({
			    oncomplete: function(data) {
			      	var sido=data.sido;
			    	var sigungu=data.sigungu;
			    	var roadname=data.address.replace(sido,'');
			    	roadname=roadname.trim();
			    	roadname=roadname.replace(sigungu,'');
			    	roadname=roadname.trim();
			    	var zonecode=data.zonecode;
			    	$('input[name=c_addr1]').val(sido);
			    	$('input[name=c_addr2]').val(sigungu);
			    	$('input[name=c_addr3]').val(roadname);
			    	$('input[name=c_zip]').val(zonecode);
			    }
			}).open({popupName:'daumAddr'});
		}
		
		function autoHypenPhone(col){
			  var str = $('input[name='+col+']').val();
				str = str.replace(/[^0-9]/g, '');
			  var tmp = '';
			  if( str.length < 4){
				  tmp=str;
			  }else if(str.length == 7){
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3);
			  }else if(str.length == 8){
				tmp += str.substr(0, 2);
			    tmp += '-';
			    tmp += str.substr(2, 2);
			    tmp += '-';
			    tmp += str.substr(4);
			  }
			  else if(str.length == 9){
				    tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 3);
				    tmp += '-';
				    tmp += str.substr(5);
				  }
			  else if(str.length == 10){
				  tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 4);
				    tmp += '-';
				    tmp += str.substr(6);
				
			  }
			  else if(str.length >11){
				  tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3, 4);
				    tmp += '-';
				    tmp += str.substr(7,4);
			  }
			  
			  else{        
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3, 4);
			    tmp += '-';
			    tmp += str.substr(7);
			  }
			     return $('input[name='+col+']').val(tmp);
			}
		
		
		function createAccount(){
			var category = $('select[name=c_category1]').val();
			if(category == '3' || category=='4'){
				$('#c_logid').attr('readonly',true)
				$('#c_pass').attr('readonly',true)
				$('#c_pass_check').attr('readonly',true)
				$('#c_logid').val('');
				$('#c_pass').val('');
				$('#c_pass_check').val('');
			}else{
				$('input[name=c_logid]').removeAttr('readonly')
				$('input[name=c_pass]').removeAttr('readonly')
				$('input[name=c_pass_check]').removeAttr('readonly')
			}
		}
		
		function modifyList(){
			var ch_idx = $(event.target).attr('id');
			var c_id = $('input[name=c_id]').val();
			window.scrollTo(0,0);
			window.open('/employee/customer/customerDetail/customerHistory/'+ch_idx+'_'+c_id,'popup2','width=1200,height=800');
		}
		
		function modify(sel){
			if(sel=='기타'){
			}
		}
	</script>
</body>
</html>