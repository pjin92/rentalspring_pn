<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">거래처 등록</span> <small class="display-block">거래처 등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" id="frm" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 등록정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>구분코드</label> 	
														<select class="select select2-hidden-accessible" name="c_category1" onchange="createAccount()">
																<option value="" selected>선택</option>
																<c:forEach var="category" items="${category}">
																<option value="${category.cc_idx }">${category.cc_name}</option>
																</c:forEach>
														</select>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>분류</label> 
														<select	class="select select2-hidden-accessible" name="c_type" >
																<option value="" selected>선택</option>
																<c:forEach var="type" items="${type}">
																<option value="${type.ct_idx}">${type.ct_name}</option>
																</c:forEach>
														 </select>
													</div>
												</div> 
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 거래처 기초정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>상호법인명</label> 
														<input type="text" name="c_comname" value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>대표자성명</label> 
														<input type="text" name="c_ceoname" value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>사업자번호</label> 
														<input type="text" name="c_comnum" value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>거래처약칭 </label> 
														<input type="text" name="c_cname" value="" class="form-control" >
													</div>
												</div>
											
											<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>전화번호<small>(회사대표번호)</small></label>  
														<input type="tel" name="c_offtel1" value="" class="form-control" onkeyup="autoHypenPhone('c_offtel1')">
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>팩스번호</label> 
														<input type="text" name="c_fax" value="" class="form-control" onkeyup="autoHypenPhone('c_fax')">
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>은행</label> 
														<select	class="select select2-hidden-accessible" name="c_bank" >
																<option value="" selected>선택</option>
																<c:forEach var="bank" items="${bank}">
																<option value="${bank.sb_code}">${bank.sb_name}</option>
																</c:forEach>
														 </select>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>계좌번호</label> 
														<input type="text" name="c_account" value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,도)</label>
															<input type="text" class="form-control input-sm" name="c_addr1"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,군,구)</label>
															<input type="text" class="form-control input-sm" name="c_addr2"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">상세주소</label>
															<input type="text" class="form-control input-sm" name="c_addr3" onclick="openAddressIfEmpty()" >
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="c_zip" readonly>
														</div>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-6 col-md-6">
													<div class="form-group form-group-default ">
													<label>사업자등록증</label> 
													<input type="file" name="c_file" id="c_file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>
													</div>
												</div> 
												
												
												<div class="col-xs-6 col-sm-6 col-md-6">
													<div class="form-group form-group-default ">
													<label>통장사본</label> 
													<input type="file" name="c_bankbook" id="c_bankbook" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>
													</div>
												</div> 
														
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default ">
														<label>메모</label> 
														<textarea class="form-control input-sm" name="c_memo" style="resize:vertical"></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 거래처 담당자 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>성명(담당자)</label> 
														<input type="text" name="c_name1" value="" class="form-control" >
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>부서</label> 
														<input type="text" name="c_part1" value="" class="form-control" >
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>직급(직책)</label> 
														<input type="text" name="c_position1"  value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>이메일</label> 
														<input type="text" name="c_email1"  value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>핸드폰번호</label> 
														<input type="text" name="c_phone1"  value="" class="form-control" onkeyup="autoHypenPhone('c_phone1')" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>전화번호 (담당자)</label> 
														<input type="text" name="c_tel1"  value="" class="form-control" onkeyup="autoHypenPhone('c_tel1')">
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>담당자메모</label> 
														<textarea class="form-control input-sm" name="c_memo1" style="resize:vertical"></textarea>
													</div>
												</div>
											
											
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 거래처 상태
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>상태</label> 	
														<select class="select select2-hidden-accessible" name="c_state" >
																<option value="" selected>선택</option>
																<c:forEach var="state" items="${state}">
																<option value="${state.cs_idx}">${state.cs_name}</option>
																</c:forEach>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>상태 메모</label> 
														<textarea class="form-control input-sm" name="c_memo2" style="resize:vertical"></textarea>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 거래처 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>ID</label>
														 <input type="text" id="" name="c_logid" value="" class="form-control" readonly> 
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>PW</label> 
														<input type="text" id="c_pass" name="c_pass" value="" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>PW 확인</label> 
														<input type="text" id="c_pass_check" name="c_pass_check" class="form-control" readonly>
													</div>
												</div>
											</div>
											
											
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
													<a class="btn btn-danger position-right" onclick="startRental()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
		<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	
	function startRental(){
		var param=$('form[name=frm]').serialize();
		$('#frm').ajaxForm({
			url:'/employee/customer/create',
			type:'post',
			enctype:"multipart/form-data",
			 contentType: false,
			success:function(res){
				if(res.trim()=='등록완료'){
					alert('등록에 성공했습니다.');
					window.opener.refreshTable();
					window.close();
				}else{
					alert(res);
					window.opener.refreshTable();
					window.close();
				}
			}
		});
		$("#frm").submit() ;
	}
	
	
	//다음 주소
		function openAddressIfEmpty(){
			if($('input[name=c_addr1]').val()==''){
				openAddress();
			}
		}
		function openAddress(){
			new daum.Postcode({
			    oncomplete: function(data) {
			      	var sido=data.sido;
			    	var sigungu=data.sigungu;
			    	var roadname=data.address.replace(sido,'');
			    	roadname=roadname.trim();
			    	roadname=roadname.replace(sigungu,'');
			    	roadname=roadname.trim();
			    	var zonecode=data.zonecode;
			    	$('input[name=c_addr1]').val(sido);
			    	$('input[name=c_addr2]').val(sigungu);
			    	$('input[name=c_addr3]').val(roadname);
			    	$('input[name=c_zip]').val(zonecode);
			    }
			}).open({popupName:'daumAddr'});
		}
		
		function autoHypenPhone(col){
			  var str = $('input[name='+col+']').val();
				str = str.replace(/[^0-9]/g, '');
			  var tmp = '';
			  if( str.length < 4){
				  tmp=str;
			  }else if(str.length == 7){
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3);
			  }else if(str.length == 8){
				tmp += str.substr(0, 2);
			    tmp += '-';
			    tmp += str.substr(2, 2);
			    tmp += '-';
			    tmp += str.substr(4);
			  }
			  else if(str.length == 9){
				    tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 3);
				    tmp += '-';
				    tmp += str.substr(5);
				  }
			  else if(str.length == 10){
				  tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 4);
				    tmp += '-';
				    tmp += str.substr(6);
				
			  }
			  else if(str.length >11){
				  tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3, 4);
				    tmp += '-';
				    tmp += str.substr(7,4);
			  }
			  
			  else{        
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3, 4);
			    tmp += '-';
			    tmp += str.substr(7);
			  }
			     return $('input[name='+col+']').val(tmp);
			}
		
		
		function createAccount(){
			var category = $('select[name=c_category1]').val();
			if(category == '3' || category=='4'){
				$('#c_logid').attr('readonly',true)
				$('#c_pass').attr('readonly',true)
				$('#c_pass_check').attr('readonly',true)
				$('#c_logid').val('');
				$('#c_pass').val('');
				$('#c_pass_check').val('');
			}else{
				$('input[name=c_logid]').removeAttr('readonly')
				$('input[name=c_pass]').removeAttr('readonly')
				$('input[name=c_pass_check]').removeAttr('readonly')
			}
			
		}
	</script>
</body>
</html>