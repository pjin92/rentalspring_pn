<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<c:set var="hData" value="${hData}"/>
						<c:set var="nData" value="${nData}"/>
						<c:if test="${empty oData}">
						<c:set var="oData" value="${hData}"/>
						</c:if>
						<c:if test="${not empty oData}">
						<c:set var="oData" value="${oData}"/>
						</c:if>
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">${hData.c_comname} &nbsp;변경이력</span> <small class="display-block">변경이력</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" id="frm" name="frm" method="post" enctype="multipart/form-data">
				
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 등록정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>등록일</label> 
														<input type="text" name="c_date" value="${hData.c_date }"class="form-control" readonly> 
														<input type="hidden" id="c_id" name="c_id" value="${hData.c_id }" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_category1 eq nData.c_category1 and nData.c_category1 eq oData.c_category1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_category1 eq hData.c_category1 and oData.c_category1 ne hData.c_category1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_category1 ne nData.c_category1 and oData.c_category1 eq hData.c_category1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
													
														<label>구분코드</label> 	
														<select class="select select2-hidden-accessible" name="c_category1" onchange="createAccount()"  disabled>
																<c:forEach var="category" items="${category}">
																<c:if test="${hData.c_category1 eq category.cc_idx }">
																<option value="${category.cc_idx }"selected>${category.cc_name}</option>
																</c:if>
																<c:if test="${hData.c_category1 ne category.cc_idx }">
																<option value="${category.cc_idx }">${category.cc_name}</option>
																</c:if>
																</c:forEach>
														</select>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_type eq nData.c_type and nData.c_type eq oData.c_type}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_type eq hData.c_type and oData.c_type ne hData.c_type}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_type ne nData.c_type and oData.c_type eq hData.c_type}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>분류</label> 
														<select	class="select select2-hidden-accessible" name="c_type" disabled>
																<c:forEach var="type" items="${type}">
																<c:if test="${hData.c_type eq type.ct_idx }">
																<option value="${type.ct_idx}"selected>${type.ct_name}</option>
																</c:if>	
																<c:if test="${hData.c_type ne type.ct_idx }">
																<option value="${type.ct_idx}">${type.ct_name}</option>
																</c:if>																
																</c:forEach>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 거래처 기초정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_comname eq nData.c_comname and nData.c_comname eq oData.c_comname}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_comname eq hData.c_comname and oData.c_comname ne hData.c_comname}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_comname ne nData.c_comname and oData.c_comname eq hData.c_comname}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>상호법인명</label> 
														<input type="text" name="c_comname" value="${hData.c_comname}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_ceoname eq nData.c_ceoname and nData.c_ceoname eq oData.c_ceoname}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_ceoname eq hData.c_ceoname and oData.c_ceoname ne hData.c_ceoname}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_ceoname ne nData.c_ceoname and oData.c_ceoname eq hData.c_ceoname}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>대표자성명</label> 
														<input type="text" name="c_ceoname" value="${hData.c_ceoname}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_comnum eq nData.c_comnum and nData.c_comnum eq oData.c_comnum}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_comnum eq hData.c_comnum and oData.c_comnum ne hData.c_comnum}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_comnum ne nData.c_comnum and oData.c_comnum eq hData.c_comnum}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>사업자번호</label> 
														<input type="text" name="c_comnum" value="${hData.c_comnum}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_cname eq nData.c_cname and nData.c_cname eq oData.c_cname}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_cname eq hData.c_cname and oData.c_cname ne hData.c_cname}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_cname ne nData.c_cname and oData.c_cname eq hData.c_cname}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>거래처 약칭</label> 
														<input type="text" name="c_cname" value="${hData.c_cname}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_offtel1 eq nData.c_offtel1 and nData.c_offtel1 eq oData.c_offtel1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_offtel1 eq hData.c_offtel1 and oData.c_offtel1 ne hData.c_offtel1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_offtel1 ne nData.c_offtel1 and oData.c_offtel1 eq hData.c_offtel1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>전화번호<small>(회사대표번호)</small></label>  
														<input type="tel" name="c_offtel1" value="${hData.c_offtel1}" class="form-control" onkeyup="autoHypenPhone('c_offtel1')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_fax eq nData.c_fax and nData.c_fax eq oData.c_fax}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_fax eq hData.c_fax and oData.c_fax ne hData.c_fax}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_fax ne nData.c_fax and oData.c_fax eq hData.c_fax}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>팩스번호</label> 
														<input type="text" name="c_fax" value="${hData.c_fax}" class="form-control" onkeyup="autoHypenPhone('c_fax')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_bank eq nData.c_bank and nData.c_bank eq oData.c_bank}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_bank eq hData.c_bank and oData.c_bank ne hData.c_bank}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_bank ne nData.c_bank and oData.c_bank eq hData.c_bank}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>은행</label> 
														<select	class="select select2-hidden-accessible" name="c_bank" disabled>
																<c:forEach var="bank" items="${bank}">
																<c:if test="${bank.sb_code eq hData.c_bank }">
																	<option value="${bank.sb_code}"selected>${bank.sb_name}</option>
																</c:if>																
																<c:if test="${bank.sb_code ne hData.c_bank }">
																	<option value="${bank.sb_code}">${bank.sb_name}</option>
																</c:if>																
																</c:forEach>
														 </select>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_account eq nData.c_account and nData.c_account eq oData.c_account}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_account eq hData.c_account and oData.c_account ne hData.c_account}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_account ne nData.c_account and oData.c_account eq hData.c_account}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>계좌번호</label> 
														<input type="text" name="c_account" value="${hData.c_account}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_addr1 eq nData.c_addr1 and nData.c_addr1 eq oData.c_addr1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_addr1 eq hData.c_addr1 and oData.c_addr1 ne hData.c_addr1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_addr1 ne nData.c_addr1 and oData.c_addr1 eq hData.c_addr1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">주소(시,도)</label>
															<input type="text" class="form-control input-sm" name="c_addr1" value="${hData.c_addr1 }"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_addr2 eq nData.c_addr2 and nData.c_addr2 eq oData.c_addr2}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_addr2 eq hData.c_addr2 and oData.c_addr2 ne hData.c_addr2}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_addr2 ne nData.c_addr2 and oData.c_addr2 eq hData.c_addr2}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">주소(시,군,구)</label>
															<input type="text" class="form-control input-sm" name="c_addr2" value="${hData.c_addr2 }" onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_addr3 eq nData.c_addr3 and nData.c_addr3 eq oData.c_addr3}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_addr3 eq hData.c_addr3 and oData.c_addr3 ne hData.c_addr3}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_addr3 ne nData.c_addr3 and oData.c_addr3 eq hData.c_addr3}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">상세주소</label>
															<input type="text" class="form-control input-sm" name="c_addr3" value="${hData.c_addr3 }"onclick="openAddressIfEmpty()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_zip eq nData.c_zip and nData.c_zip eq oData.c_zip}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_zip eq hData.c_zip and oData.c_zip ne hData.c_zip}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_zip ne nData.c_zip and oData.c_zip eq hData.c_zip}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="c_zip" value="${hData.c_zip}" readonly>
														</div>
													</div>
												</div>
												
												
													
											<div class="col-xs-6 col-sm-6 col-md-3">
												<c:choose>
													<c:when test="${hData.c_file eq nData.c_file and nData.c_file eq oData.c_file}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_file eq hData.c_file and oData.c_file ne hData.c_file}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_file ne nData.c_file and oData.c_file eq hData.c_file}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
												<label>사업자등록증</label> 
<!-- 													<input type="file" name="c_file" id="c_file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required> -->
												<a href="/file/customer_cr/${c_id}_${hData.c_comname}">${hData.c_file}</a>
												</div>
											</div> 
												
												
											<div class="col-xs-6 col-sm-3 col-md-3">
												<c:choose>
													<c:when test="${hData.c_bankbook eq nData.c_bankbook and nData.c_bankbook eq oData.c_bankbook}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_bankbook eq hData.c_bankbook and oData.c_bankbook ne hData.c_bankbook}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_bankbook ne nData.c_bankbook and oData.c_bankbook eq hData.c_bankbook}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
													<label>기존 통장사본</label>
													<a href="/file/customer_bb/${c_id}_${hData.c_account}">${hData.c_bankbook}</a>
												</div>
											</div>
											
												<div class="col-xs-12 col-sm-12 col-md-12">
												<c:choose>
													<c:when test="${hData.c_memo eq nData.c_memo and nData.c_memo eq oData.c_memo}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_memo eq hData.c_memo and oData.c_memo ne hData.c_memo}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_memo ne nData.c_memo and oData.c_memo eq hData.c_memo}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>메모</label> 
														<input type="text" class="form-control input-sm" name="c_memo" style="resize:vertical" value="${hData.c_memo }" >
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 거래처 담당자 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
												<c:choose>
													<c:when test="${hData.c_name1 eq nData.c_name1 and nData.c_name1 eq oData.c_name1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_name1 eq hData.c_name1 and oData.c_name1 ne hData.c_name1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_name1 ne nData.c_name1 and oData.c_name1 eq hData.c_name1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>성명(담당자)</label> 
														<input type="text" name="c_name1" value="${hData.c_name1}" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
												<c:choose>
													<c:when test="${hData.c_part1 eq nData.c_part1 and nData.c_part1 eq oData.c_part1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_part1 eq hData.c_part1 and oData.c_part1 ne hData.c_part1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_part1 ne nData.c_part1 and oData.c_part1 eq hData.c_part1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>부서</label> 
														<input type="text" name="c_part1" value="${hData.c_part1}" class="form-control" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-2">
												<c:choose>
													<c:when test="${hData.c_position1 eq nData.c_position1 and nData.c_position1 eq oData.c_position1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_position1 eq hData.c_position1 and oData.c_position1 ne hData.c_position1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_position1 ne nData.c_position1 and oData.c_position1 eq hData.c_position1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>직급(직책)</label> 
														<input type="text" name="c_position1"  value="${hData.c_position1}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
												<c:choose>
													<c:when test="${hData.c_email1 eq nData.c_email1 and nData.c_email1 eq oData.c_email1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_email1 eq hData.c_email1 and oData.c_email1 ne hData.c_email1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_email1 ne nData.c_email1 and oData.c_email1 eq hData.c_email1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>이메일</label> 
														<input type="text" name="c_email1"  value="${hData.c_email1}" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
												<c:choose>
													<c:when test="${hData.c_phone1 eq nData.c_phone1 and nData.c_phone1 eq oData.c_phone1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_phone1 eq hData.c_phone1 and oData.c_phone1 ne hData.c_phone1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_phone1 ne nData.c_phone1 and oData.c_phone1 eq hData.c_phone1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>핸드폰번호</label> 
														<input type="text" name="c_phone1"  value="${hData.c_phone1}" class="form-control" onkeyup="autoHypenPhone('c_phone1')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-3 col-md-2">
												<c:choose>
													<c:when test="${hData.c_tel1 eq nData.c_tel1 and nData.c_tel1 eq oData.c_tel1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_tel1 eq hData.c_tel1 and oData.c_tel1 ne hData.c_tel1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_tel1 ne nData.c_tel1 and oData.c_tel1 eq hData.c_tel1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>전화번호 (담당자)</label> 
														<input type="text" name="c_tel1"  value="${hData.c_tel1}" class="form-control" onkeyup="autoHypenPhone('c_tel1')" readonly>
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-12 col-md-12">
												<c:choose>
													<c:when test="${hData.c_memo1 eq nData.c_memo1 and nData.c_memo1 eq oData.c_memo1}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_memo1 eq hData.c_memo1 and oData.c_memo1 ne hData.c_memo1}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_memo1 ne nData.c_memo1 and oData.c_memo1 eq hData.c_memo1}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>담당자메모</label> 
														<input type="text" class="form-control input-sm" name="c_memo1" value="${hData.c_memo1 }" style="resize:vertical">
													</div>
												</div>
											
											
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 거래처 상태
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
												<c:choose>
													<c:when test="${hData.c_state eq nData.c_state and nData.c_state eq oData.c_state}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_state eq hData.c_state and oData.c_state ne hData.c_state}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_state ne nData.c_state and oData.c_state eq hData.c_state}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>상태</label> 	
														<select class="select select2-hidden-accessible" name="c_state" disabled>
																<c:forEach var="state" items="${state}">
																<c:if test="${hData.c_state eq state.cs_idx }">
																<option value="${state.cs_idx}"selected>${state.cs_name}</option>
																</c:if>
																<c:if test="${hData.c_state ne state.cs_idx }">
																<option value="${state.cs_idx}">${state.cs_name}</option>
																</c:if>
																</c:forEach>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-12">
												<c:choose>
													<c:when test="${hData.c_memo2 eq nData.c_memo2 and nData.c_memo2 eq oData.c_memo2}">
														<div class="form-group form-group-default" >
													</c:when>
													<c:when test="${nData.c_memo2 eq hData.c_memo2 and oData.c_memo2 ne hData.c_memo2}">
														<div class="form-group form-group-default" style="border: 2px solid blue">
													</c:when>
													<c:when test="${hData.c_memo2 ne nData.c_memo2 and oData.c_memo2 eq hData.c_memo2}">
														<div class="form-group form-group-default" style="background-color:yellow">
													</c:when>
													<c:otherwise>
														<div class="form-group form-group-default" style="background-color:yellow;border: 2px solid blue">
													</c:otherwise>
												</c:choose>
														<label>상태 메모</label> 
														<input type="text" class="form-control input-sm" name="c_memo2" value="${hData.c_memo2 }" style="resize:vertical">
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 거래처 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>ID</label>
														 <input type="text" name="c_logid" value="${hData.c_logid}" class="form-control" readonly> 
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div id="modify_reason" class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>6. 변경사유
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>변경사유</label>
														 <input type="text" name="c_modify_memo" value="${hData.c_modify_memo }" class="form-control" readonly> 
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
		<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
</body>
</html>