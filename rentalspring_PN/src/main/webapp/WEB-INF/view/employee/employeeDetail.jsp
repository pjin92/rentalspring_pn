<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>임직원 상세보기</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
		<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">임직원  상세&amp;수정</span> <small class="display-block"> 상세&amp;수정</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" id="frm" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 임직원 개인정보
										</h5>
									</div>
									<c:set var="ep" value="${employee}"/>
									<input type="hidden" name="e_id" value="${ep.e_id }">
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직원성명</label> 
														<input type="text" name="e_name" value="${ep.e_name }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>자택전화</label> 
														<input type="text" name="e_tell" value="${ep.e_tell }" class="form-control"  onkeyup="autoHypenPhone('e_tell')" readonly>
													</div>
												</div>
												
														
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>핸드폰번호</label> 
														<input type="text" name="e_phone" value="${ep.e_phone }" class="form-control"  onkeyup="autoHypenPhone('e_phone')" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>주민등록번호</label> 
														<input type="text" name="e_jumin" value="${ep.e_jumin }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소1</label>
															<input type="text" class="form-control input-sm" name="e_addr1" value="${ep.e_addr1}"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소2</label>
															<input type="text" class="form-control input-sm" name="e_addr2" value="${ep.e_addr2}" onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소3</label>
															<input type="text" class="form-control input-sm" name="e_addr3" value="${ep.e_addr3}" onclick="openAddressIfEmpty()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="e_zip" value="${ep.e_zip }" readonly>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 임직원 사원정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직원번호</label> 
														<input type="text" name="e_number" value="${ep.e_number }" class="form-control" onkeyup="checkId()" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>소속</label> 
														<select	class="select select2-hidden-accessible" name="e_branch" disabled>
																<c:forEach var="branch" items="${branch}">
																	<c:if test="${branch.c_id eq ep.e_branch }">
																	<option value="${branch.c_id}" selected>${branch.comname}</option>
																	</c:if>
																	<c:if test="${branch.c_id ne ep.e_branch }">
																	<option value="${branch.c_id}">${branch.comname}</option>
																	</c:if>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>부서</label> 
														<select	class="select select2-hidden-accessible" name="e_department" disabled>
																<c:forEach var="department" items="${department}">
																	<c:if test="${department.ed_idx eq ep.e_department }">
																		<option value="${department.ed_idx}" selected>${department.ed_name}</option>
																	</c:if>	
																	<c:if test="${department.ed_idx ne ep.e_department }">
																		<option value="${department.ed_idx}">${department.ed_name}</option>
																	</c:if>	
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직급</label> 
														<select	class="select select2-hidden-accessible" name="e_position" disabled>
																<c:forEach var="position" items="${position}">
																	<c:if test="${position.ep_idx eq ep.e_position }">
																		<option value="${position.ep_idx}" selected>${position.ep_name}</option>
																	</c:if>
																	<c:if test="${position.ep_idx ne ep.e_position }">
																		<option value="${position.ep_idx}">${position.ep_name}</option>
																	</c:if>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직무</label> 
														<select	class="select select2-hidden-accessible" name="e_job" disabled>
																<c:forEach var="job" items="${job}">
																	<c:if test="${job.ej_idx eq ep.e_job }">
																		<option value="${job.ej_idx}"selected>${job.ej_name}</option>
																	</c:if>	
																	<c:if test="${job.ej_idx ne ep.e_job }">
																		<option value="${job.ej_idx}">${job.ej_name}</option>
																	</c:if>	
																		
																</c:forEach>
														 </select>
													</div>
												</div>  
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>이메일</label> 
														<input type="text" name="e_email" value="${ep.e_email }" class="form-control" readonly>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>전화번호<small>(사무실번호)</small></label>  
														<input type="tel" name="e_offtel" value="${ep.e_offtel }" class="form-control" onkeyup="autoHypenPhone('e_offtel')" readonly>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 임직원 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
											
											<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>상태</label> 
														<select	class="select select2-hidden-accessible" name="e_status" disabled>
																<option value="1" selected>정상</option>
																<option value="2">퇴사</option>
																<option value="3">사용중지</option>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>로그인ID</label> 
														<input type="text" name="e_logid" value="${ep.e_logid }" class="form-control" readonly >
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>권한</label> 
														<select	class="select select2-hidden-accessible" name="e_power" >
																<c:forEach var="power" items="${power}">
																	<c:if test="${power.epo_idx eq ep.e_power }">
																		<option value="${power.epo_idx}"selected>${power.epo_name}</option>
																	</c:if>
																	<c:if test="${power.epo_idx ne ep.e_power }">
																		<option value="${power.epo_idx}">${power.epo_name}</option>
																	</c:if>
																</c:forEach>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div id="mod_button" class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
											<!-- 	<span id="checkIdSpan" style="color: red;"></span>
												<span id="checkPwSpan" style="color: red;"></span> -->
													<a id="mod_reason" class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div id="modify_reason" class="col-xs-12" style="display:none">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 수정사유
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>변경사유</label>
														<select	class="select select2-hidden-accessible" name="e_modify_memo" onchange="modify(this.value)">
															<option value="" selected>선택</option>
															<option value="개인정보수정">개인정보수정</option>
															<option value="사원정보수정">사원정보수정</option>
															<option value="기타">기타</option>
														 </select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
													<a id="mod_reason" class="btn btn-danger position-right" onclick="startRental()">수정</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left"></i>변경이력
									</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12">
												<table class="table table-hover nowrap" id="checkTable" data-page-length="10">
													<thead class="alpha-slate">
														<tr>
															<th class="active">선택</th>
															<th class="active">수정날짜</th>
															<th class="active">수정사유</th>
															<th class="active">수정자</th>
														</tr>
													</thead>
														<tbody>
														</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	
	var table = $('#productTable').DataTable();
	var e_id = $('input[name=e_id]').val();
	
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
			{ "data": "eh_idx" },
		    { "data": "e_mod_date" },
		    { "data": "e_modify_memo" },
		    { "data": "e_name" }
		  ],
		});
		//첫 dataTable initialize
		if(! $.fn.DataTable.isDataTable( '#checkTable' )){
			var param=$('form[name=searchForm]').serialize();
			checkDt('checkTable','/employee/employeeHistoryList/'+e_id);
		}
		
		function checkId(){
			var e_number=$('input[name=e_number]').val();
			if(e_number!=''){
				$.ajax('/employee/checkId?e_number='+e_number)
				.done(function(res){
					var check=Number(res);
					if(check==0){
						$('#checkIdSpan').html('');
					}else{
						$('#checkIdSpan').html('이미 사용중인 직원번호입니다.');
					}
				});
			}else{
				$('#checkIdSpan').html('');
			}
		}
		function checkPw(){
			var e_pass=$('input[name=e_pass]').val();
			var e_pass_check=$('input[name=e_pass_check]').val();
			
			if(e_pass!=''&&e_pass_check!=''&&e_pass!=e_pass_check){
				$('#checkPwSpan').html('비밀번호가 다릅니다');
			}else{
				$('#checkPwSpan').html('');
			}
			
		}
		//등록
		 function startRental(){
			 var btn=$('#mod_reason').html();
	    	var bttn = btn.substring(0,2);
	    	var e_id = $('input[name=e_id]').val();
	    	
	    	if(bttn=='확인'){
	    		if(window.confirm('수정하시겠습니까?')){
	    			var param=$('form[name=frm]').serialize();
	    			var setting={
	    				data:param,
	    				url:"/employee/employeeDetail/"+e_id,
	    				type:"post",
	    				enctype:"multipart/form-data"
	    			};
	    			$.ajax(setting).done(function(data){
	    				if(data.trim()=='등록 완료'){
	    					alert('수정하였습니다.');
							opener.parent.refreshTable();
	    					window.close();
	    				}else{
	    					alert('수정 실패하였습니다.');
	    					opener.parent.refreshTable();
	    					window.close();
	    				}
	    				
	    			});
	    		}
	    	}else{
	    		$('#mod_reason').html('확인');
	    		$('#mod_reason').removeClass('blue-hoki');
	    		$('#mod_reason').addClass('btn-danger');
	    		$('#mod_button').css('display','none');
	    		$('#modify_reason').css('display','');
	    		$("input[name=e_branch]").removeAttr('readonly');
	    		$("input[name=e_tell]").removeAttr('readonly');
	    		$("input[name=e_department]").removeAttr('readonly');
	    		$("input[name=e_position]").removeAttr('readonly');
	    		$("input[name=e_job]").removeAttr('readonly');
	    		$("input[name=e_phone]").removeAttr('readonly');
	    		$("input[name=e_email]").removeAttr('readonly');
	    		$("input[name=e_offtel]").removeAttr('readonly');
	    		$("select[name=e_power]").removeAttr('disabled');
	    		$("select[name=e_branch]").removeAttr('disabled');
	    		$("select[name=e_position]").removeAttr('disabled');
	    		$("select[name=e_job]").removeAttr('disabled');
	    		$("select[name=e_status]").removeAttr('disabled');
	    		$("select[name=e_department]").removeAttr('disabled');
	    	}
	    	
	    }
		
		//다음 주소
			function openAddressIfEmpty(){
				if($('input[name=c_addr1]').val()==''){
					openAddress();
				}
			}
			function openAddress(){
				new daum.Postcode({
				    oncomplete: function(data) {
				      	var sido=data.sido;
				    	var sigungu=data.sigungu;
				    	var roadname=data.address.replace(sido,'');
				    	roadname=roadname.trim();
				    	roadname=roadname.replace(sigungu,'');
				    	roadname=roadname.trim();
				    	var zonecode=data.zonecode;
				    	$('input[name=c_addr1]').val(sido);
				    	$('input[name=c_addr2]').val(sigungu);
				    	$('input[name=c_addr3]').val(roadname);
				    	$('input[name=c_zip]').val(zonecode);
				    }
				}).open({popupName:'daumAddr'});
			}
			
			function autoHypenPhone(col){
				  var str = $('input[name='+col+']').val();
					str = str.replace(/[^0-9]/g, '');
				  var tmp = '';
				  if( str.length < 4){
					  tmp=str;
				  }else if(str.length == 7){
				    tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3);
				  }else if(str.length == 8){
					tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 2);
				    tmp += '-';
				    tmp += str.substr(4);
				  }
				  else if(str.length == 9){
					    tmp += str.substr(0, 2);
					    tmp += '-';
					    tmp += str.substr(2, 3);
					    tmp += '-';
					    tmp += str.substr(5);
					  }
				  else if(str.length == 10){
					  tmp += str.substr(0, 2);
					    tmp += '-';
					    tmp += str.substr(2, 4);
					    tmp += '-';
					    tmp += str.substr(6);
					
				  }
				  else if(str.length >11){
					  tmp += str.substr(0, 3);
					    tmp += '-';
					    tmp += str.substr(3, 4);
					    tmp += '-';
					    tmp += str.substr(7,4);
				  }
				  
				  else{        
				    tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3, 4);
				    tmp += '-';
				    tmp += str.substr(7);
				  }
				     return $('input[name='+col+']').val(tmp);
				}
			
			
			function modifyList(){
				var eh_idx = $(event.target).attr('id');
				var e_id = $('input[name=e_id]').val();
				window.scrollTo(0,0);
				window.open('/employee/employeeHistory/'+eh_idx+'_'+e_id,'popup2','width=1200,height=800');
			}
			
			function modify(sel){
				if(sel=='기타'){
				}
			}
		</script>
</body>
</html>