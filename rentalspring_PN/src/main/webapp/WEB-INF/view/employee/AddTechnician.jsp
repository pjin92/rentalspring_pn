<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">기사 등록</span> <small class="display-block">등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 설치기사 등록정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>기사 코드</label> 
														<input type="text" name="t_number" class="form-control" onkeyup="autoInputLoginId()"> 
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>상태</label> 
														<select name="t_status"	class="select select2-hidden-accessible" >
														 <option value ="1" selected>정상 </option>
														 <option value ="2">퇴사</option>
														 <option value ="3">사용중지 </option>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 설치기사 개인정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>기사성명</label> 
														<input type="text" name="t_name" class="form-control">
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>주민번호</label> 
														<input type="text" name="t_jumin" class="form-control">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>휴대폰 번호</label> 
														<input type="tel" name="t_mobile" class="form-control"  onkeyup="autoHypenPhone('t_mobile')">
													</div>
												</div>
												
											
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>자택전화번호</label> 
														<input type="tel" name="t_tel" class="form-control"  onkeyup="autoHypenPhone('t_tel')">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>E-Mail</label> 
														<input type="text" name="t_email" class="form-control">
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>차량번호</label> 
														<input type="text" name="t_car" class="form-control">
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,도)</label>
															<input type="text" class="form-control input-sm" name="t_addr1"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,군,구)</label>
															<input type="text" class="form-control input-sm" name="t_addr2"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">상세주소</label>
															<input type="text" class="form-control input-sm" name="t_addr3" onclick="openAddressIfEmpty()" >
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="t_zip" readonly>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 기사회사정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>소속</label> 
														<select	class="select select2-hidden-accessible" name="t_branch" >
																<option value="" selected>선택</option>
																<c:forEach var="branch" items="${branch}">
																<option value="${branch.c_id}">${branch.comname}</option>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>부서</label>
														<input type="text" name="t_department" class="form-control" >
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>직급(직책)</label> 
														<input type="text" name="t_position" class="form-control">
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>직무</label> 
														<input type="text" name="t_job" class="form-control" >
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>사무실전화</label> 
														<input type="tel" name="t_offtel" class="form-control"  onkeyup="autoHypenPhone('t_offtel')">
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 설치기사 활동지역
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
													<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>설치지역</label> 
														<select class="select select2-hidden-accessible" name="ta_city" onchange="citySelect(this.value);">
															<option>선택</option>
															<c:forEach var="city" items="${city}">
															<option id="city" value="${city.sub_loc1}">${city.sub_loc1}</option>										
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-9">
													<div class="form-group form-group-default">
														<label>상세설치지역</label> 
														<select class="select select2-hidden-accessible" id ="ta_gu" name="ta_gu"  multiple="multiple" >
															<option>선택</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row"> 
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default disabled">
														<label>ID</label>
														 <input type="text" id="t_logid" name="t_logid" class="form-control" readonly> 
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>PW</label> 
														<input type="text" name="t_pass" class="form-control" >
													</div>
												</div> 
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>PW 확인</label> 
														<input type="text" name="t_pass_check" class="form-control" >
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger" onclick="startRental()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	
	// Material Select Initialization
	$(document).ready(function() {
    $('.mdb-select').material_select();
});
	
	function startRental(){
		var param=$('form[name=frm]').serialize();
		$.ajax({
			url:'/employee/AddTechnician',
			type:'post',
			data:param
		})
		.done(function(data){
			alert(data);
			if(data.trim()=='등록 완료'){
				window.opener.refreshTable();
				window.self.close();
			}else{
				window.opener.refreshTable();
				window.self.close();
			}
					
		});
	}
	
	function del(){
	table
		.row( $(this).parents('tr') )
		.remove()
		.draw();
	}
	
		function citySelect(province){
			$.ajax({
			 type: "POST",
			 url: "/employee/district",
			 dataType:"json",
			 contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			 data: {param:province},
			 success: function(result){

			  //SELECT BOX 초기화           
			  $("#ta_gu").find("option").remove();
			  //배열 개수 만큼 option 추가
			   $.each(result, function(i){
				   var district  = result[i].replace(/\{/g,'');
				   district = district.replace(/\}/g,'');
				   district = district.replace(/\=/g,'');
				   district = district.replace('sub_loc2','');
				   $("#ta_gu").append("<option value='"+  district+"'>"+district+"</option>")
			   });    
			  },
			   error: function (jqXHR, textStatus, errorThrown) {
			   alert("오류가 발생하였습니다.");
			  }                     
			 });
			}
		
		

		//다음 주소
			function openAddressIfEmpty(){
				if($('input[name=t_addr1]').val()==''){
					openAddress();
				}
			}
			function openAddress(){
				new daum.Postcode({
				    oncomplete: function(data) {
				      	var sido=data.sido;
				    	var sigungu=data.sigungu;
				    	var roadname=data.address.replace(sido,'');
				    	roadname=roadname.trim();
				    	roadname=roadname.replace(sigungu,'');
				    	roadname=roadname.trim();
				    	var zonecode=data.zonecode;
				    	$('input[name=t_addr1]').val(sido);
				    	$('input[name=t_addr2]').val(sigungu);
				    	$('input[name=t_addr3]').val(roadname);
				    	$('input[name=t_zip]').val(zonecode);
				    }
				}).open({popupName:'daumAddr'});
			}
			
			function autoHypenPhone(col){
				  var str = $('input[name='+col+']').val();
					str = str.replace(/[^0-9]/g, '');
				  var tmp = '';
				  if( str.length < 4){
					  tmp=str;
				  }else if(str.length == 7){
				    tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3);
				  }else if(str.length == 8){
					tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 2);
				    tmp += '-';
				    tmp += str.substr(4);
				  }
				  else if(str.length == 9){
					    tmp += str.substr(0, 2);
					    tmp += '-';
					    tmp += str.substr(2, 3);
					    tmp += '-';
					    tmp += str.substr(5);
					  }
				  else if(str.length == 10){
					  tmp += str.substr(0, 2);
					    tmp += '-';
					    tmp += str.substr(2, 4);
					    tmp += '-';
					    tmp += str.substr(6);
					
				  }
				  else if(str.length >11){
					  tmp += str.substr(0, 3);
					    tmp += '-';
					    tmp += str.substr(3, 4);
					    tmp += '-';
					    tmp += str.substr(7,4);
				  }
				  
				  else{        
				    tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3, 4);
				    tmp += '-';
				    tmp += str.substr(7);
				  }
				     return $('input[name='+col+']').val(tmp);
				}
			
			function autoInputLoginId(){
				var number =  $('input[name=t_number]').val();
				$('#t_logid').val(number);
			}
			
			
	</script>
</body>
</html>