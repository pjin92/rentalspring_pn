<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>거래처관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper"> 
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">거래처관리</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
			
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
						<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
								<div class="panel-body">
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">키워드 검색</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="mainKeyword" >
							                        <!-- <span class="help-block">상품명, 모델명 등</span> -->
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">구분코드</label>
							                       <select class="form-control input-sm" name="c_category1" >
																<option value="" selected>선택</option>
																<c:forEach var="category" items="${category}">
																<option value="${category.cc_idx }">${category.cc_name}</option>
																</c:forEach>
													</select>
												</div>
											</div>
										
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group" >
													<label class="form-tit-label">등록일</label>
													<div class="form_control_1">
														<input type="text" class="form-control input-sm daterange-blank" name ="c_date" value=""> 
													</div>
												</div>
											</div>
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
												<!-- <a onclick="javascript:popAddExcel()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>엑셀</a> -->
											</div>
										</div>					
									</form>
								</div>
							</div>
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>거래처</h5>
									<div class="heading-elements">
										<div class="btn-group">
											<button type="button" class="btn bg-teal-400 btn-labeled legitRipple" onclick="addPop()"><b><i class="icon-plus3"></i></b>신규등록</button>
										</div>
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
								
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<th class="active">선택</th>
											<th class="active">등록일</th>
											<th class="active">분류</th>
											<th class="active">구분코드</th>
											<th class="active">상호법인명</th>
											
											<th class="active">사업자번호</th>
											<th class="active">전화번호</th>
											<th class="active">업체주소</th>
											<th class="active">우편번호</th>
											<th class="active">업체 담당자</th>

											<th class="active">담당자전화번호</th>
											<th class="active">상태</th>
											<th class="active">관리자성명</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div> 
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "c_id" },
		    { "data": "c_date"},
		    { "data": "ct_name"},
		    { "data": "cc_name"},
		    { "data": "c_comname"},
		    
		    { "data": "c_comnum"},
		    { "data": "c_offtel1"},
		    { "data": "c_addr"},
		    { "data": "c_zip"},
		    { "data": "c_name1"},

		    { "data": "c_tel1"},
		    { "data": "cs_name"},
		    { "data": "e_name"}
		  ]
	});
	tableLoad('checkTable');
	
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		var param=$('form[name=searchForm]').serialize();
		checkDt('checkTable','/employee/customer');
	}
	
	//재검색 입력시 기존 요청 취소
	$(document).ajaxSend(function(){
		var table=$('#checkTable').DataTable();
		table.settings()[0].jqXHR.abort();
	});
		
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#checkTable').DataTable();
		table.ajax.url('/employee/customer?'+param).load(null,false);
	} 
	
	
	function addPop(){
		window.open('/employee/customer/create','popup','width=1200,height=600');
	}
	
	function customerDetail(){
		var c_id = $(event.target).attr('c_id');
		window.open('/employee/customer/customerDetail/'+c_id,'popup','width=1200,height=600');
	}
	
	function employeeDetail(){
		var e_id = $(event.target).attr('e_id');
		window.open('/employee/employeeDetail/'+e_id,'popup','width=1200,height=600');
	}
	
	//현재 보이는 dataTable refresh
	function refreshTable(){
		var table=$('#checkTable').DataTable();
		table.ajax.reload(null,false);
	}

	
	</script>
</body>
</html>