<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>기사관리</title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
<%@include file="/WEB-INF/view/main/topbar.jsp" %>
<div class="page-container">
	<div class="page-content">
		<div class="content-wrapper">
			<%@include file="/WEB-INF/view/main/menu.jsp" %>
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
					<div class="page-title">
						<h4>
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">기사 조회</span>
							<small class="display-block"></small>
						</h4>
					</div>
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			<div class="content">
				<div class="row">
					<div class="col-xs-12">
						<!-- Search layout-->
						<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>
			
							<div class="panel-body">
							
								<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">기사성명 검색</label>
												<input type="text" class="form-control input-sm" placeholder="검색" name="mainKeyword">
<!-- 						                        <span class="help-block">상품명, 신청고객명</span> -->
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">소속 검색</label>
												<input type="text" class="form-control input-sm" placeholder="검색" name="mainKeyword">
<!-- 						                        <span class="help-block">상품명, 신청고객명</span> -->
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">상태 검색</label>
												<input type="text" class="form-control input-sm" placeholder="검색" name="mainKeyword">
<!-- 						                        <span class="help-block">상품명, 신청고객명</span> -->
											</div>
										</div>	
										<%-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
				                                    <label class="control-label col-md-5 col-lg-4 form-tit-label">대분류 선택</label>       
				                                          <select class="form-control input-sm" id="main" name="sp_mm_idx" onchange="change()">
				                                              <option value="0" selected>선택</option>
														<c:forEach items="${bsort}" var="i">
															 <option value="${i.sp_mm_idx}" ${i.sp_mm_idx eq sd.sp_mm_idx ? 'selected':''}>${i.sp_mm_name}</option>
														</c:forEach>
													</select>         
				                           	</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group" >
												<label class="form-tit-label">등록일</label>
												<div class="form_control_1">
													<input type="text" class="form-control input-sm daterange-blank" name ="sp_p_date" value=""> 
												</div>
											</div>
										</div>
									</div> --%>
				
									<div class="col-xs-12 text-right">
										<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
									</div>
								</div>					
								</form>
							</div>
						</div>
						<!-- /Search layout --> 
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>기사 조회결과</h5>
								<div class="heading-elements">
									<div class="btn-group">
										<button type="button" class="btn bg-teal-400 btn-labeled legitRipple" onclick="addPop()"><b><i class="icon-plus3"></i></b>신규등록</button>
									</div>
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>		 
							<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
								<thead>
									<tr>
										<th class="active">선택</th>
										<th class="active">기사코드</th>
										<th class="active">상태</th>
										<th class="active">기사명</th>
										<th class="active">휴대폰 번호</th> 
										<th class="active">이메일</th>
										<th class="active">소속</th>
										<th class="active">차량번호</th>
										<th class="active">설치지역</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						
							<!-- <div class="panel-footer panel-footer-transparent">			
								<a class="heading-elements-toggle"><i class="icon-more"></i></a>
								<div class="heading-elements">	
									<div class="btn-group dropup ml-20">
										<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
										<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
										<ul class="dropdown-menu">
											<li><a onclick="refreshTable('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
											<hr> 
											<li><a onclick="damdang()"><i class="icon-copy3"></i>배정</a></li>
										</ul>
									</div>
								</div>
							</div> -->
						</div>
						
					</div>
				</div>
			</div>
			<!-- /content area -->
		</div>
	</div>
</div>


<%@include file="/WEB-INF/view/main/modal.jsp"%>
<%@include file="/WEB-INF/view/main/js.jsp"%>
<script>
//dataTable 컬럼별 들어갈 data
$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "t_idx" },
	    { "data": "t_number"},
	    { "data": "t_status"},
	    { "data": "t_name"},
	    { "data": "t_mobile"},
	    
	    { "data": "t_email"},
	    { "data": "c_comname"},
 	    { "data": "t_car"},
 	    { "data": "t_activity_loc"}
	  ]
});
tableLoad('checkTable');

//첫 dataTable initialize
if(! $.fn.DataTable.isDataTable( '#checkTable' )){
	var param=$('form[name=searchForm]').serialize();
	checkDt('checkTable','/employee/tech');
}
//재검색 입력시 기존 요청 취소
$(document).ajaxSend(function(){
	var table=$('#checkTable').DataTable();
	table.settings()[0].jqXHR.abort();
});


//조회 버튼 클릭시
function searchBtn(){
	var param=$('form[name=searchForm]').serialize();
	var table=$('#checkTable').DataTable();
	table.ajax.url('/employee/tech?'+param).load(null,false);
} 
function addPop(){
	window.open('AddTechnician','popup','width=1200,height=900');
}


function technician(){
	var t_id=$(event.target).attr('t_idx');
	window.open('technician/'+t_id+'/','popup','width=1200,height=900');
}

//현재 보이는 dataTable refresh
function refreshTable(){
	var table=$('#checkTable').DataTable();
	table.ajax.reload(null,false);
}


</script>
</body>
</html>