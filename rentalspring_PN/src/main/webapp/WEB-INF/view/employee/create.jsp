<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">임직원 등록</span> <small class="display-block">임직원 등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" id="frm" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 임직원 개인정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직원성명</label> 
														<input type="text" name="e_name" value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>자택전화</label> 
														<input type="text" name="e_tell" value="" class="form-control"  onkeyup="autoHypenPhone('e_tell')" >
													</div>
												</div>
												
														
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>핸드폰번호</label> 
														<input type="text" name="e_phone" value="" class="form-control"  onkeyup="autoHypenPhone('e_phone')" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>주민등록번호</label> 
														<input type="text" name="e_jumin" value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,도)</label>
															<input type="text" class="form-control input-sm" name="e_addr1"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(시,군,구)</label>
															<input type="text" class="form-control input-sm" name="e_addr2"  onclick="openAddress()" readonly>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소(상세주소)</label>
															<input type="text" class="form-control input-sm" name="e_addr3" onclick="openAddressIfEmpty()">
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="e_zip" readonly>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 임직원 사원정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>사원번호</label> 
														<input type="text" name="e_number" value="" class="form-control" onkeyup="checkId()" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>소속</label> 
														<select	class="select select2-hidden-accessible" name="e_branch" >
																<option value="" selected>선택</option>
																<c:forEach var="branch" items="${branch}">
																<option value="${branch.c_id}">${branch.comname}</option>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>부서</label> 
														<select	class="select select2-hidden-accessible" name="e_department" >
																<option value="" selected>선택</option>
																<c:forEach var="department" items="${department}">
																<option value="${department.ed_idx}">${department.ed_name}</option>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직급(직책)</label> 
														<select	class="select select2-hidden-accessible" name="e_position" >
																<option value="" selected>선택</option>
																<c:forEach var="position" items="${position}">
																<option value="${position.ep_idx}">${position.ep_name}</option>
																</c:forEach>
														 </select>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>직무</label> 
														<select	class="select select2-hidden-accessible" name="e_job" >
																<option value="" selected>선택</option>
																<c:forEach var="job" items="${job}">
																<option value="${job.ej_idx}">${job.ej_name}</option>
																</c:forEach>
														 </select>
													</div>
												</div> 
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>이메일</label> 
														<input type="text" name="e_email" value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>전화번호<small>(사무실번호)</small></label>  
														<input type="tel" name="e_offtel" value="" class="form-control" onkeyup="autoHypenPhone('e_offtel')">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 임직원 계정정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
											
											<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>상태</label> 
														<select	class="select select2-hidden-accessible" name="e_status" >
																<option value="1" selected>정상</option>
																<option value="2">퇴사</option>
																<option value="3">사용중지</option>
														 </select>
													</div>
												</div> 
												
										
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default disabled">
														<label>로그인ID</label> 
														<input type="text" name="e_logid" value="" class="form-control" readonly >
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>패스워드</label> 
														<input type="text" name="e_pass"  value="" class="form-control" >
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>패스워드</label> 
														<input type="text" name="e_pass_check"  value="" class="form-control" >
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>권한</label> 
														<select	class="select select2-hidden-accessible" name="e_power" >
																<option value="" selected>선택</option>
																<c:forEach var="power" items="${power}">
																<option value="${power.epo_idx}">${power.epo_name}</option>
																</c:forEach>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<span id="checkIdSpan" style="color: red;"></span>
												<span id="checkPwSpan" style="color: red;"></span>
													<a class="btn btn-danger position-right" onclick="newEmployee()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
		<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	
	function startRental(){
		var param=$('form[name=frm]').serialize();
		$('#frm').ajaxForm({
			url:'/employee/create',
			type:'post',
			enctype:"multipart/form-data",
			 contentType: false,
			success:function(res){
				if(res.trim()=='등록완료'){
					alert('등록에 성공했습니다.');
					window.opener.refreshTable();
					window.close();
				}else{
					alert(res);
					window.opener.refreshTable();
					window.close();
				}
			}
		});
		$("#frm").submit() ;
	}
	
	
	//다음 주소
		function openAddressIfEmpty(){
			if($('input[name=c_addr1]').val()==''){
				openAddress();
			}
		}
		function openAddress(){
			new daum.Postcode({
			    oncomplete: function(data) {
			      	var sido=data.sido;
			    	var sigungu=data.sigungu;
			    	var roadname=data.address.replace(sido,'');
			    	roadname=roadname.trim();
			    	roadname=roadname.replace(sigungu,'');
			    	roadname=roadname.trim();
			    	var zonecode=data.zonecode;
			    	$('input[name=e_addr1]').val(sido);
			    	$('input[name=e_addr2]').val(sigungu);
			    	$('input[name=e_addr3]').val(roadname);
			    	$('input[name=e_zip]').val(zonecode);
			    }
			}).open({popupName:'daumAddr'});
		}
		
		function autoHypenPhone(col){
			  var str = $('input[name='+col+']').val();
				str = str.replace(/[^0-9]/g, '');
			  var tmp = '';
			  if( str.length < 4){
				  tmp=str;
			  }else if(str.length == 7){
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3);
			  }else if(str.length == 8){
				tmp += str.substr(0, 2);
			    tmp += '-';
			    tmp += str.substr(2, 2);
			    tmp += '-';
			    tmp += str.substr(4);
			  }
			  else if(str.length == 9){
				    tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 3);
				    tmp += '-';
				    tmp += str.substr(5);
				  }
			  else if(str.length == 10){
				  tmp += str.substr(0, 2);
				    tmp += '-';
				    tmp += str.substr(2, 4);
				    tmp += '-';
				    tmp += str.substr(6);
				
			  }
			  else if(str.length >11){
				  tmp += str.substr(0, 3);
				    tmp += '-';
				    tmp += str.substr(3, 4);
				    tmp += '-';
				    tmp += str.substr(7,4);
			  }
			  
			  else{        
			    tmp += str.substr(0, 3);
			    tmp += '-';
			    tmp += str.substr(3, 4);
			    tmp += '-';
			    tmp += str.substr(7);
			  }
			     return $('input[name='+col+']').val(tmp);
			}
		
		
		function checkId(){
			var e_number=$('input[name=e_number]').val();
			if(e_number!=''){
				$.ajax('/employee/checkId?e_number='+e_number)
				.done(function(res){
					var check=Number(res);
					if(check==0){
						$('#checkIdSpan').html('');
					}else{
						$('#checkIdSpan').html('이미 사용중인 직원번호입니다.');
					}
				});
			}else{
				$('#checkIdSpan').html('');
			}
		}
		function checkPw(){
			var e_pass=$('input[name=e_pass]').val();
			var e_pass_check=$('input[name=e_pass_check]').val();
			
			if(e_pass!=''&&e_pass_check!=''&&e_pass!=e_pass_check){
				$('#checkPwSpan').html('비밀번호가 다릅니다');
			}else{
				$('#checkPwSpan').html('');
			}
			
		}
		//등록
		function newEmployee(){
			var param=$('form[name=frm]').serialize();
			$.ajax({
				url:'/employee/create',
				type:'post',
				data:param
			}).done(function(res){
				if(res=='success'){
					alert('등록완료');
					window.opener.refreshTable('checkTable');
					window.self.close();
				}else{
					alert(res);
				}
			}).always(function(){
				$('#ceForm').unblock();
			});
		}
	</script>
</body>
</html>