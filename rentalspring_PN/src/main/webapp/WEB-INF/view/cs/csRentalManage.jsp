<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">CS 항목관리</span><small class="display-block">관리</small>
						</h4>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form id="frm" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 렌탈 상품 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<c:set var="ri" value="${rentalInfo}"></c:set>
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈 상품명</label> <input type="text" name="pr_name" value="${ri.pr_name }" class="form-control" readonly/>
														<input type="hidden" name="pr_idx" value="${ri.pr_idx }"> 
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈상품코드</label> 
														<input type="text" name="pr_rentalcode" value="${ri.pr_rentalcode }"	class="form-control" readonly/>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>생성자</label> 
														<input type="text" name="pr_creater"  value="${ri.pr_creater }" readonly class="form-control">
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>CS 상품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable" >
														<thead>
															<tr>
																<th>구분</th>
																<th>사용구분</th>
																<th>CS제목</th>
																<th>CS상세내용</th>
																<th>첨부파일</th>
																<th>주기</th>
																<th>렌탈소모품</th>
																<th>수량</th>
																<th>삭제</th>
															</tr>
														</thead>
														<tbody id="csProduct"> 
														</tbody>
														<tfoot>
															<tr>
																<td colspan="9" class="no-border"><a onclick="cslist()" class="btn btn-sm bg-orange-600">상품 추가</a></td>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger" onclick="startRental()">저장</a>
											</div>
										</div>
									</div>	
								</div>
							</div> 
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/cs/modal/csList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	var table = $('#productTable').DataTable();
	
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "csm_gubun" },
		    { "data": "csm_name" },
		    { "data": "csm_memo" },
		    { "data": "csm_sycle" },
		    { "data": "csm_count" }
		  ]
	});

	ajaxDt('table','/cs/csList');
	function cslist() {
		$('#modal_large').modal('show');
	}
		
	function refreshTable(){
		var table=$('#productTable').DataTable();
		table.ajax.reload(null,false);
	}

	 function startRental(){
		var param=$('form[name=frm]').serialize();
		var setting={
			data:param,
			url:"csMatch",
			type:"post",
			enctype:"multipart/form-data"
		};
		$.ajax(setting).done(function(data){
			if(data.trim()=='등록 완료'){
				alert('등록하였습니다.');
				opener.parent.location.reload();
				window.close();
			}else{
				alert('등록 실패');
				window.close();
			}
			
		});
    }
	
	 function del(){
		table.row($(this).parents('tr')).remove().draw();
	}
	</script>
</body>
</html>