<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">CS항목 신규 등록</span> <small class="display-block">신규 등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" id="frm" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 점검항목 설정
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-4">
													<div class="form-group form-group-default ">
														<label>렌탈상품명</label> 
														<select class="select select2-hidden-accessible" name = "csm_gubun">
															<option value="0">선택</option>
															<c:forEach var="rp" items="${rp}">
																<option value="${rp.pr_idx }">${rp.pr_name }</option>
															</c:forEach>
														</select> 
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-4">
													<div class="form-group form-group-default ">
														<label>사용구분</label> 
														<select class="select select2-hidden-accessible" name = "csm_status">
															<option value="1" selected>사용</option>
															<option value="2">미사용</option>
														</select>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-4">
													<div class="form-group form-group-default ">
														<label>점검항목설명</label> 
														<select class="select select2-hidden-accessible" name = "csm_rental">
															<option value="2"selected>설치</option>
															<option value="1">배송</option>
															<option value="3">기타</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. CS 소모품구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable" >
														<thead class="alpha-slate">
															<tr>
																<th>상품명</th>
																<th>상품코드</th>
																<th>매입처</th>
																<th>원가</th>
																<th>판매가</th>
																<th>수량</th>
																<th>삭제</th>
															</tr>
														</thead>
														<tbody id="rproduct">
															<tr style="display:none">
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
														</tbody>
														<tfoot>
															<tr>
																<td colspan="7" class="no-padding no-border"><a onclick="plist()" class="btn btn-sm bg-orange-600 mt-10">상품 추가</a></td>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. CS상세정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-4">
													<div class="form-group form-group-default ">
														<label>CS명</label> 
														<input type="text" name="csm_name" class="form-control" >
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-4">
													<div class="form-group form-group-default">
														<label>cs주기</label> 
														<input type="text" name="csm_cycle" value="0" onkeyup="numKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												<div class="col-xs-6 col-sm-3 col-md-4">
													<div class="form-group form-group-default">
														<label>CS공임</label> <input type="text" name="csm_installpee" value="0" onkeyup="numCommaKeyUp()"
															class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
												
												
												<div class="col-xs-6 col-sm-3 col-md-4">
													<div class="form-group form-group-default ">
													<label>첨부파일</label> 
														<input type="file" name="csm_file" id="csm_file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>
													</div>
												</div> 
												
												<div class="col-xs-6 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
													<label>메모</label> 
														<input type="text" name="csm_memo" class="form-control">
													</div>
												</div>
											</div>
											<div class="panel-footer">
												<div class="heading-elements">
													<div class="heading-btn pull-right">
														<button type="button" class="btn btn-default" onclick="window.close()">닫기</button>
														<a class="btn btn-danger position-right" onclick="startRental()">등록</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<!-- file input -->
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<script type="text/javascript" src="/assets/js/pages/uploader_bootstrap.js"></script>	
	<script>
	var table = $('#productTable').DataTable();
	
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "p_name" },
		    { "data": "p_code" },
		    { "data": "c_comname" },
		    { "data": "p_cost_f" },
		    { "data": "p_price_f" }
		  ]
		});

		ajaxDt('table','/product/csProductList');
	function plist() {
			$('#modal_large').modal('show');
		}
		
	function refreshTable(){
		var table=$('#productTable').DataTable();
		table.ajax.reload(null,false);
	}

	function startRental(){
		var param=$('form[name=frm]').serialize();
		$('#frm').ajaxForm({
			url:'/cs/csProduct',
			type:'post',
			enctype:"multipart/form-data",
			 contentType: false,
			success:function(res){
				if(res.trim()=='등록완료'){
					alert('등록에 성공했습니다.');
					window.opener.refreshTable();
// 					window.close();
				}else{
					alert(res);
					window.opener.refreshTable();
					window.close();
				}
			}
		});
		$("#frm").submit() ;
	}
	
	
	function del(){
	table
		.row( $(this).parents('tr') )
		.remove()
		.draw();
	}
	</script>
</body>
</html>