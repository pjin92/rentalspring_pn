<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<div class="page-title">
							<h4>
								<i class="icon-address-book2 position-left"></i> <span
									class="text-bold">렌탈 상품 수정</span> <small class="display-block">수정</small>
							</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				
					<div class="content">
						<div class="row">
							<c:set var="rp" value="${plist}"></c:set>
						<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 렌탈 상품 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈 상품명</label> <input type="text" name="pr_name" value="${rp.pr_name }"
															class="form-control" readonly/>
														
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈상품코드</label> <input type="text" name="pr_rentalcode"  value="${rp.pr_rentalcode }"
															class="form-control" readonly/>
													</div>
												</div>
												
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>생성자</label> 
														<input type="text" name="pr_creater" value="${rp.pr_creater}" readonly
															class="form-control">
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>렌탈 상품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable" >
														<thead>
															<tr>
																<th>상품명</th>
																<th>상품코드</th>
																<th>매입처</th>
																<th>원가</th>
																<th>판매가</th>
																<th>수량</th>
															</tr>
														</thead>
														<tbody id="rproduct">
														
														<c:forEach var="rpd" items="${data}">
															<tr>
															<td>${rpd.p_name}
															<input type="hidden" name="p_id${rpd.p_id}" value="${rpd.p_id}">
															</td>
															<td>${rpd.p_code}</td>
															<td>${rpd.p_category1}</td>
															<td>${rpd.p_cost}</td>
															<td>${rpd.p_price }</td>
															<td>${rpd.prp_num}</td>
															</tr>
														</c:forEach>
														
														</tbody>
															<!-- <tr>
																<td colspan="6"><a onclick="plist()">상품 추가</a>
															</td>
															</tr> -->
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
<form role="form" name="frm" method="post"
					enctype="multipart/form-data" >
						
							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 렌탈 상품 가격 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈기간</label> <input type="text" name="pr_period" value="${rp.pr_period}" onkeydown="numKeyDown()"
															class="form-control" readonly/>
																<input type="hidden" name="pr_idx" value="${rp.pr_idx }">
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈총액</label> <input type="text" name="pr_total" value="${rp.pr_total}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>월렌탈료</label> <input type="text" name="pr_rental" value="${rp.pr_rental}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()" class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>일시불판매가</label> <input type="text" name="pr_ilsi" value="${rp.pr_ilsi}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>제휴카드</label> <input type="text" name="pr_aff" value="${rp.pr_aff}" 
															class="form-control" readonly/>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>제휴카드 할인가</label> <input type="text" name="pr_aff_cost" value="${rp.pr_aff_cost}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							
							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 배송 / 설치비
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>배송비(기본)</label> <input type="text" name="pr_delivery1" value="${rp.pr_delivery1}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()" class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>배송비 (도서,산간)</label> <input type="text" name="pr_delivery2" value="${rp.pr_delivery2}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>설치비</label> <input type="text" name="pr_install1" value="${rp.pr_install1}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>설치비(도서,산간)</label> <input type="text" name="pr_install2" value="${rp.pr_install2}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 이벤트 할인
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>구분</label> 	
														 
														 <c:choose>
														 <c:when test="${rp.pr_gubun eq 1 }">
														  <select name="pr_gubun"	class="form-control">
														 <option value="0" > 선택 </option>
														 <option value ="1" selected>가격할인 </option>
														 <option value ="2">할인률</option>
														 <option value ="3">렌탈료 면제 </option>
														 </select>
														 </c:when>
														 <c:when test="${rp.pr_gubun eq 2}">
														  <select name="pr_gubun"	class="form-control">
														 <option value="0" > 선택 </option>
														 <option value ="1">가격할인 </option>
														 <option value ="2" selected>할인률</option>
														 <option value ="3">렌탈료 면제 </option>
														 </select>
														 </c:when>
														 <c:when test="${rp.pr_gubun eq 3 }">
														  <select name="pr_gubun"	class="form-control">
														 <option value="0" > 선택 </option>
														 <option value ="1">가격할인 </option>
														 <option value ="2">할인률</option>
														 <option value ="3" selected>렌탈료 면제 </option>
														 </select>
														 </c:when>
														 
														 <c:otherwise>
														 	  <select name="pr_gubun"	class="form-control">
														 <option value="0" selected> 선택 </option>
														 <option value ="1">가격할인 </option>
														 <option value ="2">할인률</option>
														 <option value ="3" >렌탈료 면제 </option>
														 </select>
														 </c:otherwise>
														 </c:choose>
														
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈가격할인</label>
														 <input type="text" name="pr_rentalDC" value="${rp.pr_rentalDC}" onkeydown="numKeyDown()"
															 onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>할인률</label> <input type="text" name="pr_rentalDCp" value="${rp.pr_rentalDCp}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈료 면제</label> <input type="text" name="pr_rentalEXE" value="${rp.pr_rentalEXE}" onkeydown="numKeyDown()"
															onkeyup="numCommaKeyUp()"	class="form-control" readonly/>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>6. 변경사유
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
														 <c:set var="pr" value="${writer}"></c:set>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>최종변경자</label>
														<input type="text" name="pr_modifier" value="${pr.e_name}" readonly
															class="form-control">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default ">
														<label>최종변경일</label> <input type="text" name="pr_log" value="${rp.pr_log}"
															class="form-control" readonly/>
													</div>
												</div>

												<div class="col-xs-6 col-sm-12 col-md-12">
													<div class="form-group form-group-default ">
														<label>변경사유</label> <input type="text" name="pr_mod_reason" value="${rp.pr_mod_reason}"
															class="form-control" readonly/>
													</div>
												</div>
											</div>
										</div>
									</div>
						<div class="panel-footer"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
							<div class="heading-elements">
							<button type="button" class="btn btn-sm btn-default ml-20"
											onclick="window.close()">닫기</button>
								<div class="heading-btn pull-right">
									<a class="btn btn-sm btn-danger" onclick="startRental()">수정</a>
								</div>
							</div>
						</div>	
								</div>
							</div>
						</form>
						</div>
					</div>
			
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
		
	
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "p_name" },
		    { "data": "p_code" },
		    { "data": "p_category1" },
		    { "data": "p_cost" },
		    { "data": "p_price" }
		  ]
		});

		ajaxDt('table','/product/productList');
	function plist() {
			$('#modal_large').modal('show');
		}
		
	function refreshTable(){
		var table=$('#productTable').DataTable();
		table.ajax.reload(null,false);
	}

	 function startRental(){
	    	var btn=$(event.target).html();
	    	var bttn = btn.substring(0,2);
	    	if(bttn=='확인'){
	    		if(window.confirm('수정하시겠습니까?')){
	    			var param=$('form[name=frm]').serialize();
	    			var setting={
	    				data:param,
	    				url:"productRentalModify",
	    				type:"post",
	    				enctype:"multipart/form-data"
	    			};
	    			$.ajax(setting).done(function(data){
	    				if(data.trim()=='등록 완료'){
	    					alert('수정하였습니다.');
							opener.parent.location.reload();
	    					window.close();
	    				}else{
	    					alert('수정 실패');
	    					window.close();
	    				}
	    				
	    			});
	    		}
	    	}else{
	    		$(event.target).html('확인');
	    		$(event.target).removeClass('blue-hoki');
	    		$(event.target).addClass('btn-danger');
	    		
	    		$('form[name=frm] input').removeAttr('readonly');
	    	}
	    	
	    }
	
			
	</script>
</body>
</html>