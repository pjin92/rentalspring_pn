<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">LED 렌탈 상품 등록</span> <small class="display-block">등록</small> 
						</h4>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 렌탈 상품 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈 상품명</label> 
														<input type="text" name="pr_name" class="form-control" ><!-- onkeyup="productDuplicate()" -->

													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈상품코드</label> 
														<input type="text" name="pr_rentalcode" class="form-control" placeholder="상품등록 후 생성됩니다." readonly>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>생성자</label>
														<c:set var="pr" value="${writer}"></c:set>
														<input type="text" name="pr_creater" value="${pr.e_name}" readonly class="form-control">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> 
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 렌탈 상품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable">
														<thead class="alpha-slate">
															<tr>
																<th>상품명</th>
																<th>렌탈료</th>
																<th>렌탈기간</th>
																<th>설치비</th>
																<th style="width: 10%">수량</th>
																<!-- <th>삭제</th> -->
															</tr>
														</thead>
														<tbody id="rproduct">
															<c:forEach var="cal" items="${cal}" varStatus="status">
															<tr>
																<td>${cal.p_name}</td>
																<td>${cal.p_price }</td>
																<td>${cal.p_rent_term}</td>
																<td>${cal.p_deliverycost }</td>
																<td>
																	<input type="text" class="form-control input-sm text-center" name="prp_num${cal.p_id}" onkeydown="numKeyUp()" onkeyup="javascript:cal('${cal.p_price}', '${cal.p_id}','${cal.p_deliverycost}');numCommaKeyUp();" value="0"> 
																	<input type="hidden" class="form-control input-sm text-center" id="${cal.p_id}no" name="${cal.p_id}no" value="0" onkeydown="numKeyUp()" onkeyup="numCommaKeyUp();">
																</td>												
																<!-- <td>
																	<input type="button" onclick="del()" value="삭제">
																</td>  -->
															</tr>
															</c:forEach>
														</tbody>
														<!-- <tfoot>
															<tr>
																<td colspan="9" class="no-padding no-border"><a onclick="plist()" class="btn btn-sm bg-orange-600">상품 추가</a></td>
															</tr>
														</tfoot> -->
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> 
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 렌탈 상품 가격 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈기간</label> 
														<input type="text" name="pr_period" value="39" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()" readonly>
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>설치비</label> 
														<input type="text" id="pr_install" name="pr_install" value="0" onkeyup="rental();numCommaKeyUp();" class="form-control text-right" onkeydown="numKeyDown()" >
													</div>
														<input type="hidden" id="num_temp" name="num_temp" value="0" onkeydown="numKeyDown()" readonly>
														<input type="hidden" id="install_temp" name="install_temp" value="0" onkeydown="numKeyDown()" readonly>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>렌탈총액</label> 
														<input type="text" id="pr_total" name="pr_total" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()">
													</div>
													<input type="hidden" id="temp" name="temp" value="0" onkeydown="numKeyDown()" readonly>
													<input type="hidden" id="tempTotal" name="tempTotal" value="0" onkeydown="numKeyDown()" readonly>
													
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>월렌탈료</label> 
														<input type="text" id="pr_rental" name="pr_rental" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()" readonly>
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>일시불판매가</label> 
														<input type="text" name="pr_ilsi" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>제휴카드</label> 
														<input type="text" name="pr_aff" class="form-control text-right">
													</div>
												</div>
												<div class="col-xs-6 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>제휴카드 할인가</label> 
														<input type="text" name="pr_aff_cost" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 배송 / 설치비
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>배송비(기본)</label> 
														<input type="text" name="pr_delivery1" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>배송비 (도서,산간)</label> 
														<input type="text" name="pr_delivery2" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>설치비</label> 
														<input type="text" name="pr_install1" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>설치비(도서,산간)</label> 
														<input type="text" name="pr_install2" value="0" onkeyup="numCommaKeyUp()" class="form-control text-right" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> 
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>5. 이벤트 할인
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>구분</label> 
														<select name="pr_gubun" onchanged="select()" class="select select2-hidden-accessible">
															<option value="0" selected>선택</option>
															<option value="1">가격할인</option>
															<option value="2">할인률</option>
															<option value="3">렌탈료 면제</option>
														</select>
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>렌탈가격할인</label> 
														<input type="text" id="pr_rentalDC" name="pr_rentalDC" value="0" onkeyup="discount();numCommaKeyUp();" class="form-control text-right" onkeydown="numKeyDown()"> 
														<input type="hidden" id="dc" name="dc" value="0">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>할인률</label> 
														<input type="text" name="pr_rentalDCp" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>

												<div class="col-xs-6 col-sm-3 col-md-3">
													<div class="form-group form-group-default">
														<label>렌탈료 면제 개월수</label> 
														<input type="text" name="pr_rentalEXE" value="0" onkeyup="numCommaKeyUp()" class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger pull-right" onclick="startRental()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	/**상품명 중복방지*/
	
	function productDuplicate(){
		var pr_name = $('input[name=pr_name]').val();
		var gubun = '2';	
		$.ajax({
			url:'duplicate/',
			data:{'pr_name':pr_name,'gubun':gubun},
			type:'get',
		}).done(function(data){
			if(data.trim()=='중복'){
				alert('상품명이 중복됩니다.')
			}
		});
		
	}
	//등록
		function startRental() {
			var param = $('form[name=frm]').serialize();
			if($('input[name=pr_name]').val() == 0){
				alert("상품명을 입력해주세요.");
				return false;
			}
			$.ajax({
				url : 'calculate',
				type : 'post',
				data : param
			}).done(
					function(data) {
						if (data.trim() == '등록실패') {
							alert("등록 실패하였습니다.");
							window.close();
							window.opener.refreshTable();
						} else {
							alert("상품이 등록되었습니다.");
							if (confirm("렌탈코드는 " + data + " 입니다. 복사하시겠습니까?")) {
								window.prompt(
										"렌탈코드 복사하기: Ctrl+C를 누른 후  확인을 눌러주세요.",
										data);
								alert("렌탈코드가 복사되었습니다.")
								window.opener.refreshTable();
								window.close();
							} else {
								window.opener.refreshTable();
								window.close();
							}
						}
					});
		};

		function cal(pri, pid, ins) {

			var a = onlyNum(pri);
			var b = onlyNum(ins);
			var pee= Number(10000);
			
			var price = a.replace(/,/g, "");
			var install = b.replace(/,/g, "");

			var num = $('input[name=prp_num' + pid + ']').val(); // 입력된 갯수
			var no = $('input[name=' + pid + 'no]').val(); // 임시저장갯수
			var total = price * num; // 가격 * 갯수
			var temp2 = $('#temp').val(); // 토탈가격 임시 저장

			var num_temp = $('input[name=num_temp]').val(); // 전체갯수 임시저장
			var install_temp = $('input[name=install_temp]').val(); //설치비 임시저장
			
			
			if (isNaN(temp2)) {
				alert("숫자만 입력하세요");
				$("#temp").val("0");
				$('input[name=prp_num' + pid + ']').val("0"); // 갯수 받아오기
				$('input[name=' + pid + 'no]').val("0"); // 갯수 저장하기
			}
			var temp = Number(total) + Number(temp2); //현재 입력된 상품가격*갯수 + 토탈가격 임시저장

		 	if (num == "0") { //입려된 갯수가 0일때
		 		num_temp = Number(num_temp) - Number(no); //전체갯수임시저장 = 전체갯수 - 임시저장갯수
				if(num_temp==0){	//전체갯수임시저장이 0이면
					var install =  Number(0);	//설치비는 0	
				}else{
					var install = Number(Number(pee*2)+(pee*Number(num_temp))); //설치비= 20000+(10000*임시저장갯수)
				}
		 		var tempTotal = Number(temp) - Number(price * no) - Number(install_temp)+ Number(install); //토탈가격 = (현가격+임시가격)-전갯수가격 - 전설치비 + 현설치비
				var result = comma(Number(tempTotal).toFixed(0));
				$('input[name=' + pid + 'no]').val(num);
				$('#pr_install').val(comma(install));
				$('#pr_total').val(result);
				$("#temp").val(tempTotal);
				$('#num_temp').val(num_temp);
				$('#install_temp').val(install);
				$('#pr_rental').val(comma(Number(tempTotal / 39).toFixed(0)));
			
		 	
		 	} else if (num != no) {
		 		
				num_temp = Number(num_temp) - Number(no); //전체갯수 - 구갯수
				num_temp = Number(num_temp)+ Number(num); //전체갯수 + 입력받은 갯수
				
				
		 		var install = Number((pee*2)+Number(pee*num_temp)); // 설치비 = 20000*(10000* 전제+입력갯수)
				
		 		var tempTotal = Number(temp) - Number(price * no) - Number(install_temp); //(현상품가격*현갯수) - (현상품가격*구갯수)- 설치비
				if (isNaN(tempTotal)) {
					alert("숫자만 입력하세요");
					$('input[name=' + pid + 'num]').val("0"); // 갯수 받아오기
					$('input[name=' + pid + 'no]').val("0"); // 갯수 저장하기
					$("#temp").val("0");
				} else {
					
					var install = Number(Number(pee*2)+(pee*Number(num_temp))); //설치비 = 20000*(10000* 전제+입력갯수)
					tempTotal = Number(tempTotal) + Number(install); //현재가격 + 설치비
					var result = comma(Number(tempTotal).toFixed(0));
					
					$('input[name=' + pid + 'no]').val(num);
					$('#pr_total').val(result);
					$('#pr_install').val(comma(install));
					$("#temptotal").val(tempTotal);
					$('#num_temp').val(num_temp);
					$("#temp").val(tempTotal);
					$('#install_temp').val(install);
					$('#pr_rental').val(comma(Number(tempTotal / 39).toFixed(0)));
					if(num_temp =='0'){
						$('#pr_rental').val(0);
						$("#temp").val(0);
						$('#pr_total').val(0);
						$('#pr_install').val(0);
						$('#install_temp').val(0);
					}
				}
			} 
		};
		
		function rental(){
			var total = $('input[name=temp]').val();
			var ins_temp = $('input[name=pr_install]').val();
			var install = ins_temp.replace(/,/g, "");
			var i_temp = $('input[name=temp_install]').val();
			if(install=='0'){
				
			}else {
				var tempTotal = Number(total) - Number(i_temp);
				var temp = Number(total) + Number(install);
				
				$('#pr_total').val(comma(Number(temp).toFixed(0)));
				$('#pr_rental').val(comma(Number(temp / 39).toFixed(0)));
				$('#temp_install').val(install);
				
				
			}
			
		}

		//할인금액 적용
		function discount() {
			var pr_rental = $('#pr_rental').val(); //렌탈 총액
			var pr_rentalDC = onlyNum($('#pr_rentalDC').val());

			var rental = pr_rental.replace(/,/g, "");
			var pee = $('#pr_rentalDC').val();
			var dis_pee = pee.replace(/,/g, "");
			var dis = "";
			var pre = $("#dc").val();
			var pre_dis = Number(pre);

			if (pre_dis == '0') {
				dis = Number(rental) - Number(dis_pee);
			} else {
				dis = Number(rental) - Number(dis_pee) + Number(pre_dis);
			}
			var result = comma(Number(dis).toFixed(0));
			$('#pr_rental').val(result);
			$('#dc').val(dis_pee);
		}
	</script>
</body>
</html>