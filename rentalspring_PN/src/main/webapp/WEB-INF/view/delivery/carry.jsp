<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>배송관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">고객 조회</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body"> 
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">상품</label>
														<select name="pr_name" class="form-control input-sm">
															<c:choose>
																<c:when test="${not empty login.clogin_pr_name}">
																	<option>${login.clogin_pr_name}</option>
																</c:when>
																<c:otherwise>
																	<option>S-트레이너</option>
																	<option>닥터레이디</option>
																	<option>항균커튼</option>
																</c:otherwise>
															</c:choose>
														</select>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">구분</label>
														<select name="ds_state" class="form-control input-sm">
															<option value="">전체</option>
															<option>배송전</option>
															<option>배송요청</option>
															<option>배송완료</option>
															<option>반품요청</option>
															<option>반품완료</option>
															<option>해지반품요청</option>
															<option>해지반품완료</option>
															<option>회수요청</option>
															<option>회수완료</option>
														</select>
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">판매처</label>
														<input type="text" name="m_sale" class="form-control input-sm">
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">판매기수</label>
														<input type="text" class="form-control input-sm" name="mg_name">
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">고객 회원번호</label>
														<input type="text" class="form-control input-sm" name="m_num">
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">수령고객</label>
														<input type="text" class="form-control input-sm" name="mi_name">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">Serial No.</label>
														<input type="text" class="form-control input-sm" name="ds_serial">
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">수령인 주소</label>
														<input type="text" class="form-control input-sm" name="mi_addr">
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">택배사</label>
														<input type="text" class="form-control input-sm" name="ds_company">
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">송장번호</label>
														<input type="text" class="form-control input-sm" name="ds_num">
													</div>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> 
												<div class="form-group">
													<label class="form_control_1">출고 요청일</label>
													<input type="text" class="form-control input-sm daterange-blank" name="ds_req">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> 
												<div class="form-group">
													<label class="form_control_1">배송 완료일</label>
													<input type="text" class="form-control input-sm daterange-blank" name="ds_date">  
												</div>
											</div> 
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>					
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>고객 조회결과</h5>
									<div class="heading-elements">
										<div class="btn-group">
											<button type="button" class="btn bg-slate-400 pull-right" href="/file/delivery/carry/excel">엑셀 양식 다운로드</button>
										</div>
										<div class="btn-group">
											<button type="button" class="btn bg-teal-400 pull-right" onclick="openExcel()">택배 엑셀 업로드</button>
										</div>
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		
	
								<table class="table table-hover nowrap" id="dTable" data-page-length="25">
									<thead>
										<tr>
											<th class="active" > 구분 </th>
											<th class="active" > 기수 </th>
											<th class="active" > 상품명</th>
											<th class="active" > 수량</th>
											<th class="active" > 고객 회원번호 </th>
											<th class="active" > 수령고객</th>
											<th class="active" > 수령자 연락처</th>
											<th class="active" > 수령자 주소 </th>
											<th class="active" > 출고 요청일 </th>
											<th class="active" > 배송 완료일 </th>
											<th class="active" > 택배사 </th>
											<th class="active" > 송장번호 </th>
											<th class="active" > 배송 메시지 </th>
											
											<th class="active" > 회수/반품 요청일 </th>
											<th class="active" > 회수/반품 택배사 </th>
											<th class="active" > 회수/반품 송장번호 </th>
											<th class="active" > 회수/반품 완료일 </th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="refreshTable('dTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
		</div>
	</div>
</div>
	
	
	<%@include file="modal/carryExcel.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "ds_state" },
		    { "data": "mg_name"},
		    { "data": "pr_name"},
		    { "data": "prp_num"},
		    { "data": "m_num"},
		    
		    { "data": "mi_name"},
		    { "data": "mi_phone"},
		    { "data": "mi_addr"},
		    { "data": "ds_req"},
		    { "data": "ds_date"},
		    { "data": "ds_company"},
		    { "data": "ds_num"},
		    { "data": "mi_memo"},
		    
		    { "data": "ds_req_cancel", "defaultContent":""},
		    { "data": "ds_company_cancel", "defaultContent":""},
		    { "data": "ds_num_cancel", "defaultContent":""},
		    { "data": "ds_date_cancel", "defaultContent":""}
		  ]
	});
	tableLoad('dTable');
	
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#dTable' )){
		var param=$('form[name=searchForm]').serialize();
		ajaxDt('dTable','/delivery/carry?'+param);
	}
	
	//재검색 입력시 기존 요청 취소
	$(document).ajaxSend(function(){
		var table=$('#dTable').DataTable();
		table.settings()[0].jqXHR.abort();
	});
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#dTable').DataTable();
		table.ajax.url('/delivery/carry?'+param).load(null,false);
	} 
	/* function member(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/contract/'+m_idx+'/','popup','width=1200,height=600');
	} */
	function openExcel(){
		$('#uploadInvoice')[0].reset();
		$('#modal_large').modal('show');
	}
	</script>
</body>
</html>