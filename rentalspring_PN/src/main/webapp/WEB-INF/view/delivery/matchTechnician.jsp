<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">렌탈 상품 등록</span> <small class="display-block">등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 추천설치기사목록
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													
													
													<table class="table table-hover" id="productTable">
														<thead class="alpha-slate">
															<tr>
																<th>번호</th>
																<th>기사명</th>
																<th>활동지역</th>
																<th>세부활동지역</th>
																<th>재고보기</th>
															</tr>
														</thead>
														<tbody id="rproduct">
															<c:forEach var="clist" items="${clist}" varStatus="status">
															<tr>
																<td>
																	${status.count}
																	<input type="hidden" name="m_idx" value="${clist.m_idx }"/>
																	<input type="hidden" name="mi_idx" value="${clist.mi_idx }">
																</td>
																<td>${clist.e_name }</td>
																<td>${clist.e_activity_loc}</td>
																<td>${clist.e_activity_dis}</td>
																<td>${clist.stock }</td>
															</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 기타설치기사목록
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													
													
													<table class="table table-hover" id="productTable">
														<thead class="alpha-slate">
															<tr>
																<th>번호</th>
																<th>기사명</th>
																<th>활동지역</th>
																<th>세부활동지역</th>
																<th>재고보기</th>
															</tr>
														</thead>
														<tbody id="rproduct">
															<c:forEach var="tlist" items="${tlist}" varStatus="status">
															<tr>
																<td>
																	${status.count}
																	<input type="hidden" name="m_idx" value="${tlist.m_idx }"/>
																	<input type="hidden" name="mi_idx" value="${tlist.mi_idx }">
																</td>
																<td>${tlist.e_name }</td>
																<td>${tlist.e_activity_loc}</td>
																<td>${tlist.e_activity_dis}</td>
																<td>${tlist.stock }</td>
															</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="heading-btn pull-right">
									<a class="btn btn-danger pull-right" onclick="mathchCancel()">미배정</a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/delivery/modal/techStock.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	
		//등록
		function addMatchTechnician() {
			var eid = $(event.target).attr('e_id');
			var name = $(event.target).attr('e_name');
			var m_idx = $('input[name=m_idx]').val();
			var mi_idx = $('input[name=mi_idx]').val();
			
			if (confirm(name + '(으)로 배정하시겠습니까?')) {

				 $.ajax({
					url:'/delivery/matchTechnician',
					type:'post',
					data:{'eid':eid,'m_idx':m_idx,'mi_idx':mi_idx}
				})
				 .done(function(data){
					if(data.trim()=='등록 완료'){
						alert('등록하였습니다.');
						opener.parent.refreshTable();
						window.self.close();
					}else{
						alert(data.trim());
					} 
							
				}); 

			} else {
				return false;
			}
		};

		function showTechStock() {
			var e_id = $(event.target).attr('e_id');
	
			$('#stock').empty();
			$.ajax({
				url : '/stock/techStock/' + e_id,
				type : 'get',
				success : function(data) {
					$('#modal_large').modal('show');
					$('#stock').append(data);
				}

			});
		}
		
		function mathchCancel(){
			var m_idx = $('input[name=m_idx]').val();
			if(confirm('미배정 상태로 변경하시겠습니까?')){
				$.ajax({
					url:'/install/matchTechnician/'+m_idx,
					type:'delete'
				}).done(function(data){
					if(data.trim()=='삭제완료'){
						alert("미배정 상태로 변경되었습니다.");
						opener.parent.refreshTable();
						window.self.close();
					}else{
						alert("미배정 상태로 변경 실패하였습니다.")
					}
				}); 
			}else{
				return false;
			}
			
		}
	</script>
</body>
</html>