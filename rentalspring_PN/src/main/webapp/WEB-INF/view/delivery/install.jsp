<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>설치관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">설치관리</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body"> 
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">설치 상태</label>
														<select name="ds_state" class="form-control input-sm">
															<option value="">전체</option>
															<option>설치전</option>
															<option>설치요청</option>
															<option>설치완료</option>
														</select>
													</div>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">고객 회원번호</label>
														<input type="text" class="form-control input-sm" name="m_num">
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">설치고객</label>
														<input type="text" class="form-control input-sm" name="mi_name">
													</div>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">렌탈 상품명</label>
														<input name="pr_name" class="form-control input-sm">
													</div>
												</div>
											</div>
	
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">설치 주소</label>
														<input type="text" name="mi_addr" class="form-control input-sm">
													</div>
												</div>
											</div>
	
										</div>
										
										<div class="row">
										
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">상담사</label>
														<input type="text" class="form-control input-sm" name="m_damdang">
													</div>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-offset-0 col-lg-2">
												<div class="form-group">
													<div class="input-icon right">
														<label for="form_control_1">설치기사</label>
														<input type="text" class="form-control input-sm" name="i_e_name">
													</div>
												</div>
											</div>
	
											
	
											
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> 
												<div class="form-group">
													<label class="form_control_1">상담 완료일</label>
													<input type="text" class="form-control input-sm daterange-blank" name="m_sangdam_finish">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> 
												<div class="form-group">
													<label class="form_control_1">설치 예정일</label>
													<input type="text" class="form-control input-sm daterange-blank" name="ds_req">
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2"> 
												<div class="form-group">
													<label class="form_control_1">설치 완료일</label>
													<input type="text" class="form-control input-sm daterange-blank" name="ds_date">
												</div>
											</div>
	
											
											
											
											<div class="text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>					
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>고객 조회결과</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		
	
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<th class="active">설치상태</th>
											<th class="active">상담완료일</th>
											<th class="active">상담사</th>
											<th class="active">설치요청일</th>
											<th class="active">설치 완료일</th>
											<th class="active">판매처</th>
											<th class="active">판매기수</th>
										
											<th class="active">설치상품</th>
											<th class="active">설치고객명</th>
											<th class="active">고객번호</th>
											<th class="active">연락처(설치)</th>
											<th class="active">주소(설치)</th>
											<th class="active">접수구분</th>
										
											<th class="active">매출구분</th>
											<th class="active">상태구분</th>
											<th class="active">설치(방문)전달사항</th>
											<th class="active">설치(방문)담당자</th>
											<th class="active" data-orderable="false">설치확인서</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<!-- <li><a onclick="productDel('display', '1')"><i class="icon-eye"></i>진열</a></li>
												<li><a onclick="productDel('display', '0')"><i class="icon-eye-blocked"></i>미진열</a></li>
												<hr>
												<li><a onclick="productDel('sel', '1')">판매중</a></li>
												<li><a onclick="productDel('sel', '0')">판매안함</a></li>
												<hr> 
												<li><a onclick="productDel('copy', 'copy')"><i class="icon-copy3"></i>복제</a></li>
												<li><a onclick="productDel('del', 'del')"><i class="icon-trash-alt"></i>삭제</a></li> -->
												<li><a onclick="refreshTable()"><i class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	
	<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "ds_state" },
		    { "data": "m_sangdam_finish"},
		    { "data": "e_name"},
		    { "data": "ds_req"},
		    { "data": "ds_date"},
		    { "data": "m_sale"},
		    { "data": "mg_name"},
		  
		    { "data": "pr_name"},
		    { "data": "mi_name"},
		    { "data": "m_num"},
		    { "data": "mi_phone"},
		    { "data": "mi_addr"},
		    { "data": "m_jubsoo"},
		
		    { "data": "m_detail"},
		    { "data": "m_process"},
		    { "data": "mi_memo"},
		    { "data": "i_e_name"},
		    { "data": "mf_file"}
		    
		  ]
	});
	tableLoad('checkTable');
	
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		var param=$('form[name=searchForm]').serialize();
		ajaxDt('checkTable','/delivery/install?'+param);
	}
	
	//재검색 입력시 기존 요청 취소
	$(document).ajaxSend(function(){
		var table=$('#checkTable').DataTable();
		table.settings()[0].jqXHR.abort();
	});
	
	function installComplete(){
		var m_idx =$(event.target).attr('m_idx');
		var mi_idx = $(event.target).attr('mi_idx');
		window.open('/install/addComplete?m_idx='+m_idx+'&mi_idx='+mi_idx ,'popup','width=700,height=400');
		
	}

	function rentalProduct(){
		var pr_idx=$(event.target).attr('pr_idx');
		window.open('/product/rentalProductComp/'+pr_idx,'popup','width=1400,height=800');
	}
	
	function technician(){
		var m_idx =$(event.target).attr('m_idx');
		var mi_idx = $(event.target).attr('mi_idx');
		window.open('/delivery/matchTechnician?m_idx='+m_idx+'&mi_idx='+mi_idx ,'popup','width=700,height=600');
		
	}

	function member(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/contract/'+m_idx+'/','popup','width=1200,height=600');
	}

	//현재 보이는 dataTable refresh
	function refreshTable(){
		var table=$('#checkTable').DataTable();
		table.ajax.reload(null,false);
	}

	function addPop(){
		window.open('/customer/','popup','width=1200,height=600');
	}

	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#checkTable').DataTable();
		table.ajax.url('/delivery/install?'+param).load(null,false);
	} 

	function rentalProduct(){
		var pr_code=$(event.target).attr('pr_rentalcode');
		window.open('/product/rentalProductComp/'+pr_code,'popup','width=1400,height=800');
	}

	function installComplete(){
		var m_idx =$(event.target).attr('m_idx');
		var mi_idx = $(event.target).attr('mi_idx');
		window.open('/install/addComplete?m_idx='+m_idx+'&mi_idx='+mi_idx ,'popup','width=700,height=400');
	}

	

	</script>
</body>
</html>