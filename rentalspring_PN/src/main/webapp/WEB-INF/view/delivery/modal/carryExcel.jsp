<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Large modal -->
<div id="modal_large" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
                       <div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="modal_large_title">엑셀 업로드</span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<!-- Column selectors -->
				<div class="panel" id="modal_large_panel">
					<table class="table table-bordered">
						<tr>
							<th class="active" colspan="2">
								택배 엑셀 업로드
							</th>
						</tr>
						<tr>
							<td>
								<form name="uploadInvoice" id="uploadInvoice">
									<input class="form-control input-sm" type="file" name="deliveryFile">
								</form>
							</td>
							<th class="active"><a onclick="uploadCarryExcel()" class="btn bg-teal-400" style="width:100px;">업로드</a></th>
						</tr>
						<tr>
							<td colspan="2">
								* 구분:배송, 해지반품,반품,회수<br>
								* 엑셀의 첫 번째 행은 양식에 있는 이름 그대로를 사용하셔야 DB에 업로드 됩니다.<br>
								* 계약완료일이 없는 경우 배달완료일의+7일로 입력됩니다.<br>
								* 다른 이름의 열들은 같이 있어도 db에 입력되지 않습니다.<br>
							</td>
						</tr>
					</table>
				</div>
				<!-- /column selectors -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Large modal -->

<script>
//엑셀 업로드
function uploadCarryExcel(){
	var name=document.uploadInvoice.deliveryFile.value;
	name=name.slice(name.lastIndexOf('.')+1);
	name=name.toLowerCase();
	if(name=='xls'||name=='xlsx'){
		blockById('inside_ml');
		$('#uploadInvoice').ajaxForm({
			url:'/delivery/carry/excel',
			type:'post',
			enctype:"multipart/form-data",
			success:function(res){
				//$('#modal_large').modal('hide');
				alert(res);
				//refreshTable('dTable');
			},
			complete:function(){
				$('#inside_ml').unblock();
			}
		});
		 $("#uploadInvoice").submit() ;
	}else{
		alert('엑셀 파일을 업로드해주세요.');
	}
	
}
</script>
