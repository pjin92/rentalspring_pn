 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="modal_large" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="modal_large_title">렌탈 상품 구성 추가 </span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel" id="modal_large_content">
							<div class="panel-group accordion scrollable" id="accordion2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3_4"> <span class="fa-item"><i class="fa fa-calendar-check-o"></i> 견적 계산 </span> </a>
					</h4>
				</div>

				<div id="collapse_3_4" class="panel-collapse in">
					<div class="panel-body" style="overflow:auto;">
						<div class="table-scrollable">
									<div id="data" name="data"></div>
							<table class="table table-bordered">
								<tr>
									<th class="active"  width="10%">상품 품목</th>
									<td width="40%" colspan="2">
										<select class="form-control input-sm" name="p_sel" id="p_sel" onchange="sel_sel();">
											<option value="" >선택</option>
											<option value="세트형" >세트형</option>
											<option value="맞춤형">맞춤형</option>
										</select>
									</td>
									<th class="active" width="10%">평형</th><td width="40%" ><input type="text" class="form-control input-sm" name="ms_p_width" id="ms_p_width"></td>
								</tr>

								<tr>
									<th class="active" colspan="5" >
										<div id="radio_play" style="display:none">
											<table class="table table-bordered">
												<tr>
													<td><input type="radio" id="radio1" name="m_width_sel" onclick="cal_result(1);" value="20평형 e-엣지블루"/><label for="radio1" onclick="cal_result(1);"> 20평형 e-엣지블루</label></td>
													<td><input type="radio" id="radio2" name="m_width_sel" onclick="cal_result(2);" value="20평형 e-수정"/><label for="radio2" onclick="cal_result(2);"> 20평형 e-수정 </label></td>
													<td><input type="radio" id="radio3" name="m_width_sel" onclick="cal_result(3);" value="30평형 e-엣지블루"/><label for="radio3" onclick="cal_result(3);"> 30평형 e-엣지블루</label></td>
													<td><input type="radio" id="radio4" name="m_width_sel" onclick="cal_result(4);" value="30평형 e-수정"/><label for="radio4" onclick="cal_result(4);"> 30평형 e-수정 </label></td>
												</tr>

												<tr>
													<td><input type="radio" id="radio5" name="m_width_sel" onclick="cal_result(5);"  value="40~50평형 e-엣지블루"/><label for="radio5" onclick="cal_result(5);"> 40~50평형 e-엣지블루</label></td>
													<td><input type="radio" id="radio6" name="m_width_sel" onclick="cal_result(6);" value="40~50평형 e-수정"/><label for="radio6" onclick="cal_result(6);"> 40~50평형 e-수정 </label></td>
													<td><input type="radio" id="radio7" name="m_width_sel" onclick="cal_result(7);" value="60평형 e-엣지블루"/><label for="radio7" onclick="cal_result(7);"> 60평형 e-엣지블루</label></td>
													<td><input type="radio" id="radio8" name="m_width_sel" onclick="cal_result(8);" value="60평형 e-수정"/><label for="radio8" onclick="cal_result(8);"> 60평형 e-수정 </label></td>
												</tr>
											</table>
										</div>
									</th>
								</tr>

								<tr>
									<th class="active" colspan="5"> 상품구성 </th>
								</tr>

								<tr>
									<th class="active" >구분</th>
									<th class="active" >제품구분</th>
									<th class="active" >상품명</th>
									<th class="active" >수량</th>
									<th class="active" >설치위치</th>
								</tr>
								<tr>
									<th class="active" rowspan="4">거실등</th>
									<th  class="active" rowspan="2">e_엣지블루</th>
									<th  class="active" >LB50W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt1" id="ms_p_cnt1" value="${estimateList.opt1}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc1" id="ms_p_loc1"></td>
								</tr>

								<tr>
									<th  class="active" >KB25W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt2" id="ms_p_cnt2" value="${estimateList.opt2}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc2" id="ms_p_loc2"></td>
								</tr>

								<tr>
									<th  class="active" rowspan="2">e_수정</th>
									<th  class="active" >LC50W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt3" id="ms_p_cnt3" value="${estimateList.opt3}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc3" id="ms_p_loc3"></td>
								</tr>

								<tr>
									<th  class="active" >LC25W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt4" id="ms_p_cnt4" value="${estimateList.opt4}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc4" id="ms_p_loc4"></td>
								</tr>

								<tr>
									<th class="active" rowspan="2">침실</th>
									<th  class="active" rowspan="2">e_엣지블루</th>
									<th  class="active" >RB50W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt5" id="ms_p_cnt5" value="${estimateList.opt5}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc5" id="ms_p_loc5"></td>
								</tr>

								<tr>
									<th  class="active" >KB25W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt6" id="ms_p_cnt6" value="${estimateList.opt6}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc6" id="ms_p_loc6"></td>
								</tr>

								<tr>
									<th class="active" rowspan="2">주방</th>
									<th  class="active" rowspan="2">e_엣지블루</th>
									<th  class="active" >KB50W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt7" id="ms_p_cnt7" value="${estimateList.opt7}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc7" id="ms_p_loc7"></td>
								</tr>

								<tr>
									<th  class="active" >KB25W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt8" id="ms_p_cnt8" value="${estimateList.opt8}" style="text-align:right;" onkeyup="onlyNum(this);"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc8" id="ms_p_loc8"></td>
								</tr>

								<tr>
									<th class="active">다용도</th>
									<th  class="active">e_엣지블루</th>
									<th  class="active" >DB20W</th>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_cnt9" id="ms_p_cnt9" value="${estimateList.opt9}" style="text-align:right;"></td>
									<td style="text-align:center;"><input type="text" class="form-control input-sm" name="ms_p_loc9" id="ms_p_loc9"></td>
								</tr>
								<tr>
									<th class="active" colspan="5"> <a href="javascript:;" class="btn btn-sm blue-hoki" onclick="cal_result2()"> 계산하기 </a> </th>
								</tr>
								<tr>
									<th class="active">총액</th><td colspan="2" style="text-align:right;" ><input type="text" class="form-control input-sm" name="ms_p_allmoney" id="ms_p_allmoney" style="text-align:right;" readonly></td>
									<th class="active">총액</th><td style="text-align:right;" ><input type="text" class="form-control input-sm" name="ms_p_allmoney_t" id="ms_p_allmoney_t" style="text-align:right;" readonly></td>
								</tr>
								<tr>
									<th class="active">월 렌탈료</th><td colspan="2" style="text-align:right;"><input type="text" class="form-control input-sm" name="ms_p_month_money" id="ms_p_month_money" style="text-align:right;" readonly></td>
									<th class="active">월 렌탈료</th><td style="text-align:right;"><input type="text" class="form-control input-sm" name="ms_p_month_money_t" id="ms_p_month_money_t" style="text-align:right;" readonly></td>
								</tr>
								<tr>
									<th class="active">일시불</th><td colspan="2" style="text-align:right;" ><input type="text" class="form-control input-sm" name="ms_p_allmoney_2" id="ms_p_allmoney_2" style="text-align:right;" readonly></td>
									<th class="active">일시불</th><td  style="text-align:right;" ><input type="text" class="form-control input-sm" name="ms_p_allmoney_2_t" id="ms_p_allmoney_2_t" style="text-align:right;" readonly></td>
								</tr>

								<tr>
									<th class="active" colspan="5"> <a href="javascript:;" class="btn btn-sm blue-hoki" onclick="cal_result3()"> 견적 내역 넣기 </a> </th>
								</tr>
							</table>
							
							
							
							
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
			function sel_sel(){
				var sel = document.getElementById("p_sel").value;

				if(sel == "세트형"){
					document.getElementById("radio_play").style.display = "block";
				} else {
					document.getElementById("radio_play").style.display = "none";
				}
			}
			function comma(num){
				var len, point, str;
				num = num + "";
				point = num.length % 3 ;
				len = num.length;

				str = num.substring(0, point);
				while (point < len) {
					if (str != "") str += ",";
						str += num.substring(point, point + 3);
					point += 3;
				}

				return str;
			}

			function onlyNum(obj) {
				var val = obj.value;
				var re = /[^0-9]/gi;
				obj.value = val.replace(re, '');
			}
			
			
			function cal_result(e){
				$.ajax({
					url : "customerCalc?re_idx="+e,
					type: "GET",
					data: e,
					success:function(data){
						
						$('#data').append('<c:set var="data" value="${estimateList}"></c:set>');
					}
				
				});	
				
			}
		</script>

