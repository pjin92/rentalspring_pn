<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span lass="text-bold">기사 LED 요청</span> 
							<small class="display-block">등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 렌탈 상품 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row"> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>기사코드</label> 
														<input type="text" name="e_number" class="form-control" value="${tech.e_number}" readonly>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default ">
														<label>기사명</label> 
														<input type="text" name="e_name" value="${tech.e_name}" readonly class="form-control">
														<input type="hidden" id="e_id" name="e_id" value="${tech.e_id}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>구분</label> 	
														 <select id="lsr_status" name="lsr_status"class="select select2-hidden-accessible">
															 <option value="0" selected> 선택 </option>
															 <option value ="출고요청">출고요청 </option>
															 <option value ="반품요청">반품요청</option>
														 </select>
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 렌탈 상품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable" >
														<thead class="alpha-slate">
															<tr>
																<th>상품명</th>
																<th>렌탈료</th>
																<th>렌탈기간</th>
																<th>설치비</th>
																<th style="width:100px">요청량</th>
																<!-- <th>삭제</th> -->
															</tr>
														</thead>
														<tbody id="rproduct">
															<c:forEach var="cal" items="${cal}" varStatus="status">
															<tr>
																<td>${cal.p_name}</td>
																<td>${cal.p_price }</td>
																<td>${cal.p_rent_term}</td>
																<td>${cal.p_deliverycost }</td>
																<td>
																	<input type="text" class="form-control input-sm text-center" name="prp_num${cal.p_id}" onkeydown="numKeyUp()"  onkeyup="numCommaKeyUp();" value="0">
																	<input type ="hidden" class="form-control input-sm text-center"  id="${cal.p_id}no" name="${cal.p_id}no" value="0" onkeydown="numKeyUp()" onkeyup="numCommaKeyUp();">
																</td>
															</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 증빙서류
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="form-group form-group-default">
														<label>증빙서류1</label> 
														<input type="file" name="lsp_file1" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="form-group form-group-default">
														<label>증빙서류2</label> 
														<input type="file" name="lsp_file2" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="form-group form-group-default">
														<label>증빙서류3</label> 
														<input type="file" name="lsp_file3" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>  -->
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 메모
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12">
														<div class="form-group form-group-default">
															<label>기사메모</label>
															 <input type="text" id="lsr_t_memo" name="lsr_t_memo" class="form-control" > 
														</div>
													</div> 
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default mr-10" onclick="window.close()">닫기</button>
												<a class="btn btn-danger" onclick="startRental()">등록</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>	
	<!-- file input -->
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<script type="text/javascript" src="/assets/js/pages/uploader_bootstrap.js"></script>
	<!-- file input end-->
	<script>
	
	function refreshTable(){
		var table=$('#checkTable').DataTable();
		table.ajax.reload(null,false);
	}
	//등록
	function startRental(){
		
		if($('select[name=lsr_status]').val()=='0') {
		    alert("출고 상태를 선택하여주세요.");
		    return false;
		}

		 var param=$('form[name=frm]').serialize();
		$.ajax({
			url:'AddRequest',
			type:'post',
			data:param
		})
		.done(function(data){
			if(data.trim()=='등록완료'){
				alert("요청되었습니다.")
				window.opener.refreshTable();
				window.self.close();
				
			}else{
				alert("요청실패하였습니다.")
				window.opener.refreshTable();
				window.self.close();
			}
					
		});
};

	</script>
</body>
</html>