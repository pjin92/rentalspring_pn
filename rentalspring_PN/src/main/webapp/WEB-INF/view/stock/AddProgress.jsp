<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">기사 LED 출고</span> <small class="display-block">등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post" enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. 렌탈 상품 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row"> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>기사코드</label> 
														<input type="text" name="e_number" class="form-control" value="${list.e_number}" readonly>
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>기사명</label> 
														<input type="text" name="lsr_e_name" value="${list.e_name}" readonly class="form-control">
														<input type="hidden" id="lsr_eid" name="lsr_eid" value="${list.lsr_e_id}">
													</div>
												</div> 
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>요청상태</label> 
														<input type="text" name="status" value="${list.lsr_status}" readonly class="form-control">
													</div>
												</div> 
												<c:choose>
													<c:when test="${login.e_power eq '4' }">
													</c:when>
													<c:when test="${list.lsr_status eq '출고완료' or list.lsr_status eq '반품완료' or list.lsr_status eq '취소' }">
													</c:when>
												<c:otherwise>
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>구분</label> 	
														 <select name="lsr_status" 	class="select select2-hidden-accessible" >
															 <option value="0" selected> 선택 </option>
															 <option value ="출고완료">출고완료 </option>
															 <option value ="반품완료">반품완료</option>
															 <option value ="취소">취소</option>
														 </select>
													</div>
												</div>
												</c:otherwise>
												</c:choose>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 렌탈 상품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable" >
														<thead class="alpha-slate">
															<tr>
																<th>상품명</th>
																<th>렌탈료</th>
																<th>렌탈기간</th>
																<th>설치비</th>
																<th style="width:80px">요청량</th>
																<c:choose>
																<c:when test="${login.e_power eq '4' }">
																</c:when>
																<c:otherwise>
																	<th style="width:80px">처리량</th>
																</c:otherwise>
																</c:choose>
<!-- 																<th>삭제</th> -->
															</tr>
														</thead>
														<tbody id="rproduct"> 
															<c:forEach var="cal" items="${cal}" begin="0" end="8" varStatus="status">
															<c:set var="a" value="lsr_req${status.count}" />
															<c:set var="b" value="lsp_prs${status.count}" />
															<tr>
															<td>${cal.p_name}</td>
															<td>${cal.p_price }</td>
															<td>${cal.p_rent_term}</td>
															<td>${cal.p_deliverycost }</td>
															<td><input type="text" name="req${cal.p_id}" class="form-control input-sm text-center" value="${req[a]}" readonly>
															</td>
															<c:choose>
																<c:when test="${login.e_power eq '4' }">
																</c:when>
																<c:when test="${list.lsr_status eq '출고완료' || list.lsr_status eq '반품완료' }">
																<td><input type="text" name="prs${cal.p_id}" class="form-control input-sm text-center" value="${prs[b]}" readonly> </td>
																</c:when> 
																<c:otherwise>
																<td><input type="text" name="prp_num${cal.p_id}" onkeydown="numKeyUp()"  onkeyup="checkStock(${cal.p_id});numCommaKeyUp();" class="form-control input-sm" value="0"></td>
																</c:otherwise>
															</c:choose>
															</tr>
															</c:forEach> 
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							
							<!-- <div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. 증빙서류
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>증빙서류1</label> 
														<input type="file" name="lsr_file1" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs">
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>증빙서류2</label> 
														<input type="file" name="lsr_file2" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs">
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>증빙서류3</label> 
														<input type="file" name="lsr_file3" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs">
													</div>
												</div> 
											</div>
										</div>
									</div>
								</div>
							</div> -->
							
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. 메모
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row"> 
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														<label>기사메모</label>
														 <input type="text" id="lsr_t_memo" name="lsr_t_memo" class="form-control" value="${list.lsr_t_memo}" readonly> 
													</div>
												</div> 
												
												
												<div class="col-xs-12 col-sm-12 col-md-12">
													<div class="form-group form-group-default">
														
														<c:choose>
														<c:when test="${list.lsr_status eq '출고완료' or list.lsr_status eq '반품완료' or list.lsr_status eq '취소' }">
														<label>출고메모</label> <input type="text" id="lsp_t_memo" name="lsp_t_memo"  class="form-control" readonly/>
														</c:when>
														<c:otherwise>
														<label>출고메모</label> <input type="text" id="lsp_t_memo" name="lsp_t_memo"  class="form-control" >
														</c:otherwise>
														</c:choose>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<button type="button" class="btn btn-default" onclick="window.close()">닫기</button>
												<c:choose>
													<c:when test="${login.e_power eq '4' || list.lsr_status eq '출고완료' }">
													</c:when>
													<c:otherwise>
													<a class="btn btn-danger ml-10" onclick="startRental()">등록</a>											
													</c:otherwise>
												</c:choose>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<!-- file input -->
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<script type="text/javascript" src="/assets/js/pages/uploader_bootstrap.js"></script>	
	<script>
	
	function refreshTable(){
		var table=$('#productTable').DataTable();
		table.ajax.reload(null,false);
	}
	
	//등록
	function startRental(){ 
		if($('select[name=lsr_status]').val()=='0') {
		    alert("출고 상태를 선택하여주세요.");
		    return false;
		}
		 var param=$('form[name=frm]').serialize();
		$.ajax({
			url:'confirmProgress',
			type:'post',
			data:param
		})
		.done(function(data){
			if(data.trim()=='출고완료'){
				alert("재고처리 완료");
				window.opener.refreshTable();
				window.close(); 
			}else{
				alert("재고처리 완료 실패");
				window.opener.refreshTable();
				window.close();
			}
					
		});
	};
	
	function checkStock(id){
		var num = $('input[name=prp_num'+id+']').val();
		var params = "pid="+id+"&num="+num
		
		$.ajax({
			type:'GET',
			url:'checkStock',
			data : params
		}).done(function(data){
			alert(data);
			if(data.trim()=="초과"){
				alert("재고가 충분하지 않습니다.")
				 $('input[name=prp_num'+id+']').val(0);
				
			}
		});
				
	}
	</script>
</body>
</html>