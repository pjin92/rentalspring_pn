<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>재고요청관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">LED재고요청 조회</span>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body"> 
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">기사명 검색</label>
													<input type="text" class="form-control input-sm" placeholder="검색" name="lsr_creater">
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">상태 검색</label>
													<select class="form-control input-sm" name="lsr_status">
														<option value="">선택</option>
														<c:forEach var="lsr_status" items="${tlsr_status}">
														<option value="${lsr_status}">${lsr_status}</option>										
														</c:forEach>
													</select>
												</div>
											</div>	
											<%-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
					                                    <label class="control-label col-md-5 col-lg-4 form-tit-label">대분류 선택</label>       
					                                          <select class="form-control input-sm" id="main" name="sp_mm_idx" onchange="change()">
					                                              <option value="0" selected>선택</option>
															<c:forEach items="${bsort}" var="i">
																 <option value="${i.sp_mm_idx}" ${i.sp_mm_idx eq sd.sp_mm_idx ? 'selected':''}>${i.sp_mm_name}</option>
															</c:forEach>
														</select>         
					                           	</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group" >
													<label class="form-tit-label">등록일</label>
													<div class="form_control_1">
														<input type="text" class="form-control input-sm daterange-blank" name ="sp_p_date" value=""> 
													</div>
												</div>
											</div> --%>
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>					
									</form>
								</div>
							</div>
							<!-- /Search layout --> 
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>LED재고요청 조회결과</h5>
									<div class="heading-elements">
										<div class="btn-group">
											<button type="button" class="btn bg-teal-400 btn-labeled legitRipple" onclick="addPop()"><b><i class="icon-plus3"></i></b>신규등록</button>
										</div>
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<th data-orderable="false" class="active">선택</th>
											<th>요청일자</th>
											<th>처리일자</th>
											<th>상태</th>
											<th>기사명</th>
											
											<th>퍼즐1등 요청량</th>
											<th>퍼즐1등 처리량</th>
											<th>퍼즐2등 요청량</th>
											<th>퍼즐2등 처리량</th>
											
											<th>퍼즐4등 요청량</th>
											<th>퍼증4등 처리량</th>
											<th>퍼즐6등 요청량</th>
											<th>퍼증6등 처리량</th>
	
											<th>퍼즐9등 요청량</th>
											<th>퍼증9등 처리량</th>
											<th>퍼즐12등 요청량</th>
											<th>퍼증12등 처리량</th>
	
											<th>퍼즐15등 요청량</th>
											<th>퍼증15등 처리량</th>
											<th>퍼즐긴4등 요청량</th>
											<th>퍼즐긴4등 처리량</th>
											
<!-- 											<th>퍼즐4등(거실) 요청량</th> -->
<!-- 											<th>퍼즐4등(거실) 처리량</th>  -->
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
								<!-- <div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="refreshTable('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
												<hr> 
												<li><a onclick="damdang()"><i class="icon-copy3"></i>배정</a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div> 
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": null,"defaultContent":"" },
		    { "data": "lsr_req_date"},
		    { "data": "lsp_prs_date","defaultContent":"요청중"},
		    { "data": "lsr_status"},
		    { "data": "lsr_creater"},
		    
		    { "data": "lsr_req1"},//신청고객
		    { "data": "lsp_prs1","defaultContent":"0"},
		    { "data": "lsr_req2"},
		    { "data": "lsp_prs2","defaultContent":"0"},
		    
		    { "data": "lsr_req3"},
		    { "data": "lsp_prs3","defaultContent":"0"},
		    { "data": "lsr_req4"},
		    { "data": "lsp_prs4","defaultContent":"0"},
		    
		    { "data": "lsr_req5"},
		    { "data": "lsp_prs5","defaultContent":"0"},
		    { "data": "lsr_req6"},
		    { "data": "lsp_prs6","defaultContent":"0"},
	
		    { "data": "lsr_req7"},
		    { "data": "lsp_prs7","defaultContent":"0"},
		    { "data": "lsr_req8"},
		    { "data": "lsp_prs8","defaultContent":"0"}
	
		    
// 		    { "data": "lsr_req9"},
// 		    { "data": "lsp_prs9","defaultContent":"0"}
		  
		    ]
	});
	tableLoad('checkTable');
	
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		checkDt('checkTable','/stock/ledRequest');
	}
	
	//재검색 입력시 기존 요청 취소
	$(document).ajaxSend(function(){
		var table=$('#checkTable').DataTable();
		table.settings()[0].jqXHR.abort();
	});
		
	
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#checkTable').DataTable();
		table.ajax.url('/stock/ledRequest?'+param).load(null,false);
	} 
	
	function refreshTable(){
		var table=$('#checkTable').DataTable();
		table.ajax.reload(null,false);
	}
	
	
	function addPop(){
		window.open('AddRequest','popup','width=1200,height=800');
	}
	//등록
	function member(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/'+m_idx+'/','popup','width=1200,height=600');
	}
	
	function choose(){
		var lsr_idx=$(event.target).attr('lsr_idx');
		window.open('AddProgress/'+lsr_idx+'/','popup','width=1200,height=800');
	}
	
	</script>
</body>
</html>