<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<title>LED 재고 요청</title>
	<%@include file="/WEB-INF/view/main/m_css.jsp"%>
</head>
<body>
	<!-- LED 재고요청 조회 -->
	<div data-role="page" id="ledRequest-page" data-url="ledRequest-page"  data-title="LED 재고 요청">
		<div data-role="header" data-positon="fixed">
			<h1>LED 재고 요청</h1> 
			<a data-ajax="false" href="/indexApp" class="ui-back-btn ui-btn-right" data-role="button" role="button" title="로그아웃"><i class="icon-exit"></i></a>
		</div>
		<!-- search input -->		
		<!-- <div class="ui-grid-solo search-section">
			<div class="ui-block-a search-wrap">
				<form name="searchForm">
					<input type="text" name="mainKeyword" id="mainKeyword" placeholder="기사명, 소속, 상태" />
					<button type="submit" class="search-btn" onsubmit="searchBtn()"><i class="icon-search4"></i></button>
				</form>
			</div>
		</div>		 -->
		<!--// search input -->
		<!-- main content -->	
		<c:set var="st" value="#{data}" />
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<div class="text-right margin-10px-bottom">
				<a href="#requestAdd-page" class="btn btn-sm bg-warning" onclick="stockRequest();">신규등록</a>
				<a e_id ="${st.tled_e_id }" class="btn btn-sm bg-complete" onclick="stockRefresh();">재고갱신</a>	
			</div>
		<div class="ui-grid-solo">
				<!-- 리스트 반복 -->
				
				<div class="ui-block-a list-box-wrap">
					<div class="list-center-box">
						<ul class="double-right-list width-100">
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐1등 보유량</p>
									<p class="cont">${st.tled_led1}</p>
								</div>
							</li>
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐2등 보유량</p>
									<p class="cont">${st.tled_led2}</p>
								</div>
							</li>
							<li class="clearfix">	
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐4등 보유량</p>
									<p class="cont">${st.tled_led3}</p>
								</div>
							</li>
							<li class="clearfix">	
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐6등 보유량</p>
									<p class="cont">${st.tled_led4}</p>
								</div>
							</li>
							<li class="clearfix">	
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐9등 보유량</p>
									<p class="cont">${st.tled_led5}</p>
								</div>
							</li>
							<li class="clearfix">	
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐12등 보유량</p>
									<p class="cont">${st.tled_led6}</p>
								</div>
							</li>
							<li class="clearfix">	
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐15등 보유량</p>
									<p class="cont">${st.tled_led7}</p>
								</div>
							</li>
							<li class="clearfix">	
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐긴4등 보유량</p>
									<p class="cont">${st.tled_led8}</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- 리스트 section -->
			<div class="ui-grid-solo">
				<!-- 리스트 반복 -->
				<c:forEach var="stock" items="${stock}">
				<div class="ui-block-a list-box-wrap">
					<div class="list-center-box">	
						<fmt:parseDate var="date" value="${stock.lsr_req_date}" pattern="yyyy-MM-dd HH:mm:ss"/>
						<p class="bg-gray"><b><fmt:formatDate value="${date}" pattern="yyyy-MM-dd"/></b></p><!-- 요청일 날짜만 (시간삭제)-->
						<!-- 미처리, 처리일자 클래스 구분 -->
						<!-- <p class="bg-gray"><b>미처리</b></p> -->
						<c:choose>
						<c:when test="${stock.lsp_prs_date eq null }">
						<!-- 미출고, 출고완료 클래스 구분 -->
						<p class="bg-danger"><b>미출고</b></p>
						</c:when>
						<c:when test="${stock.lsr_status eq '출고완료' }">
						<fmt:parseDate var="date1" value="${stock.lsp_prs_date}" pattern="yyyy-MM-dd HH:mm:ss"/>
						<p class="bg-indigo"><b><fmt:formatDate value="${date1}" pattern="yyyy-MM-dd"/></b></p><!-- 처리일자 -->
						<!-- 미출고, 출고완료 클래스 구분 -->
						<p class="bg-complete"><b>출고완료</b></p>
						</c:when>

						<c:when test="${stock.lsr_status eq '반품완료' }">
						<fmt:parseDate var="date1" value="${stock.lsp_prs_date}" pattern="yyyy-MM-dd HH:mm:ss"/>
						<p class="bg-indigo"><b><fmt:formatDate value="${date1}" pattern="yyyy-MM-dd"/></b></p><!-- 처리일자 -->
						<!-- 미출고, 출고완료 클래스 구분 -->
						<p class="bg-complete"><b>반품완료</b></p>
						</c:when>


						
						</c:choose>
					
					</div>
					<div class="list-center-box">
						<ul class="double-right-list width-100">
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">상품명</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="tit">요청량</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="tit">처리량</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐1등 </p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req1}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs1}</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐2등</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req2}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs2}</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐4등</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req3}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs3}</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐6등</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req4}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs4}</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐9등</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req5}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs5}</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐12등</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req6}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs6}</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐15등 </p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req7}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs7}</p>
								</div>
							</li>
							
							<li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐 긴4등 </p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req8}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs8}</p>
								</div>
							</li>
										
							<%-- <li class="clearfix">
								<div class="float-left width-50 right-pd">
									<p class="tit">퍼즐4등(거실)</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsr_req9}</p>
								</div>
								<div class="float-right width-25 text-center">
									<p class="cont">${stock.lsp_prs9}</p>
								</div>
							</li> --%>
						</ul>
					</div>
				</div>						
			</c:forEach>
			</div>	
			<!--// 리스트 section -->
		</div>
		<!--// main content -->
		<%@ include file="../main/m_menu.jsp" %>
	</div>
	<!--// LED 재고요청 조회 -->
	
	<!-- LED 재고 등록  page -->
	<div data-role="page" id="requestAdd-page" data-url="requestAdd-page" class="jqm-demos" data-title="LED 재고 요청 등록">
		<div data-role="header" data-positon="fixed">
			<h1>LED 재고 등록</h1> 
			<a href="#ledRequest-page" data-direction="reverse" class="ui-back-btn" data-role="button" role="button"><i class="icon-circle-left2"></i></a>
			<a data-ajax="false" href="/indexApp" class="ui-back-btn ui-btn-right" data-role="button" role="button" title="로그아웃"><i class="icon-exit"></i></a>
		</div>	
		<!-- main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<div class="ui-grid-solo">
				<div class="ui-block-a">
					<form role="form" name="frm" method="post" enctype="multipart/form-data">
						<!-- 1. 렌탈 상품 정보 -->
						<div class="width-100">
							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. 렌탈 상품 정보</h4>
							<div class="form-group">
								<label for="date">기사코드</label>
								<input type="text" name="e_number" value="${tech.e_number}" readonly />
								<input type="hidden" id="e_id" name="e_id" value="${tech.e_id}">
							</div>
							<div class="form-group">
								<label for="date">기사명</label>
								<input type="text" name="e_name" value="${tech.e_name}" readonly />
							</div>
							<div class="form-group">
								<p class="text-small">구분</p>
								<div data-role="controlgroup" data-type="horizontal" data-mini="true">
									<input type="radio" name="lsr_status" id="radio-choice-c" value="출고요청">
									<label for="radio-choice-c">출고요청</label>
									<input type="radio" name="lsr_status" id="radio-choice-d" value="반품요청">
									<label for="radio-choice-d">반품요청</label>
								</div>
							</div>
						</div>
						<!--// 1. 렌탈 상품 정보 -->
						<!-- 2. 렌탈 상품 구성 -->
						<div class="margin-25px-top clearfix">
							<h4 class=""><i class="icon-folder-search"></i>&nbsp;&nbsp;2. 렌탈 상품 구성</h4>	
									<table class="table table-hover" id="productTable" >
														<thead> 
															<tr>
																<th>상품명</th>
																<th>렌탈료</th>
																<th>렌탈기간</th>
																<th>설치비</th>
																<th style="width:10%">요청량</th>
															</tr>
														</thead>
														<tbody id="rproduct">
															<c:forEach var="cal" items="${cal}" varStatus="status">
															<tr>
																<td>${cal.p_name}</td>
																<td>${cal.p_price_f }</td>
																<td>${cal.p_rent_term}</td>
																<td>${cal.p_deliverycost_f }</td>
																<td><input type="number" name="prp_num${cal.p_id}" class="form-control input-sm" value="0">
																<input type ="hidden" class="form-control input-sm"  id="${cal.p_id}no" name="${cal.p_id}no" value="0" onkeydown="numKeyUp()" onkeyup="numCommaKeyUp();">
																</td>
															</tr>
															</c:forEach>
														</tbody>
													</table>				
						</div>
						<!--// 2. 렌탈 상품 구성 -->
						<!-- 3. 증빙서류 -->
					<!-- 	<div class="margin-25px-top">
							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;3. 증빙서류</h4>
							<div class="form-group">
								<label for="lsp_file1">증빙서류1</label> 
								<input type="file" name="lsp_file1" />
							</div>
							<div class="form-group">
								<label for="lsp_file2">증빙서류2</label> 
								<input type="file" name="lsp_file2" />
							</div>
							<div class="form-group">
								<label for="lsp_file3">증빙서류3</label> 
								<input type="file" name="lsp_file3" />
							</div>		
						</div> -->
						<!--// 3. 증빙서류 -->
						<!-- 4. 메모 -->
						<div class="margin-25px-top">
							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;3. 메모</h4>
							<div class="form-group">
								<label for="lsr_t_memo">기사메모</label>
								<input type="text" data-clear-btn="true" name="lsr_t_memo" id="lsr_t_memo" value="" />
							</div>
							<!-- <div class="form-group">
								<label for="lsr_t_memo">출고메모</label>
								<input type="text" data-clear-btn="true" name="lsp_t_memo" id="lsp_t_memo" value="" />
							</div> -->
						</div>
						<!--// 4. 메모 -->
					</form>					
				</div>
			</div>			
			<div class="list-center-box padding-10px-top">
				<a href="javascript:startRental()" class="width-100 btn bg-info">등록</a>
			</div>
		</div>	
		<!--// main content -->	
	</div>
	<!--// LED 재고 등록  page -->
		
	<%@include file="/WEB-INF/view/main/m_js.jsp"%>
	
	<script>
		//등록
	function startRental(){
		 var param=$('form[name=frm]').serialize();
		 var status = $(':input[name=lsr_status]:radio:checked').val();
		 if(!status){
			 alert('요청상태를 체크하여 주세요');
		 }else{
		 $.ajax({
			url:'AddRequest',
			type:'post',
			data:param,
			success:function(data){
				if(data.trim()=='등록완료'){
					alert("요청되었습니다.");
					window.location.href='/stock/ledRequestApp';			
					};
				}
			});
		}
	};

	function stockRefresh(){
		var e_id = $(event.target).attr("e_id")
// 		alert(e_id)
		if(confirm('갱신하시겠습니까?')){
			
			$.ajax({
				url:'/stock/stockRefresh/'+e_id,
				type:'GET',
			}).done(function(data){
				alert(data);
				location.href = '/stock/ledRequestApp';
			});
		}else{
			return false;
		}
	}


	</script>
</body>
</html>