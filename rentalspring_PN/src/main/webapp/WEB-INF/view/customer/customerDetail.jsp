<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>상담관리 > 고객 정보</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<link href="/assets/css/jquery-ui.css" rel="stylesheet" type="text/css"> 
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">상담관리<i class=" icon-arrow-right13"></i> 고객</span>
							<small class="display-block">정보</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- 고객정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>1.신청 고객 정보</h5>
								</div>
								<div class="panel-body">	
									<form id="art1">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default disabled">
														<label for="form_control_1">
															렌탈 상품명
															<a class="badge bg-blue-300" onclick="openChangePr()">변경</a>
														</label>
<%-- 														<input type="text" class="form-control input-sm text-right" value="${product.pr_name}" readonly> --%>
														<a pr_idx="${product.pr_idx}" onclick='rproductDatail()' class="form-control input-sm text-right" >${product.pr_name}</a>
														
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">판매처</label>
														<input type="text" class="form-control input-sm text-right" name="m_sale" value="${m.m_sale}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default disabled">
														<label for="form_control_1">등록일</label>
														<input type="text" class="form-control input-sm text-right" value="${mi_apply.mt_log}" readonly>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">판매 기수</label>
														<input type="text" class="form-control input-sm text-right" name="mg_name" value="${mg.mg_name }" placeholder="ex)우리 1기">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">판매 차수</label>
														<input type="text" class="form-control input-sm text-right" name="mgm_cnt" value="${mg.mgm_cnt}" placeholder="ex)1차">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default disabled">
														<label for="form_control_1">상담CS</label>
														<input type="text" class="form-control input-sm text-right" value="${m_damdang.e_name}" readonly>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default disabled">
														<label for="form_control_1">고객번호</label>
														<input type="text" class="form-control input-sm text-right" value="${m.m_num}" readonly>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">신청고객명</label>
														<input type="text" class="form-control input-sm text-right" name="mi_name" value="${mi_apply.mi_name}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
													<label for="form_control_1">개인/법인</label>
													<select class="select select2-hidden-accessible" name="mi_id_type">
														<option value="">선택</option>
														<option ${mi_apply.mi_id_type eq '개인'?'selected':'' }>개인</option>
														<option ${mi_apply.mi_id_type eq '법인'?'selected':'' }>법인</option>
													</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
															<label for="form_control_1">주민번호/사업자번호</label>
															<input type="text" class="form-control input-sm text-right" name="mi_id" value="${mi_apply.mi_id}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">연락처</label>
														<input type="text" class="form-control input-sm text-right" name="mi_phone" value="${mi_apply.mi_phone}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">연락처</label>
														<input type="text" class="form-control input-sm text-right" name="mi_tel" value="${mi_apply.mi_tel}">
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">주소1</label>
														<input type="text" class="form-control input-sm text-right" name="mi_addr1" value="${mi_apply.mi_addr1}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">주소2</label>
														<input type="text" class="form-control input-sm text-right" name="mi_addr2" value="${mi_apply.mi_addr2}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">상세 주소 <a class="badge bg-orange-300" onclick="openAddress('art1')">검색</a></label>
														<input type="text" class="form-control input-sm text-right" name="mi_addr3" value="${mi_apply.mi_addr3}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">우편번호</label>
														<input type="text" class="form-control input-sm text-right" name="mi_post" value="${mi_apply.mi_post}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group form-group-default">
														<label for="form_control_1">메모</label>
														<input type="text" class="form-control input-sm" style="resize:vertical" name="mi_memo" value="${mi_apply.mi_memo}">
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-teal-400" onclick="saveArt('1')">저장 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 고객정보  -->
							<!-- 해피콜 상태 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>2_1.해피콜 구분, 상태 정보</h5>
								</div>
								<div class="panel-body">
									<form id="art2_1">
										<div class="form-group-attached">
											<!-- <p>* 해피콜 업무 프로세스 종료(설치관리로 이관)<br>1. 렌탈, 개인영업 : 상담상태 = 상담완료 / 녹취계약확정 = 확정 <br>2. 무료체험, 방문견적, 일시불(렌탈) : 상담상태 = 상담완료, 선설치(렌탈전환) / 녹취계약확정 = 미확정</p> -->
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-4">
													<div class="form-group form-group-default">
														<label>상담 상태</label>
														<select class="select select2-hidden-accessible" name="m_state">
														<c:forEach var="ms" items="${mstates}">
															<option ${m.m_state eq ms ?'selected':''}>${ms}</option>
														</c:forEach>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-4">
													<div class="form-group form-group-default">
														<label>접수구분</label>
														<select class="select select2-hidden-accessible" name="m_jubsoo">
															<option value="">선택</option>
															<c:forEach var="mj" items="${mjubsoos}">
															<option ${m.m_jubsoo eq mj ?'selected':''}>${mj}</option>
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-4">
													<div class="form-group form-group-default">
														<label>매출구분</label>
														<select class="select select2-hidden-accessible" name="m_detail">
															<option value="">선택</option>
															<c:forEach var="mdetail" items="${mdetails}">
															<option ${m.m_detail eq mdetail ?'selected':''}>${mdetail}</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-teal-400" onclick="saveArt('2_1')">저장 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 해피콜 상태 -->	
							<!-- 해피콜 상담 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>2_2.해피콜 상담 정보</h5>
								</div>
								<div class="panel-body">
									<form id="art2_2">
										<div class="form-group-attached" id="tb2_2">
											<c:if test="${empty art2_2}">
											<script>window.onload=function(){addMemo();}</script>
											</c:if>
											<c:forEach items="${art2_2}" var="memo">
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-3">
													<div class="form-group form-group-default">
														<label for="mf_log">상담일</label>
														<input type="text" class="form-control input-sm daterange-single" id="mf_log" name="mf_log" value="${memo.mf_log}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-9 position-relative pr-10">
													<div class="form-group form-group-default">
														<label for="mf_memo">상담메모</label>
														<textarea class="form-control input-sm" style="resize:vertical;background: white;" id="mf_memo" name="mf_memo">${fn:replace(memo.mf_memo,'<br>','&#10;')}</textarea>
														
														<c:if test="${login.e_power eq 7 }">
															<a class="mf-memo-del" onclick="delMemo()"><i class="icon-x"></i></a>
														</c:if>
														<!-- <a class="mf-memo-del" onclick="$(this).parents('.col-xs-12').parent('.row').remove();"><i class="icon-x"></i></a> -->
													</div>
												</div>
											</div>
											</c:forEach>
											
										</div>
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-orange-800" onclick="addMemo()">추가 </a>
											<a class="btn bg-teal-400 position-right" onclick="saveMemo()">저장 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 해피콜 상담 -->	
							<!-- 계약고객정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>3.계약 고객 정보</h5>
								</div>
								<div class="panel-body">
									<form id="art3">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<label for="form_control_1">계약완료일</label>
														<input type="text" class="form-control input-sm text-right daterange-single" name="m_contract_date" value="${m.m_contract_date}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default disabled">
														<label for="form_control_1">고객 번호</label>
														<input type="text" class="form-control input-sm text-right" value="${m.m_num}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">개인/법인</label>
														<select class="select select2-hidden-accessible" name="mi_id_type">
															<option value="">선택</option>
															<option ${mi_contract.mi_id_type eq '개인'?'selected':'' }>개인</option>
															<option ${mi_contract.mi_id_type eq '법인'?'selected':'' }>법인</option>
														</select>
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">계약 고객명</label>
														<input type="text" class="form-control input-sm text-right" name="mi_name" value="${mi_contract.mi_name}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">연락처1</label>
														<input type="text" class="form-control input-sm text-right" name="mi_phone" value="${mi_contract.mi_phone}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default"> 
														<label for="form_control_1">연락처2</label>
														<input type="text" class="form-control input-sm text-right" name="mi_tel" value="${mi_contract.mi_tel}"> 
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">주민번호</label>
														<input type="text" class="form-control input-sm text-right" name="mi_id" value="${mi_contract.mi_id}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">신용조회등급</label>
														<input type="text" class="form-control input-sm text-right" name="m_cb" value="${m.m_cb}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">계약서 수령주소1</label>
														<input type="text" class="form-control input-sm text-right" name="mi_addr1" value="${mi_contract.mi_addr1}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">계약서 수령주소2</label>
														<input type="text" class="form-control input-sm text-right" name="mi_addr2" value="${mi_contract.mi_addr2}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">우편번호</label>
														<input type="text" class="form-control input-sm text-right" name="mi_post" value="${mi_contract.mi_post}" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">상세주소<a class="badge bg-orange-300" onclick="openAddress('art3')">검색</a></label>
														<input type="text" class="form-control input-sm text-right" name="mi_addr3" value="${mi_contract.mi_addr3}">
													</div>
												</div>
												
												<div class="col-xs-12">
													<div class="form-group form-group-default">
														<label for="form_control_1">메모</label>
														<input type="text" class="form-control input-sm" style="resize:vertical" name="mi_memo" value="${mi_contract.mi_memo}">
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-orange-800" onclick="getMemberInfo('art3')">고객정보 불러오기 </a>
											<a class="btn bg-teal-400 position-right" onclick="saveArt('3')">저장 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 계약고객정보  -->		
							<!-- 배송 고객정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>4.배송/설치 고객 정보</h5>
								</div>
								<div class="panel-body">
									<form id="art4">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-2 ">
													<div class="form-group form-group-default">
														<label for="mi_name">배송/설치 고객명</label>
														<input type="text" class="form-control input-sm text-right" value="${mi_delivery.mi_name}" name="mi_name">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label for="mi_phone">연락처1</label>
														<input type="text" class="form-control input-sm text-right" value="${mi_delivery.mi_phone}" name="mi_phone">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label for="mi_tel">연락처2</label>
														<input type="text" class="form-control input-sm text-right" value="${mi_delivery.mi_tel}" name="mi_tel">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label for="mi_addr1">배송/설치 주소1</label>
														<input type="text" class="form-control input-sm text-right" value="${mi_delivery.mi_addr1}" name="mi_addr1" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label for="mi_addr2">배송/설치 주소2</label>
														<input type="text" class="form-control input-sm text-right" value="${mi_delivery.mi_addr2}" name="mi_addr2" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-2">
													<div class="form-group form-group-default">
														<label for="mi_post">우편번호</label>
														<input type="text" class="form-control input-sm text-right" value="${mi_delivery.mi_post}" name="mi_post" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group form-group-default">
														<label for="mi_addr3">상세주소<a class="badge bg-orange-300 position-right" onclick="openAddress('art4')">검색</a></label>
														<input type="text" class="form-control input-sm" value="${mi_delivery.mi_addr3}" name="mi_addr3">
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group form-group-default">
														<label for="mi_memo">배송/설치 메모</label>
														<input type="text" class="form-control input-sm" style="resize:vertical" name="mi_memo" value="${mi_delivery.mi_memo}">
													</div>
												</div>
											</div>
										</div>	
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-orange-800" onclick="getMemberInfo('art4')">고객정보 불러오기 </a>
											<a class="btn bg-teal-400 position-right" onclick="saveArt('4')">저장 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 배송고객정보  -->		
							<!-- 결제정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<c:set var="finalContract">
								<c:if test="${m.m_final_date ne ''}">readonly</c:if>
								</c:set>
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>5.결제정보</h5>
								</div>
								<div class="panel-body">
									<form id="art5">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<label for="form_control_1">이체 희망일</label>
														<input type="text" class="form-control input-sm text-right" name="m_paydate" value="${m.m_paydate}" onkeyup="numKeyUp()" maxlength="2">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">이체월수</label>
														<input type="text" class="form-control input-sm text-right" name="m_period" value="${m.m_period}" onkeyup="numKeyUp();setPayEnd();setTotal();" ${finalContract}>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">결제시작월</label>
														<fmt:parseDate var="dateString" value="${m.m_paystart}" pattern="yyyy-MM" />
														<input type="text" class="form-control month input-sm text-right" id="monthpicker" name="m_paystart" value="<fmt:formatDate value="${dateString}" pattern="yyyy-MM" />" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">결제종료일</label>
														<input type="text" class="form-control input-sm text-right" name="m_payend" value="" readonly="readonly">
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">월이체액</label>
														<input type="text" class="form-control input-sm text-right" name="m_rental" value="${m.m_rental}" onkeyup="numCommaKeyUp();setTotal();">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">총액</label>
														<input type="text" class="form-control input-sm text-right" name="m_total" value="${m.m_total}" onkeyup="numCommaKeyUp()" readonly="readonly">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">일시불 금액</label>
														<input type="text" class="form-control input-sm text-right" name="m_ilsi" value="${m.m_ilsi}" onkeyup="numCommaKeyUp()">
													</div>
												</div>
												
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">결제방식</label>
														<select class="select select2-hidden-accessible" name="m_pay_method" onchange="payMethod()">
															<option value="">선택</option>
															<option ${m.m_pay_method eq '계좌이체'?'selected':''}>계좌이체</option>
															<option ${m.m_pay_method eq '카드'?'selected':''}>카드</option>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">CMS 상태</label>
														<span class="text-right" id="m_cms">${m.m_cms }</span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
													<label for="form_control_1">개인/법인</label>
													<select class="select select2-hidden-accessible" name="m_id_type">
														<option ${m.m_id_type eq '개인'?'selected':'' }>개인</option>
														<option ${m.m_id_type eq '법인'?'selected':'' }>법인</option>
													</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">주민번호/사업자번호</label>
														<input type="text" class="form-control input-sm text-right" name="m_pay_id" value="${m.m_pay_id }">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label for="form_control_1">연락처</label>
														<input type="text" class="form-control input-sm" name="m_phone" value="${m.m_phone}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 cmsInfo">
													<div class="form-group form-group-default">
														<label for="form_control_1"><span class="cmscard">카드명</span><span class="cmsbank">은행명</span></label>
														<span class="cmsbank">
															<select name="m_pay_info_bank" class="select select2-hidden-accessible">
																	<option value="">선택</option>
																<c:forEach var="mp" items="${bank_methods}">
																	<option ${mp.sb_name eq m.m_pay_info ?'selected':''}>${mp.sb_name}</option>
																</c:forEach>
															</select>
														</span>
														<span class="cmscard">
															<select name="m_pay_info_card" class="select select2-hidden-accessible">
																<option value="">선택</option>
																<c:forEach var="mp" items="${card_methods}">
																	<option ${mp.sc_name eq m.m_pay_info ?'selected':''}>${mp.sc_name}</option>
																</c:forEach>
															</select>
														</span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 cmsInfo">
													<div class="form-group form-group-default">
														<label for="form_control_1"><span class="cmscard">카드소유자명</span><span class="cmsbank">예금주명</span></label>
														<input type="text" class="form-control input-sm" name="m_pay_owner" value="${m.m_pay_owner }">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 cmsInfo">
													<div class="form-group form-group-default">
														<label for="form_control_1"><span class="cmscard">카드번호</span><span class="cmsbank">계좌번호</span></label>
														<input type="text" class="form-control input-sm text-right" name="m_pay_num" value="${m.m_pay_num }">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 cmscard">
													<div class="form-group form-group-default">
														<label for="form_control_1">카드유효 월</label>
														<input type="text" class="form-control input-sm text-right" name="m_card_month" value="${m.m_card_month }" maxlength="2" onkeyup="numKeyUp()">
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 cmscard">
													<div class="form-group form-group-default">
														<label for="form_control_1">카드유효 년</label>
														<input type="text" class="form-control input-sm text-right" name="m_card_year" value="${m.m_card_year }" maxlength="2" onkeyup="numKeyUp()">
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-teal-400" onclick="saveArt('5')">저장 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 결제정보  -->	
							<!-- 녹취계약 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300" id="art6">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>6.녹취 계약 완료</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">녹취 계약 완료일</label>
													<span id="m_voice_contract">${m.m_voice_contract}</span> 
												</div>
											</div>
											<c:forEach items="${records}" var="record">
											<div class="col-xs-12 col-sm-4 col-md-3 recordFile">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">녹취파일(${record.mf_log})</label>
													<a class="text-danger-400" onclick="deleteFile(${record.mf_idx})"><i class="icon-cross3"></i></a>
													<a class="pull-right" href="/file/member/${record.mf_idx}_${record.mf_memo}">${record.mf_file}</a> 
												</div>
											</div>
											</c:forEach>
											<div class="col-xs-12 col-sm-4 col-md-3 creditFile" id="creditFile">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">신용정보조회동의서</label>
													<c:if test="${empty credits}"><a class="pull-right badge bg-orange-300" onclick="fileUpload('credit')">파일등록</a></c:if>
													<c:if test="${not empty credits}">
														<a class="text-danger-400" onclick="deleteFile(${credits[0].mf_idx})"><i class="icon-cross3"></i></a>
														<a class="pull-right" href="/file/member/${credits[0].mf_idx}_${credits[0].mf_memo}">${credits[0].mf_file}</a>
													</c:if>
												</div>
											</div>
											<span id="recordFile"></span>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-teal-400" onclick="fileUpload('record')">녹취파일등록 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 녹취계약  -->
							<!-- 최종계약확정 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>7.최종 계약 확정</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">설치확인서</label>
													<c:if test="${not empty installFile}">
														<a class="pull-right" href="/file/member/${installFile[0].mf_idx}_${installFile[0].mf_memo}">${installFile[0].mf_file}</a>
													</c:if> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">설치 철거 확인서</label>
													<c:if test="${not empty installRemove}">
														<a class="pull-right" href="/file/member/${installRemove[0].mf_idx}_${installRemove[0].mf_memo}">${installRemove[0].mf_file}</a>
													</c:if> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">렌탈 약정서(인증용, 이미지파일)</label>
													<c:if test="${not empty agree}"><a class="pull-right" href="/file/member/${agree[0].mf_idx}_${agree[0].mf_memo}">${agree[0].mf_file}</a></c:if> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">양도통지서</label>
													<c:if test="${not empty transfer}"><a class="pull-right">${transfer[0].mf_file}</a></c:if> 
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-teal-400" onclick="recordContract()">녹취 계약 완료 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 최종계약확정  -->
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="modal/changeRentalProduct.jsp"%>
	<%@include file="modal/rProductDetail.jsp"%>
	<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	//1. 신청고객 정보. 다음 주소
/* MonthPicker 옵션 */
		options = {
			pattern: 'yyyy-mm',
			selectedYear: 2018,
			startYear: 2018,
			finalYear: 2028,
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		};
		
		/* MonthPicker Set */
		$('#monthpicker').monthpicker(options);
		
	
	function openAddress(formId){
		new daum.Postcode({
		    oncomplete: function(data) {
		      	var sido=data.sido;
		    	var sigungu=data.sigungu;
		    	var roadname=data.address.replace(sido,'');
		    	roadname=roadname.trim();
		    	roadname=roadname.replace(sigungu,'');
		    	roadname=roadname.trim();
		    	var zonecode=data.zonecode;
		    	$('#'+formId+' input[name=mi_addr1]').val(sido);
		    	$('#'+formId+' input[name=mi_addr2]').val(sigungu);
		    	$('#'+formId+' input[name=mi_addr3]').val(roadname);
		    	$('#'+formId+' input[name=mi_post]').val(zonecode);
		    }
		}).open({popupName:'daumAddr'});
	}
	/* function jubsoo(){
		var jub=$('select[name=m_jubsoo]').val();
		if(jub!=''){
			$.ajax('/customer/mdetailList?m_jubsoo='+jub)
			.done(function(data){
				var sel='<option value="">선택</option>';
				for(i=0;i<data.length;i++){
					sel+='<option>'+data[i]+'</option>';
				}
				$('select[name=m_detail]').html(sel);
			});
		}
	} */
	
	//상담관리>고객정보 저장. title에는 2_1, 3,4 등 숫자 들어감
	function saveArt(title){
		blockById('art'+title);
		var param=$('#art'+title).serialize();
		$.ajax({
			url:'./'+title+'?'+param,
			type:'put'
		}).done(function(res){
			alert(res);
		}).always(function(){
			$('#art'+title).unblock();
		});
	}
	
	//2_2 메모 저장
	function saveMemo(){
		blockById('art2_2');
		var param=$('#art2_2').serialize();
		$.ajax({
			url:'./2_2',
			type:'post',
			data:param
		}).done(function(res){
			alert(res);
		}).always(function(){
			$('#art2_2').unblock();
		});
	}
	//2_2 상담메모 추가
	function addMemo(){
		var ran=$.now()+'';
		var form='<div class="row"><div class="col-xs-12 col-sm-12 col-md-3">';
		form+='<div class="form-group form-group-default"><label for="'+ran+'">상담일</label>';
		form+='<input type="text" class="form-control input-sm" id="'+ran+'" name="mf_log" readonly></div></div>';
		form+='<div class="col-xs-12 col-sm-12 col-md-9 position-relative pr-10">';
		form+='<div class="form-group form-group-default"><label for="mf_memo">상담메모</label>';
		form+='<textarea class="form-control input-sm" style="resize:vertical;background: white;" name="mf_memo"></textarea>';
		form+='<a class="mf-memo-del" onclick=$(this).parent().parent().parent().remove()><i class="icon-x"></i></a>';
		form+='</div></div></div>';

		/* var tr='<tr><th>상담일</th>'
		+'<td><input type="text" class="form-control input-sm" id="'+ran+'" name="mf_log" readonly></td>'
		+'<th>상담메모</th>'
		+'<td><textarea class="form-control input-sm" style="resize:vertical" name="mf_memo"></textarea></td>'
		+'<td><a onclick="$(this).parent().parent().remove();">삭제</a></td></tr>'; */
			
		$('#tb2_2').append(form);
			
		$('#'+ran).daterangepicker({ 
	    	startDate: moment(),
	        singleDatePicker: true
	    });
	}
	
	////불러온 고객 정보를 넣을 form
	var miload='';
	//고객정보 리스트 불러오기
	function getMemberInfo(form){
		miload=form;
		$('#modal_large_title').html('고객정보 불러오기');
		$('#modal_large_content').load('milist');
		$('#modal_large').modal('show');
	}
	//선택된 고객정보 해당 form에 입력
	function memberInfoDetail(){
		var miidx=$(event.target).attr('mi');
		$.ajax('/member/info/'+miidx)
		.done(function(res){
			$('#'+miload+' select[name=mi_id_type]').val(res.mi_id_type);
			$('#'+miload+' input[name=mi_name]').val(res.mi_name);
			$('#'+miload+' input[name=mi_phone]').val(res.mi_phone);
			$('#'+miload+' input[name=mi_tel]').val(res.mi_tel);
			$('#'+miload+' input[name=mi_id]').val(res.mi_id);
			$('#'+miload+' input[name=mi_addr1]').val(res.mi_addr1);
			$('#'+miload+' input[name=mi_addr2]').val(res.mi_addr2);
			$('#'+miload+' input[name=mi_addr3]').val(res.mi_addr3);
			$('#'+miload+' input[name=mi_post]').val(res.mi_post);
			$('#'+miload+' input[name=mi_memo]').val(res.mi_memo);
			
			$('#modal_large').modal('hide');
		});
	}
	////
	    
	//결제종료일 자동계산//
	setPayEnd();
	$('#art5 input[name=m_paystart]').on('apply.daterangepicker',function(ev,picker) {
		$('#art5 input[name=m_paystart]').val(picker.startDate.format('YYYY-MM'));
		setPayEnd();
	});
	function setPayEnd(){
		var m_paystart=$('#art5 input[name=m_paystart]').val();
		var m_period=$('#art5 input[name=m_period]').val();
		var m_paydate=$('#art5 input[name=m_paydate]').val();
		try{
			var payend=new Date(m_paystart);
			if(payend.getDay < m_paydate){
				payend.setDate(m_paydate)
				payend.setMonth(payend.getMonth()+Number(m_period)-2);
			}else if(payend.getDay > m_paydate){
				payend.setDate(m_paydate)
				payend.setMonth(payend.getMonth()+Number(1));
				payend.setMonth(payend.getMonth()+Number(m_period)-2);
			}
				$('#art5 input[name=m_payend]').val((payend.toISOString()).substring(0,7));
			
		}catch(err){
			
		}
	}
	//5.결제정보 >총액 계산
	function setTotal(){
		var m_rental=$('#art5 input[name=m_rental]').val();
		var m_period=$('#art5 input[name=m_period]').val();
		
		var total=Number( onlyNum(m_rental) )*Number( onlyNum(m_period) );
		$('#art5 input[name=m_total]').val(comma(total));
	}
	
	//결제방식에 따른 입력창 view
	if('${m.pay_method}'=='계좌이체'){
		$('.cmsbank').show();
		$('.cmscard').hide();
	}else{
		$('.cmsbank').hide();
		$('.cmscard').show();
	}
	
	payMethod();
	function payMethod(){
		var pay_method=$('#art5 select[name=m_pay_method]').val();
		var url=window.location.pathname;
		url=url.replace('customer','member');
		//정보 로드
		$.ajax(url+'info?mt_code=결제')
		.done(function(res){
			$('#m_cms').html(res.m_cms);
			if(res.m_pay_method==pay_method){
				$('#art5 input[name=m_pay_info]').val(res.m_pay_info);
				$('#art5 input[name=m_pay_owner]').val(res.m_pay_owner);
				$('#art5 input[name=m_pay_num]').val(res.m_pay_num);
				$('#art5 input[name=m_card_year]').val(res.m_card_year);
				$('#art5 input[name=m_card_month]').val(res.m_card_month);
			}else{
				$('#art5 input[name=m_pay_info]').val('');
				//$('#art5 input[name=m_pay_owner]').val('');
				$('#art5 input[name=m_pay_num]').val('');
				$('#art5 input[name=m_card_year]').val('');
				$('#art5 input[name=m_card_month]').val('');
			}
		});
		
		if(pay_method=='계좌이체'){
			$('.cmsInfo').show();
			$('.cmsbank').show();
			$('.cmscard').hide();
		}else if(pay_method=='카드'){
			$('.cmsInfo').show();
			$('.cmsbank').hide();
			$('.cmscard').show();
		}else{
			$('.cmsInfo').hide();
			$('.cmscard').hide();
		}
	}
	//6.녹취계약 파일 목록 가져오기 type=credit,record
	function uploadRecord(type){
		var mf_name=''
		var appendId='';
		if(type=='credit'){
			mf_name='신용정보조회동의서';
			appendId='recordFile';
		}else{
			mf_name='녹취파일';
			appendId='creditFile';
		}
		
		var url=window.location.pathname;
		url=url.replace('customer','member');
		//정보 로드
		$.ajax({
			url:url+'fileList?mf_name='+mf_name,
			async:false
		}).done(function(result){
				var div='';
				for(i=0;i<result.length;i++){
					fileTitle=result[i].mf_name;
					if(fileTitle=='녹취파일'){
						fileTitle+='('+result[i].mf_log+')';
						div+='<div class="col-xs-12 col-sm-4 col-md-3 '+type+'File">';
					}else{
						div+='<div class="col-xs-12 col-sm-4 col-md-3 '+type+'File" id="creditFile">';
					}
					div+='<div class="form-group form-group-default">';
					div+='<div class="input-icon right">';
					div+='<label for="form_control_1">'+fileTitle+'</label>';
					div+='<a class="text-danger-400" onclick="deleteFile('+result[i].mf_idx+')"><i class="icon-cross3"></i></a>';
					div+='<a class="pull-right" href="/file/member/'+result[i].mf_idx+'_'+result[i].mf_memo+'">'+result[i].mf_file+'</a>';
					div+='</div>';
					div+='</div>';
					div+='</div>';
				}
				
				$('.'+type+'File').remove();
				$(div).insertBefore('#'+appendId);
		});
	}
	
	//6.녹취계약 파일 업로드 type=record,credit
	function fileUpload(type){
		if(type=='credit'){
			$('#modal_large_title').html('신용정보조회동의서 업로드');
		}else{
			$('#modal_large_title').html('녹취파일 업로드');
		}
		var input='<form name="art6" id="form6" method="post" enctype="multipart/form-data" class="p-20 clearfix" ><input name="file" type="file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required>';
		input+='<a onclick="uploadArt(\''+type+'\')" class="btn bg-teal-400 mt-20 pull-right">파일 등록</a></form>';
		$('#modal_large_content').html(input);
		$('#modal_large').modal('show');
	}
	//6녹취계약 파일업로드
	function uploadArt(type){
		blockById('form6');
		$('#form6').ajaxForm({
			url:'./6?type='+type,
			enctype:"multipart/form-data",
			success:function(res){
				$('#modal_large').modal('hide');
				uploadRecord(type);
				alert(res);
			},
			complete:function(){
				$('#form6').unblock();
			}
		});
		 $("#form6").submit() ;
	}
	//녹취계약확정
	function recordContract(){
		$.ajax({
			url:'./recordContract',
			type:'put'
		})
		.done(function(res){
			if(res.result>0){
				$('#m_voice_contract').html(res.m_voice_contract);
			}
			alert(res.msg);
		});
	}
	
	//렌탈상품변경모달
	function openChangePr(){
		if(! $.fn.DataTable.isDataTable( '#modalChangeProductTable' )){
			$.extend( $.fn.dataTable.defaults, {
				"columns": [
				    { "data": "pr_name" },
				    { "data": "pr_rental" },
				    { "data": "pr_total" },
				    { "data": "pr_period" },
				    { "data": "pr_ilsi" },
				    { "data": "detail" }
				]
			});
				
			ajaxDt('modalChangeProductTable','/product/rentalProducts');
		}
		$('#modalChangeProduct').modal('show');
	}
	
	function choose(){
		var pr_idx=$(event.target).attr('idx');
		if(window.confirm('렌탈 상품을 변경하시겠습니까?')){
			blockById('insideChangeProduct');
			$.ajax({
				url:'./product?pr_idx='+pr_idx,
				type:'put'
			}).done(function(res){
				alert(res.msg);
				if(res.reload){
					location.reload();
				}
			}).always(function(){
				$('#insideChangeProduct').unblock();
			});
		}
	}
	function delMemo(){
		if(window.confirm('메모를 삭제하시겠습니까?\n저장버튼을 누르셔야 저장이 됩니다.')){
			$(event.target).parent().parent().parent().parent().remove()
		}
	}
	
	function deleteFile(mf_idx){
		if(window.confirm('파일을 삭제하시겠습니까?')){
			$.ajax({
				url:'/file/'+mf_idx,
				type:'delete'
			}).done(function(res){
				alert(res);
				location.reload();
			}).fail(function(){
				alert('통신오류 발생.\n잠시 후 다시 시도해주세요');
			})
		}
	}
	
	function rproductDatail() {
		var pr_idx = $(event.target).attr('pr_idx');
		$('#product').empty();
		$.ajax({
			url : '/product/productDetail/'+pr_idx,
			type : 'get',
			success : function(data) {
				$('#modal_large2').modal('show');
				$('#product').append(data);
			}
		});
	}
	
	</script>
</body>
</html>