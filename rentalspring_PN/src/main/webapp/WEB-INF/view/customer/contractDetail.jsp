<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>계약고객관리>고객 정보</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container" id="topDiv">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">계약고객</span>
							<small class="display-block">정보</small>
						</h4>
					</div>
				</div>
				<!-- /page header -->
				<!-- style="cursor: pointer;" onclick="$('#art1Panel').toggle('show')" -->
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- 신청고객정보-->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>1.신청 고객 정보</h5>
								</div>
								<div class="panel-body" id="art1Panel">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-3 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1"> 렌탈 상품명</label>
													${product.pr_name}
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1"> 판매처</label>
													<p class="no-margin">${m.m_sale}</p> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1"> 등록일</label>
													<p class="no-margin">${mi_apply.mt_log}</p> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1"> 판매 기수</label>
													<p class="no-margin">${mg.mg_name}&emsp;${mg.mgm_cnt}</p> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">상담CS</label>
													<p class="no-margin">${m_damdang.e_name}</p> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">고객번호</label>
													<p class="no-margin">${m.m_num}</p> 
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">신청고객명(${mi_apply.mi_id_type})</label>
													<p class="no-margin">${mi_apply.mi_name}</p> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">연락처</label>
													<p class="no-margin">${mi_apply.mi_phone}</p> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">연락처2</label>
													<p class="no-margin">${mi_apply.mi_tel}</p> 
												</div>
											</div> 
											<div class="col-xs-12 col-sm-12 col-md-9">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">주소</label>
													<p class="no-margin">${mi_apply.mi_addr1}&nbsp;${mi_apply.mi_addr2}&nbsp;${mi_apply.mi_addr3}&nbsp;(${mi_apply.mi_post})</p> 
												</div>
											</div> 
											<div class="col-xs-12">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">메모</label>
													<p class="no-margin">${mi_apply.mi_memo}</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- 신청고객정보 -->
							<!-- 해피콜정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<!-- style="cursor: pointer;" onclick="$('#art2Panel').toggle('show')" -->
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>2.해피콜 정보</h5>
								</div>
						
								<div class="panel-body" id="art2Panel">
									<form id="art2">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>상담 상태</label>
														<p class="no-margin">${m.m_state}</p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>접수구분</label>
														<p class="no-margin">${m.m_jubsoo}</p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>매출구분</label>
														<p class="no-margin">${m.m_detail}</p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>진행상태</label>
														<p class="no-margin">${m.m_process}</p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<label>배송/설치 상태</label>
														<p class="no-margin">${ds.ds_state}</p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default ${m.m_jubsoo ne '무료체험'?'disabled':''}">
														<label>무료체험 일수</label>
														<p class="no-margin">${m.m_free_day}</p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default ${m.m_jubsoo ne '무료체험'?'disabled':''}">
														<label>무료체험 종료일</label>
														<p class="no-margin"></p>
													</div>
												</div>
											</div>
											<c:if test="${not empty art2_2}">
											<div class="row">
												<table class="table table-bordered nowrap">
													<tbody>
														<c:if test="${not empty art2_2}">
														<tr>
															<th class="active" style="width: 20%;">상담일</th>							
															<th class="active">상담메모</th>							
														</tr>
														</c:if>
														<c:forEach items="${art2_2}" var="memo">
														<tr>
															<td>${memo.mf_log}</td>
															<td style="white-space: normal;">${memo.mf_memo}</td>
														</tr>
														</c:forEach>
													</tbody>
													</table>
												<%-- <c:forEach items="${art2_2}" var="memo">
												<div class="col-xs-12 col-sm-4 col-md-4">
													<div class="form-group form-group-default">
														<label for="mf_log">상담일</label>
														<p class="no-margin">${memo.mf_log}</p>
													</div>
												</div>
												<div class="col-xs-12 col-sm-8 col-md-8">
													<div class="form-group form-group-default">
														<label for="mf_memo">상담메모</label>
														${memo.mf_memo}
													</div>
												</div>
												</c:forEach> --%>
											</div>
											</c:if>
										</div>
									</form>						
								</div>
							</div>
							<!--// 해피콜정보 -->
							<!-- 배송 고객 정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>3_1.배송/설치 고객 정보</h5>
								</div> 
								<div class="panel-body">
									<form id="art3_1">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default"> 
														<label for="form_control_1">배송/설치  고객명</label>
														<input type="text" class="form-control input-sm" name="mi_name" value="${mi_delivery.mi_name}"> 
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 "> 
													<div class="form-group form-group-default">
														<label for="form_control_1">연락처1</label>
														<input type="text" class="form-control input-sm" name="mi_phone" value="${mi_delivery.mi_phone}"> 
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 " >
													<div class="form-group form-group-default">
														<label for="form_control_1">연락처2</label>
														<input type="text" class="form-control input-sm" name="mi_tel" value="${mi_delivery.mi_tel}"> 
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default"> 
														<label for="form_control_1">배송/설치  소1</label>
														<input type="text" class="form-control input-sm" name="mi_addr1" value="${mi_delivery.mi_addr1}" readonly="readonly"> 
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default"> 
														<label for="form_control_1">배송/설치 주소2</label>
														<input type="text" class="form-control input-sm" name="mi_addr2" value="${mi_delivery.mi_addr2}" readonly="readonly"> 
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default"> 
														<label for="form_control_1">우편번호</label>
														<input type="text" class="form-control input-sm" name="mi_post" value="${mi_delivery.mi_post}" readonly="readonly"> 
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6">
													<div class="form-group form-group-default"> 
														<label for="form_control_1">상세주소<a class="badge bg-orange-300 position-right" onclick="openAddress('art3_1')">검색</a></label>
														<input type="text" class="form-control input-sm" name="mi_addr3" value="${mi_delivery.mi_addr3}"> 
													</div>
												</div> 
												<div class="col-xs-12">
													<div class="form-group form-group-default"> 
														<label for="form_control_1">배송/설치 메모</label>
														<textarea rows="5" class="form-control input-sm" style="resize:vertical;background: white;height: 60px;" name="mi_memo">${fn:replace(mi_delivery.mi_memo,'<br>','&#10;')}</textarea>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn bg-teal-400 pull-right position-right" onclick="saveDeliveryInfo()">저장 </a>
											<a class="btn bg-orange-800 pull-right" onclick="getMemberInfo()">고객정보 불러오기 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 배송 고객 정보 -->
							<!-- 배송/회수/반품 정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>3_2.배송/회수/반품 정보</h5>
								</div>
								<div class="panel-body">
									<form id="art3_2">
										<div class="form-group-attached" id="artDs">
											<%@include file="contractDetailDeliveryState.jsp" %>
										</div>
									</form>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pl-15">
											<a onclick="refreshDs()" class="btn bg-teal-400 text-left">배송정보 새로고침</a>
										</div>
										<div class="heading-btn pull-right">
											<a onclick="deliveryState('delivery')" ds="${ds.ds_idx }" class="btn bg-teal-400 pull-right mr-5">저장</a>	
											<c:choose>
												<c:when test="${ds.ds_state eq '구매취소' }">
													<a onclick="deliveryState('cancelcancel')" ds="${ds.ds_idx }" class="btn bg-grey pull-right mr-5">구매취소 취소</a>
												</c:when>
												<c:otherwise>
													<a onclick="deliveryState('cancel')" ds="${ds.ds_idx }" class="btn bg-grey pull-right mr-5">구매 취소</a>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</div>
							<!--// 배송/회수/반품 정보 -->
							<!-- 계약 고객 정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>4.계약 고객 정보</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">계약완료일</label>
													<p class="no-margin">${m.m_contract_date}</p> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">고객 번호</label>
													<p class="no-margin">${m.m_num} </p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">계약 고객명(${mi_contract.mi_id_type})</label>
													<p class="no-margin">${mi_contract.mi_name} </p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">연락처</label>
													<p class="no-margin">${mi_contract.mi_phone} </p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">주민번호</label>
													<p class="no-margin">${mi_contract.mi_id} </p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">신용조회등급</label> 
													<p class="no-margin">${m.m_cb}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-6">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">계약서 수령 주소</label>
													<p class="no-margin">${mi_contract.mi_addr1}&nbsp;${mi_contract.mi_addr2}&nbsp;${mi_contract.mi_addr3}&nbsp;(${mi_contract.mi_post})</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--// 계약 고객 정보 -->
							<!-- 결제정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>5.결제정보</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">이체 희망일</label>
													<p class="no-margin">${m.m_paydate} </p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">이체월수</label>
													<p class="no-margin">${m.m_period} </p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1">월이체액</label>
													<p class="no-margin">${m.m_rental}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1">총액</label>
													<p class="no-margin">${m.m_total}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">결제시작월</label>
													<p class="no-margin">${m.m_paystart} </p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">결제종료일</label> 
													<p class="no-margin">${m.m_pay_finish}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1">일시불</label>
													<p class="no-margin">${m.m_ilsi_f}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1">CMS 상태</label>
													<p class="no-margin">${m.m_cms}</p>
												</div>
											</div>
											<!-- <div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">부분 일시불</label>
													<p class="no-margin"></p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1">부분 일시불 할인율</label>
													<p class="no-margin"></p>
												</div>
											</div> -->
											
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1">결제방식</label>
													<p class="no-margin">${m.m_pay_method}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1"><span class="cmscard">카드명</span><span class="cmsbank">은행명</span></label>
													<p class="no-margin">${m.m_pay_info }</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1"><span class="cmscard">카드소유자명</span><span class="cmsbank">예금주명</span></label>
													<p class="no-margin">${m.m_pay_owner}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1">연락처(현금영수증)</label>
													<p class="no-margin">${m.m_phone}</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 ">
												<div class="form-group form-group-default">
													<label for="form_control_1"><span class="cmscard">카드번호</span><span class="cmsbank">계좌번호</span></label>
													<p class="no-margin">${m.m_pay_num }</p>
											</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 cmscard">
												<div class="form-group form-group-default">
													<label for="form_control_1">카드유효 월</label>
													<p class="no-margin">${m.m_card_month }</p>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2 cmscard">
												<div class="form-group form-group-default">
													<label for="form_control_1">카드유효 년</label>
													<p class="no-margin">${m.m_card_year }</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--// 결제정보 -->
							<!-- 가상계좌 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300" id="art6">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>6.가상계좌</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row" id="artV">
											<%@include file="comp/artV.jsp"%>
										</div>
									</div>
								</div>
							</div>
							<!--// 가상계좌  -->
							<!-- 녹취 계약 완료 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>7.녹취 계약 완료</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-4">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">녹취 계약 완료일</label>
													<span id="m_voice_contract">${m.m_voice_contract}</span> 
												</div>
											</div>
											<c:forEach items="${records}" var="record">
											<div class="col-xs-12 col-sm-4 col-md-4 recordFile">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">녹취파일(${record.mf_log})</label>
													<a class="text-left" href="/file/member/${record.mf_idx}_${record.mf_memo}">${record.mf_file}</a> 
												</div>
											</div>
											</c:forEach>
											<div class="col-xs-12 col-sm-4 col-md-4 creditFile" id="creditFile">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">신용정보조회동의서</label>
													<c:if test="${empty credits}"><a class="badge bg-orange-300" onclick="fileUpload('credit')">파일등록</a></c:if>
													<c:if test="${not empty credits}">
														<a class="text-left" href="/file/member/${credits[0].mf_idx}_${credits[0].mf_memo}">${credits[0].mf_file}</a>
														<a class="badge bg-danger pull-right" onclick="fileUpload('credit')">파일재등록</a>
													</c:if>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--// 녹취 계약 완료 -->
							<!-- 최종 계약 확정 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300" id="art8">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>8.최종 계약 확정</h5>
								</div>
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">최종 계약 상태</label>
													<c:choose>
														<c:when test="${empty m.m_final_date or m.m_final_date eq ''}"><span id="finalState">미확정</span></c:when>
														<c:otherwise><span id="finalState">확정</span></c:otherwise>
													</c:choose>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">최종 계약 확정일</label>
													<span id="finalDate">${m.m_final_date }</span> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">렌탈 약정서(인증용, 이미지파일)</label>
													<c:if test="${not empty agree}">
														<a class="text-left" href="/file/member/${agree[0].mf_idx}_${agree[0].mf_memo}">${agree[0].mf_file}</a>
														<a class="badge bg-danger pull-right" onclick="fileUpload('rental')">파일재등록</a>
													</c:if>
													<c:if test="${empty agree}">
														<a class="badge bg-orange-300" id="mf_rental" onclick="fileUpload('rental')">파일등록</a>
													</c:if> 
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<div class="form-group form-group-default"> 
													<label for="form_control_1">양도통지서</label>
													<c:if test="${not empty transfer}">
														<a class="text-left" href="/file/member/${transfer[0].mf_idx}_${transfer[0].mf_memo}">${transfer[0].mf_file}</a>
														<a class="badge bg-danger pull-right" onclick="fileUpload('notice')">파일재등록</a>
													</c:if>
													<c:if test="${empty transfer}"><a class="badge bg-orange-300" id="mf_notice" onclick="fileUpload('notice')">파일등록</a></c:if> 
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a onclick="finalContract()" class="btn bg-teal-400 pull-right position-right">최종 계약 완료</a>
											
											<c:set var="paidComplete">렌탈</c:set>
											<c:if test="${m.m_detail eq '일시불'}">
											<c:set var="paidComplete">일시불</c:set>
											</c:if>
											<a onclick="paidComplete()" class="btn bg-teal-400 pull-right position-right">${paidComplete} 완료</a>
											
											<a onclick="joongdo()" class="btn bg-orange-800 pull-right position-right" data-toggle="tooltip" data-placement="top" title="배송/설치완료일이 있어야합니다." data-html="true">중도 상환</a>
											<c:choose>
												<c:when test="${ds.ds_state eq '구매취소' }">
													<a onclick="deliveryState('cancelcancel')" ds="${ds.ds_idx }" class="btn bg-grey pull-right">구매취소 취소</a>
												</c:when>
												<c:otherwise>
													<a onclick="deliveryState('cancel')" ds="${ds.ds_idx }" class="btn bg-grey pull-right">구매 취소</a>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</div>
							<!--// 최종 계약 확정 -->
							
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<!-- style="cursor: pointer;" onclick="$('#art3Panel').toggle('show')" -->
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="월 렌탈료 X {(의무사용일수 - 사용일수) / 30} X 30%<br>10원 미만은 절사" data-html="true"></i>
										9.위약금 계산
									</h5>
								</div> 
								<div class="panel-body">	  
									<div class="form-group-attahced">
										<div class="row">
											<div class="col-xs-12">
												<table class="table table-bordered nowrap">
													<tbody>
														<tr>
															<th class="active">렌탈 개월수</th>
															<td>${m.m_period}</td>
															<th class="active">월 렌탈금액</th>
															<td>${m.m_rental_f}</td>
															
														</tr>
														<tr>
															<th class="active">설치/배송 완료일</th>
															<td>${ds.ds_date}</td>
															<th class="active">위약금 계산 기준일</th>
															<td>
																<input type="text" class="form-control input-sm daterange-single text-center" name="penaltyDate" value="${today}" readonly="readonly" style="background: none;">
															</td>
														</tr>
														<tr>
															<th class="active">배송/설치상태</th>
															<td>${ds.ds_state}</td>
															<th class="active">위약금</th>
															<td id="penaltyMoney"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div> 
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<%@include file="modal/rProductDetail.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<!-- file input -->
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>	
	<script type="text/javascript" src="/assets/js/pages/uploader_bootstrap.js"></script>	
	<!--// file input -->
	<script>
	//1. 신청고객 정보. 다음 주소
	function openAddress(formId){
		new daum.Postcode({
		    oncomplete: function(data) {
		      	var sido=data.sido;
		    	var sigungu=data.sigungu;
		    	var roadname=data.address.replace(sido,'');
		    	roadname=roadname.trim();
		    	roadname=roadname.replace(sigungu,'');
		    	roadname=roadname.trim();
		    	var zonecode=data.zonecode;
		    	$('#'+formId+' input[name=mi_addr1]').val(sido);
		    	$('#'+formId+' input[name=mi_addr2]').val(sigungu);
		    	$('#'+formId+' input[name=mi_addr3]').val(roadname);
		    	$('#'+formId+' input[name=mi_post]').val(zonecode);
		    }
		}).open({popupName:'daumAddr'});
	}
	
	//3_1 배송 고객 정보 저장
	function saveDeliveryInfo(){
		blockById('art3_1');
		var param=$('#art3_1').serialize();
		var url=window.location.pathname;
		url=url.replace('/contract','');
		$.ajax({
			url:url+'4?'+param,//상담고객에서 쓰는 배송정보 저장 url
			type:'put'
		}).done(function(res){
			alert(res);
			refreshDs();
		}).always(function(){
			$('#art3_1').unblock();
		});
	}
	//고객정보 리스트 불러오기
	function getMemberInfo(){
		$('#modal_large_title').html('고객정보 불러오기');
		var url=window.location.pathname;
		url=url.replace('/contract','');
		$('#modal_large_content').load(url+'milist',function(){
			$('#modal_large').modal('show');
		});
	}
	//3_1 배송고객 주소에 불러온 고객 주소 넣기
	function memberInfoDetail(){
		var miidx=$(event.target).attr('mi');
		$.ajax('/member/info/'+miidx)
		.done(function(res){
			$('#art3_1 input[name=mi_name]').val(res.mi_name);
			$('#art3_1 input[name=mi_phone]').val(res.mi_phone);
			$('#art3_1 input[name=mi_tel]').val(res.mi_tel);
			$('#art3_1 input[name=mi_addr1]').val(res.mi_addr1);
			$('#art3_1 input[name=mi_addr2]').val(res.mi_addr2);
			$('#art3_1 input[name=mi_addr3]').val(res.mi_addr3);
			$('#art3_1 input[name=mi_post]').val(res.mi_post);
			
			$('#modal_large').modal('hide');
		});
	}
	////
	    
	//결제종료일 자동계산//
	setPayEnd();
	function setPayEnd(){
		var m_paystart=$('#art5 input[name=m_paystart]').val();
		var m_paydate = $('#art5 input[name=m_paydate]').val();
		var m_period=$('#art5 input[name=m_period]').val();
		try{
			var payend=new Date(m_paystart);
			payend.setMonth(payend.getMonth()+Number(m_period)-1);
			$('#art5 input[name=m_payend]').val( (payend.toISOString()).substring(0,7) );
		}catch(err){
			
		}
	}
	
	//결제방식에 따른 입력창 view
	if('${m.m_pay_method}'=='계좌이체'){
		$('.cmsbank').show();
		$('.cmscard').hide();
	}else{
		$('.cmsbank').hide();
		$('.cmscard').show();
	}
	
	//배송정보 저장 및 구매취소
	function deliveryState(req){
		var param=$('#art3_2').serialize();
		var ds_idx=$(event.target).attr('ds');
		
		var url=window.location.pathname;
		
		var ques='저장';
		if(req=='cancel'){
			ques='구매취소';
		}else if(req=='cancelcancel'){
			ques='구매취소 취소';
		}
		if(window.confirm(ques+' 하시겠습니까?')){
			blockById('art3_2');
			$.ajax({
				url:'/delivery/'+ds_idx+'?ds_state='+req+'&'+param+'&url='+url,
				type:'put'
			}).always(function(result){
				alert(result);
				refreshDs();
			});
		}
	}
	//배송정보 새로고침
	function refreshDs(){
		var url=window.location.pathname;
		url=url.replace('/contract','');
		
		$('#art3_2').unblock();
		$.ajax(url+'delivery_state')
		.done(function(res){
			$('#artDs').html(res);
			
			$('.dsdate').daterangepicker({
		    	autoUpdateInput: false,
		    	startDate: moment(),
		        singleDatePicker: true
		    });
			
			$('.dsdate').on('apply.daterangepicker', function(ev, picker) {
			    $(this).val(picker.startDate.format('YYYY-MM-DD'));
			});
		});
	} 
	
	//8.파일 업로드 type= rental,notice
	function fileUpload(type){
		
		var mf=$('#mf_'+type).attr('mf');
		if(mf!=undefined){
			location.href=mf;
			return;
		}
		
		if(type=='rental'){
			$('#modal_large_title').html('렌탈 약정서 업로드');
		}else if(type=='notice'){
			$('#modal_large_title').html('양도 통지서 업로드');
		}else{
			$('#modal_large_title').html('신용 정보 조회 동의서 업로드');
		}
		var input='<form id="form8" method="post" enctype="multipart/form-data" class="p-15"><div class="file-modal-upload position-relative"><input name="file" type="file" class="file-input-ajax" data-show-upload="false" data-show-caption="false" data-show-preview="true" data-main-class="input-group-xxs" required></div>';
		input+='<div class="text-right mt-15"><a onclick="uploadArt(\''+type+'\')" class="btn btn-sm bg-orange-800">파일 등록</a></div></form>';
		$('#modal_large_content').html(input);
		$('#modal_large > .modal-dialog').removeClass('modal-lg');
		$('#modal_large').modal('show');		

		// file input reload
		$('.file-input-ajax').fileinput('refresh', {
			browseLabel: ' file', 
			browseClass: 'btn btn-xs btn-primary',
			browseIcon: '<i class="icon-file-plus text-size-small"></i>',
	        removeLabel: '',
	        removeClass: 'btn btn-xs btn-danger',
	        removeIcon: '<i class="icon-cross3"></i>',
	        maxFilesNum: 1,
	        maxFileCount: 1
		});
		
	}
	//8. 파일업로드
	function uploadArt(type){
		blockById('art8');
		$('#form8').ajaxForm({
			url:'./8?type='+type,
			enctype:"multipart/form-data",
			success:function(res){
				$('#modal_large').modal('hide');
				alert(res.msg);
				if(res.result=='success'){
					location.reload();
					/* $('#mf_'+type).attr('mf','/file/member/'+res.mf);
					$('#mf_'+type).html(res.mf_file);
					$('#mf_'+type).removeClass('badge bg-orange-300'); */
				}
			},
			complete:function(){
				$('#art8').unblock();
			}
		});
		 $("#form8").submit() ;
	}
	
	//최종계약확정
	function finalContract(){
		var url=window.location.pathname;
		url=url.replace('/contract','');
		url+='final';
		blockById('topDiv');
		
		$.ajax(url)
		.done(function(res){
			alert(res.msg);
			if(res.msg.trim()=='최종계약확정했습니다.'){
				$('#finalState').html('확정');
				$('#finalDate').html(res.date);
			}
		}).always(function(){
			$('#topDiv').unblock();
		});
		
	}
	//중도상환
	function joongdo(){
		if(window.confirm('중도상환 하시겠습니까?')){
			blockById('art8');
			$.ajax('./joongdo')
			.done(function(res){
				alert(res);
				$('#art8').unblock();
			});
		}
	}
	function rproductDatail() {
		var pr_idx = $(event.target).attr('pr_idx');
		$('#product').empty();
		$.ajax({
			url : '/product/productDetail/'+pr_idx,
			type : 'get',
			success : function(data) {
				$('#modal_large2').modal('show');
				$('#product').append(data);
			}
		});
	}
	
	penalty();
	$('input[name=penaltyDate]').on('apply.daterangepicker',function(ev,picker) {
		$('input[name=penaltyDate]').val(picker.startDate.format('YYYY-MM-DD'));
		penalty();
	});
	//위약금계산
	function penalty(){
		var m_rental='${m.m_rental}';
		var m_period='${m.m_period}';
		if(m_rental==''){
			m_rental='0';
		}
		m_rental=Number(onlyNum(m_rental));
		if(m_period==''){
			m_period='0';
		}
		m_period=Number(onlyNum(m_period));
		
		var sd='${ds.ds_date}';//설치완료일
		var pd=$('input[name=penaltyDate]').val();

		if(sd!=''){
			var sd_date=new Date(sd);
			var pd_date=new Date(pd);
			
			//diff=사용일수
			var diff=pd_date.getTime() - sd_date.getTime();
		    diff = Math.ceil(diff / (1000 * 3600 * 24));
			if(diff<0){
				$('#penaltyMoney').html('위약금 계산 기준일이 설치/배송완료일 이전입니다.');
			}else{
				var pen=m_rental*(m_period-(Number(diff)/30) )*0.3;
				pen=Math.floor(pen/10)*10;
				if(pen<0){
					$('#penaltyMoney').html('0');
				}else{
					$('#penaltyMoney').html( comma(pen));
				}
			}
		}else{
			$('#penaltyMoney').html('설치/배송완료일이 없습니다.');
		}
	}
	
	//일시불 완료,렌탈완료
	function paidComplete(){
		if(window.confirm('완료처리하시겠습니까?\nCMS 삭제 처리됩니다.')){
			$.ajax({
				url : './paid',
				type : 'put',
				success : function(data) {
					alert(data);
				}
			});
		}
	}
	
	$('[data-toggle=tooltip]').tooltip();
	</script>
</body>
</html>