<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form name="vaccountForm">
	<input type="hidden" name="v_idx" value="${vaccount.v_idx}">
	<table class="table table-bordered nowrap">
		<tr>
			<th width="10%" class="active">가상계좌 등록 상태</th>
			<td width="23%">${vaccount.v_condition}</td>
			<th width="10%" class="active">등록일</th>
			<td width="23%">${vaccount.v_regi_date }</td>
			<th width="10%" class="active">사유</th>
			<td width="23%">${vaccount.v_fail}</td>
		</tr>
		<tr>
			<th class="active">가상계좌은행</th>
			<td>
				<select class="select select2-hidden-accessible artVsel" name="v_bank" onchange="v_select()">
					<option value="">선택</option>
					<c:forEach var="vaccountBank" items="${vaccountBank}">
						<c:if test="${vaccountBank.sb_v != ''}">
						<option value="${vaccountBank.sb_v}" ${vaccount.v_bank eq vaccountBank.sb_v?'selected':'' }>${vaccountBank.sb_name}</option>
						</c:if>
					</c:forEach>
				</select>
			</td>
			<th class="active">가상계좌 TYPE</th>
			<td>
				<select class="select select2-hidden-accessible artVsel" name="v_reuse">
					<option value="1" ${vaccount.v_reuse eq 1?'selected':''}>1회용</option>
					<option value="2" ${vaccount.v_reuse eq 2?'selected':''}>재사용</option>
				</select>
			</td>
			<th class="active">만료일</th>
			<td>
				<input class="form-control" type="text" name="v_end_date" value="${vaccount.v_end_date}" readonly style="background:none;">
			</td>
		</tr>
		<tr>
			<th class="active">입금 회차</th>
			<td>
				<select class="select select2-hidden-accessible artVsel" name="v_count" onchange="vmoney()">
					<c:forEach begin="1" end="${unpaid}" var="count">
					<option value="${count}">${count}회차분</option>
					</c:forEach>
				</select>
			</td>
			<th class="active">입금 금액</th>
			<td>
				<input class="form-control input-sm" type="text" name="v_money" value="${vaccount.v_money}" onkeyup="numCommaKeyUp()">
			</td>
			<th class="active">예금주명</th>
			<td>
				<input class="form-control input-sm" type="text" name="v_name" value="${vaccount.v_name}">
			</td>
		</tr>
		<tr>
			<th class="active">입금은행</th>
			<td>
				<c:choose>
					<c:when test="${empty vaccount.v_cms_idx}">
					<select class="select select2-hidden-accessible artVsel" name="v_cms_idx">
						<c:forEach items="${cmsList}" var="cms">
						<option value="${cms.cms_idx}" title="${cms.cms_memo}" ${m.cr_cms_idx eq cms.cms_idx ?'selected':'' }>${cms.cms_bank}</option>
						</c:forEach>
					</select>
					</c:when>
					<c:otherwise>
						<input type="hidden" name="v_cms_idx" value="${vaccount.v_cms_idx}">
						<p class="no-margin">${vaccount.cms_bank}</p>
					</c:otherwise>
				</c:choose>
			</td>
			<th class="active">현금영수증<br><small>(전화번호, 카드번호)</small></th>
			<td>
				<input class="form-control input-sm" type="text" name="v_receipt" value="${vaccount.v_receipt}">
			</td>
			<th class="active">가상계좌번호</th>
			<td>${vaccount.v_account }</td>
		</tr>
		<tr id="vBttn">
			<th class="active" colspan="6">
				<div class="col-xs-3">
					<a onclick="v_regi();" class="col-xs-3 btn bg-purple-300 btn-block">가상계좌발급</a>
				</div>
				<div class="col-xs-3">
					<a onclick="v_sms()" class="col-xs-3 btn bg-teal-300 btn-block">문자전송</a>
				</div>	
				<div class="col-xs-3">
					<a onclick="v_refresh()" class="col-xs-3 btn bg-primary-300 btn-block">새로고침</a>
				</div>
				<div class="col-xs-3">
					<a onclick="v_del()" class="col-xs-3 btn bg-grey-600 btn-block">삭제</a>
				</div>
			</th>
		</tr>	
	</table>
</form>
<!-- <table class="table table-bordered width-100">
	<tr id="vBttn">
		<th class="active"><a onclick="v_regi();" class="btn bg-purple-300 btn-block">가상계좌발급</a></th>
		<th class="active"><a onclick="v_sms()" class="btn bg-teal-300 btn-block">문자전송</a></th>
		<th class="active"><a onclick="v_refresh()" class="btn bg-primary-300 btn-block">새로고침</a></th>
		<th class="active"><a onclick="v_bankChange()" class="btn bg-primary-300 btn-block">회원은행 변경</a></th>
		<th class="active"><a onclick="v_del()" class="btn bg-grey-600 btn-block">삭제</a></th>
	</tr>								
</table> -->

<div id="modal_v" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal">
		<div class="modal-content-wrap">
			<div class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i>문자 발송<span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<form name="smsForm">
						<div class="panel pt-10" style="margin-bottom: 40px;">
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group form-group-default">
										<label>보내는 번호</label>
										<input type="text" name="sms_from" class="form-control">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group form-group-default">
										<label>받는 번호</label>
										<input type="text" name="sms_to" class="form-control">
									</div>
								</div>
								<div class="col-xs-12">
									<textarea rows="5" class="form-control input-sm" style="resize:vertical;" name="sms_content"></textarea>
									<a class="btn bg-teal-300 pull-right mt-10" onclick="sendSms()">문자 전송</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function v_refresh(){
		$('#artV').load('vAccount');
	}
	
	//날짜
	$('input[name=v_end_date]').daterangepicker({
	   	autoUpdateInput: false,
	   	startDate: moment(),
	    singleDatePicker: true
	});
	
	$('input[name=v_end_date]').on('apply.daterangepicker', function(ev, picker) {
	    $(this).val(picker.startDate.format('YYYY-MM-DD'));
	});
	
	//가상계좌 은행 선택시
	function v_select(){
		var v_bank=$('select[name=v_bank]').val();
		$('#artV').load('vAccount?v_bank='+v_bank);
		$('.artVsel').select2({
			minimumResultsForSearch: Infinity
		});
	}
	$('.artVsel').select2({
		minimumResultsForSearch: Infinity
	});
	function vmoney(){
		var vm='${vaccount.v_money}';
		vm=onlyNum(vm);
		vm=Number(vm);
		var time=Number($('select[name=v_count]').val());
		$('input[name=v_money]').val( comma(vm*time) );
	}
	//가상계좌 등록
	function v_regi(){
		var param=$('form[name=vaccountForm]').serialize();
		blockById('artV');
		$.ajax({
			url:'vAccount',
			type:'post',
			data:param
		}).done(function(res){
			alert(res.msg);
			$('#artV').load('vAccount?v_bank='+res.v_bank);
		}).always(function(){
			$('#artV').unblock();
		});
	}
	//가상계좌삭제
	function v_del(){
		if($('input[name=v_idx]').val()==''){
			alert('가상계좌 정보가 없습니다.');
			return;
		}
		var param=$('form[name=vaccountForm]').serialize();
		blockById('artV');
		$.ajax({
			url:'vAccount?'+param,
			type:'delete'
		}).done(function(res){
			alert(res.msg);
			$('#artV').load('vAccount?v_bank='+res.v_bank);
		}).always(function(){
			$('#artV').unblock();
		});
	}

	//가상계좌 문자 모달
	function v_sms(){
		var param='v_bank='+$('select[name=v_bank]').val();;
		blockById('artV');
		$.ajax({
			url:'smsV?'+param,
			type:'get',
			async:false
		}).done(function(res){
			$('#artV').unblock();
			if(res.msg=='null'){
				alert('등록된 가상계좌가 없습니다.');
			}else{
				$('input[name=sms_from]').val(res.send);
				$('input[name=sms_to]').val(res.phone);
				$('textarea[name=sms_content]').val(res.msg);
				$('#modal_v').modal('show');
			}
		}).always(function(){
			$('#artV').unblock();
		});
	}
	
	//가상계좌 문자 발송
	function sendSms(){
		blockById('modal_v');
		var param=$('form[name=smsForm]').serialize();
		$.ajax({
			url:'smsV',
			type:'post',
			async:false,
			data:param
		}).done(function(res){
			alert(res);
		}).always(function(){
			$('#modal_v').unblock();
		});
	}
</script>