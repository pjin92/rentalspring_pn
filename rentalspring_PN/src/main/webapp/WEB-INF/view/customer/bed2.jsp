<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,java.text.SimpleDateFormat,java.util.Date" %>
<%@ page import = "java.util.GregorianCalendar" %>
<%@ page import="org.json.simple.*"%>
<%@ page import="java.text.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="sysYear"><fmt:formatDate value="${now}" pattern="yyyy" /></c:set> 
<c:set var="sysMonth"><fmt:formatDate value="${now}" pattern="MM" /></c:set> 
<c:set var="sysDay"><fmt:formatDate value="${now}" pattern="dd" /></c:set> 
<html lang="ko">    
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<style type="text/css">
		p {margin:0;}
		ol {padding-left:0;}
		ol li {list-style-type:none;}
		.print-content-inner {padding:40px;}
		.terms-order {font-size:13px; line-height:20px;}
	</style>
</head>
<body class="bg-white">
	<div class="clearfix hidden-print mt-20 mb-20">
		<div class="text-right pr-15">
			<a class="btn btn-primary" onclick="javascript:window.print();">출력</a>
			<a class="btn btn-default" onclick="javascript:window.close();">닫기</a>
		</div>
	</div>
	<c:set var="info" value="${info}"/>

	<!-- BEGIN PAGE CONTENT INNER -->
	<div class="page-content-area">
		<div class="print-content-inner">
			<div class="text-center">
				<img src="/img/logo.png" class="text-center" alt="CI" width="120">
				<h5 class="mt-15"><strong>고객님께 알리는 말씀</strong><br><small>(렌탈채권 양도 통지)</small><br></h5>			
			</div>
			<div class="print_content mt-50">
				<h5><i class="icon-square"></i> <strong>수신인</strong> : ${info.mi_name} 고객님 귀하</h5>
				<h6>${info.addr}</h6>
					
				<h5><i class="icon-square"></i> <strong>발신인</strong> : 주식회사 제너럴네트</h5>
				<h6>서울특별시 강남구 봉은사로 29길 48(논현동) 제너럴네트빌딩 3층</h6>
			</div>
			<div class="print_content mt-50">
				<p class="terms-order">
				저희 회사는 ${info.pr_name}에 대한 고객님의 따뜻한 사랑과 성원에 머리 숙여 깊이 감사
				드리며 전임직원이 불철주야 노력하여 보다 나은 품질과 서비스로 고객님께 보답할 것을 약속 
				드립니다.<br><br>
				고객님께서 약정하여 주신 렌탈상품의 약정기간은 상품수령일로부터 ${info.m_period}개월이고 렌탈요금은 수령						
				익월부터 매월 결제가 되어 ${info.m_period}회차까지 납부가 완료되면 렌탈상품의 소유권은 고객님께 무상						
				으로 이전 됩니다.  아무쪼록 렌탈상품을 사용하시는 동안 아무런 불편함이 없도록 저희 회사						
				는 유지관리 및 AS에 만전을 기할 것 입니다.<br><br>
				또한 저희 회사는 고객님의 성원에 힘입은 매출 신장에 따라 새로운 고객님들께 보다 빠르고 						
				안전한 렌탈서비스를 제공해 드리고자 금융기관 우리은행 언주역지점과 협약을 맺어 회사의 렌탈채권을 유동화						
				하였습니다. 이에 따라 고객님께서 저희 회사와 체결 하신 ${info.pr_name} 렌탈약정에 따른 렌탈수수료의 						
				수령권리가 우리은행 언주역지점으로 양도 되었습니다. <br><br>
				그러나 고객님께서는 약정상의 납부 방법 그대로 동일한 렌탈수수료를 계속 납부해 주시면						
				되므로 채권양도에 따른 불편은 전혀 없으십니다.<br><br>
				금번 통지는 저희 회사가 고객님과 함께 더욱 성장하기 위해 도움을 주는 금융기관과 협약을						
				맺은데 따른 것이오니 앞으로도${info.pr_name }에 대한 따뜻한 관심과 사랑을 부탁 드리며 다시 한번 						
				고객님께 고개 숙여 감사의 말씀을 올립니다.		
				</p>
				<table class="printtable printtable_noline" style="width:50%;margin:80px auto 0">
					<tbody>
						<tr>
							<td colspan="3" style="font-size:15px;"><c:out value="${sysYear}"/>년 <c:out value="${sysMonth}" />월 <c:out value="${sysDay}" />일 </td>
						</tr>
						<tr>
							<td colspan="3"><h5 class="no-margin" style="letter-spacing:5"><strong>주식회사 제너럴네트</strong></h5></td>
						</tr>
						<tr>
							<td width="20%" style="text-align:right;letter-spacing:5"><h5 class="no-margin"><strong>대표이사</strong></h5></td>
							<td width="20%"><h5 class="no-margin"><strong>송강호</strong></h5></td>
							<td width="20%" style="text-align:left;"><img src="/img/sign.png" width="50" height="auto"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT INNER -->  
	<div class="clearfix hidden-print mt-20 mb-20">
		<div class="text-right pr-15">
			<a class="btn btn-primary" onclick="javascript:window.print();">출력</a>
			<a class="btn btn-default" onclick="javascript:window.close();">닫기</a>
		</div>
	</div>
	   
	<%@include file="/WEB-INF/view/main/js.jsp"%>
</body>
</html>
