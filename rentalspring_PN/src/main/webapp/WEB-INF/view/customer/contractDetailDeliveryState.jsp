<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="delType">
	<c:choose>
		<c:when test="${m.pr_type eq 1}">배송</c:when>
		<c:otherwise>설치</c:otherwise>
	</c:choose>
</c:set>
<c:set var="retrunType">
	<c:choose>
		<c:when test="${m.m_jubsoo eq '무료체험' and twoWeeks eq 'before'}">회수</c:when>
		<c:when test="${twoWeeks eq 'before' or twoWeeks eq 'none'}">반품</c:when>
		<c:otherwise>해지 반품</c:otherwise>
	</c:choose>
</c:set>

<div class="row mb-20">
	<div class="col-xs-12 mb-5">
		<h6 class="panel-title"><i class="icon-checkmark4 position-left"></i>${delType} 정보</h6>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3 disabled">
		<div class="form-group form-group-default"> 
			<label for="form_control_1">${delType} 상태</label>
			 <p class="no-margin">${ds.ds_state}</p> 
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">${delType}요청일</label>
			<div class="input-group">
				<span class="input-group-btn">
					<a class="text-danger-400" onclick="$('input[name=ds_req]').val('');"><i class="icon-cross3"></i></a>
				</span>
				<input type="text" class="form-control input-sm text-right daterange-single dsdate" name="ds_req" value="${ds.ds_req}" readonly="readonly">
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">택배사</label>
			<input type="text" class="form-control input-sm text-right" name="ds_company" value="${ds.ds_company}">
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">송장번호</label>
			<input type="text" class="form-control input-sm text-right" name="ds_num" value="${ds.ds_num}">
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">${delType}완료일</label>
			<div class="input-group">
				<span class="input-group-btn">
					<a class="text-danger-400" onclick="$('input[name=ds_date]').val('');"><i class="icon-cross3"></i></a>
				</span>
				<input type="text" class="form-control input-sm text-right daterange-single dsdate" name="ds_date" value="${ds.ds_date}" readonly="readonly">
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">Serial No.</label>
			<input type="text" class="form-control input-sm text-right" name="ds_serial" value="${ds.ds_serial}">
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">계약 완료일</label>
			<div class="input-group">
				<span class="input-group-btn">
					<a class="text-danger-400" onclick="$('input[name=m_contract_date]').val('');"><i class="icon-cross3"></i></a>
				</span>
				<input type="text" class="form-control input-sm text-right daterange-single dsdate" name="m_contract_date" value="${m.m_contract_date}" readonly="readonly">
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default ${m.m_jubsoo eq '무료체험'?'':'disabled' }">
			<label for="form_control_1">무료 체험일</label>
			<input type="text" class="form-control input-sm text-right" name="m_free_day" value="${m.m_free_day}" ${m.m_jubsoo eq '무료체험'?'':'readonly' }>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 mb-5">
		<h6 class="panel-title"><i class="icon-checkmark4 position-left"></i>${retrunType} 정보</h6>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">${retrunType} 요청일</label>
			<div class="input-group">
				<span class="input-group-btn">
					<a class="text-danger-400" onclick="$('input[name=ds_req_cancel]').val('');"><i class="icon-cross3"></i></a>
				</span>
				<input type="text" class="form-control input-sm text-right daterange-single dsdate" name="ds_req_cancel" value="${ds.ds_req_cancel}" readonly="readonly">
			</div>
		</div>
	</div> 
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">${retrunType} 택배사</label>
			<input type="text" class="form-control input-sm text-right" name="ds_company_cancel" value="${ds.ds_company_cancel}">
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">${retrunType} 송장번호</label>
			<input type="text" class="form-control input-sm text-right" name="ds_num_cancel" value="${ds.ds_num_cancel}">
		</div>
	</div> 
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="form-group form-group-default">
			<label for="form_control_1">${retrunType} 완료일</label>
			<div class="input-group">
				<span class="input-group-btn">
					<a class="text-danger-400" onclick="$('input[name=ds_date_cancel]').val('');"><i class="icon-cross3"></i></a>
				</span>
				<input type="text" class="form-control input-sm text-right daterange-single dsdate" name="ds_date_cancel" value="${ds.ds_date_cancel}" readonly="readonly">
			</div>
		</div>
	</div>
</div>

