<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,java.text.SimpleDateFormat,java.util.Date" %>
<%@ page import = "java.util.GregorianCalendar" %>
<%@ page import="org.json.simple.*"%>
<%@ page import="java.text.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ko">    
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<style type="text/css">
		p {margin:0;}
		ol {padding-left:0;}
		ol li {list-style-type:none;}
		.print-content-inner {border:3px solid #ddd;padding:20px;}
		.terms-order > li {margin-bottom:20px;}
		.terms-order strong {display:inline-block;padding-bottom:5px;}
		.terms-order p {line-height:20px;}
	</style>
</head>
<body class="bg-white">
	<div class="clearfix hidden-print mt-20 mb-20">
		<div class="text-right pr-15">
			<a class="btn btn-primary" onclick="javascript:window.print();">출력</a>
			<a class="btn btn-default" onclick="javascript:window.close();">닫기</a>
		</div>
	</div>
	<c:set var="cinfo" value="${cinfo}"/>
	<c:set var="dinfo" value="${dinfo}"/>

	<!-- BEGIN PAGE CONTENT INNER -->
	<div class="page-content-area">
		<div class="print-content-inner">
			<div class="text-center">
				<img src="/img/logo.png" class="text-center" alt="CI" width="120">
				<h5 class="mt-15"><strong>GN LED 퍼즐 조명기구 렌탈 약정서</strong></h5>			
				<p>이 약정은 임차인 "${cinfo.mi_name }"(이하 "갑"이라 한다)과 "㈜제너럴네트"(이하 "을"이라 한다)는 약정서에 기재된 "LED 조명기구"(이하"상품")의 렌탈(임대차) 조건에 대한 "임대차약정"(이하"약정"이라 한다)을 체결하며, 다음 조항에 합의 합니다.</p>
			</div>
			<div class="print_content mt-50">
				<ol class="terms-order">
					<li>
						<strong>제 1조 - 약정의 성립시점</strong>
						<p>"을"의 "갑"에 대한 신용조사에 의해 렌탈 약정체결에 이상이 없음이 확인되어 "을"이 "갑"에게 상품을 인도(설치)한 날로부터 렌탈(임대차) "약정의 효력이 발생합니다.</p>
					</li>
					<li>
						<strong>제 2조 - 렌탈 기간 및 청약 철회</strong>
						<p>1) 렌탈기간의 기산일은 "을"이 "갑"에게 상품을 인도(설치)한 날로부터 하며 렌탈기간의 종료는 설치일로부터 39개월간 또는 "을"의 해약 요청 시이고, 최초 설치일로부터 39개월간은 의무 사용 기간으로 합니다.</p>
						<p>2) "갑"은 렌탈상품 설치후 14일 이내 청약 철회가 가능합니다. 단, 이 경우 "갑"은 제6조2항의 위약금 추가비용(설치비 12만원, 철거비 12만원, 회수배송비 3만원, 등록비 5만원등 합계32만원)을 부담 합니다.</p>
					</li>
					<li>
						<strong>제 3조 - 상품의 인도 및 설치</strong>
						<p>1) "갑"이 임차의사가 있어 신청한 상품은 "갑"이 요청하는 장소에 인도(설치)하며 이때의 운송비와 설치 등록비(12만원)는 "을"의 부담으로 합니다.</p>
						<p>2) "갑" 또는 "갑 "을 대신하여 상품을 인도받는 자는 상품의 인수(설치)완료 즉시, 물건의 상태 및 성능이 정상임을 확인하신 후, 설치확인서에 서명 하셔야 되며 "을"은 본 약정서를 추후 "갑"에게 등기우편으로 발송하기로 합니다.</p>
					</li>
					<li>
						<strong>제 4조 - 상품의 보관 및 관리 유지</strong>
						<p>1) "갑"은 상품을 사용 관리함에 있어 선량한 관리자의 주의 의무를 다하며, 상품을 항상 정상의 운용 상태와 충분한 기능을 발휘할 수 있는 상태로 보관, 관리 유지하여야 합니다.</p>
						<p>2) "갑"은 "을"의 승인 없이 상품을 타인에게 양도, 전대, 개조 등을 하지 못함은 물론 하단에 기록된 설치장소 이외의 장소로 "을"에게주소변경 고지 없이 이동시켜서는 안 됩니다. 또 "갑"은 상품에 부착된 "을"의 소유권표식, 고정표식 등을 제거, 오염 시켜서는 안 됩니다.</p>
					</li>
					<li>
						<strong>제 5조 - 렌탈약정금/ 렌탈료 청구기준</strong>
						<p>1) 렌탈료는 "을"의 설치일 익월부터 지불 하시면 됩니다.</p>
						<p>2) "갑"이 특별히 원하는 날짜에 렌탈료를 납부하시길 원할 경우 최초 렌탈료는 일할 계산하여 지불 하실 수 있습니다.</p>
					</li>
					<li>
						<strong>제 6조 - 위약금</strong>
						<p>1) "갑"이 약정기간(의무사용기간)내에 해약 및 상품변경을 할 경우 "을"이 정한 방식 [월 렌탈료 X {(의무사용일수 - 사용일수) / 30} X 30%]</p><p>으로 위약금을 지불해야 합니다.</p>
						<p>2) "갑"은 위 1)항 외에도 "을"이 부담했던 설치비(12 만원), 철거비(12 만원), 회수배송비(3 만원), 등록비(5 만원)를 가산해서 지불해야 합니다.</p>
					</li>
					<li>
						<strong>제 7조 - 상품변경</strong>
						<p>1) 의무사용 기간중 상품을 변경할 경우에는 "갑"은 "을"에게 제6조 위약금을 지불하여야 합니다.</p>
						<p>2) 상품을 변경할 경우 변경된 상품이 설치된 날로부터 의무사용기간이 재적용됩니다.</p>
						<p>3) 제품하자로 인한 상품 변경 시에는 의무사용기간 재적용을 받지 않으며 의무사용기간 이내라도 "을"은 "갑"에게 위약금을 청구하지 아니합니다.</p>
						<p>4) 상품 변경시 이전 상품에 대한 사용료가 완납 되어야만 타 상품으로 변경이 가능합니다.</p>
					</li>
<!-- 				</ol> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 		<br> -->
<!-- 		<div class="print-content-inner"> -->
<!-- 			<div class="print_content"> -->
<!-- 				<ol class="terms-order"> -->
					<li>
						<strong>제 8조 - 자동이체</strong>
						<p>1) "갑"은 "을"과 체결하는 렌탈약정서에 월 렌탈료가 납기일에 자동이체 출금되도록 은행명(카드명), 계좌번호(카드번호) 및 예금주(카드회원)" 등의 사항을 기재 해야 합니다.</p>
						<p>2) 자동이체 지정계좌의 예금잔액이 납기일 현재 "을"의 청구금액보다 부족하거나 기타의 사유로 이체가 이루어지지 아니함으로 인하여 발생하는 "을"의 손해는 "갑"의 부담으로 하며, 자동이체에 따른 할인혜택은 적용되지 않습니다.</p>
						<p>3) 자동이체에 의하여 월 렌탈료를 수령한 경우 "을"은 "갑" 에게 별도의 입금 확인증을 보내지 아니합니다.</p>
						<p>4) 본 약정에 따른 렌탈료의 지급은 상품의 소유자(?제15조-소유권이전? 참조)인 [주식회사 제너럴네트]가 지정한 아래 계좌로 설치일이 속한 달의 익월부터 입금 하기로 하며, [주식회사 제너럴네트]의 별도 통지 없이는 입금계좌의 변경이 불가 합니다.다만, [주식회사 제너럴네트]에서 필요시 아래 입금계좌의 변경을 통보할 수 있으며, 이때 "갑"은 동의합니다. 또한 "을"은 본 약정의 체결 및 효력 발생과 동시에 본 약정에 따른 렌탈료에 대한 채권을 NH캐피탈에 양도하며 이에 대하여 "갑"은 동의합니다.</p>
						<p><strong>* 입금계좌 :  우리은행 언주역지점 1005-603-256005 예금주: (주)제너럴네트 </strong> </p>
					</li>
					<li>
						<strong>제 9조 - 상품의 불량이나 하자</strong>
						<p>1) 상품 설치 후 렌탈약정 기간중의 상품불량이나 하자 발생시 "을"의 부담으로 "을"의 서비스 부서에서 A/S처리 하기로 합니다.</p>
						<p>2) "갑"의 부주의로 인한 파손의 경우 수리비용 일체를 "갑"이 부담합니다.</p>
					</li>
					<li>
						<strong>제 10조 - 망실료 청구 기준</strong>
						<p>1) "갑"이 상품을 인도(설치)한 날로부터 39개월 이내 망실할 경우 제 6 조의 "을"이 정한 위약금 규정과 동일한 금액으로 "을"에게 망실료를 지급하여야 합니다.</p>
						<p>2) 완전 파손 및 압류 시에도 위 1)의 기준으로 적용합니다.</p>
					</li>
					<li>
						<strong>제 11조 - 상품이전의 고지의무</strong>
						<p>"갑"은 "을"로부터 상품을 인수한 후 주소를 변경하거나 렌탈상품을 다른 장소로 이전할 경우에는 변경 사항을 즉시 "을" 에게 서면또는 유선으로 통보하여야 하며 상품이전은 "을"의 의사에 따라 이전할 수 있습니다. 이때 이전 설치로 인해 발생되는 모든 비용"(철거비 6만원 및 이전설치비 12만원)은 "갑"이 부담합니다.</p>
					</li>
					<li>
						<strong>제 12조 - 연체료</strong>
						<p>"갑"이 약정불입일 이내에 렌탈료를 납부하지 못한 경우에는 연체한 각 월 렌탈료에 대하여 회사에서 정한 연체료(년 19%)를 가산하여 납부하여야 합니다.</p>
					</li>
					<li>
						<strong>제 13조 - 약정의 해지</strong>
						<p>1) "갑"은 렌탈상품을 인도(설치)받은 날로부터 최소 39개월이 경과한 날까지는 약정을 해지 할 수 없습니다.</p>
						<p>2) 부득이하게 "갑"의 사정으로 약정이 해지(해약)된 경우 "갑"은 해지일 현재 지급기일이 경과된 모든 연체 렌탈료를 지급해야 하며해지에 따라 추가로 부담한 비용도 "을"에게 지급해야 합니다.</p>
						<p>3) 렌탈약정 해지시 해지월의 계산은 사용일 기준으로 일할 계산하며 이때 계산된 렌탈료의 10원 단위는 절사합니다.</p>
						<p>4) "갑"이 렌탈료 불입을 2개월간 연체할 경우 "을"은 소정의 기간을 두고 그 기간 경과 후에는 렌탈 (임대차)약정을 해지할 수 있고,이때 상품의 회수는 전적으로 "을"의 권한입니다. 또한 제 6조의 규정을 적용할 때 "갑"은 이의를 달 수 없습니다.</p>
						<p>5) 염분이나 분진과 같이 제품의 정상적인 작동 에 심각한 지장을 초래할 수 있거나 사업을 목적으로 사용하시거나 혹은 제품의 수명을 단축시킬 수 있는 지역에서 사용 하실 경우에는 관리 및 유지가 불가능한 경우라고 판단하여 "을"은 약정을 해지할 수 있습니다.</p>
					</li>
					<li>
						<strong>제 14조 - A/S의 의무</strong>
						<p>1) "을"은 "갑"이 사용하는 상품에 대한 수시점검을 실시할 수 있습니다. (단, 월 렌탈료가 약정한 납기일 이내에 미납될 경우에는 A/S가 중지됩니다.)</p>
						<p>2) 렌탈 약정기간 중 "갑"의 부주의로 인한 파손이 아닌 경우 발생하는 상품수리 및 부품교환은 "을"이 무상으로 수리 합니다.</p>
						<p>3) 효율적인 A/S를 위해 "을"은 A/S관리를 제3자에게 위임하거나 제3자를 새로운 A/S업체로 지정할 수 있습니다.</p>
					</li>
<!-- 				</ol> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 		<br> -->
<!-- 		<div class="print-content-inner"> -->
<!-- 			<div class="print_content"> -->
<!-- 				<ol class="terms-order"> -->
					<li>
						<strong>제 15조 - 소유권 이전</strong>
						<p>1) "갑"과 "을"간의 계약에 의거 주식회사 제너럴네트에 상품의 소유권이 있으며, 상품을 인도(설치)한 날부터 39개월 이 경과하고 "갑"이 렌탈료를 전액 납부한 경우, "을"은 "갑"에게 소유권을 무상으로 이전합니다.</p>
						<p>2) 소유권 이전시 소정의 이전료가 발생할 수 있습니다. (단, 설치장소 변경 등 사유발생시 한함)</p>
					</li>
					<li>
						<strong>제 16조 - 신용 정보자료 제공</strong>
						<p>"을"은 본 약정에 따른 "갑"의 신용정보 일체를 본 약정과 관련한 금융기관등에게 제공할 수 있습니다. 아울러 이 약정을 위반할 경우 렌탈료 연체내용이나 렌탈부실거래 관련내용 등의 신용정보를 신용정보 관리기관등 에게 제공할 수 있습니다.</p>
					</li>
					<li>
						<strong>제 17조 - 관할법원</strong>
						<p>이 약정에 관한 분쟁이 발생하여 소송이 필요한 경우 "을" 소재 관할 법원에 제기합니다.</p>
					</li>
					<li>
						<strong>제 18조 - 기 타</strong>
						<p>이 약정에 규정되지 아니한 사항에 관하여는 관계법령 및 회사의 운영 방침에 따릅니다. 본 약정의 성립은, "을"의 콜센타에서 약정과 관련된 사항을 "갑"에게 동의를 얻어 그 내용을 녹취록으로 보관토록 하며, "을"이 "갑"에게 약정한 상품을 설치함과 동시에 효력이 발생 됩니다. 또한, "을"이 "갑"에게 본 약정으로 인해 향후 발생되는 렌탈채권 및 상품의 반환청구권에 대한 채권양도통지서를 내용증명으로 보내기로 합니다.</p>
					</li>
				</ol>
				<table class="printtable">
					<caption><i class="icon-square"></i> "<strong>갑</strong>" 고객정보</caption>
					<tbody>
						<tr>
							<th width="25%" rowspan="2">결제시작월</th>
							<td width="25%" rowspan="2">${cinfo.m_paystart}</td>
						</tr>
						<tr>
							<th width="25%" rowspan="2">결제만료월</th>
							<td width="25%" rowspan="2">${cinfo.finishdate}</td>
						</tr>
						<tr>
							<th width="25%" rowspan="2">성명(계약자)</th>
							<td width="25%" rowspan="2">${cinfo.mi_name}</td>
						</tr>
						<tr>
							<th width="25%">핸드폰번호</th>
							<td width="25%">${cinfo.mi_phone}</td>
						</tr>
						<tr>
							<th width="25%">주민등록번호</th>
							<td width="25%">${cinfo.m_pay_id}</td>
							<th width="25%">계약일련번호</th>
							<td width="25%">${cinfo.m_num}</td>
						</tr>
						<tr>
							<th width="25%">계약자주소</th>
							<td colspan="3">${cinfo.addr}</td>
						</tr>	
					</tbody>
				</table>
		<br>
		<table class="printtable">
					<tbody>
					
					<tr>
							<c:choose>
							<c:when test="${dinfo.ds_state eq '배송완료' }">
							<th width="25%" rowspan="2">배송완료일</th>
							</c:when>
							<c:otherwise>
							<th width="25%" rowspan="2">설치완료일</th>
							</c:otherwise>
							</c:choose>
							<td width="25%" rowspan="2">${dinfo.ds_date}</td>
						</tr>
						<tr>
							<th width="25%" rowspan="2"></th>
							<td width="25%" rowspan="2"></td>
						</tr>
						<tr>
							<th width="25%" rowspan="2">성명<br>(예금주)</th>
							<td width="25%" rowspan="2">${cinfo.m_pay_owner}</td>
							<%-- <th width="25%">전화번호</th>
							<td width="25%">${m_phone}.length()>9?m_u_phone.substring(0,9):m_u_phone%>****</td> --%>
						</tr>
						<tr>
							<th width="25%">핸드폰번호</th>
							<td width="25%">${dinfo.mi_phone}</td>
						</tr>
						<tr>
							<th width="25%">주민등록번호</th>
							<td width="25%">${dinfo.m_pay_id}</td>
							<th width="25%">계약일련번호</th>
							<td width="25%">${cinfo.m_num}</td>
						</tr>
						<%-- <tr>
							<th width="25%">상품 배송</th>
							<td width="25%" colspan="3"><%=ms_install_loc1%> <%=ms_install_loc2%> <%=ms_install_loc3 %></td>
						</tr> --%>
						<tr>
							<c:choose>
								<c:when test="${dinfo.m_pay_method eq '계좌이체' }">
							<th width="25%">결제계좌번호(자동이체)</th>
								<td width="25%" colspan="3">${dinfo.m_pay_info} ${dinfo.m_pay_num}</td>
								</c:when>
								<c:when test="${dinfo.m_pay_method eq '카드' }">
							<th width="25%">카드번호</th>
								<td width="25%" colspan="3">${dinfo.m_pay_info} ${dinfo.m_pay_num}</td>
								</c:when>
							</c:choose>
						</tr>			
						<tr>
						<c:choose>
							<c:when test="${dinfo.m_pay_method eq '계좌이체' }">
								<th width="25%">결제일</th>
								<td width="25%">${dinfo.m_paydate}</td>
								<th width="25%"></th>
								<td width="25%"></td>
							</c:when>
							<c:when test="${dinfo.m_pay_method eq '카드' }">
								<th width="25%">카드 유효년/월</th>
								<td width="25%">${dinfo.m_card_year } / **</td>
								<th width="25%">결제일</th>
								<td width="25%">${dinfo.m_paydate}</td>
								
							</c:when>
						</c:choose>
						<%-- <tr>
							<th width="25%">월 렌탈료</th>
							<td width="25%">${dinfo.m_rental_f}원</td>
							<th width="25%">약정월</th>
							<td width="25%">${cinfo.m_period}개월</td>
						</tr>			 --%>			
					</tbody>
				</table>
		<br>
		<table class="printtable">
					<tbody>
						<tr>
							<th width="25%" rowspan="2">성명<br>(배송자)</th>
							<td width="25%" rowspan="2">${dinfo.mi_name}</td>
						</tr>
						<tr>
							<th width="25%">핸드폰번호</th>
							<td width="25%">${dinfo.mi_phone}</td>
						</tr>
						<tr>
							<th width="25%">배송주소</th>
							<td colspan="3">${dinfo.addr}</td>
						</tr>		
					</tbody>
				</table>
				<table class="printtable mt-20">
					<caption><i class="icon-square"></i> 렌탈 상품 정보</caption>
					<tbody>
						<tr>
							<th width="20%">렌탈 상품명</th>
							<th width="20%">설치 등수</th> 
							<th width="20%">월 렌탈료</th>
							<th width="20%">약정월</th>
							<th width="20%">비고</th>
						</tr>
						<tr>
							<td width="20%">${cinfo.pr_name}</td>
							<td width="20%">${dinfo.total_count}</td>
							<td width="20%">${cinfo.m_rental_f}원</td>
							<td width="20%">${cinfo.m_period}개월</td>
							<td width="20%"></td>
						</tr>		
					</tbody>
				</table>
				<table class="printtable mt-20">
					<caption><i class="icon-square"></i> "<strong>을</strong>" 회사정보</caption>
					<tbody>
						<tr>
							<th width="25%">법인명</th>
							<td>(주)제너럴네트</td>
						</tr>
						<tr>
							<th width="25%">대표이사</th>
							<td>송강호</td>
						</tr>
						<tr>
							<th width="25%">사업장 소재지</th>
							<td>서울시 강남구 봉은사로 29길 48(논현동) 제너럴네트빌딩3층</td>
						</tr>
						<tr>
							<th width="25%">전화번호</th>
							<td>1522-7557</td>
						</tr>	
					</tbody>
				</table>
				<table class="printtable printtable_noline mt-30" style="width:50%;margin:0 auto">
					<tbody>
						<tr>
							<td colspan="3"><h5 class="no-margin" style="letter-spacing:5"><strong>주식회사 제너럴네트</strong></h5></td>
						</tr>
						<tr>
							<td width="20%" style="text-align:right;letter-spacing:5"><h5 class="no-margin"><strong>대표이사</strong></h5></td>
							<td width="60%"><h5 class="no-margin"><strong>송강호</strong></h5></td>
							<td width="20%"><img src="/img/sign.png" width="50" height="auto"></td>
						</tr>
						<tr>
							<td colspan="3"><h6 class="no-margin"><strong>서울특별시 강남구 봉은사로29길 48(논현동) 제너럴네트빌딩 3층 / 1522-7557</strong></h6></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT INNER -->  
	<div class="clearfix hidden-print mt-20 mb-20">
		<div class="text-right pr-15">
			<a class="btn btn-primary" onclick="javascript:window.print();">출력</a>
			<a class="btn btn-default" onclick="javascript:window.close();">닫기</a>
		</div>
	</div>
	   
	<%@include file="/WEB-INF/view/main/js.jsp"%>
</body>
</html>
