 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="modal_large" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="modal_large_title">렌탈 상품 구성 추가 </span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel" id="modal_large_content">
						<table id="table" class="table table-hover" >
							<thead>
								<tr>
									<th>상품명</th>
									<th>월 렌탈료</th>
									<th>렌탈총액</th>
									<th>렌탈기간</th>
									<th>일시불 금액</th>
									<th>상세보기</th>
								</tr>
							</thead>
							<tbody>
							<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

