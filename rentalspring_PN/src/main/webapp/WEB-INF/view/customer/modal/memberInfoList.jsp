<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="panel-body">
<c:forEach items="${milist}" var="mi">
	<div class="col-xs-12 col-sm-4 col-md-2 col-lg-offset-0 ">
		<div class="form-group form-group-default">
			<div class="input-icon right">
				<label for="form_control_1">${mi.mt_code}</label>
				<a onclick="memberInfoDetail()" mi="${mi.mi_idx}">${mi.mi_name eq ''?'(이름없음)':mi.mi_name}</a>
			</div>
		</div>
	</div>
</c:forEach>
</div>