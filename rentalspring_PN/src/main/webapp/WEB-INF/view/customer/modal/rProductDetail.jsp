<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="modal_large2" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml2" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="modal_large_title2">렌탈 상품 구성 추가 </span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal">
							<i class="icon-cross2 cursor-pointer" onclick="close()"></i>
						</span>
					</h4>
				</div>
				<div class="modal-body">
					<div class="panel" id="modal_large_content2">
						<table id="table" class="table table-hover" >
							<thead class="alpha-slate">
								<tr>
									<th colspan="5">상품명</th>
									<th>수량</th>
								</tr>
							</thead>
							<tbody id="product">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
function close(){
	$('#modal_large2').on('hidden.bs.modal',function (e){
		$('#modal_large2').find('#stock').reset() 
	});
	$('#modal_large2').modal('hide');
};

</script>
