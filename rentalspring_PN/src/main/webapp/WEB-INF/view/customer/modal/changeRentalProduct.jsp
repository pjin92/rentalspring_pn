 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="modalChangeProduct" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="insideChangeProduct" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i>렌탈 상품 변경<span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
						<a class="btn bg-slate-600 pull-right mr-20" onclick="addLedProduct()">견적계산기</a>
						<a class="btn bg-slate-600 pull-right mr-5" onclick="addOLedProduct()">구 견적계산기</a>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel" id="modalChangeProduct_content">
						<table id="modalChangeProductTable" class="table table-hover" >
							<thead>
								<tr>
									<th class="active">상품명</th>
									<th class="active">월 렌탈료</th>
									<th class="active">렌탈총액</th>
									<th class="active">렌탈기간</th>
									<th class="active">일시불 금액</th>
									<th class="active">상품구성</th>
								</tr>
							</thead>
							<tbody>
							<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function addLedProduct(){
	window.open('/product/calculate','calculate','width=1200,height=800');
}
function addOLedProduct(){
	window.open('/product/oldcalculate','calculate','width=1200,height=800');
}
</script>
