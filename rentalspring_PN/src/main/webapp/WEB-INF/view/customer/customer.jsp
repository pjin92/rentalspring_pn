<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">상담고객</span>
							<small class="display-block">등록</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<form name="customer">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title"><i class="icon-folder-search position-left"></i>렌탈 상품 정보 입력</h5>
									</div> 
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">렌탈 상품</label>
															<input type="hidden" name="m_pr_idx" >
															<input type="text" class="form-control input-sm" name="pr_name" readonly="readonly" onclick="rpList()" placeholder="상품선택">
														</div>
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">판매처</label>
															<input type="text" class="form-control input-sm" name="m_sale">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">월렌탈료</label>
															<input type="text" class="form-control input-sm" name="m_rental" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp();rental();">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">렌탈기간</label>
															<input type="text" class="form-control input-sm" name="m_period" onkeydown="numKeyDown()" onkeyup="numKeyUp();rental();">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">총액</label>
															<input type="text" class="form-control input-sm" name="m_total" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp();total();">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">일시불 금액</label>
															<input type="text" class="form-control input-sm" name="m_ilsi" onkeydown="numKeyDown()" onkeyup="numCommaKeyUp()">
														</div>
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">결제희망일</label>
															<input type="text" class="form-control input-sm" name="m_paydate" onkeydown="numKeyDown()" onkeyup="numKeyUp();paydate();" maxlength="2" placeholder="ex) 05">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">결제시작일</label>
															<input type="text" class="form-control daterange-single" name="m_paystart">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">계약일</label>
															<input type="text" class="form-control daterange-single" name="m_contract_date">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">배송희망일</label>
															<input type="text" class="form-control daterange-single" name="m_eta">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3 ">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">무료체험일수</label>
															<input type="text" class="form-control input-sm" name="m_free_day" onkeydown="numKeyDown()" onkeyup="numKeyUp()">
														</div>
													</div>
												</div> 
												<!-- <div class="col-xs-12">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">메모</label>
															<textarea class="form-control input-sm" name="m_memo" style="resize:vertical"></textarea>
														</div>
													</div>
												</div> --> 
											</div>
										</div>
									</div>
								</div>							
							</div>
							<div class="col-xs-12">
								<div class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title"><i class="icon-folder-search position-left"></i>상담 고객 정보 입력</h5>
									</div> 
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1"> 고객명</label>
															<input type="text" class="form-control input-sm" name="mi_name">
														</div>
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">연락처</label>
															<div class="row">
																<div>
																<input type="text" class="form-control input-sm" name="mi_phone" onkeydown="numKeyDown()"onkeyup="numKeyUp()" >
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">연락처</label>
															<div class="row">
																<input type="text" class="form-control input-sm" name="mi_tel" onkeydown="numKeyDown()"onkeyup="numKeyUp()">
															</div>
														</div>
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">개인/법인</label>
															<select class="select select2-hidden-accessible" name="mi_id_type">
																<option>개인</option>										
																<option>법인</option>										
															</select>
														</div>
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">생년월일/사업자번호</label>
															<input type="text" class="form-control input-sm" name="mi_id">
														</div>
													</div>
												</div> 
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소1</label>
															<input type="text" class="form-control input-sm" name="mi_addr1" readonly onclick="openAddress()">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소2</label>
															<input type="text" class="form-control input-sm" name="mi_addr2" readonly onclick="openAddress()">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">주소3</label>
															<input type="text" class="form-control input-sm" name="mi_addr3" onclick="openAddressIfEmpty()">
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-4 col-md-3">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">우편번호</label>
															<input type="text" class="form-control input-sm" name="mi_post">
														</div>
													</div>
												</div>
											
												<div class="col-xs-12 col-sm-12 col-md-9">
													<div class="form-group form-group-default">
														<div class="input-icon right">
															<label for="form_control_1">메모</label>
															<textarea class="form-control input-sm" name="mi_memo" style="resize:vertical"></textarea>
														</div>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="heading-elements">
											<div class="heading-btn pull-right">
												<a class="btn btn-default" onclick="window.close()">닫기 </a>
												<a class="btn bg-teal-400 position-right" onclick="startRental()">등록 </a>
											</div>
										</div>
									</div>
								</div>							
							</div>
						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="modal/rentalProductList.jsp"%>
	<%@include file="modal/rProductDetail.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<script>
	$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "pr_name" },
	    { "data": "pr_rental" },
	    { "data": "pr_total" },
	    { "data": "pr_period" },
	    { "data": "pr_ilsi" },
	    { "data": "detail" }
	  ]
	});
	
	ajaxDt('table','/product/rentalProducts');
	//다음 주소
	function openAddressIfEmpty(){
		if($('input[name=mi_addr1]').val()==''){
			openAddress();
		}
	}
	function openAddress(){
		new daum.Postcode({
		    oncomplete: function(data) {
		      	var sido=data.sido;
		    	var sigungu=data.sigungu;
		    	var roadname=data.address.replace(sido,'');
		    	roadname=roadname.trim();
		    	roadname=roadname.replace(sigungu,'');
		    	roadname=roadname.trim();
		    	var zonecode=data.zonecode;
		    	$('input[name=mi_addr1]').val(sido);
		    	$('input[name=mi_addr2]').val(sigungu);
		    	$('input[name=mi_addr3]').val(roadname);
		    	$('input[name=mi_post]').val(zonecode);
		    }
		}).open({popupName:'daumAddr'});
	}
	//렌탈상품 선택 모달
	function rpList(){
		$('#modal_large').modal('show');
	}
	function choose(){
		var idx=$(event.target).attr('idx');
		var name=$(event.target).html();
		$('input[name=m_pr_idx]').val(idx);
		$('input[name=pr_name]').val(name);
		$('#modal_large').modal('hide');
		
		$.ajax('/product/productRentalinfo/'+idx)
		.done(function(data){
			$('input[name=m_rental]').val(comma(data.pr_rental));
			$('input[name=m_period]').val(data.pr_period);
			$('input[name=m_total]').val(comma(data.pr_total));
			$('input[name=m_ilsi]').val(comma(data.pr_ilsi));
		});
	}
	//렌탈료,기간 변경
	function rental(){
		var rental=onlyNum( $('input[name=m_rental]').val());
		var period=onlyNum( $('input[name=m_period]').val());
		var total=Number(rental)*Number(period);
		$('input[name=m_total]').val(comma(total));
	}
	//총액 변경
	function total(){
		var total=onlyNum( $('input[name=m_total]').val());
		var period=onlyNum( $('input[name=m_period]').val());
		var m_rental=Math.floor(Number(total)/Number(period));
		$('input[name=m_rental]').val(comma(m_rental));
	}
	//결제희망일
	function paydate(){
		var pd=Number($('input[name=m_paydate]').val());
		if(pd>31){
			$('input[name=m_paydate]').val('31');
		}
	}
	
	function startRental(){
		var param=$('form[name=customer').serialize();
		$.ajax({
			url:'/customer/',
			type:'post',
			data:param
		})
		.done(function(data){
			alert(data);
			if(data.trim()=='등록 완료'){
				window.close();
			}
		});
	}

	
	function rproductDatail() {
		var pr_idx = $(event.target).attr('pr_idx');
		$('#product').empty();
		$.ajax({
			url : '/product/productDetail/'+pr_idx,
			type : 'get',
			success : function(data) {
				$('#modal_large2').modal('show');
				$('#product').append(data);
			}
		});
	}
	</script>
</body>
</html>