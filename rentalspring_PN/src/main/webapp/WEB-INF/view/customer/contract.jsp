<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>계약고객관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper"> 
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">고객 조회</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
			
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="상담상태가 상담완료,재계약(구성변경),보류,대기 ,취소(가망),선설치(렌탈전환)인 고객" data-html="true"></i>
										검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body">
									<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group" >
													<label class="form-tit-label">계약완료일</label> 
													<input type="text" class="form-control input-sm daterange-blank" name="m_contract_date">  
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">판매처</label>
													<input type="text" class="form-control input-sm" name="m_sale">
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">판매기수</label>
													<input type="text" class="form-control input-sm" name="mg_name">
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">상담CS</label>
													<input type="text" class="form-control input-sm" name="e_name">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">신청고객 또는 계약고객</label>
													<input type="text" class="form-control input-sm" name="mi_name">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">고객 회원 번호</label>
													<input type="text" class="form-control input-sm" name="m_num">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">연락처(계약자)</label>
													<input type="text" class="form-control input-sm" name="mi_phone">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">배송/설치 주소</label>
													<input type="text" class="form-control input-sm" name="mi_addr">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group ">
													<label class="form-tit-label">접수구분</label>
													<select class="form-control input-sm" name="m_jubsoo">
														<option value="">선택</option>
														<c:forEach var="jubsoo" items="${mjubsoos}">
														<option value="${jubsoo}">${jubsoo}</option>										
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">매출구분</label>
													<select class="form-control input-sm" name="m_detail">
														<option value="">선택</option>
														<c:forEach var="mdetail" items="${mdetails}">
														<option value="${mdetail}">${mdetail}</option>										
														</c:forEach>
													</select>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">결제 방식</label>
													<select class="form-control input-sm" name="m_pay_method">
														<option value="">전체</option>
														<option>계좌이체</option>
														<option>카드</option>
													</select>
												</div>
											</div>
											
											<!-- <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">진행 상태</label>
													<select class="form-control input-sm" name="m_process">
														<option value="">선택</option>
													</select>
												</div>
											</div> -->
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">배송/설치 상태</label>
													<select class="form-control input-sm" name="ds_state">
														<option value="">선택</option>
														<c:forEach var="ds_state" items="${ds_state}">
														<option id="ds_state" value="${ds_state.ds_state}">${ds_state.ds_state}</option>										
														</c:forEach>
													</select>
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">금융원부 기수</label>
													<input type="text" class="form-control input-sm" name="m_finance">
												</div>
											</div>
											
											<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">렌탈상품명</label>
													<input type="text" class="form-control input-sm" name="pr_name">
												</div>
											</div>
					
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>					
									</form>
								</div>
							</div>
								<!-- /Search layout --> 
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>고객 조회결과</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>		 
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<th class="active">선택</th>
											<th class="active">배송/설치완료일</th>
											<th class="active">계약완료일</th>
											<th class="active">판매처</th>
											<th class="active">판매기수</th>
											
											<th class="active">상품명</th>
											<th class="active">금융기수</th>
											<th class="active">결제 방식</th>
											<th class="active">고객 회원 번호</th>
											
											<th class="active">신청고객/계약고객</th>
											<th class="active">연락처(계약자)</th>
											<th class="active">주소(배송/설치 고객)</th>
											<th class="active">우편번호</th>
											<th class="active">상태</th>
											
											<th class="active">접수상태</th>
											<th class="active">구분</th>
											<th class="active">진행상태</th>
											<th class="active">배송상태</th>
											<th class="active">무료체험만료일</th>
											
											<th class="active">녹취계약</th>
											<th class="active">상담CS</th>
											<th class="active" data-orderable="false">렌탈약정서</th>
											<th class="active" data-orderable="false">양도통지서</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="showFgModal()"><i class="icon-trash-alt"></i>금융원부 기수 입력</a></li>
												<li><a onclick="csBatch()"><i class="icon-copy3"></i>상담사배정</a></li>
												<hr>
												<li><a onclick="refreshTable()"><i class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div> 
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "m_idx" },
		    { "data": "ds_date"},
		    { "data": "m_contract_date"},
		    { "data": "m_sale"},
		    { "data": "mg_name"},
		    
		    { "data": "pr_name"},
		    { "data": "m_finance"},
		    { "data": "m_pay_method"},
		    { "data": "m_num"},
		    
		    { "data": "app_con_name"},
		    { "data": "mi_phone"},
		    { "data": "mi_addr"},
		    { "data": "mi_post"},
		    { "data": "m_state"},
		    
		    { "data": "m_jubsoo"},
		    { "data": "m_detail"},
		    { "data": "m_process"},//진행상태
		    { "data": "ds_state"},
		    { "data": "m_free_date"},//무료체험만료일
		    
		    { "data": "m_voice_contract"},//녹취계약
		    { "data": "e_name" },//상담CS
		    { "data": "rental" },//렌탈약정서
		    { "data": "yangdo" }//양도통지서
	// 	    { "data": null,"defaultContent":"<a onclick='rental()'>렌탈약정서</a>" },//렌탈약정서
		  ]
	});
	tableLoadUnderModal('checkTable');
	
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		var param=$('form[name=searchForm]').serialize();
		checkDt('checkTable','/customer/contract?'+param);
	}
	
	//재검색 입력시 기존 요청 취소
	$(document).ajaxSend(function(){
		var table=$('#checkTable').DataTable();
		table.settings()[0].jqXHR.abort();
	});
		
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#checkTable').DataTable();
		table.ajax.url('/customer/contract?'+param).load(null,false);
	} 
	function addPop(){
		window.open('/customer/','popup','width=1200,height=600');
	}
	//현재 보이는 dataTable refresh
	function refreshTable(){
		var table=$('#checkTable').DataTable();
		table.ajax.reload(null,false);
	}
	function member(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/contract/'+m_idx+'/','popup','width=1200,height=600');
	}
	
	//금융원부 기수 modal
	function showFgModal(){
		var table=$('#checkTable').DataTable();
		var count=0;
		table.$('tr.selected').each(function(){
			count++;
		});
		
		var inside='<div class="panel-body">선택된 고객: '+count+'명<br>';
		inside+='<div class="col-xs-6"><input class="form-control input-sm" type="text" id="mf"></div>';
		inside+='<div class="col-xs-6"><a class="btn btn-primary" onclick="insertFG()">입력</a></div>';
		inside+='</div>';
		$('#modal_large_content').html(inside);
		$('#modal_large_title').html('금융원부 기수 입력');
		$('#modal_large').modal('show');
		
	}
	//금융원부 입력
	function insertFG(){
		var table=$('#checkTable').DataTable();
		var param='';
		var count=0;
		table.$('tr.selected').each(function(){
			param+=$(this).children('td:nth-child(10)').children('a').attr('m_idx')+',';
			count++;
		});
		if(count==0){
			alert('선택된 고객이 없습니다.');
			return;
		}
		
		var fg=$('#mf').val();
		$.ajax('/finance/checkName?fg='+fg+'&midxs='+param)
		.done(function(res){
			if(count==Number(res)){
				alert('금융기수 입력하였습니다.');
			}else{
				alert(res.trim()+'건 입력완료.');
			}
			$('#modal_large').modal('hide');
			refreshTable();
		});
	}
	
	function rental(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/rental/'+m_idx+'/','popup','width=1200,height=600');
	} 
	
	function yangdo(){
		var m_idx=$(event.target).attr('m_idx');
		window.open('/customer/yangdo/'+m_idx+'/','popup','width=900,height=600');
	} 
	
	
	function cs(){
		var m_idx=$(event.target).attr('m_idx');
		var mi_name=$(event.target).attr('mn');
		$('#modal_large_title').html(mi_name+'님 상담CS 배정');
		$.ajax('/employee/department/cs팀')
		.done(function(data){
			var str='<div class="p-20"><select class="select select2-hidden-accessible " id="cs_select" m="'+m_idx+'">';
			for(var i=0;i<data.length;i++){
				str+='<option value="'+data[i].e_id+'">'+data[i].e_name+'</option>';
			}
			str+='</select><div class="clearfix mt-10"><a onclick="csselect()" class="pull-right btn btn-sm bg-teal-400">배정</a></div></div>';
			$('#modal_large_content').html(str);
			$('.modal-lg').addClass('modal-sm');
			$('.modal-lg').removeClass('modal-lg');
			
			$('#cs_select').select2({
	        	minimumResultsForSearch: Infinity
	        });
			
			$('#modal_large').modal('show');
		});
	}
	
	//상담cs 배정
	function csselect(){
		var cs=$('#cs_select').val();
		var m=$('#cs_select').attr('m');
		
		var setting={
			url:'/member/damdang/'+m+'?m_damdang='+cs,
			type:'put'
		};
		$.ajax(setting)
		.done(function(data){
			var result=Number(data.trim());
			var msg='';
			if(result<0){
				msg='오류발생.';
			}else if(result==0){
				msg='배정 실패.\n다시 시도해주세요.';
			}else{
				msg='상담사를 배정하였습니다.';
				blockById('modal_large_content');
				var table=$('#checkTable').DataTable();
				table.ajax.reload(function(){
					$('#modal_large_content').unblock();
					$('#modal_large').modal('hide');
				},false);
			}
			alert(msg);
			
		});
	}
	//일괄 상담cs배정 모달
	function csBatch(){
		var m_idxs='';
		var count=0;
		
		var table=$('#checkTable').DataTable();
		table.$('tr.selected').each(function(){
			m_idxs+=$(this).children('td:nth-child(10)').children('a').attr('m_idx')+',';
			count++;
		});
		
		if(count==0){
			alert('선택된 고객이 없습니다.');
			return;
		}
		
		$('#modal_large_title').html(count+'명 상담CS 일괄배정');
		$.ajax('/employee/department/cs팀')
		.done(function(data){
			var str='<div class="p-20"><select class="select select2-hidden-accessible " id="cs_select" >';
			for(var i=0;i<data.length;i++){
				str+='<option value="'+data[i].e_id+'">'+data[i].e_name+'</option>';
			}
			str+='</select><div class="clearfix mt-10"><a onclick="csselectBatch()" class="pull-right btn btn-sm bg-teal-400">배정</a></div></div>';
			str+='<input type="hidden" id="csselectBatch" value="'+m_idxs+'">';
			$('#modal_large_content').html(str);
			$('.modal-lg').addClass('modal-sm');
			$('.modal-lg').removeClass('modal-lg');
			
			$('#cs_select').select2({
	        	minimumResultsForSearch: Infinity
	        });
			
			$('#modal_large').modal('show');
		});
	}
	//상담cs 일괄배정
	function csselectBatch(){
		var cs=$('#cs_select').val();
		var m_idxs=$('#csselectBatch').val();
		var setting={
			url:'/member/damdangBatch/'+cs,
			type:'post',
			data:'m_idxs='+m_idxs
		};
		$.ajax(setting)
		.done(function(data){
			var result=Number(data.trim());
			var msg='';
			if(result<0){
				msg='오류발생.';
			}else if(result==0){
				msg='배정 실패.\n다시 시도해주세요.';
			}else{
				msg='상담사를 배정하였습니다.';
				blockById('modal_large_content');
				var table=$('#checkTable').DataTable();
				table.ajax.reload(function(){
					$('#modal_large_content').unblock();
					$('#modal_large').modal('hide');
				},false);
			}
			alert(msg);
			
		});
	}
	
	$('[data-toggle=tooltip]').tooltip();
	
	
	</script>
</body>
</html>