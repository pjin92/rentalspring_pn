<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>CS 점검 관리</title>
	<%@include file="/WEB-INF/view/main/m_css.jsp"%>
</head>
<body>
	<!-- CS 점검 관리  page -->
	<div data-role="page" id="install-page" data-url="install-page" data-title="CS 점검 관리">
		<div data-role="header" data-positon="fixed">
			<h1>CS점검관리</h1> 
		</div>
		<!-- search input -->	
		<div class="ui-grid-solo search-section">
			<div class="ui-block-a search-wrap">
				<form name="searchForm">
					<input type="text" name="mainKeyword" id="mainKeyword" placeholder="상품명, 신청고객명" />
					<button type="submit" class="search-btn" onsubmit="searchBtn()"><i class="icon-search4"></i></button>
				</form>
			</div>
		</div>		
		// search input		
		<!--  main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<!-- list section -->
			<div class="ui-grid-solo">
				<!-- list 반복 -->
				<div class="ui-block-a list-box-wrap">
					<c:forEach var="in" items="${data}">
						<div class="clearfix">
							<div class="list-left-box">	
<!-- 							<p class="bg-complete"><b>설치완료</b></p> -->
								<p class="bg-orange mb-sm"><a href="#none">설치확인서</a><!-- 설치확인서 경로 --></p>
<!-- 							<p class="bg-gray"><b>2018-06-08</b></p> -->
								<a href="#inspect-page" m_idx="${in.m_idx}" class="width-100 btn bg-info ">점검 목록</a>
						</div>
						<div class="list-right-box">
							<ul class="list-wrap">
								<li class="">
									<p class="list-tit">상품명 : <strong class="list-cont">${in.pr_name}</strong></p>
								</li>
								<li class="">
									<p class="list-tit">설치고객명 :${in.mi_name}</p>
								</li>
								<li class="">
									<p class="list-tit">주소 : <strong class="list-cont">${in.mi_addr }</strong></p>
								</li>
								<li class="">
									<p class="list-tit">연락처 : <strong class="list-cont"><a href="tel:010-1234-5678">${in.mi_tel }</a></strong></p>
								</li>
							</ul>
						</div>
					</div>
				</c:forEach>
					<div class="list-center-box padding-10px-top">
						
					</div>
				</div>
				<!--// list 반복 -->														
			</div>	
			<!--// list section -->
			<!-- 고객정보 상세 팝업창  (고객명 a href == 팝업 id값 동일) -->
			<div data-role="popup" id="customerInfo" data-overlay-theme="a">
				<a href="javascript:;" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
				<%@ include file="../../customer/contractApp2.jsp" %>
			</div>		
			<!--// 고객정보 상세 팝업창  -->			
		</div>
		<!--//  main content -->
		<%@ include file="../../main/m_menu.jsp" %>
	</div>
	<!--// CS 점검 관리  page -->
	
	<!-- 점검 목록 리스트  page -->
	<div data-role="page" id="inspect-page" data-url="inspect-page" class="jqm-demos" data-title="점검 목록 리스트">
		<div data-role="header" data-positon="fixed">
			<h1>점검 목록 리스트</h1> 
			<a href="#install-page" data-direction="reverse" class="ui-back-btn" data-role="button" role="button"><i class="icon-circle-left2"></i></a>
		</div>	
		<!-- main content -->
		<div data-role="main" class="ui-content jqm-content jqm-fullwidth">
			<!-- list section -->
			<div class="ui-grid-solo">
				<div class="ui-block-a text-right margin-10px-bottom">
					<a href="#addcsProduct-page" class="btn bg-primary">긴급 점검 추가</a>
				</div>
				<!-- list 반복 -->
			<div id="inspectApp">
				
			</div>
				<!--// list 반복 -->		
														
			</div>	
			<!--// list section -->
			
			<!-- 점검 목록 상세 팝업창  (버튼 a href == 팝업 id값 동일) -->
			
			<!--// 점검 목록 상세 등록 팝업창 -->		
			
			<!-- 에정밀 변경 팝업창 (버튼 a href == 팝업 id값 동일) -->
			
			<!--// 에정밀 변경 팝업창 -->		
		</div>
		<!--// main content -->
		<%@ include file="../..//main/m_menu.jsp" %>
	</div>
	<!--// 점검 목록 리스트  page -->	
	
	<!-- 긴급 점검 추가  page -->
	
	<!--// 긴급 점검 추가  page -->	
	
	
	<%@include file="/WEB-INF/view/main/m_js.jsp"%>
	<script type="text/javascript">
		 function addCsInstall(){
			 alert('hi');
		 }
		 function csInfo(){
			var m_idx = $(event.target).attr('m_idx');
			$('#inspectApp').empty();
			 $.ajax({
					url:'/install/inspectApp/'+m_idx,
					success : function(data){
						$('#inspectApp').append(data);
					}
			});
		 }
	</script>
</body>
</html>