<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
			<div class="ui-grid-solo">
				<div class="ui-block-a list-box-wrap">
					<!-- 1. 신청 고객 정보 -->
					<div class="width-100">
						<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. 신청 고객 정보</h4>
						<table class="table text-left width-100">
							<tbody id="contractApp">
							</tbody>
							<!-- install/table/contractApp.jsp -->
						</table>
					</div>
					<br>
					<div class="width-100" id="ifInfo">
						
					</div>
					<input type="hidden" name="m_idx" >
					<input type="hidden" name="mi_idx">
				</div>
				<div class="list-center-box padding-10px-top">
					<a href="#installComplete" onclick="fileupload()" class="width-100 btn bg-info">설치 완료</a>
				</div>
			
				<!-- 설치 확인서 등록 팝업 -->
				<div data-role="popup" id="installComplete" data-overlay-theme="a">
					<a href="javascript:;" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
					<div class="popup-content">	
						<form role="form" id="frm" name="frm" method="post" action="installconfirmation" enctype="multipart/form-data"> 
							<input type="hidden" name="m_idx" >
							<input type="hidden" name="mi_idx">
							<!-- 1. 설치 확인서 등록 -->			 
 							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. 설치 확인서 등록</h4>
 							<div class="fileinput">
 								<input type="file" data-clear-btn="true" name="file" id="file" value="" />
 							</div>
									
							<!--// 1. 설치 확인서 등록 -->
							<!-- 2. 설치 확인 내용 -->
							<div class="margin-25px-top">
								<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;2. 설치 확인 내용</h4>
								<div class="form-group form-group-default ">
									<label>설치 완료일</label>
									<input type="date" name="ds_date">
								</div>
								<div class="form-group">
									<label for="if_addmemo">기사 메모</label>
									<input type="text" data-clear-btn="true" name="if_addmemo" id="if_addmemo" value="" />
								</div>
								<div class="form-group">
									<label for="if_extra_memo">추가공임비용</label>
									<input type="text" data-clear-btn="true" name="if_extra" id="if_extra" value="" />
								</div>
								<div class="form-group">
									<label for="if_extra">추가공임사유</label>
									<input type="text" data-clear-btn="true" name="if_extra_memo" id="if_extra_memo" value="" /> 
								</div>
								
								<div class="form-group">
									<label>설치유형</label>
									<select name="m_jubsoo">
										<option>일반설치</option>
										<option>방문견적/무료체험설치</option>
									</select>
								</div>
								<div class="margin-15px-top">
                      		 		 <a onclick="installconfirmation()" class="width-100 btn btn-submit">설치완료</a>         
                  			   </div>
							</div>
							
						</form>
					</div>
				</div>	
				<!--// 설치 확인서 등록 팝업 -->
				
				<!-- 설치 확인서 수정 팝업 -->
				<div data-role="popup" id="reInstallFile" data-overlay-theme="a">
					<a href="javascript:;" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
					<div class="popup-content">	
						<form name="reInstallFileForm" id="reInstallFileForm" method="post" action="installconfirmation" enctype="multipart/form-data"> 
							<input type="hidden" name="m_idx" >
							<input type="hidden" name="mi_idx">
							<input type="hidden" name="if_idx">
							<!-- 1. 설치 확인서 등록 -->			 
 							<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. 설치 확인서 재등록</h4>
 							<div class="fileinput">
 								<input type="file" data-clear-btn="true" name="file" id="re_file"/>
 							</div>
 							
							<div class="margin-15px-top">
                   		 		<a onclick="reuploadInstallFile()" class="width-100 btn btn-submit">재등록</a>         
               			   </div>
							<!--// 1. 설치 확인서 등록 -->
							<!-- 2. 설치 확인 내용 -->
							<div class="margin-25px-top">
								<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;2. 설치 확인 내용</h4>
								<div class="form-group form-group-default ">
									<label>설치 완료일</label>
									<input type="date" name="ds_date">
								</div>
								<div class="form-group">
									<label for="if_addmemo">기사 메모</label>
									<input type="text" data-clear-btn="true" name="if_addmemo"/>
								</div>
								<div class="form-group">
									<label for="if_extra">추가공임비용</label>
									<input type="number" data-clear-btn="true" name="if_extra"/>
								</div>
								<div class="form-group">
									<label for="if_extra_memo">추가공임사유</label>
									<input type="text" data-clear-btn="true" name="if_extra_memo"/> 
								</div>
								<div class="margin-15px-top">
                      		 		 <a onclick="updateInstallFile()" class="width-100 btn btn-submit">수정</a>         
                  			   </div>	
							</div>
							<!--// 2. 설치 확인 내용 --> 
						</form>
					</div>
				</div>	
				<!--// 설치 확인서 수정 팝업 -->	
				
				
				<!-- 무료체험 팝업 -->
				<div data-role="popup" id="freeTest" data-overlay-theme="a">
					<a href="javascript:;" data-rel="back" class="popup-close"><i class="icon-x"></i></a>
					<div class="popup-content">	
						<form role="form" id="frm" name="frm" method="post" action="freeTest" enctype="multipart/form-data"> 
<!-- 							<input type="hidden" name="m_idx" > -->
							<input type="hidden" name="mi_idx">
							<!-- 1. 무료체험 확인 내용 -->
							<div class="margin-25px-top">
								<h4><i class="icon-folder-search"></i>&nbsp;&nbsp;1. 무료 체험 확인 내용</h4>
								<div class="form-group form-group-default ">
									<label>무료체험 시작일</label>
									<input type="date" name="m_free_begin" onchange="endDate()">
								</div>
								<div class="form-group">
									<label for="if_addmemo">무료체험 기간</label>
									<input type="text" data-clear-btn="true" name="m_free_day" id="m_free_day" onkeyup="endDate()"/>
								</div>
								<div class="form-group">
									<label for="if_extra_memo">무료체험 종료일</label>
									<input type="text" name="m_free_finish" id="m_free_finish" readonly/>
								</div>
								<div class="form-group">
									<label for="if_extra">무료체험 메모</label>
									<input type="text" data-clear-btn="true" name="m_free_memo" id="m_free_memo" /> 
								</div>
																
								<div class="margin-15px-top">
                      		 		 <a onclick="freeTest()" class="width-100 btn btn-submit">무료체험 등록</a>         
                  			   </div>
							</div>
							
						</form>
					</div>
				</div>	
				<!--// 설치 확인서 등록 팝업 -->
				
						
			</div>
			
			<script>
			function fileupload(){
				document.getElementById("frm").reset();
				var m_idx = $('input[name=m_idx]').val();
				var mi_idx = $('input[name=mi_idx]').val();
				
				$('input[name=if_memo]').val('');
				$('input[name=if_extra_memo]').val('');
				$('input[name=if_extra]').val('');
				
				$('input[name=ds_date]').val('');
				$('input[name=if_addmemo]').val('');
				
				$.ajax({
					url:'/customer/filecheck/'+m_idx,
					success : function(data){
						if(data.msg.trim() == '등록'){
							$('#re_file').val('');
							$('input[name=if_idx]').val(data.if_idx);
							$('input[name=ds_date]').val(data.ds_date);
							$('input[name=if_addmemo]').val(data.if_addmemo);
							$('input[name=if_extra]').val(data.if_extra_f);
							$('input[name=if_extra_memo]').val(data.if_extra_memo);
							
							$('#reInstallFile').popup('open');
						}else{
							$('#installComplete').popup('open');
						}
					}
				});  
			};
			
			function installconfirmation(){
				var param=$('form[name=frm]').serialize();
				var m_idx = $('input[name=m_idx]').val();
				var mi_idx = $('input[name=mi_idx]').val();
				$('input[name=mi_idx]').val();	
				$('#frm').ajaxForm({
					url:'/install/installconfirmation',
					type:'post',
					enctype:"multipart/form-data",
					 contentType: false,
					success:function(res){
						if(res.trim()=='등록완료'){
							$('#installComplete').popup('close');
							alert('등록에 성공했습니다.');
							location.href='/install/installApp';
							/* $('#contractApp').empty();
							$.ajax({
								url:'/customer/contractApp/'+m_idx,
								data:{'m_idx':m_idx,'mi_idx':mi_idx},
								type:'GET',
								success : function(data){
									$('#contractApp').append(data);
									$('input[name=m_idx]').val(m_idx);
									$('input[name=mi_idx]').val(mi_idx);
								}
							}); */		
						}else{
							alert(res);
						}
					}
				});
				$("#frm").submit() ;
			}
			
			//설치확인서 재등록
			function reuploadInstallFile(){
				if($('#re_file').val()==''){
					alert('파일이 없습니다.');
					return;
				}
				
				if(window.confirm('설치확인서 재등록 하시겠습니까?\n2.설치확인내용은 업로드 되지 않습니다.')){
					$('#reInstallFileForm').ajaxForm({
						url:'/install/reuploadInstallFile',
						enctype:"multipart/form-data",
						type:'post',
						success:function(res){
							if(res.trim()=='등록완료'){
								alert('등록에 성공했습니다.');
								location.href='install/installApp';
							}else{
								alert(res);
							}
						},
						fail:function(){
							alert("오류 발생.\n잠시 후 시도해주세요.");
						}
					});
					 $("#reInstallFileForm").submit() ;
				}
			}
			//설치확인내용 수정
			function updateInstallFile(){
				if(window.confirm('설치 확인 내용 수정하시겠습니까?\n1.설치확인서는 업로드 되지 않습니다.')){
					$.ajax({
						url:'/install/addComplete?'+$('#reInstallFileForm').serialize(),
						type:'put'
					}).fail(function(){
						alert('잠시 후 다시 시도해주세요.');
					}).done(function(res){
						alert(res);
					});
				}
			}
</script>