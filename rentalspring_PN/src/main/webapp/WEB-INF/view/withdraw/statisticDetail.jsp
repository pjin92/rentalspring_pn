<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>연체관리>고객 정보</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">연체관리 <i class=" icon-arrow-right13"></i> 고객</span>
							<a onclick="pdf()" class="btn btn-sm bg-teal-400 pull-right mt-20">출력</a> 
							<small class="display-block">정보</small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- 고객정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<!-- onclick="$('#art1Panel').toggle('show')" -->
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>1.계약 고객 정보</h5>
								</div> 
								<div class="panel-body" id="art1Panel">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12">
												<c:choose>
												<c:when test="${empty mi_contract}">
													<p>계약자 정보 없음</p>
												</c:when>
												<c:otherwise>
												<table class="table table-bordered nowrap">
													<tbody>
														<tr>
															<th style="width:20%" class="active">고객명</th>
															<td style="width:30%" class="text-left">${mi_contract[0].mi_name}</td>
															<th style="width:20%" class="active">담당자</th>
															<td style="width:30%" class="text-left">${member.e_name}</td>
														</tr>
														<tr>
															<th class="active">회원번호</th>
															<td class="text-left">${member.m_num}</td>
															<th class="active">생년월일(계약자)</th>
															<td class="text-left">${mi_contract[0].mi_id}</td>
														</tr>
														<tr>
															<th class="active">연락처(계약자)</th>
															<td class="text-left">${mi_contract[0].mi_phone}</td>
															<th class="active">결제일</th>
															<td class="text-left">${member.m_paydate}</td>
														</tr>
														<tr>
															<th class="active">주소(계약자)</th>
															<td class="text-left" colspan="3">${mi_contract[0].mi_addr1} ${mi_contract[0].mi_addr2} ${mi_contract[0].mi_addr3} ${mi_contract[0].mi_addr4}</td>
														</tr>
														<tr>
															<th class="active">자동이체(${member.m_pay_method})</th>
															<td class="text-left" colspan="3">${member.m_pay_info} ${member.m_pay_num}</td>
														</tr>
														<tr>
															<th class="active">렌탈상품명</th>
															<td class="text-left">${member.pr_name }</td>
															<th class="active">계약월수</th>
															<td class="text-left">${member.m_period }</td>
														</tr>
														<tr>
															<th class="active">계약일자</th>
															<td class="text-left">${member.m_final_date}</td>
															<th class="active">월렌탈료</th>
															<td class="text-left">${member.m_rental_f}</td>
														</tr>
														<tr>
															<th class="active">시작년월</th>
															<td class="text-left">${member.m_paystart}</td>
															<th class="active">만기년월</th>
															<td class="text-left">${member.m_payend}</td>
														</tr>
														<tr>
															<th class="active">기준일자</th>
															<td></td>
															<th class="active">해지일자</th>
															<td>
																<input type="text" name="m_end_date" class="form-control text-center daterange-single" style="background: white;" value="${member.m_end_date}" readonly>
															</td>
														</tr>
														<tr>
															<th class="active">금일 입금예정액</th>
															<td class="text-left" class="active"></td>
															<th class="active">부대비용</th>
															<td class="text-left"></td>
														</tr>
													</tbody>
												</table>
												</c:otherwise>
												</c:choose>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--// 고객정보 -->
							<!-- 입출금 정보 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<!-- style="cursor: pointer;" onclick="$('#art2Panel').toggle('show')" -->
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>2.입출금 정보</h5>
								</div>
								<div class="panel-body" id="art2Panel">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12">
												<table class="table no-sorting table-hover nowrap" id="statTable">
													<thead>
														<tr>
															<th class="active" data-orderable="false">회차</th>
															<th class="active" data-orderable="false">결제일자</th>
															<th class="active" data-orderable="false">납부여부</th>
															<th class="active" data-orderable="false">연체일수</th>
															<th class="active" data-orderable="false">월렌탈료</th>
															<th class="active" data-orderable="false">연체료</th>
															<!-- <th class="active" data-orderable="false">중도상환금액</th>
															<th class="active" data-orderable="false">청구금액</th> -->
															<th class="active" data-orderable="false">입금액</th>
															<th class="active" data-orderable="false">미청구렌탈료</th>
															<th class="active" data-orderable="false">최종입금일자</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${pay}" var="pay">
														<c:set var="bgCss">
															<c:choose>
																<c:when test="${pay.pay_state_str eq '신청중'}">
																	style="background:#f6f6ff;"
																</c:when>
																<c:when test="${pay.pay_state_str eq '출금완료'}">
																	style="background:#ebfff4;"
																</c:when>
																<c:when test="${pay.delayedDay > 0 }">
																	style="background:#FFFEE0;"
																</c:when>
																
																<c:otherwise></c:otherwise>
															</c:choose>
														</c:set>
														<tr ${bgCss}>
															<td>${pay.pay_cnt}</td>
															<td>${pay.pay_original_date}</td>
															<td>${pay.pay_state_str}</td>
															<td>${pay.delayedDay}</td>
															<td>${pay.pay_money_f}</td>
															<td>
																<c:choose>
																<c:when test="${pay.delayedDay > 0 }">
																	${pay.pay_diff_f}
																</c:when>
																<c:otherwise>
																	0
																</c:otherwise>
																</c:choose>
															</td>
															<td>${pay.pay_real_f}</td>
															<td>
																<c:choose>
																<c:when test="${pay.delayedDay eq 0 }">
																	${pay.pay_diff_f}
																</c:when>
																<c:otherwise>
																	0
																</c:otherwise>
																</c:choose>
															</td>
															<td>${pay.pay_date}</td>
														</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--// 입출금 정보 -->
							
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<!-- style="cursor: pointer;" onclick="$('#art3Panel').toggle('show')" -->
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="월 렌탈료 X {(의무사용일수 - 사용일수) / 30} X 30%" data-html="true"></i>
										3. 위약금 계산
									</h5>
								</div> 
								<div class="panel-body">	  
									<div class="form-group-attahced">
										<div class="row">
											<div class="col-xs-12">
												<table class="table table-bordered nowrap">
													<tbody>
														<tr>
															<th class="active">렌탈 개월수</th>
															<td>${member.m_period}</td>
															<th class="active">월 렌탈금액</th>
															<td>${member.m_rental_f}</td>
															
														</tr>
														<tr>
															<th class="active">설치/배송 완료일</th>
															<td>${ds[0].ds_date}</td>
															<th class="active">위약금 계산 기준일</th>
															<td>
																<input type="text" class="form-control input-sm daterange-single text-center" name="penaltyDate" value="${today}" readonly="readonly" style="background: none;">
															</td>
														</tr>
														<tr>
															<th class="active">배송/설치상태</th>
															<td>${ds[0].ds_state}</td>
															<th class="active">위약금</th>
															<td id="penaltyMoney"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div> 
							</div>
							
							<!-- 메모 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<!-- style="cursor: pointer;" onclick="$('#art3Panel').toggle('show')" -->
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>4.메모</h5>
								</div> 
								<div class="panel-body" id="art3Panel">	  
									<form id="art3">
										<div class="form-group-attahced">
											<div class="row">
												<div class="col-xs-12">
													<textarea class="form-control input-sm" style="resize:vertical" name="m_memo">${member.m_memo }</textarea>
												</div>
											</div>
										</div>
									</form>
								</div> 
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn btn-default" onclick="window.close()">닫기 </a>
											<a class="btn bg-teal-400 position-right" onclick="memo()">저장 </a>
										</div>
									</div>
								</div>
							</div>
							<!--// 메모  -->
						</div>
					</div>
				</div>				
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	$('#statTable').DataTable({
		serverSide: false,
		autoWidth: false,
		"iDisplayLength": -1,
	    lengthMenu: [
	        [ 5, 10, 20, -1 ],
	        [ '5행', '10행', '20행', '행 전체' ]
	    ],
	    dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
	    language: {
	        search: '_INPUT_',
	        searchPlaceholder: '재검색',
	        paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
	        emptyTable: '데이터가 없습니다.',
	        info: '_TOTAL_개의 검색결과 [ _START_ ~ _END_  ]',
	        infoEmpty: '',
	        infoFiltered: '(총 _MAX_개의 자료 중에서 재검색)',
	        lengthMenu: '_MENU_',
	        zeroRecords: '검색 결과가 없습니다.'
	    },
	    drawCallback: function () {
	        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
	    },
	    preDrawCallback: function() {
	        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
	    },
	    colReorder: true,
	    colReorder: {
	        fixedColumnsLeft: 1
	    }
	    //order: [[ 16, 'desc' ]],
	});
	
	
	//메모저장
	function memo(){
		var memo=$('textarea[name=m_memo]').val();
		blockById('art3');
		$.ajax({
			url:'memo',
			type:'post',
			data:'m_memo='+memo
		}).done(function(res){
			alert(res);
		}).always(function(){
			$('#art3').unblock();
		});
	}
	
	//출력 팝업
	function pdf(){
		location.href='print';
	}
	
	penalty();
	$('input[name=penaltyDate]').on('apply.daterangepicker',function(ev,picker) {
		$('input[name=penaltyDate]').val(picker.startDate.format('YYYY-MM-DD'));
		penalty();
	});
	//위약금계산
	function penalty(){
		var m_rental=${member.m_rental};
		var m_period=${member.m_period};
		var sd='${ds[0].ds_date}';//설치완료일
		var pd=$('input[name=penaltyDate]').val();

		if(sd!=''){
			var sd_date=new Date(sd);
			var pd_date=new Date(pd);
			
			//diff=사용일수
			var diff=pd_date.getTime() - sd_date.getTime();
		    diff = Math.ceil(diff / (1000 * 3600 * 24));
			if(diff<0){
				$('#penaltyMoney').html('위약금 계산 기준일이 설치/배송완료일 이전입니다.');
			}else{
				var pen=m_rental*(m_period-(Number(diff)/30) )*0.3;
				pen=Math.floor(pen/10)*10;
				if(pen<0){
					$('#penaltyMoney').html('0');
				}else{
					$('#penaltyMoney').html( comma(pen));
				}
			}
		}else{
			$('#penaltyMoney').html('설치/배송완료일이 없습니다.');
		}
	}
	$('[data-toggle=tooltip]').tooltip();
	</script>
</body>
</html>