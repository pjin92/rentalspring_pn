<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>연체관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">연체관리</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content" id="divContent">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="렌탈전,방문완료,렌탈중"></i>
										연체관리 검색 조건
									</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body"> 
									<form name="searchForm"  onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">상품명</label>
													<select class="form-control input-sm" name="pr_idx">
														<option value="">전체</option>
														<c:forEach var="pr" items="${prList}">
														<option value="${pr.pr_idx}">${pr.pr_name}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">팀</label>
													<select class="form-control input-sm" name="e_department">
														<option value="전체">전체</option>
														<c:forEach var="dept" items="${deptList}">
														<option value="${dept}">${dept}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">담당자</label>
													<input type="text" class="form-control input-sm" name="e_name">
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">고객명</label>
													<input type="text" class="form-control input-sm" name="mi_name">
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">회원번호</label>
													<input type="text" class="form-control input-sm" name="m_num">
												</div>
											</div>
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>관리건 조회</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
	
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<!-- <th data-orderable="false">선택</th> -->
											<th class="active">렌탈 상품명</th>
											<th class="active">담당자</th>
											<th class="active">부서</th>
											<th class="active">계약 고객명</th>
											<th class="active">연락처(계약자)</th>
											<th class="active">회원번호</th>
											<th class="active">결제일</th>
											<th class="active">연체개월</th>
											<th class="active">계약 월수</th>
											<th class="active">총렌탈료</th>
											<th class="active">미청구렌탈료</th>
											<th class="active">연체렌탈료</th>
											<th class="active">월렌탈료</th>
											<th class="active">입금액</th>
											<!-- <th class="active">전담당자</th>
											<th class="active">전전담당자</th> -->
											<th class="active">신용등급</th>
											<th class="active">진행상태</th>
										</tr>
									</thead>
									<tbody></tbody>
									<tfoot>
										<tr>
											<!-- <th data-orderable="false">선택</th> -->
											<th class="active" colspan="7"></th>
											<th class="active">합계</th>
											<th class="active">0</th>
											<th class="active">0</th>
											<th class="active">0</th>
											<th class="active">0</th>
											<th class="active" colspan="3"></th>
										</tr>
									</tfoot>
								</table>
							
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="refreshTable('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<%@include file="modal/damdangModal.jsp"%>
	<script>
	
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": "pr_name" },
		    { "data": "e_name" },
		    { "data": "e_department" },
		    { "data": "midx"},
		    { "data": "mi_phone"},
		    { "data": "m_num" },
		    { "data": "m_paydate" },
		    { "data": "delay" },
		    { "data": "m_period" },
		    { "data": "m_total_real_f" },
		    { "data": "unpaidMoney_f" },
		    { "data": "delayMoney_f" },
		    { "data": "m_rental_f" },
		    { "data": "paidMoney_f" },
		    { "data": "m_cb" },
		    { "data": "m_process" }
		  ]
	});
	//로딩화면
	tableLoadUnderModal('checkTable');
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		var param=$('form[name=searchForm]').serialize();
		$('#checkTable').DataTable({
	    	serverSide: true,
	    	ajax:{
	    		url:'/withdraw/statistic',
	    		type:'post',
	    		data:param
	    	},
	    	footerCallback:function(tfoot,data,start,end,display){
	    		
	    		var total=0;
	    		var un=0;
	    		var delay=0;
	    		var mrental=0;
	    		for(var i=0;i<data.length;i++){
	    			total+=data[i].m_total_real;
	    			un+=data[i].unpaidMoney;
	    			delay+=data[i].delayMoney;
	    			mrental+=data[i].m_rental;
	    		}
	    		$(tfoot).find('th').eq(2).html(comma(total));
	    		$(tfoot).find('th').eq(3).html(comma(un));
	    		$(tfoot).find('th').eq(4).html(comma(delay));
	    		$(tfoot).find('th').eq(5).html(comma(mrental));
	    	},
	    	cache:false,
	    	autoWidth: false,
	        lengthMenu: [
	            [ 5, 10, 20, -1 ],
	            [ '5행', '10행', '20행', '행 전체' ]
	        ],
	        dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
	        language: {
	            search: '_INPUT_',
	            searchPlaceholder: '재검색',
	            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
	            emptyTable: '데이터가 없습니다.',
	            info: '_TOTAL_개의 검색결과 [ _START_ ~ _END_  ]',
	            infoEmpty: '',
	            infoFiltered: '(총 _MAX_개의 자료 중에서 재검색)',
	            lengthMenu: '_MENU_',
	            zeroRecords: '검색 결과가 없습니다.'
	        },
	        drawCallback: function () {
	            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
	        },
	        preDrawCallback: function() {
	            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
	        },
	        colReorder: true,
	        colReorder: {
	            fixedColumnsLeft: 1
	        },
	        keys: {
	            className: 'alpha-slate'
	        }
	    });
	}
	//조회 버튼 클릭시
	function searchBtn(){
		var param=$('form[name=searchForm]').serialize();
		var table=$('#checkTable').DataTable();
		table.ajax.url('/withdraw/statistic?'+param).load(function(res){
			res.footer
		},null);
		
	}
	//담당자 배정
	function eid(){
		var midx=$(event.target).parent().next().next().children('a').attr('midx');
		var name=$(event.target).parent().next().next().children('a').html();
		var mnum=$(event.target).parent().next().next().next().html();
		name+='( '+mnum+' )';
		
		$('#damdangMiname').html(name);
		
		if(! $.fn.DataTable.isDataTable( '#damdangModalTable' )){
			$.extend( $.fn.dataTable.defaults, {
				"columns": [
				    { "data": "mdh_log" },
				    { "data": "e_name" }
				  ]
			});
			ajaxDtGet('damdangModalTable','/member/'+midx+'/damdang/history');
			$('#damdangModalTable_filter').parent().hide();
		}else{
			var table=$('#damdangModalTable').DataTable();
			table.ajax.url('/member/'+midx+'/damdang/history').load(null,false);
		}
		document.damdang.e_id.value='';
		$('#damdangMidx').val(midx);
		$('#damdangModal').modal('show');
		
		
	}
	//고객명 클릭 팝업
	function midx(){
		var midx=Number( $(event.target).attr('midx') );
		window.open('/withdraw/statistic/'+midx+'/','statistic','width=1000,height=600,top=100,left=200');
	}
	
	$('[data-toggle=tooltip]').tooltip();
	</script>
</body>
</html>