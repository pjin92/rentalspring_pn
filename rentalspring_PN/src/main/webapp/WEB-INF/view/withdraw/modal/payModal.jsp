 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="payModal" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span>출금일자</span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body" id="payBody">
					<div class="panel">
						<div class="panel-body">
							<div class="col-xs-4" id="payModalDate">
								<div class="form-group form-group-default">
									<div class="input-icon right">
										<label for="form_control_1">출금 신청일</label>
										<input type="text" class="form-control" name="paySendDate" readonly="readonly">
									</div>
								</div>
							</div>
						</div>
						<div class="panel-footer panel-footer-transparent">			
							<a class="heading-elements-toggle"><i class="icon-more"></i></a>
							<div class="heading-elements">	
								<button type="button" class="btn btn-xs bg-primary-600 legitRipple pull-right" onclick="batch()">출금신청</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>