 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="damdangModal" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog">
		<div class="modal-content-wrap">
			<div class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span>담당 배정</span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal">
							<i class="icon-cross2 cursor-pointer"></i>
						</span>
					</h4>
				</div>

				<div class="modal-body" id="damdangBody">
					<div class="panel">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group form-group-default">
										<div class="input-icon right">
											<label for="form_control_1">계약고객명</label>
											<span id="damdangMiname"></span>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<form name="damdang">
										<input type="hidden" name="m_idx" id="damdangMidx">
										<div class="form-group form-group-default">
											<div class="input-icon right">
												<label for="form_control_1">담당 배정</label>
												<select class="form-control input-sm" name="e_id">
													<option value="">선택</option>
													<c:forEach var="emp" items="${empList}">
													<option value="${emp.e_id}">${emp.e_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</form>
									<button type="button" class="btn btn-xs bg-primary-600 legitRipple pull-right mt-15" onclick="assignDamdang()">담당 배정</button>
								</div>
							</div>
						</div>
						<div class="panel-footer panel-footer-transparent">			
							<div class="col-xs-12">
								<table class="table table-hover border-top border-default" id="damdangModalTable">
									<thead class="alpha-grey">
										<tr>
											<th data-orderable="false">배정일</th>
											<th data-orderable="false">담당자</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
//모달에서 담당배정
function assignDamdang(){
	var param=$('form[name=damdang]').serialize();
	blockById('damdangBody');
	$.ajax({
		url:'/member/'+document.damdang.m_idx.value+'/damdang',
		type:'post',
		data:param
	}).done(function(res){
		alert(res);
		document.damdang.e_id.value='';
		refreshTableBlock('damdangModalTable');
		refreshTableBlock('checkTable');
	}).always(function(){
		$('#damdangBody').unblock();
	});
}
</script>