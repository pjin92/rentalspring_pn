 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="psModal" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="bankPsDiv" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="psModalTitle"></span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel">
						<table class="table table-hover nowrap" id="psModalTable">
							<thead>
								<tr>
									<th>선택</th>
									<th>입금 은행</th>
									<th>CMS ID</th>
									<th>고객번호</th>
									<th>고객명(결제)</th>
									<th>신청 상태</th>
									<th>출금액</th>
									<th>비고</th>
								</tr>
							</thead>
						</table>
						
						<div class="panel-footer panel-footer-transparent">			
							<a class="heading-elements-toggle"><i class="icon-more"></i></a>
							<div class="heading-elements">	
								<div class="btn-group dropup ml-20">
									<button type="button" class="btn btn-xs bg-primary-600 btn-labeled" onclick="cancelPay()"><b><i class="icon-air"></i></b>신청목록에서 삭제</button>
								</div>
								<a type="button" id="sendBankSms" class="btn btn-xs bg-success legitRipple ml-15" onclick="sendBankSms()">일괄 문자 발송</a>
								<button type="button" class="btn btn-xs bg-primary-600 legitRipple ml-15" onclick="refreshTable('psModalTable')">새로고침</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>