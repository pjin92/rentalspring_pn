<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>연체관리>고객 정보>출력</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<style type="text/css">
	@media print {
		div.noprint {display: none;}
	}
	</style>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper"> 
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title clearfix">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">연체관리<i class=" icon-arrow-right13"></i>고객 정보<i class=" icon-arrow-right13"></i>출력</span>
							<a onclick="window.print()" class="btn btn-sm bg-teal-400 pull-right mt-20 ml-20">출력</a> 
							<a onclick="history.go(-1)" class="btn btn-sm bg-teal-400 pull-right mt-20">이전</a>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- 연체 고객 내역 조회 -->
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading">
									<!-- onclick="$('#art1Panel').toggle('show')" -->
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>연체 고객 내역 조회</h5>
								</div> 
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12">
												<c:choose>
												<c:when test="${empty mi_contract}">
													<p>계약자 정보 없음</p>
												</c:when>
												<c:otherwise>
												<table class="table table-bordered">
													<colgroup>
														<col style="width:20%">
														<col style="width:80%">
													</colgroup>
													<tbody>
														<tr>
															<th class="alpha-slate">고객명</th>
															<td class="text-left">${mi_contract[0].mi_name}</td>
														</tr>
														<tr>
															<th class="alpha-slate">회원번호</th>
															<td class="text-left">${member.m_num}</td>
														</tr>
														<tr>
															<th class="alpha-slate">생년월일</th>
															<td class="text-left">${mi_contract[0].mi_id}</td>
														</tr>
														<tr>
															<th class="alpha-slate">결제일</th>
															<td class="text-left">${member.m_paydate}</td>
														</tr>
														<tr>
															<th class="alpha-slate">연체개월수</th>
															<td class="text-left">${memberDelay.delay}</td>
														</tr>
														<tr>
															<th class="alpha-slate">계약일자</th>
															<td class="text-left">${member.m_final_date}</td>
														</tr>
														<tr>
															<th class="alpha-slate">연체일자</th>
															<td class="text-left">
																<c:if test="${memberDelay.delay >0}">
																${firstDelay}
																</c:if>
															</td>
														</tr>
														<tr>
															<th class="alpha-slate">해지일자</th>
															<td class="text-left">
																${member.m_end_date}
															</td>
														</tr>
													</tbody>
												</table>
												</c:otherwise>
												</c:choose>
											</div>
											<div class="col-xs-12 mt-50">
												<table class="table table-framed" style="table-layout:fixed;">
													<thead>
														<tr>
															<th class="active">상품명</th>
															<th class="active">총렌탈료</th>
															<th class="active">계약월수</th>
															<th class="active">월렌탈료</th>
															<th class="active">연체렌탈료</th>
															<th class="active">연체료</th>
															<th class="active">연체금액</th>
															<th class="active">정상청구액</th>
															<th class="active">미청구렌탈료</th>
															<th class="active">위약금계</th>
															<th class="active">합계</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>${member.pr_name}</td>
															<td>${member.m_total_f}</td>
															<td>${member.m_period}</td>
															<td>${member.m_rental_f}</td>
															<td>${memberDelay.delayMoney_f}</td> 
															<td>${memberDelay.delayMoney_f}</td>
															<td>${memberDelay.delayMoney_f}</td>
															<td>${memberDelay.delayMoney_f}</td> 
															<td>${memberDelay.unpaidMoney_f}</td> 
															<td></td>
															<td></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	
	</script>
</body>
</html>