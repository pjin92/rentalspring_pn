<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>채권관리>카드출금관리</title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
<style type="text/css">
.borderThead tr th{
	border-right: 1px solid #ddd;
	background: #eef1f5;
}
.daterangepicker.dropdown-menu{
	z-index: 4000;
}
</style>
</head>
<body class="sidebar-xs navbar-top">
<%@include file="/WEB-INF/view/main/topbar.jsp" %>
<div class="page-container">
	<div class="page-content">
		<div class="content-wrapper">
			<%@include file="/WEB-INF/view/main/menu.jsp" %>
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
					<div class="page-title">
						<h4>
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">카드 출금 관리</span>
							<small class="display-block"></small>
						</h4>
					</div>
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			<div class="content" id="divContent">
				<div class="row">
					<div class="col-xs-12">
						<!-- Search layout-->
						<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
							<div class="panel-heading">
								<h5 class="panel-title">
									<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="" data-html="true"></i>
									카드 출금 검색 조건
								</h5>
								
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>
			
							<div class="panel-body">
							
								<form name="searchForm" onkeypress="if(event.keyCode==13){searchBtn();}">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">기수</label>
												<select class="form-control input-sm" name="mg_idx">
													<option value="">선택</option>
													<c:forEach var="mg" items="${memberGroups}">
													<option value="${mg.mg_idx}">${mg.mg_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-xs-6 col-sm-3 col-md-2 col-lg-1">
											<div class="form-group">
												<label class="form-tit-label">년</label>
												<select class="form-control input-sm" name="pay_year">
													<option value="">선택</option>
													<c:forEach begin="${payYear.min}" end="${payYear.max}" step="1" var="year" varStatus="yearStatus">
														<option ${yearStatus.index eq yearNow ?'selected':'' }>${yearStatus.index}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-xs-6 col-sm-3 col-md-2 col-lg-1">
											<div class="form-group">
												<label class="form-tit-label">월</label>
												<select class="form-control input-sm" name="pay_month">
													<option value="">선택</option>
													<c:forEach begin="1" end="12" step="1" var="month" varStatus="monthStatus">
														<option ${monthStatus.index eq monthNow ?'selected':'' }>${monthStatus.index}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-xs-6 col-sm-3 col-md-2 col-lg-1">
											<div class="form-group">
												<label class="form-tit-label">약정일</label>
												<select class="form-control input-sm" name="m_paydate">
													<option value="">선택</option>
													<c:if test="${not empty payDates}">
													<c:forEach var="payDate" items="${payDates}">
													<option value="${payDate}">${payDate}</option>
													</c:forEach>
													</c:if>
												</select>
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">출금상태</label>
												<select class="form-control input-sm" name="pay_state">
													<option value="">선택</option>
													<option value="신청전">신청전</option>
													<option value="신청중">신청중</option>
													<option value="출금완료">출금완료</option>
													<option value="출금실패">출금실패</option>
												</select>
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">회원번호</label>
												<input type="text" class="form-control input-sm" name="m_num">
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">회원명(계약자/결제자)</label>
												<input type="text" class="form-control input-sm" name="mi_name">
											</div>
										</div>
				
										<div class="col-xs-12 text-right">
											<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
										</div>
									</div>					
								</form>
							</div>
						</div>
						<!-- /Search layout -->
					
					
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>카드 출금 조회</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>

							<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
								<thead>
									<tr>
										<th>선택</th>
										<th>회원번호</th>
										<th>회원명(계약자/결제자)</th>
										<th>렌탈상품명</th>
										<th>기수</th>
										<th>금융기수</th>
										<th>연락처(결제자)</th>
										<th>년</th>
										<th>월</th>
										<th>약정일</th>
										<th>출금신청일</th>
										<th>이월전 출금예정일(이월횟수)</th>
										<th>출금회차</th>
										<th>출금 시도수</th>
										<th>출금상태</th>
										<th>월 이체액</th>
										<th>사유</th>
										<th data-orderable="false"></th>
									</tr>
								</thead>
								<tbody id="cardTbody">
								</tbody>
							</table>
						
							<div class="panel-footer panel-footer-transparent">			
								<a class="heading-elements-toggle"><i class="icon-more"></i></a>
								<div class="heading-elements">	
									<div class="btn-group dropup ml-20">
										<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
										<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
										<ul class="dropdown-menu">
											<li><a onclick="payCard()"><i class="icon-trash-alt"></i>출금 신청</a></li>
											<li><a onclick="direct()"><i class="icon-trash-alt"></i>직접입금</a></li>
											<li><a onclick="directCancel()"><i class="icon-trash-alt"></i>직접입금 취소</a></li>
											<li><hr></li>
											<li><a onclick="refreshTable('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<!-- /content area -->
		</div>
	</div>
</div>

<%@include file="/WEB-INF/view/main/js.jsp"%>
<%@include file="modal/directPayModal.jsp"%>
<script>

//dataTable 컬럼별 들어갈 data
$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "pay_idx" },
	    { "data": "m_num" },
	    { "data": "m_pay_owner" },
	    { "data": "pr_name" },
	    { "data": "mg_name" },
	    { "data": "m_finance" },//금융기수
	    { "data": "m_phone" },
	    { "data": "pay_year" },
	    { "data": "pay_month" },
	    { "data": "m_paydate" },
	    { "data": "pay_date" },
	    { "data": "pay_original" },
	    { "data": "pay_cnt" },
	    { "data": "pay_cnt2" },
	    { "data": "pay_state" },
	    { "data": "pay_money" },
	    { "data": "pay_reason" },
	    { "data": "bttn"}
	  ]
});
//로딩화면
tableLoad('checkTable');
//첫 dataTable initialize
if(! $.fn.DataTable.isDataTable( '#checkTable' )){
	var param=$('form[name=searchForm]').serialize();
	checkDt('checkTable','/withdraw/pay?'+param+'&m_pay_method=카드');
}
//조회 버튼 클릭시
function searchBtn(){
	var param=$('form[name=searchForm]').serialize();
	var table=$('#checkTable').DataTable();
	table.ajax.url('/withdraw/pay?'+param+'&m_pay_method=카드').load();
	
}
//재검색 입력시 기존 요청 취소
$(document).ajaxSend(function(){
	var table=$('#checkTable').DataTable();
	table.settings()[0].jqXHR.abort();
});

//카드출금
function payCard(){
	var param=getPayIdx();
	if(param==''){
		alert('선택된 고객이 없습니다.');
		return;
	}
	if(window.confirm('카드 출금 하시겠습니까?\n출금 성공 시 문자가 발송됩니다.')){//
		blockById('checkTable');
		$.ajax({
			url:'/withdraw/card',
			type:'post',
			data:param
		}).done(function(res){
			alert(res);
			refreshTable('checkTable');
		}).fail(function(){
			alert('통신오류발생.\n관리자에게 연락하세요.');
		}).always(function(){
			$('#checkTable').unblock();
		});
		
	}
}

//직접입금 모달
function direct(){
	var param=getPayIdx();
	if(param==''){
		alert('선택된 고객이 없습니다.');
		return;
	}
	$('input[name=pay_reason]').val('');
	$('#directPayModal').modal('show');
}
$('input[name=pay_date]').daterangepicker({
	startDate: moment(),
    singleDatePicker: true
});
//직접입금
function directPay(){
	var param=getPayIdx();
	if(param==''){
		alert('선택된 고객이 없습니다.');
		return;
	}
	
	blockById('directPayBody');
	$.ajax({
		url:'/withdraw/direct',
		data:param+'&pay_date='+$('input[name=pay_date]').val()+'&pay_reason='+$('input[name=pay_reason]').val(),
		type:'post'
	}).done(function(res){
		alert(res);
		refreshTable('checkTable');
		$('#directPayModal').modal('hide');
	}).always(function(){
		$('#directPayBody').unblock();
	});
}
//직접입금취소
function directCancel(){
	var param=getPayIdx();
	if(param==''){
		alert('선택된 고객이 없습니다.');
		return;
	}
	
	if(window.confirm('직접입금 취소 처리하시겠습니까?')){
		blockById('checkTable');
		$.ajax({
			url:'/withdraw/direct?'+param,
			type:'delete'
		}).done(function(res){
			alert(res);
			refreshTable('checkTable');
		}).always(function(){
			$('#checkTable').unblock();
		});
	}
}

function getPayIdx(){
	var param='pay_idx=';
	var cnt=0;
	$('#cardTbody tr.selected').each(function(){
		param+=$(this).children('td:nth-child(1)').children('input[name=pay_idx]').val()+',';
		cnt++;
	});
	
	if(cnt==0){
		param='';
	}
	return param;
}
//출금조회
function checkPay(){
	var pi=$(event.target).parent('td').parent('tr').children('td:nth-child(1)').children('input[name=pay_idx]').val();
	blockById('checkTable');
	$.ajax('/withdraw/card/'+pi)
	.done(function(res){
		alert(res);
		refreshTable('checkTable');
	}).always(function(){
		$('#checkTable').unblock();
	});
}
//출금취소
function cancelPay(){
	var pi=$(event.target).parent('td').parent('tr').children('td:nth-child(1)').children('input[name=pay_idx]').val();
	blockById('checkTable');
	$.ajax({
		url:'/withdraw/card/'+pi,
		type:'delete'
	})
	.done(function(res){
		alert(res);
		refreshTable('checkTable');
	}).always(function(){
		$('#checkTable').unblock();
	});
}

//팝업
function member(){
	var midx=$(event.target).attr('m_idx');
	var win=window.open('/member/'+Number(midx)+'/pay','pay','width=1000,height=800');
}

$('[data-toggle=tooltip]').tooltip();
</script>
</body>
</html>