<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>채권관리>가상계좌관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
						<div class="page-title">
							<h4>
								<i class="icon-address-book2 position-left"></i> 
								<span class="text-bold">가상계좌관리</span>
								<small class="display-block"></small>
							</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content" id="divContent">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>가상계좌 검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body">
								
									<form name="searchForm"  onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">검색</label>
													<input type="text" class="form-control input-sm" name="mainKeyword">
												</div>
											</div>
					
										<div class="text-right">
											<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
										</div>
									</div>
									</form>
								</div>
							</div>
							<!-- /Search layout -->
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-address-book2 position-left"></i>가상계좌 조회
										<a class="btn btn-primary legitRipple ml-20 text-white" onclick="checkVPay()">입금확인</a>
									</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
	
								<table class="table table-hover nowrap" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<!-- <th data-orderable="false">선택</th> -->
											<th class="active">등록상태</th>
											<th class="active">입금액</th>
											<th class="active">인정회차</th>
											<th class="active">예금주명</th>
											<th class="active">회원번호</th>
											<th class="active">1회용/재사용</th>
											<th class="active">가상계좌번호</th>
											<th class="active">가상계좌은행</th>
											<th class="active">입금은행</th>
											<th class="active" data-orderable="false"></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="refreshTable('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
<script>
//dataTable 컬럼별 들어갈 data
$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "v_condition" },
	    { "data": "v_money_f" },
	    { "data": "v_count" },
	    { "data": "v_name"},
	    { "data": "m_num" },
	    { "data": "v_reuse" },
	    { "data": "v_account" },
	    { "data": "sb_name" },
	    { "data": "cms_id" },
	    { "data": null, "defaultContent":"" }
	  ]
});
//로딩화면
tableLoadUnderModal('checkTable');
//첫 dataTable initialize
if(! $.fn.DataTable.isDataTable( '#checkTable' )){
	var param=$('form[name=searchForm]').serialize();
	$('#checkTable').DataTable({
    	serverSide: true,
    	ajax:{
    		url:'/withdraw/vaccount?mainKeyword=',
    		type:'post',
    		data:param
    	},
    	cache:false,
    	autoWidth: false,
        lengthMenu: [
            [ 5, 10, 20, -1 ],
            [ '5행', '10행', '20행', '행 전체' ]
        ],
        dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '_INPUT_',
            searchPlaceholder: '재검색',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
            emptyTable: '데이터가 없습니다.',
            info: '_TOTAL_개의 검색결과 [ _START_ ~ _END_  ]',
            infoEmpty: '',
            infoFiltered: '(총 _MAX_개의 자료 중에서 재검색)',
            lengthMenu: '_MENU_',
            zeroRecords: '검색 결과가 없습니다.'
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        }
    });
}
//조회 버튼 클릭시
function searchBtn(){
	var param=$('form[name=searchForm]').serialize();
	var table=$('#checkTable').DataTable();
	table.ajax.url('/withdraw/vaccount?'+param).load();
}
//가상계좌입금확인
function checkVPay(){
	window.open('/withdraw/checkVPay','checkVPay','width=1000,height=600,top=100,left=200');
}

/* //고객명 클릭 팝업
function midx(){
	var midx=Number( $(event.target).attr('midx') );
	window.open('/withdraw/statistic/'+midx+'/','statistic','width=1000,height=600,top=100,left=200');
} */
</script>
</body>
</html>