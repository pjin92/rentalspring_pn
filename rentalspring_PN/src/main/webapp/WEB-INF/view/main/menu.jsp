<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="navbar navbar-inverse bg-slate-700" style="z-index: 1030;" id="navbar-second">
	<ul class="nav navbar-nav no-border visible-xs-block">
		<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-paragraph-justify3"></i></a></li>
	</ul>

	<div class="navbar-collapse collapse" id="navbar-second-toggle">
		<ul class="nav navbar-nav">
			<c:forEach items="${menuMap['m']}" var="mm">
				<c:set var="menuIdx">${mm.m_idx}</c:set>
				<c:choose>
				<c:when test="${empty menuMap[menuIdx]}">
					<li class="${mm.active}">
						<a href="${mm.m_uri}">
							<i class=""></i>${mm.m_name}
						</a>
					</li>
				</c:when>
				<c:otherwise>
					<li class="dropdown ${mm.active}">
						<a href="${mm.m_uri}" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button">
							<i class=""></i>${mm.m_name}
						</a>
						<ul class="dropdown-menu bg-slate-600" role="menu">
						<c:forEach items="${menuMap[menuIdx]}" var="sm">
							<li class="${sm.active}">
								<a href="${sm.sm_uri}">${sm.sm_name}</a>
							</li>
						</c:forEach>
						</ul>
					</li>
				</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>
</div>