<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="css.jsp"%>
<title></title>
</head>
<body class="sidebar-xs navbar-top">
<%@include file="topbar.jsp" %>  
<div class="page-container">
	<div class="page-content">
		<div class="content-wrapper">
			<!-- menu -->
			<%@include file="menu.jsp" %>
			<!-- /menu -->
			
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
					<div class="page-title">
						<h4>
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">Main</span>
							<small class="display-block">main</small>
						</h4>
					</div>
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			<form role="form" name="possible" method="post" enctype="multipart/form-data" action="pcustomerNew.do">
			<div class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="panel panel-flat border-top-xlg border-top-primary-300">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-folder-search position-left"></i>main..</h5>
							</div>
							<div class="panel-body">
								<div class="form-group-attached">
									<div class="row">
										<table class="table table-hover datatable-mr-product-popup" id="ptable" data-page-length="25">
											<thead>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			</form>
			<!-- /content area -->
		</div>
	</div>
</div>

<%@include file="js.jsp"%>
</body>
</html>