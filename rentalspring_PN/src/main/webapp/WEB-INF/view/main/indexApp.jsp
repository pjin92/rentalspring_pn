<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>로그인</title>
	<%@include file="/WEB-INF/view/main/m_css.jsp"%>
</head>
<body class="login-container login-cover">
	<!-- 설치관리 리스트 page -->
	<div data-role="page" id="login-page" data-url="login-page"  data-title="로그인">
			<div class="page-content">
				<div class="content-wrapper">
					<div class="content">
						<!-- Form with validation -->
						<form class="form-validate" id="frm" name="frm" action="Applogin" method="post">
							<input type="hidden" name="loc" value="${loc}">
							<div class="login-form">
								<div class="text-center margin-30px-bottom">
<!-- 									<img src="img/gn-logo.png" alt="제너럴네트" /> -->
								</div>
								<div class="form-group has-feedback has-feedback-left">
									<input type="text" class="form-control" placeholder="아이디" name="e_logid" required="required">
									<div class="form-control-feedback">
										<i class="icon-user text-muted"></i>
									</div>
								</div>
								<div class="form-group has-feedback has-feedback-left">
									<input type="password" class="form-control" placeholder="비밀번호" name="e_pass" required="required">
									<div class="form-control-feedback">
										<i class="icon-lock2 text-muted"></i>
									</div>
								</div> 
								<a href="#" onclick="document.getElementById('frm').submit();"  data-ajax="false" class="btn bg-indigo width-100">Login<i class="icon-arrow-right14"></i></a>
							</div>
						</form>
						<!-- /form with validation -->
					</div>
				</div>
			</div>
	</div>
	<%@include file="/WEB-INF/view/main/m_js.jsp"%>
</body>
</html>