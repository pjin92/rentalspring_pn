<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>Rental 고객사</title>
	<jsp:include page="css.jsp"></jsp:include>
</head>
    <body class="login-container login-cover">
        <div class="page-container">
			<!-- Page content -->
			<div class="page-content">
				<!-- Main content -->
				<div class="content-wrapper">
					<!-- Content area -->
					<div class="content">
						<!-- Form with validation -->
						<form class="form-validate" action="loginCustomer" method="post">
							<div class="panel panel-body login-form">
								 <div class="text-center">
<!-- 									<img src="assets/images/logo.png" alt="제너럴네트" /> -->
									<h5 class="content-group mt-20">
										<small class="display-block">고객사 로그인</small>
									</h5>
								</div>
								<div class="form-group has-feedback has-feedback-left">
									<input type="text" class="form-control" placeholder="아이디" name="e_logid" required="required">
									<div class="form-control-feedback">
										<i class="icon-user text-muted"></i>
									</div>
								</div>
								<div class="form-group has-feedback has-feedback-left">
									<input type="password" class="form-control" placeholder="비밀번호" name="e_pass" required="required">
									<div class="form-control-feedback">
										<i class="icon-lock2 text-muted"></i>
									</div>
								</div> 
								<!-- <div class="form-group login-options">
									<div class="row">
										<div class="col-sm-6">
											<label class="checkbox-inline">
												<input type="checkbox" class="styled" checked="checked">
												Remember
											</label>
										</div>
	
										<div class="col-sm-6 text-right">
											<a href="login_password_recover.html">Forgot password?</a>
										</div>
									</div>
								</div> -->
								<div class="form-group mt-20">
									<button type="submit" class="btn bg-pink-400 btn-block">Login<i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</form>
						<!-- /form with validation -->
					</div>
					<!-- /content area -->
				</div>
				<!-- /main content -->
			</div>
			<!-- /page content -->
		</div>
	<jsp:include page="js.jsp"></jsp:include>
    </body>
</html>