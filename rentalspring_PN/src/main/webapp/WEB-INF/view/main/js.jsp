<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
<!---------- Core JS files ---------->
<script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>

<!---------- 기능별 JS ---------->
<!--select-->
<script type="text/javascript" src="/assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="/assets/js/pages/form_select2.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/selects/selectboxit.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/inputs/duallistbox.min.js"></script>

<!-- time -->
<script type="text/javascript" src="/assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="/assets/js/plugins/pickers/jquery.mtz.monthpicker.js"></script>

<!-- DatePicker -->
<script type="text/javascript" src="/assets/js/plugins/notifications/jgrowl.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/pickers/anytime.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/pickers/pickadate/picker.js"></script>
<script type="text/javascript" src="/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script type="text/javascript" src="/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
<script type="text/javascript" src="/assets/js/plugins/pickers/pickadate/legacy.js"></script>

<!-- table -->
<script type="text/javascript" src="/assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/key_table.min.js"></script><!--셀 선택 / 페이지 이동-->
<script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/col_reorder.min.js"></script><!--열 순서변경-->
<script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script><!--엑셀다운-->
<script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

<!---------- 공통 JS ---------->

<!-- DatePicker -->
<script type="text/javascript" src="/assets/js/pages/picker_date.js"></script>
<script type="text/javascript" src="/assets/js/plugins/ui/ripple.min.js"></script>
<!-- <script type="text/javascript" src="/assets/js/pages/uploader_bootstrap.js"></script> --> 
<!-- <script type="text/javascript" src="/assets/js/pages/form_select2.js"></script> -->
<!-- <script type="text/javascript" src="/assets/js/pages/sidebar_components.js"></script> -->

<!-- sidebar -->
<script type="text/javascript" src="/assets/js/plugins/ui/drilldown.js"></script><!--토글버튼-->
<script type="text/javascript" src="/assets/js/pages/layout_fixed_custom.js"></script><!--사이드바 롤오버 우측펼침-->
<script type="text/javascript" src="/assets/js/plugins/ui/nicescroll.min.js"></script><!--내부 스크롤-->	

<script type="text/javascript" src="/assets/js/plugins/visualization/d3/d3.min.js"></script> <!--툴팁↓-->
<script type="text/javascript" src="/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="/assets/js/plugins/ui/headroom/headroom.min.js"></script><!--스크롤 시 상단메뉴 숨김↓-->
<script type="text/javascript" src="/assets/js/plugins/ui/headroom/headroom_jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/pages/layout_navbar_hideable_sidebar.js"></script>
<script type="text/javascript" src="/assets/js/plugins/ui/prism.min.js"></script><!--메뉴 호버↓-->
<script type="text/javascript" src="/assets/js/plugins/buttons/hover_dropdown.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/forms/wizards/form_wizard/form.min.js"></script><!-- formajax -->
<!-- <script type="text/javascript" src="/assets/js/pages/layout_navbar_secondary_fixed.js"></script>2단메뉴고정 -->	

<script type="text/javascript" src="/assets/js/core/app.js"></script> <!--필수-->
<script type="text/javascript" src="/assets/js/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="/resources/commonJs/datatable.js"></script><!-- 테이블 설정 -->
<!-- ajax요청시 뜨는 검은 배경 -->
<div onclick="$(this).fadeOut();" style="display:none;background:black;width: 5000px;height: 5000px;position: fixed;top: -10px;left: -10px;opacity: 0.7; z-index: 1051;" id="black"></div>
<script type="text/javascript" src="/resources/commonJs/common.js?dt=0820"></script>
<!-- 숫자관련 제이쿼리 -->
<script type="text/javascript" src="/assets/js/jquery.numberformatter.min.js"></script>

