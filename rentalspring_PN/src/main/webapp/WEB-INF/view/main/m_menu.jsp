<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<div data-role="footer" class="footer-wrap" data-position="fixed">
			<div data-role="navbar" class="nav-glyphish-example" data-grid="c">
				<ul data-inset="true" data-theme="a">
					<li><a data-ajax="false" href="/install/installApp" data-icon="custom" class="menu1">설치관리</a></li>
					<li><a data-ajax="false" href="/install/installCompleteApp" data-icon="custom" class="menu2">CS점검관리</a></li>
					<li><a data-ajax="false" href="/stock/ledRequestApp" data-icon="custom" class="menu3">LED재고요청</a></li>
					<li><a data-ajax="false" href="/install/installCalculateApp" data-icon="custom" class="menu4">정산관리</a></li>
				</ul>
			</div>
		</div>