<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="/assets/css/core.css" rel="stylesheet" type="text/css">
<link href="/assets/css/components.css?update=0823" rel="stylesheet" type="text/css">
<link href="/assets/css/colors.css" rel="stylesheet" type="text/css">
<link href="/assets/css/form_box.css" rel="stylesheet" type="text/css"> 