<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="/assets/m_css/jquery.mobile.custom.structure.css" rel="stylesheet" type="text/css">
<link href="/assets/m_css/jquery.mobile.custom.theme.css" rel="stylesheet" type="text/css">
<link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="/assets/m_css/style.css" rel="stylesheet" type="text/css">