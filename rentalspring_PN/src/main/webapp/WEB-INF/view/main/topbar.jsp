<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!-- Main navbar -->
	<div class="navbar navbar-fixed-top bg-white"> <!--navbar-default / navbar-inverse bg-blue-700-->
		<div class="navbar-header">
			<a class="navbar-brand" href="/main">
<!-- 				<img src="/assets/images/logo.png" alt=""> -->
			</a>
			<ul class="nav navbar-nav pull-right visible-xs-block"><!--모바일 버튼-->
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-move-left"></i></a></li><!--사이드바 토글-->
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-browser"></i></a></li><!--상단메뉴-->
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<!-- <ul class="nav navbar-nav">
				사이드바 토글
				<li>
					<a class="sidebar-control sidebar-main-hide hidden-xs" data-popup="tooltip" title="사이드바 토글" data-placement="bottom" data-container="body">
						<i class="icon-move-left"></i>
					</a>
				</li>
				사이드바 펼침
				<li>
					<a class="sidebar-control sidebar-main-toggle hidden-xs" data-popup="tooltip" title="사이드바 펼침" data-placement="bottom" data-container="body">
						<i class="icon-transmission"></i>
					</a>
				</li>
			</ul> -->

			<div class="navbar-right">
				<ul class="nav navbar-nav">	
					<li class="dropdown">
						<a class="dropdown-toggle" href="http://${info.sp_si_domain}" target="_blank">
						<!-- <a class="dropdown-toggle" href="http://58.229.240.244/gn_shop_shopping/index.do" target="_blank"> -->
							<i class="icon-store"></i>
							<span class="visible-xs-inline-block position-right">ShopShopping이동</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
							<i class="icon-folder-open"></i>
							<span class="visible-xs-inline-block position-right">미확인 문서함</span>
							<span class="status-mark border-orange-400"></span><!--신규 표시-->
						</a>
						<div class="dropdown-menu dropdown-content width-350">
							<div class="dropdown-content-heading">
								<!--미확인 문서함-->
								<ul class="icons-list">
									<li>${login.e_name}님 &nbsp;</li>
									<li>
										<a onclick="changePW()" data-popup="tooltip" title="" data-placement="bottom" data-container="body" data-original-title="비밀번호변경"><i class="icon-pencil7"></i></a>
									</li>
									<!-- <li>
										<a href="logout.do" data-popup="tooltip" title="" data-placement="bottom" data-container="body" data-original-title="로그아웃"><i class="icon-switch2"></i></a>
									</li> -->
								</ul>
							</div>
							<!--<ul class="media-list dropdown-content-body">
								<li class="media">
									<div class="media-left">
										<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
										<span class="badge bg-danger-400 media-badge">5</span>
									</div>

									<div class="media-body">
										<a href="javascript:;" class="media-heading">
											<span class="text-semibold">James Alexander</span>
											<span class="media-annotation pull-right">04:58</span>
										</a>

										<span class="text-muted">who knows, maybe that would be the best thing for me...</span>
									</div>
								</li>
							</ul>-->
							<div class="dropdown-content-footer">
								<a href="#" data-popup="tooltip" title="전체 보기"><i class="icon-menu display-block"></i></a>
							</div>
						</div>
					</li>	
					<li class="dropdown">
						<a class="dropdown-toggle" href="/index">
							<i class="icon-exit"></i>
							<span class="visible-xs-inline-block position-right">로그아웃</span>
						</a>
					</li>
					<!-- <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
							<i class="icon-quill4"></i>
							<span class="visible-xs-inline-block position-right">디자인 관리</span>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li>
								<a href="design.jsp">
									<i class="icon-pencil-ruler"></i>디자인 모드
								</a>
							</li> 
							<li class="dropdown-submenu dropdown-submenu-left">
								<a href="javascript:;"><i class="icon-eye"></i>권한별 보기</a>
								<ul class="dropdown-menu">
									<li><a href="javascript:;"></span>최고관리자<span class="label bg-blue-400 pull-right">you</span></a></li>
									<li><a href="javascript:;">관리자</a></li>
									<li><a href="javascript:;">회계담당자<span class="label bg-orange-400 pull-right">now</span></a></li>
									<li><a href="javascript:;">사원</a></li>
								</ul>
							</li>
						</ul>
					</li> -->
				</ul>
			</div>
		</div>
	</div>
	<!--// Main navbar -->