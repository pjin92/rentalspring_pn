<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head lang="ko">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>sms발송 내역</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<style type="text/css">
	.daterangepicker.dropdown-menu{z-index: 4000;}
	</style>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp" %>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp" %>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">sms발송 내역</span>
							<small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->
				
				<!-- Content area -->
				<div class="content" id="divContent">
					<div class="row">
						<div class="col-xs-12"> 
							<!-- Search layout-->
							<!-- <div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-folder-search position-left"></i>검색 조건</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
				
								<div class="panel-body"> 
									<form name="searchForm">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">회원번호</label>
													<input type="text" class="form-control input-sm" name="m_num">
												</div>
											</div> 
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">회원명(계약자/결제자)</label>
													<input type="text" class="form-control input-sm" name="mi_name">
												</div>
											</div>
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>					
									</form>
								</div>
							</div> -->
							<!-- /Search layout -->
						
						
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>sms발송 내역</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>
	
								<table class="table table-hover" id="checkTable" data-page-length="25">
									<thead>
										<tr>
											<th data-orderable="false">선택</th>
											<th data-orderable="false">고객번호</th>
											<th data-orderable="false">수신번호</th>
											<th data-orderable="false">내용</th>
											<th data-orderable="false">발신번호</th>
											<th data-orderable="false">발송일</th>
											<th data-orderable="false">발송결과</th>
										</tr>
									</thead>
									<tbody id="bankTbody">
									</tbody>
								</table>
							
								<div class="panel-footer panel-footer-transparent">			
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">	
										<div class="btn-group dropup ml-20">
											<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
											<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a onclick="refreshTableBlock('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>
	
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	//dataTable 컬럼별 들어갈 data
	$.extend( $.fn.dataTable.defaults, {
		"columns": [
		    { "data": null,"defaultContent":"" },
		    { "data": "m_num" },
		    { "data": "sh_phone" },
		    { "data": "sh_content" },
		    { "data": "sh_from" },
		    { "data": "sh_senddate" },
		    { "data": "sh_resultcode_str" },
		  ]
	});
	//첫 dataTable initialize
	if(! $.fn.DataTable.isDataTable( '#checkTable' )){
		var param=$('form[name=searchForm]').serialize();
		checkDt('checkTable','/statistic/sms?'+param);
	}
	
	tableLoad('checkTable');
	//재검색 입력시 기존 요청 취소
	$(document).ajaxSend(function(){
		var table=$('#checkTable').DataTable();
		table.settings()[0].jqXHR.abort();
	});
	
	</script>
</body>
</html>