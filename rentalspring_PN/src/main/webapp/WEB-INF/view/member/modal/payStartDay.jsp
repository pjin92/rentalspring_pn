 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="payStartModal" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_payStart" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i>결제 시작일 변경<span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel">
					<div class="panel-body">
						<span>회차/월 단위로 일괄 결제시작일이 변경됩니다.<br>기존 이월했던 데이터를 덮어씁니다.</span>
					<form id="payStartForm">
						<input type="hidden" name="m_paydate" value="${member.m_paydate}">
						<table class="table table-bordered nowrap">
							<tr>
								<th class="active">결제 시작일</th>
								<td>${member.m_paystart}</td>
								<th class="active">렌탈 기간</th>
								<td>${member.m_period} 개월</td>
							</tr>
							<tr>
								<th class="active">결제 시작월 변경</th>
								<td colspan="2">
									<select name="pay_year">
										<c:forEach begin="2017" end="2030" var="year">
										<option ${startYear eq year?'selected':''}>${year}</option>
										</c:forEach>
									</select>년
									<select name="pay_month">
										<c:forEach begin="1" end="12" var="month">
										<option ${startMonth eq month?'selected':''}>${month}</option>
										</c:forEach>
									</select>월&nbsp;
									${member.m_paydate}일
								</td>
								<td><a class="btn bg-teal-400" onclick="payStart()">결제 시작일 변경</a></td>
							</tr>
						</table>
						</form>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>