 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="coModal" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i>${payMap.pay_cnt}회차 이월<span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel">
					<div class="panel-body">
					<form id="coForm">
						<table class="table table-bordered nowrap">
							<tr>
								<th class="active">${payMap.pay_cnt}회차 원결제일</th>
								<td>${payMap.pay_original_date}</td>
								<th class="active">결제시작일</th>
								<td>${payMap.m_paystart}</td>
							</tr>
							<tr>
								<th class="active">${payMap.pay_cnt}회차 결제 년월</th>
								<td><select name="pay_year">
										<c:forEach begin="2017" end="2030" var="year">
										<option ${payMap.pay_year eq year?'selected':''}>${year}</option>
										</c:forEach>
									</select>년
								</td>
								<td>
									<select name="pay_month">
										<c:forEach begin="1" end="12" var="month">
										<option ${payMap.pay_month eq month?'selected':''}>${month}</option>
										</c:forEach>
									</select>월
								</td>
								<td><a class="btn bg-teal-400" onclick="carryOver('${payMap.pay_idx}')">결제월 변경</a></td>
							</tr>
						</table>
						</form>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>