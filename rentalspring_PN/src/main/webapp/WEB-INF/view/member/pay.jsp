<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>고객 출금 내역</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">출금내역</span>
						</h4> 
					</div>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading clearfix">
									<h5 class="panel-title pull-left"><i class="icon-folder-search position-left"></i>1.계약고객 정보</h5>
									<a onclick="$('#miContract').toggle('show')" class="pull-right text-slate-800"><i class="icon-menu-open"></i></a>
								</div> 
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row" id="miContract">
											<c:choose>
											<c:when test="${empty mi_contract}">
												계약자 정보 없음
											</c:when>
											<c:otherwise>
											<table class="table table-bordered nowrap">
												<tbody>
													<tr>
														<th style="width:20%" class="active">회원번호</th>
														<td style="width:30%" class="text-left">${member.m_num}</td>
														<th style="width:20%" class="active">고객명</th>
														<td style="width:30%" class="text-left">${mi_contract[0].mi_name}</td>
													</tr>
													<tr>
														<th class="active">연락처(계약자)</th>
														<td class="text-left">${mi_contract[0].mi_phone}</td>
														<th class="active">결제일</th>
														<td class="text-left">${member.m_paydate}</td>
													</tr>
													<tr>
														<th class="active">자동이체(${member.m_pay_method})</th>
														<td colspan="3" class="text-left">${member.m_pay_info} ${member.m_pay_num}</td>
													</tr>
													<tr>
														<th class="active">렌탈상품명</th>
														<td class="text-left">${member.pr_name }</td>
														<th class="active">계약월수</th>
														<td class="text-left">${member.m_period }</td>
													</tr>
													<tr>
														<th class="active">월 렌탈료</th>
														<td class="text-left">${member.m_rental_f}</td>
														<th class="active">총 렌탈료</th>
														<td class="text-left">${member.m_total_f}</td>
													</tr>
													<tr>
														<th class="active">시작년월</th>
														<td class="text-left">${member.m_paystart}</td>
														<th class="active">만기년월</th>
														<td class="text-left">${member.m_payend}</td>
													</tr>
													<tr>
														<th class="active">계약고객 메모</th>
														<td colspan="3">
															<textarea class="form-control input-sm" style="resize:vertical" readonly>${mi_contract[0].mi_memo }</textarea>
														</td>
													</tr>
													<tr>
														<th class="active">메모</th>
														<td colspan="3">
															<textarea class="form-control input-sm" style="resize:vertical" name="m_memo">${member.m_memo }</textarea>
														</td>
													</tr>
												</tbody>
											</table>
											</c:otherwise>
										</c:choose>
										</div>
									</div>
								</div>
								
								<c:if test="${login.e_power ne '1' }">
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a onclick="memo()" class="btn bg-teal-400">저장</a>
										</div>
									</div>
								</div>
								</c:if>
								
							</div>
							<div class="panel panel-flat border-top-xlg border-top-primary-300">
								<div class="panel-heading clearfix">
									<h5 class="panel-title pull-left"><i class="icon-folder-search position-left"></i>2. 출금내역</h5>
									<a onclick="$('#pays').toggle('show')" class="pull-right text-slate-800"><i class="icon-menu-open"></i></a>
								</div> 
								<div class="panel-body">
									<div class="form-group-attached">
										<div class="row" id="pays">
											<c:if test="${login.e_power ne '1' }">
											<div class="text-right">
												<a class="btn bg-teal-400 mb-10 mr-5" onclick="batchCarryOver()">일괄 이월</a> 
												<a class="btn bg-slate-600 mb-10" onclick="payStartModal()">결제 시작일 변경</a> 
											</div>
											</c:if>
											
											<div class="" style="overflow:auto; max-height:700px;">
												<table class="table table-bordered nowrap">
													<thead>
														<tr>
															<th class="active">선택</th>
															<th class="active">회차</th>
															<th class="active">요청 횟수</th>
															<th class="active">년</th>
															<th class="active">월</th>
															<th class="active">출금액</th>
															<th class="active">출금 요청일</th>
															<th class="active">상태</th>
															<th class="active">비고</th>
															<th class="active">이월/회차변경</th>
															<th class="active">이월 일자(횟수)</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="pay" items="${payList}"> 
														<c:set var="trBg">
														<c:choose>
															<c:when test="${pay.unpaid eq '미납'}">
																style="background:#FFFEE0;"
															</c:when>
															<c:when test="${pay.pay_cnt%2 eq 0}">
																style="background:#f2f2f2;"
															</c:when>
														</c:choose>
														</c:set>
														
														<c:set var="stateStyle">
														<c:if test="${pay.pay_state eq '출금실패'}">
															style="color: red;"
														</c:if>
														<c:if test="${pay.pay_state eq '출금완료'}">
															style="color: blue;"
														</c:if>
														</c:set>
														
														<tr ${trBg}>
															<td>
																<c:if test="${pay.pay_state eq '신청전' }">
																<input type="checkbox" name="pay_idxs" value="${pay.pay_idx}">
																</c:if>
															</td>
															<td>${pay.pay_cnt}</td>
															<td>${pay.pay_cnt2}</td>
															<td>${pay.pay_year}</td>
															<td>${pay.pay_month}</td>
															<td>${pay.pay_money_f}</td>
															<td>${pay.pay_date}</td>
															<td><span ${stateStyle}>${pay.pay_state}</span></td>
															<td>${pay.pay_reason}</td>
															<td>
																<c:if test="${login.e_power ne '1' }">
																	<c:if test="${pay.pay_state eq '신청전' }">
																	<a class="btn btn-sm bg-teal-400" onclick="carryOverModal(${pay.pay_idx})">이월</a>
																	</c:if>
																	<c:if test="${pay.pay_state eq '출금완료' }">
																	<a class="btn btn-sm bg-slate-600" onclick="changeCntModal(${pay.pay_idx},${pay.pay_cnt})">회차 변경</a>
																	</c:if>
																</c:if>
															</td>
															<td>${pay.pay_carryover_date}(${pay.pay_carryover_time})</td>
														</tr>
														</c:forEach>
														<c:remove var="trBg"/>
														<c:remove var="stateStyle"/>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-footer">
									<div class="heading-elements">
										<div class="heading-btn pull-right">
											<a class="btn btn-default" onclick="window.close()">닫기 </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalDiv"></div>
	
	<div id="cntModal" class="modal modal-center" data-dismiss="modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrap">
				<div id="inside_cnt" class="modal-content alpha-slate">
					<div class="modal-header">
						<h4 class="modal-title text-black">
							<i class="icon-menu2 position-left"></i><span class="cntSpan"></span>회차 변경<span class="text-light"></span>
							<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
						</h4>
					</div>
	
					<div class="modal-body">
						<div class="panel">
							<div class="panel-body">
								<form id="cntForm">
									<input name="pay_idx" type="hidden">
									<table class="table table-bordered nowrap">
										<tr>
											<th class="active">변경할 회차</th>
											<td>
												<select class="form-control input-sm" name="pay_cnt">
													<c:forEach begin="1" end="${member.m_period}" var="cnt">
													<option>${cnt}</option>
													</c:forEach>
												</select>
											</td>
											<td><a class="btn bg-teal-400" onclick="changeCnt()">회차 변경</a></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
	function memo(){
		blockById('artMemo');
		var param=$('textarea[name=m_memo]').val();
		$.ajax({
			url:'./memo?m_memo='+encodeURIComponent(param),
			type:'put'
		}).done(function(res){
			alert(res.msg);
			$('textarea[name=m_memo]').val(res.m_memo);
		}).always(function(){
			$('#artMemo').unblock();
		});
	}
	//이월 modal
	function carryOverModal(pay){
		blockById('artPay');
		$('#modalDiv').load('./pay/'+pay,function(){
			$('#coModal').modal('show');
			$('#artPay').unblock();
		});
	}
	
	//이월
	function carryOver(payidx){
		blockById('inside_ml');
		var param=$('#coForm').serialize();
		$.ajax({
			url:'./pay/'+payidx+'?'+param,
			type:'put'
		}).done(function(res){
			alert(res);
			if(res=='이월 완료'){
				location.reload();
			}
		}).always(function(){
			$('#inside_ml').unblock();
		});
	}
	//회차 변경 modal
	function changeCntModal(pay,cnt){
		$('input[name=pay_idx]').val(pay);
		$('select[name=pay_cnt]').val(cnt);
		$('.cntSpan').html(cnt);
		$('#cntModal').modal('show');
	}
	//회차 변경
	function changeCnt(){
		var param=$('#cntForm').serialize();
		blockById('inside_cnt');
		$.ajax({
			type:'put',
			url:'./changeCnt?'+param
		}).done(function(res){
			alert(res.msg);
			if(res.result>0){
				location.reload();
			}
		}).always(function(){
			$('#inside_cnt').unblock();
		});
	}
	
	//결제시작일 modal
	function payStartModal(){
		blockById('artPay');
		$('#modalDiv').load('./payStart',function(){
			$('#payStartModal').modal('show');
			$('#artPay').unblock();
		});
	}
	//결제시작일 변경
	function payStart(){
		var param=$('#payStartForm').serialize();
		blockById('inside_payStart');
		$.ajax({
			type:'put',
			url:'./payStart?'+param
		}).done(function(res){
			alert(res.msg);
			if(res.result>0){
				location.reload();
			}
		}).always(function(){
			$('#inside_payStart').unblock();
		});
	}
	
	//일괄이월
	function batchCarryOver(){
		var pay='';
		$('input[name=pay_idxs]:checked').each(function(){
			if(this.checked==true){
				pay+=this.value+',';
			}
		});
		if(pay!=''){
			blockById('artPay');
			$('#modalDiv').load('./pay/'+pay,function(){
				$('#coModal').modal('show');
				$('#artPay').unblock();
			});
		}
	}
	</script>
</body>
</html>