<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>렌탈약정서 업로드</title>
</head>
<body>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<div class="p-20">
		<form id="form8" method="post" enctype="multipart/form-data">
			<input name="file" type="file" class="">
			<div class="text-right mt-20">
				<a onclick="uploadArt('rental')" class="btn bg-teal-400 mr-20">파일 등록</a>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/forms/wizards/form_wizard/form.min.js"></script><!-- formajax -->
	
	<script>
	function uploadArt(type){
		$('#form8').ajaxForm({
			url:'/customer/contract/${m_idx}/8?type=rental',
			enctype:"multipart/form-data",
			success:function(res){
				alert(res.msg);
				window.opener.refreshTable('psModalTable');
				window.self.close();
			}
		});
		$("#form8").submit() ;
	}
	</script>
</body>
</html>