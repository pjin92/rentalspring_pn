 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="bankModal" class="modal modal-center" data-dismiss="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content-wrap">
			<div id="inside_ml" class="modal-content alpha-slate">
				<div class="modal-header">
					<h4 class="modal-title text-black">
						<i class="icon-menu2 position-left"></i><span id="bankModalTitle">CMS 선택</span><span class="text-light"></span>
						<span class="label label-flat label-rounded label-icon border-slate text-slate-600 no-margin-top pull-right" data-dismiss="modal"><i class="icon-cross2" style="cursor:pointer" ></i></span>
					</h4>
				</div>

				<div class="modal-body">
					<div class="panel">
						<div class="col-xs-4" id="bankModalDate">
							<div class="form-group form-group-default">
								<div class="input-icon right">
									<label for="form_control_1">신청일</label>
									<input type="text" class="form-control" name="ps_date" readonly="readonly">
								</div>
							</div>
						</div>
						<table class="table table-hover nowrap" id="bankModalTable">
							<thead>
								<tr>
									<th>입금 은행</th>
									<th>CMS ID</th>
									<th>메모</th>
									<th>총 대출 가능 금액</th>
									<th>대출 진행 금액</th>
									<th>잔액</th>
									<th>월 출금액</th>
									<th>회원수</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>