<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>금융관리>카드CMS관리</title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
<%@include file="/WEB-INF/view/main/topbar.jsp" %>
<div class="page-container">
	<div class="page-content">
		<div class="content-wrapper">
			<%@include file="/WEB-INF/view/main/menu.jsp" %>
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
					<div class="page-title">
						<h4>
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">카드CMS관리</span>
							<small class="display-block"></small>
						</h4>
					</div>
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			<div class="content" id="divContent">
				<div class="row">
					<div class="col-xs-12">
						<!-- Search layout-->
						<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
							<div class="panel-heading">
								<h5 class="panel-title">
									<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="금융기수가 있는 고객만 검색" data-html="true"></i>
									카드 회원 검색 조건
								</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>
			
							<div class="panel-body">  
								<form name="searchForm"  onkeypress="if(event.keyCode==13){searchBtn();}">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">금융기수</label>
												<select class="select select2-hidden-accessible" name="m_finance">
													<option value="전체">전체</option>
													<c:forEach var="fList" items="${financeList}">
													<option>${fList.m_finance}</option>
													</c:forEach>
												</select>
											</div>
										</div> 
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">CMS등록상태</label>
												<select class="select select2-hidden-accessible" name="m_cms">
													<option value="">전체</option>
													<option>미등록</option>
													<option>등록완료</option>
													<option>등록중(파일미등록)</option>
													<option>등록중</option>
													<option>수정요청</option>
													<option>수정중</option>
												</select>
											</div>
										</div> 
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">회원번호</label>
												<input type="text" class="form-control input-sm" name="m_num">
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">고객명(계약자/결제자)</label>
												<input type="text" class="form-control input-sm" name="m_pay_owner">
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">진행상태</label>
												<select class="select select2-hidden-accessible" name="m_process">
													<option value="">전체</option>
													<c:forEach var="mprocess" items="${mprocessList}">
													<option>${mprocess}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-xs-12 text-right">
											<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
										</div>
									</div>					
								</form>
							</div>
						</div>
						<!-- /Search layout -->
					
					
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>카드 회원 조회결과</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>

							<table class="table table-hover" id="checkTable" data-page-length="25">
								<thead>
									<tr>
										<th class="active">선택</th>
										<th class="active">최종계약일</th>
										<th class="active">기수</th>
										<th class="active">금융기수</th>
										<th class="active">고객번호</th>
										<th class="active">고객명(계약자/결제자)</th>
										<th class="active">렌탈상품명</th>
										<th class="active">연락처(결제자)</th>
										<th class="active">약정일</th>
										<th class="active">월 이체액</th>
										<th class="active">진행상태</th>
										<th class="active">CMS등록상태</th>
										<th class="active">CMS ID</th>
										<th class="active">비고</th>
										<th class="active" data-orderable="false"></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						
							<div class="panel-footer panel-footer-transparent">			
								<a class="heading-elements-toggle"><i class="icon-more"></i></a>
								<div class="heading-elements">	
									<div class="btn-group dropup ml-20">
										<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
										<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
										<ul class="dropdown-menu">
											<li><a onclick="selectCms()"><i class="icon-trash-alt"></i>회원 등록 신청</a></li>
											<li><a onclick="cmsCard('put')"><i class="icon-trash-alt"></i>회원 정보 수정 신청</a></li>
											<li><a onclick="cmsCard('delete')"><i class="icon-trash-alt"></i>회원 삭제</a></li>
											<li><hr></li>
											<li><a onclick="refreshTable('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<!-- /content area -->
		</div>
	</div>
</div>


<%@include file="modal/bankModal.jsp"%>
<%@include file="/WEB-INF/view/main/js.jsp"%>
<script>

//dataTable 컬럼별 들어갈 data
$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "m_idx" },
	    { "data": "m_final_date" },
	    { "data": "mg_name" },
	    { "data": "m_finance" },
	    { "data": "m_num" },
	    { "data": "m_pay_owner" },
	    { "data": "pr_name" },
	    { "data": "m_phone" },
	    { "data": "m_paydate" },
	    { "data": "m_rental_f" },
	    { "data": "m_process" },
	    { "data": "m_cms" },
	    { "data": "cms_id" },
	    { "data": "m_cms_fail" },
	    {"data":null,"defaultContent":"<a class='btn bg-success' onclick='checkCms()'>cms조회</a>"}
	  ]
});

//로딩화면
tableLoad('checkTable');
//첫 dataTable initialize
if(! $.fn.DataTable.isDataTable( '#checkTable' )){
	var param=$('form[name=searchForm]').serialize();
	checkDt('checkTable','/finance/member/card?'+param);
}
//조회 버튼 클릭시
function searchBtn(){
	var param=$('form[name=searchForm]').serialize();
	var table=$('#checkTable').DataTable();
	table.ajax.url('/finance/member/card?'+param).load();
}

//현재 보이는 dataTable refresh
function refreshTable(id){
	var table=$('#'+id).DataTable();
	table.ajax.reload();
}
//회원등록 step1 cms id 선택
function selectCms(){
	var cnt=0;
	$('#checkTable tr.selected').each(function(){
		cnt++;
	});
	if(cnt==0){
		alert('선택된 고객이 없습니다.');
		return;
	}
	
	$('#bankModalTitle').html('회원 등록');
	if(! $.fn.DataTable.isDataTable( '#bankModalTable' )){
		$.extend( $.fn.dataTable.defaults, {
			"columns": [
			    { "data": "cms_bank" },
			    { "data": "cms_id" },
			    { "data": "cms_memo" },
			    { "data": "cms_amount" },
			    { "data": "m_total" },
			    { "data": "m_ilsi" },//잔액 (임시로 칼럼명 씀)
			    { "data": "m_rental" },//월 이체액 (임시로 칼럼명 씀)
			    { "data": "m_idx" }//회원수 (임시로 칼럼명 씀)
			  ]
		});
		ajaxDt('bankModalTable','/finance/cms');
	}else{
		var table=$('#bankModalTable').DataTable();
		table.search('').draw();
	}
	$('#bankModalDate').hide();
	$('#bankModal').modal('show');
}
//회원등록 step2
function cms(){
	var cms_idx=$(event.target).attr('cms_idx');
	var param=getMidx();
	if(param==''){
		alert('선택된 고객이 없습니다.');
		return;
	}
	param='m_idx='+param;
	if(window.confirm('회원 등록 하시겠습니까?')){
		$('#bankModal').modal('hide');
		$.ajax({
			url:'/finance/card/api',
			data:param+'&cms_idx='+cms_idx,
			type:'post'
		}).done(function(res){
			refreshTable('checkTable');
			alert(res);
		});
	}
}

//m_idx가져오기
function getMidx(){
	var param='';
	$('#checkTable tr.selected').each(function(){
		param+=$(this).children('td:nth-child(6)').children('a').attr('m_idx')+',';
	});
	return param;
}
function cmsCard(sendType){
	
	var param=getMidx();
	if(param==''){
		alert('선택된 고객이 없습니다.');
		return;
	}
	param='m_idx='+param;
	
	var title='';
	if(sendType=='post'){
		title='회원등록 하시겠습니까?';
	}else if(sendType=='put'){
		title='회원정보 수정 하시겠습니까?';
	}else if(sendType=='delete'){
		title='회원삭제 하시겠습니까?';
	}else{
		alert('잘못된 접근입니다.');
		return;
	}
	
	if(window.confirm(title)){
		blockById('checkTable');
		$.ajax({
			url:'/finance/card/api?'+param,
			type:sendType
		}).done(function(res){
			alert(res);
			refreshTable('checkTable');
		}).always(function(){
			$('#checkTable').unblock();
		});
		
	}
}
//팝업
function member(){
	var midx=$(event.target).attr('m_idx');
	var win=window.open('/member/'+Number(midx)+'/pay','pay','width=1000,height=800');
}
//cms상태조회
function checkCms(){
	var m_idx=$(event.target).parent().parent().children('td:nth-child(6)').children('a').attr('m_idx');
	blockById('bankTbody');
	$.ajax({
		type:'put',
		url:'/finance/'+m_idx+'/checkCms'
	})
	.done(function(res){
		alert(res);
		refreshTable('checkTable');
	}).fail(function(){
		alert('통신 에러 발생');
	}).always(function(){
		$('#bankTbody').unblock();
	});
}
$('[data-toggle=tooltip]').tooltip();
</script>
</body>
</html>