<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>금융관리>CMS계정관리</title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body class="sidebar-xs navbar-top">
<%@include file="/WEB-INF/view/main/topbar.jsp" %>
<div class="page-container">
	<div class="page-content">
		<div class="content-wrapper">
			<%@include file="/WEB-INF/view/main/menu.jsp" %>
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs -->
					<div class="page-title">
						<h4>
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">CMS계정관리</span>
							<small class="display-block"></small>
						</h4>
					</div>
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			<div class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>CMS계정</h5>
								<div class="heading-elements">
									<div class="btn-group">
										<button type="button" class="btn bg-teal-400 btn-labeled legitRipple" onclick="addPop()"><b><i class="icon-plus3"></i></b>신규등록</button>
									</div>
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>
							
							<table class="table table-hover" id="checkTable" data-page-length="25">
								<thead>
									<tr>
										<th>입금 은행</th>
										<th>CMS ID</th>
										<th>메모</th>
										<th>계좌 번호</th>
										<th>총 대출 가능 금액</th>
										<th>대출 진행 금액</th>
										<th>잔액</th>
										<th>월 출금액</th>
										<th>회원수</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							
							<div class="panel-footer panel-footer-transparent">			
								<a class="heading-elements-toggle"><i class="icon-more"></i></a>
								<div class="heading-elements">	
									<div class="btn-group dropup ml-20">
										<button type="button" class="btn btn-xs bg-primary-600 btn-labeled" onclick="refreshTable()"><b><i class="icon-air"></i></b>새로고침</button>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<!-- /content area -->
		</div>
	</div>
</div>


<%@include file="/WEB-INF/view/main/modal.jsp"%>
<%@include file="/WEB-INF/view/main/js.jsp"%>
<script>
//dataTable 컬럼별 들어갈 data
$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "cms_bank" },
	    { "data": "cms_id" },
	    { "data": "cms_memo" },
	    { "data": "cms_account" },
	    { "data": "cms_amount" },
	    { "data": "m_total" },
	    { "data": "m_ilsi" },//잔액 (임시로 칼럼명 씀)
	    { "data": "m_rental" },//월 이체액 (임시로 칼럼명 씀)
	    { "data": "m_idx" }//회원수 (임시로 칼럼명 씀)
	  ]
});
tableLoad('checkTable');
//첫 dataTable initialize
if(! $.fn.DataTable.isDataTable( '#checkTable' )){
	ajaxDt('checkTable','/finance/cms');
}
//현재 보이는 dataTable refresh
function refreshTable(){
	var table=$('#checkTable').DataTable();
	table.ajax.reload();
}
//신규등록
function addPop(){
	window.open('/finance/cms/add','popup','width=900,height=600');
}
//금융원부 기수 modal
function showFgModal(){
	var table=$('#checkTable').DataTable();
	var count=0;
	table.$('tr.selected').each(function(){
		count++;
	});
	
	$('#modal_large_content').html('선택된 고객: '+count+'명<br><input type="text" name="m_finance"><a onclick="insertFG()">입력</a>');
	$('#modal_large_title').html('금융원부 기수 입력');
	$('#modal_large').modal('show');
	
}
</script>
</body>
</html>