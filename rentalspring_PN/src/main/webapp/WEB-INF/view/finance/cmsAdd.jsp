 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
<div class="page-container">
	<div class="page-content">
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
					<h4 class="page-title">
						<i class="icon-address-book2 position-left"></i> 
						<span class="text-bold">CMS계정</span>
						<small class="display-block">등록</small>
					</h4> 
				</div>
			</div>
			<!-- /page header -->
			<!-- Content area -->
			<div class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="panel panel-flat border-top-xlg border-top-primary-300">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-folder-search position-left"></i>CMS계정 정보 입력</h5>
							</div> 
							<div class="panel-body">
								<form name="cms" id="cmsForm">
									<div class="form-group-attached">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group form-group-default">
													<div class="input-icon right">
														<label for="form_control_1"> 은행명</label>
														<input type="text" class="form-control input-sm" name="cms_bank">
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group form-group-default">
													<div class="input-icon right">
														<label for="form_control_1">계좌번호</label>
														<input type="text" class="form-control input-sm" name="cms_account" onkeyup="numKeyUp()">
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group form-group-default">
													<div class="input-icon right">
														<label for="form_control_1"> CMS ID</label>
														<input type="text" class="form-control input-sm" name="cms_id">
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group form-group-default">
													<div class="input-icon right">
														<label for="form_control_1">CMS 연동 key</label>
														<input type="password" class="form-control input-sm" name="cms_pw">
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group form-group-default">
													<div class="input-icon right">
														<label for="form_control_1">CMS 카드 고유번호</label>
														<input type="password" class="form-control input-sm" name="cms_card">
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group form-group-default">
													<div class="input-icon right">
														<label for="form_control_1">총 대출 가능 금액</label>
														<input type="text" class="form-control input-sm" name="cms_amount" onkeyup="numCommaKeyUp()">
													</div>
												</div>
											</div>
											<div class="col-xs-12">
												<div class="form-group form-group-default">
													<div class="input-icon right">
														<label for="form_control_1">메모</label>
														<textarea class="form-control input-sm" name="cms_memo" style="resize:vertical"></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="panel-footer">
								<div class="heading-elements">
									<div class="heading-btn pull-right">
										<a class="btn btn-default" onclick="window.close()">닫기 </a>
										<a class="btn bg-teal-400 position-right" onclick="cmsRegi()">등록 </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--// Content area -->
		</div>
	</div>
</div>
<%@include file="/WEB-INF/view/main/js.jsp"%>
<script>
function cmsRegi(){
	var param=$('form[name=cms]').serialize();
	blockById('cmsForm');
	$.ajax({
		url:'/finance/cms/add',
		type:'POST',
		data:param
	})
	.done(function(res){
		res=res.trim();
		alert(res);
		if(res=='등록완료'){
			window.opener.refreshTable();
			window.self.close();
		}
	})
	.always(function(){
		$('#cmsForm').unblock();
	});
}
</script>
</body>
</html>