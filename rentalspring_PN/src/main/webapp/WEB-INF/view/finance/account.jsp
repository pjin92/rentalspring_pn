<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>금융관리>금융원부</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<style type="text/css">
	.select2-results > .select2-results__options{
		max-height: 500px;
	}
	</style>
</head>
<body class="sidebar-xs navbar-top">
	<%@include file="/WEB-INF/view/main/topbar.jsp"%>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<%@include file="/WEB-INF/view/main/menu.jsp"%>
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs --> 
						<h4 class="page-title">
							<i class="icon-address-book2 position-left"></i> 
							<span class="text-bold">금융원부 관리</span> <small class="display-block"></small>
						</h4> 
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<div class="content">
					<div class="row">
						<div class="col-xs-12">
							<!-- Search layout-->
							<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="일시불,중도상환,해지 등..제외." data-html="true"></i>
										검색 조건
									</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>

								<div class="panel-body"> 
									<form name="searchForm"  onkeypress="if(event.keyCode==13){searchBtn();}">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">금융 원부 기수</label>  
													<select	name="m_finance" class="select select2-hidden-accessible">
														<option value="">전체</option>
														<c:forEach var="i" items="${gisu}">
															<c:if test="${i.m_finance ne ''}">
																<option value="${i.m_finance}">${i.m_finance}</option>
															</c:if>
														</c:forEach>
													</select>
												</div>
											</div> 
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">판매처</label>
													<input type="text" class="form-control input-sm" placeholder="검색"	name="m_sale"> 
												</div>
											</div> 
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
												<div class="form-group">
													<label class="form-tit-label">판매기수</label> 
													<input type="text" class="form-control input-sm" placeholder="검색" name="mg_name">
												</div>
											</div> 
											<div class="col-xs-12 text-right">
												<a onclick="searchBtn()" class="btn btn-primary"><i lass="icon-arrow-down8 position-left"></i>조회하기</a>
											</div>
										</div>
									</form>
								</div>
							</div>
							<!-- /Search layout -->


							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">
										<i class="icon-address-book2 position-left" ></i>
										<span>렌탈 고객 조회결과</span>
									</h5>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse"></a></li>
										</ul>
									</div>
								</div>

								<table class="table table-hover" id="checkTable"
									data-page-length="25">
									<thead>
										<tr>
											<th>매출구분</th>
											<th>판매처</th>
											<th>금융원부기수</th>
											<th>판매기수</th>
											<th>고객번호</th>
											
											<th>담당자</th>
											<th>설치상태</th>
											<th>진행상태</th>
											<th>계약일자</th>
											
											<th>고객명(계약자)</th>
											<th>주민번호(계약자)</th>
											<th>연락처(계약자)</th>
											<th>우편번호(계약자)</th>
											<th>주소(계약자)</th>

											<th>신용조회등급</th>
											<th>접수일자(녹취계약완료일)</th>
											<th>고객명(배송/설치)</th>
											<th>연락처(배송/설치)</th>
											<th>우편번호(배송/설치)</th>
											<th>주소(배송/설치)</th>

											<th>배송/설치요청일</th>
											<th>배송/설치완료일</th>
											<th>이체희망일</th>
											
											<th>고객명(결제자)</th>
											<th>주민번호(결제자)</th>
											<th>결제방식</th>
											<th>은행명/카드명</th>
											<th>계좌번호/카드번호(유효기간 월/년)</th>
											<th>총 렌탈료</th>

											<th>월 이체금액</th>
											<th>렌탈기간</th>
											<th>결제시작일(이체개시월)</th>
											<th>렌탈상품명</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>

								<div class="panel-footer panel-footer-transparent">
									<a class="heading-elements-toggle"><i class="icon-more"></i></a>
									<div class="heading-elements">
										<div class="btn-group dropup ml-20">
											<button type="button"
												class="btn btn-xs bg-primary-600 btn-labeled">
												<b><i class="icon-air"></i></b>일괄변경
											</button>
											<button type="button"
												class="btn btn-xs bg-primary-600 dropdown-toggle"
												data-toggle="dropdown" aria-expanded="false">
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<!-- <li><a onclick="showFgModal()"><i
														class="icon-trash-alt"></i>금융원부 기수 입력</a></li>
												<li><hr></li> -->
												<li><a onclick="refreshTable()"><i
														class="icon-trash-alt"></i>새로고침</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
		</div>
	</div>


	<%@include file="/WEB-INF/view/main/modal.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
		//dataTable 컬럼별 들어갈 data
		$.extend($.fn.dataTable.defaults, {
			"columns" : 
				[
				{"data" : "m_detail"},
				{"data" : "m_sale"},
				{"data" : "m_finance"},
				{"data" : "mg_name"},
				{"data" : "m_num"},
			
				{"data" : "e_name"},
				{"data" : "ds_state"},
				{"data" : "m_process"},
				{"data" : "m_contract_date"},
				
				{"data" : "c_mi_name"},
				{"data" : "c_mi_id"},
				{"data" : "c_mi_phone"},
				{"data" : "c_mi_post"},
				{"data" : "c_mi_addr"},

				{"data" : "m_cb"},
				{"data" : "m_voice_contract"},
				{"data" : "mi_name"},
				{"data" : "mi_phone"},
				{"data" : "mi_post"},
				{"data" : "d_mi_addr"},
				
				{"data" : "ds_req"},
				{"data" : "ds_date"},
				{"data" : "m_paydate"},
				
				{"data" : "m_pay_owner"},
				{"data" : "m_pay_id"},
				{"data" : "m_pay_method"},
				{"data" : "m_pay_info"},
				{"data" : "m_pay_num_f"},
				{"data" : "m_total_f"},

				{"data" : "m_rental_f"},
				{"data" : "m_period"},
				{"data" : "m_paystart"},
				{"data" : "pr_name"}
				
				]
		});
		tableLoad('checkTable');
		//첫 dataTable initialize
		if (!$.fn.DataTable.isDataTable('#checkTable')) {
			ajaxDt('checkTable', '/finance/account');
		}
		
		//재검색 입력시 기존 요청 취소
		$(document).ajaxSend(function(){
			var table=$('#checkTable').DataTable();
			table.settings()[0].jqXHR.abort();
		});
		
		//조회 버튼 클릭시
		function searchBtn() {
			var param = $('form[name=searchForm]').serialize();
			var table = $('#checkTable').DataTable();
			table.ajax.url('/finance/account?' + param).load();
		}

		//현재 보이는 dataTable refresh
		function refreshTable() {
			var table = $('#checkTable').DataTable();
			table.ajax.reload();
		}
		//금융원부 기수 modal
		function showFgModal() {
			var table = $('#checkTable').DataTable();
			var count = 0;
			table.$('tr.selected').each(function() {
				count++;
			});

			$('#modal_large_content')
					.html(
							'선택된 고객: '
									+ count
									+ '명<br><input type="text" name="m_finance"><a onclick="insertFG()">입력</a>');
			$('#modal_large_title').html('금융원부 기수 입력');
			$('#modal_large').modal('show');

		}
		//금융원부 입력
		function insertFG() {
			var table = $('#checkTable').DataTable();
			var param = '';
			var count = 0;
			table.$('tr.selected').each(
					function() {
						param += $(this).children('td:nth-child(3)').children(
								'a').attr('m_idx')
								+ ',';
						count++;
					});
			if (count == 0) {
				alert('선택된 고객이 없습니다.');
				return;
			}

			var fg = $('input[name=m_finance]').val();
			$.ajax('/finance/checkName?fg=' + fg + '&midxs=' + param).done(
					function(res) {
						if (count == Number(res)) {
							alert('금융기수 입력하였습니다.');
						} else {
							alert(res.trim() + '건 입력완료.');
						}
						$('#modal_large').modal('hide');
						refreshTable();
					});
		}
		
		$('[data-toggle=tooltip]').tooltip();
	</script>
</body>
</html>