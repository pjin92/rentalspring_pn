<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>금융관리>은행CMS관리</title>
	<%@include file="/WEB-INF/view/main/css.jsp"%>
	<style type="text/css">
	/* .borderThead tr th {border-right:1px solid #ddd;}
	.borderThead tr th:first-child {border-left:1px solid #ddd;} */
	.daterangepicker.dropdown-menu{z-index: 4000;}
	</style>
</head>
<body class="sidebar-xs navbar-top">
<%@include file="/WEB-INF/view/main/topbar.jsp" %>
<div class="page-container">
	<div class="page-content">
		<div class="content-wrapper">
			<%@include file="/WEB-INF/view/main/menu.jsp" %>
			<!-- Page header -->
			<div class="page-header page-header-inverse has-cover"><!--밝은 배경 page-header-default page-header-inverse-->
				<div class="page-header-content page-header-sm"><!--bg-slate-400 page-header-lg / -xs --> 
					<h4  class="page-title">
						<i class="icon-address-book2 position-left"></i> 
						<span class="text-bold">은행CMS관리</span>
						<small class="display-block"></small>
					</h4> 
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			<div class="content" id="divContent">
				<div class="row">
					<div class="col-xs-12"> 
						<div class="panel panel-flat border-bottom-lg border-bottom-primary-300" id="listPanel">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>회원 신청 내역</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>
							<div class="pl-20 pr-20">
								<table class="table table-bordered table-hover no-sorting" id="cmsTable" data-page-length="10">
									<thead class="borderThead alpha-slate">
										<tr>
											<th data-orderable="false" rowspan="2">신청날짜</th>
											<th data-orderable="false" colspan="3">등록</th>
											<th data-orderable="false" colspan="3">수정</th>
											<th data-orderable="false" rowspan="2">전체</th>
											<th data-orderable="false" rowspan="2">결과요청</th>
										</tr>
										<tr>
											<th data-orderable="false">합계</th>
											<th data-orderable="false">성공</th>
											<th data-orderable="false">실패</th>
											<th data-orderable="false">합계</th>
											<th data-orderable="false">성공</th>
											<th data-orderable="false">실패</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div class="panel-footer panel-footer-transparent">			
								<a class="heading-elements-toggle"><i class="icon-more"></i></a>
								<div class="heading-elements">	
									<div class="ml-20">
										<button type="button" class="btn bg-primary legitRipple" onclick="refreshTable('cmsTable')">새로고침</button>
									</div>
								</div>
							</div>
						</div>
						
						
						<!-- Search layout-->
						<div class="panel panel-flat border-bottom-lg border-bottom-primary-300">
							<div class="panel-heading">
								<h5 class="panel-title">
									<i class="icon-folder-search position-left" data-toggle="tooltip" data-placement="top" title="금융기수가 있는 고객만 검색" data-html="true"></i>
									은행 회원 검색 조건
								</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div>
			
							<div class="panel-body"> 
								<form name="searchForm"  onkeypress="if(event.keyCode==13){searchBtn();}">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">금융기수</label>
												<select class="select select2-hidden-accessible" name="m_finance">
													<option value="전체">전체</option>
													<c:forEach var="fList" items="${financeList}">
													<option>${fList.m_finance}</option>
													</c:forEach>
												</select>
											</div>
										</div> 
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">CMS등록상태</label>
												<select class="select select2-hidden-accessible" name="m_cms">
													<option value="">전체</option>
													<option>미등록</option>
													<option>등록완료</option>
													<option>등록중(파일미등록)</option>
													<option>등록중</option>
													<option>수정요청</option>
													<option>수정중</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">회원번호</label>
												<input type="text" class="form-control input-sm" name="m_num">
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">고객명(계약자/결제자)</label>
												<input type="text" class="form-control input-sm" name="m_pay_owner">
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
											<div class="form-group">
												<label class="form-tit-label">진행상태</label>
												<select class="select select2-hidden-accessible" name="m_process">
													<option value="">전체</option>
													<c:forEach var="mprocess" items="${mprocessList}">
													<option>${mprocess}</option>
													</c:forEach>
												</select>
											</div>
										</div> 
										<div class="col-xs-12 text-right">
											<a onclick="searchBtn()" class="btn btn-primary"><i class="icon-arrow-down8 position-left"></i>조회하기</a>
										</div>
									</div>					
								</form>
							</div>
						</div>
						<!-- /Search layout -->
					
					
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="icon-address-book2 position-left"></i>은행CMS 조회결과</h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>
									</ul>
								</div>
							</div> 
							<table class="table table-hover" id="checkTable" data-page-length="25">
								<thead>
									<tr>
										<th class="active">선택</th>
										<th class="active">최종계약일</th>
										<th class="active">판매기수</th>
										<th class="active">금융기수</th>
										<th class="active">고객번호</th>
										<th class="active">고객명(계약자/결제자)</th>
										<th class="active">렌탈상품명</th>
										<th class="active">연락처(결제자)</th>
										<th class="active">약정일</th>
										<th class="active">월 이체액</th>
										<th class="active">진행상태</th>
										<th class="active">CMS등록상태</th>
										<th class="active">CMS ID</th>
										<th class="active">비고</th>
										<th class="active" data-orderable="false"></th>
									</tr>
								</thead>
								<tbody id="bankTbody">
								</tbody>
							</table>
						
							<div class="panel-footer panel-footer-transparent">			
								<a class="heading-elements-toggle"><i class="icon-more"></i></a>
								<div class="heading-elements">	
									<div class="btn-group dropup ml-20">
										<button type="button" class="btn btn-xs bg-primary-600 btn-labeled"><b><i class="icon-air"></i></b>일괄변경</button>
										<button type="button" class="btn btn-xs bg-primary-600 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
										<ul class="dropdown-menu">
											<li><a onclick="reg()"><i class="icon-trash-alt"></i>회원 등록 신청</a></li>
											<li><a onclick="changeCms()"><i class="icon-trash-alt"></i>회원 정보 수정 신청</a></li>
											<li><a onclick="cmsDelete()"><i class="icon-trash-alt"></i>회원 삭제</a></li>
											<li><hr></li>
											<li><a onclick="refreshTable('checkTable')"><i class="icon-trash-alt"></i>새로고침</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
			<!-- /content area -->
		</div>
	</div>
</div>


<%@include file="modal/bankModal.jsp"%>
<%@include file="modal/bankModalPsdate.jsp"%>
<%@include file="/WEB-INF/view/main/js.jsp"%>
<script>
//cms DT 컬럼별 들어갈 data
$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "ps_date" },
	    { "data": "regi" },
	    { "data": "regi_suc" },
	    { "data": "regi_fail" },
	    { "data": "modi" },
	    { "data": "modi_suc" },
	    { "data": "modi_fail" },
	    { "data": "total" },
	    { "data": "cms_result"}
	  ]
});
//첫 dataTable initialize
if(! $.fn.DataTable.isDataTable( '#cmsTable' )){
	ajaxDt('cmsTable','/finance/bank/sendList');
}

//dataTable 컬럼별 들어갈 data
$.extend( $.fn.dataTable.defaults, {
	"columns": [
	    { "data": "m_idx" },
	    { "data": "m_final_date" },
	    { "data": "mg_name" },
	    { "data": "m_finance" },
	    { "data": "m_num" },
	    { "data": "m_pay_owner" },
	    { "data": "pr_name" },
	    { "data": "m_phone" },
	    { "data": "m_paydate" },
	    { "data": "m_rental_f" },
	    { "data": "m_process" },
	    { "data": "m_cms" },
	    { "data": "cms_id" },
	    { "data": "m_cms_fail" },
	    {"data":null,"defaultContent":"<a class='btn bg-success' onclick='checkCms()'>cms조회</a>"}
	  ]
});
//첫 dataTable initialize
if(! $.fn.DataTable.isDataTable( '#checkTable' )){
	var param=$('form[name=searchForm]').serialize();
	checkDt('checkTable','/finance/member/bank?'+param);
}
//조회 버튼 클릭시
function searchBtn(){
	var param=$('form[name=searchForm]').serialize();
	var table=$('#checkTable').DataTable();
	table.ajax.url('/finance/member/bank?'+param).load();
}
//결제정보 modal
function member(){
	var midx=$(event.target).attr('m_idx');
	
}
//cms수정
function changeCms(){
	var param='m_idx=';
	var cnt=0;
	$('#bankTbody tr.selected').each(function(){
		param+=$(this).children('td:nth-child(6)').children('a').attr('m_idx')+',';
		cnt++;
	});
	
	if(cnt==0){
		alert('선택된 고객이 없습니다.');
		return;
	}
	if(window.confirm('회원정보 수정하시겠습니까?')){
		var setting={
			url:'/finance/bank/regi',
			type:'post',
			data:param+'&type=change'
		};
			
		blockById('checkTable');
		$.ajax(setting).done(function(res){
			refreshTable('checkTable');
			refreshTable('cmsTable');
			alert(res);
		}).always(function(){
			$('#checkTable').unblock();
		});;
	}
}
//등록 step1 cms선택
function reg(){
	var cnt=0;
	$('#bankTbody tr.selected').each(function(){
		cnt++;
	});
	if(cnt==0){
		alert('선택된 고객이 없습니다.');
		return;
	}
	
	$('#bankModalTitle').html('회원 등록');
	if(! $.fn.DataTable.isDataTable( '#bankModalTable' )){
		$.extend( $.fn.dataTable.defaults, {
			"columns": [
			    { "data": "cms_bank" },
			    { "data": "cms_id" },
			    { "data": "cms_memo" },
			    { "data": "cms_amount" },
			    { "data": "m_total" },
			    { "data": "m_ilsi" },//잔액 (임시로 칼럼명 씀)
			    { "data": "m_rental" },//월 이체액 (임시로 칼럼명 씀)
			    { "data": "m_idx" }//회원수 (임시로 칼럼명 씀)
			  ]
		});
		ajaxDt('bankModalTable','/finance/cms');
	}else{
		var table=$('#bankModalTable').DataTable();
		table.search('').draw();
	}
	
	$('#bankModal').modal('show');
}
//등록 step2 (바구니담기)
function cms(){
	var title=$('#bankModalTitle').html();
	var cms_idx=$(event.target).attr('cms_idx');
	if(window.confirm(title+' 하시겠습니까?')){
		var param='m_idx=';
		$('#bankTbody tr.selected').each(function(){
			param+=$(this).children('td:nth-child(6)').children('a').attr('m_idx')+',';
		});
		
		var setting={
			url:'/finance/bank/regi',
			type:'post',
			data:param+'&type=reg&cms_idx='+encodeURIComponent(cms_idx)+'&ps_date='+$('input[name=ps_date]').val()
		};
		
		blockById('inside_ml');
		$.ajax(setting).done(function(res){
			$('#bankModal').modal('hide');
			refreshTable('cmsTable');
			refreshTable('checkTable');
			alert(res);
		}).always(function(){
			$('#inside_ml').unblock();
		});;
	}
}
$('input[name=ps_date]').daterangepicker({
	startDate: moment(),
    singleDatePicker: true
});

//신청일자 클릭. 신청명단보기
function psDate(){
	var ps_date=$(event.target).html();
	$('#regFileBttn').attr('ps_date',ps_date);
	
	$('#psModalTitle').html(ps_date);
	if(! $.fn.DataTable.isDataTable( '#psModalTable' )){
		$.extend( $.fn.dataTable.defaults, {
			"columns": [
			    { "data": "ps_idx" },
			    { "data": "cms_bank" },
			    { "data": "cms_id" },
			    { "data": "m_num" },
			    { "data": "m_pay_owner" },
			    { "data": "m_cms" },
			    { "data": "ps_pay_idx" },
			    { "data": "ps_state" },
			    { "data": "ps_reason" },
			    { "data": "mf_file" }			    
			  ]
		});
		checkDt('psModalTable','/finance/bank/sendList/'+ps_date);
	}else{
		var table=$('#psModalTable').DataTable();
		table.search('');
		table.ajax.url('/finance/bank/sendList/'+ps_date).load();
	}
	
	$('#psModal').modal('show');
	
}
//신청목록에서 삭제 버튼
function cancelReg(){
	var param='ps_idx=';
	$('#psModalTable tr.selected').each(function(){
		param+=encodeURIComponent($(this).children('td:nth-child(1)').children('input[name=ps_idx]').val()+',');
	});
	
	var setting={
		url:'/finance/bank/deletePaySend',
		type:'post',
		data:param
	};
	
	blockById('bankPsDiv');
	$.ajax(setting)
	.done(function(res){
		refreshTable('psModalTable');
		refreshTable('cmsTable');
		alert(res);
	}).always(function(){
		$('#bankPsDiv').unblock();
	});;
}
//조회 get,신청취소(신청전으로 돌리기)delete,신청 put
function sendCms(sendType){
	var ps_date=$(event.target).parent().parent().children('td:nth-child(1)').children('a').html();
	var setting={
		url:'/finance/bank/sendList/'+ps_date,
		type:sendType
	};
	
	blockById('listPanel');
	$.ajax(setting)
	.done(function(res){
		alert(res);
		refreshTable('checkTable');
		refreshTable('cmsTable');
	}).fail(function(){
		alert('통신 에러 발생');
	}).always(function(){
		$('#listPanel').unblock();
	});
}
//약정서인증파일 업로드
function upFile(){
	var m_idx=$(event.target).attr('m_idx');
	var win=window.open('/member/'+Number(m_idx)+'/rentalFile','fileUp','width=500,height=200');
}
//파일 효성으로 전송
function regFile(){
	var ps_date=$('#regFileBttn').attr('ps_date');
	if(window.confirm(ps_date+'\n등록 상태가 등록중(파일 미등록)인 회원들의\n파일을 전송하시겠습니까?')){
		blockById('bankPsDiv');
		$.ajax('bank/sendFile?ps_date='+ps_date)
		.done(function(res){
			alert(res);
			refreshTable('psModalTable');
		})
		.always(function(){
			$('#bankPsDiv').unblock();
		});
	}
}
//결과요청
function getResult(){
	var ps_date=$(event.target).parent().parent().children('td:nth-child(1)').children('a').html();
	var setting={
		url:'/finance/bank/result/'+ps_date,
		type:'get'
	};
	
	blockById('listPanel');
	$.ajax(setting)
	.done(function(res){
		alert(res);
		refreshTable('checkTable');
		refreshTable('cmsTable');
	}).fail(function(){
		alert('통신 에러 발생');
	}).always(function(){
		$('#listPanel').unblock();
	});
}
//팝업
function member(){
	var midx=$(event.target).attr('m_idx');
	var win=window.open('/member/'+Number(midx)+'/pay','pay','width=1000,height=800');
}

//cms상태조회
function checkCms(){
	var m_idx=$(event.target).parent().parent().children('td:nth-child(6)').children('a').attr('m_idx');
	blockById('bankTbody');
	$.ajax({
		type:'put',
		url:'/finance/'+m_idx+'/checkCms'
	})
	.done(function(res){
		alert(res);
		refreshTable('checkTable');
	}).fail(function(){
		alert('통신 에러 발생');
	}).always(function(){
		$('#bankTbody').unblock();
	});
}

//m_idx가져오기
function getMidx(){
	var param='';
	$('#checkTable tr.selected').each(function(){
		param+=$(this).children('td:nth-child(6)').children('a').attr('m_idx')+',';
	});
	return param;
}

//회원삭제
function cmsDelete(){
	
	var param=getMidx();
	if(param==''){
		alert('선택된 고객이 없습니다.');
		return;
	}
	param='m_idx='+param;
	
	var title='회원삭제 하시겠습니까?';
	
	if(window.confirm(title)){
		blockById('checkTable');
		$.ajax({
			url:'/finance/card/api?'+param,
			type:'delete'
		}).done(function(res){
			alert(res);
			refreshTable('checkTable');
		}).always(function(){
			$('#checkTable').unblock();
		});
		
	}
}
$('[data-toggle=tooltip]').tooltip();
</script>
</body>
</html>