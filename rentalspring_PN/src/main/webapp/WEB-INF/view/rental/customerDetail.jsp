<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<%@include file="/WEB-INF/view/main/css.jsp"%>
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-inverse has-cover">
					<!--밝은 배경 page-header-default page-header-inverse-->
					<div class="page-header-content page-header-sm">
						<!--bg-slate-400 page-header-lg / -xs -->
						<div class="page-title">
							<h4>
								<i class="icon-address-book2 position-left"></i> <span
									class="text-bold">CS항목 신규 등록</span> <small
									class="display-block">신규 등록</small>
							</h4>
						</div>
					</div>
				</div>
				<!-- /page header -->

				<!-- Content area -->
				<form role="form" name="frm" method="post"
					enctype="multipart/form-data">
					<div class="content">
						<div class="row">
							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>1. CS항목 정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>구분</label> <select class="form_control"
															name="csm_gubun">
															<option value="0">선택</option>
															<option value="A">A</option>
															<option value="B">B</option>
															<option value="C">C</option>
														</select>

													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>사용구분</label> <select class="form_control"
															name="csm_status">
															<option value="0">선택</option>
															<option value="1">사용</option>
															<option value="2">미사용</option>
														</select>
													</div>
												</div>

												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>렌탈상품군</label> <input type="text" name="csm_rental"
															value="${pr.e_name}" class="form-control">
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>2. CS 소모품 구성
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-12">
													<table class="table table-hover" id="productTable">
														<thead>
															<tr>
																<th>상품명</th>
																<th>상품코드</th>
																<th>매입처</th>
																<th>원가</th>
																<th>판매가</th>
																<th>수량</th>
																<th>삭제</th>
															</tr>
														</thead>
														<tbody id="rproduct">
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr>


														</tbody>
														<tr>
															<td colspan="7"><a onclick="plist()">상품 추가</a></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>3. CS상세정보
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-4 col-md-2">
													<div class="form-group form-group-default ">
														<label>CS제목</label> <input type="text" name="csm_name"
															class="form-control">
													</div>
												</div>

												<div class="col-xs-6 col-sm-12 col-md-2">
													<div class="form-group form-group-default">
														<label>CS상세내용</label> <input type="text" name="csm_memo"
															class="form-control">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div
									class="panel panel-flat border-top-xlg border-top-primary-300">
									<div class="panel-heading">
										<h5 class="panel-title">
											<i class="icon-folder-search position-left"></i>4. 공임
										</h5>
									</div>
									<div class="panel-body">
										<div class="form-group-attached">
											<div class="row">
												<div class="col-xs-6 col-sm-3 col-md-2">
													<div class="form-group form-group-default">
														<label>CS공임</label> <input type="text"
															name="csm_installpee" value="0" onkeyup="numCommaKeyUp()"
															class="form-control" onkeydown="numKeyDown()">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<input type="hidden" id="cl_idx" name="cl_idx" value="">
										<a href="#" data-dismiss="modal" class="btn">닫기</a> <a
											onclick="update_memo()" class="btn btn-primary">변경</a>
									</div>
								</div>
							</div>


						</div>
					</div>
				</form>
				<!-- /content area -->
			</div>
		</div>
	</div>
	<%@include file="/WEB-INF/view/product/modal/productList.jsp"%>
	<%@include file="/WEB-INF/view/main/js.jsp"%>
	<script>
		var table = $('#productTable').DataTable();

		$.extend($.fn.dataTable.defaults, {
			"columns" : [ {
				"data" : "p_name"
			}, {
				"data" : "p_code"
			}, {
				"data" : "p_category1"
			}, {
				"data" : "p_cost"
			}, {
				"data" : "p_price"
			} ]
		});

		ajaxDt('table', '/product/productList');
		function plist() {
			$('#modal_large').modal('show');
		}

		function refreshTable() {
			var table = $('#productTable').DataTable();
			table.ajax.reload(null, false);
		}

		function startRental() {
			var param = $('form[name=frm]').serialize();
			$.ajax({
				url : 'csProduct',
				type : 'post',
				data : param
			}).done(function(data) {
				alert(data);
				if (data.trim() == '등록 완료') {
					opener.parent.location.reload();
					window.close();
				} else {
					opener.parent.location.reload();
					window.close();
				}
			});
		}

		function del() {
			table.row($(this).parents('tr')).remove().draw();
		}
	</script>
</body>
</html>