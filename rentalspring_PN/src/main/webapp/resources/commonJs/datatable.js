/* ------------------------------------------------------------------------------
*
*  # Buttons extension for Datatables. HTML5 examples
*
*  Specific JS code additions for datatable_extension_buttons_html5.html page
*
*  Version: 1.1
*  Latest update: Mar 6, 2016
*
* ---------------------------------------------------------------------------- */

    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10행', '25행', '50행', '행 전체' ]
        ],
        dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '_INPUT_',
            searchPlaceholder: '재검색',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
            emptyTable: '데이터가 없습니다.',
            info: '_TOTAL_개의 검색결과 [ _START_ ~ _END_  ]',
            infoEmpty: '',
            infoFiltered: '(총 _MAX_개의 자료 중에서 재검색)',
            lengthMenu: '_MENU_',
            zeroRecords: '검색 결과가 없습니다.'
        },
        buttons: {            
            buttons: [
				{extend: 'selectAll', className: 'btn btn-xs bg-slate-400'},
				{extend: 'selectNone', className: 'btn btn-xs bg-slate-400'},
				{
					extend: 'pageLength',
					className: 'btn btn-xs bg-slate-600'
				},
                {
                    extend: 'colvis',
                    text:'&nbsp;&nbsp;열 선택 <span class="caret"></span>',
                    className: 'btn btn-xs bg-slate-600 btn-icon'
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-xs bg-slate-800 legitRipple',
                    text: '<i class="icon-file-excel"></i>&nbsp;&nbsp;엑셀다운',
                    footer:true,
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ]
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
	
    // Column selectors ----------------------------------------------
    var table_reorder = $('.datatable-button-html5-columns').DataTable({
        order: [[ 0, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        keys: {
            className: 'focus'
        },
        select: true
    });
	
    // Column selectors ----------------------------------------------
    var table_reorder = $('.datatable-select').DataTable({
        columnDefs: [
            {
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }
        ],
        order: [[ 1, 'desc' ]],
        stateSave: false,
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    
 // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-gift-log-table').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
	 	   	{
	            orderable: true,
		            targets: [0]
	        },
            {
                orderable: false,
 	            targets: [1, 2, 3, 4, 5, 6]
            },
        ],
        order: [[ 0, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        }
    });
    
 // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-gift-log-table2').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
	 	   	{
	            orderable: true,
		            targets: [0]
	        },
            {
                orderable: false,
 	            targets: [1, 2, 3, 4]
            },
        ],
        order: [[ 0, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-possible-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
        ],
        order: [[ 1, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-franchise-level').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6]
            },
        ],
        order: [[ 3, 'asc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    /**회원관리*/
    var table_reorder = $('.mr-datatable-customer').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            },
        ],
        /*order: [[ 1, 'desc' ]],*/
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });

    
    /**상부점정산*/
    var table_reorder = $('.mr-datatable-upper-jungsan').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]
            },
        ],
        order: [[ 1, 'DESC' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    /**하부점정산*/
    var table_reorder = $('.mr-datatable-low-jungsan').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35]
            },
        ],
        order: [[ 1, 'DESC' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    
    /**사은품정산*/
    var table_reorder = $('.mr-datatable-gift-jungsan').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
            },
        ],
        order: [[ 1, 'DESC' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.datatable-ajax').DataTable({
    	serverSide: true,
    	autoWidth: false,
        lengthMenu: [
            [ 5, 10, 20, -1 ],
            [ '5행', '10행', '20행', '행 전체' ]
        ],
        dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '_INPUT_',
            searchPlaceholder: '재검색',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
            emptyTable: '데이터가 없습니다.',
            info: '_TOTAL_개의 검색결과 [ _START_ ~ _END_  ]',
            infoEmpty: '',
            infoFiltered: '(총 _MAX_개의 자료 중에서 재검색)',
            lengthMenu: '_MENU_',
            zeroRecords: '검색 결과가 없습니다.'
        },
        buttons: {            
            buttons: [
				{extend: 'selectAll', className: 'btn btn-xs bg-slate-400'},
				{extend: 'selectNone', className: 'btn btn-xs bg-slate-400'},
				{
					extend: 'pageLength',
					className: 'btn btn-xs bg-slate-600'
				}
            ]
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },
        columnDefs: [
            {
                className: 'select-checkbox',
                targets: 0
            }
        ],
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        //order: [[ 16, 'desc' ]],
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.datatable-mr-order-supplier').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
        ],
        order: [[ 1, 'asc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    var table_reorder = $('.mr-datatable-gift-order').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
            },
        ],
        //order: [[ 17, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-gift-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
            },
        ],
        order: [[ 0, 'desc' ]],
        stateSave: true,
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
 // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-del-gift-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
            },
        ],
        order: [[ 0, 'desc' ]],
        stateSave: true,
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    var table_reorder = $('.mr-datatable-request-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
 	   		{ "orderable": false, "targets": 0 }
     	   /*{
                className: 'select-checkbox',
                targets: 0
            },*/
            /*{
                orderable: false
 	            //targets: [0, 1, 2, 3, 4, 5, 6]
            },*/
        ],
        order: [[ 0, 'desc' ]],
        //stateSave: true,
        //colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        /*select: {
            style: 'multi',
            selector: 'td:first-child'
        }*/
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.datatable-mr-gift-company').DataTable({
    	autoWidth: false,
    	columnDefs: [
      	   {
                 className: 'select-checkbox',
                 targets: 0
             },
             {
                 orderable: true,
  	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
             },
         ],
    	scrollX: true,
    	colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        order: [[ 0, 'desc' ]],
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-product-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                width: '15%', targets: 8
            },
            {
                width: '7%', targets: 9
            },
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-re-product-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8]
            },
        ],
        order: [[ 3, 'asc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-sort-product-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            },
        ],
        order: [[ 3, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
 // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-manufacture-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5]
            },
        ],
        order: [[ 1, 'asc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-announce-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6]
            },
            
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    var table_reorder = $('.mr-datatable-home_announce').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [2]
            },
            
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
       select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-review-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-simple-review-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6]
            },
            {
                width: '20%', targets: 3
            },
            {
                width: '35%', targets: 4
            },
            
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
	
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-qna-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
	 	   	{
	             className: 'select-checkbox',
	             targets: 0
	        },
            {
                orderable: true,
 	            targets: [0, 1, 3, 4, 5, 6, 7]
            },
            {
                width: '30%', targets: 4
            },
            
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-tip-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
	 	   	{
	            className: 'select-checkbox',
	            targets: 0
	        },
            {
                orderable: true,
 	            targets: [0, 1, 3, 4, 5, 6, 7]
            },
            
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-event-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
	 	   	{
	            className: 'select-checkbox',
	            targets: 0
	        },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            
        ],
        order: [[ 7, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    // Column selectors ----------------------------------------------
    var table_reorder = $('.mr-datatable-reply-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
	 	   	{
	            className: 'select-checkbox',
	            targets: 0
	        },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5]
            },
            
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });

    var table_reorder = $('.datatable-mr-product-popup').DataTable({
    	autoWidth: false,
        dom: '<"datatable-header datatable-header-accent"f><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        stateSave: true,
        colReorder: true,
        select: true,
        order: [[ 0, 'desc' ]],
        colReorder: {
            fixedColumnsLeft: 1
        }
    });
    
    var table_reorder = $('.datatable-mr-product-popup2').DataTable({
    	autoWidth: false,
        dom: '<"datatable-header datatable-header-accent"f><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        columnDefs: [
            {
                className: 'select-checkbox',
                targets: 0
            },
        ],
        stateSave: true,
        colReorder: true,
        select: true,
        order: [[ 0, 'desc' ]],
        colReorder: {
            fixedColumnsLeft: 1
        },
	    keys: {
	        className: 'alpha-slate'
	    },
	    select: {
	        style: 'multi',
	        selector: 'td:first-child'
	    }
    });
    
    var table_reorder = $('.datatable-mr-product-board').DataTable({
    	autoWidth: false,
        dom: '<"datatable-header datatable-header-accent"f><"datatable-scroll-wrap"t><"datatable-footer"ip>',
    	columnDefs: [
            {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: false,
 	            targets: [0]
            },
        ],
        order: [[ 12, 'desc' ]],
        stateSave: true,
        colReorder: true,
        select: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
   
    
    var table_reorder = $('.datatable-mr-supplier-popup').DataTable({
    	autoWidth: false,
    	columnDefs: [
            {
                width: '15%', targets: 0
            },
        ],
        stateSave: true,
        colReorder: true,
        select: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'single',
            selector: 'td:first-child'
        }
    });
    
    /**배너관리*/
    var table_reorder = $('.mr-datatable-banner-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5]
            },
        ],
        order: [[ 5, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });
    
    /**팝업관리*/
    var table_reorder = $('.mr-datatable-popup-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6]
            },
        ],
        order: [[ 6, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });

    /**사은품 등급관리*/
    var table_reorder = $('.mr-datatable-gr-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4]
            },
        ],
        order: [[ 2, 'asc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });

    var table_reorder = $('.mr-datatable-hamburger-list').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: true,
 	            targets: [0, 1, 2, 3, 4, 5, 6]
            },
           
        ],
        order: [[ 6, 'desc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
    });

    // Column selectors ----------------------------------------------
    var table_reorder = $('.datatable-cal').DataTable({
        dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        columnDefs: [
            {
                className: 'select-checkbox',
                targets: 0
            },
            {
                orderable: false,
	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
            },
           
        ],
        order: [[ 1, 'asc' ]],
        stateSave: true,
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
    });
    
    var table_reorder = $('.datatable-cal-result').DataTable({
       columnDefs: [
           {
               orderable: false,
	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
           },
          
       ],
       order: [[ 0, 'asc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
   });
    
   var table_reorder = $('.mr-datatable-higherfee').DataTable({
    	autoWidth: false,
 	   	columnDefs: [
	 	   	{
	            orderable: true,
		        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
	        }
        ],
        //order: [[ 0, 'asc' ]],
        stateSave: true,
        /*scrollY: 300,*/
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        keys: {
            className: 'focus'
        }
    });
   
   var table_reorder = $('.mr-datatable-higherfee_1').DataTable({
   	autoWidth: false,
	   	columnDefs: [
	 	   	{
	            orderable: true,
		        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
	        }
       ],
       //order: [[ 0, 'asc' ]],
       stateSave: true,
       /*scrollY: 300,*/
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
       keys: {
           className: 'focus'
       }
   });
   
   	var table_reorder = $('.mr-datatable-higherfee-list').DataTable({
       columnDefs: [
           {
               orderable: false,
	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
           },
          
       ],
       order: [[ 0, 'asc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
   	});
   
   var table_reorder = $('.mr-datatable-lowfee').DataTable({
       columnDefs: [
    	   {
               className: 'select-checkbox',
               targets: 0
           },
           {
               orderable: false,
	            targets: [0, 1, 2, 3, 4, 5, 6]
           },
          
       ],
       order: [[ 1, 'asc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
       keys: {
           className: 'alpha-slate'
       },
       select: {
           style: 'multi',
           selector: 'td:first-child'
       },
   });
   
   var table_reorder = $('.mr-account-customer-table').DataTable({
       columnDefs: [

           {
               orderable: true,
	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
           },
          
       ],
       order: [[ 0, 'desc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
       keys: {
           className: 'alpha-slate'
       },
   });
   
   	var table_reorder = $('.mr-datatable-lowfee-list').DataTable({
       columnDefs: [
           {
               orderable: false,
	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
           },
          
       ],
       order: [[ 0, 'asc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
   });
    
   var table_reorder = $('.mr-datatable-lowfee-list').DataTable({
       columnDefs: [
           {
               orderable: false,
	            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
           },
          
       ],
       order: [[ 0, 'asc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
   });
   
    
   var table_reorder = $('.mr-datatable-admin-list').DataTable({
	   columnDefs: [
    	   {
               className: 'select-checkbox',
               targets: 0
           },
           {
               orderable: true,
	           targets: [0, 1, 2, 3, 4, 5, 6, 7]
           },
       ],
       order: [[ 0, 'desc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
       keys: {
           className: 'focus'
       },
       select: {
           style: 'multi',
           selector: 'td:first-child'
       },
   });

   var table_reorder = $('.mr-datatable-franchise-list').DataTable({
	   columnDefs: [
    	   {
               className: 'select-checkbox',
               targets: 0
           },
           {
               orderable: true,
	           targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
           },
           {
               width: '8%', targets: 3
           },
           {
               width: '6%', targets: 8
           },
       ],
       order: [[ 1, 'desc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
       keys: {
           className: 'focus'
       },
       select: {
           style: 'multi',
           selector: 'td:first-child'
       },
   });

   var table_reorder = $('.datatable-mr-fcustomer-popup').DataTable({
	   columnDefs: [
    	   {
               className: 'select-checkbox',
               targets: 0
           },
           {
               orderable: true,
	           targets: [0, 1, 2, 3, 4, 5, 6]
           },

       ],
       order: [[ 1, 'desc' ]],
       stateSave: true,
       colReorder: true,
       colReorder: {
           fixedColumnsLeft: 1
       },
       keys: {
           className: 'focus'
       },
       select: {
           style: 'multi',
           selector: 'td:first-child'
       },
   });
   
    // Group ----------------------------------------------
    var table_reorder = $('.datatable-group').DataTable({
        columnDefs: [
            {
                visible: false,
                targets: 0
            },
            {
                orderable: false,
                className: 'select-checkbox',
                targets: 1
            },
            /*{
                targets: 10,
                width: 75
            },*/
            /*{ 
                orderable: false,
                width: 10,
                targets: 10
            }*/
        ],
        order: [[ 2, 'desc' ]],
        stateSave: true,
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 2
        },
        keys: {
            className: 'alpha-slate'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;
 
            api.column(0, { page: 'current' }).data().each(function(group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="alpha-primary group border-double"><td colspan="12" class="text-left text-bold">' + group + '</td></tr>'
                    );
 
                    last = group;
                }
            });

            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function(settings) {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    // Order by the grouping
    $('.table-orders-history tbody').on( 'click', 'tr.group', function() {
        var currentOrder = table.order()[0];
        if (currentOrder[0] === 0 && currentOrder[1] === 'asc') {
            table.order([0, 'desc']).draw();
        }
        else {
            table.order([0, 'asc']).draw();
        }
    });
	
    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    /*$('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');*/


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });

function ajaxDt(id,ajaxUrl){
	
	$('#'+id).DataTable({
    	serverSide: true,
    	ajax:{
    		url:ajaxUrl,
    		type:'post'
    	},
    	cache:false,
    	autoWidth: false,
        lengthMenu: [
            [ 5, 10, 20, -1 ],
            [ '5행', '10행', '20행', '행 전체' ]
        ],
        dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '_INPUT_',
            searchPlaceholder: '재검색',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
            emptyTable: '데이터가 없습니다.',
            info: '_TOTAL_개의 검색결과 [ _START_ ~ _END_  ]',
            infoEmpty: '',
            infoFiltered: '(총 _MAX_개의 자료 중에서 재검색)',
            lengthMenu: '_MENU_',
            zeroRecords: '검색 결과가 없습니다.'
        },
        /*buttons: {            
            buttons: [
				{
					extend: 'pageLength',
					className: 'btn btn-xs bg-slate-600'
				}
            ]
        },*/
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        //order: [[ 16, 'desc' ]],
        keys: {
            className: 'alpha-slate'
        }
    });
}
function checkDt(id,ajaxUrl){
	$('#'+id).DataTable({
		serverSide: true,
    	ajax:{
    		url:ajaxUrl,
    		type:'post'
    	},
    	cache:false,
    	autoWidth: false,
 	   	columnDefs: [
     	   {
                className: 'select-checkbox',
                targets: 0
            }
        ],
        order: [[ 0, 'asc' ]],
        //stateSave: true,
        /*scrollY: 300,*/
        colReorder: false,
        /*colReorder: {
            fixedColumnsLeft: 1
        },*/
        keys: {
            className: 'focus'
        },
        select: {
            style: 'multi',
            selector: 'td:first-child'
        }
	});
	
}

function ajaxDtGet(id,ajaxUrl){
	
	$('#'+id).DataTable({
    	serverSide: true,
    	ajax:{
    		url:ajaxUrl,
    		type:'get'
    	},
    	cache:false,
    	autoWidth: false,
        lengthMenu: [
            [ 5, 10, 20, -1 ],
            [ '5행', '10행', '20행', '행 전체' ]
        ],
        dom: '<"datatable-header datatable-header-accent"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '_INPUT_',
            searchPlaceholder: '재검색',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
            emptyTable: '데이터가 없습니다.',
            info: '_TOTAL_개의 검색결과 [ _START_ ~ _END_  ]',
            infoEmpty: '',
            infoFiltered: '(총 _MAX_개의 자료 중에서 재검색)',
            lengthMenu: '_MENU_',
            zeroRecords: '검색 결과가 없습니다.'
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        },
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1
        },
        //order: [[ 16, 'desc' ]],
        keys: {
            className: 'alpha-slate'
        }
    });
}