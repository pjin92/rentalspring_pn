//ajax 검은 화면
function loadDiv(){
	$(document).ajaxSend(function(){
		$('#black').fadeIn();
	});
	$(document).ajaxError(function(){
		alert('에러 발생.\n다시 시도 부탁드립니다.');
		$('#black').hide();
	});
	$(document).ajaxComplete(function(){
		$('#black').hide();
	});
}
//table 로딩 중화면
function tableLoad(tableId){
	$(document).ajaxSend(function(){
		$('#'+tableId).block({
		    message: '<i class="icon-spinner4 spinner"></i>',
		    overlayCSS: {
		        backgroundColor: '#fff',
		        opacity: 0.8,
		        cursor: 'wait',
		        'z-index':5000
		    },
		    css: {
		        border: 0,
		        padding: 0,
		        backgroundColor: 'transparent',
		        'z-index':5001
		    }
		});
	}).ajaxSuccess(function(){
		$('#'+tableId).unblock();
	}).ajaxError(function(event, jqxhr, settings, thrownError){
		if(jqxhr.statusText!='abort'){
			alert('에러 발생.\n다시 시도 부탁드립니다.');
			$('#'+tableId).unblock();
		}
	});
//	.ajaxComplete(function(){
//		$('#'+tableId).unblock();
//	});
}
function tableLoadUnderModal(tableId){
	$(document).ajaxSend(function(){
		$('#'+tableId).block({
		    message: '<i class="icon-spinner4 spinner"></i>',
		    overlayCSS: {
		        backgroundColor: '#fff',
		        opacity: 0.8,
		        cursor: 'wait',
		        'z-index':900
		    },
		    css: {
		        border: 0,
		        padding: 0,
		        backgroundColor: 'transparent',
		        'z-index':901
		    }
		});
	}).ajaxSuccess(function(){
		$('#'+tableId).unblock();
	}).ajaxError(function(event, jqxhr, settings, thrownError){
		if(jqxhr.statusText!='abort'){
			alert('에러 발생.\n다시 시도 부탁드립니다.');
			$('#'+tableId).unblock();
		}
	});
}

function blockById(compid){
	$('#'+compid).block({
	    message: '<i class="icon-spinner4 spinner"></i>',
	    overlayCSS: {
	        backgroundColor: '#fff',
	        opacity: 0.8,
	        cursor: 'wait',
	        'z-index':5002
	    },
	    css: {
	        border: 0,
	        padding: 0,
	        backgroundColor: 'transparent',
	        'z-index':5003
	    }
	});
}
//숫자만 return
function onlyNum(str){
	var value=str+'';
	while( value.includes(',')){
		value=value.replace(',');
	}
	value=value.replace(/[^0-9]/gi,'')
	return value;
}
//3자리마다 컴마 찍힌 값으로 return
function comma(strOri){
	var str=onlyNum(strOri+'')+'';
	str=Number(str)+'';
	return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//onkeydown에 주면 숫자만 입력
function numKeyDown(){
	var key=Number(event.which);
	if (event.shiftKey||key<48 || (key>57 && key<96) || key > 105) {
		if(key==8||key==35||key==36||key==37||key==39||key==46){
		}else{
			event.preventDefault();
			event.returnValue = false;
		}
	}
}
//onkeyUp에 주면 숫자+콤마. (한글제거)
function numCommaKeyUp(){
	var tar=$(event.target);
	$(tar).val(comma($(tar).val()));
}
//onkeyUp에 주면 숫자. (한글제거)
function numKeyUp(){
	var tar=$(event.target);
	$(tar).val(onlyNum($(tar).val()));
}
//dataTable 새로고침
function refreshTable(tableId){
	var table=$('#'+tableId).DataTable();
	table.ajax.reload(null,false);
}
//dataTable 새로고침+block
function refreshTableBlock(tableId){
	blockById(tableId);
	var table=$('#'+tableId).DataTable();
	table.ajax.reload(function(){
		$('#'+tableId).unblock();
	},false);
}
